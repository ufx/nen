﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Cerberus.Agent {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main (string[] args) {
            System.Threading.Thread.CurrentThread.Name = "Main";
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (args.Length > 0 && string.Compare(args[0], "--console", true) == 0) {
                var service = new AgentService();
                service.Run();

                Console.WriteLine("Cerberus agent running in console mode.  Please enter to stop.");
                Console.ReadLine();
                service.Stop();
            }
            else
                ServiceBase.Run(new ServiceBase[] { new AgentService() });
        }
    }
}
