﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.IO;

namespace Cerberus.Agent {
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer {
        public ProjectInstaller () {
            InitializeComponent();
        }

        public override void Install (IDictionary stateSaver) {
            base.Install(stateSaver);

            // Find configuration file.
            var assemblypath = Context.Parameters["assemblypath"];
            var installDirectory = System.IO.Path.GetDirectoryName(assemblypath);
            var configPath = Path.Combine(installDirectory, "Cerberus.Agent.exe.config");

            // Load configuration file and snip replacement section.
            var lines = File.ReadAllLines(configPath).ToList();
            var beginIndex = lines.FindIndex(s => s.Contains("<!-- CONFIG BEGIN -->"));
            var endIndex = lines.FindIndex(s => s.Contains("<!-- CONFIG END -->"));
            lines.RemoveRange(beginIndex, (endIndex - beginIndex) + 1);

            // Insert installer configuration values.
            lines.InsertRange(beginIndex, GenerateConfiguration());

            // Write configuration file.
            File.WriteAllLines(configPath, lines.ToArray());
        }

        private IEnumerable<string> GenerateConfiguration () {
            var name = Context.Parameters["name"];
            var host = Context.Parameters["host"];
            var port = Context.Parameters["port"];
            yield return "  <agent name=\"" + name + "\" server=\"cerberus://" + host + ":" + port + "\">";

            var sevenzipPath = Context.Parameters["sevenzippath"];
            if (!File.Exists(sevenzipPath))
                throw new InstallException("7zip path does not exist.");

            yield return "    <sevenzip path=\"" + sevenzipPath + "\" />";

            var sqlConnectionString = Context.Parameters["sqlconnectionstring"];
            yield return "    <sql connectionString=\"" + sqlConnectionString + "\"/>";

            yield return "    <clientSsl validateCertificates=\"true\"/>";

            yield return "  </agent>";
        }
    }
}
