﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.IO;

namespace Cerberus.Agent {
    public partial class AgentService : ServiceBase {
        private Thread _listener;

        public AgentService () {
            InitializeComponent();
        }

        #region Service Start / Stop
        protected override void OnStart (string[] args) {
            Run();
        }

        public void Run () {
            Update();

            // Dynamically load and execute core.
            var coreAssembly = Assembly.LoadFile(Environment.CurrentDirectory + "\\Cerberus.Core.dll");
            var coreType = coreAssembly.GetType("Cerberus.Agent.Core", true);
            var core = Activator.CreateInstance(coreType);

            var runMethod = coreType.GetMethod("Run");

            _listener = new Thread(new ThreadStart(() => {
                runMethod.Invoke(core, null);
            }));
            _listener.Name = "ClientService Listener";
            _listener.IsBackground = true;
            _listener.Start();
        }

        private void Update () {
            if (!Directory.Exists("Update"))
                return;

            foreach (var fileName in Directory.GetFiles("Update")) {
                var newFileName = Environment.CurrentDirectory + "\\" + Path.GetFileName(fileName);
                File.Move(fileName, newFileName);
            }

            Directory.Delete("Update", true);
        }

        protected override void OnStop () {
            _listener.Abort();
        }
        #endregion
    }
}
