﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Security.Cryptography.X509Certificates;

namespace Cerberus.Configuration {
    #region Server Configuration
    public class ServerConfig : ConfigurationSection {
        public override bool IsReadOnly () {
            return false;
        }

        [ConfigurationProperty("port", DefaultValue = 33733)]
        public int Port {
            get { return (int) this["port"]; }
            set { this["port"] = value; }
        }

        [ConfigurationProperty("clientDirectory", DefaultValue = ".\\Client")]
        public string ClientDirectory {
            get { return (string) this["clientDirectory"]; }
            set { this["clientDirectory"] = value; }
        }

        [ConfigurationProperty("ssl", IsRequired = true)]
        public SslConfig Ssl {
            get { return (SslConfig) this["ssl"]; }
            set { this["ssl"] = value; }
        }

        [ConfigurationProperty("log", IsRequired = true)]
        public LogConfig Log {
            get { return (LogConfig) this["log"]; }
            set { this["log"] = value; }
        }

        [ConfigurationProperty("agents")]
        public AgentTokenCollection Agents {
            get { return (AgentTokenCollection) this["agents"]; }
            set { this["agents"] = value; }
        }
    }

    public class SslConfig : ConfigurationElement {
        [ConfigurationProperty("storeLocation", DefaultValue = "LocalMachine")]
        public StoreLocation StoreLocation {
            get { return (StoreLocation) this["storeLocation"]; }
            set { this["storeLocation"] = value; }
        }

        [ConfigurationProperty("storeName", DefaultValue = "My")]
        public StoreName StoreName {
            get { return (StoreName) this["storeName"]; }
            set { this["storeName"] = value; }
        }

        [ConfigurationProperty("thumbprint", IsRequired = true)]
        public string Thumbprint {
            get { return (string) this["thumbprint"]; }
            set { this["thumbprint"] = value; }
        }
    }

    public class LogConfig : ConfigurationElement {
        [ConfigurationProperty("connectionString", IsRequired = true)]
        public string ConnectionString {
            get { return (string) this["connectionString"]; }
            set { this["connectionString"] = value; }
        }
    }

    public class AgentTokenCollection : ConfigurationElementCollection {
        public override bool IsReadOnly () {
            return base.IsReadOnly();
        }

        protected override ConfigurationElement CreateNewElement () {
            return new AgentTokenConfig();
        }

        protected override object GetElementKey (ConfigurationElement element) {
            return ((AgentTokenConfig) element).Name;
        }

        public void Add (AgentTokenConfig agent) {
            BaseAdd(agent);
        }
    }

    public class AgentTokenConfig : ConfigurationElement {
        public override bool IsReadOnly () {
            return false;
        }

        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name {
            get { return (string) this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("token", IsRequired = true)]
        public Guid Token {
            get { return (Guid) this["token"]; }
            set { this["token"] = value; }
        }
    }
    #endregion

    #region Agent Configuration
    public class AgentConfig : ConfigurationSection {
        public override bool IsReadOnly () {
            return false;
        }

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name {
            get { return (string) this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("server", IsRequired = true)]
        public Uri Server {
            get { return (Uri) this["server"]; }
            set { this["server"] = value; }
        }

        [ConfigurationProperty("token")]
        public Guid Token {
            get { return (Guid) this["token"]; }
            set { this["token"] = value; }
        }

        [ConfigurationProperty("sevenzip", IsRequired = true)]
        public SevenZipConfig SevenZip {
            get { return (SevenZipConfig) this["sevenzip"]; }
            set { this["sevenzip"] = value; }
        }

        [ConfigurationProperty("sql", IsRequired = true)]
        public SqlConfig Sql {
            get { return (SqlConfig) this["sql"]; }
            set { this["sql"] = value; }
        }

        [ConfigurationProperty("clientSsl", IsRequired = true)]
        public ClientSslConfig ClientSsl {
            get { return (ClientSslConfig) this["clientSsl"]; }
            set { this["clientSsl"] = value; }
        }
    }

    public class ClientSslConfig : ConfigurationElement {
        [ConfigurationProperty("validateCertificates", DefaultValue = true)]
        public bool ValidateCertificates {
            get { return (bool) this["validateCertificates"]; }
            set { this["validateCertificates"] = value; }
        }
    }

    public class SevenZipConfig : ConfigurationElement {
        [ConfigurationProperty("path", IsRequired = true)]
        public string Path {
            get { return (string) this["path"]; }
            set { this["path"] = value; }
        }
    }

    public class SqlConfig : ConfigurationElement {
        [ConfigurationProperty("connectionString", IsRequired = true)]
        public string ConnectionString {
            get { return (string) this["connectionString"]; }
            set { this["connectionString"] = value; }
        }
    }
    #endregion
}
