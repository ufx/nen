﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using log4net;

using Cerberus.Agent;
using Cerberus.Diagnostics;

namespace Cerberus {
    public static class Log {
        public static void InitializeDefaultLogger () {
            Default = LogManager.GetLogger("Default");
            log4net.Config.XmlConfigurator.Configure();
        }

        public static void SetPersistentNetworkClient (NetworkClient client) {
            foreach (var appender in Default.Logger.Repository.GetAppenders().OfType<NetworkAppender>())
                appender.PersistentClient = client;
        }

        public static ILog Default { get; private set; }
    }
}
