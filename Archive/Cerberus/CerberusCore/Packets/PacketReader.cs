﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cerberus.Packets {
    public class PacketReader : BinaryReader {
        public PacketReader (Stream stream)
            : base(stream, Encoding.UTF8) { }

        public Packet ReadPacket () {
            var packet = Packet.Create((PacketType) ReadByte());
            packet.Read(this);

            Log.Default.DebugFormat("Read packet {0}", packet);

            return packet;
        }

        public Guid ReadGuid () {
            return new Guid(ReadBytes(16));
        }
    }
}
