﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cerberus.Packets {
    public class PacketWriter : BinaryWriter {
        public PacketWriter (Stream stream)
            : base(stream, Encoding.UTF8) { }

        public void Write (Packet packet) {
            Log.Default.DebugFormat("Write packet {0}", packet);

            Write((byte) packet.PacketType);
            packet.Write(this);
        }

        public void Write (Guid guid) {
            Write(guid.ToByteArray(), 0, 16);
        }
    }
}
