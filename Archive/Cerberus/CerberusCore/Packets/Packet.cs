﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;

using Cerberus.Internal;
using Cerberus.Diagnostics;

namespace Cerberus.Packets {
    public abstract class Packet {
        public virtual void Read (PacketReader reader) {
            ReadBody(reader);
        }

        public virtual void Write (PacketWriter writer) {
            WriteBody(writer);
        }

        protected virtual void ReadBody (PacketReader reader) { }
        protected virtual void WriteBody (PacketWriter writer) { }

        public abstract PacketType PacketType { get; }

        public static Packet Create (PacketType type) {
            switch (type) {
                case PacketType.Connect: return new ConnectPacket();
                case PacketType.Connected: return new ConnectedPacket();
                case PacketType.Failure: return new FailurePacket();
                case PacketType.Ready: return new ReadyPacket();
                case PacketType.RunScript: return new RunScriptPacket();
                case PacketType.SendFile: return new SendFilePacket();
                case PacketType.SendLogEvent: return new SendLogEventPacket();
                case PacketType.SendReport: return new SendReportPacket();
                default:
                    throw new ProtocolException(LS.T("Invalid packet type '{0}'", type));
            }
        }
    }

    #region Control Packets
    public class FailurePacket : Packet {
        protected override void ReadBody (PacketReader reader) {
            CorrelationId = reader.ReadGuid();
            Message = reader.ReadString();
        }

        protected override void WriteBody (PacketWriter writer) {
            writer.Write(CorrelationId);
            writer.Write(Message);
        }

        public override PacketType PacketType {
            get { return PacketType.Failure; }
        }

        public Guid CorrelationId { get; set; }
        public string Message { get; set; }

        public override string ToString () {
            return "(Failure: " + Message + ")";
        }
    }

    public class ConnectedPacket : Packet {
        protected override void ReadBody (PacketReader reader) {
            NewAuthenticationToken = reader.ReadGuid();
        }

        protected override void WriteBody (PacketWriter writer) {
            writer.Write(NewAuthenticationToken);
        }

        public override PacketType PacketType {
            get { return Packets.PacketType.Connected; }
        }

        public Guid NewAuthenticationToken { get; set; }

        public override string ToString () {
            return "(Connected)";
        }
    }
    #endregion

    #region Content Packets
    public abstract class ContentPacket : Packet {
        protected ContentPacket () {
            Id = Guid.NewGuid();
        }

        public override void Read (PacketReader reader) {
            Id = reader.ReadGuid();
            ReadBody(reader);
        }

        public override void Write (PacketWriter writer) {
            writer.Write(Id);
            WriteBody(writer);
        }

        public FailurePacket Fail (string message) {
            return new FailurePacket() { CorrelationId = Id, Message = message };
        }

        public Guid Id { get; private set; }
    }

    public class ConnectPacket : ContentPacket {
        protected override void ReadBody (PacketReader reader) {
            Version = reader.ReadString();
            MachineName = reader.ReadString();
            ClientName = reader.ReadString();
            IsTransient = reader.ReadBoolean();
            AuthenticationToken = reader.ReadGuid();
        }

        protected override void WriteBody (PacketWriter writer) {
            writer.Write(Version);
            writer.Write(MachineName);
            writer.Write(ClientName);
            writer.Write(IsTransient);
            writer.Write(AuthenticationToken);
        }

        public override PacketType PacketType {
            get { return PacketType.Connect; }
        }

        public string Version { get; set; }
        public string MachineName { get; set; }
        public string ClientName { get; set; }
        public bool IsTransient { get; set; }
        public Guid AuthenticationToken { get; set; }

        public override string ToString () {
            return "(Connect: " + ClientName + ")";
        }
    }

    public class ReadyPacket : ContentPacket {
        public override PacketType PacketType {
            get { return PacketType.Ready; }
        }

        public override string ToString () {
            return "(Ready)";
        }
    }

    public class RunScriptPacket : ContentPacket {
        protected override void ReadBody (PacketReader reader) {
            Name = reader.ReadString();
            Script = reader.ReadString();
        }

        protected override void WriteBody (PacketWriter writer) {
            writer.Write(Name);
            writer.Write(Script);
        }

        public override PacketType PacketType {
            get { return PacketType.RunScript; }
        }

        public string Name { get; set; }
        public string Script { get; set; }

        public override string ToString () {
            return "(RunScript: " + Name + ")";
        }
    }

    public class SendFilePacket : ContentPacket {
        // The actual contents of this packet are copied to/from their
        // respective files so large files do not need to be kept in memory.

        protected override void ReadBody (PacketReader reader) {
            Path = reader.ReadString();
            Length = reader.ReadInt64();
        }

        protected override void WriteBody (PacketWriter writer) {
            writer.Write(Path);
            writer.Write(Length);
        }

        public override PacketType PacketType {
            get { return PacketType.SendFile; }
        }

        public long Length { get; set; }
        public string Path { get; set; }

        public override string ToString () {
            return "(SendFile: " + Path + ")";
        }
    }

    public class SendLogEventPacket : ContentPacket {
        protected override void ReadBody (PacketReader reader) {
            var serializer = new System.Runtime.Serialization.DataContractSerializer(typeof(LoggingEventDto));

            var count = reader.ReadInt32();
            Events = new LoggingEventDto[count];

            for (var i = 0; i < count; i++) {
                var text = reader.ReadString();
                using (var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(text)))
                    Events[i] = (LoggingEventDto) serializer.ReadObject(memoryStream);
            }
        }

        protected override void WriteBody (PacketWriter writer) {
            var serializer = new System.Runtime.Serialization.DataContractSerializer(typeof(LoggingEventDto));

            writer.Write(Events.Length);
            foreach (var e in Events) {
                using (var memoryStream = new MemoryStream()) {
                    serializer.WriteObject(memoryStream, e);
                    var text = Encoding.UTF8.GetString(memoryStream.ToArray());
                    writer.Write(text);
                }
            }
        }

        public override PacketType PacketType {
            get { return PacketType.SendLogEvent; }
        }

        public LoggingEventDto[] Events { get; set; }

        public override string ToString () {
            return "(SendLogEvent)";
        }
    }

    public class SendReportPacket : ContentPacket {
        protected override void ReadBody (PacketReader reader) {
            FilePath = reader.ReadString();
            Report = reader.ReadString();
            Tag = reader.ReadString();
        }

        protected override void WriteBody (PacketWriter writer) {
            writer.Write(FilePath);
            writer.Write(Report);
            writer.Write(Tag);
        }

        public override PacketType PacketType {
            get { return PacketType.SendReport;  }
        }

        public string Report { get; set; }
        public string FilePath { get; set; }
        public string Tag { get; set; }

        public override string ToString () {
            return "(SendReport: " + FilePath + ")";
        }
    }
    #endregion

    public enum PacketType {
        Connect,
        Connected,
        Failure,
        Ready,
        RunScript,
        SendFile,
        FileReceived,
        SendLogEvent,
        SendReport,
    }
}
