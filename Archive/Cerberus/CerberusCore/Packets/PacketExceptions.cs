﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cerberus.Packets {
    /// <summary>
    /// The exception that is thrown when an error occurs while querying.
    /// </summary>
    [Serializable]
    public class ProtocolException : System.IO.IOException {
        /// <summary>
        /// Constructs a new ProtocolException.
        /// </summary>
        public ProtocolException () {
        }

        /// <summary>
        /// Constructs a new ProtocolException with the specified message and inner exception.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public ProtocolException (string message, Exception inner)
            : base(message, inner) {
        }

        /// <summary>
        /// Constructs a new ProtocolException with the specified message.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public ProtocolException (string message)
            : base(message) {
        }

        /// <summary>
        /// Constructs a new ProtocolException with the serialized data.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The serialization context.</param>
        protected ProtocolException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }
}
