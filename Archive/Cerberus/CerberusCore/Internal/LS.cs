﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

// todo: make internal, expose to friend assemblies

namespace Cerberus.Internal {
    public static class LS {
        /// <summary>
        /// Translates text into a localized form.  If a localized version of
        /// the text can't be found, the english text is returned.
        /// </summary>
        /// <param name="englishText">The text to translate.</param>
        /// <returns>A localized version of the english text, or the english text.</returns>
        public static string T (string englishText) {
            return englishText;
        }

        public static string T (string englishTextFormat, params object[] args) {
            return string.Format(CultureInfo.CurrentCulture, englishTextFormat, args);
        }
    }
}
