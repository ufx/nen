﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.IO;

using Cerberus.Configuration;

namespace Cerberus.Agent {
    public static class Config {
        private static AgentConfig _section;
        private static System.Configuration.Configuration _config;

        static Config () {
            var exe = System.Reflection.Assembly.GetEntryAssembly().GetName().Name + ".exe";
            _config = ConfigurationManager.OpenExeConfiguration(exe);
            _section = (AgentConfig) _config.GetSection("agent");
        }

        public static string Name {
            get { return _section.Name; }
        }

        public static Guid AuthenticationToken {
            get { return _section.Token; }
            set {
                _section.Token = value;
                _config.Save(ConfigurationSaveMode.Minimal, false);
            }
        }

        public static Uri Server {
            get { return _section.Server; }
        }

        public static string SevenZipPath {
            get { return _section.SevenZip.Path; }
        }

        public static string SqlConnectionString {
            get { return _section.Sql.ConnectionString; }
        }

        public static bool ValidateCertificates {
            get { return _section.ClientSsl.ValidateCertificates; }
        }
    }
}
