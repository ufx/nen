﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading;
using System.Reflection;

using Cerberus.Packets;

namespace Cerberus.Agent {
    public class Core {
        private NetworkClient _client;
        private bool _restart;

        public void Run () {
            Log.InitializeDefaultLogger();

            try {
                // Connect a client.
                try {
                    using (_client = new NetworkClient(Config.Name, Config.AuthenticationToken)) {
                        _client.NewAuthenticationTokenReceived += NewAuthenticationTokenReceived;
                        
                        Log.SetPersistentNetworkClient(_client);

                        _client.Core = this;

                        try {
                            _client.Hostname = Config.Server.Host;
                            _client.Port = Config.Server.Port;

                            _client.Run();
                        } catch (ThreadAbortException) {
                            throw;
                        } catch (Exception ex) {
                            Log.Default.Fatal("Error running Core listener", ex);
                            throw;
                        }
                    }
                } finally {
                    _client.NewAuthenticationTokenReceived -= NewAuthenticationTokenReceived;
                    _client = null;
                    Log.SetPersistentNetworkClient(null);
                }

                // Let the OS restart the service.
                if (_restart)
                    Environment.Exit(1);

            } catch (ThreadAbortException) {
                throw;
            } catch (Exception ex) {
                Log.Default.Fatal("Error running Core client", ex);
                throw;
            }
        }

        private void NewAuthenticationTokenReceived (object sender, AuthenticationTokenEventArgs e) {
            Config.AuthenticationToken = e.AuthenticationToken;
        }

        public bool IsStopping {
            get { return _restart; }
        }

        public void Restart () {
            _restart = true;

            // fixme: close client connection
        }
    }
}
