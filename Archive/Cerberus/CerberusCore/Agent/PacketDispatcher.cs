﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.IO;
using Nen.IO;
using Cerberus.Packets;
using Cerberus.Internal;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace Cerberus.Agent {
    public class PacketDispatcher {
        private NetworkClient _client;

        public PacketDispatcher (NetworkClient client) {
            _client = client;
        }

        public void Dispatch (Packet packet) {
            switch (packet.PacketType) {
                case PacketType.Ready:
                    // Send it back when we're ready to go.
                    _client.WritePacket(new ReadyPacket());
                    break;

                case PacketType.Failure:
                    Log.Default.Error(((FailurePacket) packet).Message);
                    break;

                case PacketType.RunScript:
                    RunScript((RunScriptPacket) packet, null);
                    break;

                case PacketType.SendFile:
                    SendFile((SendFilePacket) packet);
                    break;

                default:
                    throw new ArgumentException(LS.T("Unknown packet type {0}", packet.GetType()), "packet");
            }

            // Ask for more.
            _client.WritePacket(new ReadyPacket());
        }

        private void RunScript (RunScriptPacket run, string newPath) {
            using (var runspace = RunspaceFactory.CreateRunspace()) {
                runspace.Open();
                runspace.SessionStateProxy.SetVariable("Cerberus", _client.Core);

                using (var pipeline = runspace.CreatePipeline()) {
                    if (newPath != null) {
                        var command = new Command("Set-Location");
                        command.Parameters.Add("path", newPath);
                        pipeline.Commands.Add(command);
                    }

                    pipeline.Commands.AddScript(run.Script);
                    pipeline.Commands.Add("Out-String");

                    Collection<PSObject> results;

                    try {
                        results = pipeline.Invoke();

                        var report = new StringBuilder();
                        foreach (var obj in results)
                            report.AppendLine(obj.ToString());

                        var errorObjects = pipeline.Error.ReadToEnd();
                        if (errorObjects.Count > 0) {
                            if (report.Length > 0)
                                report.AppendLine();

                            foreach (var obj in errorObjects) {
                                if (obj is PSObject) {
                                    var psobj = (PSObject) obj;
                                    var error = psobj.BaseObject as ErrorRecord;
                                    if (error != null)
                                        report.AppendLine("ERROR: " + GetErrorReport(error));
                                }
                                report.AppendLine("ERROR: " + obj.ToString());
                            }
                        }

                        _client.Report(run.Name, report.ToString(), "Script");
                    } catch (ActionPreferenceStopException ex) {
                        if (ex.ErrorRecord == null)
                            _client.Report(run.Name, ex.GetType().Name + "\r\n" + ex.Message, "Script");
                        else
                            _client.Report(run.Name, GetErrorReport(ex.ErrorRecord), "Script");
                    } catch (RuntimeException ex) {
                        _client.Report(run.Name, ex.GetType().Name + "\r\n" + ex.Message, "Script");
                    }
                }

                runspace.Close();
            }
        }

        private string GetErrorReport (ErrorRecord error) {
            var report = new StringBuilder();
            if (error.CategoryInfo != null)
                report.AppendLine(error.CategoryInfo.ToString());
            if (error.InvocationInfo != null && !string.IsNullOrWhiteSpace(error.InvocationInfo.PositionMessage))
                report.AppendLine(error.InvocationInfo.PositionMessage);
            return report.ToString();
        }

        private void SendFile (SendFilePacket sendFile) {
            // SQL files are executed; no need to actually write a file.
            if (sendFile.Path.ToLower().EndsWith(".sql")) {
                RunSql(sendFile);
                return;
            }

            var fullWorkingPath = Path.Combine(_client.WorkingPath, sendFile.Path);
            var fileName = Path.GetFileName(fullWorkingPath);
            var containingDirectory = fullWorkingPath.Substring(0, fullWorkingPath.Length - fileName.Length);

            Directory.CreateDirectory(containingDirectory);

            using (var stream = File.OpenWrite(fullWorkingPath))
                WriteFileStreamTo(sendFile, stream);

            // If this is a package, execute it.
            if (fileName.EndsWith(".3"))
                RunPackage(fullWorkingPath, sendFile.Path);
            else
                _client.Report(sendFile.Path, "", "File");
        }

        private void WriteFileStreamTo (SendFilePacket sendFile, Stream stream) {
            var bytesRead = _client.PacketReader.BaseStream.BoundedCopyTo(stream, sendFile.Length);
            if (bytesRead != sendFile.Length)
                throw new ProtocolException(LS.T("Read {0} of {1} expected bytes for file {2}", bytesRead, sendFile.Length, sendFile.Path));
        }

        private void RunSql (SendFilePacket sendFile) {
            try {
                DataSet results;
                using (var stream = new MemoryStream((int) sendFile.Length)) {
                    WriteFileStreamTo(sendFile, stream);
                    var scriptText = Encoding.UTF8.GetString(stream.ToArray());

                    using (var sqlConnection = new SqlConnection(Config.SqlConnectionString)) {
                        var server = new Server(new ServerConnection(sqlConnection));
                        results = server.ConnectionContext.ExecuteWithResults(scriptText);
                    }
                }

                var report = new StringBuilder();
                for (int i = 0; i < results.Tables.Count; i++) {
                    if (i > 0)
                        report.AppendLine();

                    var table = results.Tables[i];
                    report.AppendLine("Results " + (i + 1));

                    var columnNames = table.Columns.Cast<DataColumn>().Select(c => c.ColumnName);
                    report.AppendLine(string.Join(",", columnNames.ToArray()));

                    foreach (DataRow row in table.Rows) {
                        var items = row.ItemArray.Select(o => Nen.IO.Csv.FormatValue(o));
                        report.AppendLine(string.Join(",", items.ToArray()));
                    }
                }

                _client.Report(sendFile.Path, report.ToString(), "CSV");
            } catch (ThreadAbortException) {
                throw;
            } catch (Exception ex) {
                _client.Report(sendFile.Path, ex.ToString(), "ERROR");
            }
        }

        private void RunPackage (string fullWorkingPath, string originalPath) {
            // Unzip the package.
            var fileName = Path.GetFileName(fullWorkingPath);
            var packageName = fileName.Substring(0, fileName.Length - 2);

            var unzipPackageInfo = new ProcessStartInfo(Config.SevenZipPath);
            unzipPackageInfo.CreateNoWindow = true;
            unzipPackageInfo.WorkingDirectory = Path.GetDirectoryName(Path.GetFullPath(fullWorkingPath));
            unzipPackageInfo.Arguments = "x -bd -y -o\"" + packageName + "\" \"" + fileName + "\"";
            unzipPackageInfo.RedirectStandardError = true;
            unzipPackageInfo.RedirectStandardInput = true;
            unzipPackageInfo.RedirectStandardOutput = true;
            unzipPackageInfo.UseShellExecute = false;
            var process = Process.Start(unzipPackageInfo);
            var stdout = process.StandardOutput.ReadToEnd();

            if (!stdout.Contains("Everything is Ok")) {
                _client.Report(originalPath, stdout, "Script");
                return;
            }

            // Locate the run script inside the package.
            var packagePath = Path.Combine(_client.WorkingPath, packageName);

            // Prefer Run.ps1, but use any script if that file can't be found.
            var runScriptPath = Directory.GetFiles(packagePath, "Run.ps1", SearchOption.AllDirectories).FirstOrDefault();
            if (runScriptPath == null)
                runScriptPath = Directory.GetFiles(packagePath, "*.ps1", SearchOption.AllDirectories).FirstOrDefault();

            if (runScriptPath == null)
                _client.Report(originalPath, "No ps1 script found in package.", "Script");
            else {
                // Load it up and execute it.
                var script = File.ReadAllText(runScriptPath);

                RunScript(new RunScriptPacket() { Name = originalPath, Script = script }, Path.GetDirectoryName(runScriptPath));
            }

            // Now cleanup package and directories.
            DirectoryEx.ForceDelete(packagePath);
            File.Delete(fullWorkingPath);
        }
    }
}
