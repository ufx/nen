﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Security;
using System.Net.Sockets;
using System.IO;

using Cerberus.Internal;
using Cerberus.Packets;
using Nen.Net.Sockets;

namespace Cerberus.Agent {
    public class NetworkClient : PersistentTcpClient {
        private const string _protocolVersion = "1.0.0.0";

        private bool _closed;
        private PacketReader _reader;
        private PacketWriter _writer;
        private object _clientLock = new object();
        private string _name;
        private Guid _authenticationToken;
        private PacketDispatcher _dispatcher;
        private Thread _keepAlive;

        #region Events
        public event EventHandler<AuthenticationTokenEventArgs> NewAuthenticationTokenReceived;

        protected virtual void OnNewAuthenticationTokenReceived (Guid authenticationToken) {
            var e = NewAuthenticationTokenReceived;
            if (e != null)
                e(this, new AuthenticationTokenEventArgs(authenticationToken));
        }
        #endregion

        #region Constructor / Disposer
        public NetworkClient (string name, Guid authenticationToken) {
            _name = name;
            _authenticationToken = authenticationToken;
            _dispatcher = new PacketDispatcher(this);
        }

        protected override void Dispose (bool disposing) {
            lock (_clientLock) {
                if (_closed)
                    return;

                _closed = true;

                base.Dispose(disposing);

                if (disposing) {
                    Core = null;

                    if (_reader != null) {
                        _reader.Close();
                        _reader = null;
                    }

                    if (_writer != null) {
                        _writer.Close();
                        _writer = null;
                    }
                }
            }
        }
        #endregion

        #region Connection Maintenance
        protected override void OnConnected () {
            var stream = new SslStream(UnderlyingClient.GetStream(), true, (sender, cert, chain, errors) => {
                return !Config.ValidateCertificates || errors == SslPolicyErrors.None;
            });
            stream.AuthenticateAsClient(Hostname);

            _reader = new PacketReader(stream);
            _writer = new PacketWriter(stream);

            if (!Handshake())
                throw new ProtocolException("Error in handshake");

            if (!IsTransient)
                WritePacket(new ReadyPacket());

            Log.Default.InfoFormat("Connected to {0}:{1}", Hostname, Port);
        }

        public void Run () {
            WorkingPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            Directory.CreateDirectory(WorkingPath);

            _keepAlive = new Thread(KeepAlive);
            _keepAlive.Name = "NetworkClient KeepAlive";
            _keepAlive.IsBackground = true;
            _keepAlive.Start();

            Forever(() => {
                try {
                    var packet = _reader.ReadPacket();
                    _dispatcher.Dispatch(packet);
                } catch (ProtocolException ex) {
                    // Disconnect the socket to clear any junk and reconnect.
                    UnderlyingClient.Close();
                    HandleRecoverableException(ex);
                }
            });
        }

        private void KeepAlive () {
            while (true) {
                try {
                    // Pulse the connection every 5 minutes.
                    Thread.Sleep(1000 * 60 * 5);
                    WritePacket(new ReadyPacket());
                } catch (IOException) {
                    // We've done our job.  Let the client reconnect.
                } catch (ThreadAbortException) {
                    throw;
                } catch (Exception ex) {
                    Log.Default.Error("Error writing KeepAlive packet.", ex);

                    // It may be unsafe.  Don't try to keep the connection alive anymore.
                    break;
                }
            }
        }

        protected override void OnDisconnected (Exception ex) {
            base.OnDisconnected(ex);

            if (SecondsToSleepAfterDisconnect <= 0)
                Log.Default.Warn("Connection error.  Attempting to reconnect.", ex);
            else
                Log.Default.Warn(string.Format("Connection error.  Sleeping for {0} seconds.", SecondsToSleepAfterDisconnect), ex);
        }
        #endregion

        #region Dialogs
        private bool Handshake () {
            var request = new ConnectPacket();
            request.MachineName = Environment.MachineName;
            request.ClientName = _name;
            request.Version = _protocolVersion;
            request.IsTransient = IsTransient;
            request.AuthenticationToken = _authenticationToken;
            WritePacket(request);

            var response = _reader.ReadPacket();
            if (response is FailurePacket) {
                Log.Default.FatalFormat("Error in handshake: {0}", ((FailurePacket) response).Message);
                return false;
            }

            if (!(response is ConnectedPacket)) {
                Log.Default.FatalFormat("Unexpected packet in handshake: {0}", response.GetType());
                return false;
            }

            var connected = (ConnectedPacket) response;

            if (connected.NewAuthenticationToken != Guid.Empty)
                OnNewAuthenticationTokenReceived(connected.NewAuthenticationToken);

            return true;
        }

        public void Report (string filePath, string results, string tag) {
            var report = new SendReportPacket();
            report.FilePath = filePath;
            report.Report = results;
            report.Tag = tag;
            WritePacket(report);
        }
        #endregion

        #region Packets
        public void WritePacket (Packet packet) {
            lock (_clientLock) {
                _writer.Write(packet);
                _writer.Flush();
            }
        }
        #endregion

        #region Accessors
        public Core Core { get; internal set; }
        internal PacketReader PacketReader { get { return _reader; } }
        public bool IsTransient { get; set; }
        public string WorkingPath { get; set; }
        #endregion
    }

    public class AuthenticationTokenEventArgs : EventArgs {
        public AuthenticationTokenEventArgs (Guid authenticationToken) {
            AuthenticationToken = authenticationToken;
        }

        public Guid AuthenticationToken { get; private set; }
    }
}
