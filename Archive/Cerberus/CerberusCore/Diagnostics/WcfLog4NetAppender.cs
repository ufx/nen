﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

using log4net.Appender;
using log4net.Core;

using Nen.ServiceModel;

namespace Cerberus.Diagnostics {
    public class WcfLog4NetAppender : AppenderSkeleton {
        #region Appending
        protected override void Append (LoggingEvent loggingEvent) {
            var data = new LoggingEventDto(loggingEvent.GetLoggingEventData());

            var client = new WcfLog4NetAppenderClient(Uri);
            client.Do(() => client.Append(data));
        }
        #endregion

        #region Accessors
        public string Uri { get; set; }
        #endregion
    }

    public class WcfLog4NetAppenderClient : WcfClient<ILogAppender>, ILogAppender {
        #region Constructors
        public WcfLog4NetAppenderClient (string uri)
            : base(uri) {
        }
        #endregion

        public void Append (LoggingEventDto dto) {
            base.Channel.Append(dto);
        }
    }

    [ServiceContract]
    public interface ILogAppender {
        [OperationContract]
        void Append (LoggingEventDto dto);
    }
}
