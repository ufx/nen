﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using log4net.Core;
using log4net.Util;

namespace Cerberus.Diagnostics {
    [DataContract]
    public class LoggingEventDto {
        #region Constructors
        public LoggingEventDto () {
        }

        public LoggingEventDto (LoggingEventData data) {
            Domain = data.Domain;
            ExceptionString = data.ExceptionString;
            Identity = data.Identity;
            Level = new LevelDto(data.Level);
            LoggerName = data.LoggerName;
            Message = data.Message;
            ThreadName = data.ThreadName;
            TimeStamp = data.TimeStamp;
            UserName = data.UserName;

            if (data.LocationInfo != null)
                LocationInfo = new LocationInfoDto(data.LocationInfo);

            if (data.Properties != null) {
                Properties = new Dictionary<string, string>();
                foreach (System.Collections.DictionaryEntry entry in data.Properties)
                    Properties[entry.Key.ToString()] = entry.Value.ToString();
            }
        }
        #endregion

        #region Conversion
        public LoggingEventData ToLoggingEventData () {
            var data = new LoggingEventData();
            data.Domain = Domain;
            data.ExceptionString = ExceptionString;
            data.Identity = Identity;
            data.Level = Level.ToLevel();
            data.LoggerName = LoggerName;
            data.Message = Message;
            data.ThreadName = ThreadName;
            data.TimeStamp = TimeStamp;
            data.UserName = UserName;

            if (LocationInfo != null)
                data.LocationInfo = LocationInfo.ToLocationInfo();

            if (Properties != null) {
                data.Properties = new PropertiesDictionary();
                foreach (var item in Properties)
                    data.Properties[item.Key] = item.Value;
            }

            return data;
        }
        #endregion

        #region Accessors
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public string ExceptionString { get; set; }
        [DataMember]
        public string Identity { get; set; }
        [DataMember]
        public LevelDto Level { get; set; }
        [DataMember]
        public LocationInfoDto LocationInfo { get; set; }
        [DataMember]
        public string LoggerName { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public Dictionary<string, string> Properties { get; set; }
        [DataMember]
        public string ThreadName { get; set; }
        [DataMember]
        public DateTime TimeStamp { get; set; }
        [DataMember]
        public string UserName { get; set; }
        #endregion
    }

    [DataContract]
    public class LocationInfoDto {
        #region Constructors
        public LocationInfoDto () {
        }

        public LocationInfoDto (LocationInfo info) {
            ClassName = info.ClassName;
            FileName = info.FileName;
            LineNumber = info.LineNumber;
            MethodName = info.MethodName;
        }
        #endregion

        #region Conversion
        public LocationInfo ToLocationInfo () {
            return new LocationInfo(ClassName, MethodName, FileName, LineNumber);
        }
        #endregion

        #region Accessors
        [DataMember]
        public string ClassName { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string LineNumber { get; set; }
        [DataMember]
        public string MethodName { get; set; }
        #endregion
    }

    [DataContract]
    public class LevelDto {
        #region Constructors
        public LevelDto () {
        }

        public LevelDto (Level level) {
            DisplayName = level.DisplayName;
            Name = level.Name;
            Value = level.Value;
        }
        #endregion

        #region Conversion
        public Level ToLevel () {
            return new Level(Value, Name, DisplayName);
        }
        #endregion

        #region Accessors
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Value { get; set; }
        #endregion
    }
}
