﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using log4net.Appender;
using log4net.Core;

using Cerberus.Agent;
using Cerberus.Packets;

namespace Cerberus.Diagnostics {
    public class NetworkAppender : AppenderSkeleton {
        #region Appending
        protected override void Append (LoggingEvent loggingEvent) {
            Append(new LoggingEvent[] { loggingEvent });
        }

        protected override void Append (LoggingEvent[] loggingEvents) {
            var packet = new SendLogEventPacket();
            packet.Events = loggingEvents.Select(e => new LoggingEventDto(e.GetLoggingEventData())).ToArray();

            try {
                if (PersistentClient == null) {
                    if (Uri != null) {
                        using (var client = new NetworkClient(ClientName, AuthenticationToken)) {
                            client.IsTransient = true;
                            client.Connect(Uri.Host, Uri.Port);
                            client.WritePacket(packet);
                        }
                    }
                } else if (PersistentClient.UnderlyingClient != null && PersistentClient.UnderlyingClient.Connected)
                    PersistentClient.WritePacket(packet);
            } catch (IOException ex) {
                ErrorHandler.Error("IO error communicating with Cerberus server", ex, ErrorCode.WriteFailure);
            }
        }
        #endregion

        #region Accessors
        public string ClientName { get; set; }
        public Guid AuthenticationToken { get; set; }
        public Uri Uri { get; set; }
        public NetworkClient PersistentClient { get; set; }
        #endregion
    }
}
