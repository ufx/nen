CREATE DATABASE Cerberus
GO

USE Cerberus
GO

CREATE TABLE Tag (
	Id INT NOT NULL IDENTITY PRIMARY KEY,
	Name NVARCHAR(500) NOT NULL
)

CREATE TABLE LogMessage (
	Id INT NOT NULL IDENTITY PRIMARY KEY,
	Hash CHAR(40) NOT NULL UNIQUE,
	[Text] NTEXT NOT NULL
)

CREATE TABLE LogEntry (
	Id INT NOT NULL IDENTITY PRIMARY KEY,
	ClientName NVARCHAR(200) NOT NULL,
	MessageId INT REFERENCES LogMessage(Id),
	ExceptionMessageId INT REFERENCES LogMessage(Id),
	[TimeStamp] DATETIME NOT NULL DEFAULT(GETDATE())
)

CREATE TABLE LogTag (
	Id INT NOT NULL IDENTITY PRIMARY KEY,
	TagId INT NOT NULL REFERENCES Tag(Id),
	LogEntryId INT NOT NULL REFERENCES LogEntry(Id)
)

CREATE TABLE LogSource (
	Id INT NOT NULL IDENTITY PRIMARY KEY,
	LogEntryId INT NOT NULL REFERENCES LogEntry(Id),
	Domain NVARCHAR(500),
	ThreadIdentity NVARCHAR(500),
	ThreadName NVARCHAR(500),
	UserName NVARCHAR(500),
	ClassName NVARCHAR(500),
	FileName NVARCHAR(500),
	LineNumber NVARCHAR(100),
	MethodName NVARCHAR(500)
)

CREATE TABLE LogProperty (
	Id INT NOT NULL IDENTITY PRIMARY KEY,
	LogEntryId INT NOT NULL REFERENCES LogEntry(Id),
	Name NVARCHAR(500) NOT NULL,
	Value NVARCHAR(500)
)
GO

CREATE FUNCTION GetTags (@LogEntryId INT)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @Tags NVARCHAR(MAX)

	SELECT	@Tags = COALESCE(@Tags + ', ' + Tag.Name, Tag.Name)
	FROM	LogTag
	JOIN	Tag ON Tag.Id = LogTag.TagId
	WHERE	LogTag.LogEntryId = @LogEntryId

	RETURN @Tags
END
GO

CREATE VIEW LogData
AS

SELECT		LogEntry.Id, LogEntry.ClientName, LogEntry.TimeStamp, dbo.GetTags(LogEntry.Id) AS Tags,
			Message.Text AS MessageText, Exception.Text AS ExceptionText,
			LogSource.Domain, LogSource.ThreadIdentity, LogSource.ThreadName,
			LogSource.ClassName, LogSource.FileName, LogSource.LineNumber, LogSource.MethodName
FROM		LogEntry
LEFT JOIN	LogMessage Message ON Message.Id = LogEntry.MessageId
LEFT JOIN	LogMessage Exception ON Exception.Id = LogEntry.ExceptionMessageId
LEFT JOIN	LogSource ON LogSource.LogEntryId = LogEntry.Id
GO