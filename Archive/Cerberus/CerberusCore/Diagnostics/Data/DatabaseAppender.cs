﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using log4net.Appender;
using log4net.Core;

using Nen.Data.SqlClient;
using Nen.Data.ObjectRelationalMapper;

namespace Cerberus.Diagnostics.Data {
    public class DatabaseAppender : AppenderSkeleton {
        #region Appending
        protected override void Append (LoggingEvent loggingEvent) {
            Append(new LoggingEvent[] { loggingEvent });
        }

        protected override void Append (LoggingEvent[] loggingEvents) {
            foreach (var log in loggingEvents)
                Write(log);
        }

        private void Write (LoggingEvent log) {
            using (var context = DataEnvironment.CreateContext()) {
                var entry = LogEntry.FromLog(log);
                context.Save(entry);
            }
        }
        #endregion
    }
}
