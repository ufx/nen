﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

using log4net.Core;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Diagnostics.Data {
    [Persistent]
    public class LogEntry {
        #region Constructors
        public LogEntry () {
            LogProperties = new Collection<LogProperty>();
            LogTags = new Collection<LogTag>();
        }

        public static LogEntry FromLog (LoggingEvent log) {
            var entry = new LogEntry();

            // Basic information.
            entry.Message = LogMessage.GetOrCreate(log.RenderedMessage);
            entry.ExceptionMessage = LogMessage.GetOrCreate(log.GetExceptionString());
            entry.TimeStamp = log.TimeStamp;

            // Tags
            if (!string.IsNullOrWhiteSpace(log.Level.Name))
                entry.LogTags.Add(new LogTag(Tag.GetOrCreate(log.Level.Name), entry));

            if (!string.IsNullOrWhiteSpace(log.LoggerName))
                entry.LogTags.Add(new LogTag(Tag.GetOrCreate(log.LoggerName), entry));

            // Source.
            entry.LogSource = new LogSource();
            entry.LogSource.LogEntry = entry;
            entry.LogSource.ThreadIdentity = log.Identity;
            entry.LogSource.ThreadName = log.ThreadName;
            entry.LogSource.Domain = log.Domain;
            entry.LogSource.UserName = log.UserName;

            // Obtaining LocationInformation is causing lockups.  Ignore it.

            //if (log.LocationInformation != null) {
            //    LogSource.ClassName = log.LocationInformation.ClassName;
            //    LogSource.FileName = log.LocationInformation.FileName;
            //    LogSource.LineNumber = log.LocationInformation.LineNumber;
            //    LogSource.MethodName = log.LocationInformation.MethodName;
            //}

            // Special properties.
            if (log.Properties.Contains("__CerberusClient"))
                entry.ClientName = (string) log.Properties["__CerberusClient"];
            else
                entry.ClientName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;

            // Other properties.
            var userKeys = log.Properties.GetKeys().Where(k => !k.StartsWith("__Cerberus") && !k.StartsWith("log4net:"));
            foreach (var key in userKeys) {
                var property = new LogProperty();
                property.LogEntry = entry;
                property.Name = key;

                var value = log.Properties[key];
                if (value != null)
                    property.Value = value.ToString();

                entry.LogProperties.Add(property);
            }

            return entry;
        }
        #endregion

        #region Accessors
        public int Id { get; set; }
        public string ClientName { get; set; }
        public LogMessage Message { get; set; }
        public LogMessage ExceptionMessage { get; set; }
        public DateTime TimeStamp { get; set; }
        [Subordinate]
        public LogSource LogSource { get; set; }
        public Collection<LogProperty> LogProperties { get; private set; }
        [Include]
        public Collection<LogTag> LogTags { get; private set; }
        #endregion
    }

    [Persistent]
    public class LogSource {
        public int Id { get; set; }
        [Required]
        public LogEntry LogEntry { get; set; }
        public string Domain { get; set; }
        public string ThreadIdentity { get; set; }
        public string ThreadName { get; set; }
        public string UserName { get; set; }
        public string ClassName { get; set; }
        public string FileName { get; set; }
        public string LineNumber { get; set; }
        public string MethodName { get; set; }
    }

    [Persistent]
    public class LogProperty {
        public int Id { get; set; }
        [Required]
        public LogEntry LogEntry { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    [Persistent]
    public class LogMessage {
        public static LogMessage GetOrCreate (string text) {
            if (string.IsNullOrWhiteSpace(text))
                return null;

            using (var context = DataEnvironment.CreateContext()) {
                var sha1 = System.Security.Cryptography.SHA1.Create();
                var hash = BitConverter.ToString(sha1.ComputeHash(Encoding.UTF8.GetBytes(text))).Replace("-", "");

                var message = context.Get<LogMessage>().FirstOrDefault(m => m.Hash == hash);
                if (message == null) {
                    message = new LogMessage();
                    message.Text = text;
                    message.Hash = hash;
                }
                return message;
            }
        }

        public int Id { get; set; }
        public string Hash { get; set; }
        public string Text { get; set; }
    }

    [Persistent]
    public class Tag {
        public static Tag GetOrCreate (string name) {
            if (string.IsNullOrWhiteSpace(name))
                return null;

            using (var context = DataEnvironment.CreateContext()) {
                var tag = context.Get<Tag>().FirstOrDefault(n => n.Name == name);
                if (tag == null) {
                    tag = new Tag();
                    tag.Name = name;
                }
                return tag;
            }
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString () {
            return Name;
        }
    }

    [Persistent]
    public class LogTag {
        public LogTag () { }

        public LogTag (Tag tag, LogEntry entry) {
            Tag = tag;
            LogEntry = entry;
        }

        public int Id { get; set; }
        [Required]
        public Tag Tag { get; set; }
        [Required]
        public LogEntry LogEntry { get; set; }

        public override string ToString () {
            if (Tag == null)
                return "null";

            return Tag.Name;
        }
    }
}
