﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Data.SqlClient;
using Nen.Data.ObjectRelationalMapper;

namespace Cerberus.Diagnostics.Data {
    public static class DataEnvironment {
        private static MapConfiguration _configuration;

        public static DataContext CreateContext () {
            if (_configuration == null) {
                _configuration = new MapConfiguration();
                _configuration.DefaultStore = new DatabaseStore(_configuration, Database);
                _configuration.Initialize();
            }

            return new DataContext(_configuration, ContextOptions.TrackChanges);
        }

        public static SqlDatabase Database { get; set; }
    }
}
