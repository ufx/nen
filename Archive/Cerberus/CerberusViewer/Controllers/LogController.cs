﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Cerberus.Diagnostics.Data;

namespace CerberusViewer.Controllers {
    [HandleError]
    public class LogController : Controller {
        private const int _pageSize = 200;

        public ActionResult Index () {
            return List(null, null);
        }

        public ActionResult List (string name, int? page) {
            using (var context = DataEnvironment.CreateContext()) {
                var fromTime = DateTime.Now.Date.AddDays(-1);
                var toTime = DateTime.Now.Date.AddDays(1);

                IEnumerable<LogEntry> query = from c in context.Get<LogEntry>()
                            where c.TimeStamp >= fromTime && c.TimeStamp <= toTime
                            orderby c.TimeStamp descending
                            select c;

                // Handle paging
                if (page != null && page.Value > 0)
                    query = query.Skip(_pageSize * page.Value);
                query = query.Take(_pageSize);

                return View(query.ToArray());
            }
        }
    }
}
