﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Cerberus.Diagnostics.Data.LogEntry>>" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Log Viewer
</asp:Content>

<asp:Content ID="indexHead" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".LogDetail").hide();
            //$("#LogEntries").tablesorter();
        });

        function toggleDetail(header, detailRow) {
            if (detailRow.hasClass('LogEnabled')) {
                detailRow.removeClass('LogEnabled');
                detailRow.hide();
            } else {
                var content = $('#' + header.id + ' .LogExcerpt').text();
                detailRow.find('textarea').text(content);

                detailRow.addClass('LogEnabled');
                detailRow.show();
            }
        }
    </script>
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <table id="LogEntries">
        <thead>
            <tr>
                <th style="width: 6em" />
                <th style="width: 10em" />
                <th style="width: 5em" />
                <th style="overflow: hidden" />
            </tr>
        </thead>
        <tbody>
            <%
                var currentDate = DateTime.MinValue;
                foreach (var entry in Model) {
                    if (entry.TimeStamp.Date != currentDate.Date) {
                        currentDate = entry.TimeStamp.Date;
                        %><tr class="logGroupHeader"><td colspan="4"><%= Html.Encode(currentDate.Date.ToShortDateString()) %></td></tr><%
                    }
                     %>
                <tr id="LogHeader<%= entry.Id %>" onclick="toggleDetail(this, $('#LogDetail<%= entry.Id %>'));return 1;" class="LogHeader">
                    <td><%= Html.Encode(entry.TimeStamp.ToShortTimeString()) %></td>
                    <td><%= Html.Encode(entry.ClientName) %></td>
                    <td><%= Html.Encode(entry.Level) %></td>
                    <td><div class="LogExcerpt"><%
                            var str = ((entry.Message == null ? "" : entry.Message.Text) + "\r\n" + (entry.ExceptionMessage == null ? "" : entry.ExceptionMessage.Text)).Trim();
                            Response.Write(Html.Encode(str));
                        %></div></td>
                </tr>

                <tr id="LogDetail<%= entry.Id %>" class="LogDetail">
                    <td colspan="4">
                        <div><textarea rows="10"></textarea></div>
                    </td>
                </tr>
            <% } %>
        </tbody>
    </table>
</asp:Content>