﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using Nen.Data.ObjectRelationalMapper;

namespace CerberusViewer {
    public static class Config {
        public static string ConnectionString {
            get { return ConfigurationManager.AppSettings["ConnectionString"]; }
        }
    }
}