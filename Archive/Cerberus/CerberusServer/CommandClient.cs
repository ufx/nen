﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net.Security;
using System.IO;

using Cerberus.Packets;

namespace Cerberus.Server {
    public class CommandClient : IDisposable {
        private bool _closed;
        private TcpClient _client;
        private PacketReader _reader;
        private PacketWriter _writer;
        private object _clientLock = new object();

        #region Constructor / Disposer
        public CommandClient (TcpClient client) {
            _client = client;

            var stream = new SslStream(client.GetStream());
            stream.AuthenticateAsServer(Config.GetSslCertificate());
            _reader = new PacketReader(stream);
            _writer = new PacketWriter(stream);
        }

        public void Dispose () {
            Dispose(true);
        }

        protected virtual void Dispose (bool disposing) {
            lock (_clientLock) {
                if (_closed)
                    return;

                _closed = true;

                if (disposing) {
                    _client = null;

                    if (_reader != null) {
                        _reader.Close();
                        _reader = null;
                    }

                    if (_writer != null) {
                        _writer.Close();
                        _writer = null;
                    }
                }
            }
        }
        #endregion

        #region Dialog
        internal bool Handshake () {
            var packet = ReadPacket();
            if (!(packet is ConnectPacket)) {
                WriteFailure(packet, "Expected ConnectPacket");
                return false;
            }

            var request = (ConnectPacket) packet;
            Name = request.ClientName;
            Machine = request.MachineName;
            IsTransient = request.IsTransient;

            var response = new ConnectedPacket();

            var token = Config.GetAgentToken(Name);
            if (Directory.Exists(ClientPath)) {
                // Verify the client token.
                if (token != request.AuthenticationToken) {
                    WriteFailure(packet, "Invalid authentication token");
                    return false;
                }
            } else {
                // Create a new token.
                Directory.CreateDirectory(ClientPath);
                response.NewAuthenticationToken = token;
            }

            WritePacket(response);
            return true;
        }
        #endregion

        #region Packet Writing
        public void WriteFailure (Packet lastPacket, string message) {
            FailurePacket failure;
            if (lastPacket is ContentPacket)
                failure = ((ContentPacket) lastPacket).Fail(message);
            else
                failure = new FailurePacket() { Message = message };

            WritePacket(failure);
        }

        public void WritePacket (Packet packet) {
            lock (_clientLock) {
                try {
                    IsBusy = true;

                    _writer.Write(packet);
                    _writer.Flush();
                } finally {
                    IsBusy = false;
                }
            }
        }

        public void WritePacketOperation (Packet packet, Action<PacketWriter> operation) {
            lock (_clientLock) {
                try {
                    IsBusy = true;

                    _writer.Write(packet);
                    operation(_writer);
                    _writer.Flush();
                } finally {
                    IsBusy = false;
                }
            }
        }

        public Packet ReadPacket () {
            return _reader.ReadPacket();
        }
        #endregion

        #region Accessors
        public string ClientPath {
            get { return Config.ClientDirectory + Path.DirectorySeparatorChar + Name; }
        }

        public string Name { get; private set; }
        public string Machine { get; private set; }
        public bool IsTransient { get; private set; }
        public bool IsBusy { get; private set; }

        public TcpClient BaseClient { get { return _client; } }
        #endregion
    }
}
