﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;

using Cerberus.Internal;
using Cerberus.Configuration;

namespace Cerberus.Server {
    public static class Config {
        private static ServerConfig _section;
        private static System.Configuration.Configuration _config;
        private static object _configLock = new object();

        static Config () {
            var exe = System.Reflection.Assembly.GetEntryAssembly().GetName().Name + ".exe";
            _config = ConfigurationManager.OpenExeConfiguration(exe);
            _section = (ServerConfig) _config.GetSection("server");
        }

        public static string ClientDirectory {
            get { return _section.ClientDirectory; }
        }

        public static int Port {
            get { return _section.Port; }
        }

        public static string LogConnectionString {
            get { return _section.Log.ConnectionString; }
        }

        private static X509Certificate2 _sslCertificate;
        public static X509Certificate2 GetSslCertificate () {
            if (_sslCertificate != null)
                return _sslCertificate;

            var sslConfig = _section.Ssl;

            var store = new X509Store(sslConfig.StoreName, sslConfig.StoreLocation);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            try {
                var findValue = sslConfig.Thumbprint.Replace(" ", "");

                // OK, Certificates.Find simply would not under any circumstances work.
                // No idea why.  But this works.
                _sslCertificate = store.Certificates.OfType<X509Certificate2>().FirstOrDefault(c => string.Compare(c.Thumbprint, findValue, true) == 0);
                if (_sslCertificate == null)
                    throw new ConfigurationErrorsException(LS.T("Unable to find SSL certificate with value {0}", findValue));
                return _sslCertificate;
            } finally {
                store.Close();
            }
        }

        public static Guid GetAgentToken (string name) {
            lock (_configLock) {
                var agent = _section.Agents.Cast<AgentTokenConfig>().FirstOrDefault(a => string.Compare(a.Name, name, true) == 0);
                if (agent == null) {
                    agent = new AgentTokenConfig();
                    agent.Name = name;
                    agent.Token = Guid.NewGuid();
                    _section.Agents.Add(agent);
                    _config.Save(ConfigurationSaveMode.Minimal, false);
                }
                return agent.Token;
            }
        }
    }
}
