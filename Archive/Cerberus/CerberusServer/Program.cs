﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Cerberus.Server {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main (string[] args) {
            System.Threading.Thread.CurrentThread.Name = "Main";
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            Log.InitializeDefaultLogger();
            Cerberus.Diagnostics.Data.DataEnvironment.Database = new Nen.Data.SqlClient.SqlDatabase(Config.LogConnectionString);            

            try {
                if (args.Length > 0 && string.Compare(args[0], "--console", true) == 0) {
                    var service = new ServerService();
                    service.Run();
                    Console.WriteLine("Cerberus server running in console mode.  Please enter to stop.");
                    Console.ReadLine();
                    service.Stop();
                } else
                    ServiceBase.Run(new ServiceBase[] { new ServerService() });
            } catch (System.Threading.ThreadAbortException) {
                throw;
            } catch (Exception ex) {
                Log.Default.Fatal("Fatal error in Main", ex);
            }
        }
    }
}
