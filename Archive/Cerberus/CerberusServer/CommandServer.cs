﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.IO;

using Nen.Net.Sockets;

using Cerberus.Packets;

namespace Cerberus.Server {
    public class CommandServer : AsynchronousTcpServer {
        private Dictionary<string, CommandClient> _clients = new Dictionary<string, CommandClient>();
        private object _clientsLock = new object();

        public CommandServer () {
            ConnectionMode = TcpServerConnectionMode.ThreadPerConnection;
        }

        protected override void ServiceClient (TcpClient tcpClient) {
            System.Threading.Thread.CurrentThread.Name = "CommandClient";

            var clientEndpoint = tcpClient.Client.RemoteEndPoint.ToString();
            Log.Default.InfoFormat("Client {0} connected", clientEndpoint);
            string clientName = null;
            bool clientIsTransient = false;

            using (var client = new CommandClient(tcpClient)) {
                var dispatcher = new PacketDispatcher(client);
                Packet packet = null;
                try {
                    if (!client.Handshake()) {
                        Log.Default.WarnFormat("Client {0} failed handshake", clientEndpoint);
                        return;
                    }

                    clientName = client.Name;
                    clientIsTransient = client.IsTransient;

                    if (!clientIsTransient) {
                        lock (_clientsLock)
                            _clients[clientName] = client;
                    }

                    while (tcpClient.Connected) {
                        packet = client.ReadPacket();
                        if (packet == null)
                            break;

                        dispatcher.Dispatch(packet);
                    }
                } catch (ThreadAbortException) {
                    throw;
                } catch (IOException ex) {
                    if (!IsExceptionIgnored(ex))
                        Log.Default.Warn("Network error", ex);
                } catch (Exception ex) {
                    Log.Default.Error("Client error", ex);

                    if (client.BaseClient.Connected)
                        client.WriteFailure(packet, ex.Message);
                } finally {
                    if (clientName != null && !clientIsTransient) {
                        lock (_clientsLock)
                            _clients.Remove(clientName);
                    }
                }
            }

            Log.Default.InfoFormat("Client {0} disconnected", clientEndpoint);
        }

        public CommandClient GetClient (string name) {
            lock (_clientsLock)
                return _clients.ContainsKey(name) ? _clients[name] : null; 
        }

        public CommandClient[] GetClients () {
            lock (_clientsLock)
                return _clients.Values.ToArray();
        }

        #region Exceptions
        private bool IsExceptionIgnored (IOException ex) {
            if (ex is EndOfStreamException)
                return true;

            var socketException = ex.InnerException as SocketException;
            if (socketException == null)
                return false;

            switch (socketException.ErrorCode) {
                case 10004: // WSACancelBlockingCall
                case 10054: // WSAECONNRESET
                case 10060: // WSAETIMEDOUT
                    return true;

                default:
                    Log.Default.WarnFormat("Unknown socket ErrorCode {0}", socketException.ErrorCode);
                    break;
            }

            return false;
        }
        #endregion
    }
}
