﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using Cerberus.Internal;
using Cerberus.Packets;
using Cerberus.Diagnostics.Data;
using Nen;
using Nen.Caching;
using Nen.Collections;
using Nen.IO;

namespace Cerberus.Server {
    public class PacketDispatcher {
        private CommandClient _client;
        private WebCache _sentFiles = new WebCache(System.Web.HttpRuntime.Cache);

        public PacketDispatcher (CommandClient client) {
            _client = client;
        }

        public void Dispatch (Packet packet) {
            Log.Default.DebugFormat("Dispatching packet {0}", packet);

            switch (packet.PacketType) {
                case PacketType.Failure:
                    Log.Default.Error(((FailurePacket) packet).Message);
                    break;

                case PacketType.Ready:
                    GetScript();
                    break;

                case PacketType.SendReport:
                    WriteReport((SendReportPacket) packet);
                    break;
                    
                case PacketType.SendLogEvent:
                    SaveLogEvent((SendLogEventPacket) packet);
                    break;

                default:
                    Log.Default.ErrorFormat("Unknown packet type {0}", packet.GetType());
                    break;
            }
        }

        #region Operations
        private void SaveLogEvent (SendLogEventPacket packet) {
            if (packet.Events == null)
                return;

            ThreadPool.QueueUserWorkItem(new WaitCallback((o) => {
                try {
                    foreach (var log in packet.Events) {
                        var loggingEvent = new log4net.Core.LoggingEvent(log.ToLoggingEventData());
                        loggingEvent.Properties["__CerberusClient"] = _client.Name;
                        Log.Default.Logger.Log(loggingEvent);
                    }
                } catch (ThreadAbortException) {
                    throw;
                } catch (Exception ex) {
                    Log.Default.Error("Error logging an event", ex);
                }
            }));
        }

        private void WriteReport (SendReportPacket report) {
            // Delete the script when the report rolls in.
            DeleteFile(report.FilePath);

            if (string.IsNullOrWhiteSpace(report.Report))
                return;

            // Write the report to the database.
            using (var context = Cerberus.Diagnostics.Data.DataEnvironment.CreateContext()) {
                var entry = new LogEntry();
                entry.Message = LogMessage.GetOrCreate(report.Report);
                entry.ClientName = _client.Name;
                entry.LogTags.Add(new LogTag(Tag.GetOrCreate("INFO"), entry));
                entry.LogTags.Add(new LogTag(Tag.GetOrCreate(report.Tag), entry));
                entry.TimeStamp = DateTime.Now;

                context.Save(entry);
            }
        }

        private void GetScript () {
            var uploadDirectory = new DirectoryInfo(_client.ClientPath);

            var allFiles = uploadDirectory.GetFiles("*.*", SearchOption.AllDirectories);
            // Filter files we've already sent.
            var files = allFiles.Where(f => _sentFiles[f.FullName] == null).OrderBy(f => f.FullName).ToArray();
            if (files.Length == 0)
                return;

            // Find an accessible non-script file first.
            foreach (var file in files.Where(f => f.Extension != ".ps1")) {
                try {
                    using (var stream = file.OpenRead()) {
                        SendFile(file, stream);
                        return;
                    }
                } catch (IOException) {
                    // On to the next.
                } catch (UnauthorizedAccessException) {
                    // On to the next.
                }
            }

            // Nothing left to send.  Try a script.
            foreach (var file in files.Where(f => f.Extension == ".ps1")) {
                try {
                    using (var stream = file.OpenRead()) {
                        RunScript(file, stream);
                        return;
                    }
                } catch (IOException) {
                    // On to the next.
                }
            }
        }

        private void SendFile (FileInfo file, FileStream fileStream) {
            var sendFile = new SendFilePacket();
            var uploadDirectory = new DirectoryInfo(_client.ClientPath);
            sendFile.Path = file.FullName.AfterFirst(uploadDirectory.FullName + "\\");
            sendFile.Length = file.Length;

            _client.WritePacketOperation(sendFile, w => fileStream.CopyTo(w.BaseStream));

            // Wait 5 minutes for an acknowledgment before sending this file again.
            _sentFiles.Insert(file.FullName, true, CacheOptions.Absolute(DateTime.Now.AddMinutes(5)));
        }

        private void RunScript (FileInfo script, FileStream fileStream) {
            var run = new RunScriptPacket();
            run.Name = script.Name;

            using (var reader = new StreamReader(fileStream))
                run.Script = reader.ReadToEnd();

            _client.WritePacket(run);

            // Wait 5 minutes for an acknowledgment before sending this script again.
            _sentFiles.Insert(script.FullName, true, CacheOptions.Absolute(DateTime.Now.AddMinutes(5)));
        }
        #endregion

        #region File Maintenance
        private void DeleteFile (string path) {
            if (path.Contains("..") || path.Contains(":\\")) {
                Log.Default.ErrorFormat("Refusing to delete dangerous path {0}", path);
                return;
            }

            var file = new FileInfo(_client.ClientPath + "\\" + path);
            if (file.Exists) {
                _sentFiles.Remove(file.FullName);
                FileEx.ForceDelete(file.FullName);
            }
        }
        #endregion
    }
}
