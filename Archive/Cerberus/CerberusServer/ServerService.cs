﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Net;
using System.IO;

using Nen.IO;
using Nen.Net.Sockets;
using Cerberus.Packets;

namespace Cerberus.Server {
    public partial class ServerService : ServiceBase {
        private Thread _directoryWatcher;
        private Thread _listener;
        private CommandServer _server;

        public ServerService () {
            InitializeComponent();
        }

        protected override void OnStart (string[] args) {
            Run();
        }

        public void Run () {
            _server = new CommandServer();

            Directory.CreateDirectory(Config.ClientDirectory);

            // Start the server.
            _listener = new Thread(new ThreadStart(() => {
                try {
                    _server.Start(IPAddress.Any, Config.Port);
                } catch (ThreadAbortException) {
                    throw;
                } catch (Exception ex) {
                    Log.Default.Fatal("Error running command server", ex);
                }
            }));
            _listener.IsBackground = true;
            _listener.Name = "CommandServer Listener";
            _listener.Start();

            // Start a directory watcher.
            _directoryWatcher = new Thread(WatchForUploads);
            _directoryWatcher.IsBackground = true;
            _directoryWatcher.Name = "CommandServer Directory Watcher";
            _directoryWatcher.Start();

            Log.Default.InfoFormat("Server listening on port {0}", Config.Port);
        }

        private void WatchForUploads () {
            try {
                while (true) {
                    var waitingClients = _server.GetClients().Where(c => !c.IsTransient && !c.IsBusy);

                    foreach (var client in waitingClients) {
                        if (Directory.EnumerateFiles(client.ClientPath).Any()) {
                            Log.Default.DebugFormat("Files found for {0}", client.Name);
                            client.WritePacket(new ReadyPacket());
                        }
                    }

                    // Resume in 5 seconds.
                    Thread.Sleep(5000);
                }
            } catch (ThreadAbortException) {
                throw;
            } catch (Exception ex) {
                Log.Default.Fatal("Error watching for uploads", ex);
            }
        }

        protected override void OnStop () {
            try {
                Log.Default.Info("Server shutting down");
                _directoryWatcher.Abort();
                _server.Stop();
            } catch (ThreadAbortException) {
                throw;
            } catch (Exception ex) {
                Log.Default.Fatal("Error stopping command server", ex);
            }
        }
    }
}
