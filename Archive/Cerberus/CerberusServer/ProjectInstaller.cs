﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.IO;

namespace Cerberus.Server {
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer {
        public ProjectInstaller () {
            InitializeComponent();
        }

        public override void Install (IDictionary stateSaver) {
            base.Install(stateSaver);

            // Find configuration file.
            var assemblypath = Context.Parameters["assemblypath"];
            var installDirectory = System.IO.Path.GetDirectoryName(assemblypath);
            var configPath = Path.Combine(installDirectory, "Cerberus.Server.exe.config");

            // Load configuration file and snip replacement section.
            var lines = File.ReadAllLines(configPath).ToList();
            var beginIndex = lines.FindIndex(s => s.Contains("<!-- CONFIG BEGIN -->"));
            var endIndex = lines.FindIndex(s => s.Contains("<!-- CONFIG END -->"));
            lines.RemoveRange(beginIndex, (endIndex - beginIndex) + 1);

            // Insert installer configuration values.
            lines.InsertRange(beginIndex, GenerateConfiguration());

            // Write configuration file.
            File.WriteAllLines(configPath, lines.ToArray());
        }

        private IEnumerable<string> GenerateConfiguration () {
            var port = Context.Parameters["port"];
            var clientDirectory = Context.Parameters["clientdirectory"];
            yield return "  <server port=\"" + port + "\" clientDirectory=\"" + clientDirectory + "\">";

            var storeLocation = Context.Parameters["storelocation"];
            var thumbprint = Context.Parameters["thumbprint"];
            var storeName = Context.Parameters["storename"];
            yield return "    <ssl thumbprint=\"" + thumbprint + "\" storeLocation=\"" + storeLocation + "\" storeName=\"" + storeName + "\" />";

            var logConnectionString = Context.Parameters["logconnectionstring"];
            yield return "    <log connectionString=\"" + logConnectionString + "\" />";

            yield return "  </server>";
        }
    }
}
