﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

using Nen.IO;

using Cerberus.Diagnostics.Data;

namespace CerberusWpfViewer {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow () {
            InitializeComponent();
        }

        protected override void OnClosing (System.ComponentModel.CancelEventArgs e) {
            base.OnClosing(e);
            SaveSettings();
        }

        #region Settings
        private void SaveSettings () {
            var settings = CerberusWpfViewer.Properties.Settings.Default;
            settings.MainWindowLeft = Left;
            settings.MainWindowTop = Top;
            settings.MainWindowWidth = Width;
            settings.MainWindowHeight = Height;
            settings.MainWindowEntryDetailHeight = _entryDetailRow.Height.IsStar ? 0 : _entryDetailRow.Height.Value;
            settings.MainWindowFilterColumnWidth = _filterColumn.Width.Value;
            settings.SelectedLogEntryId = _logEntries.SelectedItem == null ? 0 : ((LogEntryView) _logEntries.SelectedItem).LogEntry.Id;

            settings.FilterMessageText = _message.Text;
            settings.FilterRecordLimit = int.Parse(_recordLimit.Text);
            settings.FilterFromDate = _fromDate.SelectedDate ?? default(DateTime);
            settings.FilterToDate = _toDate.SelectedDate ?? default(DateTime);

            settings.FilterTags = new System.Collections.Specialized.StringCollection();
            foreach (var tagView in Tags.Where(t => t.ToggleState != TagToggleState.Ignored))
                settings.FilterTags.Add(string.Format("{0}={1}", tagView.Tag.Id, tagView.ToggleState));

            settings.Save();
        }

        private void ClearSettings () {
            var settings = CerberusWpfViewer.Properties.Settings.Default;
            settings.Reset();
            settings.Save();
        }

        private void LoadSettings (TagView[] tags) {
            var settings = CerberusWpfViewer.Properties.Settings.Default;

            if (settings.MainWindowLeft != 0)
                Left = settings.MainWindowLeft;

            if (settings.MainWindowTop != 0)
                Top = settings.MainWindowTop;

            if (settings.MainWindowHeight != 0)
                Height = settings.MainWindowHeight;

            if (settings.MainWindowWidth != 0)
                Width = settings.MainWindowWidth;

            if (settings.MainWindowEntryDetailHeight != 0)
                _entryDetailRow.Height = new GridLength(settings.MainWindowEntryDetailHeight, GridUnitType.Pixel);

            if (settings.MainWindowFilterColumnWidth != 0)
                _filterColumn.Width = new GridLength(settings.MainWindowFilterColumnWidth, GridUnitType.Pixel);

            if (settings.FilterToDate != default(DateTime))
                _toDate.SelectedDate = settings.FilterToDate;

            if (settings.FilterFromDate != default(DateTime))
                _fromDate.SelectedDate = settings.FilterFromDate;

            if (!string.IsNullOrWhiteSpace(settings.FilterMessageText))
                _message.Text = settings.FilterMessageText;

            if (settings.FilterRecordLimit != 0)
                _recordLimit.Text = settings.FilterRecordLimit.ToString();

            if (settings.FilterTags != null && settings.FilterTags.Count > 0) {
                foreach (var tagFilter in settings.FilterTags) {
                    var elements = tagFilter.Split('=');
                    var match = tags.FirstOrDefault(t => t.Tag.Id.ToString() == elements[0]);
                    if (match == null)
                        continue;

                    TagToggleState state;
                    if (!Enum.TryParse<TagToggleState>(elements[1], out state))
                        continue;

                    match.ToggleState = state;
                }
            }
        }
        #endregion

        #region Data Refresh
        protected override void OnInitialized (EventArgs e) {
            base.OnInitialized(e);

            try {
                using (var context = DataEnvironment.CreateContext()) {
                    var tags = context.Get<Tag>().ToArray().Select(t => new TagView(t)).ToArray();
                    LoadSettings(tags);

                    Tags = new ObservableCollection<TagView>(tags);
                    _tagsList.SelectedIndex = -1;

                    RefreshLog();

                    var settings = CerberusWpfViewer.Properties.Settings.Default;
                    if (settings.SelectedLogEntryId != 0) {
                        var entry = LogEntries.FirstOrDefault(l => l.LogEntry.Id == settings.SelectedLogEntryId);
                        if (entry != null)
                            _logEntries.SelectedItem = entry;
                    }
                }
            } catch (System.Threading.ThreadAbortException) {
                throw;
            } catch (Exception) {
                ClearSettings();
                throw;
            }
        }

        private void RefreshLog () {
            try {
                Cursor = Cursors.Wait;

                int recordLimit;
                if (!int.TryParse(_recordLimit.Text, out recordLimit))
                    recordLimit = 500;

                using (var context = DataEnvironment.CreateContext()) {
                    context.Log = new Nen.Diagnostics.DebugWriter();

                    IQueryable<LogEntry> query = context.Get<LogEntry>();

                    if (!string.IsNullOrWhiteSpace(_message.Text))
                        query = query.Where(l => l.Message.Text.Contains(_message.Text) || l.ExceptionMessage.Text.Contains(_message.Text));

                    if (_fromDate.SelectedDate != null)
                        query = query.Where(l => l.TimeStamp >= _fromDate.SelectedDate.Value);

                    if (_toDate.SelectedDate != null)
                        query = query.Where(l => l.TimeStamp <= _toDate.SelectedDate.Value.AddDays(1));

                    var includedTags = Tags.Where(t => t.ToggleState == TagToggleState.Included).Select(t => t.Tag).ToArray();
                    if (includedTags.Length > 0)
                        query = query.Where(l => includedTags.All(i => l.LogTags.Any(t => t.Tag == i)));

                    var excludedTags = Tags.Where(t => t.ToggleState == TagToggleState.Excluded).Select(t => t.Tag).ToArray();
                    if (excludedTags.Length > 0)
                        query = query.Where(l => excludedTags.All(i => !l.LogTags.Any(t => t.Tag == i)));

                    query = query.OrderByDescending(l => l.TimeStamp);
                    query = query.Take(recordLimit);

                    LogEntries = new ObservableCollection<LogEntryView>(query.ToArray().Select(l => new LogEntryView(l)));
                }
            } finally {
                Cursor = Cursors.Arrow;
            }
        }
        #endregion

        #region Dependency Properties
        public static DependencyProperty LogEntriesProperty = DependencyProperty.Register("LogEntries", typeof(ObservableCollection<LogEntryView>), typeof(MainWindow));
        public ObservableCollection<LogEntryView> LogEntries {
            get { return (ObservableCollection<LogEntryView>) GetValue(LogEntriesProperty); }
            set { SetValue(LogEntriesProperty, value); }
        }

        public static DependencyProperty TagsProperty = DependencyProperty.Register("Tags", typeof(ObservableCollection<TagView>), typeof(MainWindow));
        public ObservableCollection<TagView> Tags {
            get { return (ObservableCollection<TagView>) GetValue(TagsProperty); }
            set { SetValue(TagsProperty, value); }
        }
        #endregion

        #region Control Events
        private void _toggleTag_Click (object sender, MouseButtonEventArgs e) {
            var label = (Label) sender;
            var tagView = (TagView) label.DataContext;
            tagView.Toggle();
            label.Background = tagView.BackgroundBrush;

            _logEntries.Focus();
        }

        private void _recordLimit_PreviewTextInput (object sender, TextCompositionEventArgs e) {
            int value;
            e.Handled = !int.TryParse(e.Text, out value);
        }

        private void _export_Click (object sender, RoutedEventArgs e) {
            var tempFile = System.IO.Path.GetTempFileName() + ".csv";

            using (var fileStream = File.OpenWrite(tempFile)) {
                using (var stream = new StreamWriter(fileStream)) {
                    stream.WriteLine("Id,TimeStamp,Tags,ClientName,Message,Exception");

                    var values = new List<string>();
                    foreach (var entryView in LogEntries) {
                        var entry = entryView.LogEntry;

                        values.Add(entry.Id.ToString());
                        values.Add(entry.TimeStamp.ToString());
                        values.Add(Csv.FormatValue(entryView.Tags));
                        values.Add(Csv.FormatValue(entry.ClientName));

                        if (entry.Message == null)
                            values.Add("(null)");
                        else
                            values.Add(Csv.FormatValue(entry.Message.Text));

                        if (entry.ExceptionMessage == null)
                            values.Add("(null)");
                        else
                            values.Add(Csv.FormatValue(entry.ExceptionMessage.Text));

                        stream.WriteLine(string.Join(",", values.ToArray()));
                        values.Clear();
                    }
                }
            }

            // Open.
            System.Diagnostics.Process.Start(tempFile);
        }

        private void _refresh_Click (object sender, RoutedEventArgs e) {
            RefreshLog();
        }
        #endregion
    }
}
