﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CerberusWpfViewer {
    public static class Config {
        public static string ConnectionString {
            get { return ConfigurationManager.AppSettings["ConnectionString"]; }
        }
    }
}