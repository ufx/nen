﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Cerberus.Diagnostics.Data;

namespace CerberusWpfViewer {
    /// <summary>
    /// Interaction logic for LogEntryDetail.xaml
    /// </summary>
    public partial class LogEntryDetail : UserControl {
        public LogEntryDetail () {
            InitializeComponent();
        }

        #region Binding
        protected override void OnPropertyChanged (DependencyPropertyChangedEventArgs e) {
            base.OnPropertyChanged(e);

            if (e.Property == LogEntryProperty)
                BindLogEntry((LogEntry) e.NewValue);
        }

        private void BindLogEntry (LogEntry entry) {
            if (entry == null)
                return;

            // Append info block
            var text = new StringBuilder();

            text.AppendLine(entry.TimeStamp.ToString());

            if (entry.ClientName != null)
                text.AppendLine("Client: " + entry.ClientName);

            var tagNames = entry.LogTags.Select(t => t.Tag.Name).ToArray();
            if (tagNames.Length > 0)
                text.AppendLine("Tags: " + string.Join(", ", tagNames));

            if (entry.LogSource != null) {
                text.AppendLine();

                if (entry.LogSource.Domain != null)
                    text.AppendLine("Domain: " + entry.LogSource.Domain);

                if (!string.IsNullOrWhiteSpace(entry.LogSource.ThreadIdentity))
                    text.AppendLine("ThreadIdentity: " + entry.LogSource.ThreadIdentity);

                if (entry.LogSource.ThreadName != null)
                    text.AppendLine("ThreadName: " + entry.LogSource.ThreadName);

                if (entry.LogSource.UserName != null)
                    text.AppendLine("UserName: " + entry.LogSource.UserName);

                if (entry.LogSource.FileName != null)
                    text.AppendLine("FileName: " + entry.LogSource.FileName);

                if (entry.LogSource.ClassName != null)
                    text.AppendLine("ClassName: " + entry.LogSource.ClassName);

                if (entry.LogSource.MethodName != null)
                    text.AppendLine("MethodName: " + entry.LogSource.MethodName);

                if (entry.LogSource.LineNumber != null)
                    text.AppendLine("LineNumber: " + entry.LogSource.LineNumber);
            }

            if (entry.LogProperties.Count > 0) {
                text.AppendLine();

                foreach (var property in entry.LogProperties) {
                    text.AppendLine(property.Name + ": " + property.Value);
                }
            }

            // Append messages
            text.AppendLine();

            if (entry.Message != null) {
                text.AppendLine("Message:");
                text.AppendLine(entry.Message.Text);
            }

            if (entry.ExceptionMessage != null) {
                if (text.Length > 0)
                    text.AppendLine();

                text.AppendLine("Exception:");
                text.AppendLine(entry.ExceptionMessage.Text);
            }

            // Insert
            _text.Text = text.ToString().Trim();
        }
        #endregion

        #region Dependency Properties
        public static DependencyProperty LogEntryProperty = DependencyProperty.Register("LogEntry", typeof(LogEntry), typeof(LogEntryDetail));
        public LogEntry LogEntry {
            get { return (LogEntry) GetValue(LogEntryProperty); }
            set { SetValue(LogEntryProperty, value); }
        }
        #endregion

        #region Actions
        private void _open_Click (object sender, RoutedEventArgs e) {
            var entry = LogEntry;

            var tempFile = System.IO.Path.GetTempFileName();

            // Fix the extension.
            if (entry.LogTags.Any(t => t.Tag.Name == "CSV")) {
                System.IO.File.Move(tempFile, tempFile + ".csv");
                tempFile = tempFile + ".csv";
            } else {
                System.IO.File.Move(tempFile, tempFile + ".txt");
                tempFile = tempFile + ".txt";
            }

            // Write file.
            System.IO.File.WriteAllText(tempFile, entry.Message.Text);

            // Open.
            System.Diagnostics.Process.Start(tempFile);
        }
        #endregion
    }
}
