﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Cerberus.Diagnostics.Data;

namespace CerberusWpfViewer {
    public class LogEntryView {
        public LogEntryView (LogEntry entry) {
            LogEntry = entry;
        }

        public LogEntry LogEntry { get; set; }

        public string Tags {
            get {
                return string.Join(", ", LogEntry.LogTags.Select(t => t.Tag.Name).OrderBy(n => n).ToArray());
            }
        }

        public string Header {
            get {
                var text = new StringBuilder();
                text.Append(LogEntry.TimeStamp.ToString("hh:mm:ss tt") + " - " + Tags);

                if (LogEntry.Message != null) {
                    text.Append(" :: ");
                    text.Append(LogEntry.Message.Text);
                }

                if (LogEntry.ExceptionMessage != null) {
                    text.Append(" :: ");
                    text.Append(LogEntry.ExceptionMessage.Text);
                }

                text.Replace("\r\n", " ");
                text.Replace("\r", " ");
                text.Replace("\n", " ");

                return text.ToString();
            }
        }
    }

    public class TagView {
        public TagView (Tag tag) {
            Tag = tag;
            ToggleState = TagToggleState.Ignored;
        }

        public TagToggleState Toggle () {
            switch (ToggleState) {
                case TagToggleState.Ignored:
                    ToggleState = TagToggleState.Included;
                    break;

                case TagToggleState.Included:
                    ToggleState = TagToggleState.Excluded;
                    break;

                case TagToggleState.Excluded:
                    ToggleState = TagToggleState.Ignored;
                    break;
            }

            return ToggleState;
        }

        public System.Windows.Media.Brush BackgroundBrush {
            get {
                switch (ToggleState) {
                    case TagToggleState.Ignored: return System.Windows.Media.Brushes.Transparent;
                    case TagToggleState.Included: return System.Windows.Media.Brushes.CornflowerBlue;
                    case TagToggleState.Excluded: return System.Windows.Media.Brushes.Red;
                    default:
                        throw new NotSupportedException();
                }
            }
        }

        public Tag Tag { get; set; }
        public TagToggleState ToggleState { get; set; }
    }

    public enum TagToggleState { Ignored, Included, Excluded };
}
