using System;
using System.Collections.Generic;
using System.Text;

namespace Nen.ChangeTracking {
    /// <summary>
    /// Allows objects to communicate whenever properties change, and
    /// optionally cancel changes before they are made.
    /// </summary>
    public interface IChangeTrackable {
        /// <summary>
        /// Raised whenever a property has changed.
        /// </summary>
        event EventHandler<PropertyChangeEventArgs> PropertyChanged;

        /// <summary>
        /// Raised when a property is changing, but before the change has taken place.
        /// </summary>
        event EventHandler<PropertyChangingEventArgs> PropertyChanging;

        /// <summary>
        /// Raises the PropertyChanged event.  Typically used by the internal
        /// Field.Set helper method.
        /// </summary>
        /// <param name="eventArgs">Property changed detail information.</param>
        void OnPropertyChanged (PropertyChangeEventArgs eventArgs);

        /// <summary>
        /// Raises the PropertyChanging event.  Typically used by the internal
        /// Field.Set helper method.
        /// </summary>
        /// <param name="eventArgs">Property changing detail information.</param>
        void OnPropertyChanging (PropertyChangingEventArgs eventArgs);
    }

    #region PropertyChange
    /// <summary>
    /// Contains details on property change events.
    /// </summary>
    public class PropertyChangeEventArgs : EventArgs {
        #region Constructor
        /// <summary>
        /// Constructs a new PropertyChangeEventArgs.
        /// </summary>
        /// <param name="name">The name of the property.</param>
        /// <param name="newValue">The new value of the property.</param>
        /// <param name="oldValue">The previous value of the property.</param>
        public PropertyChangeEventArgs (string name, object newValue, object oldValue) {
            Name = name;
            NewValue = newValue;
            OldValue = oldValue;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The name of the property.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The new value of the property.
        /// </summary>
        public object NewValue { get; private set; }

        /// <summary>
        /// The previous value of the property.
        /// </summary>
        public object OldValue { get; private set; }
        #endregion
    }

    /// <summary>
    /// Contains details on cancelable property change events.
    /// </summary>
    public class PropertyChangingEventArgs : PropertyChangeEventArgs {
        #region Constructor
        /// <summary>
        /// Constructs a new PropertyChangingEventArgs.
        /// </summary>
        /// <param name="name">The name of the property.</param>
        /// <param name="newValue">The new value of the property.</param>
        /// <param name="oldValue">The previous value of the property.</param>
        public PropertyChangingEventArgs (string name, object newValue, object oldValue)
            : base(name, newValue, oldValue) {
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Mark this property true if the property change should be canceled.
        /// </summary>
        public bool CancelChange { get; set; }
        #endregion
    }
    #endregion
}
