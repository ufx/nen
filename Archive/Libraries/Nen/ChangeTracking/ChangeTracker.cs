using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Reflection;

namespace Nen.ChangeTracking {
    /// <summary>
    /// Collects the change events for all bound objects and republishes their
    /// events. This makes change tracking of an entire object graph easy to
    /// manage.  Understands IFuture and ObservableCollection.
    /// </summary>
    public class ChangeTracker: IDisposable {
        private Dictionary<IFuture, IObservableCollection> _trackedCollectionFutures = new Dictionary<IFuture, IObservableCollection>();
        private ReferenceCountingSet<IObservableCollection> _trackedCollections = new ReferenceCountingSet<IObservableCollection>();
        private ReferenceCountingSet<IFuture> _trackedFutures = new ReferenceCountingSet<IFuture>();
        private ReferenceCountingSet<IChangeTrackable> _trackedObjects = new ReferenceCountingSet<IChangeTrackable>();

        #region Events
        /// <summary>
        /// Raised whenever a tracked collection has changed.
        /// </summary>
        public event EventHandler<CollectionChangedEventArgs> CollectionChanged;

        /// <summary>
        /// Raised whenever the object graph has changed, i.e. an object
        /// reference was added or removed on any tracked object.
        /// </summary>
        public event EventHandler<GraphChangedEventArgs> GraphChanged;

        /// <summary>
        /// Raised whenever a property has changed on any tracked object. 
        /// </summary>
        public event EventHandler<PropertyChangeEventArgs> PropertyChanged;

        /// <summary>
        /// Raised when a property is changing on any tracked object, but before the change has taken place.
        /// </summary>
        public event EventHandler<PropertyChangingEventArgs> PropertyChanging;

        private void TrackedFutureCollectionResolved (object sender, ComputationResultEventArgs e) {
            var future = (IFuture) sender;
            var collection = _trackedCollectionFutures[future];
            foreach (var obj in collection)
                StartTracking(obj);
            future.Resolved -= TrackedFutureCollectionResolved;
        }
        
        private void TrackedFutureResolved (object sender, ComputationResultEventArgs e) {
            StopTracking((IFuture) sender);
            StartTracking(e.Result);
        }

        private void TrackedCollectionChanged (object sender, CollectionChangedEventArgs e) {
            if (e.ChangeAction == CollectionChangeAction.Inserted)
                StartTracking(e.Item);
            else if (e.ChangeAction == CollectionChangeAction.Removed)
                StopTracking(e.Item);

            if (CollectionChanged != null)
                CollectionChanged(sender, e);
        }

        private void TrackedPropertyChanging (object sender, PropertyChangingEventArgs e) {
            if (PropertyChanging != null)
                PropertyChanging(sender, e);
        }

        private void TrackedPropertyChanged (object sender, PropertyChangeEventArgs e) {
            StopTracking(e.OldValue);
            StartTracking(e.NewValue);

            if (PropertyChanged != null)
                PropertyChanged(sender, e);
        }

        private void OnGraphChanged (GraphChangeAction changeAction, object obj) {
            if (GraphChanged != null)
                GraphChanged(this, new GraphChangedEventArgs(changeAction, obj));
        }
        #endregion

        #region Reference Counting
        /// <summary>
        /// Retrieves the graph reference count of the tracked object.
        /// </summary>
        /// <param name="obj">The tracked object.</param>
        /// <returns>The number of references in the graph.</returns>
        public int GetReferenceCount (IChangeTrackable obj) {
            return _trackedObjects.GetReferenceCount(obj);
        }

        /// <summary>
        /// Retrieves the graph reference count of the tracked collection.
        /// </summary>
        /// <param name="collection">The tracked collection.</param>
        /// <returns>The number of references in the graph.</returns>
        public int GetReferenceCount (IObservableCollection collection) {
            return _trackedCollections.GetReferenceCount(collection);
        }

        /// <summary>
        /// Retrieves the graph reference count of the tracked future.
        /// </summary>
        /// <param name="future">The tracked future.</param>
        /// <returns>The number of references in the graph.</returns>
        public int GetReferenceCount (IFuture future) {
            return _trackedFutures.GetReferenceCount(future);
        }
        #endregion

        #region StartTracking
        /// <summary>
        /// Inserts the object into the graph of tracked objects, and observes
        /// any changes in its state.
        /// </summary>
        /// <param name="obj">The object to track.</param>
        public void StartTracking (IChangeTrackable obj) {
            if (obj == null)
                throw new ArgumentNullException("obj");

            // Increment the reference count.
            if (_trackedObjects.Increment(obj) > 1)
                return;

            // Track new property change events.
            OnGraphChanged(GraphChangeAction.Inserted, obj);

            obj.PropertyChanged += TrackedPropertyChanged;
            obj.PropertyChanging += TrackedPropertyChanging;

            // Recursively track contained values.
            var values = VariableInfo.GetValues(obj, obj.GetType().GetVariables(TypeEx.InstanceBindingFlags, MemberTypes.Property));
            foreach (var value in values)
                StartTracking(value);
        }

        /// <summary>
        /// Observes collection state changes, as well as the changes of any
        /// objects it contains.  Understands collections that are backed with
        /// IFuture items.
        /// </summary>
        /// <param name="collection">The collection to track.</param>
        public void StartTracking (IObservableCollection collection) {
            if (collection == null)
                throw new ArgumentNullException("collection");

            // Increment the reference count.
            if (_trackedCollections.Increment(collection) > 1)
                return;

            // Track new collection change events.
            collection.Changed += TrackedCollectionChanged;

            // Defer tracking of contained objects until internal list future
            // is resolved, if applicable.
            var items = collection.GetInternalItems() as IFuture;
            if (items != null) {
                _trackedCollectionFutures[items] = collection;
                items.Resolved += TrackedFutureCollectionResolved;
                return;
            }

            // Track contained objects.
            foreach (var obj in collection)
                StartTracking(obj);
        }

        /// <summary>
        /// Waits until the future is resolved, then tracks the result value.
        /// </summary>
        /// <param name="future">The future to track.</param>
        public void StartTracking (IFuture future) {
            if (future == null)
                throw new ArgumentNullException("future");

            // Track the result only if it's already been resolved.
            if (future.IsResolved) {
                StartTracking(future.Resolve());
                return;
            }

            // Increment the reference count.
            if (_trackedFutures.Increment(future) > 1)
                return;

            // Track new future resolution events.
            future.Resolved += TrackedFutureResolved;
        }

        private void StartTracking (object obj) {
            if (obj == null)
                return;

            if (obj is IChangeTrackable)
                StartTracking((IChangeTrackable) obj);
            else if (obj is IObservableCollection)
                StartTracking((IObservableCollection) obj);
            else if (obj is IFuture)
                StartTracking((IFuture) obj);
        }
        #endregion

        #region StopTracking
        void IDisposable.Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all managed events.
        /// </summary>
        /// <param name="disposing">true if disposing, false if otherwise.</param>
        protected virtual void Dispose (bool disposing) {
            if (!disposing)
                return;

            // Release all events.
            foreach (var future in _trackedFutures)
                future.Resolved -= TrackedFutureResolved;
            _trackedFutures.Clear();

            foreach (var collection in _trackedCollections)
                collection.Changed -= TrackedCollectionChanged;
            _trackedCollections.Clear();

            foreach (var items in _trackedCollectionFutures.Keys)
                items.Resolved -= TrackedFutureCollectionResolved;
            _trackedCollectionFutures.Clear();

            foreach (var obj in _trackedObjects) {
                obj.PropertyChanged -= TrackedPropertyChanged;
                obj.PropertyChanging -= TrackedPropertyChanging;
            }
            _trackedObjects.Clear();
        }

        /// <summary>
        /// Reduces the reference count of the object, and stops tracking when
        /// that count reaches zero.
        /// </summary>
        /// <param name="obj">The object to stop tracking.</param>
        public void StopTracking (IChangeTrackable obj) {
            if (obj == null)
                throw new ArgumentNullException("obj");

            if (!_trackedObjects.Contains(obj))
                return;

            // Decrement the reference count.
            if (_trackedObjects.Decrement(obj) > 0)
                return;

            // Remove dead tracking events.
            OnGraphChanged(GraphChangeAction.Removed, obj);

            obj.PropertyChanged -= TrackedPropertyChanged;
            obj.PropertyChanging -= TrackedPropertyChanging;

            // Recursively stop tracking contained values.
            var values = VariableInfo.GetValues(obj, obj.GetType().GetVariables(TypeEx.InstanceBindingFlags, MemberTypes.Property));
            foreach (object value in values)
                StopTracking(value);
        }

        /// <summary>
        /// Reduces the reference count of the collection, and stops tracking
        /// when that count reaches zero.
        /// </summary>
        /// <param name="collection">The collection to stop tracking.</param>
        public void StopTracking (IObservableCollection collection) {
            if (collection == null)
                throw new ArgumentNullException("collection");

            if (!_trackedCollections.Contains(collection))
                return;

            // Decrement the reference count.
            if (_trackedCollections.Decrement(collection) > 0)
                return;

            // Remove dead tracking events.
            collection.Changed -= TrackedCollectionChanged;

            // Stop tracking list future resolution, if applicable.
            var items = collection.GetInternalItems() as IFuture;
            if (items != null) {
                _trackedCollectionFutures.Remove(items);
                items.Resolved -= TrackedFutureCollectionResolved;
                return;
            }

            // Stop tracking contained objects.
            foreach (var obj in collection)
                StopTracking(obj);
        }

        /// <summary>
        /// Reduces the reference count of the future, and stops tracking when
        /// that count reaches zero.
        /// </summary>
        /// <param name="future">The future to stop tracking.</param>
        public void StopTracking (IFuture future) {
            if (future == null)
                throw new ArgumentNullException("future");

            if (!_trackedFutures.Contains(future))
                return;

            // Decrement the reference count.
            if (_trackedFutures.Decrement(future) > 0)
                return;

            // Remove dead tracking events.
            future.Resolved -= TrackedFutureResolved;
        }

        private void StopTracking (object obj) {
            if (obj == null)
                return;

            if (obj is IChangeTrackable)
                StopTracking((IChangeTrackable) obj);
            else if (obj is IObservableCollection)
                StopTracking((IObservableCollection) obj);
            else if (obj is IFuture)
                StopTracking((IFuture) obj);
        }

        /// <summary>
        /// Stops tracking all tracked items.
        /// </summary>
        public void StopTrackingAll () {
            Dispose(true);
        }
        #endregion
    }

    #region GraphChanged
    /// <summary>
    /// Used to specify what kind of action took place in the graph.
    /// </summary>
    public enum GraphChangeAction {
        /// <summary>
        /// An object reference was inserted in the graph.
        /// </summary>
        Inserted,

        /// <summary>
        /// An object reference was removed from the graph.
        /// </summary>
        Removed
    }

    /// <summary>
    /// Contains details on graph change events.
    /// </summary>
    public class GraphChangedEventArgs : EventArgs {
        /// <summary>
        /// Constructs a new GraphChangedEventArgs.
        /// </summary>
        /// <param name="changeAction">The change action that took place.</param>
        /// <param name="obj">The object where the change occurred.</param>
        public GraphChangedEventArgs (GraphChangeAction changeAction, object obj) {
            ChangeAction = changeAction;
            Object = obj;
        }

        #region Accessors
        /// <summary>
        /// The change action that took place.
        /// </summary>
        public GraphChangeAction ChangeAction { get; private set; }

        /// <summary>
        /// The object where the change occurred.
        /// </summary>
        public object Object { get; private set; }
        #endregion
    }
    #endregion
}
