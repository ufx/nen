using System;
using System.Collections.Generic;
using System.Text;

namespace Nen.ChangeTracking {
    /// <summary>
    /// Field helper class for handling change tracking.
    /// </summary>
    public static class Field {
        /// <summary>
        /// Calls change tracking events and supports change cancelation.
        /// </summary>
        /// <typeparam name="T">The type of the field being set by this property.</typeparam>
        /// <param name="obj">The trackable object.</param>
        /// <param name="name">The name of the property.</param>
        /// <param name="value">The new value of the property.</param>
        /// <param name="field">A reference to the underlying property field.</param>
        /// <returns>true if the field was changed, false if the field didn't need to be changed or the operation was canceled.</returns>
        public static bool Set<T> (IChangeTrackable obj, string name, T value, ref T field) {
            if (Equals(field, value)) {
                // Assign the field even if the values are equal, because some
                // objects have special rules governing equality that shouldn't
                // be extended to field changes.  Change events are probably
                // not required in such cases.

                field = value;
                return false;
            }

            var eventArgs = new PropertyChangingEventArgs(name, value, field);
            obj.OnPropertyChanging(eventArgs);
            if (eventArgs.CancelChange)
                return false;

            field = value;
            obj.OnPropertyChanged(eventArgs);
            return true;
        }
    }
}
