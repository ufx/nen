using System;
using System.Collections.Generic;
using System.Text;

namespace Nen.ChangeTracking {
    /// <summary>
    /// Implements IChangeTrackable events and provides a simple field setter
    /// helper to broadcast these events.
    /// </summary>
    public abstract class ChangeTrackable : IChangeTrackable {
        #region IChangeTrackable Members
        /// <summary>
        /// Raised whenever a property has changed.
        /// </summary>
        public event EventHandler<PropertyChangeEventArgs> PropertyChanged;

        /// <summary>
        /// Raised when a property is changing, but before the change has taken place.
        /// </summary>
        public event EventHandler<PropertyChangingEventArgs> PropertyChanging;

        void IChangeTrackable.OnPropertyChanged (PropertyChangeEventArgs eventArgs) {
            if (PropertyChanged != null)
                PropertyChanged(this, eventArgs);
        }

        void IChangeTrackable.OnPropertyChanging (PropertyChangingEventArgs eventArgs) {
            if (PropertyChanging != null)
                PropertyChanging(this, eventArgs);
        }
        #endregion

        #region Field Change Helper
        /// <summary>
        /// Helper method to call the appropriate IChangeTrackable events.
        /// </summary>
        /// <typeparam name="T">The type of field being set by this property.</typeparam>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="value">The new value of the property.</param>
        /// <param name="field">A reference to the underlying property field.</param>
        protected void Set<T> (string propertyName, T value, ref T field) {
            Field.Set(this, propertyName, value, ref field);
        }
        #endregion
    }
}
