using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

using Nen.ChangeTracking;

namespace Nen.DataBinding {
    /// <summary>
    /// Stores a list of data binding items.
    /// </summary>
    public class BindingItemCollection : Collection<BindingItemCore> {
        #region Constructors
        /// <summary>
        /// Constructs a new BindingItemCollection.
        /// </summary>
        /// <param name="dataBinder">The underlying data binder.</param>
        public BindingItemCollection (DataBinder dataBinder) {
            DataBinder = dataBinder;
        }

        /// <summary>
        /// Constructs a new BindingItemCollection.
        /// </summary>
        public BindingItemCollection () {
        }
        #endregion

        #region Collection Members
        /// <summary>
        /// Inserts an item into the collection, and sets the item DataBinder
        /// property to the collection DataBinder property.
        /// </summary>
        /// <param name="index">The index of the item to insert.</param>
        /// <param name="item">The item to insert.</param>
        protected override void InsertItem (int index, BindingItemCore item) {
            base.InsertItem(index, item);
            item.DataBinder = DataBinder;
        }

        /// <summary>
        /// Removes and disposes an item from the collection, and sets the item
        /// DataBinder property to null.
        /// </summary>
        /// <param name="index">The index of the item to remove.</param>
        protected override void RemoveItem (int index) {
            var item = this[index];
            item.Dispose();
            item.DataBinder = null;

            base.RemoveItem(index);
        }

        /// <summary>
        /// Removes and disposes all items from the collection, and sets their
        /// DataBinder properties to null.
        /// </summary>
        protected override void ClearItems () {
            foreach (var item in this) {
                item.Dispose();
                item.DataBinder = null;
            }

            base.ClearItems();
        }

        /// <summary>
        /// Replaces an item in the collection with a new item.  The replaced
        /// item is disposed and its DataBinder property is set to null, and
        /// the new item DataBinder property is set to the collection
        /// DataBinder property.
        /// </summary>
        /// <param name="index">The index of the item to replace.</param>
        /// <param name="item">The new item.</param>
        protected override void SetItem (int index, BindingItemCore item) {
            var oldItem = this[index];
            oldItem.Dispose();
            oldItem.DataBinder = null;

            base.SetItem(index, item);
            item.DataBinder = DataBinder;
        }
        #endregion

        #region Add
        /// <summary>
        /// Adds a binding item for the specified properties.
        /// </summary>
        /// <typeparam name="TSource">The source object type.</typeparam>
        /// <typeparam name="TDestination">The destination object type.</typeparam>
        /// <param name="sourceObject">The source object to bind.</param>
        /// <param name="sourcePropertyName">The property name on the source object.</param>
        /// <param name="destinationObject">The destination object to bind.</param>
        /// <param name="destinationPropertyName">The property name on the destination object.</param>
        public void Add<TSource, TDestination> (TSource sourceObject, string sourcePropertyName, TDestination destinationObject, string destinationPropertyName)
            where TSource : IChangeTrackable
            where TDestination : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceObject, sourcePropertyName);
            var destination = new PropertyBindingParameter<TDestination>(destinationObject, destinationPropertyName);

            Add(source, destination);
        }

        /// <summary>
        /// Adds a binding item for the specified parameters.
        /// </summary>
        /// <param name="source">The source parameter.</param>
        /// <param name="destination">The destination parameter.</param>
        public void Add (BindingParameter source, BindingParameter destination) {
            Add(new BindingItem(source, destination));
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The underlying data binder.
        /// </summary>
        internal DataBinder DataBinder { get; set; }
        #endregion
    }
}
