using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

using Nen.ChangeTracking;
using Nen.Collections;
using Nen.Reflection;

namespace Nen.DataBinding {
    /// <summary>
    /// A binding parameter represents one side of a binding item.
    /// </summary>
    public abstract class BindingParameter : IDisposable {
        #region Dispose
        /// <summary>
        /// Disposes the BindingParameter.
        /// </summary>
        public void Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the BindingParameter.
        /// </summary>
        /// <param name="disposing">true if disposing, false if otherwise.</param>
        protected virtual void Dispose (bool disposing) {
        }
        #endregion

        #region Events
        /// <summary>
        /// Raised whenever the the parameter value has changed.
        /// </summary>
        public virtual event EventHandler<EventArgs> ValueChanged;

        /// <summary>
        /// Raises the ValueChanged event.
        /// </summary>
        protected virtual void OnValueChanged () {
            if (ValueChanged != null)
                ValueChanged(this, EventArgs.Empty);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The BindingItem that contains this parameter.
        /// </summary>
        public BindingItemCore BindingItem { get; set; }

        /// <summary>
        /// The converter that all reads are passed through.
        /// </summary>
        public Converter<object, object> GetConverter { get; set; }

        /// <summary>
        /// The source of this parameter.
        /// </summary>
        public abstract object DataSource { get; }

        /// <summary>
        /// The converter that all writes are passed through.
        /// </summary>
        public Converter<object, object> SetConverter { get; set; }

        /// <summary>
        /// Gets or sets the converted parameter value.
        /// </summary>
        public object Value {
            get {
                var value = ValueCore;
                if (GetConverter != null)
                    value = GetConverter(value);
                return value;
            }
            set {
                if (SetConverter != null)
                    ValueCore = SetConverter(value);
                else
                    ValueCore = value;
            }
        }

        /// <summary>
        /// Gets or sets the core parameter value, without conversion.
        /// </summary>
        protected abstract object ValueCore { get; set; }
        #endregion
    }

    /// <summary>
    /// A binding parameter that uses the given value as both its data source and value.
    /// </summary>
    public class ValueBindingParameter : BindingParameter {
        private object _value;

        #region Constructors
        /// <summary>
        /// Constructs a new ValueBindingParameter.
        /// </summary>
        /// <param name="value">The value of this parameter.</param>
        public ValueBindingParameter (object value) {
            _value = value;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The value of this parameter.
        /// </summary>
        public override object DataSource {
            get { return _value; }
        }

        /// <summary>
        /// Gets or sets the value of this parameter.
        /// </summary>
        protected override object ValueCore {
            get { return _value; }
            set { _value = value; }
        }
        #endregion
    }

    /// <summary>
    /// A binding to a property value on a trackable data source.
    /// </summary>
    public abstract class PropertyBindingParameter : BindingParameter {
        private IChangeTrackable _dataSource;

        #region Constructors
        /// <summary>
        /// Constructs a new PropertyBindingParameter.
        /// </summary>
        /// <param name="dataSource">The source where the property lives.</param>
        /// <param name="property">The property to bind.</param>
        protected PropertyBindingParameter (IChangeTrackable dataSource, VariableInfo property) {
            _dataSource = dataSource;
            Property = property;

            dataSource.PropertyChanged += SourcePropertyChanged;
        }
        #endregion

        #region Property Access
        /// <summary>
        /// Gets a weakly typed variable wrapper for the given property name.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <param name="declaringType">The declaring type where the property lives.</param>
        /// <returns>A weakly typed variable wrapper around the property.</returns>
        protected static VariableInfo GetProperty (string propertyName, Type declaringType) {
            var property = declaringType.GetVariable(propertyName) as VariableInfo;
            if (property == null)
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Property '{0}' does not exist on the source object type '{1}'", propertyName, declaringType), "propertyName");

            // Allow simple conversions for databound properties.
            return new WeaklyTypedVariableInfo(property);
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Disposes the PropertyBindingParameter by unbinding the
        /// PropertyChanged event on the data source.
        /// </summary>
        /// <param name="disposing">true if disposing, false if otherwise.</param>
        protected override void Dispose (bool disposing) {
            base.Dispose(disposing);

            if (disposing)
                _dataSource.PropertyChanged -= SourcePropertyChanged;
        }
        #endregion

        #region Tracking Events
        /// <summary>
        /// Calls OnValueChanged if the property change matches the bound
        /// property.
        /// </summary>
        /// <param name="sender">The publisher of the property change event.</param>
        /// <param name="e">The property change event arguments.</param>
        private void SourcePropertyChanged (object sender, PropertyChangeEventArgs e) {
            if (e.Name == Property.Name)
                OnValueChanged();
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The source where the property lives.
        /// </summary>
        public override object DataSource {
            get { return _dataSource; }
        }

        /// <summary>
        /// The property to bind.
        /// </summary>
        public VariableInfo Property { get; private set; }

        /// <summary>
        /// Gets or sets the value of the property on the data source.
        /// </summary>
        protected override object ValueCore {
            get { return Property.GetValue(_dataSource); }
            set { Property.SetValue(_dataSource, value); }
        }
        #endregion
    }

    /// <summary>
    /// A binding to a property value on a trackable data source.
    /// </summary>
    /// <typeparam name="T">The type of data source.</typeparam>
    public class PropertyBindingParameter<T> : PropertyBindingParameter where T : IChangeTrackable {
        #region Constructors
        /// <summary>
        /// Constructs a new PropertyBindingParameter.
        /// </summary>
        /// <param name="dataSource">The source where the property lives.</param>
        /// <param name="propertyName">The name of the property to bind.</param>
        public PropertyBindingParameter (T dataSource, string propertyName)
            : base(dataSource, GetProperty(propertyName, typeof(T))) {
        }
        #endregion
    }
}
