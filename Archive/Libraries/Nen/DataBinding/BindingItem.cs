using System;
using System.Collections.Generic;
using System.Text;

using Nen.Collections;

namespace Nen.DataBinding {
    /// <summary>
    /// Provides a binding between a source and destination binding parameter,
    /// managing synchronization and keeping values up to date.
    /// </summary>
    public abstract class BindingItemCore : IDisposable {
        private bool _isBinding;
        private bool _isDisposed;

        #region Constructors
        /// <summary>
        /// Constructs a new BindingItemCore.
        /// </summary>
        /// <param name="source">The source binding parameter.</param>
        /// <param name="destination">The destination binding parameter.</param>
        protected BindingItemCore (BindingParameter source, BindingParameter destination) {
            if (source == null)
                throw new ArgumentNullException("source");

            if (destination == null)
                throw new ArgumentNullException("destination");

            destination.BindingItem = this;
            source.BindingItem = this;
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Unooks change events and disposes of contained parameters.
        /// </summary>
        public void Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Unhooks change events and disposes of conatined parameters.
        /// </summary>
        /// <param name="disposing">true if disposing, false if otherwise.</param>
        protected virtual void Dispose (bool disposing) {
            if (disposing && !_isDisposed) {
                _isDisposed = true;

                if (IsSynchronized) {
                    Destination.ValueChanged -= DestinationValueChanged;
                    Source.ValueChanged -= SourceValueChanged;
                }

                Destination.Dispose();
                Source.Dispose();
            }
        }
        #endregion

        #region Value Change Events
        private void SourceValueChanged (object sender, EventArgs e) {
            if (_isDisposed)
                throw new ObjectDisposedException("BindingItem");

            if (_isBinding)
                return;

            try {
                _isBinding = true;
                Destination.Value = Source.Value;
            } finally {
                _isBinding = false;
            }
        }

        private void DestinationValueChanged (object sender, EventArgs e) {
            if (_isDisposed)
                throw new ObjectDisposedException("BindingItem");

            if (_isBinding)
                return;

            try {
                _isBinding = true;
                Source.Value = Destination.Value;
            } finally {
                _isBinding = false;
            }
        }
        #endregion

        #region Synchronization
        /// <summary>
        /// Synchronizes changes between the source and destination parameters,
        /// and ensures subsequent changes are communicated.
        /// </summary>
        public virtual void Synchronize () {
            if (_isDisposed)
                throw new ObjectDisposedException("BindingItem");

            if (IsSynchronized)
                return;

            Destination.Value = Source.Value;

            // Keep data synchronized when changes happen.
            Destination.ValueChanged += DestinationValueChanged;
            Source.ValueChanged += SourceValueChanged;

            IsSynchronized = true;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The DataBinder that contains this item.
        /// </summary>
        public DataBinder DataBinder { get; set; }

        /// <summary>
        /// The destination binding parameter.
        /// </summary>
        public abstract BindingParameter Destination { get; }

        /// <summary>
        /// Gets an indicator on the synchronization status of the item.
        /// </summary>
        public bool IsSynchronized { get; private set; }

        /// <summary>
        /// The source binding parameter.
        /// </summary>
        public abstract BindingParameter Source { get; }
        #endregion
    }

    /// <summary>
    /// Provides a binding between a simple source and destination binding
    /// parameter, managing synchronization and keeping values up to date.
    /// </summary>
    public class BindingItem : BindingItemCore {
        private BindingParameter _destination;
        private BindingParameter _source;

        #region Constructors
        /// <summary>
        /// Constructs a new BindingItem.
        /// </summary>
        /// <param name="source">The source binding parameter.</param>
        /// <param name="destination">The destination binding parameter.</param>
        public BindingItem (BindingParameter source, BindingParameter destination)
            : base(source, destination) {
            _destination = destination;
            _source = source;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The destination binding parameter.
        /// </summary>
        public override BindingParameter Destination {
            get { return _destination; }
        }

        /// <summary>
        /// The source binding parameter.
        /// </summary>
        public override BindingParameter Source {
            get { return _source; }
        }
        #endregion
    }
}
