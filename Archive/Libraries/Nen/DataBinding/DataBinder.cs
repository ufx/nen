using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;

using Nen.ChangeTracking;

namespace Nen.DataBinding {
    /// <summary>
    /// Binds trackable object values.
    /// </summary>
    public class DataBinder : IDisposable {
        #region Constructors
        /// <summary>
        /// Constructs a new DataBinder.
        /// </summary>
        public DataBinder () {
            Bindings = new BindingItemCollection(this);
        }

        /// <summary>
        /// Constructs a new DataBinder.
        /// </summary>
        /// <param name="bindings">A collection of binding items.</param>
        protected DataBinder (BindingItemCollection bindings) {
            if (bindings == null)
                throw new ArgumentNullException("bindings");

            Bindings = bindings;
            Bindings.DataBinder = this;
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Clears all bindings.
        /// </summary>
        public void Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Clears all bindings.
        /// </summary>
        /// <param name="disposing">true if disposing, false if otherwise.</param>
        protected virtual void Dispose (bool disposing) {
            if (disposing)
                Bindings.Clear();
        }
        #endregion

        #region Binding
        /// <summary>
        /// Synchronizes all binding changes.
        /// </summary>
        public virtual void Synchronize () {
            foreach (var item in Bindings)
                item.Synchronize();
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The bindings managed by this data binder.
        /// </summary>
        public BindingItemCollection Bindings { get; private set; }
        #endregion
    }
}
