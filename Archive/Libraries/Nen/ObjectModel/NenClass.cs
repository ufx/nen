﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using Nen.Reflection;
using System.Runtime.Serialization;

namespace Nen.ObjectModel {
    [Serializable]
    public sealed class NenClass : NenObject, ISerializable {
        private static Dictionary<Type, NenClass> _classes = new Dictionary<Type, NenClass>();
        public NenClass BaseClass { get; private set; }
        public Type PhysicalType { get; private set; }
        public string Name { get; private set; }
        public Dictionary<string, NenProperty> DeclaredProperties { get; internal set; }

        #region ISerializable Members
        private NenClass (SerializationInfo info, StreamingContext context) {
            BaseClass = (NenClass)info.GetValue("BaseClass", typeof(NenClass));
            PhysicalType = (Type)info.GetValue("PhysicalType", typeof(Type));
            Name = info.GetString("Name");
            DeclaredProperties = 
                (Dictionary<string, NenProperty>)info.GetValue("DeclaredProperties", typeof(Dictionary<string, NenProperty>));
        }

        void ISerializable.GetObjectData (SerializationInfo info, StreamingContext context) {
            if (PhysicalType != null) {
                info.SetType(typeof(NenClassReference));
                info.AddValue("PhysicalType", PhysicalType);
            } else {
                info.AddValue("PhysicalType", PhysicalType);
                info.AddValue("BaseClass", BaseClass);
                info.AddValue("Name", Name);
                info.AddValue("DeclaredProperties", DeclaredProperties);
            }
        }

        #endregion

        private NenClass (Type type) {
            PhysicalType = type;
            Name = type.Name;
            DeclaredProperties = new Dictionary<string, NenProperty>();
        }

        private NenClass (NenClass baseClass, string name) {
            BaseClass = baseClass;
        }

        private void Initialize () {
            if (PhysicalType != null && PhysicalType.BaseType != null && PhysicalType != typeof(object))
                BaseClass = GetClass(PhysicalType.BaseType);

            InitializeProperties();
        }

        /// <summary>
        /// Initializes implicitly-registered NenProperty&lt;T&gt; properties.
        /// </summary>
        private void InitializeProperties () {
            // Virtual types have no physical properties to scan...
            if (PhysicalType == null)
                return;

            foreach (FieldInfo fi in PhysicalType.GetFields(TypeEx.StaticBindingFlags)) {
                if (fi.FieldType.IsInstanceOfGenericType(typeof(NenProperty<>)) && fi.Name.EndsWith("Property")) {
                    fi.SetValue(null, 
                        NenProperty.Register(
                            fi.Name.Remove(fi.Name.Length - ("Property".Length)), 
                            PhysicalType, 
                            fi.FieldType.GetGenericArguments()[0]));
                }
            }
        }

        private static NenClass RegisterClass (Type frameworkType) {
            if (_classes.ContainsKey(frameworkType))
                throw new InvalidOperationException("The class is already registered.");

            NenClass klass = new NenClass(frameworkType);
            _classes[frameworkType] = klass;
            klass.Initialize();
            return klass;
        }

        public static NenClass GetClass (Type frameworkType) {
            NenClass klass;

            if (frameworkType == typeof(NenClass))
                return null; // todo: metaclasses

            if (!_classes.TryGetValue(frameworkType, out klass))
                klass = RegisterClass(frameworkType);

            return klass;
        }

        internal void RegisterProperty (NenProperty property) {
            DeclaredProperties[property.Name] = property;
        }

        public Type NearestPhysicalType {
            get { return PhysicalType ?? (BaseClass != null ? BaseClass.NearestPhysicalType : null); }
        }
    }

    [Serializable]
    internal class NenClassReference : IObjectReference, ISerializable {
        private Type _physicalType;

        #region IObjectReference Members
        public object GetRealObject (StreamingContext context) {
            return NenClass.GetClass(_physicalType);
        }
        #endregion

        #region ISerializable Members
        private NenClassReference (SerializationInfo info, StreamingContext context) {
            _physicalType = (Type)info.GetValue("PhysicalType", typeof(Type));
        }

        public void GetObjectData (SerializationInfo info, StreamingContext context) {
            // do nothing.
        }
        #endregion
    }

}
