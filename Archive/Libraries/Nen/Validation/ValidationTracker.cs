using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

using Nen.ChangeTracking;
using Nen.Collections;

namespace Nen.Validation {
    /// <summary>
    /// Collects the validation errors for all bound objects and publishes them
    /// as a set of change events.  Also listens for changes on bound objects
    /// for validation and republication.
    /// </summary>
    public class ValidationTracker : IDisposable {
        private ReferenceCountingDictionary<IValidationTrackable, Collection<ValidationResult>> _errors = new ReferenceCountingDictionary<IValidationTrackable, Collection<ValidationResult>>();

        #region Events
        /// <summary>
        /// Raised whenever a tracked object's validation status has changed.
        /// </summary>
        public event EventHandler<ValidationStatusChangedEventArgs> ValidationChanged;

        private void TrackedPropertyChanged (object sender, PropertyChangeEventArgs e) {
            Validate((IValidationTrackable) sender);
        }

        private void OnValidationChanged (ValidationErrorChangeAction changeAction, ValidationResult error) {
            if (ValidationChanged != null)
                ValidationChanged(this, new ValidationStatusChangedEventArgs(changeAction, error));
        }
        #endregion

        #region Tracking
        /// <summary>
        /// Stops tracking all objects.
        /// </summary>
        void IDisposable.Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Stops tracking all objects.
        /// </summary>
        /// <param name="disposing">true if is disposing, false if otherwise.</param>
        protected virtual void Dispose (bool disposing) {
            if (!disposing)
                return;

            List<IValidationTrackable> trackedObjects = new List<IValidationTrackable>(_errors.Keys);
            foreach (var obj in trackedObjects)
                StopTrackingCore(obj, true);
        }

        /// <summary>
        /// Initiates validation tracking of the specified object.
        /// </summary>
        /// <param name="obj">The object to track validations on.</param>
        public void StartTracking (IValidationTrackable obj) {
            if (_errors.ContainsKey(obj)) {
                _errors.Increment(obj);
                return;
            }

            _errors[obj] = new Collection<ValidationResult>();
            obj.PropertyChanged += TrackedPropertyChanged;
        }

        /// <summary>
        /// Reduces the reference count of the objct, and stops validation
        /// tracking when that count reaches zero.
        /// </summary>
        /// <param name="obj">The object to stop trackign vaildations on.</param>
        public void StopTracking (IValidationTrackable obj) {
            StopTrackingCore(obj, false);
        }

        private void StopTrackingCore (IValidationTrackable obj, bool removeAllReferences) {
            if (!_errors.ContainsKey(obj))
                return;

            var errors = _errors[obj];

            if (removeAllReferences)
                _errors.RemoveAll(obj);
            else if (_errors.Decrement(obj) > 0)
                return;

            obj.PropertyChanged -= TrackedPropertyChanged;

            foreach (var error in errors)
                OnValidationChanged(ValidationErrorChangeAction.Resolved, error);
        }

        /// <summary>
        /// Stops tracking all objects.
        /// </summary>
        public void StopTrackingAll () {
            Dispose(true);
        }
        #endregion

        #region Validation
        /// <summary>
        /// Refreshes validation status of all tracked objects.
        /// </summary>
        public void Refresh () {
            foreach (var obj in _errors.Keys)
                Validate(obj);
        }

        private void Validate (IValidationTrackable obj) {
            IEnumerable<ValidationResult> insertedErrors, removedErrors;
            var currentErrors = _errors[obj];
            var newErrors = obj.Validate();

            CollectionEx.FactorChanges(currentErrors, newErrors, out insertedErrors, out removedErrors);

            currentErrors.AddRange(insertedErrors);
            foreach (var error in insertedErrors)
                OnValidationChanged(ValidationErrorChangeAction.Present, error);

            currentErrors.RemoveRange(removedErrors);
            foreach (var error in removedErrors)
                OnValidationChanged(ValidationErrorChangeAction.Resolved, error);
        }
        #endregion
    }

    #region ValidationStatusChanged
    /// <summary>
    /// Used to specify what kind of validation change action took place.
    /// </summary>
    public enum ValidationErrorChangeAction {
        /// <summary>
        /// A new validation error is now present.
        /// </summary>
        Present,

        /// <summary>
        /// An existing validation error was resolved.
        /// </summary>
        Resolved
    }

    /// <summary>
    /// Contains details on validation status change events.
    /// </summary>
    public class ValidationStatusChangedEventArgs : EventArgs {
        /// <summary>
        /// Constructs a new ValidationChangedEventArgs.
        /// </summary>
        /// <param name="changeAction">The change action that took place.</param>
        /// <param name="error">The error this action applies to.</param>
        public ValidationStatusChangedEventArgs (ValidationErrorChangeAction changeAction, ValidationResult error) {
            ChangeAction = changeAction;
            Error = error;
        }

        #region Accessors
        /// <summary>
        /// The change action that took place.
        /// </summary>
        public ValidationErrorChangeAction ChangeAction { get; private set; }

        /// <summary>
        /// The error this action applies to.
        /// </summary>
        public ValidationResult Error { get; private set; }
        #endregion
    }
    #endregion
}
