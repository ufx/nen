using System;
using System.Collections.Generic;
using System.Text;

using Nen.ChangeTracking;

namespace Nen.Validation {
    /// <summary>
    /// IValidationTrackable objects can have their validation status tracked
    /// across state changes.
    /// </summary>
    public interface IValidationTrackable : IChangeTrackable, IValidatable {
    }
}
