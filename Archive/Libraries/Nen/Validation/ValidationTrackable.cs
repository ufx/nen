using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel.DataAnnotations;

using Nen.ChangeTracking;

namespace Nen.Validation {
    /// <summary>
    /// Implements the IValidationTrackable interface.
    /// </summary>
    public class ValidationTrackable : ChangeTrackable, IValidationTrackable {
        /// <summary>
        /// Uses Validator to validate the current object.
        /// </summary>
        /// <returns>The error collection returned by Validator.</returns>
        public virtual Collection<ValidationResult> Validate () {
            var results = new Collection<ValidationResult>();
            Validator.TryValidateObject(this, null, results);
            return results;
        }
    }
}
