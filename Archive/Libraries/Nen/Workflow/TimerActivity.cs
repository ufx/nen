﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Nen.ObjectModel;

namespace Nen.Workflow {
    [Serializable]
    public class TimerActivity : Activity {
        public static readonly NenProperty<TimeSpan> DurationProperty;
        
        [Local]
        public TimeSpan Duration {
            get { return (TimeSpan)GetValue(DurationProperty); }
            set { SetValue(DurationProperty, value); }
        }

        public override Continuation Execute (Continuation currentContinuation) {
            ActivityContext context = ActivityContext.CurrentContext;
            ContinuationHandle handle = context.CreateHandle(currentContinuation);
            Timer timer = null;

            timer = new Timer(s => {
                context.ResumeHandle(handle);
                timer.Dispose();
            }, null, (int)Duration.TotalMilliseconds, Timeout.Infinite);

            return BlockingDelimiter.Block(currentContinuation);
        }
    }
}
