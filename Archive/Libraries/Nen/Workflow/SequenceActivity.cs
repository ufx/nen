﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.ObjectModel;

namespace Nen.Workflow {
    [Serializable]
    public class SequenceActivity : Activity {
        public List<Activity> Children { get; private set; }
        [Local]
        private int _currentIndex;

        public SequenceActivity () {
            Children = new List<Activity>();
        }

        public override Continuation Execute (Continuation currentContinuation) {
            _currentIndex = -1;
            return currentContinuation.Push(NextIteration);
        }

        private Continuation NextIteration (Continuation currentContinuation) {
            _currentIndex++;
            if (_currentIndex < Children.Count)
                return currentContinuation.Push(NextIteration).Call(this, Children[_currentIndex]);
            else
                return currentContinuation;
        }
    }
}
