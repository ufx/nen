﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Nen.ObjectModel;

namespace Nen.Workflow {
    /// <summary>
    /// A handle for the continuation of an activity. Raw continuations
    /// should never be exposed outside the workflow system itself,
    /// as they may become invalid after a persistence event.
    /// 
    /// Activity handles may be created by calling ActivityContext.CreateHandle.
    /// </summary>
    [Serializable]
    public class ContinuationHandle {
        public Guid Id { get; private set; }
        public ActivityHandle Owner { get; set; }
        internal Continuation Continuation { get; private set; }

        internal ContinuationHandle (ActivityHandle owner, Continuation continuation) {
            Id = Guid.NewGuid();
            Owner = owner;
            Continuation = continuation;
        }

        public Activity Frame {
            get { return Continuation.Frame; }
        }
    }

    [Serializable]
    public class ActivityHandle {
        public Guid Id { get; private set; }
        public Activity Activity { get; private set; }
        internal int QueuedContinuations { get; set; }

        private Dictionary<Guid, ContinuationHandle> _continuations =
            new Dictionary<Guid, ContinuationHandle>();

        public ActivityHandle (Activity rootActivity) {
            Id = Guid.NewGuid();
            Activity = rootActivity;
        }

        internal ContinuationHandle CreateContinuationHandle (Continuation continuation) {
            var rootActivity = continuation.GetRootFrame();

            if (rootActivity != Activity)
                throw new ArgumentOutOfRangeException("continuation", "Continuation is not for this activity.");

            var handle = new ContinuationHandle(this, continuation);
            _continuations[handle.Id] = handle;
            return handle;
        }

        public ContinuationHandle GetContinuationHandle (Guid id) {
            return _continuations.ContainsKey(id) ? _continuations[id] : null;
        }

        internal void RemoveContinuationHandle (ContinuationHandle handle) {
            _continuations.Remove(handle.Id);
        }
    }

    public class ActivityEventArgs : EventArgs {
        public ActivityHandle Handle { get; set; }
    }

    /// <summary>
    /// The execution engine for a workflow. Takes over the thread it is run on
    /// and manages the scheduling of activity continuations.
    /// </summary>
    [Serializable]
    public class ActivityContext {
        private Queue<Continuation> _waitingContinuations = new Queue<Continuation>();
        private Dictionary<Activity, ActivityHandle> _handles = new Dictionary<Activity, ActivityHandle>();
        private Dictionary<Type, object> _services = new Dictionary<Type, object>();

        public event EventHandler<ActivityEventArgs> WorkflowStarted;
        public event EventHandler<ActivityEventArgs> WorkflowBlocked;
        public event EventHandler<ActivityEventArgs> WorkflowFinished;

        [ThreadStatic]
        private static ActivityContext _currentContext;

        public static ActivityContext CurrentContext {
            get { return _currentContext; }
        }

        /// <summary>
        /// Schedules a root activity.
        /// </summary>
        /// <param name="root">The root activity to start. The activity is cloned and scheduled for execution.</param>
        /// <returns>A new clone of the root activity, which is executed.</returns>
        public Activity Add (Activity root) {
            root.PrepareInternal(new StaticEnvironment { ActivityType = root.Class });

            var omega = new Continuation(null, null, null, null);
            var alpha = omega.Call(null, root);

            omega.Frame = alpha.Frame;
            omega.Delegate = alpha.Frame.FinishExecution;
            
            lock (_waitingContinuations) {
                _handles[alpha.Frame] = new ActivityHandle(alpha.Frame) { QueuedContinuations = 1 };
                Enqueue(alpha);
            }

            return alpha.Frame;
        }

        public void Start (Activity root) {
            Add(root);
            Run();
        }

        public void AddService<TInterface, TImpl> (TImpl service) where TImpl: class, TInterface {
            _services[typeof(TInterface)] = service;
        }

        public TInterface GetService<TInterface> () where TInterface: class {
            object iface;

            if (_services.TryGetValue(typeof(TInterface), out iface))
                return (TInterface)iface;

            return null;
        }

        internal void FinishExecution (Activity root) {
            lock (_waitingContinuations)
                _handles.Remove(root);
        }

        public void Run () {
            Run(false);
        }

        /// <summary>
        /// Begins executing scheduled activities. This method returns only when
        /// the root activity scheduled for this context is completed. To schedule
        /// activities for execution in a running context, use the ResumeHandle method.
        /// </summary>
        public void Run (bool returnOnSuspend) {
            ActivityContext parentContext = CurrentContext;
            try {
                _currentContext = this;

                while (true) {
                    Continuation currentContinuation = null;
                    ActivityHandle currentHandle = null;

                    lock (_waitingContinuations) {
                        if (_waitingContinuations.Count > 0) {
                            currentContinuation = _waitingContinuations.Dequeue();
                            currentHandle = _handles[currentContinuation.GetRootFrame()];
                            currentHandle.QueuedContinuations--;
                        } else if (_handles.Count == 0) {
                            return;
                        } else {
                            if (returnOnSuspend)
                                return;
                            else
                                Monitor.Wait(_waitingContinuations);
                        }
                    }

                    while (currentContinuation != null)
                        currentContinuation = currentContinuation.Delegate(currentContinuation.Next);

                    lock (_waitingContinuations) {
                        if (currentHandle != null && currentHandle.QueuedContinuations == 0) {
                            if (_handles.ContainsKey(currentHandle.Activity)) {
                                OnWorkflowBlocked(new ActivityEventArgs { Handle = currentHandle });
                            } else {
                                OnWorkflowFinished(new ActivityEventArgs { Handle = currentHandle });
                            }
                        }
                    }
                }
            } finally {
                _currentContext = parentContext;
            }
        }

        public ContinuationHandle CreateHandle (Continuation continuation) {
            if (CurrentContext != this)
                throw new InvalidOperationException("Continuation handles may only be created in the current activity context.");

            var rootFrame = continuation.GetRootFrame();

            lock (_waitingContinuations) {
                if (!_handles.ContainsKey(rootFrame))
                    throw new ArgumentOutOfRangeException("continuation",
                        "The continuation's root activity does not belong to this context.");

                var activityHandle = _handles[rootFrame];
                return activityHandle.CreateContinuationHandle(continuation);
            }
        }

        public void ResumeHandle (ContinuationHandle handle) {
            if (!_handles.ContainsKey(handle.Owner.Activity))
                _handles[handle.Owner.Activity] = handle.Owner;

            handle.Owner.RemoveContinuationHandle(handle);
            handle.Owner.QueuedContinuations++;
            Enqueue(handle.Continuation);
        }

        public ContinuationHandle GetHandle (Guid activityId, Guid continuationId) {
            var activityHandle = _handles.Values.FirstOrDefault(ah => ah.Id == activityId);

            return activityHandle != null ? activityHandle.GetContinuationHandle(continuationId) : null;
        }

        public void PurgeBlockedActivities () {
            lock (_waitingContinuations) {
                foreach (var blockedWorkflow in _handles.Values.Where(h => h.QueuedContinuations == 0).ToArray())
                    _handles.Remove(blockedWorkflow.Activity);
            }
        }

        private void Enqueue (Continuation continuation) {
            lock (_waitingContinuations) {
                _waitingContinuations.Enqueue(continuation);
                Monitor.Pulse(_waitingContinuations);
            }
        }

        protected void OnWorkflowStarted (ActivityEventArgs e) {
            var handler = WorkflowStarted;
            if (handler != null)
                handler(this, e);
        }

        protected void OnWorkflowBlocked (ActivityEventArgs e) {
            var handler = WorkflowBlocked;
            if (handler != null)
                handler(this, e);
        }

        protected void OnWorkflowFinished (ActivityEventArgs e) {
            var handler = WorkflowFinished;
            if (handler != null)
                handler(this, e);
        }
    }
}
