﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Nen.ObjectModel;
using Nen.Reflection;

namespace Nen.Workflow {
    /// <summary>
    /// The basic unit of work in a workflow. An activity roughly corresponds to
    /// a control structure, function or operator in other programming languages.
    /// 
    /// Activities perform work through one or more execution methods, which receive
    /// a Continuation object representing the next activity to execute in the control
    /// flow, and returning a Continuation which may either be the untouched continuation
    /// passed to the method, or, in the case of activities which modify the control flow,
    /// any other alternate continuation.
    /// 
    /// 
    /// </summary>
    [Serializable]
    public class Activity : NenObject {
        [Local]
        public Activity Caller { get; internal set; }
        public int ContextId { get; internal set; }

        private Activity _bindingContext;
        private Dictionary<int, Activity> _callContexts;

        public Activity BindingContext {
            get { return _bindingContext ?? Caller; }
            set { _bindingContext = value; }
        }

        public List<ActivityBinding> Bindings { get; private set; }
        public string Id { get; set; }

        internal void PrepareInternal (StaticEnvironment env) {
            Prepare(env);
        }

        protected virtual void Prepare (StaticEnvironment env) {
            for (Type t = this.GetType(); t != typeof(object); t = t.BaseType) {
                foreach (PropertyInfo pi in t.GetProperties(TypeEx.InstanceBindingFlags)) {
                    env.Variables[pi.Name] = new StaticEnvironmentEntry {
                        Property = pi
                    };
                }
            }

            List<Activity> childActivities = new List<Activity>();
            foreach (PropertyInfo pi in env.Variables.Values.Select(see => see.Property).ToArray()) {
                if (pi.GetAttribute<LocalAttribute>(false) != null)
                    continue;

                object value = pi.GetValue(this, null);

                if (value == null)
                    continue;

                if (pi.PropertyType == typeof(Activity)) {
                    Activity activity = (Activity)value;

                    env.Variables[pi.Name] = new StaticEnvironmentEntry {
                        Property = pi, ActivityType = activity.Class, ContextId = (activity.ContextId = ++env.TopContextId)
                    };

                    if (!string.IsNullOrEmpty(activity.Id))
                        env.Variables[activity.Id] = env.Variables[pi.Name];

                    childActivities.Add(activity);
                } else if (typeof(IList<Activity>).IsAssignableFrom(pi.PropertyType)) {
                    foreach (Activity activity in (IList<Activity>)value) {
                        activity.ContextId = ++env.TopContextId;

                        if (!string.IsNullOrEmpty(activity.Id)) {
                            env.Variables[activity.Id] = new StaticEnvironmentEntry {
                                Property = pi, ActivityType = activity.Class, ContextId = activity.ContextId
                            };
                        }

                        childActivities.Add(activity);
                    }
                }
            }

            foreach (Activity childActivity in childActivities)
                childActivity.Prepare(env.CreateChild(childActivity));

            foreach (ActivityBinding binding in Bindings) {
                binding.Compile(env);
                SetValue(Class.DeclaredProperties[binding.Name], binding);
            }
        }

        public Activity () {
            Bindings = new List<ActivityBinding>();
        }

        internal Continuation FinishExecution (Continuation currentContinuation) {
            ActivityContext.CurrentContext.FinishExecution(this);
            return null;
        }

        public virtual Continuation Execute (Continuation currentContinuation) {
            Execute();
            return currentContinuation;
        }

        public virtual void Execute () {
            // do nothing.
        }

        public void SetContext (int contextId, Activity activity) {
            if (_callContexts == null)
                _callContexts = new Dictionary<int, Activity>();

            _callContexts[contextId] = activity;
        }

        public Activity GetContext (int contextId) {
            if (_callContexts == null)
                return null;

            Activity context;
            _callContexts.TryGetValue(contextId, out context);
            return context;
        }
    }
}
