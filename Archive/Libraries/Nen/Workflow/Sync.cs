﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.ObjectModel;
using Nen.Collections;

namespace Nen.Workflow {
    public interface IEventData {
        object EventData { get; set; }
    }

    public interface ISyncStore {
        void AddListener (string eventName, Guid activityId, Guid continuationId);
        IEnumerable<Pair<Guid, Guid>> GetListeners (string eventName);
        void ResetListeners (string eventName);
    }

    public class MemorySyncStore : ISyncStore {
        private Dictionary<string, List<Pair<Guid, Guid>>> _listeners = new Dictionary<string, List<Pair<Guid, Guid>>>();

        #region ISyncStore Members

        public void AddListener (string eventName, Guid activityId, Guid continuationId) {
            _listeners.Activate(eventName).Add(Pair.Of(activityId, continuationId));
        }

        public IEnumerable<Pair<Guid, Guid>> GetListeners (string eventName) {
            List<Pair<Guid, Guid>> listeners = null;

            if (_listeners.TryGetValue(eventName, out listeners))
                return listeners;

            return Enumerable.Empty<Pair<Guid, Guid>>();
        }

        public void ResetListeners (string eventName) {
            _listeners.Remove(eventName);
        }

        #endregion
    }

    /// <summary>
    /// Low level event notification primitives.
    /// </summary>
    public class Sync {
        public static Continuation Wait<T> (string eventName, T notifyActivity, Continuation currentContinuation)
            where T : Activity, IEventData {

            // TODO: thread safety
            var syncStore = ActivityContext.CurrentContext.GetService<ISyncStore>();

            if (syncStore == null) {
                var memorySyncStore = new MemorySyncStore();
                ActivityContext.CurrentContext.AddService<ISyncStore, MemorySyncStore>(memorySyncStore);
                syncStore = memorySyncStore;
            }
                
            // TODO: this is ugly. Figure out a better way to pass information
            // back into an activity when resuming a continuation.
            var handle = ActivityContext.CurrentContext.CreateHandle(
                new Continuation(Continuation.Identity, notifyActivity, null, currentContinuation)
            );

            syncStore.AddListener(eventName, handle.Owner.Id, handle.Id);
            return BlockingDelimiter.Block(currentContinuation);
        }

        public static void Notify (ActivityContext ctx, string eventName, object data) {
            var syncStore = ctx.GetService<ISyncStore>();
            var store = ctx.GetService<ActivityStore>();

            if (syncStore == null)
                return;

            foreach (var handleIdPair in syncStore.GetListeners(eventName)) {
                var handle = ctx.GetHandle(handleIdPair.First, handleIdPair.Second);

                if (handle == null) {
                    if (store == null)
                        throw new InvalidOperationException(
                            string.Format("Tried to notify activity '{0}' of event '{1}' but it is not registered in the activity context and no activity store is available.",
                                handleIdPair.First, eventName));

                    handle = store.Get(handleIdPair.First).GetContinuationHandle(handleIdPair.Second);
                }

                ((IEventData)handle.Frame).EventData = data;
                ctx.ResumeHandle(handle);
            }

            syncStore.ResetListeners(eventName);
        }
    }

    [Serializable]
    public class WaitEventActivity : Activity, IEventData {
        [Local]
        public static NenProperty<string> EventNameProperty;

        [Local]
        public static NenProperty<object> EventDataProperty;

        public string EventName {
            get { return (string)GetValue(EventNameProperty); }
            set { SetValue(EventNameProperty, value); }
        }

        public object EventData {
            get { return GetValue(EventDataProperty); }
            set { SetValue(EventDataProperty, value); }
        }

        public override Continuation Execute (Continuation currentContinuation) {
            return Sync.Wait(EventName, this, currentContinuation);
        }
    }
}
