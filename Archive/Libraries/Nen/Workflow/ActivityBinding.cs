﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using Nen.ObjectModel;
using System.Runtime.Serialization;

namespace Nen.Workflow {
    [Serializable]
    public class ActivityBinding : NenBinding {
        public string Name { get; set; }
        public string Expr { get; set; }

        [NonSerialized]
        private Func<Activity, object> _accessor;

        internal Func<Activity, object> Accessor {
            get { return _accessor; }
            set { _accessor = value; }
        }

        [NonSerialized]
        private LambdaExpression _lambda;

        [NonSerialized]
        private Delegate _delegate;
        internal void Compile (StaticEnvironment env) {
            var bindingContext = Expression.Parameter(typeof(Activity), "bindingContext");
            Expression expr = bindingContext;
            var components = Expr.Split('.');
            var found = false;

            if (components.Length == 0)
                throw new InvalidOperationException("Invalid binding expression.");

            for (var currentInfo = env; currentInfo != null; currentInfo = currentInfo.Parent) {
                StaticEnvironmentEntry see;

                if (currentInfo.Variables.TryGetValue(components[0], out see)) {
                    if (see.ActivityType != null) {
                        expr = Expression.Convert(Expression.Call(expr, typeof(Activity).GetMethod("GetContext"), Expression.Constant(see.ContextId)), see.ActivityType.NearestPhysicalType);
                    } else {
                        expr = Expression.Property(expr, see.Property);
                    }
                    found = true;
                    break;
                } else if (currentInfo.Parent != null) {
                    expr = Expression.Convert(Expression.Property(expr, "BindingContext"), currentInfo.Parent.ActivityType.NearestPhysicalType);
                }
            }

            if (!found)
                throw new InvalidOperationException(string.Format("The variable '{0}' could not be found.", components[0]));

            for (var i = 1; i < components.Length; ++i)
                expr = Expression.PropertyOrField(expr, components[i]);

            expr = Expression.Convert(expr, typeof(object));
            _lambda = Expression.Lambda<Func<Activity, object>>(expr, bindingContext);
            _delegate = _lambda.Compile();
            Accessor = (Func<Activity, object>)_delegate;
        }

        public override object GetValue (NenObject bindingContext) {
            return Accessor((Activity)bindingContext);
        }

        public override void SetValue (NenObject bindingContext, object value) {
            throw new NotImplementedException();
        }
    }
}
