﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Nen.Workflow {
    public abstract class ActivityStore {
        public abstract void Put (ActivityHandle handle);
        public abstract ActivityHandle Get (Guid id);
        public abstract void Remove (ActivityHandle handle);

        public void Attach (ActivityContext context) {
            context.WorkflowBlocked += OnWorkflowBlocked;
            context.WorkflowFinished += OnWorkflowFinished;
            context.AddService<ActivityStore, ActivityStore>(this);
        }

        private void OnWorkflowBlocked (object sender, ActivityEventArgs e) {
            Put(e.Handle);
        }

        private void OnWorkflowFinished (object sender, ActivityEventArgs e) {
            Remove(e.Handle);
        }
    }

    public class FileActivityStore: ActivityStore {
        public string RootPath { get; private set; }

        public FileActivityStore (string rootPath) {
            RootPath = rootPath;
        }

        public override void Put (ActivityHandle handle) {
            var activityPath = RootPath + "\\" + handle.Id.ToString();
            var formatter = new BinaryFormatter();

            if (!Directory.Exists(RootPath))
                Directory.CreateDirectory(RootPath);

            using (var stream = new FileStream(activityPath, FileMode.Create))
                formatter.Serialize(stream, handle);
        }

        public override ActivityHandle Get (Guid id) {
            var activityPath = RootPath + "\\" + id.ToString();
            var formatter = new BinaryFormatter();

            using (var stream = new FileStream(activityPath, FileMode.Open)) {
                var handle = (ActivityHandle)formatter.Deserialize(stream);

                handle.Activity.PrepareInternal(new StaticEnvironment { ActivityType = handle.Activity.Class });
                return handle;
            }
        }

        public override void Remove (ActivityHandle handle) {
            var activityPath = RootPath + "\\" + handle.Id.ToString();

            if (File.Exists(activityPath))
                File.Delete(activityPath);
        }
    }
}
