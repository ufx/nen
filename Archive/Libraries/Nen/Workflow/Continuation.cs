﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.ObjectModel;
using System.Runtime.Serialization;

namespace Nen.Workflow {
    public delegate Continuation ContinuationDelegate (Continuation currentContinuation);

    [Serializable]
    public class Continuation : NenObject {
        public ContinuationDelegate Delegate { get; set; }
        public Continuation Next { get; set; }
        public object Delimiter { get; set; }
        public Activity Frame { get; set; }

        public Continuation (ContinuationDelegate cd, Activity frame, object delimiter, Continuation next) {
            Delegate = cd;
            Next = next;
            Delimiter = delimiter;
            Frame = frame;
        }

        public Continuation Push (ContinuationDelegate cd) {
            return new Continuation(cd, Frame, null, this);
        }

        public Continuation Push (ContinuationDelegate cd, object delimiter) {
            return new Continuation(cd, Frame, delimiter, this);
        }

        public Continuation Call (Activity caller, Activity activity) {
            var clone = (Activity)activity.Clone();
            clone.Caller = caller;

            if (activity.ContextId != 0)
                caller.SetContext(activity.ContextId, clone);

            return new Continuation(clone.Execute, clone, null, this);
        }

        public Activity GetRootFrame () {
            for (var frame = Frame; ; frame = frame.Caller)
                if (frame.Caller == null)
                    return frame;

            throw new InvalidOperationException("No root frame found.");
        }

        public static Continuation Identity (Continuation currentContinuation) {
            return currentContinuation;
        }

        public Continuation PushIdentity () {
            return Push(Identity);
        }

        public static Continuation Empty = new Continuation(Identity, null, null, null);
    }
}
