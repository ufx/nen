namespace Nen.Windows.Forms {
    partial class EditForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            if (disposing)
                BindingProvider.ControlDataBinder.ControlValidationStatusChanged -= ValidationRenderer.ControlValidationStatusChanged;

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this.components = new System.ComponentModel.Container();
            this.BindingProvider = new Nen.Windows.Forms.DataBinding.BindingProvider(this.components);
            this.ValidationRenderer = new Nen.Windows.Forms.Validation.ValidationRenderer(this.components);
            this.FatalErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.WarningErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize) (this.FatalErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.WarningErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // ValidationRenderer
            // 
            this.ValidationRenderer.FatalErrorProvider = this.FatalErrorProvider;
            this.ValidationRenderer.WarningErrorProvider = this.WarningErrorProvider;
            // 
            // FatalErrorProvider
            // 
            this.FatalErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.FatalErrorProvider.ContainerControl = this;
            // 
            // WarningErrorProvider
            // 
            this.WarningErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.WarningErrorProvider.ContainerControl = this;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Name = "EditForm";
            this.Text = "EditForm";
            ((System.ComponentModel.ISupportInitialize) (this.FatalErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.WarningErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Nen.Windows.Forms.DataBinding.BindingProvider BindingProvider;
        protected Nen.Windows.Forms.Validation.ValidationRenderer ValidationRenderer;
        protected System.Windows.Forms.ErrorProvider FatalErrorProvider;
        protected System.Windows.Forms.ErrorProvider WarningErrorProvider;

    }
}