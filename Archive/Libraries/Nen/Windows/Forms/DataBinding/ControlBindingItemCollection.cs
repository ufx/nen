#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Nen.ChangeTracking;
using Nen.DataBinding;

namespace Nen.Windows.Forms.DataBinding {
    public class ControlBindingItemCollection : BindingItemCollection {
        #region Add
        public void Add<TSource, TCollection> (TSource sourceDataSource, string sourcePropertyName, DataGridView destinationControl) where TSource : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new DataGridViewDataSourceBindingParameter<TCollection>(destinationControl);

            Add(new ScalarControlBindingItem(source, destination));
        }

        public void Add<TCollection> (ICollection<TCollection> sourceCollection, DataGridView destinationControl) {
            var source = new ValueBindingParameter(sourceCollection);
            var destination = new DataGridViewDataSourceBindingParameter<TCollection>(destinationControl);

            Add(new DataSourceControlBindingItem<DataGridView, TCollection>(source, destination));
        }

        public void Add<TSource> (TSource sourceDataSource, string sourcePropertyName, TextBoxBase destinationControl) where TSource : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new TextBoxBaseBindingParameter(destinationControl);

            Add(new ScalarControlBindingItem(source, destination));
        }

        public void Add<TSource> (TSource sourceDataSource, string sourcePropertyName, ToolStripLabel destinationComponent) where TSource : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new ToolStripLabelBindingParameter(destinationComponent);

            Add(source, destination);
        }

        public void Add<TSource> (TSource sourceDataSource, string sourcePropertyName, NumericUpDown destinationControl) where TSource : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new NumericUpDownBindingParameter(destinationControl);

            Add(new ScalarControlBindingItem(source, destination));
        }

        public void Add<TSource> (TSource sourceDataSource, string sourcePropertyName, DateTimePicker destinationControl) where TSource : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new DateTimePickerBindingParameter(destinationControl);

            Add(new ScalarControlBindingItem(source, destination));
        }

        public void Add<TSource> (TSource sourceDataSource, string sourcePropertyName, CheckBox destinationControl) where TSource : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new CheckBoxBindingParameter(destinationControl);

            Add(new ScalarControlBindingItem(source, destination));
        }

        public void Add<TSource> (TSource sourceDataSource, string sourcePropertyName, Label destinationControl) where TSource : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new LabelBindingParameter(destinationControl);

            Add(new ScalarControlBindingItem(source, destination));
        }

        public void Add<TSource, TControl, TValue> (TSource sourceDataSource, string sourcePropertyName, TControl destinationControl)
            where TSource : IChangeTrackable
            where TControl : Control, IBindableCompositeControl
            where TValue : class, IChangeTrackable, new() {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new BindableCompositeControlBindingParameter<TControl, TValue>(destinationControl);

            Add(new ScalarControlBindingItem(source, destination));
        }

        public void Add<TSource, TControl> (TSource sourceDataSource, string sourcePropertyName, TControl destinationControl)
            where TSource : IChangeTrackable
            where TControl : Control, IBindableScalarControl {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            var destination = new BindableScalarControlBindingParameter<TControl>(destinationControl);

            Add(new ScalarControlBindingItem(source, destination));
        }

        public void Add<TSource> (TSource sourceDataSource, string sourcePropertyName, ControlBindingParameter controlBindingParameter) where TSource : IChangeTrackable {
            var source = new PropertyBindingParameter<TSource>(sourceDataSource, sourcePropertyName);
            Add(new ScalarControlBindingItem(source, controlBindingParameter));
        }
        #endregion
    }
}
