#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Nen.Collections;
using Nen.DataBinding;
using Nen.ChangeTracking;
using Nen.Validation;

namespace Nen.Windows.Forms.DataBinding {
    public abstract class ControlBindingItem : BindingItemCore {
        #region Constructors
        protected ControlBindingItem (BindingParameter source, ControlBindingParameter destination)
            : base(source, destination) {
        }
        #endregion

        #region Dispose
        protected override void Dispose (bool disposing) {
            base.Dispose(disposing);

            if (!disposing || !IsSynchronized)
                return;

            StopValidationTracking();
        }
        #endregion

        #region Binding Queries
        public abstract bool IsBoundTo (object sourceValue, ICollection<string> sourceVariableNames);
        #endregion

        #region Synchronization
        public override void Synchronize () {
            if (IsSynchronized)
                return;

            base.Synchronize();

            // Hook into the base validation tracker.
            StartValidationTracking();
        }
        #endregion

        #region Validation Tracking
        protected virtual void StartValidationTracking () {
            var sourceValue = Source.DataSource as IValidationTrackable;
            if (sourceValue != null)
                DataBinder.ValidationTracker.StartTracking(sourceValue);
        }

        protected virtual void StopValidationTracking () {
            var sourceValue = Source.DataSource as IValidationTrackable;
            if (sourceValue != null)
                DataBinder.ValidationTracker.StopTracking(sourceValue);
        }
        #endregion

        #region Accessors
        public abstract ControlBindingParameter DestinationControlBindingParameter { get; }

        protected new ControlDataBinder DataBinder {
            get { return (ControlDataBinder) base.DataBinder; }
        }
        #endregion
    }

    public class ScalarControlBindingItem : ControlBindingItem {
        private ControlBindingParameter _destination;
        private PropertyBindingParameter _source;

        #region Constructors
        public ScalarControlBindingItem (PropertyBindingParameter source, ControlBindingParameter destination)
            : base(source, destination) {
            _destination = destination;
            _source = source;
        }
        #endregion

        #region Binding Queries
        public override bool IsBoundTo (object sourceValue, ICollection<string> sourceVariableNames) {
            if (_source.DataSource != sourceValue)
                return false;

            if (!sourceVariableNames.Contains(_source.Property.Name))
                return false;

            return true;
        }
        #endregion

        #region Synchronization
        public override void Synchronize () {
            if (IsSynchronized)
                return;

            // Configure validations on the control.
            _destination.ConfigureValidations(_source.Property.Member);

            base.Synchronize();
        }
        #endregion

        #region Accessors
        public override BindingParameter Destination {
            get { return _destination; }
        }

        public override ControlBindingParameter DestinationControlBindingParameter {
            get { return _destination; }
        }

        public override BindingParameter Source {
            get { return _source; }
        }
        #endregion
    }

    public class DataSourceControlBindingItem<TControl, TCollection> : ControlBindingItem where TControl : Control {
        private DataSourceControlBindingParameter<TControl, TCollection> _destination;
        private BindingParameter _source;

        #region Constructors
        public DataSourceControlBindingItem (BindingParameter source, DataSourceControlBindingParameter<TControl, TCollection> destination)
            : base(source, destination) {
            _destination = destination;
            _source = source;
        }
        #endregion

        #region Binding Queries
        public override bool IsBoundTo (object sourceValue, ICollection<string> sourceVariableNames) {
            if (_destination.BoundCollection == null || !(sourceValue is TCollection))
                return false;

            return _destination.BoundCollection.Contains((TCollection) sourceValue);
        }
        #endregion

        #region Validation Tracking
        private void DestinationBoundCollectionChanged (object sender, CollectionChangedEventArgs<TCollection> e) {
            var item = e.SpecificItem as IValidationTrackable;
            if (item == null)
                return;

            if (e.ChangeAction == CollectionChangeAction.Inserted)
                DataBinder.ValidationTracker.StartTracking(item);
            else if (e.ChangeAction == CollectionChangeAction.Removed)
                DataBinder.ValidationTracker.StopTracking(item);
        }

        protected override void StartValidationTracking () {
            base.StartValidationTracking();

            if (_destination.BoundCollection != null) {
                _destination.BoundCollection.Changed += DestinationBoundCollectionChanged;

                foreach (var item in _destination.BoundCollection.OfType<IValidationTrackable>())
                    DataBinder.ValidationTracker.StartTracking(item);
            }
        }

        protected override void StopValidationTracking () {
            base.StopValidationTracking();

            if (_destination.BoundCollection != null) {
                _destination.BoundCollection.Changed -= DestinationBoundCollectionChanged;

                foreach (var item in _destination.BoundCollection.OfType<IValidationTrackable>())
                    DataBinder.ValidationTracker.StopTracking(item);
            }
        }
        #endregion

        #region Accessors
        public override BindingParameter Destination {
            get { return _destination; }
        }

        public override ControlBindingParameter DestinationControlBindingParameter {
            get { return _destination; }
        }

        public override BindingParameter Source {
            get { return _source; }
        }
        #endregion
    }
}
