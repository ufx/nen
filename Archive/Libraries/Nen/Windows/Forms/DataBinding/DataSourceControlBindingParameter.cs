#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Nen.ChangeTracking;
using Nen.Collections;
using Nen.DataBinding;

namespace Nen.Windows.Forms.DataBinding {
    public abstract class DataSourceControlBindingParameter<TControl, TCollection> : ControlBindingParameter<TControl> where TControl : Control {
        #region Constructors
        protected DataSourceControlBindingParameter (TControl collectionControl)
            : base(collectionControl) {
        }
        #endregion

        #region Dispose
        protected override void Dispose (bool disposing) {
            base.Dispose(disposing);

            if (disposing)
                Unbind();
        }
        #endregion

        #region Tracking Events
        protected virtual void OnCollectionChanged (object sender, CollectionChangedEventArgs<TCollection> e) {
            var item = e.SpecificItem as IChangeTrackable;
            if (item == null)
                return;

            if (e.ChangeAction == Nen.Collections.CollectionChangeAction.Inserted)
                item.PropertyChanged += OnContainedItemPropertyChanged;
            else if (e.ChangeAction == Nen.Collections.CollectionChangeAction.Removed)
                item.PropertyChanged -= OnContainedItemPropertyChanged;
        }

        protected abstract void OnContainedItemPropertyChanged (object sender, PropertyChangeEventArgs e);
        #endregion

        #region Collection Binding
        protected virtual void Unbind () {
            if (BoundCollection == null)
                return;

            foreach (var trackableItem in BoundCollection.OfType<IChangeTrackable>())
                trackableItem.PropertyChanged -= OnContainedItemPropertyChanged;

            BoundCollection.Changed -= OnCollectionChanged;
            BoundCollection = null;
        }

        protected virtual void Bind (IObservableCollection<TCollection> collection) {
            Unbind();

            if (collection == null)
                return;

            BoundCollection = collection;
            BoundCollection.Changed += OnCollectionChanged;

            foreach (var trackableItem in BoundCollection.OfType<IChangeTrackable>())
                trackableItem.PropertyChanged += OnContainedItemPropertyChanged;
        }
        #endregion

        #region Accessors
        public IObservableCollection<TCollection> BoundCollection { get; private set; }
        #endregion
    }

    public class DataGridViewDataSourceBindingParameter<T> : DataSourceControlBindingParameter<DataGridView, T> {
        private BindingList<T> _bindingList;

        #region Constructors
        public DataGridViewDataSourceBindingParameter (DataGridView dataGridView)
            : base(dataGridView) {
        }
        #endregion

        #region DataSourceControlBindingParameter Members
        protected override void Bind (IObservableCollection<T> collection) {
            base.Bind(collection);

            if (BoundCollection != null)
                _bindingList = new BindingList<T>(new List<T>(collection));
        }

        protected override event EventHandler ControlValueChanged {
            add { SpecificControl.DataSourceChanged += value; }
            remove { SpecificControl.DataSourceChanged -= value; }
        }

        protected override void OnCollectionChanged (object sender, CollectionChangedEventArgs<T> e) {
            base.OnCollectionChanged(sender, e);

            if (e.ChangeAction == Nen.Collections.CollectionChangeAction.Inserted)
                _bindingList.Insert(e.Index, e.SpecificItem);
            else if (e.ChangeAction == Nen.Collections.CollectionChangeAction.Removed)
                _bindingList.RemoveAt(e.Index);
        }

        protected override void OnContainedItemPropertyChanged (object sender, PropertyChangeEventArgs e) {
            SpecificControl.RefreshCell(sender, e.Name);
        }

        protected override void Unbind () {
            base.Unbind();

            _bindingList = null;
        }

        protected override object ValueCore {
            get { return SpecificControl.DataSource; }
            set {
                Bind(value as IObservableCollection<T>);
                SpecificControl.DataSource = _bindingList;
            }
        }
        #endregion
    }
}
