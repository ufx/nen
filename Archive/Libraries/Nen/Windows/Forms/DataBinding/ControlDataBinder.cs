#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Nen.ChangeTracking;
using Nen.DataBinding;
using Nen.Validation;

namespace Nen.Windows.Forms.DataBinding {
    public class ControlDataBinder : DataBinder {
        private ValidationTracker _validationTracker = new ValidationTracker();

        #region Constructors
        public ControlDataBinder ()
            : base(new ControlBindingItemCollection()) {
            _validationTracker.ValidationChanged += ValidationChanged;
        }
        #endregion

        #region Dispose
        protected override void Dispose (bool disposing) {
            base.Dispose(disposing);

            if (disposing)
                _validationTracker.StopTrackingAll();
        }
        #endregion

        #region Synchronization
        public override void Synchronize () {
            base.Synchronize();

            _validationTracker.Refresh();
        }
        #endregion

        #region Validation Tracking
        public event EventHandler<ControlValidationStatusChangedEventArgs> ControlValidationStatusChanged;

        protected virtual void OnControlValidationStatusChanged (ValidationStatusChangedEventArgs e, Control boundControl) {
            if (ControlValidationStatusChanged != null)
                ControlValidationStatusChanged(this, new ControlValidationStatusChangedEventArgs(e.ChangeAction, e.Error, boundControl));
        }

        private void ValidationChanged (object sender, ValidationStatusChangedEventArgs e) {
            foreach (var controlBindingItem in Bindings.OfType<ControlBindingItem>()) {
                if (controlBindingItem.IsBoundTo(e.Error.Object, e.Error.Sources))
                    OnControlValidationStatusChanged(e, controlBindingItem.DestinationControlBindingParameter.Control);
            }
        }

        public ValidationTracker ValidationTracker {
            get { return _validationTracker; }
        }
        #endregion

        public new ControlBindingItemCollection Bindings {
            get { return (ControlBindingItemCollection) base.Bindings; }
        }
    }

    /// <summary>
    /// Contains details on bound control validation status change events.
    /// </summary>
    public class ControlValidationStatusChangedEventArgs : ValidationStatusChangedEventArgs {
        /// <summary>
        /// Constructs a new ControlValidationStatusChangedEventArgs.
        /// </summary>
        /// <param name="changeAction">The change action that took place.</param>
        /// <param name="error">The error this action applies to.</param>
        /// <param name="boundControl">The control bound to the source of this error.</param>
        public ControlValidationStatusChangedEventArgs (ValidationErrorChangeAction changeAction, ValidationError error, Control boundControl)
            : base(changeAction, error) {
            BoundControl = boundControl;
        }

        public Control BoundControl { get; private set; }
    }
}
