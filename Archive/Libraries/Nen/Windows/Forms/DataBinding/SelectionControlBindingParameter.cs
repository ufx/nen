#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Nen.Windows.Forms.DataBinding {
    public abstract class SelectionControlBindingParameter<T> : ControlBindingParameter<T> where T : Control {
        #region Constructors
        protected SelectionControlBindingParameter (T collectionControl)
            : base(collectionControl) {
        }
        #endregion
    }

    public class DataGridViewSelectionControlBindingParameter : SelectionControlBindingParameter<DataGridView> {
        private Dictionary<EventHandler<EventArgs>, DataGridViewCellEventHandler> _convertedChangeDelegates = new Dictionary<EventHandler<EventArgs>, DataGridViewCellEventHandler>();

        #region Constructors
        public DataGridViewSelectionControlBindingParameter (DataGridView dataGridView)
            : base(dataGridView) {
        }
        #endregion

        #region SelectionControlBindingParameter Members
        protected override event EventHandler ControlValueChanged {
            add {
                var manager = (CurrencyManager) SpecificControl.BindingContext[SpecificControl.DataSource];
                manager.CurrentChanged += value;
            }
            remove {
                var manager = (CurrencyManager) SpecificControl.BindingContext[SpecificControl.DataSource];
                manager.CurrentChanged -= value;
            }
        }

        protected override object ValueCore {
            get { return SpecificControl.GetCurrentRowItem(); }
            set {
                if (value == null) {
                    SpecificControl.CurrentCell = null;
                    return;
                }

                var rowIndex = SpecificControl.GetRowIndex(value);
                if (rowIndex == -1)
                    return;

                var row = SpecificControl.Rows[rowIndex];
                SpecificControl.CurrentCell = row.Cells[0];
            }
        }
        #endregion
    }
}
