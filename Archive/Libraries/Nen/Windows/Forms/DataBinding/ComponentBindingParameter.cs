#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

using Nen.DataBinding;

namespace Nen.Windows.Forms.DataBinding {
    public abstract class ComponentBindingParameter<T> : BindingParameter where T: Component {
        private Dictionary<EventHandler<EventArgs>, EventHandler> _convertedChangeDelegates = new Dictionary<EventHandler<EventArgs>, EventHandler>();

        #region Constructors
        protected ComponentBindingParameter (T component) {
            SpecificComponent = component;
        }
        #endregion

        #region ValueChanged Events
        protected abstract event EventHandler ComponentValueChanged;

        public override event EventHandler<EventArgs> ValueChanged {
            add {
                _convertedChangeDelegates.Add(value, new EventHandler(value));
                ComponentValueChanged += _convertedChangeDelegates[value];
            }
            remove {
                ComponentValueChanged -= _convertedChangeDelegates[value];
                _convertedChangeDelegates.Remove(value);
            }
        }
        #endregion

        #region Accessors
        public override object DataSource {
            get { return SpecificComponent; }
        }

        protected T SpecificComponent { get; private set; }
        #endregion
    }

    public class ToolStripLabelBindingParameter : ComponentBindingParameter<ToolStripLabel> {
        #region Constructors
        public ToolStripLabelBindingParameter (ToolStripLabel toolStripLabel)
            : base(toolStripLabel) {
        }
        #endregion

        #region Events
        protected override event EventHandler ComponentValueChanged {
            add { SpecificComponent.TextChanged += value; }
            remove { SpecificComponent.TextChanged -= value; }
        }
        #endregion

        #region Value Access
        protected override object ValueCore {
            get { return SpecificComponent.Text; }
            set { SpecificComponent.Text = (string) value; }
        }
        #endregion
    }
}
