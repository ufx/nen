#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using Nen.DataBinding;
using Nen.Reflection;
using Nen.ChangeTracking;
using Nen.Validation;

namespace Nen.Windows.Forms.DataBinding {
    public abstract class ControlBindingParameter : BindingParameter {
        #region Validation
        public virtual void ConfigureValidations (MemberInfo member) {
        }
        #endregion

        #region Accessors
        public abstract Control Control { get; }
        #endregion
    }

    public abstract class ControlBindingParameter<T> : ControlBindingParameter where T : Control {
        private Dictionary<EventHandler<EventArgs>, EventHandler> _convertedChangeDelegates = new Dictionary<EventHandler<EventArgs>, EventHandler>();

        #region Constructors
        protected ControlBindingParameter (T control) {
            SpecificControl = control;
        }
        #endregion

        #region ValueChanged Events
        protected abstract event EventHandler ControlValueChanged;

        public override event EventHandler<EventArgs> ValueChanged {
            add {
                _convertedChangeDelegates.Add(value, new EventHandler(value));
                ControlValueChanged += _convertedChangeDelegates[value];
            }
            remove {
                ControlValueChanged -= _convertedChangeDelegates[value];
                _convertedChangeDelegates.Remove(value);
            }
        }
        #endregion

        #region Accessors
        public override Control Control {
            get { return SpecificControl; }
        }

        public override object DataSource {
            get { return SpecificControl; }
        }

        protected T SpecificControl { get; private set; }
        #endregion
    }

    public class TextBoxBaseBindingParameter : ControlBindingParameter<TextBoxBase> {
        #region Constructors
        public TextBoxBaseBindingParameter (TextBoxBase textBox)
            : base(textBox) {
        }
        #endregion

        #region ControlBindingParameter Members
        protected override event EventHandler ControlValueChanged {
            add { SpecificControl.TextChanged += value; }
            remove { SpecificControl.TextChanged -= value; }
        }

        protected override object ValueCore {
            get { return SpecificControl.Text; }
            set { SpecificControl.Text = (string) value; }
        }

        public override void ConfigureValidations (MemberInfo member) {
            var maximumLength = member.GetAttribute<MaximumLengthAttribute>(true);
            if (maximumLength != null)
                SpecificControl.MaxLength = maximumLength.Value;
        }
        #endregion
    }

    public class NumericUpDownBindingParameter : ControlBindingParameter<NumericUpDown> {
        #region Constructors
        public NumericUpDownBindingParameter (NumericUpDown numericUpDown)
            : base(numericUpDown) {
        }
        #endregion

        #region ControlBindingParameter Members
        protected override event EventHandler ControlValueChanged {
            add { SpecificControl.ValueChanged += value; }
            remove { SpecificControl.ValueChanged -= value; }
        }

        protected override object ValueCore {
            get { return SpecificControl.Value; }
            set { SpecificControl.Value = (decimal) ConvertEx.ChangeWeakType(value, typeof(decimal)); }
        }

        public override void ConfigureValidations (MemberInfo member) {
            var maximumValue = member.GetAttribute<MaximumValueAttribute>(true);
            if (maximumValue != null)
                SpecificControl.Maximum = Convert.ToDecimal(maximumValue.Value, CultureInfo.InvariantCulture);

            var minimumValue = member.GetAttribute<MinimumValueAttribute>(true);
            if (minimumValue != null)
                SpecificControl.Minimum = Convert.ToDecimal(minimumValue.Value, CultureInfo.InvariantCulture);
        }
        #endregion
    }

    public class DateTimePickerBindingParameter : ControlBindingParameter<DateTimePicker> {
        #region Constructors
        public DateTimePickerBindingParameter (DateTimePicker dateTimePicker)
            : base(dateTimePicker) {
        }
        #endregion

        #region ControlBindingParameter Members
        protected override event EventHandler ControlValueChanged {
            add { SpecificControl.ValueChanged += value; }
            remove { SpecificControl.ValueChanged -= value; }
        }

        protected override object ValueCore {
            get { return SpecificControl.Value; }
            set { SpecificControl.Value = Convert.ToDateTime(value, CultureInfo.CurrentCulture); }
        }

        public override void ConfigureValidations (MemberInfo member) {
            var maximumValue = member.GetAttribute<MaximumValueAttribute>(true);
            if (maximumValue != null)
                SpecificControl.MaxDate = Convert.ToDateTime(maximumValue.Value, CultureInfo.InvariantCulture);

            var minimumValue = member.GetAttribute<MinimumValueAttribute>(true);
            if (minimumValue != null)
                SpecificControl.MinDate = Convert.ToDateTime(minimumValue.Value, CultureInfo.InvariantCulture);
        }
        #endregion
    }

    public class CheckBoxBindingParameter : ControlBindingParameter<CheckBox> {
        #region Constructors
        public CheckBoxBindingParameter (CheckBox checkBox)
            : base(checkBox) {
        }
        #endregion

        #region ControlBindingParameter Members
        protected override event EventHandler ControlValueChanged {
            add { SpecificControl.CheckedChanged += value; }
            remove { SpecificControl.CheckedChanged -= value; }
        }

        protected override object ValueCore {
            get { return SpecificControl.Checked; }
            set { SpecificControl.Checked = Convert.ToBoolean(value, CultureInfo.CurrentCulture); }
        }
        #endregion
    }

    public class LabelBindingParameter : ControlBindingParameter<Label> {
        #region Constructors
        public LabelBindingParameter (Label label)
            : base(label) {
        }
        #endregion

        #region ControlBindingParameter Members
        protected override event EventHandler ControlValueChanged {
            add { SpecificControl.TextChanged += value; }
            remove { SpecificControl.TextChanged -= value; }
        }

        protected override object ValueCore {
            get { return SpecificControl.Text; }
            set { SpecificControl.Text = Convert.ToString(value, CultureInfo.CurrentCulture); }
        }
        #endregion
    }

    public class BindableCompositeControlBindingParameter<TControl, TValue> : ControlBindingParameter<TControl>
        where TControl : Control, IBindableCompositeControl
        where TValue : class, IChangeTrackable, new() {
        private TValue _newValue;

        #region Constructors
        public BindableCompositeControlBindingParameter (TControl bindableControl)
            : base(bindableControl) {
        }
        #endregion

        #region Dispose
        protected override void Dispose (bool disposing) {
            base.Dispose(disposing);

            if (disposing && _newValue != null)
                _newValue.PropertyChanged -= NewValuePropertyChanged;
        }
        #endregion

        #region ControlBindingParameter Members
        protected override event EventHandler ControlValueChanged;

        protected override object ValueCore {
            get { return SpecificControl.Value; }
            set {
                // fixme: resolve futures, allow bindable controls
                // to accept bindings to futures directly, or resolve
                // them automatically if destination type is not a future?

                if (value == null) {
                    if (_newValue != null)
                        _newValue.PropertyChanged -= NewValuePropertyChanged;

                    _newValue = new TValue();
                    _newValue.PropertyChanged += NewValuePropertyChanged;
                    SpecificControl.Value = _newValue;
                } else
                    SpecificControl.Value = (IChangeTrackable) value;
            }
        }

        private void NewValuePropertyChanged (object sender, PropertyChangeEventArgs e) {
            _newValue.PropertyChanged -= NewValuePropertyChanged;
            _newValue = default(TValue);

            // Fire this event once as soon as the value is changed from its
            // defaults.
            if (ControlValueChanged != null)
                ControlValueChanged(this, EventArgs.Empty);
        }
        #endregion
    }

    public static class BindableCompositeControlBindingParameter {
        public static ControlBindingParameter Create (IBindableCompositeControl control) {
            if (control == null)
                throw new ArgumentNullException("control");

            var genericArguments = new Type[] { control.GetType(), control.ValueType };
            return (ControlBindingParameter) ActivatorEx.CreateGenericInstance(typeof(BindableCompositeControlBindingParameter<,>), genericArguments, control);
        }
    }

    public class BindableScalarControlBindingParameter<TControl> : ControlBindingParameter<TControl>
        where TControl : Control, IBindableScalarControl {
        #region Constructors
        public BindableScalarControlBindingParameter (TControl bindableControl)
            : base(bindableControl) {
        }
        #endregion

        #region ControlBindingParameter Members
        protected override event EventHandler ControlValueChanged {
            add { SpecificControl.ValueChanged += value; }
            remove { SpecificControl.ValueChanged -= value; }
        }

        protected override object ValueCore {
            get { return SpecificControl.Value; }
            set { SpecificControl.Value = value; }
        }
        #endregion
    }

    public static class BindableScalarControlBindingParameter {
        public static ControlBindingParameter Create (IBindableScalarControl control) {
            if (control == null)
                throw new ArgumentNullException("control");

            var genericArguments = new Type[] { control.GetType() };
            return (ControlBindingParameter) ActivatorEx.CreateGenericInstance(typeof(BindableScalarControlBindingParameter<>), genericArguments, control);
        }
    }
}
