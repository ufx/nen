#pragma warning disable 1591

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

using Nen.ChangeTracking;
using Nen.Collections;

namespace Nen.Windows.Forms.DataBinding {
    [ProvideProperty("CheckBoxProperty", typeof(CheckBox))]
    [ProvideProperty("BindableCompositeControlProperty", typeof(IBindableCompositeControl))]
    [ProvideProperty("BindableScalarControlProperty", typeof(IBindableScalarControl))]
    [ProvideProperty("DataGridViewDataSource", typeof(DataGridView))]
    [ProvideProperty("DataGridViewSelection", typeof(DataGridView))]
    [ProvideProperty("DateTimePickerProperty", typeof(DateTimePicker))]
    [ProvideProperty("LabelProperty", typeof(Label))]
    [ProvideProperty("NumericUpDownProperty", typeof(NumericUpDown))]
    [ProvideProperty("TextBoxProperty", typeof(TextBoxBase))]
    public partial class BindingProvider : Component, IExtenderProvider {
        private HashSet<Control> _boundCollections = new HashSet<Control>();
        private Dictionary<Component, BoundProperty> _boundProperties = new Dictionary<Component, BoundProperty>();
        private Dictionary<Control, BoundSelection> _boundSelections = new Dictionary<Control, BoundSelection>();

        private class BoundProperty {
            public string PropertyName;
            public ControlBindingParameter ControlBindingParameter;

            public BoundProperty (string propertyName, ControlBindingParameter controlBindingParameter) {
                PropertyName = propertyName;
                ControlBindingParameter = controlBindingParameter;
            }
        }

        private class BoundSelection {
            public ControlBindingParameter Source;
            public ControlBindingParameter Destination;

            public BoundSelection (ControlBindingParameter source, ControlBindingParameter destination) {
                Source = source;
                Destination = destination;
            }
        }

        #region Constructors
        public BindingProvider () {
            InitializeComponent();
        }

        public BindingProvider (IContainer container) {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region Property Extension
        [Category("DataBinding"), DefaultValue(null)]
        private string GetPropertyName (Component component) {
            return _boundProperties.ContainsKey(component) ? _boundProperties[component].PropertyName : null;
        }

        public void SetCheckBoxProperty (CheckBox component, string value) {
            _boundProperties[component] = new BoundProperty(value, new CheckBoxBindingParameter(component));
        }

        [Category("DataBinding"), DefaultValue(null)]
        public string GetCheckBoxProperty (CheckBox component) {
            return GetPropertyName(component);
        }

        public void SetLabelProperty (Label component, string value) {
            _boundProperties[component] = new BoundProperty(value, new LabelBindingParameter(component));
        }

        [Category("DataBinding"), DefaultValue(null)]
        public string GetLabelProperty (Label component) {
            return GetPropertyName(component);
        }

        public void SetTextBoxProperty (TextBoxBase component, string value) {
            _boundProperties[component] = new BoundProperty(value, new TextBoxBaseBindingParameter(component));
        }

        [Category("DataBinding"), DefaultValue(null)]
        public string GetTextBoxProperty (TextBoxBase component) {
            return GetPropertyName(component);
        }

        public void SetNumericUpDownProperty (NumericUpDown component, string value) {
            _boundProperties[component] = new BoundProperty(value, new NumericUpDownBindingParameter(component));
        }

        [Category("DataBinding"), DefaultValue(null)]
        public string GetNumericUpDownProperty (NumericUpDown component) {
            return GetPropertyName(component);
        }

        public void SetDateTimePickerProperty (DateTimePicker component, string value) {
            if (component is IBindableScalarControl)
                SetBindableScalarControlProperty((IBindableScalarControl) component, value);
            else
                _boundProperties[component] = new BoundProperty(value, new DateTimePickerBindingParameter(component));
        }

        [Category("DataBinding"), DefaultValue(null)]
        public string GetDateTimePickerProperty (DateTimePicker component) {
            return GetPropertyName(component);
        }

        public void SetBindableCompositeControlProperty (IBindableCompositeControl component, string value) {
            var parameter = BindableCompositeControlBindingParameter.Create(component);
            _boundProperties[(Component) component] = new BoundProperty(value, parameter);
        }

        [Category("DataBinding"), DefaultValue(null)]
        public string GetBindableCompositeControlProperty (IBindableCompositeControl component) {
            return GetPropertyName((Component) component);
        }

        public void SetBindableScalarControlProperty (IBindableScalarControl component, string value) {
            var parameter = BindableScalarControlBindingParameter.Create(component);
            _boundProperties[(Component) component] = new BoundProperty(value, parameter);
        }

        [Category("DataBinding"), DefaultValue(null)]
        public string GetBindableScalarControlProperty (IBindableScalarControl component) {
            return GetPropertyName((Component) component);
        }
        #endregion

        #region DataSource Extension
        [Category("DataBinding"), DefaultValue(false)]
        public bool GetDataGridViewDataSource (DataGridView control) {
            return _boundCollections.Contains(control);
        }

        public void SetDataGridViewDataSource (DataGridView control, bool value) {
            if (value)
                _boundCollections.Add(control);
            else
                _boundCollections.Remove(control);
        }
        #endregion

        #region Selection Extension
        [Category("DataBinding"), DefaultValue(null)]
        public IBindableCompositeControl GetDataGridViewSelection (DataGridView control) {
            return _boundSelections.ContainsKey(control) ? (IBindableCompositeControl) _boundSelections[control].Destination.Control : null;
        }

        public void SetDataGridViewSelection (DataGridView control, IBindableCompositeControl selectionDestination) {
            var destination = BindableCompositeControlBindingParameter.Create(selectionDestination);
            _boundSelections[control] = new BoundSelection(new DataGridViewSelectionControlBindingParameter(control), destination);
        }
        #endregion

        #region IExtenderProvider Members
        public bool CanExtend (object extendee) {
            if (extendee is TextBoxBase
                || extendee is NumericUpDown
                || extendee is DateTimePicker
                || extendee is DataGridView
                || extendee is CheckBox
                || extendee is Label
                || extendee is IBindableCompositeControl
                || extendee is IBindableScalarControl)
                return true;

            return false;
        }
        #endregion

        #region Synchronization
        public void SynchronizeProperties<T> (T source) where T : IChangeTrackable {
            SynchronizeProperties(source, true);
        }

        public void SynchronizeProperties<T> (T source, bool clearBindings) where T : IChangeTrackable {
            if (clearBindings)
                ControlDataBinder.Bindings.Clear();

            SynchronizeSelectionsCore();

            foreach (var pair in _boundProperties) {
                if (pair.Value == null)
                    throw new NotImplementedException("Invalid component type encountered.");

                ControlDataBinder.Bindings.Add(source, pair.Value.PropertyName, pair.Value.ControlBindingParameter);
            }

            ControlDataBinder.Synchronize();
        }

        public void SynchronizeDataSource<T> (ICollection<T> dataSource) {
            SynchronizeDataSource(dataSource, true);
        }

        public void SynchronizeDataSource<T> (ICollection<T> dataSource, bool clearBindings) {
            if (clearBindings)
                ControlDataBinder.Bindings.Clear();

            foreach (var control in _boundCollections) {
                if (control is DataGridView)
                    ControlDataBinder.Bindings.Add(dataSource, (DataGridView) control);
                else
                    throw new InvalidOperationException("Invalid collection binding control type encountered.");
            }

            SynchronizeSelectionsCore();

            ControlDataBinder.Synchronize();
        }

        public void SynchronizeSelections () {
            SynchronizeSelections(true);
        }

        public void SynchronizeSelections (bool clearBindings) {
            if (clearBindings)
                ControlDataBinder.Bindings.Clear();

            SynchronizeSelectionsCore();

            ControlDataBinder.Synchronize();
        }

        private void SynchronizeSelectionsCore () {
            foreach (var boundSelection in _boundSelections.Values)
                ControlDataBinder.Bindings.Add(boundSelection.Source, boundSelection.Destination);
        }
        #endregion

        #region Accessors
        [Browsable(false)]
        public ControlDataBinder ControlDataBinder { get; private set; }
        #endregion
    }
}
