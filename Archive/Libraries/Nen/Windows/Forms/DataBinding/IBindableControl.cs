#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Text;

using Nen.ChangeTracking;

namespace Nen.Windows.Forms.DataBinding {
    public interface IBindableCompositeControl {
        IChangeTrackable Value { get; set; }
        Type ValueType { get; }
    }

    public interface IBindableScalarControl {
        object Value { get; set; }
        event EventHandler ValueChanged;
    }
}
