#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Nen.Windows.Forms {
    public partial class EditForm : Form {
        public EditForm () {
            InitializeComponent();

            BindingProvider.ControlDataBinder.ControlValidationStatusChanged += ValidationRenderer.ControlValidationStatusChanged;
        }
    }
}