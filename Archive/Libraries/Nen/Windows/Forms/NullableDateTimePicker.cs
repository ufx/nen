using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

using Nen.Windows.Forms.DataBinding;

namespace Nen.Windows.Forms {
    public class NullableDateTimePicker : DateTimePicker, IBindableScalarControl {
        private const string _defaultNullDateSelectedFormat = "'No date selected.'";

        private string _dateCustomFormat = "";

        #region Constructors
        public NullableDateTimePicker () {
            base.ShowCheckBox = true;
            base.Checked = false;
            DateFormat = DateTimePickerFormat.Long;
            NullDateSelectedFormat = _defaultNullDateSelectedFormat;

            SelectNullValue();
        }
        #endregion

        #region Value Management
        protected override void OnValueChanged (EventArgs eventargs) {
            base.OnValueChanged(eventargs);

            if (Checked)
                SelectNonNullValue();
            else
                SelectNullValue();
        }

        private void SelectNullValue () {
            CustomFormat = NullDateSelectedFormat;
            Format = DateTimePickerFormat.Custom;
        }

        private void SelectNonNullValue () {
            CustomFormat = _dateCustomFormat;
            Format = DateFormat;
        }
        #endregion

        #region Accessors
        [Browsable(false), DefaultValue(false)]
        public new bool Checked {
            get { return base.Checked; }
            set { base.Checked = value; }
        }

        [Browsable(false), DefaultValue(_defaultNullDateSelectedFormat)]
        public new string CustomFormat {
            get { return base.CustomFormat; }
            set { base.CustomFormat = value; }
        }

        [Category("Behavior"), DefaultValue("")]
        public string DateCustomFormat { get; set; }

        [Category("Behavior"), DefaultValue(DateTimePickerFormat.Long)]
        public DateTimePickerFormat DateFormat { get; set; }

        [Browsable(false), DefaultValue(DateTimePickerFormat.Custom)]
        public new DateTimePickerFormat Format {
            get { return base.Format; }
            set { base.Format = value; }
        }

        [Category("Behavior"), DefaultValue(_defaultNullDateSelectedFormat)]
        public string NullDateSelectedFormat { get; set; }

        [Browsable(false), DefaultValue(true)]
        public new bool ShowCheckBox {
            get { return base.ShowCheckBox; }
            set { base.ShowCheckBox = value; }
        }

        [Browsable(false), DefaultValue(null), RefreshProperties(RefreshProperties.All)]
        public new DateTime? Value {
            get { return Checked ? (DateTime?) base.Value : null; }
            set {
                if (value == null) {
                    Checked = false;
                    SelectNullValue();
                } else {
                    Checked = true;
                    base.Value = value.Value;
                    SelectNonNullValue();
                }
            }
        }
        #endregion

        #region IBindableScalarControl Members
        object IBindableScalarControl.Value {
            get { return Value; }
            set { Value = (DateTime?) value; }
        }

        event EventHandler IBindableScalarControl.ValueChanged {
            add { ValueChanged += value; }
            remove { ValueChanged -= value; }
        }
        #endregion
    }
}
