using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

using Nen.Collections;
using Nen.Validation;
using Nen.Windows.Forms.DataBinding;

namespace Nen.Windows.Forms.Validation {
    /// <summary>
    /// Correlates validation errors with controls and renders notifications
    /// for them.
    /// </summary>
    public partial class ValidationRenderer : Component {
        private Dictionary<Control, List<ValidationError>> _controlErrors = new Dictionary<Control, List<ValidationError>>();

        #region Constructors
        /// <summary>
        /// Constructs a new ValidationRenderer.
        /// </summary>
        public ValidationRenderer () {
            InitializeComponent();
        }

        /// <summary>
        /// Constructs a new ValidationRenderer.
        /// </summary>
        /// <param name="container">The container of this renderer.</param>
        public ValidationRenderer (IContainer container) {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region Error Management
        /// <summary>
        /// Clears all validation errors.
        /// </summary>
        public void Clear () {
            _controlErrors.Clear();

            if (FatalErrorProvider != null)
                FatalErrorProvider.Clear();

            if (WarningErrorProvider != null)
                WarningErrorProvider.Clear();
        }

        /// <summary>
        /// Manages a change in validation status.  This method is usually
        /// listening to a ControlDataBinder's ControlValidationStatusChanged
        /// event.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The validation status change event arguments.</param>
        public void ControlValidationStatusChanged (object sender, ControlValidationStatusChangedEventArgs e) {
            var errors = _controlErrors.Activate(e.BoundControl);

            if (e.ChangeAction == ValidationErrorChangeAction.Present)
                errors.Add(e.Error);
            else
                errors.Remove(e.Error);

            RenderErrors(e.BoundControl, errors);
        }
        #endregion

        #region Rendering
        private void RenderErrors (Control control, List<ValidationError> errors) {
            if (control is DataGridView)
                RenderGridErrors((DataGridView) control, errors);
            else
                RenderScalarErrors(control, errors);
        }

        private static void RenderGridErrors (DataGridView dataGridView, List<ValidationError> errors) {
            dataGridView.ClearErrors();

            // Render existing errors.
            foreach (var error in errors) {
                int rowIndex = dataGridView.GetRowIndex(error.Object);
                if (rowIndex == -1)
                    continue;

                var row = dataGridView.Rows[rowIndex];

                foreach (var source in error.Sources) {
                    int cellIndex = dataGridView.GetColumnIndex(source);
                    if (cellIndex == -1)
                        continue;

                    var cell = row.Cells[cellIndex];

                    var text = new StringBuilder(cell.ErrorText);
                    if (text.Length > 0)
                        text.Append("\n");
                    text.Append(GetErrorText(error));

                    cell.ErrorText = text.ToString();
                }
            }
        }

        private void RenderScalarErrors (Control control, List<ValidationError> errors) {
            ValidationErrorSeverity maxSeverity;
            var errorText = GetErrorText(errors, out maxSeverity);

            if (WarningErrorProvider != null) {
                if (maxSeverity == ValidationErrorSeverity.Warning)
                    WarningErrorProvider.SetError(control, errorText);
                else
                    WarningErrorProvider.SetError(control, "");
            }

            if (FatalErrorProvider != null) {
                if (maxSeverity == ValidationErrorSeverity.Fatal)
                    FatalErrorProvider.SetError(control, errorText);
                else
                    FatalErrorProvider.SetError(control, "");
            }
        }

        private static string GetErrorText (List<ValidationError> errors, out ValidationErrorSeverity maxSeverity) {
            maxSeverity = ValidationErrorSeverity.Warning;

            if (errors.Count == 0)
                return "";

            var text = new StringBuilder();
            foreach (var error in errors) {
                if (text.Length > 0)
                    text.Append("\n");

                text.Append(GetErrorText(error));

                if (error.Severity == ValidationErrorSeverity.Fatal)
                    maxSeverity = ValidationErrorSeverity.Fatal;
            }

            return text.ToString();
        }

        private static string GetErrorText (ValidationError error) {
            return string.Format(CultureInfo.InvariantCulture, "{0}: {1}", error.Severity, error.Reason);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the error provider for fatal errors.
        /// </summary>
        [Category("Appearance"), DefaultValue(null)]
        public ErrorProvider FatalErrorProvider { get; set; }

        /// <summary>
        /// Gets or sets the error provider for warning errors.
        /// </summary>
        [Category("Appearance"), DefaultValue(null)]
        public ErrorProvider WarningErrorProvider { get; set; }
        #endregion
    }
}
