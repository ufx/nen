using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Nen.Windows.Forms {
    /// <summary>
    /// Extensions to the System.Windows.Forms.DataGridView class.
    /// </summary>
    public static class DataGridViewEx {
        #region Bound Item Retrieval
        /// <summary>
        /// Retrieves the current row item.
        /// </summary>
        /// <param name="dataGridView">The dataGridView to retrieve the current row item from.</param>
        /// <returns>The current row, or null.</returns>
        public static object GetCurrentRowItem (this DataGridView dataGridView) {
            if (dataGridView == null)
                throw new ArgumentNullException("dataGridView");

            // dataGridView.CurrentRow.DataBoundItem spectacularly fails when
            // there are no items in the list.

            return dataGridView.GetCurrent(dataGridView.DataSource);
        }
        #endregion

        #region Display Refreshing
        /// <summary>
        /// Refreshes data in the grid.
        /// </summary>
        /// <param name="dataGridView">The dataGridView to refresh.</param>
        public static void RefreshDataSource (this DataGridView dataGridView) {
            if (dataGridView == null)
                throw new ArgumentNullException("dataGridView");

            dataGridView.Refresh(dataGridView.DataSource);
        }

        /// <summary>
        /// Refreshes data in a single cell.
        /// </summary>
        /// <param name="dataGridView">The dataGridView to refresh.</param>
        /// <param name="rowSource">The row to refresh.</param>
        /// <param name="dataPropertyName">The property name of the column to refresh.</param>
        public static void RefreshCell (this DataGridView dataGridView, object rowSource, string dataPropertyName) {
            if (dataGridView == null)
                throw new ArgumentNullException("dataGridView");

            var rowIndex = dataGridView.GetRowIndex(rowSource);
            if (rowIndex == -1)
                return;

            var columnIndex = dataGridView.GetColumnIndex(dataPropertyName);
            if (columnIndex == -1)
                return;

            dataGridView.InvalidateCell(columnIndex, rowIndex);
        }
        #endregion

        #region Index Retrieval
        /// <summary>
        /// Retrieves the row index of a data bound item.
        /// </summary>
        /// <param name="dataGridView">The dataGridView to retrieve a row index for.</param>
        /// <param name="rowSource">The data bound row item to retrieve a row index for.</param>
        /// <returns>The index of the row source, or -1.</returns>
        public static int GetRowIndex (this DataGridView dataGridView, object rowSource) {
            if (dataGridView == null)
                throw new ArgumentNullException("dataGridView");

            var manager = (CurrencyManager) dataGridView.BindingContext[dataGridView.DataSource];
            for (var i = 0; i < manager.List.Count; i++) {
                if (manager.List[i] == rowSource)
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Retrieves the column index of a property name.
        /// </summary>
        /// <param name="dataGridView">The dataGridView to retrieve a column index for.</param>
        /// <param name="dataPropertyName">The property name of the column to retrieve a column index for.</param>
        /// <returns>The index of the property, or -1.</returns>
        public static int GetColumnIndex (this DataGridView dataGridView, string dataPropertyName) {
            if (dataGridView == null)
                throw new ArgumentNullException("dataGridView");

            for (var i = 0; i < dataGridView.Columns.Count; i++) {
                if (dataGridView.Columns[i].DataPropertyName == dataPropertyName)
                    return i;
            }

            return -1;
        }
        #endregion

        #region Errors
        /// <summary>
        /// Clears all error text in the grid.
        /// </summary>
        /// <param name="dataGridView">The dataGridView to clear errors from.</param>
        public static void ClearErrors (this DataGridView dataGridView) {
            if (dataGridView == null)
                throw new ArgumentNullException("dataGridView");

            foreach (DataGridViewRow row in dataGridView.Rows) {
                foreach (DataGridViewCell cell in row.Cells)
                    cell.ErrorText = "";
                row.ErrorText = "";
            }
        }
        #endregion
    }
}
