using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Nen.Windows.Forms {
    /// <summary>
    /// Extensions to the System.Windows.Forms.IBindableComponent clsses.
    /// </summary>
    public static class BindableComponentEx {
        /// <summary>
        /// Refreshes the CurrencyManager associated with the component data source.
        /// </summary>
        /// <param name="component">The component to refresh.</param>
        /// <param name="dataSource">The data source on the component to refresh.</param>
        public static void Refresh (this IBindableComponent component, object dataSource) {
            if (component == null)
                throw new ArgumentNullException("component");

            // Nothing to refresh.
            if (dataSource == null)
                return;

            var manager = (CurrencyManager) component.BindingContext[dataSource];
            if (manager == null)
                return;

            manager.Refresh();
        }

        /// <summary>
        /// Retrieves the current object with the component data source.
        /// </summary>
        /// <param name="component">The component that has selected the object.</param>
        /// <param name="dataSource">The data source on the component that has selected the object.</param>
        /// <returns>The current object, or null.</returns>
        public static object GetCurrent (this IBindableComponent component, object dataSource) {
            if (component == null)
                throw new ArgumentNullException("component");

            // Nothing to retrieve.
            if (dataSource == null)
                return null;

            var manager = (CurrencyManager) component.BindingContext[dataSource];
            if (manager == null)
                return null;

            if (manager.Position == -1)
                return null;

            return manager.Current;
        }
    }
}
