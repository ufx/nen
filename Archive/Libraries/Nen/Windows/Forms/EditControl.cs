using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Nen.Windows.Forms {
    /// <summary>
    /// EditControl is a convenience class designed to be inherited by other
    /// UserControls that edit information.  It has a binding provider and
    /// validation renderer baked in.
    /// </summary>
    public partial class EditControl : UserControl {
        /// <summary>
        /// Constructs a new EditControl.
        /// </summary>
        public EditControl () {
            InitializeComponent();

            BindingProvider.ControlDataBinder.ControlValidationStatusChanged += ValidationRenderer.ControlValidationStatusChanged;
        }
    }
}
