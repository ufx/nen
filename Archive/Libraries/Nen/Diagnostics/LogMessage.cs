﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

namespace Nen.Diagnostics {
    public enum LogLevel { Debug, Information, Statistic, Warning, Error }

    [DataContract]
    [Persistent]
    public class LogMessage {
        #region Constructors
        public LogMessage () {
            AdditionalInformation = new LogInformationCollection();
            AdditionalInformation.LogMessage = this;
        }
        #endregion

        #region Reporting
        public string CreateReport () {
            var report = new StringBuilder();

            if (Subject != null) {
                report.AppendLine(Subject);
                report.Append(Environment.NewLine);
            }

            if (Application != null) {
                if (Application.Name != null)
                    report.AppendFormat(CultureInfo.CurrentCulture, "Application: {0}{1}", Application.Name, Environment.NewLine);

                if (Application.Environment != null)
                    report.AppendFormat(CultureInfo.CurrentCulture, "Environment: {0}{1}", Application.Environment, Environment.NewLine);

                if (Application.Version != null)
                    report.AppendFormat(CultureInfo.CurrentCulture, "Version: {0}{1}", Application.Version, Environment.NewLine);
            }

            report.AppendFormat("Log Level: {0}{1}", Level, Environment.NewLine);
            report.Append(Environment.NewLine);

            if (AdditionalInformation.Count > 0) {
                foreach (var info in AdditionalInformation)
                    report.AppendFormat(CultureInfo.CurrentCulture, "{0}: {1}", info.Description, info.Value);

                report.Append(Environment.NewLine);
            }

            if (Details != null)
                report.AppendLine(Details);

            return report.ToString();
        }
        #endregion

        #region Accessors
        [DataMember] public LogInformationCollection AdditionalInformation { get; private set; }
        [DataMember] public ApplicationInformation Application { get; set; }
        [MaximumLength(2000)] [DataMember] public string Details { get; set; }
        [DataMember] public int Id { get; private set; }
        [DataMember] public LogLevel Level { get; set; }
        [MaximumLength(500)] [DataMember] public string Subject { get; set; }
        [Persistent(false)] public Exception WritingFailed { get; set; }
        #endregion
    }

    public class LogInformationCollection : Collection<LogInformation> {
        #region Constructors
        public LogInformationCollection () {
        }
        #endregion

        #region Collection Consistency
        protected override void InsertItem (int index, LogInformation item) {
            item.LogMessage = LogMessage;
            base.InsertItem(index, item);
        }

        protected override void RemoveItem (int index) {
            this[index].LogMessage = null;
            base.RemoveItem(index);
        }

        protected override void ClearItems () {
            foreach (var item in this)
                item.LogMessage = null;
            base.ClearItems();
        }

        protected override void SetItem (int index, LogInformation item) {
            this[index].LogMessage = null;
            item.LogMessage = LogMessage;
            base.SetItem(index, item);
        }
        #endregion

        #region Accessors
        public LogMessage LogMessage { get; internal set; }
        #endregion
    }

    [DataContract]
    [Persistent]
    public class LogInformation {
        #region Constructors
        public LogInformation () {
        }

        public LogInformation (string description, string value) {
            Description = description;
            Value = value;
        }
        #endregion

        #region Accessors
        [DataMember] public int Id { get; set; }
        [DataMember] public LogMessage LogMessage { get; set; }
        [MaximumLength(500)] [DataMember] public string Description { get; set; }
        [MaximumLength(500)] [DataMember] public string Value { get; set; }
        #endregion
    }
}
