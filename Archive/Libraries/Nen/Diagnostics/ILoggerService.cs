﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Nen.Diagnostics {
    /// <summary>
    /// Allows log messages to be written.
    /// </summary>
    [ServiceContract]
    public interface ILoggerService {
        /// <summary>
        /// Writes a log message.
        /// </summary>
        /// <param name="message">The message to write.</param>
        [OperationContract]
        void Write (LogMessage message);
    }
}
