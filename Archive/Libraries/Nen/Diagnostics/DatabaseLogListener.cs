﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

using Nen.Data;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Diagnostics {
    [DataContract]
    public class DatabaseLogListener : LogListener {
        private MapConfiguration _configuration;

        #region Constructors
        public DatabaseLogListener (Database database) {
            Database = database;
        }
        #endregion

        #region DatabaseLogListenerConventions
        private class DatabaseLogListenerConventions : Conventions {
            public override bool HasPersistentTypes (Assembly assembly) {
                // Restrict persistent types to this assembly only.
                return assembly == typeof(DatabaseLogListener).Assembly;
            }
        }
        #endregion

        #region Writing
        public override void Write (LogMessage message) {
            using (var context = GetConfiguration().CreateContext()) {
                var changeset = context.CreateChangeSet();
                changeset.Add(message);
                changeset.AddRange(message.AdditionalInformation);
                changeset.Commit();
            }
        }

        private MapConfiguration GetConfiguration () {
            if (_configuration == null) {
                _configuration = new MapConfiguration(new DatabaseLogListenerConventions());
                _configuration.Initialize();
                _configuration.DefaultStore = new DatabaseStore(_configuration, Database);
            }
            return _configuration;
        }
        #endregion

        #region Accessors
        [DataMember] public Database Database { get; set; }
        #endregion
    }
}
