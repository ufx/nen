﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace Nen.Diagnostics {
    [DebuggerStepThroughAttribute]
    public class LoggerServiceClient : ClientBase<ILoggerService>, ILoggerService {
        #region Constructors
        public LoggerServiceClient (string uri)
            : base(new WSHttpBinding(), new EndpointAddress(uri)) {
        }

        public LoggerServiceClient (Binding binding, EndpointAddress remoteAddress) :
            base(binding, remoteAddress) {
        }
        #endregion

        #region Operations
        public void Write (LogMessage message) {
            base.Channel.Write(message);
        }
        #endregion
    }
}
