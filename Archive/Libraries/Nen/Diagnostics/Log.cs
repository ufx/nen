﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Threading;

using Nen.Collections;
using Nen.Reflection;

namespace Nen.Diagnostics {
    public static class Log {
        #region Constructors
        static Log () {
            ApplicationInformation = new ApplicationInformation();

#if DEBUG
            ApplicationInformation.Environment = "Debug";
#else
            ApplicationInformation.Environment = "Release";
#endif

            var entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly != null) {
                ApplicationInformation.Name = entryAssembly.GetAttribute<AssemblyTitleAttribute>().Title;
                ApplicationInformation.Version = entryAssembly.GetName().Version.ToString();
            }

            Listeners = new Collection<LogListener>();
            Listeners.Add(new DebugLogListener());
        }
        #endregion

        #region Message Appending
        public static void Append (LogMessage message) {
            foreach (var listener in Listeners) {
                try {
                    listener.Write(message);
                } catch (ThreadAbortException) {
                    throw;
                } catch (Exception ex) {
                    message.WritingFailed = ex;
                }
            }
        }

        public static LogMessage Append (LogLevel level, Exception ex) {
            return Append(level, ex, null);
        }

        public static LogMessage Append (LogLevel level, Exception ex, IEnumerable<LogInformation> additionalInformation) {
            var subject = string.Format(CultureInfo.CurrentCulture, "Exception: {0}", ex.GetType());
            return Append(level, subject, ex.ToString(), additionalInformation);
        }

        public static LogMessage Append (LogLevel level, string subject, string details) {
            return Append(level, subject, details, null);
        }

        public static LogMessage Append (LogLevel level, string subject, string details, IEnumerable<LogInformation> additionalInformation) {
            var message = new LogMessage();
            message.Application = ApplicationInformation;
            message.Details = details;
            message.Subject = subject;
            message.Level = level;
            message.AdditionalInformation.AddRange(additionalInformation);
            Append(message);
            return message;
        }

        public static LogMessage Debug (string subject, string details) {
            return Append(LogLevel.Debug, subject, details);
        }

        public static LogMessage Information (string subject, string details) {
            return Append(LogLevel.Information, subject, details);
        }

        public static LogMessage Statistic (string subject, string details) {
            return Append(LogLevel.Statistic, subject, details);
        }

        public static LogMessage Warning (string subject, string details) {
            return Append(LogLevel.Warning, subject, details);
        }

        public static LogMessage Warning (Exception ex) {
            return Append(LogLevel.Warning, ex);
        }

        public static LogMessage Error (string subject, string details) {
            return Append(LogLevel.Error, subject, details);
        }

        public static LogMessage Error (Exception ex) {
            return Append(LogLevel.Error, ex);
        }
        #endregion

        #region Accessors
        public static ApplicationInformation ApplicationInformation { get; set; }
        public static Collection<LogListener> Listeners { get; private set; }
        #endregion
    }
}
