﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Text;

namespace Nen.Diagnostics {
    [DataContract]
    [KnownType(typeof(FilterLogListener)), KnownType(typeof(DebugLogListener))]
    [KnownType(typeof(EventLogLogListener)), KnownType(typeof(DatabaseLogListener))]
    [KnownType(typeof(TraceLogListener)), KnownType(typeof(EmailLogListener))]
    public abstract class LogListener {
        public abstract void Write (LogMessage message);
    }

    [DataContract]
    public class FilterLogListener : LogListener {
        #region Constructors
        public FilterLogListener (LogLevel minimumLogLevel, LogListener innerLogListener) {
            if (innerLogListener == null)
                throw new ArgumentNullException("innerLogListener");

            MinimumLogLevel = minimumLogLevel;
            InnerLogListener = innerLogListener;
        }
        #endregion

        #region Writing
        public override void Write (LogMessage message) {
            if (message.Level >= MinimumLogLevel)
                InnerLogListener.Write(message);
        }
        #endregion

        #region Accessors
        [DataMember] public LogLevel MinimumLogLevel { get; set; }
        [DataMember] public LogListener InnerLogListener { get; set; }
        #endregion
    }

    #region TextWriter Listeners
    public class TextWriterLogListener : LogListener {
        private TextWriter _writer;

        public TextWriterLogListener (TextWriter writer) {
            _writer = writer;
        }

        public override void Write (LogMessage message) {
            _writer.WriteLine(message.CreateReport());
        }
    }

    [DataContract]
    public class ConsoleLogListener : TextWriterLogListener {
        public ConsoleLogListener ()
            : base(Console.Out) {
        }
    }
    #endregion

    #region System.Diagnostics Listeners
    [DataContract]
    public class DebugLogListener : LogListener {
        public override void Write (LogMessage message) {
            Debug.WriteLine(message.CreateReport());
        }
    }
    
    [DataContract]
    public class EventLogLogListener : LogListener {
        private static Dictionary<LogLevel, EventLogEntryType> _eventLogLevels = new Dictionary<LogLevel, EventLogEntryType>();

        static EventLogLogListener () {
            _eventLogLevels[LogLevel.Debug] = EventLogEntryType.Information;
            _eventLogLevels[LogLevel.Information] = EventLogEntryType.Information;
            _eventLogLevels[LogLevel.Statistic] = EventLogEntryType.Information;
            _eventLogLevels[LogLevel.Warning] = EventLogEntryType.Error;
            _eventLogLevels[LogLevel.Error] = EventLogEntryType.Error;
        }

        public override void Write (LogMessage message) {
            var source = message.Application == null ? "Application" : message.Application.Name;
            EventLog.WriteEntry(source, message.CreateReport(), _eventLogLevels[message.Level]);
        }
    }

    [DataContract]
    public class TraceLogListener : LogListener {
        public override void Write (LogMessage message) {
            Trace.WriteLine(message);
        }
    }
    #endregion

    [DataContract]
    public class EmailLogListener : LogListener {
        #region Writing
        public override void Write (LogMessage message) {
            string subject;
            if (message.Application == null)
                subject = message.Subject;
            else
                subject = string.Format(CultureInfo.CurrentCulture, "[{0}] ({1} v{2}) {3}", message.Application.Environment, message.Application.Name, message.Application.Version, message.Subject);

            var client = new SmtpClient(SmtpServerHostname);
            client.Send(FromAddress, ToAddress, subject, message.CreateReport());
        }
        #endregion

        #region Accessors
        [DataMember] public string FromAddress { get; set; }
        [DataMember] public string SmtpServerHostname { get; set; }
        [DataMember] public string ToAddress { get; set; }
        #endregion
    }

    public class ServiceLogListener : LogListener {
        #region Constructors
        public ServiceLogListener (ILoggerService loggerService) {
            LoggerService = loggerService;
        }
        #endregion

        #region Writing
        public override void Write (LogMessage message) {
            LoggerService.Write(message);
        }
        #endregion

        #region Accessors
        public ILoggerService LoggerService { get; set; }
        #endregion
    }
}
