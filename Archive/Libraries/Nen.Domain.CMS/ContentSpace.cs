﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;
using Nen.Collections;

namespace Nen.Domain.CMS {
    [Persistent, DiscriminatorValue("{A7ECEC9B-25BD-40e6-AF8B-4E8B730E407B}")]
    public class ContentSpace: Content {
        public ObservableCollection<ContentSpaceMember> Members { get; set; }

        public ContentSpace () {
            Members = new ObservableCollection<ContentSpaceMember>();
        }
    }
}
