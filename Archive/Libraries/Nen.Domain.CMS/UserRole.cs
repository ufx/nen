﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

namespace Nen.Domain.CMS {
    [Persistent]
    public class UserRole: Entity {
        [Required]
        public User User { get; set; }

        [Required]
        public Role Role { get; set; }
    }
}
