﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

namespace Nen.Domain.CMS {
    [Persistent, DiscriminatorValue("{D19F6744-9DED-4290-9F37-70CB0CC3937E}")]
    public class Content: Entity {
        [Required, MaximumLength(256)]
        public string Title { get; set; }

        [Required, MaximumLength(256)]
        public string Identifier { get; set; }

        [Required]
        public User Creator { get; set; }

        [Discriminator]
        public Guid ContentType { get; set; }
    }
}
