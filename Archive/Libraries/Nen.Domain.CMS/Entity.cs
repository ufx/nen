﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Domain.CMS {
    [Persistent, Flatten]
    public class Entity {
        public Guid Id { get; set; }
        [TimeStamp]
        public DateTime DateModified { get; set; }
        public DateTime DateCreated { get; set; }

        public Entity () {
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
        }
    }
}
