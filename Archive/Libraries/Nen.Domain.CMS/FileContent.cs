﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;
using System.IO;

namespace Nen.Domain.CMS {
    [Persistent, DiscriminatorValue("{F0BB06DF-488E-4a7c-8AB1-B98546179D8C}")]
    public class FileContent: Content {
        [MaximumLength(512)]
        public string ExternalPath { get; set; }

        [Required, MaximumLength(64)]
        public string MimeType { get; set; }

        public virtual Stream GetStream () {
            // TODO: support for DB-side streams
            return new FileStream(ExternalPath, FileMode.Open);
        }
    }
}
