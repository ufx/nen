﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Domain.CMS {
    [Persistent, DiscriminatorValue("{77C093A2-1483-4f4c-B9BA-EC65D053F338}")]
    public abstract class ModuleInstance: Content {
        public Guid ModuleDefinitionId { get; set; }
    }
}
