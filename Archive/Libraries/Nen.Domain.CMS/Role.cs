﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

namespace Nen.Domain.CMS {
    [Persistent]
    public class Role: Entity {

        [Required]
        public string Name { get; set; }
    }
}
