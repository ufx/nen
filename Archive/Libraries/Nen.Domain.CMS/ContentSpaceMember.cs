﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Domain.CMS {
    [Persistent]
    public class ContentSpaceMember {
        public ContentSpace ContentSpace { get; set; }
        public Content Member { get; set; }
    }
}
