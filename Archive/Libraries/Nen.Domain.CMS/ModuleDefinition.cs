﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

namespace Nen.Domain.CMS {
    public class ModuleDefinition {
        public Guid Id { get; set; }

        [Required, MaximumLength(64)]
        public string Name { get; set; }

        [Required, MaximumLength(64)]
        public string Vendor { get; set; }

        [Required, MaximumLength(64)]
        public string Version { get; set; }

        public Type ControllerType { get; set; }
    }
}
