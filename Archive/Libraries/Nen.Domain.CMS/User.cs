﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Domain.CMS {
    [Persistent]
    public class User: Entity {
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string DistinguishedName { get; set; }
    }
}
