﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Deme.Medical.Hl7.Mllp;
using Nen.Net.Sockets;

namespace Deme.Tests.Medical.Hl7.Mllp {
    [TestClass]
    public class MllpTests {
        [TestMethod, Timeout(4000)]
        public void WriteContent () {
            string message = "Hello, World!";

            var listenerThread = new Thread(() => RunListenerForWrites(message));
            listenerThread.Start();
            Thread.Sleep(500); // Wait for listener to begin listening.

            var client = new MllpClient("localhost", Config.ServerTestPort);
            client.WriteContent(UnicodeEncoding.UTF8.GetBytes(message));
            client.TcpClient.Close();

            listenerThread.Join();
        }

        [TestMethod, Timeout(4000)]
        public void WriteLargeContent () {
            string message = @"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In risus risus, tincidunt blandit, fringilla in, consequat id, lacus. Nam facilisis libero a libero. Integer magna. Aliquam vel diam. Donec ante urna, auctor a, pharetra a, suscipit eu, mi. Sed rhoncus blandit enim. Nulla facilisis odio at libero. Donec vitae pede et diam hendrerit eleifend. Sed volutpat. Etiam lacinia, mauris non gravida iaculis, pede nunc scelerisque nulla, a eleifend velit nibh vitae purus. Phasellus blandit tortor eu sapien.

Vivamus tincidunt imperdiet eros. Duis facilisis fringilla metus. Integer aliquet purus in lorem. Morbi ante. Quisque consequat odio ac tellus. Quisque elit. Vestibulum elementum ipsum consectetuer leo. Cras nibh. Donec suscipit. Morbi ullamcorper erat et velit. Aliquam accumsan eros a elit eleifend posuere. Morbi nec nunc. In vel leo sed pede tempor feugiat. Curabitur porta. Vestibulum urna dui, ullamcorper id, tristique nec, vehicula eu, risus. Nunc eros. Sed velit.

Pellentesque dignissim neque ut est. Suspendisse ut justo. Aliquam volutpat varius risus. Quisque viverra justo quis neque ornare consequat. Nunc laoreet. Sed vitae leo nec urna gravida fermentum. Nullam sagittis ipsum eget mi. Phasellus aliquam, ipsum eu egestas rutrum, sapien turpis vestibulum sapien, sed dignissim nulla mauris vel nunc. Vivamus lobortis pharetra est. Nulla turpis nunc, fringilla in, ornare sed, blandit sit amet, risus. Mauris commodo lorem ut arcu. Vivamus nec augue ac purus suscipit tincidunt. Curabitur sodales luctus nisi. Aliquam sem.

Aliquam laoreet dapibus diam. Sed pellentesque ante ut ipsum. Phasellus dolor urna, commodo in, nonummy sed, tincidunt non, enim. Donec ac libero vitae ligula vehicula commodo. Phasellus vitae nunc vel orci sollicitudin gravida. Donec blandit neque non odio malesuada semper. Nam condimentum. Pellentesque quis felis nec lacus sagittis ultrices. Suspendisse potenti. In nec eros. Sed aliquet, nibh eget convallis convallis, velit urna semper leo, at consequat ante orci eget augue. Praesent porttitor mauris ullamcorper purus. Praesent laoreet, metus a varius lobortis, risus enim semper dolor, vitae lacinia quam justo eget nunc. Morbi elementum mi eu pede. Sed condimentum ligula sit amet mi semper sodales. Donec eget purus. Mauris pellentesque ultrices odio.

Morbi tincidunt massa facilisis diam. Maecenas auctor, velit nec fringilla venenatis, leo sem tempus leo, id viverra sem augue fermentum enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur vel odio. Donec eu libero. Donec sit amet risus. Nunc et sapien ut tellus congue hendrerit. Sed vestibulum. Pellentesque nonummy dolor eget pede. Nullam porttitor congue erat. Phasellus hendrerit nisl in libero. Integer turpis ipsum, pharetra cursus, dignissim eget, varius quis, nibh. Phasellus quis massa at odio lobortis commodo. Donec id orci vitae turpis lacinia porta. Vivamus ac risus sit amet eros faucibus placerat. Nullam sed libero. Maecenas magna.

Proin at lorem non ante suscipit hendrerit. Donec augue. Sed quam. In et arcu. Mauris tincidunt lectus tempus tellus. Mauris ut lorem. Donec placerat, dui aliquam consectetuer aliquam, massa lorem consequat risus, eu tincidunt dolor sem eget tortor. Vivamus ut eros in enim ullamcorper ullamcorper. Duis vitae leo et neque pretium rutrum. Duis vestibulum purus ac ante. Ut porta ipsum non sapien. In hac habitasse platea dictumst. Mauris sapien arcu, auctor et, aliquam sed, ultricies a, arcu. Vivamus eleifend, tortor ut blandit euismod, turpis enim faucibus sem, eget tristique risus purus et enim. Cras nec nunc ut tellus pellentesque volutpat. Nulla aliquam lacus. Praesent fermentum sem et nisi. Donec ipsum lorem, ornare et, porta sed, aliquam vulputate, felis. Quisque non felis. Maecenas mollis posuere diam.

In tempus consequat lorem. Sed volutpat urna. Quisque sit amet mi. Etiam dictum tellus quis mauris. Proin vitae velit at magna dignissim laoreet. Pellentesque aliquet nullam.";

            var listenerThread = new Thread(() => RunListenerForWrites(message));
            listenerThread.Start();
            Thread.Sleep(500); // Wait for listener to begin listening.

            var client = new MllpClient("localhost", Config.ServerTestPort);
            client.WriteContent(UnicodeEncoding.UTF8.GetBytes(message));
            client.TcpClient.Close();

            listenerThread.Join();
        }

        [TestMethod]
        public void ReadContent () {
            var listenerThread = new Thread(() => RunListenerForReads());
            listenerThread.Start();
            Thread.Sleep(500); // Wait for listener to begin listening.

            byte[] content = UnicodeEncoding.UTF8.GetBytes("Celebrity");
            TcpClientEx.SimpleSend("localhost", Config.ServerTestPort, MllpProtocol.FrameContent(content));

            listenerThread.Join();
        }

        #region ReadContent Helpers
        private void RunListenerForReads () {
            var contentReceived = false;
            using (var server = new MllpServer()) {
                server.ContentReceived += (s, e) => {
                    string content = UnicodeEncoding.UTF8.GetString(e.Content);
                    Assert.AreEqual("Celebrity", content);

                    contentReceived = true;
                    server.Stop();
                };

                server.Start(IPAddress.Any, Config.ServerTestPort);
            }

            Assert.IsTrue(contentReceived);
        }
        #endregion

        #region WriteContent Helpers
        private void RunListenerForWrites (string message) {
            var data = TcpListenerEx.SimpleReceive(IPAddress.Any, Config.ServerTestPort);
            Assert.AreEqual(message.Length + 3, data.Length);
            Assert.AreEqual(data[0], MllpProtocol.StartBlock);
            string content = UnicodeEncoding.UTF8.GetString(data, 1, data.Length - 3);
            Assert.AreEqual(message, content);
            Assert.AreEqual(MllpProtocol.EndBlock, data[data.Length - 2]);
            Assert.AreEqual(MllpProtocol.CarriageReturn, data[data.Length - 1]);
        }
        #endregion
    }
}
