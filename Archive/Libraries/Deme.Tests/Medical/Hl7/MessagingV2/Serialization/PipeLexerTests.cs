﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Deme.Medical.Hl7.MessagingV2.Serialization;

namespace Deme.Tests.Medical.Hl7.MessagingV2.Serialization {
    [TestClass]
    public class PipeLexerTests {
        [TestMethod]
        public void Analyze () {
            var message = "MSH|^~\\&|foo^bar|baz";

            var tokens = PipeLexer.Analyze(message);
            Assert.AreEqual(4, tokens.Count);

            var segmentIdentifier = tokens[0];
            Assert.IsTrue(segmentIdentifier.Value.IsProvided);
            Assert.AreEqual("MSH", segmentIdentifier.Value.Value);
            Assert.AreEqual(0, segmentIdentifier.Subtokens.Count);

            var delimitedValue = tokens[2];
            Assert.IsTrue(delimitedValue.Value.IsProvided);
            Assert.AreEqual("foo^bar", delimitedValue.Value.Value);
            Assert.AreEqual(2, delimitedValue.Subtokens.Count);

            var firstDelimitedValue = delimitedValue.Subtokens[0];
            Assert.IsTrue(firstDelimitedValue.Value.IsProvided);
            Assert.AreEqual("foo", firstDelimitedValue.Value.Value);
            Assert.AreEqual(0, firstDelimitedValue.Subtokens.Count);

            var secondDelimitedValue = delimitedValue.Subtokens[1];
            Assert.IsTrue(secondDelimitedValue.Value.IsProvided);
            Assert.AreEqual("bar", secondDelimitedValue.Value.Value);
            Assert.AreEqual(0, secondDelimitedValue.Subtokens.Count);

            var undelimitedValue = tokens[3];
            Assert.IsTrue(undelimitedValue.Value.IsProvided);
            Assert.AreEqual("baz", undelimitedValue.Value.Value);
            Assert.AreEqual(0, undelimitedValue.Subtokens.Count);
        }
    }
}
