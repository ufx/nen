﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Deme.Tests {
    public static class Config {
        public static int ServerTestPort {
            get { return int.Parse(ConfigurationManager.AppSettings["Nen.Net.TestPort"]); }
        }
    }
}
