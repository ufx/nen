﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Workflow;
using Nen.ObjectModel;
using System.IO;
using System.Threading;

namespace Nen.Tests.Workflow {
    [Serializable]
    public class ReadActivity : Activity {
        public static readonly NenProperty<string> PromptProperty;
        public static readonly NenProperty<string> InputProperty;

        [Local]
        public string Prompt {
            get { return (string)GetValue(PromptProperty); }
            set { SetValue(PromptProperty, value); }
        }

        [Local]
        public string Input {
            get { return (string)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }


        private static Queue<ContinuationHandle> _waitForInput = new Queue<ContinuationHandle>();
        private static bool _blocking = false;

        public override Continuation Execute (Continuation currentContinuation) {
            if (!_blocking) {
                return InitiateRead(currentContinuation);
            } else {
                _waitForInput.Enqueue(
                    ActivityContext.CurrentContext.CreateHandle(currentContinuation.Push(InitiateRead))
                );

                return BlockingDelimiter.Block(currentContinuation);
            }
        }

        private Continuation InitiateRead (Continuation currentContinuation) {
            var context = ActivityContext.CurrentContext;

            _blocking = true;
            Console.Write(Prompt);
            var handle = ActivityContext.CurrentContext.CreateHandle(currentContinuation.Push(CompleteRead));

            ThreadPool.QueueUserWorkItem(s => {
                Input = Console.ReadLine();

                context.ResumeHandle(handle);
            });

            return BlockingDelimiter.Block(currentContinuation);
        }

        private Continuation CompleteRead (Continuation currentContinuation) {
            _blocking = false;
            if (_waitForInput.Count > 0)
                ActivityContext.CurrentContext.ResumeHandle(_waitForInput.Dequeue());

            return currentContinuation;
        }
    }

    [Serializable]
    public class PrintActivity : Activity {
        public static readonly NenProperty<string> FormatProperty;
        public static readonly NenProperty<TextWriter> DestinationProperty;

        [Local]
        public string Format {
            get { return (string)GetValue(FormatProperty); }
            set { SetValue(FormatProperty, value); }
        }

        [Local]
        public TextWriter Destination {
            get { return (TextWriter)GetValue(DestinationProperty); }
            set { SetValue(DestinationProperty, value); }
        }

        [Local]
        public object[] Arguments { get; set; }

        public override Continuation Execute (Continuation currentContinuation) {
            (Destination ?? Console.Out).WriteLine(Format, Arguments ?? new object[0]);

            return currentContinuation;
        }
    }
}
