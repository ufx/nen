﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Workflow;

namespace Nen.Tests.Workflow {
    [TestClass]
    public class BasicActivityTests {
        [TestMethod]
        public void PrintLoop () {
            new ActivityContext().Start(new LoopActivity {
                LoopStart = 0, LoopEnd = 2,
                Action = new SequenceActivity {
                    Children = {
                        new PrintActivity { Format = "Test!" },
                    }
                }
            });
        }
    }
}
