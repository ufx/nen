﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Collections;
using Nen.Workflow;
using Nen.ObjectModel;

namespace Nen.Tests.Workflow {
    [TestClass]
    public class DataflowTests {
        [Serializable]
        class FixedInput<T> : Input<T> {
            private IEnumerable<T> _source;
            private IEnumerator<T> _sourceEnumerator;
            private bool _finished;

            public FixedInput (IEnumerable<T> source) {
                _source = source;
                _sourceEnumerator = source.GetEnumerator();
                _finished = !_sourceEnumerator.MoveNext();
            }

            #region Input<T> Members

            public T ReadInput () {
                var value = PeekInput();
                _finished = !_sourceEnumerator.MoveNext();
                return value;
            }

            public T PeekInput () {
                return _sourceEnumerator.Current;
            }

            #endregion

            #region Input Members

            public bool IsInputReady {
                get { return !_finished; }
            }

            public event EventHandler InputReady;

            protected void OnInputReady (EventArgs e) {
                if (InputReady != null)
                    InputReady(this, e);
            }

            #endregion

            #region IDataStream Members

            public bool IsFinished {
                get { return _finished; }
                set { throw new NotImplementedException(); }
            }

            #endregion
        }

        [Serializable]
        public class Adder : Activity {
            public static NenProperty<Input<int>> LeftProperty;
            public static NenProperty<Input<int>> RightProperty;
            public static NenProperty<Output<int>> OutputProperty;

            [Local]
            public Input<int> Left {
                get { return (Input<int>)GetValue(LeftProperty); }
                set { SetValue(LeftProperty, value); }
            }

            [Local]
            public Input<int> Right {
                get { return (Input<int>)GetValue(RightProperty); }
                set { SetValue(RightProperty, value); }
            }

            [Local]
            public Output<int> Output {
                get { return (Output<int>)GetValue(OutputProperty); }
                set { SetValue(OutputProperty, value); }
            }

            public override void Execute () {
                if (Output == null)
                    Output = new RingBuffer<int>(8);

                while (Left.IsInputReady && Right.IsInputReady && Output.IsOutputReady) {
                    Output.WriteOutput(Left.ReadInput() + Right.ReadInput());
                }

                if (Left.IsFinished || Right.IsFinished)
                    Output.IsFinished = true;
            }
        }

        [TestMethod]
        public void SyncDataflow () {
            var buffer = new RingBuffer<int>(1);
            var left = new int[] { 1, 2, 3, 4 };
            var right = new int[] { 5, 6, 7, 8 };

            var workflow = new DataflowActivity {
                Children = {
                    new Adder { 
                        Left = new FixedInput<int>(left), 
                        Right = new FixedInput<int>(right),
                        Output = buffer
                    }
                }
            };

            var ctx = new ActivityContext();
            var instance = ctx.Add(workflow);

            for (var i = 0; i < 4; ++i) {
                ctx.Run(true);
                Assert.AreEqual(left[i] + right[i], buffer.ReadInput());
                Assert.IsFalse(buffer.IsInputReady);
            }
        }
    }
}
