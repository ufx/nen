﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Workflow;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using Nen.ObjectModel;
using Nen.Collections;

namespace Nen.Tests.Workflow {
    [TestClass]
    public class PersistenceTests {
        public class SuspendHandle {
            public ContinuationHandle ContinuationHandle { get; set; }
        }

        [Serializable]
        public class SuspendActivity: Activity {
            [NonSerialized]
            private SuspendHandle _handle;

            public SuspendHandle Handle {
                get { return _handle; }
                set { _handle = value; }
            }

            public override Continuation Execute (Continuation currentContinuation) {
                Handle.ContinuationHandle = ActivityContext.CurrentContext.CreateHandle(currentContinuation);
                return BlockingDelimiter.Block(currentContinuation);
            }
        }

        private ActivityContext CreateContext (bool useStorage) {
            var context = new ActivityContext();

            if (useStorage) {
                var store = new FileActivityStore(".\\Workflows");
             
                store.Attach(context);
            }

            return context;
        }

        private Activity CreateSuspendWorkflow (TextWriter destination, SuspendHandle suspendHandle) {
            return new SequenceActivity {
                Children = {
                    new PrintActivity { Destination = destination, Format = "1" },
                    new SuspendActivity { Handle = suspendHandle },
                    new PrintActivity { Destination = destination, Format = "2" }
                }
            };
        }

        private Activity CreateWaitEventWorkflow () {
            return new SequenceActivity {
                Children = {
                    new WaitEventActivity { Id = "wait", EventName = "Launch Missiles" },
                    new PrintActivity { Bindings = { new ActivityBinding { Name = "Format", Expr = "wait.EventData" } } }
                }
            };
        }

        [TestMethod]
        public void SuspendActivity_NoStorage () {
            var ctx = CreateContext(false);
            var suspendHandle = new SuspendHandle();
            var stringWriter = new StringWriter();
            var workflow = ctx.Add(CreateSuspendWorkflow(stringWriter, suspendHandle));

            ctx.Run(true);

            Assert.AreEqual("1\r\n", stringWriter.ToString());

            ctx.ResumeHandle(suspendHandle.ContinuationHandle);
            ctx.Run(true);

            Assert.AreEqual("1\r\n2\r\n", stringWriter.ToString());
        }

        [TestMethod]
        public void SuspendActivity_WithStorage () {
            var ctx = CreateContext(true);
            var suspendHandle = new SuspendHandle();
            var stringWriter = new StringWriter();
            var workflow = ctx.Add(CreateSuspendWorkflow(stringWriter, suspendHandle));

            ctx.Run(true);

            Assert.AreEqual("1\r\n", stringWriter.ToString());

            ctx = CreateContext(false);
            var store = new FileActivityStore(".\\Workflows");
            var activityHandle = store.Get(suspendHandle.ContinuationHandle.Owner.Id);
            var contHandle = activityHandle.GetContinuationHandle(suspendHandle.ContinuationHandle.Id);

            ctx.ResumeHandle(contHandle);
            ctx.Run(true);

            // note: stringWriter gets serialized, so it breaks the object reference!
            Assert.AreEqual("1\r\n2\r\n", ((PrintActivity)((SequenceActivity)activityHandle.Activity).Children[0]).Destination.ToString());
        }

        [TestMethod]
        public void SuspendActivity_WithStoragePurgeContext () {
            var store = new FileActivityStore(".\\Workflows");
            var ctx = new ActivityContext();

            store.Attach(ctx);

            var suspendHandle = new SuspendHandle();
            var stringWriter = new StringWriter();
            var workflow = ctx.Add(CreateSuspendWorkflow(stringWriter, suspendHandle));

            ctx.Run(true);

            Assert.AreEqual("1\r\n", stringWriter.ToString());

            ctx.PurgeBlockedActivities();

            Assert.AreEqual(0,
                ((Dictionary<Activity, ActivityHandle>)
                    ctx.GetType().GetField("_handles", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ctx)).Count);

            var activityHandle = store.Get(suspendHandle.ContinuationHandle.Owner.Id);
            var contHandle = activityHandle.GetContinuationHandle(suspendHandle.ContinuationHandle.Id);

            ctx.ResumeHandle(contHandle);
            ctx.Run(true);
            Assert.AreEqual("1\r\n2\r\n", ((PrintActivity)((SequenceActivity)activityHandle.Activity).Children[0]).Destination.ToString());
        }

        [TestMethod]
        public void WaitEventActivity_Simple () {
            var ctx = CreateContext(true);
            var stringWriter = new StringWriter();

            var cout = Console.Out;
            try {
                Console.SetOut(stringWriter);

                var workflow = ctx.Add(CreateWaitEventWorkflow());

                ctx.Run(true);
                Sync.Notify(ctx,  "Launch Missiles", "**BOOM**");
                ctx.Run(true);
                Assert.AreEqual("**BOOM**\r\n", stringWriter.ToString());
            } finally {
                Console.SetOut(cout);
            }
        }

        [TestMethod]
        public void WaitEventActivity_Parallel () {
            var ctx = CreateContext(true);
            var stringWriter = new StringWriter();

            var cout = Console.Out;
            try {
                Console.SetOut(stringWriter);

                var workflow = ctx.Add(new SequenceActivity {
                    Children = {
                        new PrintActivity { Format = "start" },
                        new ParallelActivity {
                            Children = {
                                new SequenceActivity {
                                    Children = {
                                        new WaitEventActivity { Id = "wait", EventName = "left" },
                                        new PrintActivity { Format = "left" },
                                        new PrintActivity {
                                            Bindings = { new ActivityBinding { Name = "Format", Expr = "wait.EventData" } }
                                        }
                                    }
                                },

                                new SequenceActivity {
                                    Children = {
                                        new WaitEventActivity { Id = "wait", EventName = "right" },
                                        new PrintActivity { Format = "right" },
                                        new PrintActivity {
                                            Bindings = { new ActivityBinding { Name = "Format", Expr = "wait.EventData" } }
                                        }
                                    }
                                }
                            }
                        },
                        new PrintActivity { Format = "end" }
                    }
                });

                ctx.Run(true);
                Sync.Notify(ctx, "right", "1");
                ctx.Run(true);
                Sync.Notify(ctx, "left", "2");
                ctx.Run(true);

                Assert.AreEqual("start\r\nright\r\n1\r\nleft\r\n2\r\nend\r\n", stringWriter.ToString());

                stringWriter = new StringWriter();
                Console.SetOut(stringWriter);

                workflow = ctx.Add(workflow);
                ctx.Run(true);
                Sync.Notify(ctx, "left", "1");
                ctx.Run(true);
                Sync.Notify(ctx, "right", "2");
                ctx.Run(true);

                Assert.AreEqual("start\r\nleft\r\n1\r\nright\r\n2\r\nend\r\n", stringWriter.ToString());
            } finally {
                Console.SetOut(cout);
            }
        }
    }
}
