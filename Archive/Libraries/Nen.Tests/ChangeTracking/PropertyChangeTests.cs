using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.ChangeTracking;

namespace Nen.Tests.ChangeTracking {
    [TestClass]
    public class PropertyChangeTests {
        [TestMethod]
        public void PropertyChangeEvents () {
            Stack<PropertyChangeEventArgs> changed = new Stack<PropertyChangeEventArgs>();
            Stack<PropertyChangingEventArgs> changing = new Stack<PropertyChangingEventArgs>();

            TestClass c = new TestClass();
            c.PropertyChanged += delegate(object s, PropertyChangeEventArgs e) { changed.Push(e); };
            c.PropertyChanging += delegate(object s, PropertyChangingEventArgs e) { changing.Push(e); };

            c.StringField = "foo";

            Assert.AreEqual(1, changed.Count);
            Assert.AreEqual(1, changing.Count);

            PropertyChangeEventArgs changedArgs = changed.Pop();
            Assert.AreEqual("StringField", changedArgs.Name);
            Assert.AreEqual("foo", changedArgs.NewValue);
            Assert.AreEqual("", changedArgs.OldValue);

            PropertyChangingEventArgs changingArgs = changing.Pop();
            Assert.AreSame(changingArgs, changedArgs);
            Assert.IsFalse(changingArgs.CancelChange);

            c.StringField = "bar";

            Assert.AreEqual(1, changed.Count);
            Assert.AreEqual(1, changing.Count);

            changedArgs = changed.Pop();
            Assert.AreEqual("StringField", changedArgs.Name);
            Assert.AreEqual("bar", changedArgs.NewValue);
            Assert.AreEqual("foo", changedArgs.OldValue);

            changingArgs = changing.Pop();
            Assert.AreSame(changingArgs, changedArgs);
            Assert.IsFalse(changingArgs.CancelChange);

            c.StringField = "bar";

            Assert.AreEqual(0, changed.Count);
            Assert.AreEqual(0, changing.Count);
        }

        [TestMethod]
        public void PropertyChangeCancellation () {
            Stack<PropertyChangeEventArgs> changed = new Stack<PropertyChangeEventArgs>();
            Stack<PropertyChangingEventArgs> changing = new Stack<PropertyChangingEventArgs>();

            TestClass c = new TestClass();
            c.PropertyChanged += delegate(object s, PropertyChangeEventArgs e) { changed.Push(e); };
            c.PropertyChanging += delegate(object s, PropertyChangingEventArgs e) {
                changing.Push(e);
                e.CancelChange = true;
            };

            c.StringField = "cancel";

            Assert.AreEqual("", c.StringField);
            Assert.AreEqual(0, changed.Count);
            Assert.AreEqual(1, changing.Count);

            PropertyChangingEventArgs changingArgs = changing.Pop();
            Assert.AreEqual("StringField", changingArgs.Name);
            Assert.AreEqual("cancel", changingArgs.NewValue);
            Assert.AreEqual("", changingArgs.OldValue);
            Assert.AreEqual(true, changingArgs.CancelChange);
        }

        #region Test Class
        private class TestClass : ChangeTrackable {
            private string _stringField = "";

            public string StringField {
                get { return _stringField; }
                set { Set("StringField", value, ref _stringField); }
            }
        }
        #endregion
    }
}
