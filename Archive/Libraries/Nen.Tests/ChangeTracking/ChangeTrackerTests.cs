using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.ChangeTracking;
using Nen.Collections;

namespace Nen.Tests.ChangeTracking {
    [TestClass]
    public class ChangeTrackerTests {
        [TestMethod]
        public void TrackingRecursiveGraphs () {
            RecursiveA a = new RecursiveA();
            RecursiveB b = new RecursiveB(), b2 = new RecursiveB();
            a.B = b;
            a.B2 = b2;
            a.B.A = a;
            a.B.A2 = a;
            a.B2.A = a;

            ChangeTracker tracker = new ChangeTracker();

            tracker.StartTracking(a);
            Assert.AreEqual(4, tracker.GetReferenceCount(a));
            Assert.AreEqual(1, tracker.GetReferenceCount(a.B2));
            Assert.AreEqual(1, tracker.GetReferenceCount(a.B));

            a.B.A = null;
            Assert.AreEqual(3, tracker.GetReferenceCount(a));

            a.B.A = a;
            Assert.AreEqual(4, tracker.GetReferenceCount(a));
            Assert.AreEqual(1, tracker.GetReferenceCount(a.B2));
            Assert.AreEqual(1, tracker.GetReferenceCount(a.B));

            a.B = null;
            Assert.AreEqual(2, tracker.GetReferenceCount(a));
            Assert.AreEqual(0, tracker.GetReferenceCount(b));

            a.B2 = null;
            Assert.AreEqual(1, tracker.GetReferenceCount(a));
            Assert.AreEqual(0, tracker.GetReferenceCount(b2));
        }

        [TestMethod]
        public void TrackingObservableCollections () {
            Duck d = new Duck(), d2 = new Duck();
            Warhead w = new Warhead(), w2 = new Warhead(), w3 = new Warhead(); ;
            w.Duck = d;
            w2.Duck = d;
            w3.Duck = d2;
            d.Warheads.Add(w);
            d.Warheads.Add(w2);
            d2.Warheads.Add(w3);

            ChangeTracker tracker = new ChangeTracker();

            tracker.StartTracking(d);
            Assert.AreEqual(3, tracker.GetReferenceCount(d));
            Assert.AreEqual(1, tracker.GetReferenceCount(w));
            Assert.AreEqual(1, tracker.GetReferenceCount(w2));
            Assert.AreEqual(1, tracker.GetReferenceCount(d.Warheads));

            tracker.StartTracking(d2);
            Assert.AreEqual(3, tracker.GetReferenceCount(d));
            Assert.AreEqual(1, tracker.GetReferenceCount(w));
            Assert.AreEqual(1, tracker.GetReferenceCount(w2));
            Assert.AreEqual(1, tracker.GetReferenceCount(d.Warheads));
            Assert.AreEqual(2, tracker.GetReferenceCount(d2));
            Assert.AreEqual(1, tracker.GetReferenceCount(d2.Warheads));
            Assert.AreEqual(1, tracker.GetReferenceCount(w3));

            w2.Duck = null;
            Assert.AreEqual(2, tracker.GetReferenceCount(d));

            d.Warheads.Remove(w2);
            Assert.AreEqual(0, tracker.GetReferenceCount(w2));

            d2.Warheads = null;
            Assert.AreEqual(1, tracker.GetReferenceCount(d2));
            Assert.AreEqual(0, tracker.GetReferenceCount(w3));
        }

        [TestMethod]
        public void TrackingFutures () {
            Mass mass = new Mass();
            mass.Amount = 5000;
            BlackHole hole = new BlackHole(mass);

            ChangeTracker tracker = new ChangeTracker();
            EventHandler<PropertyChangeEventArgs> fail = delegate { Assert.Fail("Tracking of unresolved value shouldn't occur."); };
            tracker.PropertyChanged += fail;
            tracker.StartTracking(hole);

            Assert.AreEqual(0, tracker.GetReferenceCount(mass));
            Assert.AreEqual(1, tracker.GetReferenceCount(hole));
            Assert.AreEqual(1, tracker.GetReferenceCount(hole.Mass));

            Assert.IsFalse(hole.Mass.IsResolved);
            mass.Amount = 6000;

            tracker.PropertyChanged -= fail;
            bool caughtResolvedChange = false;
            tracker.PropertyChanged += delegate { caughtResolvedChange = true; };
            hole.Mass.Resolve();
            Assert.AreEqual(0, tracker.GetReferenceCount(hole.Mass));
            Assert.AreEqual(1, tracker.GetReferenceCount(mass));
            Assert.IsFalse(caughtResolvedChange);
            mass.Amount = 7000;
            Assert.IsTrue(caughtResolvedChange);
        }
    }

    #region Test Classes
    public class RecursiveA : ChangeTrackable {
        private RecursiveB _b, _b2;

        public RecursiveB B {
            get { return _b; }
            set { Set("B", value, ref _b); }
        }

        public RecursiveB B2 {
            get { return _b2; }
            set { Set("B2", value, ref _b2); }
        }
    }

    public class RecursiveB : ChangeTrackable {
        private RecursiveA _a, _a2;

        public RecursiveA A {
            get { return _a; }
            set { Set("A", value, ref _a); }
        }

        public RecursiveA A2 {
            get { return _a2; }
            set { Set("A2", value, ref _a2); }
        }
    }

    public class Duck : ChangeTrackable {
        private ObservableCollection<Warhead> _warheads = new ObservableCollection<Warhead>();

        public ObservableCollection<Warhead> Warheads {
            get { return _warheads; }
            set { Set("Warheads", value, ref _warheads); }
        }
    }

    public class Warhead : ChangeTrackable {
        private Duck _duck;

        public Duck Duck {
            get { return _duck; }
            set { Set("Duck", value, ref _duck); }
        }
    }

    public class BlackHole : ChangeTrackable {
        private Future<Mass> _mass;

        public BlackHole (Mass futureMass) {
            _mass = new Future<Mass>(delegate { return futureMass; });
        }

        public Future<Mass> Mass {
            get { return _mass; }
            set { Set("Mass", value, ref _mass); }
        }
    }

    public class Mass : ChangeTrackable {
        private int _amount;

        public int Amount {
            get { return _amount; }
            set { Set("Amount", value, ref _amount); }
        }
    }
    #endregion
}
