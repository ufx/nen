﻿-- Foreign Key Drops

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LogMessage_LogLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[LogMessage]'))
ALTER TABLE [dbo].[LogMessage] DROP CONSTRAINT [FK_LogMessage_LogLevel]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LogInformation_LogMessage]') AND parent_object_id = OBJECT_ID(N'[dbo].[LogInformation]'))
ALTER TABLE [dbo].[LogInformation] DROP CONSTRAINT [FK_LogInformation_LogMessage]
GO

-- Table Drops

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogLevel]') AND type in (N'U'))
DROP TABLE [dbo].[LogLevel]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogMessage]') AND type in (N'U'))
DROP TABLE [dbo].[LogMessage]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogInformation]') AND type in (N'U'))
DROP TABLE [dbo].[LogInformation]
GO

-- Table Creates

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Level] [int] NOT NULL,
	[Subject] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Details] [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ApplicationEnvironment] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApplicationName] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApplicationVersion] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_LogMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogLevel](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_LogLevel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogInformation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogMessageId] [int] NOT NULL,
	[Description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Value] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_LogInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Foreign Key Creates

ALTER TABLE [dbo].[LogMessage]  WITH CHECK ADD  CONSTRAINT [FK_LogMessage_LogLevel] FOREIGN KEY([Level])
REFERENCES [dbo].[LogLevel] ([Id])
GO

ALTER TABLE [dbo].[LogInformation]  WITH CHECK ADD  CONSTRAINT [FK_LogInformation_LogMessage] FOREIGN KEY([LogMessageId])
REFERENCES [dbo].[LogMessage] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

-- Reference Data Inserts

INSERT INTO LogLevel (Id, Name) VALUES(0, 'Debug');
INSERT INTO LogLevel (Id, Name) VALUES(1, 'Information');
INSERT INTO LogLevel (Id, Name) VALUES(2, 'Statistic');
INSERT INTO LogLevel (Id, Name) VALUES(3, 'Warning');
INSERT INTO LogLevel (Id, Name) VALUES(4, 'Error');
GO

