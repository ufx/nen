﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Collections;
using Nen.Diagnostics;
using Nen.Tests.Data;

namespace Nen.Tests.Diagnostics {
    [TestClass]
    public class DatabaseLogListenerTests {
        private Collection<LogListener> _previousListeners;

        [TestInitialize]
        public void Initialize () {
            _previousListeners = new Collection<LogListener>(Log.Listeners);
            Log.Listeners.Clear();
            Log.Listeners.Add(new DatabaseLogListener(TestEnvironment.SqlDatabase));
        }

        [TestCleanup]
        public void Cleanup () {
            Log.Listeners.Clear();
            Log.Listeners.AddRange(_previousListeners);

            TestEnvironment.SqlDatabase.ExecuteNonQuerySql("DELETE FROM LogMessage WHERE Subject IN ('Test Simple Information Write', 'Test Additional Information Write')");
            TestEnvironment.SqlDatabase.ExecuteNonQuerySql("DELETE FROM LogInformation WHERE Description IN ('TestItem1', 'TestItem2')");
        }

        [TestMethod]
        public void WriteSimple () {
            var message = Log.Information("Test Simple Information Write", "This is a test of the database log listener.");
            Assert.IsNotNull(message);
            Assert.IsNull(message.WritingFailed);

            var row = TestEnvironment.SqlDatabase.ExecuteDataRowSql("SELECT * FROM LogMessage WHERE Subject = 'Test Simple Information Write'");
            Assert.IsNotNull(row);

            var id = (int) row["Id"];

            Assert.AreEqual(LogLevel.Information, (LogLevel) row["Level"]);
            Assert.IsTrue(id > 0);
            Assert.AreEqual("Test Simple Information Write", row["Subject"]);
            Assert.AreEqual("This is a test of the database log listener.", row["Details"]);
            Assert.AreEqual("Debug", row["ApplicationEnvironment"]);
            Assert.AreSame(DBNull.Value, row["ApplicationName"]);
            Assert.AreSame(DBNull.Value, row["ApplicationVersion"]);

            Assert.AreEqual(0, TestEnvironment.SqlDatabase.ExecuteScalarSql("SELECT COUNT(*) FROM LogInformation WHERE LogMessageId = " + id));
        }

        [TestMethod]
        public void WriteAdditionalInformation () {
            var message = new LogMessage();
            message.Subject = "Test Additional Information Write";
            message.Details = "This is a test of the database log listener with additional information.";
            message.Level = LogLevel.Statistic;
            message.AdditionalInformation.Add(new LogInformation("TestItem1", "TestValue1"));
            message.AdditionalInformation.Add(new LogInformation("TestItem2", "TestValue2"));

            Log.Append(message);
            Assert.IsNull(message.WritingFailed);

            var messageRow = TestEnvironment.SqlDatabase.ExecuteDataRowSql("SELECT * FROM LogMessage WHERE Subject = 'Test Additional Information Write'");
            Assert.IsNotNull(messageRow);

            var messageId = (int) messageRow["Id"];

            Assert.AreEqual(LogLevel.Statistic, (LogLevel) messageRow["Level"]);
            Assert.IsTrue(messageId > 0);
            Assert.AreEqual("Test Additional Information Write", messageRow["Subject"]);
            Assert.AreEqual("This is a test of the database log listener with additional information.", messageRow["Details"]);
            Assert.AreSame(DBNull.Value, messageRow["ApplicationEnvironment"]);
            Assert.AreSame(DBNull.Value, messageRow["ApplicationName"]);
            Assert.AreSame(DBNull.Value, messageRow["ApplicationVersion"]);

            var infoTable = TestEnvironment.SqlDatabase.ExecuteDataTableSql("SELECT * FROM LogInformation WHERE LogMessageId = " + messageId + " ORDER BY Description");
            Assert.IsNotNull(infoTable);
            Assert.AreEqual(2, infoTable.Rows.Count);

            var test1Row = infoTable.Rows[0];
            Assert.IsTrue(((int) test1Row["Id"]) > 0);
            Assert.AreEqual(messageId, test1Row["LogMessageId"]);
            Assert.AreEqual("TestItem1", test1Row["Description"]);
            Assert.AreEqual("TestValue1", test1Row["Value"]);

            var test2Row = infoTable.Rows[1];
            Assert.IsTrue(((int) test2Row["Id"]) > 0);
            Assert.AreEqual(messageId, test2Row["LogMessageId"]);
            Assert.AreEqual("TestItem2", test2Row["Description"]);
            Assert.AreEqual("TestValue2", test2Row["Value"]);
        }
    }
}
