using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.ChangeTracking;
using Nen.DataBinding;

namespace Nen.Tests.DataBinding {
    [TestClass]
    public class DataBinderTests {
        [TestMethod]
        public void SimpleSynchronization () {
            A a = new A();
            a.Foo = "Qux";

            B b = new B();
            b.Bar = "Nuke";

            DataBinder binder = new DataBinder();
            binder.Bindings.Add(a, "Foo", b, "Bar");

            Assert.AreEqual("Qux", a.Foo);
            Assert.AreEqual("Nuke", b.Bar);

            binder.Synchronize();

            Assert.AreEqual("Qux", a.Foo);
            Assert.AreEqual("Qux", b.Bar);

            a.Foo = "Quuux";

            Assert.AreEqual("Quuux", a.Foo);
            Assert.AreEqual("Quuux", b.Bar);

            b.Bar = "Laminate";

            Assert.AreEqual("Laminate", a.Foo);
            Assert.AreEqual("Laminate", b.Bar);
        }

        private class A : ChangeTrackable {
            private string _foo = "";

            public string Foo {
                get { return _foo; }
                set { Set("Foo", value, ref _foo); }
            }
        }

        private class B : ChangeTrackable {
            private string _bar = "";

            public string Bar {
                get { return _bar; }
                set { Set("Bar", value, ref _bar); }
            }
        }
    }
}
