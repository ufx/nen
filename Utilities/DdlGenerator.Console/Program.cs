﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using Nen.Data.ObjectRelationalMapper;
using Nen.Data;
using Nen.Data.SqlClient;

namespace DdlGenerator.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                System.Console.Error.WriteLine("Usage: DdlGenerator.Console.exe <assembly path> [map configuration]");
                return;
            }

            var assemblyFile = args[0];
            var root = Path.GetDirectoryName(assemblyFile);
            if (string.IsNullOrWhiteSpace(root))
                root = ".";
            string mapConfigName = null;

            if (args.Length > 1)
                mapConfigName = args[1];

            AppDomain.CurrentDomain.AssemblyResolve += delegate(object sender, ResolveEventArgs e)
            {
                var str2 = e.Name.Substring(0, e.Name.IndexOf(','));
                var file = Directory.GetFiles(root, str2 + "*.*").First<string>(f => f.EndsWith(".dll") || f.EndsWith(".exe"));
                return Assembly.Load(AssemblyName.GetAssemblyName(file));
            };

            // Load config files if any are found.
            LoadConfiguration(root);

            // Lookup the MapConfiguration type
            var type = Assembly.Load(AssemblyName.GetAssemblyName(assemblyFile)).GetExportedTypes()
                .Where(t => typeof(MapConfiguration).IsAssignableFrom(t))
                .FirstOrDefault(t => t.Name == mapConfigName || mapConfigName == null);

            if (type == null)
                type = typeof(MapConfiguration);

            // Create the configuration.
            MapConfiguration config;
            try
            {
                config = (MapConfiguration)Activator.CreateInstance(type);
            }
            catch (MissingMethodException)
            {
                System.Console.WriteLine("No parameterless constructor specified for {0}", type.FullName);
                System.Environment.Exit(-1);
                return;
            }

            config.Initialize();
            var text = GenerateDdl(config);
            System.Console.WriteLine(text);
        }

        private static string GenerateDdl(MapConfiguration config)
        {
            var databaseStore = config.DefaultStore as DatabaseStore;
            var database = databaseStore == null ? new SqlDatabase() : databaseStore.Database;

            var compound = config.GenerateSchemaDataSet().GenerateSql();
            var formatter = database.CreateFormatter();
            formatter.EmbedLiterals = true;
            var cmd = formatter.Generate(compound);
            return cmd.CommandText;
        }

        private static void LoadConfiguration(string root)
        {
            foreach (var configFileName in Directory.GetFiles(root, "*.config"))
                Nen.Configuration.ConfigurationManagerEx.ImportConfigurationAppSettings(configFileName);
        }
    }
}
