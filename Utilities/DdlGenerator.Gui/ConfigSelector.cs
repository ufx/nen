﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using Nen.Data.ObjectRelationalMapper;

namespace DdlGenerator {
    public partial class ConfigSelector : UserControl {
        public ConfigSelector () {
            InitializeComponent();
        }

        public event EventHandler<EventArgs> GenerateClicked;

        private void _generate_Click (object sender, EventArgs e) {
            if (GenerateClicked != null)
                GenerateClicked(sender, e);
        }

        public void OpenAssembly (string fileName) {
            _fileName.Text = fileName;

            // Open assembly, read configs
            var assembly = Assembly.LoadFile(fileName);
            var types = assembly.GetExportedTypes();
            var assignableTypes = types.Where(t => typeof(MapConfiguration).IsAssignableFrom(t));

            foreach (var type in assignableTypes)
                _configurations.Items.Add(type);

            _configurations.Items.Add(typeof(MapConfiguration));
            _configurations.SelectedIndex = 0;

            // Add databases
            var databaseTypes = types.Where(t => typeof(Nen.Data.Database).IsAssignableFrom(t));
            foreach (var type in databaseTypes)
                _database.Items.Add(type);

            var nenDatabases = typeof(Nen.Data.Database).Assembly.GetExportedTypes()
                .Where(t => typeof(Nen.Data.Database).IsAssignableFrom(t));
            foreach (var type in nenDatabases)
                _database.Items.Add(type);

            _database.SelectedItem = typeof(Nen.Data.SqlClient.SqlDatabase);
        }

        [Browsable(false)]
        public Type SelectedConfig
        {
            get { return (Type) _configurations.SelectedItem; }
            set { _configurations.SelectedItem = value; }
        }

        public Type SelectedDatabase
        {
            get { return (Type)_database.SelectedItem; }
            set { _database.SelectedItem = value; }
        }
    }
}
