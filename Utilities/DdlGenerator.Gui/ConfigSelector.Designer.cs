﻿namespace DdlGenerator {
    partial class ConfigSelector {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this._fileName = new System.Windows.Forms.Label();
            this._configurations = new System.Windows.Forms.ComboBox();
            this._generate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._database = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // _fileName
            // 
            this._fileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._fileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._fileName.Location = new System.Drawing.Point(3, 5);
            this._fileName.Name = "_fileName";
            this._fileName.Size = new System.Drawing.Size(423, 17);
            this._fileName.TabIndex = 1;
            this._fileName.Text = "[FileName]";
            // 
            // _configurations
            // 
            this._configurations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._configurations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._configurations.FormattingEnabled = true;
            this._configurations.Location = new System.Drawing.Point(77, 25);
            this._configurations.Name = "_configurations";
            this._configurations.Size = new System.Drawing.Size(268, 21);
            this._configurations.TabIndex = 2;
            // 
            // _generate
            // 
            this._generate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._generate.Location = new System.Drawing.Point(351, 23);
            this._generate.Name = "_generate";
            this._generate.Size = new System.Drawing.Size(75, 23);
            this._generate.TabIndex = 3;
            this._generate.Text = "Generate";
            this._generate.UseVisualStyleBackColor = true;
            this._generate.Click += new System.EventHandler(this._generate_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Config:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Database:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _database
            // 
            this._database.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._database.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._database.FormattingEnabled = true;
            this._database.Location = new System.Drawing.Point(77, 52);
            this._database.Name = "_database";
            this._database.Size = new System.Drawing.Size(268, 21);
            this._database.TabIndex = 5;
            // 
            // ConfigSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this._database);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._generate);
            this.Controls.Add(this._configurations);
            this.Controls.Add(this._fileName);
            this.Name = "ConfigSelector";
            this.Size = new System.Drawing.Size(429, 80);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _fileName;
        private System.Windows.Forms.ComboBox _configurations;
        private System.Windows.Forms.Button _generate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _database;
    }
}
