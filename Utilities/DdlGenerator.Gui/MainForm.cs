﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Nen.Data;
using Nen.Data.ObjectRelationalMapper;

namespace DdlGenerator {
    public partial class MainForm : Form {
        private ConfigSelector _selector;
        private string _fileName;

        public MainForm () {
            InitializeComponent();

            AppDomain.CurrentDomain.AssemblyResolve += AssemblyResolve;
        }

        private void _exit_Click (object sender, EventArgs e) {
            Close();
        }

        private void _open_Click (object sender, EventArgs e) {
            var file = new OpenFileDialog();
            file.Filter = "Assemblies (*.dll)|*.dll|All files (*.*)|*.*";
            if (file.ShowDialog() != DialogResult.OK)
                return;

            OpenAssembly(file.FileName);
        }

        private void OpenAssembly (string fileName) {
            _fileName = fileName;

            _selector = new ConfigSelector();
            _selector.GenerateClicked += new EventHandler<EventArgs>(GenerateClicked);
            _selector.OpenAssembly(fileName);

            _top.Controls.Clear();
            _top.Controls.Add(_selector);

            _selector.Dock = DockStyle.Fill;
            _top.Height = 80;
        }

        private void GenerateClicked (object sender, EventArgs e) {
            try {
                var type = _selector.SelectedConfig;
                var instance = (MapConfiguration) Activator.CreateInstance(type);
                instance.Initialize();
                var sql = instance.GenerateSchemaDataSet().GenerateSql();

                var databaseType = _selector.SelectedDatabase;
                var database = (Nen.Data.Database)Activator.CreateInstance(databaseType);
                var formatter = database.CreateFormatter();
                formatter.EmbedLiterals = true;
                var text = formatter.Generate(sql);
                _sqlText.Text = text.CommandText;

                Clipboard.SetText(text.CommandText);
            } catch (ThreadAbortException) {
                throw;
            } catch (Exception ex) {
                _sqlText.Text = ex.ToString();
            }
        }


        private Assembly AssemblyResolve (object sender, ResolveEventArgs args) {
            var directory = Path.GetDirectoryName(_fileName);

            var assemblyName = args.Name.Substring(0, args.Name.IndexOf(','));
            var files = Directory.GetFiles(directory, assemblyName + "*.*");
            var assemblyFileName = files.First(f => f.EndsWith(".dll") || f.EndsWith(".exe"));
            return Assembly.Load(AssemblyName.GetAssemblyName(assemblyFileName));
        }
    }
}
