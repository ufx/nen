﻿namespace DdlGenerator {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._open = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._exit = new System.Windows.Forms.ToolStripMenuItem();
            this._top = new System.Windows.Forms.Panel();
            this._hint = new System.Windows.Forms.Label();
            this._sqlText = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this._top.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(526, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._open,
            this.toolStripSeparator1,
            this._exit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // _open
            // 
            this._open.Name = "_open";
            this._open.Size = new System.Drawing.Size(111, 22);
            this._open.Text = "&Open";
            this._open.Click += new System.EventHandler(this._open_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(108, 6);
            // 
            // _exit
            // 
            this._exit.Name = "_exit";
            this._exit.Size = new System.Drawing.Size(111, 22);
            this._exit.Text = "E&xit";
            this._exit.Click += new System.EventHandler(this._exit_Click);
            // 
            // _top
            // 
            this._top.BackColor = System.Drawing.SystemColors.Control;
            this._top.Controls.Add(this._hint);
            this._top.Dock = System.Windows.Forms.DockStyle.Top;
            this._top.Location = new System.Drawing.Point(0, 24);
            this._top.Name = "_top";
            this._top.Size = new System.Drawing.Size(526, 22);
            this._top.TabIndex = 1;
            // 
            // _hint
            // 
            this._hint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._hint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._hint.Location = new System.Drawing.Point(12, 4);
            this._hint.Name = "_hint";
            this._hint.Size = new System.Drawing.Size(502, 17);
            this._hint.TabIndex = 0;
            this._hint.Text = "Select File -> Open to choose an assembly.";
            // 
            // _sqlText
            // 
            this._sqlText.Dock = System.Windows.Forms.DockStyle.Fill;
            this._sqlText.Location = new System.Drawing.Point(0, 46);
            this._sqlText.Multiline = true;
            this._sqlText.Name = "_sqlText";
            this._sqlText.ReadOnly = true;
            this._sqlText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._sqlText.Size = new System.Drawing.Size(526, 314);
            this._sqlText.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(526, 360);
            this.Controls.Add(this._sqlText);
            this.Controls.Add(this._top);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MainForm";
            this.Text = "DDL Generator";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this._top.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _open;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _exit;
        private System.Windows.Forms.Panel _top;
        private System.Windows.Forms.Label _hint;
        private System.Windows.Forms.TextBox _sqlText;

    }
}

