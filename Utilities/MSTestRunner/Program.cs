﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Configuration;
using Nen.Reflection;

namespace MSTestRunner {
    class Program {
        static string _directory;
        static string[] _testNames;

        static void Main (string[] args) {
            if (args.Length == 0) {
                Console.WriteLine("Usage: {0} <test assembly filename> [<test method or class 1> ... <test method or class n>]", Environment.CommandLine);
                return;
            }

            if (args.Length > 1)
                _testNames = args.Skip(1).Select(a => a.ToLower()).ToArray();

            var fullPath = Path.GetFullPath(args[0]);
            _directory = Path.GetDirectoryName(fullPath);

            AppDomain.CurrentDomain.AssemblyResolve += AssemblyResolve;

            LoadConfiguration(fullPath);
            var testClasses = LoadTestClasses(fullPath);

            foreach (var testClass in testClasses)
                RunTests(testClass);

            Console.WriteLine("All done.");
        }

        #region Damned Dirty Hacks
        static Assembly AssemblyResolve (object sender, ResolveEventArgs args) {
            var assemblyName = args.Name.Substring(0, args.Name.IndexOf(','));
            var files = Directory.GetFiles(_directory, assemblyName + "*.*");
            var assemblyFileName = files.First(f => f.EndsWith(".dll") || f.EndsWith(".exe"));
            return Assembly.Load(AssemblyName.GetAssemblyName(assemblyFileName));
        }

        static void LoadConfiguration (string fullPath) {
            var config = ConfigurationManager.OpenExeConfiguration(fullPath);

            foreach (var key in config.AppSettings.Settings.AllKeys)
                ConfigurationManagerEx.SetReadOnlyAppSetting(key, config.AppSettings.Settings[key].Value);
        }
        #endregion

        #region Test Running
        static IEnumerable<Type> LoadTestClasses (string fullPath) {
            var testAssembly = Assembly.LoadFile(fullPath);
            return testAssembly.GetExportedTypesWithAttribute<TestClassAttribute>(true);
        }

        static void RunTests (Type testClass) {
            var runAll = _testNames == null || _testNames.Contains(testClass.Name.ToLower());

            var testMethods = testClass.FindMembersWithAttribute<TestMethodAttribute>(MemberTypes.Method, TypeEx.DeclaredInstanceBindingFlags, false).Cast<MethodInfo>();
            var testCleanups = testClass.FindMembersWithAttribute<TestCleanupAttribute>(MemberTypes.Method, TypeEx.DeclaredInstanceBindingFlags, false).Cast<MethodInfo>().ToArray();
            var testInits = testClass.FindMembersWithAttribute<TestInitializeAttribute>(MemberTypes.Method, TypeEx.DeclaredInstanceBindingFlags, false).Cast<MethodInfo>().ToArray();

            var instance = Activator.CreateInstance(testClass);

            var methodsToRun = testMethods.Where(m => runAll || _testNames.Contains(m.Name.ToLower()));

            foreach (var testMethod in methodsToRun) {
                try {
                    foreach (var testInit in testInits)
                        testInit.Invoke(instance, null);

                    testMethod.Invoke(instance, null);

                    foreach (var testCleanup in testCleanups)
                        testCleanup.Invoke(instance, null);

                    Console.WriteLine("Test passed.");
                } catch (ThreadAbortException) {
                    throw;
                } catch (Exception ex) {
                    Console.WriteLine("Unhandled exception: {0}", ex);
                }
            }
        }
        #endregion
    }
}
