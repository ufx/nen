﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace SerializationViewer {
    public partial class MainForm : Form {
        public MainForm () {
            InitializeComponent();
        }

        private void _view_Click (object sender, EventArgs e) {
            var serializer = new DataContractSerializer(typeof(Nen.Diagnostics.DatabaseLogListener));

            var obj = GetObject();

            //var reader = System.Xml.XmlDictionaryReader.CreateTextReader(UnicodeEncoding.UTF8.GetBytes(xml), new System.Xml.XmlDictionaryReaderQuotas());
            //var obj = serializer.ReadObject(reader);

            var stream = new MemoryStream();
            serializer.WriteObject(stream, obj);

            var buffer = stream.ToArray();
            var text = UnicodeEncoding.UTF8.GetString(stream.GetBuffer());

            _result.Text = text;
        }

        private object GetObject () {
            var database = new Nen.Data.SqlClient.SqlDatabase("test conn string");
            var listener = new Nen.Diagnostics.DatabaseLogListener(database);
            return listener;
        }
    }
}
