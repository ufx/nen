﻿namespace SerializationViewer {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this._view = new System.Windows.Forms.Button();
            this._result = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _view
            // 
            this._view.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._view.Location = new System.Drawing.Point(655, 13);
            this._view.Name = "_view";
            this._view.Size = new System.Drawing.Size(105, 23);
            this._view.TabIndex = 0;
            this._view.Text = "View";
            this._view.UseVisualStyleBackColor = true;
            this._view.Click += new System.EventHandler(this._view_Click);
            // 
            // _result
            // 
            this._result.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._result.Location = new System.Drawing.Point(12, 42);
            this._result.Multiline = true;
            this._result.Name = "_result";
            this._result.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._result.Size = new System.Drawing.Size(748, 503);
            this._result.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 557);
            this.Controls.Add(this._result);
            this.Controls.Add(this._view);
            this.Name = "MainForm";
            this.Text = "Serialization Viewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _view;
        private System.Windows.Forms.TextBox _result;
    }
}

