﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EventLogManager {
    public partial class MainForm : Form {
        public MainForm () {
            InitializeComponent();
        }

        private void _createEventSource_Click (object sender, EventArgs e) {
            var source = _eventSource.Text.Trim();
            if (source == "")
                return;

            try {
                EventLog.CreateEventSource(source, "Application");
            } catch (ArgumentException ex) {
                MessageBox.Show(ex.Message);
                return;
            }

            MessageBox.Show("Event source created.");
        }

        private void _deleteEventSource_Click (object sender, EventArgs e) {
            var source = _eventSource.Text.Trim();

            try {
                EventLog.DeleteEventSource(source);
            } catch (ArgumentException ex) {
                MessageBox.Show(ex.Message);
                return;
            }

            MessageBox.Show("Event source deleted.");
        }
    }
}
