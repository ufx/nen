﻿namespace EventLogManager {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this._eventSource = new System.Windows.Forms.TextBox();
            this._createEventSource = new System.Windows.Forms.Button();
            this._deleteEventSource = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _eventSource
            // 
            this._eventSource.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._eventSource.Location = new System.Drawing.Point(12, 12);
            this._eventSource.Name = "_eventSource";
            this._eventSource.Size = new System.Drawing.Size(196, 20);
            this._eventSource.TabIndex = 0;
            // 
            // _createEventSource
            // 
            this._createEventSource.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._createEventSource.Location = new System.Drawing.Point(12, 38);
            this._createEventSource.Name = "_createEventSource";
            this._createEventSource.Size = new System.Drawing.Size(196, 23);
            this._createEventSource.TabIndex = 1;
            this._createEventSource.Text = "Create Event Source";
            this._createEventSource.UseVisualStyleBackColor = true;
            this._createEventSource.Click += new System.EventHandler(this._createEventSource_Click);
            // 
            // _deleteEventSource
            // 
            this._deleteEventSource.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._deleteEventSource.Location = new System.Drawing.Point(12, 67);
            this._deleteEventSource.Name = "_deleteEventSource";
            this._deleteEventSource.Size = new System.Drawing.Size(196, 23);
            this._deleteEventSource.TabIndex = 2;
            this._deleteEventSource.Text = "Delete Event Source";
            this._deleteEventSource.UseVisualStyleBackColor = true;
            this._deleteEventSource.Click += new System.EventHandler(this._deleteEventSource_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(220, 97);
            this.Controls.Add(this._deleteEventSource);
            this.Controls.Add(this._createEventSource);
            this.Controls.Add(this._eventSource);
            this.MaximumSize = new System.Drawing.Size(236, 133);
            this.MinimumSize = new System.Drawing.Size(236, 133);
            this.Name = "MainForm";
            this.Text = "Event Log Manager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _eventSource;
        private System.Windows.Forms.Button _createEventSource;
        private System.Windows.Forms.Button _deleteEventSource;
    }
}

