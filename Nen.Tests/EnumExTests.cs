﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Reflection;

namespace Nen.Tests {
    [TestClass]
    public class EnumExTests {
        [TestMethod]
        public void TestGetDescriptionDictionary () {
            var dictionary = EnumEx.GetDescriptionDictionary<TestEnum>();
            Assert.AreEqual("Apples FOO", dictionary[TestEnum.Apples]);
            Assert.AreEqual("Oranges", dictionary[TestEnum.Oranges]);
            Assert.AreEqual("Dinosaur", dictionary[TestEnum.Velociraptors]);
        }

        public enum TestEnum {
            [System.ComponentModel.Description("Apples FOO")]
            Apples,
            Oranges,
            [System.ComponentModel.Description("Dinosaur")]
            Velociraptors
        }
    }
}
