﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.IO;

namespace Nen.Tests.IO {
    [TestClass]
    public class PathTests {
        [TestMethod]
        public void PathExtensions () {
            Assert.AreEqual("C:\\Foo\\Bar", PathEx.UpOneLevel("C:\\Foo\\Bar\\Baz.txt"));
            Assert.AreEqual("C:\\Foo\\Bar", PathEx.UpOneLevel("C:\\Foo\\Bar\\Baz\\"));
            Assert.AreEqual("C:\\Foo\\Bar", PathEx.UpOneLevel("C:\\Foo\\Bar\\Baz"));
            Assert.IsNull(PathEx.UpOneLevel("Foo\\"));

            Assert.AreEqual("Baz\\Qux", PathEx.RelativeTo("C:\\Foo\\Bar\\Baz\\Qux", "C:\\Foo\\Bar"));
            Assert.AreEqual("Blah.txt", PathEx.RelativeTo("C:\\Foo\\Bar\\Blah.txt", "C:\\Foo\\Bar\\"));
            Assert.IsNull(PathEx.RelativeTo("C:\\Foo\\Bar", "X:\\Whatever"));
        }
    }
}
