﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nen.Tests
{
    [TestClass]
    public class StringExTests
    {
        [TestMethod]
        public void TestAfterFirst()
        {
            var str = "foo:bar:baz";
            Assert.AreEqual("bar:baz", str.AfterFirst(":"));

            var edge = "foo:";
            Assert.AreEqual("", edge.AfterFirst(":"));

            var moreThanOneCharacter = "foobizzlewack";
            Assert.AreEqual("bizzlewack", moreThanOneCharacter.AfterFirst("foo"));
        }

        [TestMethod]
        public void TestBetweenFirst()
        {
            var str = "foo:bar_baz";
            Assert.AreEqual("bar", str.BetweenFirst(":", "_"));
        }

        [TestMethod]
        public void TestBeforeFirst()
        {
            var str = "i_love_you";
            Assert.AreEqual("i_love", str.BeforeFirst("_you"));
        }

        [TestMethod]
        public void TestToUpperWithUnderscores()
        {
            Assert.AreEqual("SHARK_BOARD_ID", StringEx.ToUpperWithUnderscores("SharkBoardId"));
            Assert.AreEqual("SHARK", StringEx.ToUpperWithUnderscores("Shark"));
            Assert.AreEqual("SHARK", StringEx.ToUpperWithUnderscores("SHARK"));
            Assert.AreEqual("SHARK_BOARD", StringEx.ToUpperWithUnderscores("SHARK_BOARD"));
            Assert.AreEqual("MOVING_TREES_AROUND", StringEx.ToUpperWithUnderscores("MOVingTreesAround"));
            Assert.AreEqual("ABIGRUNON", StringEx.ToUpperWithUnderscores("abigrunon"));
            Assert.AreEqual("SOME5TIMES_WITH3_NUMBERS", StringEx.ToUpperWithUnderscores("Some5timesWith3Numbers"));
        }

        [TestMethod]
        public void TestToLowerWithUnderscores()
        {
            Assert.AreEqual("fk_empty_fixture_office_fixture_pk", StringEx.ToLowerWithUnderscores("FK_Empty_Fixture_OfficeFixture_PK"));
        }
    }
}
