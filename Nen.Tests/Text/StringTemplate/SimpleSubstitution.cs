using System;
using System.Text;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Text.StringTemplate;

namespace Nen.Tests.Text.StringTemplate {
    [TestClass]
    public class SimpleSubstitution {
        public SimpleSubstitution () {
        }

        [TestMethod]
        public void SimpleSubstitution_String () {
            var e = TemplateBuiltins.CreateEnvironment();
            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("foo", "123");
            args.Add("bar", "456");
            Assert.AreEqual("123 + 456 = 579", TemplateUtilities.ApplyStringTemplate(e, "$foo$ + $bar$ = 579", args));
        }
    }
}
