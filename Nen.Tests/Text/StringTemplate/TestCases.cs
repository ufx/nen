using System;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Text.StringTemplate;

namespace Nen.Tests.Text.StringTemplate {
    [TestClass]
    public class TestCases {
        private static Nen.Text.StringTemplate.TemplateEnvironment _env = TemplateBuiltins.CreateEnvironment();

        private Expression ParseExpr (string str) {
            TemplateLexer l = new TemplateLexer(new StringReader(str));
            l.StartExpression();
            TemplateParser p = new TemplateParser(l);
            return p.expr_group();
        }

        private object EvalExpr (string expr) {
            return ParseExpr(expr).Evaluate(_env);
        }

        private static void Reset () {
            _env = TemplateBuiltins.CreateEnvironment();
        }

        public TestCases () { }

        [TestMethod]
        public void TestPrecedence () {
            Reset();

            Assert.AreEqual(4, EvalExpr("2 + 2"));
            Assert.AreEqual(10, EvalExpr("2 * (2 + 3)"));
            Assert.AreEqual(1, EvalExpr("10 / 5 / 2"));
            Assert.AreEqual(false, EvalExpr("true && false || false"));
        }

        [TestMethod]
        public void TestFunctions () {
            Reset();

            Assert.AreEqual(2, EvalExpr("(+).Arity"));
            Assert.AreEqual(typeof(BlockFunction), EvalExpr("[|x y z|]").GetType());
            Assert.AreEqual(typeof(TemplateFunction), EvalExpr("{|x y z|}").GetType());
            Assert.AreEqual(2, EvalExpr("([|x y|] @ []).Arity"));
        }

        [TestMethod]
        public void TestWhitespace () {
            Reset();

            Assert.AreEqual("foo", EvalExpr("{ |x|  $x$}(\"foo\")"));
            Assert.AreEqual(" foo", EvalExpr("{|x|\\ $x$}(\"foo\")"));
        }

        [TestMethod]
        public void TestCurrying () {
            Reset();

            EvalExpr("f := [|x y z| x * y + z]; g := f(1); h := g(2)");
            Assert.AreEqual(7, EvalExpr("g(4, 3)"));
            Assert.AreEqual(5, EvalExpr("h(3)"));
            Assert.AreEqual(0, EvalExpr("f(0,0,0)"));
            Assert.AreEqual(7, EvalExpr("(g@h)(3)(2)"));
        }

        [TestMethod]
        public void TestPrimCurrying () {
            Reset();

            EvalExpr("commas := list @ sep(\", \")");
            Assert.AreEqual("1, 2, 3", EvalExpr("commas(\"1\", \"2\", \"3\")"));
        }

        [TestMethod]
        public void TestKeywords () {
            Reset();

            EvalExpr("counter := [|start step| [start := start + step]]");
            EvalExpr("foo := counter(0, 10)");
            Assert.AreEqual(2, EvalExpr("counter.Arity"));
            Assert.AreEqual(1, EvalExpr("counter(0, step: 10).Arity"));
            EvalExpr("bar := counter(0, step: 10)(5)");
            EvalExpr("baz := counter(0, step: 3)(6, step: 9)");
            EvalExpr("qux := counter(0, step: 5); qux(start: 6); quux := qux(7)");
            Assert.AreEqual(10, EvalExpr("foo()"));
            Assert.AreEqual(20, EvalExpr("foo()"));
            Assert.AreEqual(10, EvalExpr("foo(start: 0)"));
            Assert.AreEqual(35, EvalExpr("foo(step: 15)"));
            Assert.AreEqual(45, EvalExpr("foo()"));
            Assert.AreEqual(5, EvalExpr("bar()"));
            Assert.AreEqual(6, EvalExpr("baz()"));
            Assert.AreEqual(7, EvalExpr("quux()"));
        }

        [TestMethod]
        public void BenchmarkFuncalls () {
            EvalExpr("counter := [|start step| [start := start + step]](0, 1)");
            Expression e = ParseExpr("counter()");
            for (int i = 0; i < 100000; ++i) {
                e.Evaluate(_env);
            }
        }

        [TestMethod]
        public void BenchmarkNothing () {
            EvalExpr("foo := []");
            Expression e = ParseExpr("foo()");

            for (int i = 0; i < 1000000; ++i)
                e.Evaluate(_env);
        }

        [TestMethod]
        public void BenchmarkFact () {
            EvalExpr("fact := [|x| if(x=0, [1], [x*fact(x-1)])]");

            Assert.AreEqual(120, EvalExpr("fact(5)"));

            Expression e = ParseExpr("fact(10)");
            for (int i = 0; i < 100000; ++i)
                e.Evaluate(_env);
        }

        [TestMethod]
        public void BenchmarkFib () {
            Reset();

            EvalExpr("fib := [|x| if(x<2, [1], [fib(x-2) + fib(x-1)])]");
            Assert.AreEqual(3524578, EvalExpr("fib(32)"));
        }
    }
}
