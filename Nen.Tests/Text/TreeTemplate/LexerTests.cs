﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Text.TreeTemplate;
using System.IO;
using System.Diagnostics;

namespace Nen.Tests.Text.TreeTemplate {
//    static class LexerEx {
//        public static void ExpectTokens (this TTLexer lexer, params TTLexerTokenType[] tokenTypes) {
//            foreach (var token in tokenTypes) {
//                lexer.Advance();
//                Assert.AreEqual(token, lexer.CurrentToken.Type);
//            }
//        }

//        public static void ExpectTokens (this TTLexer lexer, params Func<TTLexerToken, bool>[] tokenPreds) {
//            var i = 1;
//            foreach (var pred in tokenPreds) {
//                lexer.Advance();
//                Assert.IsTrue(pred(lexer.CurrentToken), string.Format("Test {0} failed.", i));
//                i++;
//            }
//        }
//    }

//    [TestClass]
//    public class LexerTests {
//        [TestMethod]
//        public void Indent () {
//            var testString = @"
//foo
//  bar
//    baz
//    quux
//  glorp
//";

//            TTLexer lexer = new TTLexer(new MemoryStream(Encoding.UTF8.GetBytes(testString)));
//            lexer.ExpectTokens(
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Separator,
//                TTLexerTokenType.Indent,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Separator,
//                TTLexerTokenType.Indent,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Separator,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Separator,
//                TTLexerTokenType.Dedent,
//                TTLexerTokenType.Word
//            );
//        }

//        [TestMethod]
//        public void TagSymbols () {
//            var testString = "%foo.bar#baz{a=b}";

//            TTLexer lexer = new TTLexer(new MemoryStream(Encoding.UTF8.GetBytes(testString)));
//            lexer.ExpectTokens(
//                TTLexerTokenType.PercentSign,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Dot,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.NumberSign,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.LeftBrace,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Equals,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.RightBrace
//            );
//        }

//        [TestMethod]
//        public void ExprWhitespace () {
//            var testString = "foo  bar baz";

//            TTLexer lexer = new TTLexer(new MemoryStream(Encoding.UTF8.GetBytes(testString)));
//            lexer.ExpectTokens(
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Word
//            );
//        }

//        [TestMethod]
//        public void LineNumbers () {
//            var testString = @"foo
//  bar
//  baz
//quux";

//            TTLexer lexer = new TTLexer(new MemoryStream(Encoding.UTF8.GetBytes(testString)));
//            Func<int, TTLexerTokenType, Func<TTLexerToken, bool>> lineFn =
//                (expectLine, expectType) =>
//                (TTLexerToken token) =>
//                    token.LineNumber == expectLine && token.Type == expectType;

//            lexer.ExpectTokens(
//                lineFn(0, TTLexerTokenType.Word),
//                lineFn(0, TTLexerTokenType.Separator),
//                lineFn(1, TTLexerTokenType.Indent),
//                lineFn(1, TTLexerTokenType.Word),
//                lineFn(1, TTLexerTokenType.Separator),
//                lineFn(2, TTLexerTokenType.Word),
//                lineFn(2, TTLexerTokenType.Separator)
//            );
//        }

//        [TestMethod]
//        public void Number () {
//            var testString = "%abc{foo=123}";

//            TTLexer lexer = new TTLexer(new MemoryStream(Encoding.UTF8.GetBytes(testString)));
//            lexer.ExpectTokens(
//                TTLexerTokenType.PercentSign,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.LeftBrace,
//                TTLexerTokenType.Word,
//                TTLexerTokenType.Equals,
//                TTLexerTokenType.Number,
//                TTLexerTokenType.RightBrace,
//                TTLexerTokenType.Separator,
//                TTLexerTokenType.Invalid
//            );
//        }

//        [TestMethod]
//        public void Exercise_01 () {
//            var testString = @"%body
//  %form{action=""~/Finance/ExpenseReport/New"" method=""post""}
//    %table.data-form
//      %thead
//        %tr
//          - foreach (var e in ViewData.Model.ExpenseCategories)
//            %th $e.Title
//      %tbody
//        - foreach (var i in (0).UpTo(15))
//          %tr
//            %td.x-date %input.x-date
//            - foreach (var e in ViewData.Model.ExpenseCategories)
//              %td %input.x-expenseitem{name=e.Id, value=""0.00""}
//            %td.x-expensetotal \$.00
//        %tr.x-totals
//          %td Totals:
//          - foreach (var e in ViewData.Model.ExpenseCategories)
//            %td.x-coltotal \$0.00
//          %td#x-grandtotal \$0.00
//        %tr
//          %td{colspan=ViewData.Model.ExpenseCategories + 2} %input{type=""submit""}";

//            var lexer = new TTLexer(new MemoryStream(Encoding.UTF8.GetBytes(testString)));
//            var tokens = new List<TTLexerToken>();

//            lexer.Advance();
//            while (lexer.CurrentToken.Type != TTLexerTokenType.Invalid) {
//                tokens.Add(lexer.CurrentToken);
//                lexer.Advance();
//            }
//        }
//    }
}
