﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Text.TreeTemplate;
using System.Xml.Linq;

namespace Nen.Tests.Text.TreeTemplate {
	[TestClass]
	public class BuilderTests {
		private static TTElementExpression ParseElement (string text) {
			return (TTElementExpression)new TTParser().Parse(text).Children.First();
		}

		[TestMethod]
		public void BuildSimpleElement () {
			var xml = TTXElementBuilder.Convert(ParseElement("%html%head%title Test page;;%body.debug{onunload=\"foo()\"}this is a body"));

			Assert.IsTrue(xml != null);
		}

		[TestMethod]
		public void SimpleSubstitution () {
			var env = new Dictionary<string, object>();

			env["Title"] = "Test 123";
			env["Background"] = "#fefe00";
			env["OtherVariable"] = 456;

			var xml = TTXElementBuilder.Convert(ParseElement(@"
%html %head %title @Title
	%body{bgcolor=Background}
		@(OtherVariable + 2)
"), env);

			Assert.IsTrue(xml != null);
		}

		[TestMethod]
		public void SimpleTagLibrary () {
			var xml = TTXElementBuilder.Convert(new TTParser().Parse(@"
!import test Nen.Tests.Text.TreeTemplate.TestTagLibrary, Nen.Tests
%html
  %body
    %test:funTimes
	%test:coolXml{someAttr=57}
"));

			var xmlStr = xml.ToString();
			Assert.IsTrue(xmlStr.Contains("xml57"));
			Assert.IsTrue(xmlStr.Contains("Oh yeah!"));
		}

		[TestMethod]
		public void PropertyReference () {
			var env = new Dictionary<string, object>();
			env["test"] = new { Foo = 123 };
			var xml = TTXElementBuilder.Convert(new TTParser().Parse("%span foo %em @test.Foo"), env);
			Assert.AreEqual("<span>foo<em>123</em></span>", xml.ToString());
		}

		[TestMethod]
		public void ForEachSimpleArray () {
			var env = new Dictionary<string, object>();
			env["ary"] = new int[] { 1, 2, 3, 4, 5 };

			var xml = TTXElementBuilder.Convert(new TTParser().Parse(@"
%ul
  - foreach item in ary
    %li @item
"), env);

			Assert.AreEqual("<ul><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li></ul>", 
				xml.ToString().Replace("\r","").Replace("\n","").Replace("\t","").Replace(" ", ""));
		}

		[TestMethod]
		public void ForEachNested () {
			var env = new Dictionary<string, object>();
			env["ary"] = new int[] { 1, 2, 3, 4, 5 };

			var xml = TTXElementBuilder.Convert(new TTParser().Parse(@"
%ul
  - foreach item in ary
    - foreach item2 in ary
		%li @item @item2
"), env);
			Assert.AreEqual("<ul>\r\n  <li>1 1</li>\r\n  <li>1 2</li>\r\n  <li>1 3</li>\r\n  <li>1 4</li>\r\n  <li>1 5</li>\r\n  <li>2 1</li>\r\n  <li>2 2</li>\r\n  <li>2 3</li>\r\n  <li>2 4</li>\r\n  <li>2 5</li>\r\n  <li>3 1</li>\r\n  <li>3 2</li>\r\n  <li>3 3</li>\r\n  <li>3 4</li>\r\n  <li>3 5</li>\r\n  <li>4 1</li>\r\n  <li>4 2</li>\r\n  <li>4 3</li>\r\n  <li>4 4</li>\r\n  <li>4 5</li>\r\n  <li>5 1</li>\r\n  <li>5 2</li>\r\n  <li>5 3</li>\r\n  <li>5 4</li>\r\n  <li>5 5</li>\r\n</ul>",
				xml.ToString());
		}

		[TestMethod]
		public void AssignScalar () {
			var xml = TTXElementBuilder.Convert(new TTParser().Parse(@"
%ul
	- foo := 1
	@foo
"));

			Assert.AreEqual("<ul>1</ul>", xml.ToString());
		}
	}

	public class TestTagLibrary {
		public object FunTimes (TTXElementBuilder evaluator, TTElementExpression expr) {
			return "Oh yeah!";
		}

		public object CoolXml (TTXElementBuilder evaluator, TTElementExpression expr) {
			return new XElement("div",
				new XAttribute("class", "foo"),
				new XText("this is some xml" + 
					expr.Attributes.First(a => a.Name == "someAttr").Value.Accept(evaluator)));
		}
	}
}
