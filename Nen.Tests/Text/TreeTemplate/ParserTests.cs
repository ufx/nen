﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Text.TreeTemplate;

namespace Nen.Tests.Text.TreeTemplate {
	[TestClass]
	public class ParserTests {
		private static TTElementExpression ParseElement (string text) {
			return (TTElementExpression)new TTParser().Parse(text).Children.First();
		}

		[TestMethod]
		public void SimpleElement () {
			var testString = "%bar.baz#quux";
			var expr = (TTElementExpression)new TTParser().Parse(testString).Children.First();

			Assert.AreEqual("bar", expr.Name);
			Assert.AreEqual("baz", expr.Class);
			Assert.AreEqual("quux", expr.ID);
		}

		[TestMethod]
		public void AttributedElement () {
			var testString = "%bar.baz#quux{foo=123 bar}";
			var expr = (TTElementExpression)new TTParser().Parse(testString).Children.First();

			Assert.AreEqual(2, expr.Attributes.Count);
			Assert.AreEqual("foo", expr.Attributes[0].Name);
			Assert.AreEqual(123m, ((TTLiteralExpression)expr.Attributes[0].Value).Value);
			Assert.AreEqual("bar", expr.Attributes[1].Name);
			Assert.AreEqual(true, ((TTLiteralExpression)expr.Attributes[1].Value).Value);
		}

		[TestMethod]
		public void NestedInlineElement () {
			var testString = "%bar %baz";
			var expr = (TTElementExpression)new TTParser().Parse(testString).Children.First();

			Assert.AreEqual("bar", expr.Name);
			Assert.AreEqual(1, expr.Children.Count);

			var child = (TTElementExpression)expr.Children[0];
			Assert.AreEqual("baz", child.Name);
			Assert.AreEqual(0, child.Children.Count);
		}

		[TestMethod]
		public void NestedInlineElementList () {
			var testString = "%bar %baz; %quux";
			var expr = (TTElementExpression)new TTParser().Parse(testString).Children.First();

			Assert.AreEqual("bar", expr.Name);
			Assert.AreEqual(2, expr.Children.Count);

			Assert.AreEqual("baz", ((TTElementExpression)expr.Children[0]).Name);
			Assert.AreEqual("quux", ((TTElementExpression)expr.Children[1]).Name);
		}

		[TestMethod]
		public void NestedDeepInlineElementList () {
			var testString = "%bar %baz; %quux %foo; %bar;; %gwah";
			var expr = (TTElementExpression)new TTParser().Parse(testString).Children.First();

			Assert.AreEqual("bar", expr.Name);
			Assert.AreEqual(3, expr.Children.Count);

			var child0 = (TTElementExpression)expr.Children[0];
			var child1 = (TTElementExpression)expr.Children[1];
			var child2 = (TTElementExpression)expr.Children[2];

			Assert.AreEqual("baz", child0.Name);
			Assert.AreEqual("quux", child1.Name);
			Assert.AreEqual("gwah", child2.Name);
			Assert.AreEqual(2, child1.Children.Count);

			var child1_0 = (TTElementExpression)child1.Children[0];
			var child1_1 = (TTElementExpression)child1.Children[1];

			Assert.AreEqual("foo", child1_0.Name);
			Assert.AreEqual("bar", child1_1.Name);
		}

		[TestMethod]
		public void PlainText () {
			var testString = @"%foo -- SDIFH sSKDJHF @$$%BANG!11
  this is plain text too!
  --    this is also plain text, but the double dash is stripped!";

			var expr = (TTElementExpression)new TTParser().Parse(testString).Children.First();
			Assert.AreEqual("foo", expr.Name);
			Assert.AreEqual(3, expr.Children.Count);

			var child0 = (TTLiteralExpression)expr.Children[0];
			var child1 = (TTLiteralExpression)expr.Children[1];
			var child2 = (TTLiteralExpression)expr.Children[2];

			Assert.AreEqual(" SDIFH sSKDJHF @$$%BANG!11", child0.Value);
			Assert.AreEqual("this is plain text too!", child1.Value);
			Assert.AreEqual("    this is also plain text, but the double dash is stripped!", child2.Value);
		}

		[TestMethod]
		public void NestedElement () {
			var testString = @"%foo
  %bar
    %yar{a=5}
  %baz";
			var expr = (TTElementExpression)new TTParser().Parse(testString).Children.First();

			Assert.AreEqual("foo", expr.Name);
			Assert.AreEqual(2, expr.Children.Count);

			var child0 = (TTElementExpression)expr.Children[0];
			var child1 = (TTElementExpression)expr.Children[1];

			Assert.AreEqual("bar", child0.Name);
			Assert.AreEqual("baz", child1.Name);

			Assert.AreEqual(1, child0.Children.Count);

			var child0_1 = (TTElementExpression)child0.Children[0];
			Assert.AreEqual("yar", child0_1.Name);
			Assert.AreEqual(1, child0_1.Attributes.Count);
			Assert.AreEqual("a", child0_1.Attributes[0].Name);
		}

		[TestMethod]
		public void ElementLiteralText () {
			var testString = "%foo this is  a test!! %bar also a test; %baz huh";
			var expr = (TTElementExpression)new TTParser().Parse(testString).Children.First();

			Assert.AreEqual("foo", expr.Name);
			Assert.AreEqual(3, expr.Children.Count);
			Assert.AreEqual("this is  a test!!", ((TTLiteralExpression)expr.Children[0]).Value);
			var child1 = (TTElementExpression)expr.Children[1];
			var child2 = (TTElementExpression)expr.Children[2];
			Assert.AreEqual("bar", child1.Name);
			Assert.AreEqual(1, child1.Children.Count);
			Assert.AreEqual("also a test", ((TTLiteralExpression)child1.Children[0]).Value);
			Assert.AreEqual("baz", child2.Name);
			Assert.AreEqual("huh", ((TTLiteralExpression)child2.Children[0]).Value);
		}

		[TestMethod]
		public void AdditiveExpression () {
			var expr = ParseElement("%foo{a=2*2}");

			Assert.AreEqual("foo", expr.Name);
			Assert.AreEqual(1, expr.Attributes.Count);

			Assert.AreEqual("a", expr.Attributes[0].Name);
			Assert.IsInstanceOfType(expr.Attributes[0].Value, typeof(TTBinaryExpression));

			var attrVal = (TTBinaryExpression)expr.Attributes[0].Value;
			Assert.IsInstanceOfType(attrVal.Left, typeof(TTLiteralExpression));
			Assert.AreEqual(TTBinaryOperator.Times, attrVal.Operator);
			Assert.IsInstanceOfType(attrVal.Right, typeof(TTLiteralExpression));
		}

		[TestMethod]
		public void Exercise_01 () {
			var cont = new TTParser().Parse(@"
%html
  %head %title Test Page
    %script{src=""/JavaScript/Foo"" type=""text/javascript""}
  %body
     .header


       %ul.menu
         %li Menu item 1
         %li Menu item 2
         %li Menu item 3
     / Comment time!
     .content
       %p -- Here is some kick ass content. / No comment! Actual slash.
          -- Oh yeah!
          -- You need the dashes because normally . starts a new element =(
     .footer
       %p -- Copyright (c) 2008 Nen Software LLC. All Rights Reserved.
       %ul %li Privacy; %li Contact Us; %li Powered by Nen
");
		}

		[TestMethod]
		public void Exercise_02 () {
			var expr = ParseElement(@"
.library.init{data-library-id=Model.Id}
  .box-header
    - if ViewData.Maximized
      %a.nounderline{href=Url.Action(""Index"")} 
        %img.right{src=Url.Content(""~/Content/24/close.png"") width=24 height=24 alt=""Close""}
    %a.nounderline{href=Url.Action(""Index"" {libraryId=Model.Id})}
      %img{src=Url.Content(""~/Content/24/library.png"") width=24 height=24 alt=""Library""}
      @Model.LibraryName
  .library-container
    .tree-icon{onclick=""$(this).parent().parent().toggleTreeState('library')""}
    - if ViewData.Account.IsAdministrator
      %a.fancybox.nounderline{href=Url.Action(""EditLibrary"" {libraryId=Model.Id})}
        %img{src=Url.Content(""~/Content/16/edit.png"") width=16 height=16}
    %span{onclick=""$(this).parent().parent().toggleTreeState('library')""} @Model.LibraryName Library
    .library-contents
      - foreach folder in Model.Roots()
        @Html.RenderPartial(""Folder"" folder)
      - foreach document in Model.Documents
        @Html.RenderPartial(""Document"" document)
      - if ViewData.Account.IsAdministrator
        %a.fancybox{href=Url.Action(""CreateFolder"" {libraryId=Model.Id})} Add Folder
        %a.fancybox{href=Url.Action(""CreateDocument"" {libraryId=Model.Id})} Add Document
");
		}

		[TestMethod]
		public void ImportDirective () {
			var expr = (TTImportDirectiveExpression)new TTParser().Parse("!import mvc clr-namespace:Nen.Web.Mvc.Tags").Children[0];

			Assert.AreEqual("import", expr.DirectiveName);
			Assert.AreEqual("mvc", expr.Prefix);
			Assert.AreEqual("clr-namespace:Nen.Web.Mvc.Tags", expr.Url);
		}

		[TestMethod]
		public void MultipleClasses () {
			var testString = @"
%foo.bar.baz -- Select the pistol, then select your horse.
";

			var expr = ParseElement(testString);

			Assert.AreEqual(1, expr.Children.Count);
			Assert.AreEqual("bar baz", expr.Class);
		}

		[TestMethod]
		public void NestedMultipleClasses () {
			var testString = @"
%.foo %.bar %.baz
";

			var expr = ParseElement(testString);
		}

		[TestMethod]
		public void ClassVsElementWhitespace () {
			var expr = ParseElement(@".foo.bar .baz this has text");

			Assert.AreEqual(1, expr.Children.Count);
			Assert.AreEqual("foo bar", expr.Class);

			var child = (TTElementExpression)expr.Children[0];

			Assert.AreEqual("baz", child.Class);
			Assert.AreEqual(1, child.Children.Count);
			Assert.IsTrue(child.Children[0] is TTLiteralExpression);
			Assert.AreEqual("this has text", ((TTLiteralExpression)child.Children[0]).Value);
		}

		[TestMethod]
		public void WackyChars () {
			var expr = ParseElement("%foo ABC Ã DEF");

			Assert.IsInstanceOfType(expr.Children[0], typeof(TTLiteralExpression));
			Assert.AreEqual("ABC Ã DEF", ((TTLiteralExpression)expr.Children[0]).Value);
		}

		[TestMethod]
		public void TabWidthDirective () {
			var testString = @"
!tabs 3
%foo
	%tabChild
   %spaceChild
 %nonChild
";

			var expr = (TTContainerExpression)new TTParser().Parse(testString);


			Assert.AreEqual(2, expr.Children.Count);
			Assert.IsInstanceOfType(expr.Children[0], typeof(TTElementExpression));
			Assert.AreEqual(2, ((TTElementExpression)expr.Children[0]).Children.Count);
			Assert.IsInstanceOfType(expr.Children[1], typeof(TTLiteralExpression));
			Assert.AreEqual("%nonChild", ((TTLiteralExpression)expr.Children[1]).Value);
		}

		[TestMethod]
		public void ExpressionInterpolation () {
			var expr = ParseElement("%foo some literal text @Variable @@lit-at @{2}+2");

			Assert.AreEqual(5, expr.Children.Count);
			Assert.AreEqual("some literal text ", ((TTLiteralExpression)expr.Children[0]).Value);
			Assert.AreEqual("Variable", ((TTIdentifierExpression)expr.Children[1]).Name);
			Assert.AreEqual(" @lit-at ", ((TTLiteralExpression)expr.Children[2]).Value);
			Assert.AreEqual((decimal)2, ((TTLiteralExpression)expr.Children[3]).Value);
			Assert.AreEqual("+2", ((TTLiteralExpression)expr.Children[4]).Value);
		}

		[TestMethod]
		public void CondBlock () {
			var expr = ParseElement(@"
%foo
  - case
    - when x == 4
      do some stuff
    - when y == 4
      y is 4
    - default
      do some other stuff
");

			Assert.AreEqual(1, expr.Children.Count);
		}

		[TestMethod]
		public void IfBlock () {
			var expr = ParseElement(@"
%foo
	- if x > 4
		One thing
	- else if x < 10
		Other thing
	- else
		Third thing
");

			var cond = (TTConditionExpression)expr.Children[0];

			Assert.AreEqual(2, cond.Conditions.Count);
			Assert.IsNotNull(cond.Alternative);
			Assert.IsTrue(((TTBinaryExpression)cond.Conditions[0].Test).Operator == TTBinaryOperator.Gt);

			expr = ParseElement(@"
%foo
	- if x > 4
		One thing
	- case
		- when x < 10
			Other thing
	Content
");

			Assert.AreEqual(3, expr.Children.Count);
			Assert.IsInstanceOfType(expr.Children[0], typeof(TTConditionExpression));
			Assert.IsInstanceOfType(expr.Children[1], typeof(TTConditionExpression));

			expr = ParseElement(@"
%foo
	- if x > 4
		- if y > 4
			One
		- else
			Two
	- else
		- if z > 4
			Three
		- else
			Four
");

			Assert.AreEqual(1, expr.Children.Count);
		}

		[TestMethod]
		public void ForeachBlock () {
			var expr = ParseElement(@"
%foo
	- foreach foo in 123
		do stuff
		do more stuff
");

			Assert.AreEqual(1, expr.Children.Count);

			var forexpr = (TTForeachExpression)expr.Children[0];

			Assert.AreEqual(2, forexpr.Children.Count);
			Assert.AreEqual("foo", forexpr.VariableName.Name);
		}

		[TestMethod]
		public void UsingBlock () {
			var expr = ParseElement(@"
%foo
	- using testSource
		that one doesn't have a variable.
		- using foo := otherSource
			This one does.
");
		}
	}
}
