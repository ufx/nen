using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Validation;

namespace Nen.Tests.Validation {
    [TestClass]
    public class ValidationAttributeTests {
        [TestMethod]
        public void VariableValidation () {
            var c = new Car();
            var errors = c.Validate();
            Assert.AreEqual(3, errors.Count);

            c.Name = "Large";
            c.NumberOfMissiles = 99;
            c.Born = new DateTime(2500, 01, 02);
            errors = c.Validate();
            Assert.AreEqual(2, errors.Count);
        }

        [TestMethod]
        public void CustomValidation () {
            var car = new Car();
            car.Name = "Sissy";
            car.Born = new DateTime(2003, 04, 05);
            car.NumberOfMissiles = 9;
            var errors = car.Validate();
            Assert.AreEqual(1, errors.Count);

            car.Name = "Kuro";
            errors = car.Validate();
            Assert.AreEqual(0, errors.Count);
        }

        #region Test Class
        
        public class Car : Validatable {
            #region Constructors
            public Car () {
            }
            #endregion

            #region Validation
            public static ValidationResult CombatReady (string name, ValidationContext context) {
                if (name == "Sissy")
                    return new ValidationResult("Not ready for combat with a name like that!", new string[] { "Name" });
                return ValidationResult.Success;
            }
            #endregion

            #region Accessors
            [Range(typeof(DateTime), "01/01/2000", "12/31/2010")]
            public DateTime Born { get; set; }

            [CustomValidation(typeof(Car), "CombatReady")]
            [Required(AllowEmptyStrings = false), StringLength(20, MinimumLength = 3)]
            public string Name { get; set; }

            [Range(2, 10)]
            public int NumberOfMissiles { get; set; }
            #endregion
        }
        #endregion
    }
}
