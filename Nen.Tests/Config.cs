using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Nen.Tests
{
    public static class Config
    {
        public static string SqlConnectionString
        {
            get { return ConfigurationManager.AppSettings["ConnectionString"]; }
        }

        public static Type DatabaseType
        {
            get { return Type.GetType(ConfigurationManager.AppSettings["DatabaseTypeName"]); }
        }

        public static Type ConventionsType
        {
            get { return Type.GetType(ConfigurationManager.AppSettings["ConventionsTypeName"]); }
        }
    }
}
