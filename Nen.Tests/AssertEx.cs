﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nen.Tests {
    /// <summary>
    /// Extensions to the Microsoft.VisualStudio.TestTools.UnitTesting.Assert class.
    /// </summary>
    public static class AssertEx {
        /// <summary>
        /// Asserts that an exception is thrown upon executing an action.
        /// </summary>
        /// <typeparam name="TException">The type of exception to catch.</typeparam>
        /// <param name="action">The action that must throw the exception.</param>
        public static void ExceptionThrown<TException> (Action action) where TException : Exception {
            var caughtException = CheckException<TException>(action);
            Assert.IsTrue(caughtException, string.Format(CultureInfo.CurrentCulture, "Failed to catch exception of type '{0}'", typeof(TException)));
        }

        /// <summary>
        /// Checks that an exception is thrown upon executing an action.
        /// </summary>
        /// <typeparam name="TException">The type of exception to check.</typeparam>
        /// <param name="action"></param>
        /// <returns>true if the exception was thrown and caught, false if no exception was thrown.</returns>
        public static bool CheckException<TException> (Action action) where TException : Exception {
            try {
                action();
            } catch (TException) {
                return true;
            }

            return false;
        }

        // Determines if the actual value matches any of the given expected values.
        public static bool AreEqual(object actual, params object[] anyExpected)
        {
            if (actual == null && anyExpected == null)
                return true;

            if (actual == null || anyExpected == null)
                return false;

            return anyExpected.Any(e => Equals(e, actual));
        }
    }
}
