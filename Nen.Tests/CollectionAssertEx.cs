﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nen.Tests {
    public static class CollectionAssertEx {
        public static void AreSame(ICollection expected, ICollection actual) {
            Assert.AreEqual(expected.Count, actual.Count);

            var expectedList = new List<object>(expected.Cast<object>());
            var actualList = new List<object>(actual.Cast<object>());

            for (int i = 0; i < expectedList.Count; i++)
                Assert.AreSame(expectedList[i], actualList[i]);
        }
    }
}
