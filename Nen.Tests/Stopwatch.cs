﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Nen.Tests {
    public class Stopwatch {
        #region Accessors
        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }

        public TimeSpan Interval {
            get { return EndTime - StartTime; }
        }
        #endregion

        public void Start () {
            StartTime = DateTime.Now;
        }

        public void Stop () {
            EndTime = DateTime.Now;
        }

        public void Record (string name, Action action) {
            Start();
            action();
            Stop();
            Debug.WriteLine("{0}: {1}ms", name, Math.Round(Interval.TotalMilliseconds, 3));
        }
    }
}
