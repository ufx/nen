﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using Nen.Linq.Expressions;

namespace Nen.Tests.Linq.Expressions {
    [TestClass]
    public class TransformTests {
        [TestMethod]
        public void SubstituteParameters () {
            Expression<Func<string, string>> expr =
                p => ((Func<string, string>)((string p2) => p2 + p))("Test") + p;

            var dict = new Dictionary<string, Expression>();
            dict["p"] = Expression.Constant("2");

            var transform = new SubstituteParametersTransform(dict);
            var transformedExpr = (LambdaExpression)transform.Transform(expr);

            var compiledExpr = expr.Compile();
            var compiledTransformedExpr = transformedExpr.Compile();

            Assert.AreEqual("Test11", compiledExpr("1"));
            Assert.AreEqual("Test22", compiledTransformedExpr.DynamicInvoke("1"));
        }
    }
}
