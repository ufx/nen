﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nen.Tests
{
    [TestClass]
    public class ConvertTests
    {
        [TestMethod]
        public void ConvertNumberToEnum()
        {
            Assert.AreEqual(TestEnum.Foo, ConvertEx.ChangeWeakType(1, typeof(TestEnum)));
            Assert.AreEqual(TestEnum.Bar, ConvertEx.ChangeWeakType(2, typeof(TestEnum)));
            Assert.AreEqual(TestEnum.Bar, ConvertEx.ChangeWeakType((byte)2, typeof(TestEnum)));
            Assert.AreEqual(TestEnum.Baz, ConvertEx.ChangeWeakType((short)3, typeof(TestEnum)));
        }

        [TestMethod]
        public void ConvertStringToEnum()
        {
            Assert.AreEqual(TestEnum.Baz, ConvertEx.ChangeWeakType("baz", typeof(TestEnum)));
            Assert.AreEqual(TestEnum.Baz, ConvertEx.ChangeWeakType("Baz", typeof(TestEnum)));
            Assert.AreEqual(TestEnum.Foo, ConvertEx.ChangeWeakType("Foo", typeof(TestEnum)));
        }

        [TestMethod]
        public void ConvertNullableEnums()
        {
            Assert.AreEqual(TestEnum.Baz, ConvertEx.ChangeWeakType(3, typeof(TestEnum?)));
            Assert.IsNull(ConvertEx.ChangeWeakType(null, typeof(TestEnum?)));
            Assert.AreEqual(TestEnum.Bar, ConvertEx.ChangeWeakType("Bar", typeof(TestEnum?)));
        }

        [TestMethod]
        public void ConvertClassToInterface()
        {
            var list = new List<int>();
            Assert.AreSame(list, ConvertEx.ChangeWeakType(list, typeof(IList<int>)));
        }

        private enum TestEnum { Foo = 1, Bar, Baz }
    }
}
