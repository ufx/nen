﻿#if NET4
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;

// todo test: structs
// todo test: collections of primitives
// todo test: serialize arrays

namespace Nen.Tests.Runtime {
    [TestClass]
    public class XSerializerTests {
        [TestCategory("Benchmarks"), TestMethod]
        public void BenchmarkSimpleSerialize () {
            var serializer = new XSerializer(typeof(XSimpleTest));
            var watch = new Stopwatch();
            var xmlSerializer = new XmlSerializer(typeof(XSimpleTest));
            var dataContractSerializer = new DataContractSerializer(typeof(XSimpleTest));
            var guid = Guid.NewGuid();

            xmlSerializer.Serialize(new MemoryStream(), new XSimpleTest { A = 0, B = "", C = "", D = guid });
            dataContractSerializer.WriteObject(new MemoryStream(), new XSimpleTest { A = 0, B = "", C = "", D = guid });

            watch.Record("Serialize XSerializer", () => {
                for (var i = 0; i < 100000; ++i) {
                    var ms = new MemoryStream();
                    serializer.WriteObject(ms, new XSimpleTest { A = i, B = "foo", C = "bar", D = guid });
                }
            });

            watch.Record("Serialize XmlSerializer", () => {
                for (var i = 0; i < 100000; ++i) {
                    var ms = new MemoryStream();
                    xmlSerializer.Serialize(ms, new XSimpleTest { A = i, B = "foo", C = "bar", D = guid });
                }
            });

            watch.Record("Serialize DataContract", () => {
                for (var i = 0; i < 100000; ++i) {
                    var ms = new MemoryStream();
                    dataContractSerializer.WriteObject(ms, new XSimpleTest { A = i, B = "foo", C = "bar", D = guid });
                }
            });

            watch.Record("Serialize DataContract to DOM", () => {
                for (var i = 0; i < 100000; ++i) {
                    var doc = new XDocument();
                    using (var writer = doc.CreateWriter()) {
                        dataContractSerializer.WriteObject(writer, new XSimpleTest { A = i, B = "foo", C = "bar", D = guid });
                    }
                }
            });
        }

        [TestMethod]
        public void NullableTypeSerialize () {
            var write = new XNullableTest() { A = 5, B = null, C = 1313, D = 4424 };

            var serializer = new XSerializer(typeof(XNullableTest));
            var text = serializer.WriteObject(write);
            Assert.AreEqual("<XNullableTest C=\"1313\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><A>5</A><D>4424</D></XNullableTest>", text);

            var read = (XNullableTest) serializer.ReadObject(text);
            Assert.AreEqual(5, read.A);
            Assert.IsNull(read.B);
            Assert.AreEqual(1313, read.C);
            Assert.AreEqual(4424, read.D);
        }

        [TestMethod]
        public void AbstractBaseClassSerialize () {
            var write = new XConcreteTest() { BaseFoo = "Bar", ConcreteValue = 1231.342m };

            var serializer = new XSerializer(typeof(XConcreteTest));
            var text = serializer.WriteObject(write);
            Assert.AreEqual("<XConcreteTest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><BaseFoo>Bar</BaseFoo><ConcreteValue>1231.342</ConcreteValue></XConcreteTest>", text);

            var read = (XConcreteTest) serializer.ReadObject(text);
            Assert.AreEqual("Bar", read.BaseFoo);
            Assert.AreEqual(1231.342m, read.ConcreteValue);
        }

        [TestMethod]
        public void UnknownTypeSerialize () {
            var write = new XUnknownTypeTest() { Value = "String" };

            var serializer = new XSerializer(typeof(XUnknownTypeTest));
            var text = serializer.WriteObject(write);
            Assert.AreEqual("<XUnknownTypeTest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Value xsi:type=\"System.String\">String</Value></XUnknownTypeTest>", text);

            var read = (XUnknownTypeTest) serializer.ReadObject(text);
            Assert.AreEqual("String", read.Value);
        }

        [TestMethod]
        public void PrimitiveCollectionSerialize () {
            var write = new XPrimitiveCollectionTest();
            write.Numbers = new List<int>();
            write.Numbers.Add(4);
            write.Numbers.Add(8);

            var serializer = new XSerializer(typeof(XPrimitiveCollectionTest));
            var text = serializer.WriteObject(write);
            Assert.AreEqual("<XPrimitiveCollectionTest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Numbers><Item>4</Item><Item>8</Item></Numbers></XPrimitiveCollectionTest>", text);

            var read = (XPrimitiveCollectionTest) serializer.ReadObject(text);
            Assert.AreEqual(2, read.Numbers.Count);
            Assert.AreEqual(4, read.Numbers[0]);
            Assert.AreEqual(8, read.Numbers[1]);
        }

        [TestMethod]
        public void CollectionSerialize () {
            var write = new XCollectionTest();
            
            write.Members.Add(new XSimpleTest { A = 1, B = "2", C = "3" });
            write.Members.Add(new XSimpleTest { A = 4, B = "5", C = "6" });

            var serializer = new XSerializer(typeof(XCollectionTest));
            var text = serializer.WriteObject(write);
            var read = (XCollectionTest)serializer.ReadObject(text);

            Assert.AreEqual(1, read.Members[0].A);
            Assert.AreEqual("2", read.Members[0].B);
            Assert.AreEqual("3", read.Members[0].C);
            Assert.AreEqual(4, read.Members[1].A);
            Assert.AreEqual("5", read.Members[1].B);
            Assert.AreEqual("6", read.Members[1].C);
        }

        [TestMethod, Ignore]
        public void ArraySerialize () {
            var write = new XArrayTest();
            write.Numbers = new int[] { 1, 2 };
            write.Stuff = new object[] { "Foo", 123.34m };

            var list = (Array) write.Numbers;

            var serializer = new XSerializer(typeof(XArrayTest));
            var text = serializer.WriteObject(write);
            Assert.AreEqual("<XArrayTest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Numbers><Item>1</Item><Item>2</Item></Numbers><Stuff><Item xsi:type=\"System.String\">Foo</Item><Item xsi:type=\"System.Decimal\">123.34</Item></Stuff></XArrayTest>", text);

            var read = (XArrayTest) serializer.ReadObject(text);
            Assert.AreEqual(2, read.Numbers.Length);
            Assert.AreEqual(1, read.Numbers[0]);
            Assert.AreEqual(2, read.Numbers[1]);
            Assert.AreEqual(2, read.Stuff.Length);
            Assert.AreEqual("Foo", read.Stuff[0]);
            Assert.AreEqual(123.34m, read.Stuff[1]);
        }
    }

    [XDataContract, DataContract]
    public class XSimpleTest {
        [XDataMember, DataMember]
        public int A { get; set; }

        [XDataMember, DataMember]
        public string B { get; set; }

        [XDataMember, DataMember]
        public string C { get; set; }

        [XDataMember, DataMember]
        public Guid D { get; set; }
    }

    [XDataContract]
    public class XNullableTest {
        [XDataMember]
        public int? A { get; set; }

        [XDataMember(XDataType.Attribute)]
        public int? B { get; set; }

        [XDataMember(XDataType.Attribute)]
        public int? C { get; set; }

        [XDataMember]
        public int D { get; set; }
    }

    [XDataContract]
    public abstract class XAbstractTest {
        [XDataMember]
        public string BaseFoo { get; set; }
    }

    [XDataContract]
    public class XConcreteTest : XAbstractTest {
        [XDataMember]
        public decimal ConcreteValue { get; set; }
    }

    [XDataContract]
    public class XCollectionTest {
        [XDataMember]
        public List<XSimpleTest> Members { get; set; }

        public XCollectionTest () {
            Members = new List<XSimpleTest>();
        }
    }

    [XDataContract]
    public class XUnknownTypeTest {
        [XDataMember]
        public object Value { get; set; }
    }

    [XDataContract]
    public class XPrimitiveCollectionTest {
        [XDataMember]
        public List<int> Numbers { get; set; }
    }

    [XDataContract]
    public class XArrayTest {
        [XDataMember]
        public object[] Stuff { get; set; }

        public int[] Numbers { get; set; }
    }
}
#endif