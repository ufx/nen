using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Reflection;

namespace Nen.Tests.Reflection {
    [TestClass]
    public class AssemblyExTests {
        [TestMethod]
        public void IsMicrosoftAssemblyTest () {
            Assert.IsTrue(typeof(System.Random).Assembly.IsMicrosoftAssembly());
            Assert.IsFalse(typeof(AssemblyExTests).Assembly.IsMicrosoftAssembly());
        }
    }
}
