using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Reflection;

namespace Nen.Tests.Reflection {
    [TestClass]
    public class VariableInfoTests {
        [TestMethod]
        public void FieldAccess () {
            TestClass c = new TestClass();

            VariableInfo field = typeof(TestClass).GetVariable("_field");
            Assert.IsNotNull(field);
            Assert.AreEqual(99, field.GetValue(c));
            field.SetValue(c, 67);
            Assert.AreEqual(67, c.Field);

            bool caughtException = false;
            try {
                VariableInfo nonfield = typeof(TestClass).GetVariable("_nonfield");
            } catch (MissingMemberException) {
                caughtException = true;
            }
            Assert.IsTrue(caughtException);
        }

        [TestMethod]
        public void PropertyAccess () {
            TestClass c = new TestClass();

            VariableInfo property = typeof(TestClass).GetVariable("Property");
            Assert.IsNotNull(property);
            Assert.AreEqual("bar", property.GetValue(c));
            property.SetValue(c, "foo");
            Assert.AreEqual("foo", c.Property);

            bool caughtException = false;
            try {
                VariableInfo nonproperty = typeof(TestClass).GetVariable("NonProperty");
            } catch (MissingMemberException) {
                caughtException = true;
            }
            Assert.IsTrue(caughtException);
        }

        [TestMethod]
        public void WeakTypeConversions () {
            TestClass c = new TestClass();

            // byte -> bool
            VariableInfo boolField = new WeaklyTypedVariableInfo(typeof(TestClass).GetVariable("BoolField"));
            boolField.SetValue(c, (byte) 1);
            Assert.IsTrue(c.BoolField);
            boolField.SetValue(c, (byte) 0);
            Assert.IsFalse(c.BoolField);

            // decimal -> int
            VariableInfo intField = new WeaklyTypedVariableInfo(typeof(TestClass).GetVariable("_field"));
            intField.SetValue(c, (decimal) 333);
            Assert.AreEqual(333, c.Field);

            // Int64 -> TimeSpan
            TimeSpan span = new TimeSpan(15);
            VariableInfo timeSpanField = new WeaklyTypedVariableInfo(typeof(TestClass).GetVariable("TimeSpanField"));
            timeSpanField.SetValue(c, span.Ticks);
            Assert.AreEqual(span.Ticks, c.TimeSpanField.Ticks);

            // string(1) -> char
            VariableInfo charField = new WeaklyTypedVariableInfo(typeof(TestClass).GetVariable("CharField"));
            charField.SetValue(c, "G");
            Assert.AreEqual('G', c.CharField);

            // enum -> enum?
            VariableInfo enumField = new WeaklyTypedVariableInfo(typeof(TestClass).GetVariable("TestEnumField"));
            enumField.SetValue(c, TestEnum.Apples);
            Assert.AreEqual(TestEnum.Apples, c.TestEnumField);
        }

        [TestMethod]
        public void Descriptions () {
            string typeDescription = typeof(TestClass).GetDescription();
            Assert.AreEqual("TestClass Description", typeDescription);

            string fieldDescription = typeof(TestClass).GetVariable("Field").Description;
            Assert.AreEqual("Field Description", fieldDescription);
        }

        [TestMethod]
        public void CompositeClassAccess () {
            var composite = typeof(TestClass).GetVariable(new string[] { "InnerClassField", "Value" });
            Assert.IsNotNull(composite);

            var instance = new TestClass();
            AssertEx.ExceptionThrown<NullReferenceException>(delegate { composite.SetValue(instance, "null ref on set"); });
            AssertEx.ExceptionThrown<NullReferenceException>(delegate { composite.GetValue(instance); });

            composite.PropogateNulls = true;
            Assert.IsNull(composite.GetValue(instance));

            composite.InstantiateNullComponents = true;
            composite.SetValue(instance, "inner value");
            Assert.IsNotNull(instance.InnerClassField);
            Assert.AreEqual("inner value", instance.InnerClassField.Value);
        }

        [TestMethod]
        public void CompositeStructAccess () {
            var composite = typeof(TestClass).GetVariable(new string[] { "InnerStructField", "Item" });
            Assert.IsNotNull(composite);

            var instance = new TestClass();
            instance.InnerStructField.Item = 677;
            Assert.AreEqual(677, composite.GetValue(instance));

            composite.SetValue(instance, 342);
            Assert.AreEqual(342, instance.InnerStructField.Item);
            Assert.AreEqual(342, composite.GetValue(instance));
        }

        [TestMethod]
        public void CompositeDirectAccess () {
            var composite = typeof(TestClass).GetVariable(new string[] { "CharField" });
            Assert.IsNotNull(composite);

            var instance = new TestClass();
            composite.SetValue(instance, 'b');
            Assert.AreEqual('b', instance.CharField);

            instance.CharField = 'x';
            Assert.AreEqual('x', composite.GetValue(instance));
        }

        #region Test Class
        private enum TestEnum { Apples, Oranges, Robots }

        [System.ComponentModel.Description("TestClass Description")]
        private class TestClass {
            private int _field = 99;
            private string _property = "bar";

            public bool BoolField = false;
            public char CharField = '\0';
            public TimeSpan TimeSpanField;
            public TestEnum? TestEnumField = null;
            public TestInnerClass InnerClassField = null;
            public TestInnerStruct InnerStructField;

            [System.ComponentModel.Description("Field Description")]
            public int Field {
                get { return _field; }
            }

            public string Property {
                get { return _property; }
                set { _property = value; }
            }
        }

        private class TestInnerClass {
            public string Value { get; set; }
        }

        private struct TestInnerStruct {
            public int Item { get; set; }
        }
        #endregion
    }
}
