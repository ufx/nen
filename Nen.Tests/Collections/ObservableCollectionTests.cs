using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Collections;

namespace Nen.Tests.Collections {
    [TestClass]
    public class ObservableCollectionTests {
        [TestMethod]
        public void CollectionChangeEvents () {
            ObservableCollection<int> ints = new ObservableCollection<int>();
            bool inserted = false, removed = false;
            ints.Changed += delegate (object sender, CollectionChangedEventArgs<int> e) {
                if (e.ChangeAction == CollectionChangeAction.Inserted)
                    inserted = true;
                else
                    removed = true;
            };

            ints.Add(5);
            ints.Remove(5);

            Assert.IsTrue(inserted);
            Assert.IsTrue(removed);
        }
    }
}
