﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Collections;

namespace Nen.Tests.Collections {
    [TestClass]
    public class CollectionExTests {
        [TestMethod]
        public void GetItemType () {
            var instance = new Collection<int>();
            Assert.AreEqual(typeof(int), instance.GetItemType());

            var array = new int[] { 1, 2, 3 };
            Assert.AreEqual(typeof(int), array.GetItemType());
        }
    }
}
