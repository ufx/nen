﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper {
    [TestClass]
    public class DataQueryTests {
        [TestMethod]
        public void CreateQuery () {
            using (var context = new TestDataContext()) {
                var table = context.Get<TestClass>();
                var lambda = MakeLambda(table, t => t.Foo == "Qux");

                var expression = Expression.Call(
                    typeof(Queryable), "Where",
                    new Type[] { table.ElementType },
                    table.Expression, Expression.Quote(lambda)
                );

                var resultQuery = table.CreateQuery(expression);

                Assert.IsNotNull(resultQuery);
            }
        }

        private static Expression<Func<TSource, TValue>> MakeLambda<TSource, TValue> (IQueryable<TSource> source, Expression<Func<TSource, TValue>> keySelector) {
            return keySelector;
        }

        private class TestClass {
            public string Foo { get; set; }
        }

        [TestMethod]
        public void QueryableInclude()
        {
            var list = new List<TestClass>();
            list.Add(new TestClass() { Foo = "Baz" });
            list.Add(new TestClass() { Foo = "Bar" });

            var query = list.AsQueryable().Where(l => l.Foo == "Baz").Include(l => l.Foo).Select(l => l.Foo);
            Assert.IsNotNull(query);

            var result = query.ToArray();
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Length);
            Assert.AreEqual("Baz", result[0]);
        }
    }
}
