﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class SubordinateQueryTests {
        [TestMethod]
        public void QuerySubordinateEdge () {
            using (var context = new TestDataContext()) {
                var query = context.Scroll.Where(s => s.ScrollCase != null && s.Id == new Guid("1218A3C6-668C-4E82-8586-82F55CE9F67A"));
                var list = query.ToList();
                Assert.AreEqual(1, list.Count);
                SubordinateMappingTests.CheckWordy(list[0], true);
            }
        }

        [TestMethod]
        public void QueryThroughSubordinate () {
            using (var context = new TestDataContext()) {
                var query = context.Scroll.Where(s => s.ScrollParchment.Id == new Guid("351FB9AE-6407-4745-970F-636C33AA2B41"));
                var list = query.ToList();
                Assert.AreEqual(1, list.Count);
                SubordinateMappingTests.CheckWordy(list[0], true);
            }
        }
    }
}
