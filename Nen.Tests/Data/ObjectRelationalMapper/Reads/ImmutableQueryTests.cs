﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.Immutable;
using Nen.Linq.Dynamic;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class ImmutableQueryTests {
        [TestMethod]
        public void QueryMax () {
            using (var context = new TestDataContext()) {
                var max = context.EvilTower.Max(t => t.MindControlAntennaPower);

                Assert.AreEqual(0, context.LastQueryStatistics.NumberOfContextCacheMisses);
                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(5000, max);
            }
        }

        [TestMethod]
        public void QueryDynamicFirstOrDefault () {
            using (var context = new TestDataContext()) {
                var noTower = context.Get(typeof(EvilTower)).FirstOrDefault("CodeName = @0", "nonexistant");
                Assert.IsNull(noTower);

                var tokyoTower = (EvilTower) context.Get(typeof(EvilTower)).First("CodeName = @0", "Tokyo Tower");
                ImmutableMappingTests.CheckTokyoTower(tokyoTower);
            }
        }

        [TestMethod]
        public void QueryStaticInstanceProperty () {
            using (var context = new TestDataContext()) {
                var query = from t in context.EvilTower where t == EvilTowerRepository.TokyoTower select t;
                var towers = query.ToList();

                Assert.AreEqual(1, towers.Count);
                var tokyoTower = towers[0];
                Assert.IsNotNull(tokyoTower);
                ImmutableMappingTests.CheckTokyoTower(tokyoTower);
            }
        }
    }

    public static class EvilTowerRepository {
        public static EvilTower TokyoTower {
            get {
                using (var context = new TestDataContext())
                    return context.EvilTower.Load(new Guid("BDAC851C-E510-4138-B785-638704B70B5A"));
            }
        }
    }
}
