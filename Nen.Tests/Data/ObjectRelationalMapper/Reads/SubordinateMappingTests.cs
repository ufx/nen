﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Subordinates;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class SubordinateMappingTests {
        [TestMethod]
        public void Load () {
            using (var context = new TestDataContext()) {
                var wordy = context.Scroll.Load(new Guid("1218A3C6-668C-4E82-8586-82F55CE9F67A"));

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckWordy(wordy, true);
            }
        }

        [TestMethod]
        public void LoadNoLinks () {
            using (var context = new TestDataContext()) {
                var chicken = context.Scroll.Load(new Guid("BA03F307-52CE-4200-81B7-764BF51C4F7D"));

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckChicken(chicken);
            }
        }

        [TestMethod]
        public void AdviceThroughDeferredSubordinate () {
            using (var context = new TestDataContext()) {
                var query = context.Scroll.Where(s => s.Text == "This scroll has a lot of words.")
                    .Include(s => s.ScrollParchment.Properties);
                var scrolls = query.ToArray();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.AreEqual(1, scrolls.Length);
                CheckWordy(scrolls[0], true);
            }
        }

        #region Individual Tests
        internal static void CheckWordy (Scroll wordy, bool checkCircularItems) {
            Assert.IsNotNull(wordy);
            Assert.AreEqual(new Guid("1218A3C6-668C-4E82-8586-82F55CE9F67A"), wordy.Id);
            Assert.AreEqual("This scroll has a lot of words.", wordy.Text);

            Assert.IsNotNull(wordy.ScrollCase);
            Assert.AreSame(wordy, wordy.ScrollCase.Scroll);
            if (checkCircularItems)
                CheckWisdom(wordy.ScrollCase, false);

            Assert.IsNotNull(wordy.ScrollParchment);
            Assert.AreSame(wordy, wordy.ScrollParchment.Scroll);
            if (checkCircularItems)
                CheckWordyParchment(wordy.ScrollParchment, false);
        }

        internal static void CheckWordyParchment (ScrollParchment wordyParchment, bool checkCircularItems) {
            Assert.IsNotNull(wordyParchment);
            Assert.AreEqual(new Guid("351FB9AE-6407-4745-970F-636C33AA2B41"), wordyParchment.Id);
            Assert.AreEqual(99, wordyParchment.Quality);

            Assert.IsNotNull(wordyParchment.Scroll);
            Assert.AreSame(wordyParchment, wordyParchment.Scroll.ScrollParchment);

            Assert.IsNotNull(wordyParchment.Properties);
            Assert.AreEqual(1, wordyParchment.Properties.Count);
            Assert.AreEqual("~Magic~", wordyParchment.Properties[0].Name);

            if (checkCircularItems)
                CheckWordy(wordyParchment.Scroll, false);
        }

        internal static void CheckWisdom (ScrollCase wisdom, bool checkCircularItems) {
            Assert.IsNotNull(wisdom);
            Assert.AreEqual(new Guid("847AEA0F-2B5C-4090-8C23-9690DFE71616"), wisdom.Id);
            Assert.AreEqual("Words of wisdom.", wisdom.EngravedText);

            Assert.IsNotNull(wisdom.Scroll);
            Assert.AreSame(wisdom, wisdom.Scroll.ScrollCase);
            if (checkCircularItems)
                CheckWordy(wisdom.Scroll, false);
        }

        internal static void CheckChicken (Scroll chicken) {
            Assert.IsNotNull(chicken);
            Assert.AreEqual(new Guid("BA03F307-52CE-4200-81B7-764BF51C4F7D"), chicken.Id);
            Assert.IsNull(chicken.ScrollParchment);
            Assert.IsNull(chicken.ScrollCase);
            Assert.AreEqual("This scroll has a list of popular names for chicken.", chicken.Text);
        }
        #endregion
    }
}
