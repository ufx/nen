﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.DiscriminatedSequentialKeys;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class DiscriminatedSequentialKeyMappingTests {
        #region Individual Tests
        public static void CheckBare (Fixture fixture) {
            Assert.IsNotNull(fixture);
            Assert.IsInstanceOfType(fixture, typeof(EmptyFixture));

            var bare = (EmptyFixture) fixture;
            Assert.AreEqual(1, bare.Id);
            Assert.AreEqual(42.34m, bare.Cost);
            Assert.AreEqual(FixtureType.Empty, bare.Type);
            Assert.AreEqual("GotNothing", bare.Name);
        }
        #endregion
    }
}
