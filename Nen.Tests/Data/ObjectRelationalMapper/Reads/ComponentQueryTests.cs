﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Components;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class ComponentQueryTests {
        [TestMethod]
        public void QueryThroughComponent () {
            using (var context = new TestDataContext()) {
                var query = from s in context.Starship where s.Missile.Warheads == 26 select s;
                var ships = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, ships.Count);

                var ship = ships[0];
                ComponentMappingTests.CheckShip(ship);
            }
        }

        [TestMethod]
        public void QueryAdHocView () {
            using (var context = new TestDataContext()) {
                var query = from s in context.Starship orderby s.Serial select new { Missile = s.Missile, ForePower = s.ForePhaser.Power };
                var phaserPowers = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(2, phaserPowers.Count);
                Assert.AreEqual(878, phaserPowers[0].ForePower);
                Assert.AreEqual(26, phaserPowers[0].Missile.Warheads);
                Assert.AreEqual(343, phaserPowers[1].ForePower);
                Assert.AreEqual(35, phaserPowers[1].Missile.Warheads);
            }
        }

        [TestMethod]
        public void QueryStoredProcedure () {
            using (var context = new TestDataContext()) {
                var query = from s in context.ReadStarshipBySerial("FOOCC-8909-X") select s;
                var list = query.ToList();

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                var ship = list[0];
                ComponentMappingTests.CheckShip(ship);
            }
        }

        [TestMethod]
        public void QuerySingle () {
            using (var context = new TestDataContext()) {
                var ship = context.Starship.Single(s => s.Serial == "FOOCC-8909-X");

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                ComponentMappingTests.CheckShip(ship);
            }
        }

        [TestMethod]
        public void QueryFirst () {
            using (var context = new TestDataContext()) {
                var ship = context.Starship.First();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.IsNotNull(ship);
            }
        }

        [TestMethod]
        public void QueryCombinedWhere () {
            using (var context = new TestDataContext()) {
                var query = context.Starship.Where(s => s.Serial.StartsWith("FOO")).Where(s => s.Serial.Contains("CC"));
                var ships = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, ships.Count);
                var ship = ships[0];

                ComponentMappingTests.CheckShip(ship);
            }
        }

        [TestMethod]
        public void QueryUnaryWhere () {
            using (var context = new TestDataContext()) {
                var query = context.Starship.Where(s => s.AftPhaser.Active);
                var ships = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, ships.Count);
                var ship = ships[0];
                Assert.IsNotNull(ship);
                Assert.IsTrue(ship.AftPhaser.Active);
            }
        }
    }
}
