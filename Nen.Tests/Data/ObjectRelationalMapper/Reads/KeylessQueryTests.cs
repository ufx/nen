﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.Keyless;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class KeylessQueryTests {
        [TestMethod]
        public void QueryStoredProcedure () {
            using (var context = new TestDataContext()) {
                var query = from c in context.ReadDoorByName("Portal") select c;
                var list = query.ToList();

                Assert.AreEqual(1, list.Count);
                KeylessMappingTests.CheckPortal(list[0]);
            }
        }

        [TestMethod]
        public void QueryNonGeneric () {
            using (var context = new TestDataContext()) {
                var found = false;
                foreach (var item in context.Get(typeof(Door)))
                    found = true;
                Assert.IsTrue(found);
            }
        }

        [TestMethod]
        public void QueryBooleanUnary () {
            using (var context = new TestDataContext()) {
                var portal = context.Door.FirstOrDefault(d => d.Open);
                KeylessMappingTests.CheckPortal(portal);
            }
        }

        [TestMethod]
        public void QueryBooleanNotUnary()
        {
            using (var context = new TestDataContext())
            {
                var gate = context.Door.FirstOrDefault(d => !d.Open);
                KeylessMappingTests.CheckGate(gate);
            }
        }

        [TestMethod]
        public void QueryOrderedCount () {
            using (var context = new TestDataContext()) {
                var count = context.Door.OrderBy(d => d.Name).Count();
                Assert.AreEqual(2, count);
            }
        }

        [TestMethod]
        public void QueryLowerString()
        {
            using (var context = new TestDataContext())
            {
                var portal = context.Door.FirstOrDefault(d => d.Name.ToLower() == "portal");
                KeylessMappingTests.CheckPortal(portal);
            }
        }

        [TestMethod, Ignore]
        public void QuerySelectStringSet () {
            using (var context = new TestDataContext()) {
                var names = context.Door.OrderBy(d => d.Name).Select(d => d.Name).ToArray();
                Assert.AreEqual(2, names.Length);
                Assert.AreEqual("Gate", names[0]);
                Assert.AreEqual("Portal", names[1]);
            }
        }
    }
}
