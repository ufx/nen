﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.DiscriminatedSequentialKeys;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class DiscriminatedSequentialKeyQueryTests {
        [TestMethod]
        public void LoadReferenceBaseFromSubTable () {
            using (var context = new TestDataContext()) {
                var fixture = context.EmptyFixture.Load(1);

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfJoins);
                Assert.IsTrue(context.LastQueryStatistics.TotalQueryTime.TotalSeconds < 1);

                DiscriminatedSequentialKeyMappingTests.CheckBare(fixture);
            }
        }

        [TestMethod]
        public void LoadReferenceBaseFromBaseTable () {
            using (var context = new TestDataContext()) {
                var fixture = context.OfficeFixture.Load(1);

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfJoins);
                Assert.IsTrue(context.LastQueryStatistics.TotalQueryTime.TotalSeconds < 1);

                DiscriminatedSequentialKeyMappingTests.CheckBare(fixture);
            }
        }

        [TestMethod]
        public void TrackReferenceBaseData () {
            using (var context = new TestDataContext(ContextOptions.TrackChanges)) {
                var fixture = context.EmptyFixture.Load(1);

                var conventions = context.Configuration.Conventions;
                var tableName = conventions.GetDataIdentifierName("EmptyFixture", DataIdentifierType.Table);
                var idColumn = conventions.GetDataIdentifierName("Id", DataIdentifierType.Column);

                Assert.IsTrue(context.TrackedData.ContainsTable(tableName));
                var emptyFixtureTable = context.TrackedData[tableName];
                Assert.IsNotNull(emptyFixtureTable);
                Assert.AreEqual(1, emptyFixtureTable.Rows.Count());

                var emptyFixtureRow = emptyFixtureTable[0];
                Assert.IsNotNull(emptyFixtureRow);

                var idValue = emptyFixtureRow[idColumn];
                if (idValue is decimal)
                    Assert.AreEqual(1m, idValue);
                else
                    Assert.AreEqual(1, idValue);
            }
        }

        [TestMethod]
        public void DisambiguateQueryWithOfType () {
            using (var context = new TestDataContext()) {
                var query = context.Get<Fixture>().OfType<OfficeFixture>().Where(f => f.Name == "GotNothing");
                var fixtures = query.ToList();

                Assert.AreEqual(1, fixtures.Count);
                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfJoins);
                Assert.IsTrue(context.LastQueryStatistics.TotalQueryTime.TotalSeconds < 1);

                DiscriminatedSequentialKeyMappingTests.CheckBare(fixtures[0]);
            }
        }
    }
}
