﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Tests.Data.ObjectRelationalMapper.Models.RenamedPrimaryKeys;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class RenamedPrimaryKeyTests {
        [TestMethod]
        public void LoadCubes () {
            using (var ctx = new TestDataContext()) {
                var allCubes = ctx.Get<Cube>().OrderBy(c => c.Id).ToArray();

                Assert.AreEqual(1, allCubes[0].X1);
                Assert.AreEqual(1, allCubes[0].Y1);
                Assert.AreEqual(1, allCubes[0].Z1);
                Assert.AreEqual(2, allCubes[0].X2);
                Assert.AreEqual(2, allCubes[0].Y2);
                Assert.AreEqual(2, allCubes[0].Z2);

                Assert.IsTrue(allCubes[0].GetType() == typeof(GelatinousCube));
                Assert.IsTrue(allCubes[1].GetType() == typeof(Cube));
                
                Assert.AreEqual(3, ((GelatinousCube)allCubes[0]).PlayersAbsorbed);
            }
        }

        [TestMethod]    
        public void QueryRenamedIdThroughAnonymousHierarchy () {
            using (var context = new TestDataContext()) {
                var anonCube = context.Get<GelatinousCube>().Where(g => g.PlayersAbsorbed == 3).Select(g => new { AnonId = g.Id, Abosrbed = g.PlayersAbsorbed }).First();

                Assert.IsNotNull(anonCube);
                Assert.AreEqual(3, anonCube.Abosrbed);
                Assert.IsTrue(anonCube.AnonId > 0);
            }
        }

        [TestMethod]
        public void QueryBasePropertyThroughAnonymousHierarchy () {
            using (var context = new TestDataContext()) {
                var anonCube = context.Get<GelatinousCube>().Where(g => g.PlayersAbsorbed == 3).Select(g => new { AnonX1 = g.X1 }).First();

                Assert.IsNotNull(anonCube);
                Assert.AreEqual(1, anonCube.AnonX1);
            }
        }

        [TestMethod]
        public void QueryDistinct()
        {
            using (var context = new TestDataContext())
            {
                var distinctTypeCount = context.Get<Cube>().Select(c => c.CubeType).Distinct().Count();
                Assert.AreEqual(2, distinctTypeCount);
            }
        }

        [TestMethod]
        public void QueryDynamicOfTypeToArray()
        {
            using (var context = new TestDataContext())
            {
                var query = context.Get(typeof(Cube)).OfType<object>();
                var result = query.ToArray();
            }
        }
    }
}
