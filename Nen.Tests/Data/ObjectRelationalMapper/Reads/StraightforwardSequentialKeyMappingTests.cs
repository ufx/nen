﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.StraightforwardSequentialKeys;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class StraightforwardSequentialKeyMappingTests {
        public class CavemanWrapper
        {
            public CavemanWrapper() { }
            public CavemanWrapper(Caveman c) { Caveman = c; }
            public static CavemanWrapper Create(Caveman c) { return new CavemanWrapper(c); }
            public static CavemanWrapper Create(int meaningless, Caveman c) { return new CavemanWrapper(c); }

            public Caveman Caveman { get; set; }
        }

        [TestMethod, Ignore]
        public void QueryExternalSelectNew()
        {
            using (var context = new TestDataContext())
            {
                var query = context.Get<Caveman>()
                    .Where(c => c.Name == "Ugg")
                    .Select(c => new CavemanWrapper(c));

                var results = query.ToArray();
                Assert.AreEqual(1, results.Length);
                CheckUgg(results[0].Caveman);
            }
        }

        [TestMethod]
        public void QueryExternalSelectStatic()
        {
            using (var context = new TestDataContext())
            {
                var query = context.Get<Caveman>()
                    .Where(c => c.Name == "Ugg")
                    .Select(c => CavemanWrapper.Create(c));

                var results = query.ToArray();
                Assert.AreEqual(1, results.Length);
                CheckUgg(results[0].Caveman);
            }
        }
        
        [TestMethod, Ignore]
        public void QueryExternalStatic2()
        {
            using (var context = new TestDataContext())
            {
                var query = context.Get<Caveman>()
                    .Where(c => c.Name == "Ugg")
                    .Select(c => CavemanWrapper.Create(1, c));

                var results = query.ToArray();
                Assert.AreEqual(1, results.Length);
                CheckUgg(results[0].Caveman);
            }
        }

        public static void CheckUgg (Caveman ugg) {
            Assert.IsNotNull(ugg);
            Assert.AreEqual(1, ugg.Id);
            Assert.AreEqual("Ugg", ugg.Name);
            Assert.AreEqual(1, ugg.PetDinosaurs.Count);
            Assert.AreEqual(1, ugg.Genes.Count);
            Assert.AreEqual("TTACAA", ugg.Genes[0].CavemanGene.Name);
            Assert.AreEqual(1, ugg.Genes[0].CavemanGene.Attributes.Count);
            Assert.AreEqual("Unibrow", ugg.Genes[0].CavemanGene.Attributes[0].Name);
        }
    }
}
