﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward;
using Nen.Data.ObjectRelationalMapper;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Linq.Expressions;
using System.Transactions;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads
{
    public static class WizardEx
    {
        public static Expression<Predicate<Wizard>> NameSurroundedByMacro(string prefix, string suffix)
        {
            var wizard = new WhereBuilder<Wizard>();

            if (prefix != null)
                wizard = wizard.Where(w => w.Name.StartsWith(prefix));
            if (suffix != null)
                wizard = wizard.Where(w => w.Name.EndsWith(suffix));

            return wizard.Expr;
        }

        [QueryMacro]
        public static bool NameSurroundedBy(this Wizard wizard, string prefix = null, string suffix = null)
        {
            // NOTE: Could also re-implement, cache, or simply throw NotSupportedException.
            return NameSurroundedByMacro(prefix, suffix).Compile()(wizard);
        }

        public static Expression<Predicate<string>> SurroundedByMacro(string prefix, string suffix)
        {
            var str = new WhereBuilder<string>();

            if (prefix != null)
                str = str.Where(s => s.StartsWith(prefix));

            if (suffix != null)
                str = str.Where(s => s.EndsWith(suffix));

            return str.Expr;
        }

        [QueryMacro]
        public static bool SurroundedBy(this string str, string prefix = null, string suffix = null)
        {
            var result = true;

            if (prefix != null)
                result = result && str.StartsWith(prefix);

            if (suffix != null)
                result = result && str.EndsWith(suffix);

            return result;
        }

        public static Expression<Predicate<Wizard>> HasSpellNamedMacro(string name)
        {
            var namePrefix = name + ": ";
            return (w => w.Spells.Any(s => s.Description.SurroundedBy(namePrefix, "Sorcery]")));
        }

        [QueryMacro]
        public static bool HasSpellNamed(this Wizard wizard, string name)
        {
            throw new NotSupportedException("This method can only be used within a Nen query.");
        }
    }

    [TestClass]
    public class QueryMacroTests
    {
        [TestMethod]
        public void QuerySimpleMacro()
        {
            var prefix = Guid.NewGuid().ToString();
            var suffix = "-WIZ";

            var wiz1 = new Wizard { Name = prefix + "-Foo-123" + suffix };
            var wiz2 = new Wizard { Name = prefix + "-Bar-456" + suffix };
            var wiz3 = new Wizard { Name = prefix + "-Baz-789" };

            var fireball = new Spell
            {
                Wizard = wiz1,
                Description = "Fireball: Pretty much exactly what it sounds like. [Sorcery]"
            };

            var effect1 = new SpellEffect
            {
                Spell = fireball,
                Damage = 256
            };

            using (var ctx = new TestDataContext())
            {
                var changeSet = ctx.CreateChangeSet();
                changeSet.Add(wiz1);
                changeSet.Add(wiz2);
                changeSet.Add(wiz3);
                changeSet.Add(fireball);
                changeSet.Add(effect1);
                changeSet.Commit();

                try
                {

                    var results = ctx.Get<Wizard>().Where(w => w.NameSurroundedBy(prefix, suffix)).OrderByDescending(w => w.Name).ToList();
                    Assert.AreEqual(2, results.Count);
                    Assert.AreEqual(prefix + "-Foo-123" + suffix, results[0].Name);

                    var fireWizard = ctx.Get<Wizard>().Where(w => w.HasSpellNamed("Fireball")).Single();
                    Assert.AreEqual(prefix + "-Foo-123" + suffix, results[0].Name);
                }
                finally
                {
                    ctx.Delete(effect1);
                    ctx.Delete(fireball);
                    ctx.Delete(wiz3);
                    ctx.Delete(wiz2);
                    ctx.Delete(wiz1);
                }
            }
        }
    }
}
