﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Discriminated;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class DiscriminatedMappingTests {
        [TestMethod]
        public void Load () {
            using (var context = new TestDataContext()) {
                var farley = context.NinjaEmployee.Load(new Guid("3A8F48E4-D31B-47AF-9A64-9F14B420D7D6"));

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckFarley(farley);
            }
        }

        [TestMethod]
        public void LoadAmbiguous () {
            using (var context = new TestDataContext()) {
                var sanosuke = context.Employee.Load(new Guid("B587C85B-A841-4d90-A64B-2B26C417D977"));

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckSanosuke(sanosuke);
            }
        }

        [TestMethod]
        public void LoadDiscriminatedReference () {
            using (var context = new TestDataContext()) {
                var ground0 = context.Employer.Load(new Guid("F78EF818-2DDB-4128-989D-2F3A0AA5E156"));

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);
                
                CheckGround0(ground0);
            }
        }

        [TestMethod]
        public void LoadAmbiguousTwoDepth () {
            using (var context = new TestDataContext()) {
                var robozo = context.Employee.Load(new Guid("53270DDA-4CE9-418F-A397-0D138FD12DE6"));

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckRobozo(robozo);
            }
        }

        [TestMethod]
        public void LoadAmbiguousReferenceBaseTable () {
            using (var context = new TestDataContext()) {
                var hiro = context.Employee.Load(new Guid("92D2EE26-6DF9-4DFA-99DA-FC6A14A0AFB1"));

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckHiro(hiro, true, false);
            }
        }

        [TestMethod]
        public void LoadAmbiguousReferenceBaseTableTwoDepth () {
            using (var context = new TestDataContext()) {
                var lacroix = context.Employee.Load(new Guid("F9C432BA-4D74-4D93-A475-EF7370D34997"));

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckLacroix(lacroix, true);
            }
        }

        [TestMethod]
        public void LoadAmbiguousRoot () {
            using (var context = new TestDataContext()) {
                var high5 = context.Employee.Load(new Guid("2F8FD845-DA28-4cfe-96CA-60EA7E651C6A"));

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckHigh5(high5);
            }
        }

        [TestMethod]
        public void LoadDiscriminatedCollection () {
            using (var context = new TestDataContext()) {
                var pizzaNinjas = context.Get<Department>().First(d => d.Name == "Pizza Ninjas");

                Assert.AreEqual(1, pizzaNinjas.Id);
                Assert.AreEqual(3, pizzaNinjas.Employees.Count);
                Assert.AreEqual("Pizza Ninjas", pizzaNinjas.Name);
            }
        }

        #region Individual Tests
        internal static void CheckHigh5 (Employee high5) {
            Assert.IsNotNull(high5);
            Assert.IsInstanceOfType(high5, typeof(Employee));

            Assert.AreEqual("High5", high5.Name);
            Assert.AreEqual(new Guid("2F8FD845-DA28-4cfe-96CA-60EA7E651C6A"), high5.Id);
            Assert.AreEqual(EmployeeType.Base, high5.Type);
        }

        internal static void CheckRobozo (Employee robozo) {
            Assert.IsNotNull(robozo);
            Assert.IsInstanceOfType(robozo, typeof(RoboClownEmployee));

            var robo = (RoboClownEmployee) robozo;
            Assert.AreEqual("Robozo", robo.Name);
            Assert.AreEqual(new Guid("53270DDA-4CE9-418F-A397-0D138FD12DE6"), robo.Id);
            Assert.AreEqual(94, robo.ChildrenVaporized);
            Assert.AreEqual(15.92m, robo.ShoeLength);
            Assert.AreEqual(EmployeeType.RoboClown, robo.Type);
        }

        internal static void CheckFarley (Employee farley) {
            Assert.IsNotNull(farley);
            Assert.IsInstanceOfType(farley, typeof(NinjaEmployee));

            var ninja = (NinjaEmployee) farley;
            Assert.AreEqual("Farley", ninja.Name);
            Assert.AreEqual(new Guid("3A8F48E4-D31B-47AF-9A64-9F14B420D7D6"), ninja.Id);
            Assert.AreEqual(1, ninja.PiratesSlain);
            Assert.AreEqual(EmployeeType.Ninja, ninja.Type);
        }

        internal static void CheckSanosuke (Employee sanosuke) {
            Assert.IsNotNull(sanosuke);
            Assert.IsInstanceOfType(sanosuke, typeof(NinjaEmployee));

            var ninja = (NinjaEmployee) sanosuke;
            Assert.AreEqual("Sanosuke", ninja.Name);
            Assert.AreEqual(new Guid("B587C85B-A841-4d90-A64B-2B26C417D977"), ninja.Id);
            Assert.AreEqual(10, ninja.PiratesSlain);
            Assert.AreEqual(EmployeeType.Ninja, ninja.Type);
        }

        internal static void CheckBozo (Employee bozo) {
            Assert.IsNotNull(bozo);
            Assert.IsInstanceOfType(bozo, typeof(ClownEmployee));

            var clown = (ClownEmployee) bozo;
            Assert.AreEqual(new Guid("E031BE0B-E9B0-4A29-ACDC-A4661D5556CE"), clown.Id);
            Assert.AreEqual("Bozo", clown.Name);
            Assert.AreEqual(10.45m, clown.ShoeLength);
            Assert.AreEqual(EmployeeType.Clown, clown.Type);
        }

        internal static void CheckHiro (Employee hiro, bool checkCircularItems, bool departmentResolved) {
            Assert.IsNotNull(hiro);
            Assert.IsInstanceOfType(hiro, typeof(PizzaDeliveryEmployee));

            var pizzaMan = (PizzaDeliveryEmployee) hiro;
            Assert.AreEqual(new Guid("92D2EE26-6DF9-4DFA-99DA-FC6A14A0AFB1"), pizzaMan.Id);
            Assert.AreEqual("Hiro", pizzaMan.Name);
            Assert.AreEqual(EmployeeType.PizzaDelivery, pizzaMan.Type);
            Assert.AreEqual(3, pizzaMan.DeliveredPizzas.Count);
            Assert.AreEqual(departmentResolved, hiro.Department.IsResolved);

            if (checkCircularItems)
                Assert.AreEqual(1, pizzaMan.WimpyPizzas.Count);
        }

        internal static void CheckLacroix (Employee lacroix, bool checkCircularItems) {
            Assert.IsNotNull(lacroix);
            Assert.IsInstanceOfType(lacroix, typeof(VampirePizzaDeliveryEmployee));

            var prince = (VampirePizzaDeliveryEmployee) lacroix;
            Assert.AreEqual(new Guid("F9C432BA-4D74-4D93-A475-EF7370D34997"), prince.Id);
            Assert.AreEqual("LaCroix", prince.Name);
            Assert.AreEqual(EmployeeType.VampirePizzaDelivery, prince.Type);
            Assert.AreEqual("Werewolferoni", prince.FavoriteBloodyTopping);
            Assert.AreEqual(0, prince.DeliveredPizzas.Count);

            if (checkCircularItems)
                Assert.AreEqual(0, prince.WimpyPizzas.Count);
        }

        internal static void CheckGround0 (Employer ground0) {
            Assert.IsNotNull(ground0);
            Assert.AreEqual("Ground0", ground0.Name);
            Assert.AreEqual(new Guid("F78EF818-2DDB-4128-989D-2F3A0AA5E156"), ground0.Id);
            CheckBozo(ground0.Assistant);
            CheckFarley(ground0.OfficeNinja);
        }
        #endregion
    }
}
