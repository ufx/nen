﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.Keyless;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class KeylessMappingTests {
        [TestMethod]
        public void Load () {
            using (var context = new TestDataContext()) {
                var doors = context.Door.Where(d => d.Name == "Portal").ToList();
                Assert.AreEqual(1, doors.Count);

                var portal = doors[0];
                CheckPortal(portal);
            }
        }

        public static void CheckPortal (Door portal) {
            Assert.IsNotNull(portal);
            Assert.AreEqual("Portal", portal.Name);
            Assert.IsTrue(portal.Open);
        }

        public static void CheckGate(Door gate)
        {
            Assert.IsNotNull(gate);
            Assert.AreEqual("Gate", gate.Name);
            Assert.IsFalse(gate.Open);
        }
    }
}
