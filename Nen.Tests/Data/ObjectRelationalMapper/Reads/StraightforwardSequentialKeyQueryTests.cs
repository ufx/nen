﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.StraightforwardSequentialKeys;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class StraightforwardSequentialKeyQueryTests {
        [TestMethod]
        public void QueryWithCollectionMemberAdvice () {
            using (var context = new TestDataContext()) {
                var query = context.Get<Caveman>().Include(c => c.PetDinosaurs.Include(d => d.Egg).Include(d => d.Teeth));
                var ugg = query.FirstOrDefault();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);

                StraightforwardSequentialKeyMappingTests.CheckUgg(ugg);

                var thorax = ugg.PetDinosaurs[0];
                Assert.IsNotNull(thorax);
                Assert.AreEqual(1, thorax.Id);
                Assert.AreEqual("Thorax", thorax.Name);
                Assert.AreSame(thorax.OwnerCaveman, ugg);
                Assert.IsTrue(thorax.LazyEgg.IsResolved);
                Assert.AreEqual(1, thorax.Teeth.Count);

                var egg = thorax.Egg;
                Assert.IsNotNull(egg);
                Assert.AreEqual(1, egg.Id);
                Assert.IsFalse(egg.Hatched);

                var tooth = thorax.Teeth[0];
                Assert.IsNotNull(tooth);
                Assert.AreEqual(1, tooth.Id);
                Assert.AreEqual(53543, tooth.Sharpness);
                Assert.AreSame(thorax, tooth.PetDinosaur);
            }
        }

        [TestMethod]
        public void QueryKeyArray () {
            using (var context = new TestDataContext()) {
                var keys = new int[] { 1, 200, 300 };
                var query = context.Get<Caveman>().Where(c => keys.Contains(c.Id));
                var caveman = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.AreEqual(1, caveman.Count);

                StraightforwardSequentialKeyMappingTests.CheckUgg(caveman[0]);
            }
        }

        [TestMethod]
        public void QueryContainsSubquery () {
            using (var context = new TestDataContext()) {
                var subquery = context.Get<Caveman>().Where(c => c.Name == "Ugg");
                var query = context.Get<Caveman>().Where(c => subquery.Contains(c));
                var caveman = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.AreEqual(1, caveman.Count);

                StraightforwardSequentialKeyMappingTests.CheckUgg(caveman[0]);
            }
        }


        [TestMethod]
        public void QueryOneField () {
            using (var context = new TestDataContext()) {
                var name = context.Get<Caveman>().Where(c => c.Id == 1).Select(c => c.Name).FirstOrDefault();
                Assert.AreEqual("Ugg", name);
            }
        }

        [TestMethod]
        public void QueryIncludeCollectionPropertyCollection () {
            using (var context = new TestDataContext()) {
                var query = context.Get<Caveman>()
                    .Include(c => c.Genes.Include(g => g.CavemanGene.Attributes))
                    .Where(c => c.Name == "Ugg");
                var caveman = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.AreEqual(1, caveman.Count);

                StraightforwardSequentialKeyMappingTests.CheckUgg(caveman[0]);
            }
        }

        [TestMethod, Ignore]
        public void QueryUserFunction () {
            using (var context = new TestDataContext()) {
                var query = context.Get<Caveman>()
                    .Select(c => new { ChangedName = DatabaseFunctions.ChangeIt(c.Name) });

                Assert.Fail("not implemented");
            }
        }

        [TestMethod]
        public void QueryCollectionAnyThroughReference () {
            using (var context = new TestDataContext()) {
                var query = context.Get<PetDinosaur>().Where(d => d.OwnerCaveman.Genes.Any(g => g.CavemanGene.Name == "TTACAA"));
                var dinosaurs = query.ToArray();
                Assert.AreEqual(1, dinosaurs.Length);
                Assert.AreEqual("Thorax", dinosaurs[0].Name);
            }
        }

        [TestMethod]
        public void QueryStringNotContainsThroughReference () {
            using (var context = new TestDataContext()) {
                var query = context.Get<PetDinosaur>().Where(d => !d.OwnerCaveman.Name.Contains("g"));
                var dinosaurs = query.ToArray();
                Assert.AreEqual(0, dinosaurs.Length);
            }
        }

        [TestMethod]
        public void QueryDataRefThroughProjection () {
            using (var context = new TestDataContext()) {
                var query = context.Get<PetDinosaur>().OrderBy(d => d.Id).Select(d => new { DREgg = d.Egg });
                var eggs = query.ToArray();

                Assert.AreEqual(2, eggs.Length);

                Assert.IsNotNull(eggs[0]);
                var egg = eggs[0].DREgg;
                Assert.IsNotNull(egg);
                Assert.AreEqual(1, egg.Id);
                Assert.IsFalse(egg.Hatched);

                Assert.IsNotNull(eggs[1]);
                Assert.IsNull(eggs[1].DREgg);
            }
        }

        [TestMethod]
        public void QueryDateDiffDays()
        {
            using (var context = new TestDataContext())
            {
                var cutoff = new DateTime(2000, 02, 10);
                var query = context.Get<Caveman>().Where(c => (((DateTime) c.Discovered) - cutoff).Days < 10);
                var ugg = query.FirstOrDefault();
                StraightforwardSequentialKeyMappingTests.CheckUgg(ugg);
            }
        }

        [TestMethod]
        public void QueryDateAddDays()
        {
            using (var context = new TestDataContext())
            {
                var cutoff = new DateTime(2000, 02, 10);
                var query = context.Get<Caveman>().Where(c => ((DateTime) c.Discovered).AddDays(-2) < cutoff);
                var ugg = query.FirstOrDefault();
                StraightforwardSequentialKeyMappingTests.CheckUgg(ugg);
            }
        }

        [TestMethod]
        public void QueryCache()
        {
            var cache = new Nen.Caching.WebCache(System.Web.HttpRuntime.Cache);

            using (var context1 = new TestDataContext())
            {
                context1.Cache = cache;
                var query = context1.Get<Caveman>().Where(c => c.Name == "Ugg");
                var list = query.ToList();
                Assert.AreEqual(1, list.Count);
                Assert.AreEqual(1, context1.LastQueryStatistics.NumberOfDatabaseQueries);
            }

            using (var context2 = new TestDataContext())
            {
                context2.Cache = cache;
                var query = context2.Get<Caveman>().Where(c => c.Name == "Ugg");
                var list = query.ToList();
                Assert.AreEqual(1, list.Count);
                Assert.IsNull(context2.LastQueryStatistics);
            }

            cache.Clear();
        }
    }
}
