﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Discriminated;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class DiscriminatedQueryTests {
        [TestMethod]
        public void OptimizedOutQueryRetainsDiscriminator () {
            using (var context = new TestDataContext()) {
                var employees = context.Get<PizzaDeliveryEmployee>().ToList();
                Assert.AreEqual(2, employees.Count);
            }
        }

        [TestMethod]
        public void ConcreteQueryRetainsDiscriminator () {
            using (var context = new TestDataContext()) {
                var employees = context.Get<ClownEmployee>().ToList();
                Assert.AreEqual(2, employees.Count);
            }
        }

        [TestMethod]
        public void OptimizedOutQueryRetainsDiscriminatorWithCriteria () {
            using (var context = new TestDataContext()) {
                var employees = context.Get<PizzaDeliveryEmployee>().Where(e => e.Name.Contains("x")).ToList();
                Assert.AreEqual(1, employees.Count);
            }
        }

        [TestMethod]
        public void QueryUnambiguousBasePropertyOfAmbiguousType () {
            using (var context = new TestDataContext()) {
                var query = from e in context.Employee where e.Name == "Sanosuke" select e;
                var ninjas = query.ToList();

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, ninjas.Count);

                var sanosuke = ninjas[0];
                DiscriminatedMappingTests.CheckSanosuke(sanosuke);
            }
        }
        
        [TestMethod]
        public void QueryUnambiguousSubPropertyOfAmbiguousType () {
            using (var context = new TestDataContext()) {
                var bozo = context.Get<ClownEmployee>().FirstOrDefault(e => e.ShoeLength == 10.45m);

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                DiscriminatedMappingTests.CheckBozo(bozo);
            }
        }

        [TestMethod]
        public void QueryEnumWithCast () {
            using (var context = new TestDataContext()) {
                int type = (int) EmployeeType.Ninja;
                var query = from e in context.Employee where e.Name == "Sanosuke" && e.Type == (EmployeeType) type select e;
                var ninjas = query.ToList();

                Assert.AreEqual(2, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, ninjas.Count);

                var sanosuke = ninjas[0];
                DiscriminatedMappingTests.CheckSanosuke(sanosuke);
            }
        }

        [TestMethod]
        public void QueryAmbiguousPropertyRetainsAdvice () {
            using (var context = new TestDataContext()) {
                var query = context.Employee.Where(e => e.Name == "Hiro").Include(e => e.Department);

                var hiro = query.FirstOrDefault();

                Assert.IsNotNull(hiro);

                Assert.IsTrue(hiro.Department.IsResolved);
                Assert.AreEqual(1, hiro.Department.Value.Id);
                Assert.AreEqual("Pizza Ninjas", hiro.Department.Value.Name);

                DiscriminatedMappingTests.CheckHiro(hiro, true, true);
            }
        }

        [TestMethod]
        public void QueryAdviceOnAmbiguousTypes () {
            using (var context = new TestDataContext()) {
                var hiro = context.Employee.WhenType<Employee, PizzaDeliveryEmployee>(p => p.Include(e => e.WimpyPizzas)).FirstOrDefault(e => e.Name == "Hiro");

                Assert.AreEqual(4, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                DiscriminatedMappingTests.CheckHiro(hiro, true, false);
            }
        }

        [TestMethod]
        public void QueryDiscriminatorRootCount () {
            using (var context = new TestDataContext()) {
                var count = context.Employee.Where(e => e.Name == "Hiro").Count();
                Assert.AreEqual(1, count);
            }
        }
    }
}
