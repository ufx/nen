﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class StraightforwardMappingTests {
        [TestMethod]
        public void Load () {
            // Fred
            using (var context = new TestDataContext()) {
                var fred = context.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckFred(fred, true);
            }

            // Rat
            using (var context = new TestDataContext()) {
                var rat = context.Zombie.Load(new Guid("CE447EF1-6AB3-48B4-BCBD-2C5DFF606593"));

                Assert.AreEqual(4, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckRat(rat, true);
            }
        }


        [TestMethod]
        public void MapDerivedCollectionThroughOneReference () {
            using (var context = new TestDataContext()) {
                var bullet = context.Bullet.Load(new Guid("36CAFF06-E991-489F-864C-5CC23586810C"));
                Assert.IsNotNull(bullet);
                Assert.AreEqual(BulletKind.Silver, bullet.Kind);
                CheckFred(bullet.Zombie, true);
            }
        }

        [TestMethod]
        public void MapDerivedCollectionThroughTwoReferences () {
            using (var context = new TestDataContext()) {
                var greaseEffect1 = context.SpellEffect.Load(new Guid("DC657CEE-CD5F-45D6-A03C-5EBA5AD5B3D0"));
                Assert.IsNotNull(greaseEffect1);
                Assert.AreEqual(new Guid("DC657CEE-CD5F-45D6-A03C-5EBA5AD5B3D0"), greaseEffect1.Id);
                Assert.AreEqual(200, greaseEffect1.Damage);
                Assert.IsNotNull(greaseEffect1.Spell);
                Assert.IsNotNull(greaseEffect1.Spell.Wizard);
                CheckEinhowzer(greaseEffect1.Spell.Wizard);
            }
        }

        [TestMethod]
        public void MapDeferredLazyReference () {
            using (var context = new TestDataContext()) {
                var bob = context.Zombie.Load(new Guid("E7174014-0F25-40DF-B875-2DA0C90C5F68"));
                CheckBob(bob);
            }
        }

        [TestMethod]
        public void MapCollectionOfCollection () {
            using (var context = new TestDataContext()) {
                var ein = context.Wizard.Load(new Guid("4305C3CF-68CB-4E79-BF7C-60306CD18053"));
                CheckEinhowzer(ein);
            }
        }

        #region Wizard Tests
        internal static void CheckEinhowzer (Wizard ein) {
            Assert.IsNotNull(ein);
            Assert.AreEqual(new Guid("4305C3CF-68CB-4E79-BF7C-60306CD18053"), ein.Id);
            Assert.AreEqual("Einhowzer", ein.Name);
            Assert.AreEqual(3, ein.Spells.Count);
            Assert.AreSame(ein, ein.Spells.Wizard);

            var grease = ein.Spells[0];
            CheckGrease(grease, ein);

            var magicMissive = ein.Spells[1];
            Assert.IsNotNull(magicMissive);
            Assert.AreEqual(new Guid("70B7FBB8-6A6A-44C3-B0A3-95695D7A0AEF"), magicMissive.Id);
            Assert.AreEqual("Magic Missive", magicMissive.Description);
            Assert.AreSame(ein, magicMissive.Wizard);
            Assert.AreEqual(1, magicMissive.SpellEffects.Count);

            var magicMissiveEffect = magicMissive.SpellEffects[0];
            Assert.IsNotNull(magicMissiveEffect);
            Assert.AreEqual(new Guid("6BAAA889-E4B1-4C68-ABC6-03FFC5B3710C"), magicMissiveEffect.Id);
            Assert.AreEqual(10, magicMissiveEffect.Damage);
            Assert.AreSame(magicMissive, magicMissiveEffect.Spell);

            var shockingBeard = ein.Spells[2];
            Assert.IsNotNull(shockingBeard);
            Assert.AreEqual(new Guid("61674321-3F87-49CB-8A09-40A27FE59BA6"), shockingBeard.Id);
            Assert.AreEqual("Shocking Beard", shockingBeard.Description);
            Assert.AreSame(ein, shockingBeard.Wizard);
            Assert.AreEqual(1, shockingBeard.SpellEffects.Count);

            var shockingBeardEffect = shockingBeard.SpellEffects[0];
            Assert.IsNotNull(shockingBeardEffect);
            Assert.AreEqual(new Guid("7642F750-9A93-4958-A737-4BA9DA0B4FC0"), shockingBeardEffect.Id);
            Assert.AreEqual(15, shockingBeardEffect.Damage);
            Assert.AreSame(shockingBeard, shockingBeardEffect.Spell);
        }

        internal static void CheckGrease (Spell grease, Wizard parent) {
            Assert.IsNotNull(grease);
            Assert.AreEqual(new Guid("FA5BCFAA-9F72-4A18-8CB8-C10F6729FF62"), grease.Id);
            Assert.AreEqual("Grease", grease.Description);
            Assert.AreSame(parent, grease.Wizard);
            Assert.AreEqual(2, grease.SpellEffects.Count);

            var greaseEffect1 = grease.SpellEffects[0];
            Assert.IsNotNull(greaseEffect1);
            Assert.AreEqual(new Guid("DC657CEE-CD5F-45D6-A03C-5EBA5AD5B3D0"), greaseEffect1.Id);
            Assert.AreEqual(200, greaseEffect1.Damage);
            Assert.AreSame(grease, greaseEffect1.Spell);

            var greaseEffect2 = grease.SpellEffects[1];
            Assert.IsNotNull(greaseEffect2);
            Assert.AreEqual(new Guid("03E88C3E-F49D-4B12-B2B2-71C246220A78"), greaseEffect2.Id);
            Assert.AreEqual(333, greaseEffect2.Damage);
            Assert.AreSame(grease, greaseEffect2.Spell);
        }
        #endregion

        #region Zombie Tests
        internal static void CheckBob (Zombie bob) {
            Assert.IsNotNull(bob);
            Assert.AreEqual(new Guid("E7174014-0F25-40DF-B875-2DA0C90C5F68"), bob.Id);
            Assert.AreEqual("Bob", bob.Name);
            Assert.IsNull(bob.Infected);
            Assert.IsNull(bob.TatteredClothing);
            Assert.AreEqual(new DateTime(1997, 03, 06), bob.DateModified);
            Assert.IsNull(bob.Infected);
            Assert.AreEqual(4, bob.RemainingAppendages);
            Assert.IsFalse(bob.LazyCarriedBullet.IsResolved);
            Assert.AreEqual(0, bob.Bullets.Count);

            var carriedBullet = bob.CarriedBullet;
            Assert.IsNotNull(carriedBullet);
            Assert.AreEqual(new Guid("7C9C752F-48F6-436F-811A-6F768B9D8BA0"), carriedBullet.Id);
            Assert.AreEqual(BulletKind.Hollow, carriedBullet.Kind);
            CheckFred(carriedBullet.Zombie, true);
        }

        internal static void CheckFred (Zombie fred, bool checkCircularItems) {
            Assert.IsNotNull(fred);
            Assert.AreEqual(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"), fred.Id);
            Assert.AreEqual("Fred", fred.Name);
            Assert.AreEqual(5, fred.RemainingAppendages);
            Assert.IsNull(fred.BitBy);
            Assert.AreEqual(new DateTime(1987, 01, 04), fred.Infected);
            Assert.AreEqual(new DateTime(1987, 01, 05), fred.DateModified);
            Assert.IsTrue(fred.LazyCarriedBullet.IsResolved);

            Assert.IsNotNull(fred.TatteredClothing);
            Assert.AreEqual(new Guid("A5A07A43-3057-431C-AFBD-ADEBE1E2FDA8"), fred.TatteredClothing.Id);
            Assert.AreEqual(19, fred.TatteredClothing.NumberOfHoles);

            Assert.AreEqual(1, fred.Bitten.Count);
            if (checkCircularItems) {
                Zombie rat = fred.Bitten[0];
                CheckRat(rat, false);
            }

            Assert.AreEqual(3, fred.Bullets.Count);
            if (checkCircularItems) {
                var sortedBullets = new List<Bullet>(from b in fred.Bullets orderby b.Kind select b);
                CollectionAssertEx.AreSame(fred.Bullets, sortedBullets);

                foreach (Bullet bullet in fred.Bullets) {
                    Assert.IsNotNull(bullet);
                    CheckFred(bullet.Zombie, false);
                }

                Assert.IsTrue(fred.SilverBullets.Count > 0);
                Assert.IsTrue(fred.SilverBullets.All(b => b.Kind == BulletKind.Silver));
                Assert.IsTrue(fred.NormalBullets.Count > 0);
                Assert.IsTrue(fred.NormalBullets.All(b => b.Kind == BulletKind.Normal));
            }
        }

        internal static void CheckRat (Zombie rat, bool checkCircularItems) {
            Assert.IsNotNull(rat);
            Assert.AreEqual(new Guid("CE447EF1-6AB3-48B4-BCBD-2C5DFF606593"), rat.Id);
            Assert.AreEqual("Rat", rat.Name);
            Assert.AreEqual(3, rat.RemainingAppendages);
            Assert.IsNull(rat.TatteredClothing);
            Assert.AreEqual(0, rat.Bitten.Count);
            Assert.AreEqual(0, rat.Bullets.Count);
            Assert.AreEqual(0, rat.NormalBullets.Count);
            Assert.AreEqual(0, rat.SilverBullets.Count);
            Assert.IsNull(rat.Infected);
            Assert.AreEqual(new DateTime(1980, 01, 01), rat.DateModified);

            if (checkCircularItems)
                CheckFred(rat.BitBy, false);
        }
        #endregion
    }
}
