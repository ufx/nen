﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper;
using Nen.Linq;
using Nen.Linq.Dynamic;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class StraightforwardQueryTests {
        [TestMethod]
        public void QueryWithoutModifiers () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(5, zombies.Count);
            }
        }

        [TestMethod]
        public void QueryOnStringProperty () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.Name == "Rat" select z;
                var zombies = query.ToList();

                Assert.AreEqual(4, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);
                Assert.AreEqual(6, context.GetAttachedObjects().Count);

                Assert.AreEqual(1, zombies.Count);

                var rat = zombies[0];
                StraightforwardMappingTests.CheckRat(rat, true);
            }
        }

        [TestMethod]
        public void QueryOnConstantReference () {
            using (var context = new TestDataContext()) {
                var fred = context.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));
                var query = from z in context.Zombie where z.BitBy == fred select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);

                var rat = zombies[0];
                StraightforwardMappingTests.CheckRat(rat, true);

                var nullQuery = from z in context.Zombie where z.BitBy == null && z.Name == "Fred" select z;
                var nullZombies = nullQuery.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, nullZombies.Count);

                fred = nullZombies[0];
                StraightforwardMappingTests.CheckFred(fred, true);

                var notNullQuery = from z in context.Zombie where z.BitBy != null && z.Name == "Rat" select z;
                var notNullZombies = notNullQuery.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, notNullZombies.Count);

                rat = notNullZombies[0];
                StraightforwardMappingTests.CheckRat(rat, true);
            }
        }

        [TestMethod]
        public void QueryThroughReferenceProperty () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.BitBy.Name == "Fred" select z;
                var zombies = query.ToList();

                Assert.AreEqual(4, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);

                var rat = zombies[0];
                StraightforwardMappingTests.CheckRat(rat, true);
            }
        }

        [TestMethod]
        public void QueryThroughConstantReference () {
            using (var context = new TestDataContext()) {
                var fred = context.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));
                var query = from z in context.Zombie where z.BitBy.Name == fred.Name select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);

                var rat = zombies[0];
                StraightforwardMappingTests.CheckRat(rat, true);
            }
        }

        [TestMethod]
        public void QueryIncludeCollection () {
            using (var context = new TestDataContext()) {
                var query = (from z in context.Zombie where z.Name == "Fred" select z).Include(z => z.Bitten);
                var zombies = query.ToList();

                Assert.AreEqual(4, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);

                var fred = zombies[0];
                var items = fred.Bitten.GetInternalItems();
                Assert.IsFalse(items is IFuture);
                StraightforwardMappingTests.CheckFred(fred, true);
            }
        }

        [TestMethod]
        public void QueryIncludeCollectionChained () {
            using (var context = new TestDataContext()) {
                var query = context.Zombie.Where(z => z.Name == "Fred")
                    .Include(z => z.Bitten)
                    .Include(z => z.Bitten.Include(b => b.SilverBullets));
                var zombies = query.ToList();

                Assert.AreEqual(5, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);

                var fred = zombies[0];
                var bittenItems = fred.Bitten.GetInternalItems();
                Assert.IsFalse(bittenItems is IFuture);

                var bittenSilverBulletItems = fred.Bitten[0].SilverBullets.GetInternalItems();
                Assert.IsFalse(bittenSilverBulletItems is IFuture);

                StraightforwardMappingTests.CheckFred(fred, true);
            }
        }

        [TestMethod]
        public void QueryIncludeProperty () {
            using (var context = new TestDataContext()) {
                var query = (from z in context.Zombie where z.Name == "Joe" select z).Include(z => z.CarriedBullet);
                var zombies = query.ToList();

                Assert.AreEqual(4, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);

                var fred = zombies[0];
                Assert.IsTrue(fred.LazyCarriedBullet.IsResolved);
            }
        }

        [TestMethod]
        public void QueryOrderBy () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie orderby z.Name select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(5, zombies.Count);

                var sortedZombies = new List<Zombie>(from z in zombies orderby z.Name select z);
                CollectionAssertEx.AreSame(zombies, sortedZombies);
            }
        }

        [TestMethod]
        public void QueryOrderByThenBy () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie orderby z.RemainingAppendages, z.Name select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(5, zombies.Count);

                var sortedZombies = new List<Zombie>(from z in zombies orderby z.RemainingAppendages, z.Name select z);
                CollectionAssertEx.AreSame(zombies, sortedZombies);
            }
        }

        [TestMethod]
        public void QueryOrderByAndWhere () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.RemainingAppendages == 5 orderby z.Name select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(2, zombies.Count);

                var sortedZombies = new List<Zombie>(from z in zombies where z.RemainingAppendages == 5 orderby z.Name select z);
                CollectionAssertEx.AreSame(zombies, sortedZombies);
            }
        }

        [TestMethod]
        public void QueryOrderByDescending () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie orderby z.Name descending select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(5, zombies.Count);

                var sortedZombies = new List<Zombie>(from z in zombies orderby z.Name descending select z);
                CollectionAssertEx.AreSame(zombies, sortedZombies);
            }
        }

        [TestMethod]
        public void QueryMultipleResults () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.Name == "Fred" || z.Name == "Rat" orderby z.Name select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(2, zombies.Count);

                StraightforwardMappingTests.CheckFred(zombies[0], true);
                StraightforwardMappingTests.CheckRat(zombies[1], true);
            }
        }

        [TestMethod]
        public void QueryAdHocView () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.Name == "Fred" select new { Id = z.Id, Name = z.Name };
                var zombieNames = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombieNames.Count);

                var fredName = zombieNames[0];
                Assert.AreEqual("Fred", fredName.Name);
                Assert.AreEqual(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"), fredName.Id);
            }
        }

        [TestMethod]
        public void QueryAdHocViewImplicitId () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.RemainingAppendages < 5 orderby z.Name select new { Name = z.Name };
                var zombieNames = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(2, zombieNames.Count);
                Assert.AreEqual("Bob", zombieNames[0].Name);
                Assert.AreEqual("Rat", zombieNames[1].Name);
            }
        }

        [TestMethod]
        public void QueryStaticView () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Get<ZombieView>() where z.Name == "Fred" select z;
                var zombieViews = query.ToList();

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombieViews.Count);

                var fredView = zombieViews[0];
                Assert.AreEqual("Fred", fredView.Name);
                Assert.AreEqual(new DateTime(1987, 01, 04), fredView.KnownInfectedDate);
            }
        }

        [TestMethod]
        public void QueryPaginationSkipTake () {
            using (var context = new TestDataContext()) {
                var query = (from z in context.Zombie orderby z.Name select z).Skip(1).Take(3);
                var zombies = query.ToList();

                Assert.AreEqual(3, zombies.Count);
                Assert.AreEqual("Bob", zombies[0].Name);
                Assert.AreEqual("Fred", zombies[1].Name);
                Assert.AreEqual("Joe", zombies[2].Name);
            }
        }

        [TestMethod]
        public void QueryPaginationTakeSkip () {
            using (var context = new TestDataContext()) {
                var query = (from z in context.Zombie orderby z.Name select z).Take(3).Skip(1);
                var zombies = query.ToList();

                Assert.AreEqual(2, zombies.Count);
                Assert.AreEqual("Bob", zombies[0].Name);
                Assert.AreEqual("Fred", zombies[1].Name);
            }
        }

        [TestMethod]
        public void QueryStartsWith () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.Name.StartsWith("Ra") select z;
                var zombies = query.ToList();

                Assert.AreEqual(1, zombies.Count);
                StraightforwardMappingTests.CheckRat(zombies[0], true);
            }
        }

        [TestMethod]
        public void QueryToLowerStartsWith()
        {
            using (var context = new TestDataContext())
            {
                var query = from z in context.Zombie where z.Name.ToLower().StartsWith("ra") select z;
                var zombies = query.ToList();

                Assert.AreEqual(1, zombies.Count);
                StraightforwardMappingTests.CheckRat(zombies[0], true);
            }
        }

        [TestMethod]
        public void QueryEndsWith () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.Name.EndsWith("e") orderby z.Name select z;
                var zombies = query.ToList();

                Assert.AreEqual(2, zombies.Count);
                Assert.AreEqual("Arnie", zombies[0].Name);
                Assert.AreEqual("Joe", zombies[1].Name);
            }
        }

        [TestMethod]
        public void QueryStringContains () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.Name.Contains("red") select z;
                var zombies = query.ToList();

                Assert.AreEqual(1, zombies.Count);
                StraightforwardMappingTests.CheckFred(zombies[0], true);
            }
        }

        [TestMethod]
        public void QueryCollectionContainsConstantReference () {
            using (var context = new TestDataContext()) {
                var rat = context.Zombie.Load(new Guid("CE447EF1-6AB3-48B4-BCBD-2C5DFF606593"));
                var query = from z in context.Zombie where z.Bitten.Contains(rat) select z;
                var zombies = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);
                StraightforwardMappingTests.CheckFred(zombies[0], true);
            }
        }

        [TestMethod]
        public void QueryCollectionContainsConstantReferenceThroughReferenceProperty () {
            using (var context = new TestDataContext()) {
                var rat = context.Zombie.Load(new Guid("CE447EF1-6AB3-48B4-BCBD-2C5DFF606593"));
                var query = from b in context.Bullet where b.Zombie.Bitten.Contains(rat) select b;
                var bullets = query.ToList();

                Assert.AreEqual(3, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(3, bullets.Count);
                StraightforwardMappingTests.CheckFred(bullets[0].Zombie, true);
            }
        }

        [TestMethod]
        public void QueryCount () {
            using (var context = new TestDataContext()) {
                var query = from z in context.Zombie where z.Name == "Rat" select z;
                var count = query.Count();

                Assert.AreEqual(0, context.LastQueryStatistics.NumberOfContextCacheMisses);
                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, count);
            }
        }

        [TestMethod]
        public void QueryCountExtraConditions()
        {
            using (var context = new TestDataContext())
            {
                var count = context.Zombie.Where(z => z.CarriedBullet == null).Count(z => z.RemainingAppendages < 5);

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.AreEqual(1, count);
            }
        }

        [TestMethod]
        public void DynamicQueryOnStringProperty () {
            using (var context = new TestDataContext()) {
                var query = context.Get(typeof(Zombie)).Where("Name == \"Rat\"");
                var zombies = query.Cast<Zombie>().ToList();

                Assert.AreEqual(4, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);
                var rat = zombies[0];
                StraightforwardMappingTests.CheckRat(rat, true);
            }
        }

        [TestMethod]
        public void DynamicQueryOnGuid()
        {
            using (var context = new TestDataContext())
            {
                var query = context.Get(typeof(Zombie)).Where("Id == @0", new Guid("CE447EF1-6AB3-48B4-BCBD-2C5DFF606593"));
                var zombies = query.Cast<Zombie>().ToList();

                Assert.AreEqual(4, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                Assert.AreEqual(1, zombies.Count);
                var rat = zombies[0];
                StraightforwardMappingTests.CheckRat(rat, true);
            }
        }

        [TestMethod]
        public void QueryAdHocViewCount () {
            using (var context = new TestDataContext()) {
                var query = from w in context.Wizard orderby w.Name select new { Name = w.Name, NumberOfSpells = w.Spells.Count() };
                var wizardSpells = query.ToList();

                Assert.AreEqual(2, wizardSpells.Count);

                var ein = wizardSpells[0];
                Assert.IsNotNull(ein);
                Assert.AreEqual("Einhowzer", ein.Name);
                Assert.AreEqual(3, ein.NumberOfSpells);

                var merlug = wizardSpells[1];
                Assert.AreEqual("Merlug", merlug.Name);
                Assert.AreEqual(2, merlug.NumberOfSpells);
            }
        }

        [TestMethod]
        public void QueryAdHocViewCountWithCriteria () {
            using (var context = new TestDataContext()) {
                var query = from w in context.Wizard orderby w.Name select new { Name = w.Name, NumberOfSpells = w.Spells.Count(s => s.Description.Contains("t")) };
                var wizardSpells = query.ToList();

                Assert.AreEqual(2, wizardSpells.Count);

                var ein = wizardSpells[0];
                Assert.IsNotNull(ein);
                Assert.AreEqual("Einhowzer", ein.Name);
                Assert.AreEqual(0, ein.NumberOfSpells);

                var merlug = wizardSpells[1];
                Assert.AreEqual("Merlug", merlug.Name);
                Assert.AreEqual(2, merlug.NumberOfSpells);
            }
        }

        [TestMethod]
        public void QueryAdHocReference () {
            using (var context = new TestDataContext()) {
                var query = from s in context.Get<Spell>() where s.Description == "Grease" select s.Wizard;
                var wizardsWithGrease = query.ToList();

                Assert.AreEqual(1, wizardsWithGrease.Count);

                var ein = wizardsWithGrease[0];
                StraightforwardMappingTests.CheckEinhowzer(ein);
            }
        }

        [TestMethod]
        public void QueryAdHocThenSaveWithImplicitlyTrackedContext()
        {
            using (var ctx = new TestDataContext(ContextOptions.TrackChanges))
            {
                ctx.ChangeOptions = ChangeSetOptions.ImplicitlyCommitTrackedChanges;
                var changes = ctx.CreateChangeSet();

                var query = from w in ctx.Wizard orderby w.Name select new { w.Id, Name = w.Name, NumberOfSpells = w.Spells.Count(s => s.Description.Contains("t")) };
                var wizardSpells = query.ToList();

                Assert.AreEqual(2, wizardSpells.Count);
                changes.Commit();
            }
        }

        [TestMethod]
        public void QueryCollectionAny () {
            using (var context = new TestDataContext()) {
                var query = context.Get<Wizard>().Where(w => w.Spells.Any(s => s.Description == "Grease"));
                var wizardsWithGrease = query.ToList();

                Assert.AreEqual(1, wizardsWithGrease.Count);

                var ein = wizardsWithGrease[0];
                StraightforwardMappingTests.CheckEinhowzer(ein);
            }
        }

		[TestMethod]
		public void QueryUnrelatedAny()
		{
			using (var context = new TestDataContext())
			{
				var query1 = context.Get<Wizard>();
				var query2 = context.Get<Spell>();

				var result = query1.Where(w => query2.Any(s => w.Id == s.Id)).ToList();
                Assert.AreEqual(0, result.Count);
			}
		}

        [TestMethod]
        public void QueryJoinOnLoadWithOnly () {
            using (var context = new TestDataContext()) {
                var query = context.Get<Spell>().Where(s => s.Description == "Grease").JoinOnLoadWithOnly(s => s.Wizard);
                var spells = query.ToList();

                Assert.AreEqual(1, spells.Count);

                StraightforwardMappingTests.CheckGrease(spells[0], null);
            }
        }

        [TestMethod]
        public void QueryEmptyArrayContains () {
            using (var context = new TestDataContext()) {
                var ids = new Guid[0];
                var query = context.Zombie.Where(z => ids.Contains(z.Id));
                var zombies = query.ToArray();
                Assert.AreEqual(0, zombies.Length);
            }
        }

        [TestMethod]
        public void QueryEmptyArrayNegateContains()
        {
            using (var context = new TestDataContext())
            {
                var ids = new Guid[0];
                var query = context.Zombie.Where(z => z.Name == "Fred" && !ids.Contains(z.Id));
                var zombies = query.ToArray();
                Assert.AreEqual(1, zombies.Length);
                StraightforwardMappingTests.CheckFred(zombies[0], true);
            }
        }

        [TestMethod]
        public void QueryCountScalar()
        {
            using (var context = new TestDataContext())
            {
                var numberBitten = context.Zombie.Select(z => z.Infected).Count();
                Assert.AreEqual(1, numberBitten);
            }
        }

        [TestMethod]
        public void QuerySumWithIsNull()
        {
            using (var context = new TestDataContext())
            {
                var appendages = context.Zombie.Where(z => z.Name == "Rat").Sum(z => z.RemainingAppendages ?? 0);
                Assert.AreEqual(3, appendages);
            }
        }
    }
}
