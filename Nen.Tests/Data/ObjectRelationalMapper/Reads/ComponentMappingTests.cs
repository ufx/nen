﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Components;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class ComponentMappingTests {
        [TestMethod]
        public void Load () {
            using (var context = new TestDataContext()) {
                Starship ship = context.Starship.Load(new Guid("76A37A13-6BD2-4CC4-AE21-1E9234A58804"));

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckShip(ship);
            }
        }

        #region Individual Tests
        internal static void CheckShip (Starship ship) {
            Assert.IsNotNull(ship);
            Assert.AreEqual(new Guid("76A37A13-6BD2-4CC4-AE21-1E9234A58804"), ship.Id);
            Assert.IsNotNull(ship.AftPhaser);
            Assert.AreEqual(false, ship.AftPhaser.Active);
            Assert.AreEqual(1111, ship.AftPhaser.Power);
            Assert.IsNotNull(ship.ForePhaser);
            Assert.AreEqual(true, ship.ForePhaser.Active);
            Assert.AreEqual(878, ship.ForePhaser.Power);
            Assert.IsNotNull(ship.Missile);
            Assert.AreEqual(26, ship.Missile.Warheads);
            Assert.AreEqual("FOOCC-8909-X", ship.Serial);
            Assert.AreEqual(DateTime.Parse("3007-12-20 21:15:37.107"), ship.ShipTime);
            Assert.IsNotNull(ship.Shuttle);
            Assert.AreEqual("BARSS-7641-Z", ship.Shuttle.Serial);
            Assert.IsNotNull(ship.Shuttle.OmniPhaser);
            Assert.AreEqual(false, ship.Shuttle.OmniPhaser.Active);
            Assert.AreEqual(432, ship.Shuttle.OmniPhaser.Power);
            Assert.IsNotNull(ship.Shield);
            Assert.AreEqual(new Guid("914C8E0E-D14C-427B-A07F-34DB2013BCFE"), ship.Shield.Id);
            Assert.AreEqual(33, ship.Shield.Energy);
            Assert.IsNotNull(ship.Shield.Leech);
            Assert.AreEqual(true, ship.Shield.Leech.SuckingPower);
            Assert.IsNotNull(ship.Clock);
            Assert.AreEqual(DateTime.Parse("3007-12-19 11:44:22"), ship.Clock.WarpTime);
        }
        #endregion
    }
}
