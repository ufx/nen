﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.Immutable;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class ImmutableMappingTests {
        [TestMethod]
        public void Load () {
            using (var context = new TestDataContext()) {
                var tokyoTower = context.EvilTower.Load(new Guid("BDAC851C-E510-4138-B785-638704B70B5A"));
                CheckTokyoTower(tokyoTower);
            }
        }

        #region Individual Tests
        internal static void CheckTokyoTower (EvilTower tokyoTower) {
            Assert.IsNotNull(tokyoTower);
            Assert.AreEqual(new Guid("BDAC851C-E510-4138-B785-638704B70B5A"), tokyoTower.Id);
            Assert.AreEqual("Tokyo Tower", tokyoTower.CodeName);
            Assert.AreEqual(5000, tokyoTower.MindControlAntennaPower);
            Assert.AreEqual(9445, tokyoTower.NumberOfZombiesCreated);
            Assert.IsNotNull(tokyoTower.EvilStuff);
            Assert.IsTrue(tokyoTower.EvilStuff.SequenceEqual(Encoding.Unicode.GetBytes("you must obey")));
        }
        #endregion
    }
}
