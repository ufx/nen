﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.Flattened;

namespace Nen.Tests.Data.ObjectRelationalMapper.Reads {
    [TestClass]
    public class FlattenedMappingTests {
        [TestMethod]
        public void Load () {
            using (var context = new TestDataContext()) {
                var chocliter = context.CandyPlanet.Load(new Guid("74CFE715-E2EC-4ada-8641-9F0B0CB5D7D7"));

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckChocliter(chocliter);
            }
        }

        [TestMethod]
        public void MultiLevelLoad () {
            using (var context = new TestDataContext()) {
                var clownury = context.NoMoreFunPlanet.Load(new Guid("02DE0076-9E52-44d4-A4F9-433A05FF4981"));

                Assert.AreEqual(1, context.LastQueryStatistics.NumberOfDatabaseQueries);
                Assert.IsTrue(context.LastQueryStatistics.TotalTime.TotalSeconds < 1);

                CheckClownury(clownury);
            }
        }

        [TestMethod]
        public void QuerySum () {
            using (var context = new TestDataContext()) {
                var lollipops = context.CandyPlanet.Sum(c => c.NumberOfLollipops);

                Assert.AreEqual(3422, lollipops);
            }
        }

        [TestMethod]
        public void QueryAverage () {
            using (var context = new TestDataContext()) {
                var lollipops = context.CandyPlanet.Average(c => c.NumberOfLollipops);

                Assert.AreEqual(3422, lollipops);
            }
        }

        #region Individual Tests
        private void CheckChocliter (CandyPlanet chocliter) {
            Assert.IsNotNull(chocliter);
            Assert.AreEqual(new Guid("74CFE715-E2EC-4ada-8641-9F0B0CB5D7D7"), chocliter.Id);
            Assert.AreEqual("Chocliter", chocliter.Name);
            Assert.AreEqual(8789, chocliter.Diameter);
            Assert.AreEqual(3422, chocliter.NumberOfLollipops);
        }

        private void CheckClownury (NoMoreFunPlanet clownury) {
            Assert.IsNotNull(clownury);
            Assert.AreEqual(new Guid("02DE0076-9E52-44d4-A4F9-433A05FF4981"), clownury.Id);
            Assert.AreEqual("Clownury", clownury.Name);
            Assert.AreEqual(677, clownury.Diameter);
            Assert.AreEqual(new DateTime(1995, 4, 1), clownury.FunStartDate);
            Assert.AreEqual(new DateTime(1996, 4, 2), clownury.FunEndDate);
        }
        #endregion
    }
}
