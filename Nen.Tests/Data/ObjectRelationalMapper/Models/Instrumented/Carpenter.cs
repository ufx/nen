﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Instrumented {
	[Persistent]
	public class Carpenter {
		public int Id { get; set; }
		public int RageLevel { get; set; }
		[AutoRef] public Hammer Hammer { get; set; }
		[AutoRef] public Saw Saw { get; set; }

		public Carpenter () { }
		public Carpenter (int rageLevel) {
			RageLevel = rageLevel;
		}
	}

	[Persistent]
	public class Hammer {
		public int Id { get; set; }
		[AutoRef] public Claw Claw { get; set; }
		public Handle Handle { get; set; }
		public int ProgrammersBeatenToDeath { get; set; }
	}

	[Persistent]
	public class Claw {
		public int Id { get; set; }
		public bool IsSharp { get; set; }
	}

	[Persistent]
	public class Saw {
		public int Id { get; set; }
		[AutoRef] public Handle Handle { get; set; }
	}

	[Persistent]
	public class Handle {
		public int Id { get; set; }
		public int Length { get; set; }
	}
}
