﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Components {
    [Persistent]
    public class Starship {
        #region Constructors
        public Starship () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        [StringLength(50)] public string Serial { get; set; }
        public ShieldArray Shield { get; set; }
        public DateTime ShipTime { get; set; }
        public MissileBank Missile { get; set; }
        public PhaserBank AftPhaser { get; set; }
        public PhaserBank ForePhaser { get; set; }
        public Shuttle Shuttle { get; set; }
        public WarpClock Clock { get; set; }
        public PhaserBank? AlternatePhaser { get; set; } 
        #endregion
    }

    [Persistent, Component]
    public struct PhaserBank {
        private bool _active;
        private int _power;

        #region Constructors
        public PhaserBank (bool active, int power) {
            _active = active;
            _power = power;
        }
        #endregion

        #region Accessors
        public bool Active {
            get { return _active; }
            set { _active = value; }
        }

        public int Power {
            get { return _power; }
            set { _power = value; }
        }
        #endregion
    }

    [Persistent, Component]
    public struct MissileBank {
        public int Warheads { get; set; }
    }

    [Persistent, Component]
    public struct Shuttle {
        private string _serial;
        private PhaserBank _omniPhaser;

        #region Constructors
        public Shuttle (string serial, PhaserBank omniPhaser) {
            _serial = serial;
            _omniPhaser = omniPhaser;
        }
        #endregion

        #region Accessors
        [StringLength(50)]
        public string Serial {
            get { return _serial; }
            set { _serial = value; }
        }

        public PhaserBank OmniPhaser {
            get { return _omniPhaser; }
            set { _omniPhaser = value; }
        }
        #endregion
    }

    [Persistent, Component]
    public struct WarpClock {
        [TimeStamp] public DateTime WarpTime { get; private set; }
    }
}
