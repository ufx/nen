﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Components {
    [Persistent]
    public class ShieldArray {
        #region Constructors
        public ShieldArray () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        public decimal Energy { get; set; }
        public ShieldLeech Leech { get; set; }
        #endregion
    }

    [Persistent, Component]
    public struct ShieldLeech {
        private bool _suckingPower;

        #region Constructors
        public ShieldLeech (bool suckingPower) {
            _suckingPower = suckingPower;
        }
        #endregion

        #region Accessors
        public bool SuckingPower {
            get { return _suckingPower; }
            set { _suckingPower = value; }
        }
        #endregion
    }
}
