﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Immutable {
    [Persistent]
    public class EvilTower {
        private string _codeName;
        private int _mindControlAntennaPower;
        private Guid _id;

        public EvilTower (Guid id, int mindControlAntennaPower) {
            throw new NotSupportedException("This is the wrong constructor to call.  Test failed.");
        }

        public EvilTower (Guid id, int mindControlAntennaPower, string evilCodeName) {
            _codeName = evilCodeName;
            _id = id;
            _mindControlAntennaPower = mindControlAntennaPower;
        }

        #region Accessors
        [Immutable]
        public Guid Id {
            get { return _id;  }
            set { throw new NotSupportedException("Id is immutable and can only be set via the constructor."); }
        }

        [Immutable]
        public int MindControlAntennaPower {
            get { return _mindControlAntennaPower; }
            set { throw new NotSupportedException("MindContorlAtennaPower is immutable and can only be set via the constructor."); }
        }

        [Immutable("evilCodeName")]
        [StringLength(100)]
        public string CodeName {
            get { return _codeName; }
            set { throw new NotSupportedException("CodeName is immutable and can only be set via the constructor."); }
        }

        public int NumberOfZombiesCreated { get; set; }

        [Range(0, 512)]
        public byte[] EvilStuff { get; set; }
        #endregion
    }
}
