﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward {
    [Persistent]
    public class Wizard {
        #region Constructors
        public Wizard () {
            Id = Guid.NewGuid();
            Spells = new SpellCollection();
            Spells.Wizard = this;
        }
        #endregion

        #region Data Configuration
        [MapConfigurationInitializer]
        private static void ConfigureDataMapping (MapConfiguration configuration) {
            configuration.AssociateWith<Wizard>(w => w.Spells.OrderBy(s => s.Description));
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        [StringLength(100)] public string Name { get; set; }
        [Include] public SpellCollection Spells { get; set; }
        #endregion
    }

    public class SpellCollection : Collection<Spell> {
        #region Constructors
        public SpellCollection () {
        }

        public SpellCollection (IList<Spell> list)
            : base(list) {
        }
        #endregion

        #region Collection Integrity
        protected override void InsertItem (int index, Spell item) {
            item.Wizard = Wizard;

            base.InsertItem(index, item);
        }

        protected override void RemoveItem (int index) {
            this[index].Wizard = null;

            base.RemoveItem(index);
        }

        protected override void SetItem (int index, Spell item) {
            this[index].Wizard = null;
            item.Wizard = Wizard;

            base.SetItem(index, item);
        }

        protected override void ClearItems () {
            foreach (var spell in this)
                spell.Wizard = null;

            base.ClearItems();
        }
        #endregion

        #region Accessors
        public Wizard Wizard { get; internal set; }
        #endregion
    }
}
