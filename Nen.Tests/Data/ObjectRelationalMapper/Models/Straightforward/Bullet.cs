using System;
using System.Collections.Generic;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward {
    public enum BulletKind { Normal = 1, Hollow, Silver }

    [Persistent]
    public class Bullet : Nen.Validation.Validatable {
        #region Constructors
        public Bullet () {
            Id = Guid.NewGuid();
            Kind = BulletKind.Normal;
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        public BulletKind Kind { get; set; }
        public Zombie Zombie { get; set; }
        #endregion
    }
}
