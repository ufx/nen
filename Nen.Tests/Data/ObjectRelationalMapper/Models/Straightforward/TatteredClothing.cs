using System;
using System.Collections.Generic;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward {
    [Persistent]
    public class TatteredClothing {
        #region Constructors
        public TatteredClothing () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        public int NumberOfHoles { get; set; }
        #endregion
    }
}
