﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward {
    [Persistent]
    public class SpellEffect {
        #region Constructors
        public SpellEffect () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        public int Damage { get; set; }
        public Spell Spell { get; set; }
        #endregion
    }
}
