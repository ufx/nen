﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward {
    [Persistent]
    public class Spell {
        #region Constructors
        public Spell () {
            Id = Guid.NewGuid();
            SpellEffects = new Collection<SpellEffect>();
        }
        #endregion

        #region Data Configuration
        [MapConfigurationInitializer]
        private static void ConfigureDataMapping (MapConfiguration configuration) {
            configuration.AssociateWith<Spell>(s => s.SpellEffects.OrderBy(e => e.Damage));
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        [StringLength(100)] public string Description { get; set; }
        public Wizard Wizard { get; set; }
        [Include] public Collection<SpellEffect> SpellEffects { get; set; }
        #endregion
    }
}
