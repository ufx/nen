using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward {
    [Persistent]
    public class Zombie {
        private DataRef<Bullet> _carriedBullet = new DataRef<Bullet>();

        #region Constructors
        public Zombie () {
            Id = Guid.NewGuid();
            Bitten = new Collection<Zombie>();
            Bullets = new Collection<Bullet>();
        }
        #endregion

        [MapConfigurationInitializer]
        private static void ConfigureDataMapping (MapConfiguration configuration) {
            configuration.AssociateWith<Zombie>(z => z.NormalBullets.Where(b => b.Kind == BulletKind.Normal));
            configuration.AssociateWith<Zombie>(z => z.SilverBullets.Where(b => b.Kind == BulletKind.Silver));
            configuration.AssociateWith<Zombie>(z => z.Bullets.OrderBy(b => b.Kind));
        }

        #region Accessors
        public Guid Id { get; set; }
        public Zombie BitBy { get; set; }
        [CollectionKey("BitBy")] public Collection<Zombie> Bitten { get; set; }
        [Include] public Collection<Bullet> Bullets { get; set; }
        [StringLength(50)] public string Name { get; set; }
        [Include]
        public Collection<Bullet> NormalBullets { get; set; }
        public Collection<Bullet> SilverBullets { get; set; }
        public int? RemainingAppendages { get; set; }
        public TatteredClothing TatteredClothing { get; set; }
        public DateTime? Infected { get; set; }
		public TimeSpan VirusIncubationTime { get; set; }

        [TimeStamp] public DateTime DateModified { get; private set; }

        [DataRef("_carriedBullet")]
        public Bullet CarriedBullet {
            get { return _carriedBullet.Value; }
            set { _carriedBullet.Value = value; }
        }

        public DataRef<Bullet> LazyCarriedBullet {
            get { return _carriedBullet; }
        }
        #endregion
    }

    [Persistent, View(typeof(Zombie))]
    public class ZombieView {
        public string Name { get; set; }

        [From("Infected")]
        public DateTime? KnownInfectedDate { get; set; }
    }
}
