﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Experimental {
    [Persistent]
    public class Science {
        private DataRef<byte[]> _results = new DataRef<byte[]>();

        public int Id { get; set; }
        [StringLength(1000)]
        public string Name { get; set; }

        public DataRef<byte[]> ResultsRef {
            get { return _results; }
        }

        [DataRef("_results"), Range(0, int.MaxValue)]
        public byte[] Results {
            get { return _results.Value; }
            set { _results.Value = value; }
        }
    }
}
