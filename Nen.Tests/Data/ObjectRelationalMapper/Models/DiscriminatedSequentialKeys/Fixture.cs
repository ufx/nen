﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.DiscriminatedSequentialKeys {
    public enum FixtureType { Fireplace = 1, Empty }

    [Persistent]
    public abstract class Fixture {
        public int Id { get; private set; }
        public decimal Cost { get; set; }
        [Discriminator] public FixtureType Type { get; private set; }
    }

    [Persistent, DiscriminatorValue(FixtureType.Fireplace)]
    public class FireplaceFixture : Fixture {
        public int MagicalArtifactsBurned { get; set; }
    }

    [Persistent]
    public abstract class OfficeFixture : Fixture {        
        [StringLength(200)] public string Name { get; set; }
    }

    [Persistent, DiscriminatorValue(FixtureType.Empty)]
    public class EmptyFixture : OfficeFixture {
        // Crickets.
    }
}
