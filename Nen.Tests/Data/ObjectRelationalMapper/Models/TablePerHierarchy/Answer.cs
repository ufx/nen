﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.TablePerHierarchy {

    public enum AnswerType { NumberAnswer, TextAnswer, DateAnswer }

    [Persistent, Hierarchy(HierarchyMapping.TablePerHierarchy)]
    public abstract class Answer {
        public int Id { get; set; }

        [Required, StringLength(100)]
        public string Question { get; set; }

        [Discriminator]
        public AnswerType Type { get; set; }
    }

    [Persistent, DiscriminatorValue(AnswerType.NumberAnswer)]
    public class NumberAnswer : Answer {
        public int Number { get; set; }
    }

    [Persistent, DiscriminatorValue(AnswerType.TextAnswer)]
    public class TextAnswer : Answer {
        [Required, StringLength(100)] // Should be nullable anyway due to TablePerHierarchy.
        public string Text { get; set; }
    }

    [Persistent, DiscriminatorValue(AnswerType.DateAnswer)]
    public class DateAnswer : Answer {
        public DateTime Date { get; set; }
    }
}
