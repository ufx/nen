﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Discriminated {
    [Persistent]
    public class Employer {
        #region Constructors
        public Employer () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        [StringLength(50)] public string Name { get; set; }
        [Required] public NinjaEmployee OfficeNinja { get; set; }
        [Required] public Employee Assistant { get; set; }
        #endregion
    }

    [Persistent]
    public class Department {
        #region Constructors
        public Department () {
            Employees = new Collection<Employee>();
        }
        #endregion

        #region Accessors
        public int Id { get; set; }
        [Required, StringLength(50)] public string Name { get; set; }
        [Include] public Collection<Employee> Employees { get; private set; }
        #endregion
    }
}
