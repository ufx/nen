﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;
using Nen.Validation;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Discriminated {
    public enum EmployeeType { Ninja = 1, Clown, PizzaDelivery, RoboClown, VampirePizzaDelivery, Base }

    [Persistent, DiscriminatorValue(EmployeeType.Base)]
    public class Employee {
        #region Constructors
        public Employee () {
            Id = Guid.NewGuid();
            Department = new DataRef<Department>();
        }
        #endregion

        #region Accessors
        public DataRef<Department> Department { get; set; }
        [Discriminator] public EmployeeType Type { get; set; }
        public Guid Id { get; set; }
        [StringLength(50)] public string Name { get; set; }
        #endregion
    }
   
    [Persistent, DiscriminatorValue(EmployeeType.Ninja)]
    public class NinjaEmployee : Employee {
        #region Accessors
        public int PiratesSlain { get; set; }
        #endregion
    }

    [Persistent, DiscriminatorValue(EmployeeType.Clown)]
    public class ClownEmployee : Employee {
        #region Accessors
        public decimal ShoeLength { get; set; }
        public Money? Worth { get; set; }
        #endregion
    }

    [Persistent, DiscriminatorValue(EmployeeType.RoboClown)]
    public class RoboClownEmployee : ClownEmployee {
        #region Accessors
        public int ChildrenVaporized { get; set; }
        #endregion
    }

    [Persistent, DiscriminatorValue(EmployeeType.PizzaDelivery), ReferenceBase]
    public class PizzaDeliveryEmployee : Employee {
        #region Constructors
        public PizzaDeliveryEmployee () {
            DeliveredPizzas = new Collection<Pizza>();
        }
        #endregion

        #region Data Configuration
        [MapConfigurationInitializer]
        private static void ConfigureDataMapping (MapConfiguration configuration) {
            configuration.AssociateWith<PizzaDeliveryEmployee>(e => e.WimpyPizzas.Where(p => p.CheeseFactor < 9000));
        }
        #endregion

        #region Accessors
        [Include] public Collection<Pizza> DeliveredPizzas { get; set; }
        public Collection<Pizza> WimpyPizzas { get; set; }
        #endregion
    }

    [Persistent, DiscriminatorValue(EmployeeType.VampirePizzaDelivery)]
    public class VampirePizzaDeliveryEmployee : PizzaDeliveryEmployee {
        #region Accessors
        [StringLength(50)] public string FavoriteBloodyTopping { get; set; }
        #endregion
    }

    [Persistent]
    public class Pizza {
        #region Constructors
        public Pizza () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        public int CheeseFactor { get; set; }
        public PizzaDeliveryEmployee PizzaDeliveryEmployee { get; set; }
        #endregion
    }

    // This class should never be discovered and persisted.
    public class IgnoredVampirePizzaDeliveryEmployee : VampirePizzaDeliveryEmployee {
        #region Accessors
        public string ThisPropertyDoesntExist { get; set; }
        #endregion
    }
}
