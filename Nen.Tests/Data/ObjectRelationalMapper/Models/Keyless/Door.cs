﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Keyless {
    [Persistent]
    public class Door {
        [StringLength(100)]
        public string Name { get; set; }

        public bool Open { get; set; }
    }
}
