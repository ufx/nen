﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.RenamedPrimaryKeys {
    public enum CubeType { RegularCube = 1, GelatinousCube = 2 }

    [Persistent, DiscriminatorValue(CubeType.RegularCube)]
    public class Cube {
        public int Id { get; set; }

        [Discriminator]
        public CubeType CubeType { get; set; }

        public int X1 { get; set; }
        public int X2 { get; set; }
        public int Y1 { get; set; }
        public int Y2 { get; set; }
        public int Z1 { get; set; }
        public int Z2 { get; set; }
    }
}
