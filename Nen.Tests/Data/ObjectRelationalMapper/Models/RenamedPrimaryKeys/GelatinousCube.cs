﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.RenamedPrimaryKeys {
    [Persistent, DiscriminatorValue(CubeType.GelatinousCube)]
    public class GelatinousCube: Cube {
        public float Permeability { get; set; }
        public int PlayersAbsorbed { get; set; }
    }
}
