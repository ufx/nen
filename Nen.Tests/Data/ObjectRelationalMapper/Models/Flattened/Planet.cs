﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Flattened {
    [Persistent, Flatten]
    public class Planet {
        #region Constructors
        public Planet () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        public int Diameter { get; set; }
        public Guid Id { get; set; }
        [StringLength(50)] public string Name { get; set; }
        #endregion
    }

    [Persistent]
    public class CandyPlanet : Planet {
        public int NumberOfLollipops { get; set; }
    }

    [Persistent, Flatten]
    public class FunPlanet : Planet {
        public DateTime FunStartDate { get; set; }
    }

    [Persistent]
    public class NoMoreFunPlanet : FunPlanet {
        public DateTime FunEndDate { get; set; }
    }
}
