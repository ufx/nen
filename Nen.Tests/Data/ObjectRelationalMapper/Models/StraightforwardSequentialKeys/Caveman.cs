﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Reflection;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.StraightforwardSequentialKeys {
    [FunctionContainer]
    public static class DatabaseFunctions {
        [Function]
        public static string ChangeIt (string input) {
            return input.ToUpper();
        }
    }

    [Persistent, QueryCache(relativeExpirationSeconds: 60)]
    public class Caveman {
        #region Constructors
        public Caveman () {
            PetDinosaurs = new Collection<PetDinosaur>();
        }
        #endregion

        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [CollectionKey("OwnerCaveman")]
        public Collection<PetDinosaur> PetDinosaurs { get; private set; }
        public Collection<CavemanGeneMap> Genes { get; private set; }
        public DateTime? Discovered { get; set; }

        //[ArrayAsString]
        //public int[] PossibleAges { get; set; }
    }

    [Persistent]
    public class PetDinosaur {
        private DataRef<DinoEgg> _egg = new DataRef<DinoEgg>();

        public int Id { get; set; }
        public Caveman OwnerCaveman { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public Collection<DinoTooth> Teeth { get; private set; }

        [DataRef("_egg")]
        public DinoEgg Egg {
            get { return _egg.Value; }
            set { _egg.Value = value; }
        }

        public DataRef<DinoEgg> LazyEgg {
            get { return _egg; }
        }
    }

    [Persistent]
    public class DinoEgg {
        public int Id { get; set; }
        public bool Hatched { get; set; }
    }

    [Persistent]
    public class DinoTooth {
        public int Id { get; set; }
        public int Sharpness { get; set; }
        public PetDinosaur PetDinosaur { get; set; }
    }

    [Persistent]
    public class CavemanGeneMap {
        public int Id { get; set; }
        [Required]
        public Caveman Caveman { get; set; }
        [Required]
        public CavemanGene CavemanGene { get; set; }
    }

    [Persistent]
    public class CavemanGene {
        public CavemanGene () {
            Attributes = new Collection<CavemanGeneAttribute>();
        }

        public int Id { get; set; }
        [Required, StringLength(50)]
        public string Name { get; set; }
        public Collection<CavemanGeneAttribute> Attributes { get; private set; }
    }

    [Persistent]
    public class CavemanGeneAttribute {
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string Name { get; set; }
        [Required]
        public CavemanGene CavemanGene { get; set; }
    }

    public class ArrayAsStringAttribute : UserMapAttribute
    {
        public override VariableMap Create(VariableInfo variable, Type physicalVariableType, TypeMap declaringTypeMap)
        {
            return new ArrayAsStringMapping(variable, physicalVariableType, declaringTypeMap);
        }
    }

    public class ArrayAsStringMapping : PrimitiveVariableMap
    {
        public ArrayAsStringMapping(VariableInfo primitive, Type physicalVariableType, TypeMap declaringTypeMap)
            : base(primitive, physicalVariableType, declaringTypeMap)
        {
            this.DbType = System.Data.DbType.String;
        }
        protected ArrayAsStringMapping(ArrayAsStringMapping copy)
            : base(copy)
        {
            this.DbType = System.Data.DbType.String;
        }

        public override VariableMap Clone()
        {
            return new ArrayAsStringMapping(this);
        }

        protected override object ConvertDatabaseValueCore(object value)
        {
            if (value == null)
                return null;

            // int[] -> string
            var arr = (Array)value;
            return string.Join(",", arr.Cast<object>().Select(v => v.ToString()));
        }

        protected override object ConvertValueForInstance(object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            // string -> int[]
            var str = (string)value;
            var parts = str.Split(',');
            return parts.Select(int.Parse).ToArray();
        }
    }
}
