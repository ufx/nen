﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;
using System.Collections.ObjectModel;

namespace Nen.Tests.Data.ObjectRelationalMapper.Models.Subordinates {
    [Persistent]
    public class Scroll {
        #region Constructors
        public Scroll () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        public Guid Id { get; set; }
        [Subordinate] public ScrollCase ScrollCase { get; set; }
        [Subordinate, DeferJoin] public ScrollParchment ScrollParchment { get; set; }
        [StringLength(1000)]
        public string Text { get; set; }
        #endregion
    }

    [Persistent]
    public class ScrollCase {
        #region Constructors
        public ScrollCase () {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Accessors
        [StringLength(500)]
        public string EngravedText { get; set; }
        public Guid Id { get; set; }
        public Scroll Scroll { get; set; }
        #endregion
    }

    [Persistent]
    public class ScrollParchment {
        public ScrollParchment () {
            Properties = new Collection<ScrollParchmentProperty>();
        }

        #region Accessors
        public Guid Id { get; set; }
        public Scroll Scroll { get; set; }
        public int Quality { get; set; }
        public Collection<ScrollParchmentProperty> Properties { get; private set; }
        #endregion
    }

    [Persistent]
    public class ScrollParchmentProperty {
        public Guid Id { get; set; }
        public ScrollParchment ScrollParchment { get; set; }
        [StringLength(500)]
        public string Name { get; set; }
    }
}
