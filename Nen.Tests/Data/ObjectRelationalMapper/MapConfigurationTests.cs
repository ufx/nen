﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data;
using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Discriminated;
using System.Data.SqlClient;

namespace Nen.Tests.Data.ObjectRelationalMapper {
    [TestClass]
    public class MapConfigurationTests {
        [TestMethod]
        public void TypeMaps () {
            var config = TestConfiguration.CreateConfiguration();
            Assert.IsTrue(config.TypeMaps.Count > 1);
        }

        [TestMethod]
        public void NotPersistedTypes () {
            var config = TestConfiguration.CreateConfiguration();
            Assert.IsFalse(config.TypeMaps.ContainsKey(typeof(IgnoredVampirePizzaDeliveryEmployee)));
        }

        // Not a priority
        //[TestMethod]
        //public void SerializeConfiguration () {
        //    var config = TestConfiguration.CreateConfiguration();
        //    var xml = config.Serialize();
        //    Assert.IsNotNull(xml);
        //}

        [TestMethod]
        public void DefinitionGeneration () {
            var config = TestConfiguration.CreateConfiguration();
            var dataSet = config.GenerateSchemaDataSet();

            CheckSchemaDataSet(dataSet, config.Conventions);

            var definitions = dataSet.GenerateSql();
            var database = TestEnvironment.Database;

            var formatter = database.CreateFormatter();
            formatter.EmbedLiterals = true;
            var cmd = formatter.Generate(definitions);
            Assert.IsNotNull(cmd.CommandText);
        }

        private void CheckSchemaDataSet (DataSet dataSet, Conventions conventions) {
            CheckComponentSchema(dataSet, conventions);
            CheckDiscriminatedSchema(dataSet, conventions);
            CheckTablePerHierarchySchema(dataSet, conventions);
        }

        private void CheckComponentSchema (DataSet dataSet, Conventions conventions) {
            var starship = dataSet.Tables["Starship"];
            Assert.IsNotNull(starship);

            // Ensure null specifiers are correct.
            var forePhaserActive = starship.Columns[conventions.GetDataIdentifierName("ForePhaserActive", DataIdentifierType.Column)];
            Assert.IsNotNull(forePhaserActive);
            Assert.AreEqual(false, forePhaserActive.AllowDBNull);

            var forePhaserPower = starship.Columns[conventions.GetDataIdentifierName("ForePhaserPower", DataIdentifierType.Column)];
            Assert.IsNotNull(forePhaserPower);
            Assert.AreEqual(false, forePhaserPower.AllowDBNull);

            var alternatePhaserActive = starship.Columns[conventions.GetDataIdentifierName("AlternatePhaserActive", DataIdentifierType.Column)];
            Assert.IsNotNull(alternatePhaserActive);
            Assert.AreEqual(true, alternatePhaserActive.AllowDBNull);

            var alternatePhaserPower = starship.Columns[conventions.GetDataIdentifierName("AlternatePhaserPower", DataIdentifierType.Column)];
            Assert.IsNotNull(alternatePhaserPower);
            Assert.AreEqual(true, alternatePhaserPower.AllowDBNull);
        }

        private void CheckDiscriminatedSchema (DataSet dataSet, Conventions conventions) {
            var clownEmployee = dataSet.Tables[conventions.GetDataIdentifierName("ClownEmployee", DataIdentifierType.Table)];
            Assert.IsNotNull(clownEmployee);

            // Ensure column data types are correct.
            var shoeLength = clownEmployee.Columns[conventions.GetDataIdentifierName("ShoeLength", DataIdentifierType.Column)];
            Assert.IsNotNull(shoeLength);
            Assert.AreEqual(typeof(decimal), shoeLength.DataType);

            var worth = clownEmployee.Columns[conventions.GetDataIdentifierName("WorthValue", DataIdentifierType.Column)];
            Assert.IsNotNull(worth);
            Assert.AreEqual(typeof(decimal), worth.DataType);
            Assert.AreEqual(DbType.Currency, worth.GetDbType());
            Assert.IsTrue(worth.AllowDBNull);
        }

        private void CheckTablePerHierarchySchema (DataSet dataSet, Conventions conventions) {
            var answer = dataSet.Tables["Answer"];
            Assert.IsNotNull(answer);

            // Ensure required columns are actually nullable.
            var text = answer.Columns[conventions.GetDataIdentifierName("Text", DataIdentifierType.Column)];
            Assert.IsNotNull(text);
            Assert.IsTrue(text.AllowDBNull);
        }
    }
}
