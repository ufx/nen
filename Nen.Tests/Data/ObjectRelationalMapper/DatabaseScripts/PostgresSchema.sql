﻿-- Setup database
drop database nen;

CREATE DATABASE nen
  WITH ENCODING='UTF8'
       OWNER=nen;

-- Setup schema
CREATE SEQUENCE pkseq_answer; 
CREATE TABLE answer (
    id INTEGER NOT NULL,
    date TIMESTAMP NULL,
    number INTEGER NULL,
    question VARCHAR(100) NOT NULL,
    text VARCHAR(100) NULL,
    type INTEGER NOT NULL,
    CONSTRAINT pk_answer PRIMARY KEY(id)
); 
CREATE TABLE answer_type (
    id INTEGER NOT NULL,
    name VARCHAR(100) NOT NULL,
    short_description VARCHAR(300) NULL,
    CONSTRAINT pk_answer_type PRIMARY KEY(id)
); 
CREATE TABLE bullet (
    id UUID NOT NULL,
    kind INTEGER NOT NULL,
    zombie_id UUID NULL,
    CONSTRAINT pk_bullet PRIMARY KEY(id)
); 
CREATE TABLE bullet_kind (
    id INTEGER NOT NULL,
    name VARCHAR(100) NOT NULL,
    short_description VARCHAR(300) NULL,
    CONSTRAINT pk_bullet_kind PRIMARY KEY(id)
); 
CREATE TABLE candy_planet (
    id UUID NOT NULL,
    diameter INTEGER NOT NULL,
    name VARCHAR(50) NULL,
    number_of_lollipops INTEGER NOT NULL,
    CONSTRAINT pk_candy_planet PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_carpenter; 
CREATE TABLE carpenter (
    id INTEGER NOT NULL,
    hammer_id INTEGER NULL,
    rage_level INTEGER NOT NULL,
    saw_id INTEGER NULL,
    CONSTRAINT pk_carpenter PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_caveman; 
CREATE TABLE caveman (
    id INTEGER NOT NULL,
    discovered TIMESTAMP NULL,
    name VARCHAR(50) NULL,
    CONSTRAINT pk_caveman PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_caveman_gene; 
CREATE TABLE caveman_gene (
    id INTEGER NOT NULL,
    name VARCHAR(50) NOT NULL,
    CONSTRAINT pk_caveman_gene PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_caveman_gene_attribute; 
CREATE TABLE caveman_gene_attribute (
    id INTEGER NOT NULL,
    caveman_gene_id INTEGER NOT NULL,
    name VARCHAR(50) NOT NULL,
    CONSTRAINT pk_caveman_gene_attribute PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_caveman_gene_map; 
CREATE TABLE caveman_gene_map (
    id INTEGER NOT NULL,
    caveman_gene_id INTEGER NOT NULL,
    caveman_id INTEGER NOT NULL,
    CONSTRAINT pk_caveman_gene_map PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_claw; 
CREATE TABLE claw (
    id INTEGER NOT NULL,
    is_sharp BOOL NOT NULL,
    CONSTRAINT pk_claw PRIMARY KEY(id)
); 
CREATE TABLE clown_employee (
    id UUID NOT NULL,
    shoe_length DECIMAL(18, 4) NOT NULL,
    worth_value DECIMAL(18, 6) NULL,
    CONSTRAINT pk_clown_employee PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_cube; 
CREATE TABLE cube (
    CubeId INTEGER NOT NULL,
    cube_type INTEGER NOT NULL,
    x1 INTEGER NOT NULL,
    x2 INTEGER NOT NULL,
    y1 INTEGER NOT NULL,
    y2 INTEGER NOT NULL,
    z1 INTEGER NOT NULL,
    z2 INTEGER NOT NULL,
    CONSTRAINT pk_cube PRIMARY KEY(CubeId)
); 
CREATE TABLE cube_type (
    id INTEGER NOT NULL,
    name VARCHAR(100) NOT NULL,
    short_description VARCHAR(300) NULL,
    CONSTRAINT pk_cube_type PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_department; 
CREATE TABLE department (
    id INTEGER NOT NULL,
    name VARCHAR(50) NOT NULL,
    CONSTRAINT pk_department PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_dino_egg; 
CREATE TABLE dino_egg (
    id INTEGER NOT NULL,
    hatched BOOL NOT NULL,
    CONSTRAINT pk_dino_egg PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_dino_tooth; 
CREATE TABLE dino_tooth (
    id INTEGER NOT NULL,
    pet_dinosaur_id INTEGER NULL,
    sharpness INTEGER NOT NULL,
    CONSTRAINT pk_dino_tooth PRIMARY KEY(id)
); 
CREATE TABLE door (
    name VARCHAR(100) NULL,
    open BOOL NOT NULL
); 
CREATE TABLE employee (
    id UUID NOT NULL,
    department_id INTEGER NULL,
    name VARCHAR(50) NULL,
    type INTEGER NOT NULL,
    CONSTRAINT pk_employee PRIMARY KEY(id)
); 
CREATE TABLE employee_type (
    id INTEGER NOT NULL,
    name VARCHAR(100) NOT NULL,
    short_description VARCHAR(300) NULL,
    CONSTRAINT pk_employee_type PRIMARY KEY(id)
); 
CREATE TABLE employer (
    id UUID NOT NULL,
    assistant_id UUID NOT NULL,
    name VARCHAR(50) NULL,
    office_ninja_id UUID NOT NULL,
    CONSTRAINT pk_employer PRIMARY KEY(id)
); 
CREATE TABLE empty_fixture (
    id INTEGER NOT NULL,
    CONSTRAINT pk_empty_fixture PRIMARY KEY(id)
); 
CREATE TABLE evil_tower (
    id UUID NOT NULL,
    code_name VARCHAR(100) NULL,
    evil_stuff BYTEA NULL,
    mind_control_antenna_power INTEGER NOT NULL,
    number_of_zombies_created INTEGER NOT NULL,
    CONSTRAINT pk_evil_tower PRIMARY KEY(id)
); 
CREATE TABLE fireplace_fixture (
    id INTEGER NOT NULL,
    magical_artifacts_burned INTEGER NOT NULL,
    CONSTRAINT pk_fireplace_fixture PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_fixture; 
CREATE TABLE fixture (
    id INTEGER NOT NULL,
    cost DECIMAL(18, 4) NOT NULL,
    type INTEGER NOT NULL,
    CONSTRAINT pk_fixture PRIMARY KEY(id)
); 
CREATE TABLE fixture_type (
    id INTEGER NOT NULL,
    name VARCHAR(100) NOT NULL,
    short_description VARCHAR(300) NULL,
    CONSTRAINT pk_fixture_type PRIMARY KEY(id)
); 
CREATE TABLE gelatinous_cube (
    GelatinousCubeId INTEGER NOT NULL,
    permeability FLOAT(24) NOT NULL,
    players_absorbed INTEGER NOT NULL,
    CONSTRAINT pk_gelatinous_cube PRIMARY KEY(GelatinousCubeId)
); 
CREATE SEQUENCE pkseq_hammer; 
CREATE TABLE hammer (
    id INTEGER NOT NULL,
    claw_id INTEGER NULL,
    handle_id INTEGER NULL,
    programmers_beaten_to_death INTEGER NOT NULL,
    CONSTRAINT pk_hammer PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_handle; 
CREATE TABLE handle (
    id INTEGER NOT NULL,
    length INTEGER NOT NULL,
    CONSTRAINT pk_handle PRIMARY KEY(id)
); 
CREATE TABLE ninja_employee (
    id UUID NOT NULL,
    pirates_slain INTEGER NOT NULL,
    CONSTRAINT pk_ninja_employee PRIMARY KEY(id)
); 
CREATE TABLE no_more_fun_planet (
    id UUID NOT NULL,
    diameter INTEGER NOT NULL,
    fun_end_date TIMESTAMP NOT NULL,
    fun_start_date TIMESTAMP NOT NULL,
    name VARCHAR(50) NULL,
    CONSTRAINT pk_no_more_fun_planet PRIMARY KEY(id)
); 
CREATE TABLE office_fixture (
    id INTEGER NOT NULL,
    name VARCHAR(200) NULL,
    CONSTRAINT pk_office_fixture PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_pet_dinosaur; 
CREATE TABLE pet_dinosaur (
    id INTEGER NOT NULL,
    egg_id INTEGER NULL,
    name VARCHAR(50) NULL,
    owner_caveman_id INTEGER NULL,
    CONSTRAINT pk_pet_dinosaur PRIMARY KEY(id)
); 
CREATE TABLE pizza (
    id UUID NOT NULL,
    cheese_factor INTEGER NOT NULL,
    pizza_delivery_employee_id UUID NULL,
    CONSTRAINT pk_pizza PRIMARY KEY(id)
); 
CREATE TABLE robo_clown_employee (
    id UUID NOT NULL,
    children_vaporized INTEGER NOT NULL,
    CONSTRAINT pk_robo_clown_employee PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_saw; 
CREATE TABLE saw (
    id INTEGER NOT NULL,
    handle_id INTEGER NULL,
    CONSTRAINT pk_saw PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_science; 
CREATE TABLE science (
    id INTEGER NOT NULL,
    name VARCHAR(1000) NULL,
    results BYTEA NULL,
    CONSTRAINT pk_science PRIMARY KEY(id)
); 
CREATE TABLE scroll (
    id UUID NOT NULL,
    text VARCHAR(1000) NULL,
    CONSTRAINT pk_scroll PRIMARY KEY(id)
); 
CREATE TABLE scroll_case (
    id UUID NOT NULL,
    engraved_text VARCHAR(500) NULL,
    scroll_id UUID NULL,
    CONSTRAINT pk_scroll_case PRIMARY KEY(id)
); 
CREATE TABLE scroll_parchment (
    id UUID NOT NULL,
    quality INTEGER NOT NULL,
    scroll_id UUID NULL,
    CONSTRAINT pk_scroll_parchment PRIMARY KEY(id)
); 
CREATE TABLE scroll_parchment_property (
    id UUID NOT NULL,
    name VARCHAR(500) NULL,
    scroll_parchment_id UUID NULL,
    CONSTRAINT pk_scroll_parchment_property PRIMARY KEY(id)
); 
CREATE TABLE shield_array (
    id UUID NOT NULL,
    energy DECIMAL(18, 4) NOT NULL,
    leech_sucking_power BOOL NOT NULL,
    CONSTRAINT pk_shield_array PRIMARY KEY(id)
); 
CREATE TABLE spell (
    id UUID NOT NULL,
    description VARCHAR(100) NULL,
    wizard_id UUID NULL,
    CONSTRAINT pk_spell PRIMARY KEY(id)
); 
CREATE TABLE spell_effect (
    id UUID NOT NULL,
    damage INTEGER NOT NULL,
    spell_id UUID NULL,
    CONSTRAINT pk_spell_effect PRIMARY KEY(id)
); 
CREATE TABLE starship (
    id UUID NOT NULL,
    aft_phaser_active BOOL NOT NULL,
    aft_phaser_power INTEGER NOT NULL,
    alternate_phaser_active BOOL NULL,
    alternate_phaser_power INTEGER NULL,
    clock_warp_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    fore_phaser_active BOOL NOT NULL,
    fore_phaser_power INTEGER NOT NULL,
    missile_warheads INTEGER NOT NULL,
    serial VARCHAR(50) NULL,
    shield_id UUID NULL,
    ship_time TIMESTAMP NOT NULL,
    shuttle_omni_phaser_active BOOL NOT NULL,
    shuttle_omni_phaser_power INTEGER NOT NULL,
    shuttle_serial VARCHAR(50) NULL,
    CONSTRAINT pk_starship PRIMARY KEY(id)
); 
CREATE TABLE tattered_clothing (
    id UUID NOT NULL,
    number_of_holes INTEGER NOT NULL,
    CONSTRAINT pk_tattered_clothing PRIMARY KEY(id)
); 
CREATE SEQUENCE pkseq_treehouse; 
CREATE TABLE treehouse (
    id INTEGER NOT NULL,
    height INTEGER NULL,
    width INTEGER NULL,
    CONSTRAINT pk_treehouse PRIMARY KEY(id)
); 
CREATE TABLE vampire_pizza_delivery_employee (
    id UUID NOT NULL,
    favorite_bloody_topping VARCHAR(50) NULL,
    CONSTRAINT pk_vampire_pizza_delivery_employee PRIMARY KEY(id)
); 
CREATE TABLE wizard (
    id UUID NOT NULL,
    name VARCHAR(100) NULL,
    CONSTRAINT pk_wizard PRIMARY KEY(id)
); 
CREATE TABLE zombie (
    id UUID NOT NULL,
    bit_by_id UUID NULL,
    carried_bullet_id UUID NULL,
    date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    infected TIMESTAMP NULL,
    name VARCHAR(50) NULL,
    remaining_appendages INTEGER NULL,
    tattered_clothing_id UUID NULL,
    virus_incubation_time TIME NOT NULL,
    CONSTRAINT pk_zombie PRIMARY KEY(id)
); 
ALTER TABLE answer 
    ADD CONSTRAINT fk_answer_answer_type_type FOREIGN KEY(type) REFERENCES answer_type (id); 
ALTER TABLE bullet 
    ADD CONSTRAINT fk_bullet_zombie_zombie_id FOREIGN KEY(zombie_id) REFERENCES zombie (id), 
    ADD CONSTRAINT fk_bullet_bullet_kind_kind FOREIGN KEY(kind) REFERENCES bullet_kind (id); 
ALTER TABLE carpenter 
    ADD CONSTRAINT fk_carpenter_hammer_hammer_id FOREIGN KEY(hammer_id) REFERENCES hammer (id), 
    ADD CONSTRAINT fk_carpenter_saw_saw_id FOREIGN KEY(saw_id) REFERENCES saw (id); 
ALTER TABLE caveman_gene_attribute 
    ADD CONSTRAINT fk_caveman_gene_attribute_caveman_gene_caveman_gene_id FOREIGN KEY(caveman_gene_id) REFERENCES caveman_gene (id); 
ALTER TABLE caveman_gene_map 
    ADD CONSTRAINT fk_caveman_gene_map_caveman_caveman_id FOREIGN KEY(caveman_id) REFERENCES caveman (id), 
    ADD CONSTRAINT fk_caveman_gene_map_caveman_gene_caveman_gene_id FOREIGN KEY(caveman_gene_id) REFERENCES caveman_gene (id); 
ALTER TABLE clown_employee 
    ADD CONSTRAINT fk_clown_employee_employee_pk FOREIGN KEY(id) REFERENCES employee (id); 
ALTER TABLE cube 
    ADD CONSTRAINT fk_cube_cube_type_cube_type FOREIGN KEY(cube_type) REFERENCES cube_type (id); 
ALTER TABLE dino_tooth 
    ADD CONSTRAINT fk_dino_tooth_pet_dinosaur_pet_dinosaur_id FOREIGN KEY(pet_dinosaur_id) REFERENCES pet_dinosaur (id); 
ALTER TABLE employee 
    ADD CONSTRAINT fk_employee_department_department_id FOREIGN KEY(department_id) REFERENCES department (id), 
    ADD CONSTRAINT fk_employee_employee_type_type FOREIGN KEY(type) REFERENCES employee_type (id); 
ALTER TABLE employer 
    ADD CONSTRAINT fk_employer_ninja_employee_office_ninja_id FOREIGN KEY(office_ninja_id) REFERENCES ninja_employee (id), 
    ADD CONSTRAINT fk_employer_employee_assistant_id FOREIGN KEY(assistant_id) REFERENCES employee (id); 
ALTER TABLE empty_fixture 
    ADD CONSTRAINT fk_empty_fixture_office_fixture_pk FOREIGN KEY(id) REFERENCES office_fixture (id); 
ALTER TABLE fireplace_fixture 
    ADD CONSTRAINT fk_fireplace_fixture_fixture_pk FOREIGN KEY(id) REFERENCES fixture (id); 
ALTER TABLE fixture 
    ADD CONSTRAINT fk_fixture_fixture_type_type FOREIGN KEY(type) REFERENCES fixture_type (id); 
ALTER TABLE gelatinous_cube 
    ADD CONSTRAINT fk_gelatinous_cube_cube_pk FOREIGN KEY(GelatinousCubeId) REFERENCES cube (CubeId); 
ALTER TABLE hammer 
    ADD CONSTRAINT fk_hammer_claw_claw_id FOREIGN KEY(claw_id) REFERENCES claw (id), 
    ADD CONSTRAINT fk_hammer_handle_handle_id FOREIGN KEY(handle_id) REFERENCES handle (id); 
ALTER TABLE ninja_employee 
    ADD CONSTRAINT fk_ninja_employee_employee_pk FOREIGN KEY(id) REFERENCES employee (id); 
ALTER TABLE office_fixture 
    ADD CONSTRAINT fk_office_fixture_fixture_pk FOREIGN KEY(id) REFERENCES fixture (id); 
ALTER TABLE pet_dinosaur 
    ADD CONSTRAINT fk_pet_dinosaur_caveman_owner_caveman_id FOREIGN KEY(owner_caveman_id) REFERENCES caveman (id), 
    ADD CONSTRAINT fk_pet_dinosaur_dino_egg_egg_id FOREIGN KEY(egg_id) REFERENCES dino_egg (id); 
ALTER TABLE pizza 
    ADD CONSTRAINT fk_pizza_employee_pizza_delivery_employee_id FOREIGN KEY(pizza_delivery_employee_id) REFERENCES employee (id); 
ALTER TABLE robo_clown_employee 
    ADD CONSTRAINT fk_robo_clown_employee_clown_employee_pk FOREIGN KEY(id) REFERENCES clown_employee (id); 
ALTER TABLE saw 
    ADD CONSTRAINT fk_saw_handle_handle_id FOREIGN KEY(handle_id) REFERENCES handle (id); 
ALTER TABLE scroll_case 
    ADD CONSTRAINT fk_scroll_case_scroll_scroll_id FOREIGN KEY(scroll_id) REFERENCES scroll (id); 
ALTER TABLE scroll_parchment 
    ADD CONSTRAINT fk_scroll_parchment_scroll_scroll_id FOREIGN KEY(scroll_id) REFERENCES scroll (id); 
ALTER TABLE scroll_parchment_property 
    ADD CONSTRAINT fk_scroll_parchment_property_scroll_parchment_scroll_parchment_id FOREIGN KEY(scroll_parchment_id) REFERENCES scroll_parchment (id); 
ALTER TABLE spell 
    ADD CONSTRAINT fk_spell_wizard_wizard_id FOREIGN KEY(wizard_id) REFERENCES wizard (id); 
ALTER TABLE spell_effect 
    ADD CONSTRAINT fk_spell_effect_spell_spell_id FOREIGN KEY(spell_id) REFERENCES spell (id); 
ALTER TABLE starship 
    ADD CONSTRAINT fk_starship_shield_array_shield_id FOREIGN KEY(shield_id) REFERENCES shield_array (id); 
ALTER TABLE vampire_pizza_delivery_employee 
    ADD CONSTRAINT fk_vampire_pizza_delivery_employee_employee_pk FOREIGN KEY(id) REFERENCES employee (id); 
ALTER TABLE zombie 
    ADD CONSTRAINT fk_zombie_zombie_bit_by_id FOREIGN KEY(bit_by_id) REFERENCES zombie (id), 
    ADD CONSTRAINT fk_zombie_tattered_clothing_tattered_clothing_id FOREIGN KEY(tattered_clothing_id) REFERENCES tattered_clothing (id), 
    ADD CONSTRAINT fk_zombie_bullet_carried_bullet_id FOREIGN KEY(carried_bullet_id) REFERENCES bullet (id); 
INSERT INTO answer_type (id, name, short_description) VALUES('0', 'NumberAnswer', NULL); 
INSERT INTO answer_type (id, name, short_description) VALUES('1', 'TextAnswer', NULL); 
INSERT INTO answer_type (id, name, short_description) VALUES('2', 'DateAnswer', NULL); 
INSERT INTO bullet_kind (id, name, short_description) VALUES('1', 'Normal', NULL); 
INSERT INTO bullet_kind (id, name, short_description) VALUES('2', 'Hollow', NULL); 
INSERT INTO bullet_kind (id, name, short_description) VALUES('3', 'Silver', NULL); 
INSERT INTO cube_type (id, name, short_description) VALUES('1', 'RegularCube', NULL); 
INSERT INTO cube_type (id, name, short_description) VALUES('2', 'GelatinousCube', NULL); 
INSERT INTO employee_type (id, name, short_description) VALUES('1', 'Ninja', NULL); 
INSERT INTO employee_type (id, name, short_description) VALUES('2', 'Clown', NULL); 
INSERT INTO employee_type (id, name, short_description) VALUES('3', 'PizzaDelivery', NULL); 
INSERT INTO employee_type (id, name, short_description) VALUES('4', 'RoboClown', NULL); 
INSERT INTO employee_type (id, name, short_description) VALUES('5', 'VampirePizzaDelivery', NULL); 
INSERT INTO employee_type (id, name, short_description) VALUES('6', 'Base', NULL); 
INSERT INTO fixture_type (id, name, short_description) VALUES('1', 'Fireplace', NULL); 
INSERT INTO fixture_type (id, name, short_description) VALUES('2', 'Empty', NULL);


-- Procedure Creates

CREATE OR REPLACE FUNCTION ReadDoorByName(iName VARCHAR)
RETURNS table (name VARCHAR, open BOOL)
AS $$
	BEGIN RETURN QUERY
		SELECT * FROM Door WHERE Door.Name = iName;
	END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ReadStarshipBySerial (iSerial VARCHAR)
RETURNS table (
	id UUID,
	aft_phaser_active BOOL,
    aft_phaser_power INTEGER,
    alternate_phaser_active BOOL,
    alternate_phaser_power INTEGER,
    clock_warp_time TIMESTAMP,
    fore_phaser_active BOOL,
    fore_phaser_power INTEGER,
    missile_warheads INTEGER,
    serial VARCHAR,
    shield_id UUID,
    ship_time TIMESTAMP,
    shuttle_omni_phaser_active BOOL,
    shuttle_omni_phaser_power INTEGER,
    shuttle_serial VARCHAR
)
AS $$
	BEGIN RETURN QUERY
		SELECT * FROM Starship where Starship.serial = iSerial;
	END
$$ LANGUAGE plpgsql;
