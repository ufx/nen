﻿delete from PET_DINOSAUR where NAME in ('WrittenKorgo', 'WrittenLork');
delete from dino_egg where hatched = 'T';
delete from CAVEMAN where NAME IN ('WrittenGwar', 'WrittenSmlarg', 'WrittenSmlargy', 'WrittenMug', 'WrittenShlug');
delete from ZOMBIE WHERE Name IN ('WrittenRomero', 'WrittenHeather', 'UnwrittenBadThing', 'WrittenNewGuy', 'WrittenDeleter');
delete from TATTERED_CLOTHING WHERE NUMBER_OF_HOLES IN (62, 66, 67, 68);
update ZOMBIE SET REMAINING_APPENDAGES = 3, DATE_MODIFIED = timestamp'1980-01-01 00:00:00' WHERE Id = 'ce447ef16ab348b4bcbd2c5dff606593';
delete from SPELL_EFFECT WHERE Damage = 909090;
delete from Spell WHERE Description = 'test tracking collection';
delete from Treehouse;
delete from EVIL_TOWER WHERE CODE_NAME = 'WrittenTower';
delete from Science WHERE Name IN ('Zombrex', 'BlackHelicopters', 'Quacktor');
delete from FIREPLACE_FIXTURE;
delete from EMPTY_FIXTURE WHERE Id IN (SELECT Id FROM OFFICE_FIXTURE WHERE Name IN ('WrittenReferenceBase', 'WrittenTrackedReferenceBase'));
delete from OFFICE_FIXTURE WHERE Name IN ('WrittenReferenceBase', 'WrittenTrackedReferenceBase');
delete from Fixture WHERE Type = 1 OR Cost = 1231 OR Cost = 3434;
update OFFICE_FIXTURE SET Name = 'GotNothing' WHERE Id = 1;
delete from Scroll WHERE Text = 'TestTrackedSubordinateWrite';
delete from SCROLL_PARCHMENT WHERE Quality = 8888;
delete from SHIELD_ARRAY WHERE Energy IN (78);
delete from Starship WHERE Serial IN ('WRITTEN-FOOBAR123', 'WRITTEN-POPULAR');