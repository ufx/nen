BEGIN TRAN

-- Deletes

DELETE FROM Zombie WHERE CarriedBulletId IS NOT NULL
DELETE FROM Bullet
DELETE FROM Zombie
DELETE FROM TatteredClothing
DELETE FROM Starship
DELETE FROM ShieldArray
DELETE FROM Pizza
DELETE FROM Employer
DELETE FROM VampirePizzaDeliveryEmployee
DELETE FROM NinjaEmployee
DELETE FROM RoboClownEmployee
DELETE FROM ClownEmployee
DELETE FROM Employee
DELETE FROM CandyPlanet
DELETE FROM NoMoreFunPlanet
DELETE FROM SpellEffect
DELETE FROM Spell
DELETE FROM Wizard
DELETE FROM EvilTower
DELETE FROM ScrollParchmentProperty
DELETE FROM ScrollCase
DELETE FROM ScrollParchment
DELETE FROM [Scroll]
DELETE FROM DinoTooth
DELETE FROM PetDinosaur
DELETE FROM DinoEgg
DELETE FROM CavemanGeneMap
DELETE FROM CavemanGeneAttribute
DELETE FROM CavemanGene
DELETE FROM Caveman
DELETE FROM FireplaceFixture
DELETE FROM EmptyFixture
DELETE FROM OfficeFixture
DELETE FROM Fixture
DELETE FROM Door
DELETE FROM Department
DELETE FROM GelatinousCube
DELETE FROM [Cube]
DELETE FROM Science
DELETE FROM Carpenter
DELETE FROM Hammer
DELETE FROM Saw
DELETE FROM Claw
DELETE FROM Handle

-- Door

INSERT INTO Door (Name, [Open])
	VALUES('Portal', 1)
	
INSERT INTO Door (Name, [Open])
	VALUES('Gate', 0)

-- Caveman

SET IDENTITY_INSERT Caveman ON

INSERT INTO Caveman (Id, Name, Discovered)
	VALUES(1, 'Ugg', '02/07/2000')

SET IDENTITY_INSERT Caveman OFF

-- DinoEgg

SET IDENTITY_INSERT DinoEgg ON

INSERT INTO DinoEgg (Id, Hatched)
	VALUES(1, 0)
	
SET IDENTITY_INSERT DinoEgg OFF

-- PetDinosaur

SET IDENTITY_INSERT PetDinosaur ON

INSERT INTO PetDinosaur (Id, OwnerCavemanId, Name, EggId) VALUES
	(1, 1, 'Thorax', 1),
	(2, null, 'Jorg', null)

SET IDENTITY_INSERT PetDinosaur OFF

-- DinoTooth

SET IDENTITY_INSERT DinoTooth ON

INSERT INTO DinoTooth (Id, PetDinosaurId, Sharpness) VALUES(1, 1, 53543)

SET IDENTITY_INSERT DinoTooth OFF

-- Caveman Genes and Attributes

SET IDENTITY_INSERT CavemanGene ON

INSERT INTO CavemanGene (Id, Name)
	VALUES(1, 'TTACAA')

SET IDENTITY_INSERT CavemanGene OFF

SET IDENTITY_INSERT CavemanGeneAttribute ON

INSERT INTO CavemanGeneAttribute (Id, Name, CavemanGeneId)
	VALUES(1, 'Unibrow', 1)

SET IDENTITY_INSERT CavemanGeneAttribute OFF

SET IDENTITY_INSERT CavemanGeneMap ON

INSERT INTO CavemanGeneMap (Id, CavemanId, CavemanGeneId)
	VALUES(1, 1, 1)

SET IDENTITY_INSERT CavemanGeneMap OFF

-- Scroll

INSERT INTO [Scroll] (Id, Text)
	VALUES('1218A3C6-668C-4e82-8586-82F55CE9F67A', 'This scroll has a lot of words.')
	
INSERT INTO [Scroll] (Id, Text)
	VALUES('BA03F307-52CE-4200-81B7-764BF51C4F7D', 'This scroll has a list of popular names for chicken.')
	
-- ScrollCase

INSERT INTO ScrollCase (Id, ScrollId, EngravedText)
	VALUES('847AEA0F-2B5C-4090-8C23-9690DFE71616', '1218A3C6-668C-4e82-8586-82F55CE9F67A', 'Words of wisdom.')
	
INSERT INTO ScrollCase (Id, ScrollId, EngravedText)
	VALUES('C6A90783-A030-4f40-99D0-B122E2324BEE', NULL, 'Emptiness.')

-- ScrollParchment

INSERT INTO ScrollParchment (Id, ScrollId, Quality)
	VALUES('351FB9AE-6407-4745-970F-636C33AA2B41', '1218A3C6-668C-4e82-8586-82F55CE9F67A', 99)

-- ScrollParchmentProperty

INSERT INTO ScrollParchmentProperty (Id, ScrollParchmentId, Name)
	VALUES('DC7223B3-475A-4630-BD06-2B1C46C772A5', '351FB9AE-6407-4745-970F-636C33AA2B41', '~Magic~')

-- EvilTower

INSERT INTO EvilTower (Id, MindControlAntennaPower, CodeName, NumberOfZombiesCreated, EvilStuff)
	VALUES('BDAC851C-E510-4138-B785-638704B70B5A', 5000, 'Tokyo Tower', 9445, 0x79006F00750020006D0075007300740020006F00620065007900)
	
INSERT INTO EvilTower (Id, MindControlAntennaPower, CodeName, NumberOfZombiesCreated)
	VALUES('24486DB3-6D04-41af-A031-5DE8CB308A53', 3333, 'Big Ben', 12311)

-- Wizard

INSERT INTO Wizard (Id, Name)
	VALUES('4305C3CF-68CB-4e79-BF7C-60306CD18053', 'Einhowzer')
	
INSERT INTO Wizard (Id, Name)
	VALUES('DD244C2F-958A-4f1c-863E-6154BF891DD5', 'Merlug')
	
-- Spell

INSERT INTO Spell (Id, Description, WizardId)
	VALUES('70B7FBB8-6A6A-44c3-B0A3-95695D7A0AEF', 'Magic Missive', '4305C3CF-68CB-4e79-BF7C-60306CD18053')

INSERT INTO Spell (Id, Description, WizardId)
	VALUES('FA5BCFAA-9F72-4a18-8CB8-C10F6729FF62', 'Grease', '4305C3CF-68CB-4e79-BF7C-60306CD18053')
	
INSERT INTO Spell (Id, Description, WizardId)
	VALUES('61674321-3F87-49cb-8A09-40A27FE59BA6', 'Shocking Beard', '4305C3CF-68CB-4e79-BF7C-60306CD18053')
	
INSERT INTO Spell (Id, Description, WizardId)
	VALUES('2399781E-9775-4211-8E27-6BB6D1FE2842', 'Create Lesser Acne', 'DD244C2F-958A-4f1c-863E-6154BF891DD5')
	
INSERT INTO Spell (Id, Description, WizardId)
	VALUES('A353530C-822E-4e77-87C6-90DFC8173825', 'Flashlight', 'DD244C2F-958A-4f1c-863E-6154BF891DD5')
	
-- SpellEffect
	
INSERT INTO SpellEffect (Id, Damage, SpellId)
	VALUES('6BAAA889-E4B1-4c68-ABC6-03FFC5B3710C', 10, '70B7FBB8-6A6A-44c3-B0A3-95695D7A0AEF')
	
INSERT INTO SpellEffect (Id, Damage, SpellId)
	VALUES('DC657CEE-CD5F-45d6-A03C-5EBA5AD5B3D0', 200, 'FA5BCFAA-9F72-4a18-8CB8-C10F6729FF62')

INSERT INTO SpellEffect (Id, Damage, SpellId)
	VALUES('03E88C3E-F49D-4b12-B2B2-71C246220A78', 333, 'FA5BCFAA-9F72-4a18-8CB8-C10F6729FF62')

INSERT INTO SpellEffect (Id, Damage, SpellId)
	VALUES('7642F750-9A93-4958-A737-4BA9DA0B4FC0', 15, '61674321-3F87-49cb-8A09-40A27FE59BA6')

INSERT INTO SpellEffect (Id, Damage, SpellId)
	VALUES('CA71487E-4145-42c2-A33D-5DD92D23506A', 9001, '2399781E-9775-4211-8E27-6BB6D1FE2842')

INSERT INTO SpellEffect (Id, Damage, SpellId)
	VALUES('913D4937-2234-4fb3-A9A5-B824380579A5', 0, 'A353530C-822E-4e77-87C6-90DFC8173825')

-- CandyPlanet

INSERT INTO CandyPlanet (Id, Name, Diameter, NumberOfLollipops)
	VALUES('74CFE715-E2EC-4ada-8641-9F0B0CB5D7D7', 'Chocliter', 8789, 3422)

-- NoMoreFunPlanet

INSERT INTO NoMoreFunPlanet (Id, Name, Diameter, FunStartDate, FunEndDate)
	VALUES('02DE0076-9E52-44d4-A4F9-433A05FF4981', 'Clownury', 677, '04/01/1995', '04/02/1996')

-- Department

SET IDENTITY_INSERT Department ON

INSERT INTO Department (Id, Name)
	VALUES(1, 'Pizza Ninjas')
	
INSERT INTO Department (Id, Name)
	VALUES(2, 'Nightmares')
	
SET IDENTITY_INSERT Department OFF

-- Base Employee
INSERT INTO Employee (Id, Name, Type)
	VALUES('2F8FD845-DA28-4cfe-96CA-60EA7E651C6A', 'High5', 6)

-- NinjaEmployee

INSERT INTO Employee (Id, Name, Type)
	VALUES('B587C85B-A841-4d90-A64B-2B26C417D977', 'Sanosuke', 1)
	
INSERT INTO NinjaEmployee (Id, PiratesSlain)
	VALUES('B587C85B-A841-4d90-A64B-2B26C417D977', 10)
	
INSERT INTO Employee (Id, Name, Type, DepartmentId)
	VALUES('3A8F48E4-D31B-47af-9A64-9F14B420D7D6', 'Farley', 1, 1)
	
INSERT INTO NinjaEmployee (Id, PiratesSlain)
	VALUES('3A8F48E4-D31B-47af-9A64-9F14B420D7D6', 1)
	
-- ClownEmployee

INSERT INTO Employee (Id, Name, Type, DepartmentId)
	VALUES('E031BE0B-E9B0-4a29-ACDC-A4661D5556CE', 'Bozo', 2, 2)

INSERT INTO ClownEmployee (Id, ShoeLength)
	VALUES('E031BE0B-E9B0-4a29-ACDC-A4661D5556CE', 10.45)

-- RoboClownEmployee

INSERT INTO Employee (Id, Name, Type)
	VALUES('53270DDA-4CE9-418f-A397-0D138FD12DE6', 'Robozo', 4)

INSERT INTO ClownEmployee (Id, ShoeLength)
	VALUES('53270DDA-4CE9-418f-A397-0D138FD12DE6', 15.92)

INSERT INTO RoboClownEmployee (Id, ChildrenVaporized)
	VALUES('53270DDA-4CE9-418f-A397-0D138FD12DE6', 94)

-- VampirePizzaDeliveryEmployee

INSERT INTO Employee (Id, Name, Type, DepartmentId)
	VALUES('F9C432BA-4D74-4d93-A475-EF7370D34997', 'LaCroix', 5, 1)

INSERT INTO VampirePizzaDeliveryEmployee (Id, FavoriteBloodyTopping)
	VALUES('F9C432BA-4D74-4d93-A475-EF7370D34997', 'Werewolferoni')

-- PizzaDeliveryEmployee

INSERT INTO Employee (Id, Name, Type, DepartmentId)
	VALUES('92D2EE26-6DF9-4dfa-99DA-FC6A14A0AFB1', 'Hiro', 3, 1)

-- Pizza

INSERT INTO Pizza (Id, CheeseFactor, PizzaDeliveryEmployeeId)
	VALUES('B6BD5292-658E-4246-A7C8-CBB218DEB736', 100, '92D2EE26-6DF9-4dfa-99DA-FC6A14A0AFB1')

INSERT INTO Pizza (Id, CheeseFactor, PizzaDeliveryEmployeeId)
	VALUES('7B1ADD4C-2E60-482a-BA7B-BDA2BF91632A', 9500, '92D2EE26-6DF9-4dfa-99DA-FC6A14A0AFB1')

INSERT INTO Pizza (Id, CheeseFactor, PizzaDeliveryEmployeeId)
	VALUES('4ED25070-DF39-4a91-A3A9-0102752E087C', 11000, '92D2EE26-6DF9-4dfa-99DA-FC6A14A0AFB1')

-- Employer

INSERT INTO Employer (Id, Name, OfficeNinjaId, AssistantId)
	VALUES('F78EF818-2DDB-4128-989D-2F3A0AA5E156', 'Ground0', '3A8F48E4-D31B-47af-9A64-9F14B420D7D6', 'E031BE0B-E9B0-4a29-ACDC-A4661D5556CE')

-- TatteredClothing

INSERT INTO TatteredClothing (Id, NumberOfHoles)
	VALUES('A5A07A43-3057-431C-AFBD-ADEBE1E2FDA8', 19)

-- Zombie

INSERT INTO Zombie (Id, Name, RemainingAppendages, BitById, TatteredClothingId, Infected, DateModified, VirusIncubationTime)
	VALUES('94EC8D8E-8073-404E-9918-9EA22B7566BD', 'Fred', 5, NULL, 'A5A07A43-3057-431C-AFBD-ADEBE1E2FDA8', '01/04/1987', '01/05/1987', '04:00:05')
	
INSERT INTO Zombie (Id, Name, RemainingAppendages, BitById, TatteredClothingId, Infected, DateModified, VirusIncubationTime)
	VALUES('CE447EF1-6AB3-48B4-BCBD-2C5DFF606593', 'Rat', 3, '94EC8D8E-8073-404E-9918-9EA22B7566BD', NULL, NULL, '01/01/1980', '04:00:05')

INSERT INTO Zombie (Id, Name, RemainingAppendages, DateModified, VirusIncubationTime)
	VALUES('2281B71D-0A95-4b9b-B4B1-612171B815FC', 'Joe', 8, '02/04/1990', '01:00:05')
	
INSERT INTO Zombie (Id, Name, RemainingAppendages, DateModified, VirusIncubationTime)
	VALUES('E7174014-0F25-40df-B875-2DA0C90C5F68', 'Bob', 4, '03/06/1997', '04:00:05')

INSERT INTO Zombie (Id, Name, RemainingAppendages, DateModified, VirusIncubationTime)
	VALUES('B4DA75F1-1C86-4897-8DCB-33198C0DC9B8', 'Arnie', 5, '06/04/2003', '04:00:06.01')

-- Bullet

INSERT INTO Bullet (Id, Kind, ZombieId)
	VALUES('F3A4043D-5618-488E-94B3-84DF209AA701', 1, '94EC8D8E-8073-404E-9918-9EA22B7566BD')
	
INSERT INTO Bullet (Id, Kind, ZombieId)
	VALUES('7C9C752F-48F6-436F-811A-6F768B9D8BA0', 2, '94EC8D8E-8073-404E-9918-9EA22B7566BD')

INSERT INTO Bullet (Id, Kind, ZombieId)
	VALUES('36CAFF06-E991-489F-864C-5CC23586810C', 3, '94EC8D8E-8073-404E-9918-9EA22B7566BD')

-- Zombie Bullets

UPDATE Zombie SET CarriedBulletId = 'F3A4043D-5618-488E-94B3-84DF209AA701' WHERE Id = '2281B71D-0A95-4b9b-B4B1-612171B815FC'
UPDATE Zombie SET CarriedBulletId = '7C9C752F-48F6-436F-811A-6F768B9D8BA0' WHERE Id = 'E7174014-0F25-40df-B875-2DA0C90C5F68'

-- ShieldArray


INSERT INTO ShieldArray (Id, Energy, LeechSuckingPower)
	VALUES('914C8E0E-D14C-427b-A07F-34DB2013BCFE', 33, 1)

INSERT INTO ShieldArray (Id, Energy, LeechSuckingPower)
	VALUES('6B2B71B0-91FF-4045-B5DB-F00903FDB463', 1231, 0)

-- Starship

INSERT INTO Starship (Id, Serial, ShieldId, ShipTime, ClockWarpTime,
		MissileWarheads, AftPhaserActive, AftPhaserPower, ForePhaserActive, ForePhaserPower,
		ShuttleSerial, ShuttleOmniPhaserActive, ShuttleOmniPhaserPower)
	VALUES('76A37A13-6BD2-4cc4-AE21-1E9234A58804', 'FOOCC-8909-X', '914C8E0E-D14C-427b-A07F-34DB2013BCFE', '3007-12-20 21:15:37.107', '3007-12-19 11:44:22',
		26, 0, 1111, 1, 878,
		'BARSS-7641-Z', 0, 432)
		
INSERT INTO Starship (Id, Serial, ShieldId, ShipTime, ClockWarpTime,
		MissileWarheads, AftPhaserActive, AftPhaserPower, ForePhaserActive, ForePhaserPower,
		ShuttleSerial, ShuttleOmniPhaserActive, ShuttleOmniPhaserPower)
	VALUES('2361C0F2-B51A-43c1-AFBE-268A1A3061E6', 'QUXCC-5343-V', '6B2B71B0-91FF-4045-B5DB-F00903FDB463', '3010-06-19 09:15:55.34', '3005-06-19 09:15:55.34',
		35, 1, 242, 1, 343,
		'MUGSX-7662-L', 0, 0)
		
-- Fixture

SET IDENTITY_INSERT Fixture ON
INSERT INTO Fixture (Id, Cost, Type) VALUES(1, 42.34, 2)
SET IDENTITY_INSERT Fixture OFF

-- OfficeFixture
INSERT INTO OfficeFixture (Id, Name) VALUES(1, 'GotNothing')

-- EmptyFixture

INSERT INTO EmptyFixture (Id) VALUES(1)

-- Cube

SET IDENTITY_INSERT [Cube] ON

INSERT [Cube] (CubeId, CubeType, X1, Y1, Z1, X2, Y2, Z2) VALUES
	(1, 2, 1, 1, 1, 2, 2, 2),
	(2, 1, 2, 2, 2, 3, 3, 3)

SET IDENTITY_INSERT [Cube] OFF

-- Gelatinous Cube

INSERT [GelatinousCube] (GelatinousCubeId, Permeability, PlayersAbsorbed) VALUES (1, 0.5, 3)

-- Handle

SET IDENTITY_INSERT Handle ON

INSERT Handle (Id, Length) VALUES (1, 4)
INSERT Handle (Id, Length) VALUES (2, 5)

SET IDENTITY_INSERT Handle OFF

-- Claw

SET IDENTITY_INSERT Claw ON

INSERT Claw (Id, IsSharp) VALUES (1, 0)
INSERT Claw (Id, IsSharp) VALUES (2, 1)

SET IDENTITY_INSERT Claw OFF

-- Saw

SET IDENTITY_INSERT Saw ON

INSERT Saw (Id, HandleId) VALUES (1, 1);
INSERT Saw (Id, HandleId) VALUES (2, 2);

SET IDENTITY_INSERT Saw OFF

-- Hammer

SET IDENTITY_INSERT Hammer ON

INSERT Hammer (Id, ClawId, HandleId, ProgrammersBeatenToDeath) VALUES (1, 2, 2, 5)
INSERT Hammer (Id, ClawId, HandleId, ProgrammersBeatenToDeath) VALUES (2, 1, 1, 0)

SET IDENTITY_INSERT Hammer OFF

-- Carpenter

SET IDENTITY_INSERT Carpenter ON

INSERT Carpenter (Id, RageLevel, HammerId, SawId) VALUES (1, 0, 2, 1)
INSERT Carpenter (Id, RageLevel, HammerId, SawId) VALUES (2, 100, 1, 2)

SET IDENTITY_INSERT Carpenter OFF

COMMIT TRAN
