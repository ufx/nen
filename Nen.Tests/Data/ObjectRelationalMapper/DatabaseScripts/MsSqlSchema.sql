-- Setup database
USE master
ALTER DATABASE Nen SET single_user WITH ROLLBACK IMMEDIATE
DROP DATABASE Nen
CREATE DATABASE Nen
ALTER DATABASE Nen SET multi_user WITH ROLLBACK IMMEDIATE
USE Nen

-- Create schema
CREATE TABLE Answer (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Date DATETIME2 NULL,
    Number INTEGER NULL,
    Question NVARCHAR(100) NOT NULL,
    Text NVARCHAR(100) NULL,
    Type INTEGER NOT NULL,
    CONSTRAINT PK_Answer PRIMARY KEY(Id)
); 
CREATE TABLE AnswerType (
    Id INTEGER NOT NULL,
    Name NVARCHAR(100) NOT NULL,
    ShortDescription NVARCHAR(300) NULL,
    CONSTRAINT PK_AnswerType PRIMARY KEY(Id)
); 
CREATE TABLE Bullet (
    Id UNIQUEIDENTIFIER NOT NULL,
    Kind INTEGER NOT NULL,
    ZombieId UNIQUEIDENTIFIER NULL,
    CONSTRAINT PK_Bullet PRIMARY KEY(Id)
); 
CREATE TABLE BulletKind (
    Id INTEGER NOT NULL,
    Name NVARCHAR(100) NOT NULL,
    ShortDescription NVARCHAR(300) NULL,
    CONSTRAINT PK_BulletKind PRIMARY KEY(Id)
); 
CREATE TABLE CandyPlanet (
    Id UNIQUEIDENTIFIER NOT NULL,
    Diameter INTEGER NOT NULL,
    Name NVARCHAR(50) NULL,
    NumberOfLollipops INTEGER NOT NULL,
    CONSTRAINT PK_CandyPlanet PRIMARY KEY(Id)
); 
CREATE TABLE Carpenter (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    HammerId INTEGER NULL,
    RageLevel INTEGER NOT NULL,
    SawId INTEGER NULL,
    CONSTRAINT PK_Carpenter PRIMARY KEY(Id)
); 
CREATE TABLE Caveman (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Name NVARCHAR(50) NULL,
	Discovered DATETIME2,
	PossibleAges NVARCHAR(1000) NULL,
    CONSTRAINT PK_Caveman PRIMARY KEY(Id)
); 
CREATE TABLE CavemanGene (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Name NVARCHAR(50) NOT NULL,
    CONSTRAINT PK_CavemanGene PRIMARY KEY(Id)
); 
CREATE TABLE CavemanGeneAttribute (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    CavemanGeneId INTEGER NOT NULL,
    Name NVARCHAR(50) NOT NULL,
    CONSTRAINT PK_CavemanGeneAttribute PRIMARY KEY(Id)
); 
CREATE TABLE CavemanGeneMap (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    CavemanGeneId INTEGER NOT NULL,
    CavemanId INTEGER NOT NULL,
    CONSTRAINT PK_CavemanGeneMap PRIMARY KEY(Id)
); 
CREATE TABLE Claw (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    IsSharp BIT NOT NULL,
    CONSTRAINT PK_Claw PRIMARY KEY(Id)
); 
CREATE TABLE ClownEmployee (
    Id UNIQUEIDENTIFIER NOT NULL,
    ShoeLength DECIMAL(18, 4) NOT NULL,
    WorthValue MONEY NULL,
    CONSTRAINT PK_ClownEmployee PRIMARY KEY(Id)
); 
CREATE TABLE Cube (
    CubeId INTEGER IDENTITY(1,1) NOT NULL,
    CubeType INTEGER NOT NULL,
    X1 INTEGER NOT NULL,
    X2 INTEGER NOT NULL,
    Y1 INTEGER NOT NULL,
    Y2 INTEGER NOT NULL,
    Z1 INTEGER NOT NULL,
    Z2 INTEGER NOT NULL,
    CONSTRAINT PK_Cube PRIMARY KEY(CubeId)
); 
CREATE TABLE CubeType (
    Id INTEGER NOT NULL,
    Name NVARCHAR(100) NOT NULL,
    ShortDescription NVARCHAR(300) NULL,
    CONSTRAINT PK_CubeType PRIMARY KEY(Id)
); 
CREATE TABLE Department (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Name NVARCHAR(50) NOT NULL,
    CONSTRAINT PK_Department PRIMARY KEY(Id)
); 
CREATE TABLE DinoEgg (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Hatched BIT NOT NULL,
    CONSTRAINT PK_DinoEgg PRIMARY KEY(Id)
); 
CREATE TABLE DinoTooth (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    PetDinosaurId INTEGER NULL,
    Sharpness INTEGER NOT NULL,
    CONSTRAINT PK_DinoTooth PRIMARY KEY(Id)
); 
CREATE TABLE Door (
    Name NVARCHAR(100) NULL,
    [Open] BIT NOT NULL
); 
CREATE TABLE Employee (
    Id UNIQUEIDENTIFIER NOT NULL,
    DepartmentId INTEGER NULL,
    Name NVARCHAR(50) NULL,
    Type INTEGER NOT NULL,
    CONSTRAINT PK_Employee PRIMARY KEY(Id)
); 
CREATE TABLE EmployeeType (
    Id INTEGER NOT NULL,
    Name NVARCHAR(100) NOT NULL,
    ShortDescription NVARCHAR(300) NULL,
    CONSTRAINT PK_EmployeeType PRIMARY KEY(Id)
); 
CREATE TABLE Employer (
    Id UNIQUEIDENTIFIER NOT NULL,
    AssistantId UNIQUEIDENTIFIER NOT NULL,
    Name NVARCHAR(50) NULL,
    OfficeNinjaId UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT PK_Employer PRIMARY KEY(Id)
); 
CREATE TABLE EmptyFixture (
    Id INTEGER NOT NULL,
    CONSTRAINT PK_EmptyFixture PRIMARY KEY(Id)
); 
CREATE TABLE EvilTower (
    Id UNIQUEIDENTIFIER NOT NULL,
    CodeName NVARCHAR(100) NULL,
    EvilStuff VARBINARY(512) NULL,
    MindControlAntennaPower INTEGER NOT NULL,
    NumberOfZombiesCreated INTEGER NOT NULL,
    CONSTRAINT PK_EvilTower PRIMARY KEY(Id)
); 
CREATE TABLE FireplaceFixture (
    Id INTEGER NOT NULL,
    MagicalArtifactsBurned INTEGER NOT NULL,
    CONSTRAINT PK_FireplaceFixture PRIMARY KEY(Id)
); 
CREATE TABLE Fixture (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Cost DECIMAL(18, 4) NOT NULL,
    Type INTEGER NOT NULL,
    CONSTRAINT PK_Fixture PRIMARY KEY(Id)
); 
CREATE TABLE FixtureType (
    Id INTEGER NOT NULL,
    Name NVARCHAR(100) NOT NULL,
    ShortDescription NVARCHAR(300) NULL,
    CONSTRAINT PK_FixtureType PRIMARY KEY(Id)
); 
CREATE TABLE GelatinousCube (
    GelatinousCubeId INTEGER NOT NULL,
    Permeability FLOAT(24) NOT NULL,
    PlayersAbsorbed INTEGER NOT NULL,
    CONSTRAINT PK_GelatinousCube PRIMARY KEY(GelatinousCubeId)
); 
CREATE TABLE Hammer (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    ClawId INTEGER NULL,
    HandleId INTEGER NULL,
    ProgrammersBeatenToDeath INTEGER NOT NULL,
    CONSTRAINT PK_Hammer PRIMARY KEY(Id)
); 
CREATE TABLE Handle (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Length INTEGER NOT NULL,
    CONSTRAINT PK_Handle PRIMARY KEY(Id)
); 
CREATE TABLE NinjaEmployee (
    Id UNIQUEIDENTIFIER NOT NULL,
    PiratesSlain INTEGER NOT NULL,
    CONSTRAINT PK_NinjaEmployee PRIMARY KEY(Id)
); 
CREATE TABLE NoMoreFunPlanet (
    Id UNIQUEIDENTIFIER NOT NULL,
    Diameter INTEGER NOT NULL,
    FunEndDate DATETIME2 NOT NULL,
    FunStartDate DATETIME2 NOT NULL,
    Name NVARCHAR(50) NULL,
    CONSTRAINT PK_NoMoreFunPlanet PRIMARY KEY(Id)
); 
CREATE TABLE OfficeFixture (
    Id INTEGER NOT NULL,
    Name NVARCHAR(200) NULL,
    CONSTRAINT PK_OfficeFixture PRIMARY KEY(Id)
); 
CREATE TABLE PetDinosaur (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    EggId INTEGER NULL,
    Name NVARCHAR(50) NULL,
    OwnerCavemanId INTEGER NULL,
    CONSTRAINT PK_PetDinosaur PRIMARY KEY(Id)
); 
CREATE TABLE Pizza (
    Id UNIQUEIDENTIFIER NOT NULL,
    CheeseFactor INTEGER NOT NULL,
    PizzaDeliveryEmployeeId UNIQUEIDENTIFIER NULL,
    CONSTRAINT PK_Pizza PRIMARY KEY(Id)
); 
CREATE TABLE RoboClownEmployee (
    Id UNIQUEIDENTIFIER NOT NULL,
    ChildrenVaporized INTEGER NOT NULL,
    CONSTRAINT PK_RoboClownEmployee PRIMARY KEY(Id)
); 
CREATE TABLE Saw (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    HandleId INTEGER NULL,
    CONSTRAINT PK_Saw PRIMARY KEY(Id)
); 
CREATE TABLE Science (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Name NVARCHAR(1000) NULL,
    Results VARBINARY(MAX) NULL,
    CONSTRAINT PK_Science PRIMARY KEY(Id)
); 
CREATE TABLE Scroll (
    Id UNIQUEIDENTIFIER NOT NULL,
    Text NVARCHAR(1000) NULL,
    CONSTRAINT PK_Scroll PRIMARY KEY(Id)
); 
CREATE TABLE ScrollCase (
    Id UNIQUEIDENTIFIER NOT NULL,
    EngravedText NVARCHAR(500) NULL,
    ScrollId UNIQUEIDENTIFIER NULL,
    CONSTRAINT PK_ScrollCase PRIMARY KEY(Id)
); 
CREATE TABLE ScrollParchment (
    Id UNIQUEIDENTIFIER NOT NULL,
    Quality INTEGER NOT NULL,
    ScrollId UNIQUEIDENTIFIER NULL,
    CONSTRAINT PK_ScrollParchment PRIMARY KEY(Id)
);
CREATE TABLE ScrollParchmentProperty (
	Id UNIQUEIDENTIFIER NOT NULL,
	ScrollParchmentId UNIQUEIDENTIFIER,
	Name NVARCHAR(500) NOT NULL,
	CONSTRAINT PK_ScrollParchmentProperty PRIMARY KEY(Id)
);
CREATE TABLE ShieldArray (
    Id UNIQUEIDENTIFIER NOT NULL,
    Energy DECIMAL(18, 4) NOT NULL,
    LeechSuckingPower BIT NOT NULL,
    CONSTRAINT PK_ShieldArray PRIMARY KEY(Id)
); 
CREATE TABLE Spell (
    Id UNIQUEIDENTIFIER NOT NULL,
    Description NVARCHAR(100) NULL,
    WizardId UNIQUEIDENTIFIER NULL,
    CONSTRAINT PK_Spell PRIMARY KEY(Id)
); 
CREATE TABLE SpellEffect (
    Id UNIQUEIDENTIFIER NOT NULL,
    Damage INTEGER NOT NULL,
    SpellId UNIQUEIDENTIFIER NULL,
    CONSTRAINT PK_SpellEffect PRIMARY KEY(Id)
); 
CREATE TABLE Starship (
    Id UNIQUEIDENTIFIER NOT NULL,
    AftPhaserActive BIT NOT NULL,
    AftPhaserPower INTEGER NOT NULL,
    AlternatePhaserActive BIT NULL,
    AlternatePhaserPower INTEGER NULL,
    ClockWarpTime DATETIME2 DEFAULT GETDATE() NOT NULL,
    ForePhaserActive BIT NOT NULL,
    ForePhaserPower INTEGER NOT NULL,
    MissileWarheads INTEGER NOT NULL,
    Serial NVARCHAR(50) NULL,
    ShieldId UNIQUEIDENTIFIER NULL,
    ShipTime DATETIME2 NOT NULL,
    ShuttleOmniPhaserActive BIT NOT NULL,
    ShuttleOmniPhaserPower INTEGER NOT NULL,
    ShuttleSerial NVARCHAR(50) NULL,
    CONSTRAINT PK_Starship PRIMARY KEY(Id)
); 
CREATE TABLE TatteredClothing (
    Id UNIQUEIDENTIFIER NOT NULL,
    NumberOfHoles INTEGER NOT NULL,
    CONSTRAINT PK_TatteredClothing PRIMARY KEY(Id)
); 
CREATE TABLE Treehouse (
    Id INTEGER IDENTITY(1,1) NOT NULL,
    Height INTEGER NULL,
    Width INTEGER NULL,
    CONSTRAINT PK_Treehouse PRIMARY KEY(Id)
); 
CREATE TABLE VampirePizzaDeliveryEmployee (
    Id UNIQUEIDENTIFIER NOT NULL,
    FavoriteBloodyTopping NVARCHAR(50) NULL,
    CONSTRAINT PK_VampirePizzaDeliveryEmployee PRIMARY KEY(Id)
); 
CREATE TABLE Wizard (
    Id UNIQUEIDENTIFIER NOT NULL,
    Name NVARCHAR(100) NULL,
    CONSTRAINT PK_Wizard PRIMARY KEY(Id)
); 
CREATE TABLE Zombie (
    Id UNIQUEIDENTIFIER NOT NULL,
    BitById UNIQUEIDENTIFIER NULL,
    CarriedBulletId UNIQUEIDENTIFIER NULL,
    DateModified DATETIME2 DEFAULT GETDATE() NOT NULL,
    Infected DATETIME2 NULL,
    Name NVARCHAR(50) NULL,
    RemainingAppendages INTEGER NOT NULL,
    TatteredClothingId UNIQUEIDENTIFIER NULL,
    VirusIncubationTime TIME(7) NOT NULL,
    CONSTRAINT PK_Zombie PRIMARY KEY(Id)
); 
ALTER TABLE Answer ADD 
    CONSTRAINT FK_Answer_AnswerType_Type FOREIGN KEY(Type) REFERENCES AnswerType (Id); 
ALTER TABLE Bullet ADD 
    CONSTRAINT FK_Bullet_Zombie_ZombieId FOREIGN KEY(ZombieId) REFERENCES Zombie (Id), 
    CONSTRAINT FK_Bullet_BulletKind_Kind FOREIGN KEY(Kind) REFERENCES BulletKind (Id); 
ALTER TABLE Carpenter ADD 
    CONSTRAINT FK_Carpenter_Hammer_HammerId FOREIGN KEY(HammerId) REFERENCES Hammer (Id), 
    CONSTRAINT FK_Carpenter_Saw_SawId FOREIGN KEY(SawId) REFERENCES Saw (Id); 
ALTER TABLE CavemanGeneAttribute ADD 
    CONSTRAINT FK_CavemanGeneAttribute_CavemanGene_CavemanGeneId FOREIGN KEY(CavemanGeneId) REFERENCES CavemanGene (Id); 
ALTER TABLE CavemanGeneMap ADD 
    CONSTRAINT FK_CavemanGeneMap_Caveman_CavemanId FOREIGN KEY(CavemanId) REFERENCES Caveman (Id), 
    CONSTRAINT FK_CavemanGeneMap_CavemanGene_CavemanGeneId FOREIGN KEY(CavemanGeneId) REFERENCES CavemanGene (Id); 
ALTER TABLE ClownEmployee ADD 
    CONSTRAINT FK_ClownEmployee_Employee_PK FOREIGN KEY(Id) REFERENCES Employee (Id); 
ALTER TABLE Cube ADD 
    CONSTRAINT FK_Cube_CubeType_CubeType FOREIGN KEY(CubeType) REFERENCES CubeType (Id); 
ALTER TABLE DinoTooth ADD 
    CONSTRAINT FK_DinoTooth_PetDinosaur_PetDinosaurId FOREIGN KEY(PetDinosaurId) REFERENCES PetDinosaur (Id); 
ALTER TABLE Employee ADD 
    CONSTRAINT FK_Employee_Department_DepartmentId FOREIGN KEY(DepartmentId) REFERENCES Department (Id), 
    CONSTRAINT FK_Employee_EmployeeType_Type FOREIGN KEY(Type) REFERENCES EmployeeType (Id); 
ALTER TABLE Employer ADD 
    CONSTRAINT FK_Employer_NinjaEmployee_OfficeNinjaId FOREIGN KEY(OfficeNinjaId) REFERENCES NinjaEmployee (Id), 
    CONSTRAINT FK_Employer_Employee_AssistantId FOREIGN KEY(AssistantId) REFERENCES Employee (Id); 
ALTER TABLE EmptyFixture ADD 
    CONSTRAINT FK_EmptyFixture_OfficeFixture_PK FOREIGN KEY(Id) REFERENCES OfficeFixture (Id); 
ALTER TABLE FireplaceFixture ADD 
    CONSTRAINT FK_FireplaceFixture_Fixture_PK FOREIGN KEY(Id) REFERENCES Fixture (Id); 
ALTER TABLE Fixture ADD 
    CONSTRAINT FK_Fixture_FixtureType_Type FOREIGN KEY(Type) REFERENCES FixtureType (Id); 
ALTER TABLE GelatinousCube ADD 
    CONSTRAINT FK_GelatinousCube_Cube_PK FOREIGN KEY(GelatinousCubeId) REFERENCES Cube (CubeId); 
ALTER TABLE Hammer ADD 
    CONSTRAINT FK_Hammer_Claw_ClawId FOREIGN KEY(ClawId) REFERENCES Claw (Id), 
    CONSTRAINT FK_Hammer_Handle_HandleId FOREIGN KEY(HandleId) REFERENCES Handle (Id); 
ALTER TABLE NinjaEmployee ADD 
    CONSTRAINT FK_NinjaEmployee_Employee_PK FOREIGN KEY(Id) REFERENCES Employee (Id); 
ALTER TABLE OfficeFixture ADD 
    CONSTRAINT FK_OfficeFixture_Fixture_PK FOREIGN KEY(Id) REFERENCES Fixture (Id); 
ALTER TABLE PetDinosaur ADD 
    CONSTRAINT FK_PetDinosaur_Caveman_OwnerCavemanId FOREIGN KEY(OwnerCavemanId) REFERENCES Caveman (Id), 
    CONSTRAINT FK_PetDinosaur_DinoEgg_EggId FOREIGN KEY(EggId) REFERENCES DinoEgg (Id); 
ALTER TABLE Pizza ADD 
    CONSTRAINT FK_Pizza_Employee_PizzaDeliveryEmployeeId FOREIGN KEY(PizzaDeliveryEmployeeId) REFERENCES Employee (Id); 
ALTER TABLE RoboClownEmployee ADD 
    CONSTRAINT FK_RoboClownEmployee_ClownEmployee_PK FOREIGN KEY(Id) REFERENCES ClownEmployee (Id); 
ALTER TABLE Saw ADD 
    CONSTRAINT FK_Saw_Handle_HandleId FOREIGN KEY(HandleId) REFERENCES Handle (Id); 
ALTER TABLE ScrollCase ADD 
    CONSTRAINT FK_ScrollCase_Scroll_ScrollId FOREIGN KEY(ScrollId) REFERENCES Scroll (Id); 
ALTER TABLE ScrollParchment ADD 
    CONSTRAINT FK_ScrollParchment_Scroll_ScrollId FOREIGN KEY(ScrollId) REFERENCES Scroll (Id); 
ALTER TABLE ScrollParchmentProperty ADD
	CONSTRAINT FK_ScrollParchmentProperty_ScrollParchment_ScrollParchmentId FOREIGN KEY(ScrollParchmentId) REFERENCES ScrollParchment (Id);
ALTER TABLE Spell ADD 
    CONSTRAINT FK_Spell_Wizard_WizardId FOREIGN KEY(WizardId) REFERENCES Wizard (Id); 
ALTER TABLE SpellEffect ADD 
    CONSTRAINT FK_SpellEffect_Spell_SpellId FOREIGN KEY(SpellId) REFERENCES Spell (Id); 
ALTER TABLE Starship ADD 
    CONSTRAINT FK_Starship_ShieldArray_ShieldId FOREIGN KEY(ShieldId) REFERENCES ShieldArray (Id); 
ALTER TABLE VampirePizzaDeliveryEmployee ADD 
    CONSTRAINT FK_VampirePizzaDeliveryEmployee_Employee_PK FOREIGN KEY(Id) REFERENCES Employee (Id); 
ALTER TABLE Zombie ADD 
    CONSTRAINT FK_Zombie_Zombie_BitById FOREIGN KEY(BitById) REFERENCES Zombie (Id), 
    CONSTRAINT FK_Zombie_TatteredClothing_TatteredClothingId FOREIGN KEY(TatteredClothingId) REFERENCES TatteredClothing (Id), 
    CONSTRAINT FK_Zombie_Bullet_CarriedBulletId FOREIGN KEY(CarriedBulletId) REFERENCES Bullet (Id); 
INSERT INTO AnswerType (Id, Name, ShortDescription) VALUES('0', 'NumberAnswer', NULL); 
INSERT INTO AnswerType (Id, Name, ShortDescription) VALUES('1', 'TextAnswer', NULL); 
INSERT INTO AnswerType (Id, Name, ShortDescription) VALUES('2', 'DateAnswer', NULL); 
INSERT INTO BulletKind (Id, Name, ShortDescription) VALUES('1', 'Normal', NULL); 
INSERT INTO BulletKind (Id, Name, ShortDescription) VALUES('2', 'Hollow', NULL); 
INSERT INTO BulletKind (Id, Name, ShortDescription) VALUES('3', 'Silver', NULL); 
INSERT INTO CubeType (Id, Name, ShortDescription) VALUES('1', 'RegularCube', NULL); 
INSERT INTO CubeType (Id, Name, ShortDescription) VALUES('2', 'GelatinousCube', NULL); 
INSERT INTO EmployeeType (Id, Name, ShortDescription) VALUES('1', 'Ninja', NULL); 
INSERT INTO EmployeeType (Id, Name, ShortDescription) VALUES('2', 'Clown', NULL); 
INSERT INTO EmployeeType (Id, Name, ShortDescription) VALUES('3', 'PizzaDelivery', NULL); 
INSERT INTO EmployeeType (Id, Name, ShortDescription) VALUES('4', 'RoboClown', NULL); 
INSERT INTO EmployeeType (Id, Name, ShortDescription) VALUES('5', 'VampirePizzaDelivery', NULL); 
INSERT INTO EmployeeType (Id, Name, ShortDescription) VALUES('6', 'Base', NULL); 
INSERT INTO FixtureType (Id, Name, ShortDescription) VALUES('1', 'Fireplace', NULL); 
INSERT INTO FixtureType (Id, Name, ShortDescription) VALUES('2', 'Empty', NULL);
    
ALTER TABLE dbo.Zombie ADD CONSTRAINT CK_Zombie_BadNames CHECK (Name != 'UnwrittenBadThing')
GO

-- Procedure Creates

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ReadDoorByName]
	@Name nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON

	SELECT * FROM Door WHERE Name = @Name
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ReadStarshipBySerial]
	@Serial NVARCHAR(500)
AS
BEGIN
	SET NOCOUNT ON

	SELECT * FROM Starship WHERE Serial = @Serial
END
GO
