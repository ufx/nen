﻿begin

delete from NEN.PET_DINOSAUR where NAME in ('WrittenKorgo', 'WrittenLork');
delete from dino_egg where hatched = 'T';
delete from NEN.CAVEMAN where NAME IN ('WrittenGwar', 'WrittenSmlarg', 'WrittenSmlargy', 'WrittenMug', 'WrittenShlug');
delete from NEN.ZOMBIE WHERE Name IN ('WrittenRomero', 'WrittenHeather', 'UnwrittenBadThing', 'WrittenNewGuy', 'WrittenDeleter');
delete from NEN.TATTERED_CLOTHING WHERE NUMBER_OF_HOLES IN (62, 66, 67, 68);
update NEN.ZOMBIE SET REMAINING_APPENDAGES = 3, DATE_MODIFIED = timestamp'1980-01-01 00:00:00' WHERE Id = 'ce447ef16ab348b4bcbd2c5dff606593';
delete from NEN.SPELL_EFFECT WHERE Damage = 909090;
delete from NEN.Spell WHERE Description = 'test tracking collection';
delete from NEN.Treehouse;
delete from NEN.EVIL_TOWER WHERE CODE_NAME = 'WrittenTower';
delete from NEN.Science WHERE Name IN ('Zombrex', 'BlackHelicopters', 'Quacktor');
delete from NEN.FIREPLACE_FIXTURE;
delete from NEN.EMPTY_FIXTURE WHERE Id IN (SELECT Id FROM NEN.OFFICE_FIXTURE WHERE Name IN ('WrittenReferenceBase', 'WrittenTrackedReferenceBase'));
delete from NEN.OFFICE_FIXTURE WHERE Name IN ('WrittenReferenceBase', 'WrittenTrackedReferenceBase');
delete from NEN.Fixture WHERE Type = 1 OR Cost = 1231 OR Cost = 3434;
update NEN.OFFICE_FIXTURE SET Name = 'GotNothing' WHERE Id = 1;
delete from NEN.Scroll WHERE Text = 'TestTrackedSubordinateWrite';
delete from NEN.SCROLL_PARCHMENT WHERE Quality = 8888;
delete from NEN.SHIELD_ARRAY WHERE Energy IN (78);
delete from NEN.Starship WHERE Serial IN ('WRITTEN-FOOBAR123', 'WRITTEN-POPULAR');

end;