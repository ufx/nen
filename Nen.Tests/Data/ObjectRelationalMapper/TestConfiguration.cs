using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward;
using Nen.Reflection;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.Oracle;
using System.Data;
using Nen.Data.Postgres;

namespace Nen.Tests.Data.ObjectRelationalMapper {
    public class TestConfiguration : MapConfiguration {
        public override DataContext CreateContext () {
            return new TestDataContext(this, ContextOptions.None);
        }

        public TestConfiguration() : this(GetCachedConventions()) { }

        public TestConfiguration(Conventions conventions)
            : base(conventions)
        {
            DefaultStore = new DatabaseStore(this, TestEnvironment.Database);
        }

        public static TestConfiguration CreateConfiguration () {
            var config = new TestConfiguration(GetCachedConventions());
            config.Initialize();
            return config;
        }

        private static TestConfiguration _configuration;
        public static TestConfiguration GetCachedConfiguration () {
            if (_configuration == null)
                _configuration = CreateConfiguration();
            return _configuration;
        }

        private static Conventions _conventions;
        public static Conventions GetCachedConventions()
        {
            if (_conventions == null)
                _conventions = (Conventions)Activator.CreateInstance(Config.ConventionsType);
            return _conventions;
        }
    }

    public class TestConventions : Conventions {
        internal static string GetPrimaryKeyColumnNameCore(VariableInfo primaryKeyVariable, TableMap tableMap)
        {
            var type = tableMap.OriginTypeMap.Type;
            if (type == typeof(Nen.Tests.Data.ObjectRelationalMapper.Models.RenamedPrimaryKeys.Cube) ||
                type == typeof(Nen.Tests.Data.ObjectRelationalMapper.Models.RenamedPrimaryKeys.GelatinousCube))
                return tableMap.OriginTypeMap.Type.Name + "Id";

            return null;
        }

        public override string GetPrimaryKeyColumnName(VariableInfo primaryKeyVariable, TableMap tableMap)
        {
            var name = TestConventions.GetPrimaryKeyColumnNameCore(primaryKeyVariable, tableMap);
            return name == null ? base.GetPrimaryKeyColumnName(primaryKeyVariable, tableMap) : name;
        }
    }

    public class OracleTestConventions : OracleConventions
    {
        public override string GetSchemaName(Type type)
        {
            return "NEN";
        }

        public override string GetPrimaryKeyColumnName(VariableInfo primaryKeyVariable, TableMap tableMap)
        {
            var name = TestConventions.GetPrimaryKeyColumnNameCore(primaryKeyVariable, tableMap);
            return name == null ? base.GetPrimaryKeyColumnName(primaryKeyVariable, tableMap) : GetDataIdentifierName(name, DataIdentifierType.Column);
        }
    }

    public class PostgresTestConventions : PostgresConventions
    {
        public override string GetPrimaryKeyColumnName(VariableInfo primaryKeyVariable, TableMap tableMap)
        {
            var name = TestConventions.GetPrimaryKeyColumnNameCore(primaryKeyVariable, tableMap);
            return name == null ? base.GetPrimaryKeyColumnName(primaryKeyVariable, tableMap) : name;
        }
    }

    public static class DataRowTestEx
    {
        public static object GetValue(this DataRow row, string columnName)
        {
            var conventions = TestConfiguration.GetCachedConventions();
            var formattedName = conventions.GetDataIdentifierName(columnName, DataIdentifierType.Column);
            return row[formattedName];
        }

        public static Guid GetGuid(this DataRow row, string columnName)
        {
            var value = row.GetValue(columnName);
            return value is string ? Guid.Parse((string)value) : (Guid)value;
        }

        public static int GetInt(this DataRow row, string columnName)
        {
            var value = row.GetValue(columnName);
            return Convert.ToInt32(value);
        }

        public static bool GetBool(this DataRow row, string columnName)
        {
            var value = row.GetValue(columnName);
            return value is string ? Equals(value, "Y") : (bool)value;
        }
    }
}
