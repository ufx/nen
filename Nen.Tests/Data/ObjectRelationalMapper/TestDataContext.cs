﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Diagnostics;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Components;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Discriminated;
using Nen.Tests.Data.ObjectRelationalMapper.Models.DiscriminatedSequentialKeys;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Flattened;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Immutable;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Keyless;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward;
using Nen.Tests.Data.ObjectRelationalMapper.Models.StraightforwardSequentialKeys;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Subordinates;
using Nen.Data.ObjectRelationalMapper.QueryModel;

namespace Nen.Tests.Data.ObjectRelationalMapper {
    public class TestDataContext : DataContext {
        #region Constructors
        public TestDataContext ()
            : this(ContextOptions.None) {
        }

        public TestDataContext (ContextOptions options)
            : this(TestConfiguration.GetCachedConfiguration(), options) {
        }

        public TestDataContext (TestConfiguration config, ContextOptions options)
            : base(config, options) {
            Log = new DebugWriter();
        }
        #endregion

        #region Query Statistics
        protected override void OnQueryExecuted (QueryExpression query, DataQueryResults results, object values) {
            base.OnQueryExecuted(query, results, values);

            if (Log != null)
                Log.WriteLine(string.Format("Total queries: {0}, time: {1}", query.Statistics.NumberOfDatabaseQueries, query.Statistics.TotalTime));
        }
        #endregion

        #region Tables
        public DataQuery<Bullet> Bullet {
            get { return Get<Bullet>(); }
        }

        public DataQuery<CandyPlanet> CandyPlanet {
            get { return Get<CandyPlanet>(); }
        }

        public DataQuery<Door> Door {
            get { return Get<Door>(); }
        }

        public DataQuery<Employee> Employee {
            get { return Get<Employee>(); }
        }

        public DataQuery<Employer> Employer {
            get { return Get<Employer>(); }
        }

        public DataQuery<EmptyFixture> EmptyFixture {
            get { return Get<EmptyFixture>(); }
        }

        public DataQuery<EvilTower> EvilTower {
            get { return Get<EvilTower>(); }
        }

        public DataQuery<Fixture> Fixture {
            get { return Get<Fixture>(); }
        }

        public DataQuery<Wizard> Wizard {
            get { return Get<Wizard>(); }
        }

        public DataQuery<NinjaEmployee> NinjaEmployee {
            get { return Get<NinjaEmployee>(); }
        }

        public DataQuery<NoMoreFunPlanet> NoMoreFunPlanet {
            get { return Get<NoMoreFunPlanet>(); }
        }

        public DataQuery<OfficeFixture> OfficeFixture {
            get { return Get<OfficeFixture>(); }
        }

        public DataQuery<Scroll> Scroll {
            get { return Get<Scroll>(); }
        }

        public DataQuery<ScrollCase> ScrollCase {
            get { return Get<ScrollCase>(); }
        }

        public DataQuery<SpellEffect> SpellEffect {
            get { return Get<SpellEffect>(); }
        }

        public DataQuery<Starship> Starship {
            get { return Get<Starship>(); }
        }

        public DataQuery<Zombie> Zombie {
            get { return Get<Zombie>(); }
        }
        #endregion

        #region Stored Procedures
        public DataQuery<Door> ReadDoorByName (string name) {
            return Get<Door>("ReadDoorByName", name);
        }

        public DataQuery<Starship> ReadStarshipBySerial (string serial) {
            return Get<Starship>("ReadStarshipBySerial", serial);
        }
        #endregion
    }
}
