﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Instrumented;
using System.Reflection;
using Nen.Reflection;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper {
	[TestClass]
	public class InstrumentationTests {
		[TestMethod, Ignore]
		public void CreateNewInstrumented () {
			var carpenter = new Carpenter();
			var carpenter2 = new Carpenter(5);

			carpenter.Saw = new Saw();
			carpenter2.Hammer = new Hammer();

			Assert.AreEqual(5, carpenter2.RageLevel);
			Assert.IsNotNull(carpenter.Saw);
			Assert.IsNotNull(carpenter2.Hammer);

			Assert.IsNotNull(typeof(Carpenter).GetMethod("__init_DataRef", BindingFlags.NonPublic | BindingFlags.Instance));
		}

		[TestMethod, Ignore]
		public void LazyRead () {
			using (var ctx = new TestDataContext()) {
				var crp = ctx.Load<Carpenter>(1);

				var underlyingHammer = typeof(Carpenter).GetField("<Hammer>__dataRef", TypeEx.InstanceBindingFlags);

				Assert.IsNotNull(underlyingHammer);

				var dataRef = (DataRef<Hammer>)underlyingHammer.GetValue(crp);

				Assert.IsFalse(dataRef.IsResolved);
				Assert.AreEqual(2, dataRef.Key);

				dataRef.Resolve();

				Assert.IsTrue(dataRef.IsResolved);
				Assert.AreEqual(0, dataRef.Value.ProgrammersBeatenToDeath);
			}
		}
	}
}
