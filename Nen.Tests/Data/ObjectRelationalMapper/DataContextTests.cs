﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward;

namespace Nen.Tests.Data.ObjectRelationalMapper {
    [TestClass]
    public class DataContextTests {
        [TestMethod]
        public void LoadFromIdentityMap () {
            Zombie fred, fred2;
            using (var context = new TestDataContext()) {
                fred = context.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));
                fred2 = context.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));
                Assert.AreSame(fred, fred2);
            }

            using (var context2 = new TestDataContext()) {
                fred2 = context2.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));
                Assert.AreNotSame(fred, fred2);
            }
        }

        [TestMethod]
        public void LoadFromParentIdentityMap () {
            var outerContext = new TestDataContext();
            using (outerContext) {
                Assert.AreSame(outerContext, DataContext.Current);

                Zombie outerFred = outerContext.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));

                using (var innerContext = new TestDataContext()) {
                    Assert.AreSame(innerContext, DataContext.Current);

                    Zombie innerFred = innerContext.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));

                    Assert.AreSame(outerFred, innerFred);
                }

                Assert.IsNotNull(DataContext.Current);

                Zombie outerFred2 = outerContext.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD"));
                Assert.AreSame(outerFred, outerFred2);
            }

            Assert.IsNull(DataContext.Current);

            AssertEx.ExceptionThrown<ObjectDisposedException>(() => outerContext.Zombie.Load(new Guid("94EC8D8E-8073-404E-9918-9EA22B7566BD")));
        }

        [TestMethod]
        public void InheritTrackChanges () {
            using (var outerOuterContext = new TestDataContext(ContextOptions.None)) {
                Assert.AreEqual(ContextOptions.None, outerOuterContext.Options);

                using (var outerContext = new TestDataContext(ContextOptions.TrackChanges)) {
                    Assert.AreEqual(ContextOptions.TrackChanges, outerContext.Options);

                    using (var innerContext = new TestDataContext(ContextOptions.None))
                        Assert.AreEqual(ContextOptions.TrackChanges, innerContext.Options);
                }
            }
        }

#if NET4
        // Not a priority
        //[TestMethod]
        //public void SerializeQuery () {
        //    string queryData;

        //    using (var context = new TestDataContext()) {
        //        var query = context.Zombie.Where(z => z.Name == "Fred").OrderBy(z => z.Id);
        //        queryData = context.Serialize(query);

        //        Assert.AreEqual("<QueryExpression z:id=\"0\" Shape=\"Sequence\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:z=\"http://www.w3.org/XML/Schema\"><Advice/><IsPossiblyAmbiguous>true</IsPossiblyAmbiguous><OrderBy><OrderExpression z:id=\"1\" Direction=\"Ascending\"><Over xsi:type=\"VariableExpression\" z:id=\"2\"><Container z:id=\"0\"/><Variable z:id=\"3\" Variable=\"Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward.Zombie,Nen.Tests::Id::System.Guid,mscorlib\"/></Over></OrderExpression></OrderBy><Projection z:id=\"4\"><Items/><Parent z:id=\"0\"/></Projection><Subject xsi:type=\"TableExpression\" z:id=\"5\" Name=\"Zombie\"/><WhereCriteria xsi:type=\"BinaryOperatorExpression\" z:id=\"6\" Operator=\"Equal\"><Left xsi:type=\"VariableExpression\" z:id=\"7\"><Container z:id=\"0\"/><Variable z:id=\"8\" Variable=\"Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward.Zombie,Nen.Tests::Name::System.String,mscorlib\"/></Left><Right xsi:type=\"LiteralExpression\" z:id=\"9\"><Value xsi:type=\"System.String\">Fred</Value></Right></WhereCriteria></QueryExpression>", queryData);
        //    }

        //    using (var context = new TestDataContext()) {
        //        var query = context.Deserialize(queryData);

        //        var zombies = query.ExecuteSequence().ToArray();
        //        Assert.AreEqual(1, zombies.Length);
        //        Nen.Tests.Data.ObjectRelationalMapper.Reads.StraightforwardMappingTests.CheckFred((Zombie) zombies[0], true);
        //    }
        //}
#endif
    }
}
