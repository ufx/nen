﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Tests.Data.ObjectRelationalMapper.Models.Immutable;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class ImmutableModelWriteTests {
        private string _evilTowerTable;
        private string _codeNameColumn;

        [TestInitialize]
        public void Initialize()
        {
            var conventions = TestConfiguration.GetCachedConventions();
            _evilTowerTable = conventions.GetDataIdentifierName("EvilTower", DataIdentifierType.Table);
            _codeNameColumn = conventions.GetDataIdentifierName("CodeName", DataIdentifierType.Column);
        }

        [TestCleanup]
        public void Cleanup () {
            TestEnvironment.RunDatabaseCleanup();
        }

        [TestMethod]
        public void Insert () {
            var tower = new EvilTower(Guid.NewGuid(), 343, "WrittenTower");
            tower.NumberOfZombiesCreated = 111;
            tower.EvilStuff = GetRandomBytes();

            using (var context = new TestDataContext())
                context.Save(tower);

            var row = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM " + _evilTowerTable + " WHERE " + _codeNameColumn + " = 'WrittenTower'");
            Assert.IsNotNull(row);
            Assert.AreEqual(tower.Id, row.GetGuid("Id"));
            Assert.AreEqual(111, row.GetInt("NumberOfZombiesCreated"));
            Assert.AreEqual(343, row.GetInt("MindControlAntennaPower"));

            Assert.AreNotEqual(DBNull.Value, row.GetValue("EvilStuff"));
            Assert.IsTrue(tower.EvilStuff.SequenceEqual((byte[]) row.GetValue("EvilStuff")));
        }

        [TestMethod]
        public void InsertNullBytes () {
            var tower = new EvilTower(Guid.NewGuid(), 343, "WrittenTower");

            using (var context = new TestDataContext())
                context.Save(tower);

            var row = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM " + _evilTowerTable + " WHERE " + _codeNameColumn + " = 'WrittenTower'");
            Assert.IsNotNull(row);
            Assert.AreEqual(tower.Id, row.GetGuid("Id"));
            Assert.AreEqual(343, row.GetInt("MindControlAntennaPower"));
            Assert.AreSame(DBNull.Value, row.GetValue("EvilStuff"));
        }

        private byte[] GetRandomBytes () {
            var random = new Random();
            var bytes = new byte[256];
            random.NextBytes(bytes);
            return bytes;
        }
    }
}
