﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Subordinates;

namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class SubordinateWriteTests {
        [TestCleanup]
        public void Cleanup () {
            TestEnvironment.RunDatabaseCleanup();
        }

        [TestMethod]
        public void TrackSubordinateReference () {
            using (var context = new TestDataContext(ContextOptions.TrackChanges)) {
                var scroll = new Scroll();
                scroll.Text = "TestTrackedSubordinateWrite";
                scroll.ScrollParchment = new ScrollParchment();
                scroll.ScrollParchment.Scroll = scroll;
                scroll.ScrollParchment.Quality = 8888;
                
                var changes = context.CreateChangeSet();
                changes.Add(scroll);

                Assert.AreEqual(2, changes.OperationsByInstance.Count);
            }
        }
    }
}
