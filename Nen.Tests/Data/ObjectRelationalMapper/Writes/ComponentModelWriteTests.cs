﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Components;
using Nen.Data.SqlModel;

namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class ComponentModelWriteTests {
        private static string _shieldArrayTable;
        private static string _clockWarpTimeColumn;

        [TestInitialize]
        public void Initialize()
        {
            var conventions = TestConfiguration.GetCachedConventions();
            _shieldArrayTable = conventions.GetDataIdentifierName("ShieldArray", DataIdentifierType.Table);
            _clockWarpTimeColumn = conventions.GetDataIdentifierName("ClockWarpTime", DataIdentifierType.Column);
        }

        [TestCleanup]
        public void Cleanup () {
            TestEnvironment.RunDatabaseCleanup();
        }

        [TestMethod]
        public void Insert () {
            var hasLeech = new ShieldArray();
            hasLeech.Leech = new ShieldLeech(true);
            hasLeech.Energy = 78m;

            using (var context = new TestDataContext())
                context.Save(hasLeech);

            var hasLeechRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM " + _shieldArrayTable + " WHERE Energy = 78");
            Assert.IsNotNull(hasLeechRow);
            AssertEx.AreEqual(hasLeechRow["Id"], hasLeech.Id, hasLeech.Id.ToString("N"));
            Assert.AreEqual(78m, hasLeechRow["Energy"]);
            AssertEx.AreEqual(hasLeechRow.GetValue("LeechSuckingPower"), true, "Y");
        }


        [TestMethod]
        public void MultiLevelInsert () {
            var ship = new Starship();
            ship.AftPhaser = new PhaserBank(true, 99);
            ship.Serial = "WRITTEN-FOOBAR123";
            ship.ForePhaser = new PhaserBank(false, 111);
            ship.ShipTime = new DateTime(1999, 7, 12);
            ship.Shuttle = new Shuttle("WRITTEN-SHUTTLE555", new PhaserBank(true, 453));

            using (var context = new TestDataContext())
                context.Save(ship);

            var shipRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Starship WHERE Serial = 'WRITTEN-FOOBAR123'");
            Assert.IsNotNull(shipRow);
            Assert.AreEqual(ship.Id, shipRow.GetGuid("Id"));
            Assert.AreEqual(DBNull.Value, shipRow.GetValue("ShieldId"));
            Assert.AreEqual("WRITTEN-FOOBAR123", shipRow["Serial"]);
            Assert.AreEqual(true, shipRow.GetBool("AftPhaserActive"));
            Assert.AreEqual(99, shipRow.GetInt("AftPhaserPower"));
            Assert.AreEqual(false, shipRow.GetBool("ForePhaserActive"));
            Assert.AreEqual(111, shipRow.GetInt("ForePhaserPower"));
            Assert.AreEqual(0, shipRow.GetInt("MissileWarheads"));
            Assert.AreEqual("WRITTEN-SHUTTLE555", shipRow.GetValue("ShuttleSerial"));
            Assert.AreEqual(true, shipRow.GetBool("ShuttleOmniPhaserActive"));
            Assert.AreEqual(453, shipRow.GetInt("ShuttleOmniPhaserPower"));
            Assert.AreEqual(new DateTime(1999, 7, 12), shipRow.GetValue("ShipTime"));
            Assert.AreEqual(ship.Clock.WarpTime.StripMilliseconds(), ((DateTime) shipRow.GetValue("ClockWarpTime")).StripMilliseconds());
        }

        [TestMethod]
        public void ConcurrencyFailure () {
            var ship = new Starship();
            ship.AftPhaser = new PhaserBank(true, 99);
            ship.Serial = "WRITTEN-POPULAR";
            ship.ForePhaser = new PhaserBank(false, 111);
            ship.ShipTime = new DateTime(1999, 7, 12);
            ship.Shuttle = new Shuttle("WRITTEN-SHUTTLE555", new PhaserBank(true, 453));

            using (var context = new TestDataContext()) {
                context.Save(ship);

                var newTime = ship.Clock.WarpTime.AddMinutes(10);
                var formatter = TestEnvironment.Database.CreateFormatter();

                var update = new SqlUpdate();
                update.Table = new SqlIdentifier("Starship");
                update.Where = new SqlBinaryOperatorExpression(new SqlIdentifier("Id"), "=", new SqlLiteralExpression(ship.Id));
                update.Set(new SqlIdentifier(_clockWarpTimeColumn), new SqlLiteralExpression(ship.Clock.WarpTime.AddMinutes(10)));

                var cmd = formatter.Generate(update);
                TestEnvironment.Database.ExecuteNonQuery(cmd);

                if (TestEnvironment.Database.AreModifiedRowsReturnedAccurate)
                    AssertEx.ExceptionThrown<UpdateException>(delegate { context.Save(ship); });
                else
                    Assert.Inconclusive("Database does not accurately return modified rows, not testable.");
            }
        }
    }
}
