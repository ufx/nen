﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.StraightforwardSequentialKeys;

namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class StraightforwardSequentialKeyWriteTests {
        private string _petDinosaurTable;
        private string _dinoEggTable;

        [TestInitialize]
        public void Initialize()
        {
            var conventions = TestConfiguration.GetCachedConventions();
            _petDinosaurTable = conventions.GetDataIdentifierName("PetDinosaur", DataIdentifierType.Table);
            _dinoEggTable = conventions.GetDataIdentifierName("DinoEgg", DataIdentifierType.Table);
        }

        [TestCleanup]
        public void Cleanup () {
            TestEnvironment.RunDatabaseCleanup();
        }

        [TestMethod]
        public void InsertSingle () {
            var gwar = new Caveman();
            gwar.Name = "WrittenGwar";
            //gwar.PossibleAges = new int[] { 15, 18, 23, 45 };

            using (var context = new TestDataContext())
                context.Save(gwar);

            Assert.IsTrue(gwar.Id > 0);

            var gwarRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Caveman WHERE Name = 'WrittenGwar'");
            Assert.IsNotNull(gwarRow);
            Assert.AreEqual(gwar.Id, Convert.ToInt32(gwarRow["Id"]));
            Assert.AreEqual("WrittenGwar", gwarRow["Name"]);
            //Assert.AreEqual("15,18,23,45", gwarRow["PossibleAges"]);
        }

        [TestMethod]
        public void InsertParentChild () {
            var mug = new Caveman();
            mug.Name = "WrittenMug";

            var korgo = new PetDinosaur();
            korgo.Name = "WrittenKorgo";
            korgo.OwnerCaveman = mug;

            mug.PetDinosaurs.Add(korgo);

            using (var context = new TestDataContext()) {
                var changeset = context.CreateChangeSet();
                changeset.Add(mug);
                changeset.Add(korgo);
                changeset.Commit();
            }

            Assert.IsTrue(mug.Id > 0);
            Assert.IsTrue(korgo.Id > 0);

            var mugRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Caveman WHERE Name = 'WrittenMug'");
            Assert.IsNotNull(mugRow);
            Assert.AreEqual(mug.Id, mugRow.GetInt("Id"));
            Assert.AreEqual("WrittenMug", mugRow["Name"]);

            var korgoRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM " + _petDinosaurTable + " WHERE Name = 'WrittenKorgo'");
            Assert.IsNotNull(korgoRow);
            Assert.AreEqual(korgo.Id, korgoRow.GetInt("Id"));
            Assert.AreEqual(mug.Id, korgoRow.GetInt("OwnerCavemanId"));
            Assert.AreEqual("WrittenKorgo", korgoRow["Name"]);
        }

        [TestMethod]
        public void UpdateSingle () {
            var smlarg = new Caveman();
            smlarg.Name = "WrittenSmlarg";

            using (var context = new TestDataContext()) {
                context.Save(smlarg);

                int id = smlarg.Id;

                smlarg.Name = "WrittenSmlargy";
                context.Save(smlarg);

                Assert.AreEqual(id, smlarg.Id);
            }

            var smlargRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Caveman WHERE Name = 'WrittenSmlargy'");
            Assert.IsNotNull(smlargRow);
            Assert.AreEqual(smlarg.Id, Convert.ToInt32(smlargRow["Id"]));
            Assert.AreEqual("WrittenSmlargy", smlargRow["Name"]);
        }

        [TestMethod]
        public void ChangedTrackedInsertWithReference () {
            var shlug = new Caveman();
            shlug.Name = "WrittenShlug";

            var lork = new PetDinosaur();
            lork.Name = "WrittenLork";
            lork.OwnerCaveman = shlug;
            shlug.PetDinosaurs.Add(lork);

            using (var context = new TestDataContext(ContextOptions.TrackChanges)) {
                var changes = context.GetTrackedChanges();
                changes.Add(lork);

                Assert.AreEqual(2, changes.OperationsByInstance.Count);
                changes.Commit();

                var newChanges = context.GetTrackedChanges();
                Assert.AreEqual(0, newChanges.OperationsByInstance.Count);
            }
        }

        [TestMethod]
        public void InsertBoolean()
        {
            int id;
            using (var context = new TestDataContext())
            {
                var egg = new DinoEgg();
                egg.Hatched = true;
                context.Save(egg);
                id = egg.Id;

                var row = TestEnvironment.Database.ExecuteDataRowSql("select * from " + _dinoEggTable + " where Id = " + id);
                Assert.IsTrue((bool) row["hatched"]);
            }

            using (var context = new TestDataContext())
            {
                var egg = context.Load<DinoEgg>(id);
                Assert.AreEqual(id, egg.Id);
                Assert.IsTrue(egg.Hatched);
            }
        }
    }
}
