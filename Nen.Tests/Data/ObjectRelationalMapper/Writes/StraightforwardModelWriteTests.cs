﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper;
using Nen.Data.SqlModel;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Straightforward;
using System.Data.SqlClient;

namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class StraightforwardModelWriteTests {
        private static string _tatteredClothingTable;
        private static string _spellEffectTable;
        private static string _zombieTable;
        private static string _numberOfHolesColumn;
        private static string _remainingAppendagesColumn;
        private static string _dateModifiedColumn;

        [TestInitialize]
        public void Initialize()
        {
            var conventions = TestConfiguration.GetCachedConventions();
            _tatteredClothingTable = conventions.GetDataIdentifierName("TatteredClothing", DataIdentifierType.Table);
            _numberOfHolesColumn = conventions.GetDataIdentifierName("NumberOfHoles", DataIdentifierType.Column);
            _zombieTable = conventions.GetDataIdentifierName("Zombie", DataIdentifierType.Table);
            _remainingAppendagesColumn = conventions.GetDataIdentifierName("RemainingAppendages", DataIdentifierType.Column);
            _dateModifiedColumn = conventions.GetDataIdentifierName("DateModified", DataIdentifierType.Column);
            _spellEffectTable = conventions.GetDataIdentifierName("SpellEffect", DataIdentifierType.Table);
        }

        [TestCleanup]
        public void Cleanup () {
            TestEnvironment.RunDatabaseCleanup();
        }

        [TestMethod]
        public void Insert () {
            var romero = new Zombie();
            romero.Name = "WrittenRomero";
            romero.RemainingAppendages = 7;
            romero.TatteredClothing = new TatteredClothing();
            romero.TatteredClothing.NumberOfHoles = 67;
            romero.BitBy = new Zombie();
            romero.BitBy.Name = "WrittenHeather";
            romero.BitBy.RemainingAppendages = 5;
            romero.BitBy.Infected = new DateTime(2005, 04, 02);
            Assert.AreEqual(default(DateTime), romero.DateModified);
            Assert.AreEqual(default(DateTime), romero.BitBy.DateModified);

            using (var context = new TestDataContext()) {
                var changes = context.CreateChangeSet();
                changes.Add(romero);
                changes.Add(romero.BitBy);
                changes.Add(romero.TatteredClothing);
                changes.Commit();
            }

            Assert.AreNotEqual(default(DateTime), romero.DateModified);
            Assert.AreNotEqual(default(DateTime), romero.BitBy.DateModified);

            var romeroRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Zombie WHERE Name = 'WrittenRomero'");
            Assert.IsNotNull(romeroRow);
            AssertEx.AreEqual(romeroRow["Id"], romero.Id, romero.Id.ToString("N"));
            Assert.AreEqual(7, Convert.ToInt32(romeroRow.GetValue("RemainingAppendages")));
            AssertEx.AreEqual(romeroRow.GetValue("TatteredClothingId"), romero.TatteredClothing.Id, romero.TatteredClothing.Id.ToString("N"));
            AssertEx.AreEqual(romeroRow.GetValue("BitById"), romero.BitBy.Id, romero.BitBy.Id.ToString("N"));
            Assert.AreEqual(DBNull.Value, romeroRow["Infected"]);
            Assert.AreEqual("WrittenRomero", romeroRow["Name"]);
            Assert.AreEqual(romero.DateModified.StripMilliseconds(), ((DateTime) romeroRow.GetValue("DateModified")).StripMilliseconds());

            var heatherRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Zombie WHERE Name = 'WrittenHeather'");
            Assert.IsNotNull(heatherRow);
            AssertEx.AreEqual(heatherRow["Id"], romero.BitBy.Id, romero.BitBy.Id.ToString("N"));
            Assert.AreEqual("WrittenHeather", heatherRow["Name"]);
            Assert.AreEqual(DBNull.Value, heatherRow.GetValue("BitById"));
            Assert.AreEqual(new DateTime(2005, 04, 02), heatherRow["Infected"]);
            Assert.AreEqual(DBNull.Value, heatherRow.GetValue("TatteredClothingId"));
            Assert.AreEqual(romero.BitBy.DateModified.StripMilliseconds(), ((DateTime) heatherRow.GetValue("DateModified")).StripMilliseconds());

            var clothingRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM " + _tatteredClothingTable + " WHERE " + _numberOfHolesColumn + " = 67");
            Assert.IsNotNull(clothingRow);
            AssertEx.AreEqual(clothingRow["Id"], romero.TatteredClothing.Id, romero.TatteredClothing.Id.ToString("N"));
            Assert.AreEqual(67, Convert.ToInt32(clothingRow[_numberOfHolesColumn]));
        }

        [TestMethod]
        public void TransactionalChangeSetCommit () {
            var badThing = new Zombie();
            badThing.Name = "UnwrittenBadThing";
            badThing.TatteredClothing = new TatteredClothing();
            badThing.TatteredClothing.NumberOfHoles = 62;

            using (var context = new TestDataContext()) {
                var changes = context.CreateChangeSet();
                changes.Add(badThing);
                changes.Add(badThing.TatteredClothing);

                // This will fail with a check constraint for UnwrittenBadThing
                // on the Zombie table.

                var thrown = AssertEx.CheckException<Exception>(delegate { changes.Commit(); });
                if (!thrown)
                    Assert.Inconclusive("Transaction never generated an error.");
            }

            Assert.IsFalse(TestEnvironment.Database.RowsExistSql("SELECT * FROM Zombie WHERE Name = 'UnwrittenBadThing'"));
            Assert.IsFalse(TestEnvironment.Database.RowsExistSql("SELECT * FROM " + _tatteredClothingTable + " WHERE " + _numberOfHolesColumn + " = 62"));
        }

        [TestMethod]
        public void Update () {
            var newThing = new Zombie();
            newThing.Name = "WrittenNewGuy";
            newThing.TatteredClothing = new TatteredClothing();
            newThing.TatteredClothing.NumberOfHoles = 66;
            newThing.Infected = new DateTime(2004, 02, 06);
			newThing.VirusIncubationTime = TimeSpan.FromHours(3);
            newThing.RemainingAppendages = 4;

            using (var context = new TestDataContext()) {
                var changes = context.CreateChangeSet();
                changes.Add(newThing);
                changes.Add(newThing.TatteredClothing);
                changes.Commit();

                newThing.TatteredClothing.NumberOfHoles = 68;
                context.Save(newThing.TatteredClothing);

                var ts = TimeSpan.FromHours(2);
				var reZombie = context.Get<Zombie>().Where(z => z.Id == newThing.Id && z.VirusIncubationTime > ts).First();
				Assert.AreEqual(3, reZombie.VirusIncubationTime.Hours);
            }

            Assert.IsFalse(TestEnvironment.Database.RowsExistSql("SELECT * FROM " + _tatteredClothingTable + " WHERE " + _numberOfHolesColumn + " = 66"));

            var table = TestEnvironment.Database.ExecuteDataTableSql("SELECT * FROM " + _tatteredClothingTable + " WHERE " + _numberOfHolesColumn + " = 68");
            Assert.AreEqual(1, table.Rows.Count);

            var row = table.Rows[0];
            AssertEx.AreEqual(row["Id"], newThing.TatteredClothing.Id.ToString("N"), newThing.TatteredClothing.Id);
            Assert.AreEqual(68, newThing.TatteredClothing.NumberOfHoles);
        }

        [TestMethod]
        public void Delete () {
            var formatter = TestEnvironment.Database.CreateFormatter();

            var deleter = new Zombie();
            deleter.Name = "WrittenDeleter";
            deleter.RemainingAppendages = 111;

            using (var context = new TestDataContext()) {
                context.Save(deleter);
                Assert.IsTrue(TestEnvironment.Database.RowsExistSql(string.Format("SELECT * FROM Zombie WHERE Id = '{0}'", formatter.FormatValue(deleter.Id))));
                context.Delete(deleter);
                Assert.IsFalse(TestEnvironment.Database.RowsExistSql(string.Format("SELECT * FROM Zombie WHERE Id = '{0}'", formatter.FormatValue(deleter.Id))));
            }
        }

        [TestMethod]
        public void DeleteWithForeignKeys()
        {
            var formatter = TestEnvironment.Database.CreateFormatter();

            var spell = new Spell();
            spell.Description = "DeleteTest";
            var spellEffect = new SpellEffect();
            spellEffect.Spell = spell;
            spellEffect.Damage = 1;

            
            using (var context = new TestDataContext())
            {
                var changeSet = context.CreateChangeSet();
                changeSet.Add(spell);
                changeSet.Add(spellEffect);

                changeSet.Commit();

                Assert.IsTrue(TestEnvironment.Database.RowsExistSql(string.Format("SELECT * FROM Spell WHERE Id = '{0}'", formatter.FormatValue(spell.Id))));
                Assert.IsTrue(TestEnvironment.Database.RowsExistSql(string.Format("SELECT * FROM " + _spellEffectTable + " WHERE Id = '{0}'", formatter.FormatValue(spellEffect.Id))));

                changeSet = context.CreateChangeSet();
                changeSet.Delete(spellEffect);
                changeSet.Delete(spell);
                changeSet.Commit();
                Assert.IsFalse(TestEnvironment.Database.RowsExistSql(string.Format("SELECT * FROM Spell WHERE Id = '{0}'", formatter.FormatValue(spell.Id))));
                Assert.IsFalse(TestEnvironment.Database.RowsExistSql(string.Format("SELECT * FROM " + _spellEffectTable + " WHERE Id = '{0}'", formatter.FormatValue(spellEffect.Id))));
            }
        }

        [TestMethod]
        public void ChangeTrackedUpdate () {
            using (var context = new TestDataContext(ContextOptions.TrackChanges)) {
                var rat = context.Zombie.First(z => z.Name == "Rat");
                Assert.IsNotNull(rat);

                rat.RemainingAppendages = 10;

                var formatter = TestEnvironment.Database.CreateFormatter();

                var changes = context.GetTrackedChanges();
                Assert.AreEqual(1, changes.OperationsByInstance.Count);

                var update = (UpdateChangeOperation) changes.OperationsByInstance[rat];
                var row = changes.Data[_zombieTable].FindRowByPrimaryKey(rat.Id);
                Assert.AreEqual(10, row[_remainingAppendagesColumn]);

                update.PreCommit();
                int numberOfStatements;
                bool isPrepared;
                var command = update.GenerateCommand(TestEnvironment.Database.CreateFormatter(), context.DbConnection, null, out numberOfStatements, out isPrepared);
                Assert.AreEqual(1, numberOfStatements);
                Assert.AreEqual(10, ((IDbDataParameter) command.Parameters[formatter.FormatParameterName(_remainingAppendagesColumn)]).Value);
                var newTimeStamp = (DateTime) ((IDbDataParameter) command.Parameters[formatter.FormatParameterName(_dateModifiedColumn)]).Value;
                Assert.IsTrue(newTimeStamp > new DateTime(1980, 01, 01) && newTimeStamp <= DateTime.UtcNow);
                Assert.AreEqual(formatter.FormatValue(new Guid("CE447EF1-6AB3-48B4-BCBD-2C5DFF606593")), ((IDbDataParameter) command.Parameters[formatter.FormatParameterName("N__PrimaryKey__")]).Value);
                Assert.AreEqual(new DateTime(1980, 01, 01), ((IDbDataParameter) command.Parameters[formatter.FormatParameterName("N__PreviousTimeStamp__")]).Value);

                changes.Commit();

                var data = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Zombie WHERE Id = '" + formatter.FormatValue(rat.Id) + "'");
                Assert.IsNotNull(data);
                Assert.AreEqual(10, data.GetInt(_remainingAppendagesColumn));

                var nextChanges = context.GetTrackedChanges();
                Assert.AreEqual(0, nextChanges.OperationsByInstance.Count);
            }
        }
        
        [TestMethod]
        public void ChangeTrackedCollectionInsert () {
            using (var context = new TestDataContext(ContextOptions.TrackChanges)) {
                var ein = context.Wizard.First(w => w.Name == "Einhowzer");
                Assert.IsNotNull(ein);

                var spell = new Spell();
                spell.Wizard = ein;
                spell.Description = "test tracking collection";
                ein.Spells.Add(spell);

                var effect = new SpellEffect();
                effect.Damage = 909090;
                effect.Spell = spell;

                spell.SpellEffects.Add(effect);

                var changes = context.GetTrackedChanges();
                Assert.AreEqual(2, changes.OperationsByInstance.Count);

                var formatter = TestEnvironment.Database.CreateFormatter();

                changes.Commit();

                var spellData = TestEnvironment.Database.ExecuteDataRowSql(string.Format("SELECT * FROM Spell WHERE Id = '{0}'", formatter.FormatValue(spell.Id)));
                Assert.IsNotNull(spellData);
                Assert.AreEqual(spell.Id, spellData.GetGuid("Id"));
                Assert.AreEqual("test tracking collection", spellData["Description"]);
                Assert.AreEqual(ein.Id, spellData.GetGuid("WizardId"));

                var effectData = TestEnvironment.Database.ExecuteDataRowSql(string.Format("SELECT * FROM " + _spellEffectTable + " WHERE Id = '{0}'", formatter.FormatValue(effect.Id)));
                Assert.IsNotNull(effectData);
                Assert.AreEqual(effect.Id, effectData.GetGuid("Id"));
                Assert.AreEqual(909090, effectData.GetInt("Damage"));
                Assert.AreEqual(spell.Id, effectData.GetGuid("SpellId"));

                var nextChanges = context.GetTrackedChanges();
                Assert.AreEqual(0, nextChanges.OperationsByInstance.Count);
            }
        }
    }
}
