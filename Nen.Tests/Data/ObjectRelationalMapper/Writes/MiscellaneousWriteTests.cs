﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;


namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class MiscellaneousWriteTests {
        [TestCleanup]
        public void Cleanup () {
            TestEnvironment.RunDatabaseCleanup();
        }

        #region Purely Database-Generated Objects
        [TestMethod]
        public void InsertOnlyNulls () {
            var empty = new Treehouse();

            using (var context = new TestDataContext()) {
                context.Save(empty);

                Assert.AreNotEqual(0, empty.Id);
                Assert.IsNull(empty.Height);
                Assert.IsNull(empty.Width);

                var row = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Treehouse");
                Assert.IsNotNull(row);
                Assert.AreEqual(empty.Id, row.GetInt("Id"));
                Assert.AreEqual(DBNull.Value, row["Height"]);
                Assert.AreEqual(DBNull.Value, row["Width"]);
            }
        }
        #endregion
    }

    [Persistent]
    public class Treehouse {
        public int Id { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
    }

}
