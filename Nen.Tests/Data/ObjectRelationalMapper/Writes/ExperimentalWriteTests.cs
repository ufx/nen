﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.Experimental;

namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class ExperimentalWriteTests {
        [TestCleanup, TestInitialize]
        public void Cleanup () {
            TestEnvironment.RunDatabaseCleanup();
        }

        [TestMethod]
        public void WriteAndReadDataRefBytes () {
            var zombrex = new Science();
            zombrex.Name = "Zombrex";
            zombrex.Results = Encoding.ASCII.GetBytes("zombie outbreak");

            using (var context = new TestDataContext())
                context.Save(zombrex);

            var row = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Science WHERE Name = 'Zombrex'");
            CheckScienceRow(row, zombrex);

            using (var context = new TestDataContext(ContextOptions.TrackChanges)) {
                var load = context.Get<Science>().Load(zombrex.Id);
                Assert.IsNotNull(load);
                Assert.AreEqual(zombrex.Id, load.Id);
                Assert.AreEqual("Zombrex", load.Name);
                Assert.IsFalse(load.ResultsRef.IsResolved);
                Assert.IsTrue(zombrex.Results.SequenceEqual(load.Results));
                Assert.IsTrue(load.ResultsRef.IsResolved);

                // Update something other than results row.
                load.Name = "Quacktor";
                var changes = context.GetTrackedChanges();
                var op = (UpdateChangeOperation) changes.OperationsByInstance[load];
                Assert.AreEqual(1, op.UnchangedColumns.Count);
                Assert.AreEqual("Results", op.UnchangedColumns.Single().Variable.Name);
                changes.Commit();

                var updateNoByteChangeRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Science WHERE Name = 'Quacktor'");
                CheckScienceRow(updateNoByteChangeRow, load);

                // Update results.
                load.Results = Encoding.ASCII.GetBytes("update on outbreak");

                context.GetTrackedChanges().Commit();
                var updateRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Science WHERE Name = 'Quacktor'");
                CheckScienceRow(updateRow, load);
            }
        }

        private void CheckScienceRow (DataRow row, Science science) {
            Assert.IsNotNull(row);
            Assert.AreEqual(science.Id, Convert.ToInt32(row["Id"]));
            Assert.AreEqual(science.Name, row["Name"]);
            Assert.IsTrue(science.Results.SequenceEqual((byte[]) row["Results"]));
        }

        [TestMethod]
        public void WriteAndReadDataRefNull () {
            var helicopters = new Science();
            helicopters.Name = "BlackHelicopters";
            helicopters.Results = null;

            using (var context = new TestDataContext())
                context.Save(helicopters);

            var row = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM Science WHERE Name = 'BlackHelicopters'");
            Assert.IsNotNull(row);
            Assert.AreEqual(helicopters.Id, Convert.ToInt32(row["Id"]));
            Assert.AreEqual("BlackHelicopters", row["Name"]);
            Assert.AreEqual(DBNull.Value, row["Results"]);

            using (var context = new TestDataContext()) {
                var load = context.Get<Science>().Load(helicopters.Id);
                Assert.IsNotNull(load);
                Assert.AreEqual(helicopters.Id, load.Id);
                Assert.AreEqual("BlackHelicopters", load.Name);
                Assert.IsFalse(load.ResultsRef.IsResolved);
                Assert.IsNull(load.Results);
                Assert.IsTrue(load.ResultsRef.IsResolved);
            }
        }
    }
}
