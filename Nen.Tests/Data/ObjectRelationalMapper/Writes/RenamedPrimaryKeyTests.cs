﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Tests.Data.ObjectRelationalMapper.Models.RenamedPrimaryKeys;
using Nen.Data.ObjectRelationalMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class RenamedPrimaryKeyTests {
        [TestMethod]
        public void Insert () {
            var gcube = new GelatinousCube { X1 = 3, Y1 = 3, Z1 = 3, X2 = 3, Y2 = 3, Z2 = 3 };

            using (var ctx = new TestDataContext()) {
                ctx.Save(gcube);

                var newCube = ctx.Load<GelatinousCube>(gcube.Id);
                Assert.AreEqual(3, newCube.X1);
                Assert.AreEqual(3, newCube.Y1);
                Assert.AreEqual(3, newCube.Z1);

                Assert.AreEqual(3, newCube.X2);
                Assert.AreEqual(3, newCube.Y2);
                Assert.AreEqual(3, newCube.Z2);
            }
        }

        [TestMethod]
        public void Update () {
            using (var ctx = new TestDataContext()) {
                var gcube = ctx.Load<GelatinousCube>(1);
                gcube.Permeability = 1.0f;
                ctx.Save(gcube);
            }
        }
    }
}
