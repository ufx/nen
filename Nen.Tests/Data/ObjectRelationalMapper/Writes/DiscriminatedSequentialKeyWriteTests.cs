﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.ObjectRelationalMapper;
using Nen.Tests.Data.ObjectRelationalMapper.Models.DiscriminatedSequentialKeys;

namespace Nen.Tests.Data.ObjectRelationalMapper.Writes {
    [TestClass]
    public class DiscriminatedSequentialKeyWriteTests {
        private static string _emptyFixtureTable;
        private static string _officeFixtureTable;

        [TestInitialize]
        public void Initialize()
        {
            var conventions = TestConfiguration.GetCachedConventions();
            _emptyFixtureTable = conventions.GetDataIdentifierName("EmptyFixture", DataIdentifierType.Table);
            _officeFixtureTable = conventions.GetDataIdentifierName("OfficeFixture", DataIdentifierType.Table);
        }

        [TestCleanup]
        public void Cleanup () {
            TestEnvironment.RunDatabaseCleanup();
        }

        [TestMethod]
        public void Insert () {
            var conventions = TestConfiguration.GetCachedConventions();

            var fireplace = new FireplaceFixture();
            fireplace.Cost = 123.45m;
            fireplace.MagicalArtifactsBurned = 13;

            using (var context = new TestDataContext())
                context.Save(fireplace);

            Assert.IsTrue(fireplace.Id > 0);
            Assert.AreEqual(FixtureType.Fireplace, fireplace.Type);

            var fireplaceRow = TestEnvironment.Database.ExecuteDataRowSql(string.Format("SELECT * FROM " + conventions.GetDataIdentifierName("FireplaceFixture", DataIdentifierType.Table) + " WHERE Id = {0}", fireplace.Id));
            Assert.IsNotNull(fireplaceRow);
            Assert.AreEqual(fireplace.Id, fireplaceRow.GetInt("Id"));
            Assert.AreEqual(13, fireplaceRow.GetInt("MagicalArtifactsBurned"));

            var fixtureRow = TestEnvironment.Database.ExecuteDataRowSql(string.Format("SELECT * FROM Fixture WHERE Id = {0}", fireplace.Id));
            Assert.IsNotNull(fixtureRow);
            Assert.AreEqual(fireplace.Id, fixtureRow.GetInt("Id"));
            Assert.AreEqual(123.45m, fixtureRow["Cost"]);
        }

        [TestMethod]
        public void UpdateReferenceBase () {
            using (var context = new TestDataContext(ContextOptions.TrackChanges)) {
                var empty = context.EmptyFixture.Load(1);

                empty.Name = "GotNothingElse";
                var changes = context.GetTrackedChanges();
                Assert.AreEqual(1, changes.OperationsByInstance.Count);
                changes.Commit();
            }

            var emptyOfficeRow = TestEnvironment.Database.ExecuteDataRowSql("SELECT * FROM " + _officeFixtureTable + " WHERE Id = 1");
            Assert.AreEqual("GotNothingElse", emptyOfficeRow["Name"]);
        }

        [TestMethod]
        public void InsertReferenceBase () {
            using (var context = new TestDataContext()) {
                var empty = new EmptyFixture();
                empty.Cost = 1231;
                empty.Name = "WrittenReferenceBase";

                context.Save(empty);

                Assert.AreEqual(FixtureType.Empty, empty.Type);

                var emptyRow = TestEnvironment.Database.ExecuteDataRowSql(string.Format("SELECT * FROM " + _emptyFixtureTable + " WHERE Id = {0}", empty.Id));
                Assert.IsNotNull(emptyRow);

                var officeRow = TestEnvironment.Database.ExecuteDataRowSql(string.Format("SELECT * FROM " + _officeFixtureTable + " WHERE Id = {0}", empty.Id));
                Assert.IsNotNull(officeRow);
                Assert.AreEqual("WrittenReferenceBase", officeRow["Name"]);

                var fixtureRow = TestEnvironment.Database.ExecuteDataRowSql(string.Format("SELECT * FROM Fixture WHERE Id = {0}", empty.Id));
                Assert.IsNotNull(fixtureRow);
                Assert.AreEqual(1231m, fixtureRow["Cost"]);
            }
        }

        [TestMethod]
        public void TrackedInsertReferenceBase () {
            using (var context = new TestDataContext(ContextOptions.TrackChanges)) {
                var empty = new EmptyFixture();
                empty.Cost = 3434;
                empty.Name = "WrittenTrackedReferenceBase";

                context.Save(empty);

                Assert.AreEqual(FixtureType.Empty, empty.Type);

                var changes = context.GetTrackedChanges();
                Assert.AreEqual(0, changes.OperationsByInstance.Count);
            }
        }
    }
}
