using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data.SqlModel;
using Nen.Data.SqlClient;
using System.Data.SqlClient;

namespace Nen.Tests.Data.SqlClient {
    [TestClass]
    public class SqlServerFormatterTests {
        [TestMethod]
        public void FormatSimpleSelect () {
            SqlServerFormatter formatter = new SqlServerFormatter();
            IDbCommand command = formatter.Generate(GenerateSimpleSelect());

            string sql = "SELECT Gnome.Id, Gnome.Name, Hat.GnomeId, Hat.Color FROM Gnome INNER JOIN Hat ON Hat.GnomeId = Gnome.Id WHERE Hat.Color = @1";
            Assert.AreEqual(sql, command.CommandText);
            Assert.AreEqual(1, command.Parameters.Count);
            Assert.AreEqual("Blue", ((IDbDataParameter) command.Parameters[0]).Value);
        }

        [TestMethod]
        public void FormatPaginatedSelect () {
            SqlSelect select = GenerateSimpleSelect();
            select.OrderBy.Add(new SqlOrder(SqlOrderDirection.Descending, "Gnome", "Name"));
            select.Limit = 10;

            SqlServerFormatter formatter = new SqlServerFormatter();
            IDbCommand command = formatter.Generate(select);

            string limitedSql = "SELECT TOP 10 Gnome.Id, Gnome.Name, Hat.GnomeId, Hat.Color FROM Gnome INNER JOIN Hat ON Hat.GnomeId = Gnome.Id WHERE Hat.Color = @1 ORDER BY Gnome.Name DESC";
            Assert.AreEqual(limitedSql, command.CommandText);

            select.Offset = 5;

            command = formatter.Generate(select);
            string windowedSql = "SELECT TOP 10 * FROM (SELECT Gnome.Id, Gnome.Name, Hat.GnomeId, Hat.Color, ROW_NUMBER() OVER( ORDER BY Gnome.Name DESC) AS __ROW_NUMBER__ FROM Gnome INNER JOIN Hat ON Hat.GnomeId = Gnome.Id WHERE Hat.Color = @1) AS __WINDOW__ WHERE __ROW_NUMBER__ > 5";         
            Assert.AreEqual(windowedSql, command.CommandText);
        }

        #region Test Helpers
        private SqlSelect GenerateSimpleSelect () {
            SqlSelect select = new SqlSelect();
            select.Columns.Add(new SqlColumn("Gnome", "Id"));
            select.Columns.Add(new SqlColumn("Gnome", "Name"));
            select.Columns.Add(new SqlColumn("Hat", "GnomeId"));
            select.Columns.Add(new SqlColumn("Hat", "Color"));
            select.From.Add(new SqlTable("Gnome"));
            select.Joins.Add(new SqlJoin("Hat", new SqlBinaryOperatorExpression(new SqlColumn("Hat", "GnomeId"), "=", new SqlColumn("Gnome", "Id"))));
            select.Where = new SqlBinaryOperatorExpression(new SqlColumn("Hat", "Color"), "=", new SqlLiteralExpression("Blue"));

            return select;
        }
        #endregion
    }
}
