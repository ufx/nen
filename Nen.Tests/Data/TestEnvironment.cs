using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

using Nen.Reflection;
using Nen.Data.SqlClient;
using Nen.Data;
using System.Reflection;

namespace Nen.Tests.Data {
    public static class TestEnvironment {
        private static Database _database;
        public static Database Database
        {
            get {
                if (_database == null)
                {
                    var type = Config.DatabaseType;
                    _database = (Database)Activator.CreateInstance(type, Config.SqlConnectionString);
                }
                return _database;
            }
        }

        public static void RunDatabaseCleanup()
        {
            var scriptName = ConfigurationManager.AppSettings["DatabaseCleanupScript"];
            var script = Assembly.GetCallingAssembly().ReadManifestResource("Nen.Tests.Data.ObjectRelationalMapper.DatabaseScripts." + scriptName);
            Database.ExecuteNonQuerySql(script);
        }
    }
}
