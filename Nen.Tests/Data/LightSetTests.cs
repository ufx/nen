﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Nen.Data;

namespace Nen.Tests.Data {
    [TestClass]
    public class LightSetTests {
        [TestMethod]
        public void Index () {
            var set = CreateSet1();
            var foo = set["Foo"];
            Assert.IsNotNull(foo);

            var pk1 = foo.FindRowByPrimaryKey(560);
            Assert.IsNotNull(pk1);
            Assert.AreEqual(560, pk1["1"]);
            Assert.AreEqual(780, pk1["2"]);
            Assert.AreEqual("bb", pk1["3"]);

            var noMatches = foo.FindRows("3", "zz");
            Assert.AreEqual(0, noMatches.Count());

            var matches = foo.FindRows("3", "cc");
            Assert.AreEqual(2, matches.Count());
            Assert.IsTrue(matches.Any(m => Equals(m["1"], 900)));
            Assert.IsTrue(matches.Any(m => Equals(m["1"], 341)));
        }

        [TestMethod]
        public void Merge () {
            var set1 = CreateSet1();
            var set2 = CreateSet2();
            set1.Merge(set2);

            Assert.AreEqual(set1.Tables.Count(), 3);

            var foo = set1["Foo"];
            Assert.AreEqual(foo.Rows.Count(), 6);
            var merged = foo.FindRowByPrimaryKey(120);
            Assert.AreEqual(120, merged["1"]);
            Assert.AreEqual(999, merged["2"]);
            Assert.AreEqual("bzzt", merged["3"]);

            var bar = set1["Bar"];
            Assert.AreEqual(bar.Rows.Count(), 4);

            var qux = set1["Qux"];
            Assert.AreEqual(qux.Rows.Count(), 1);
        }

        private static LightSet CreateSet1 () {
            var set = new LightSet();
            var foo = new LightTable("Foo");
            foo.AddColumn("1");
            foo.AddColumn("2");
            foo.AddColumn("3");
            foo.AddIndex("1", true);

            foo.AddRow(120, 340, "aa");
            foo.AddRow(560, 780, "bb");
            foo.AddRow(900, 121, "cc");
            foo.AddRow(341, 561, "cc");
            set.AddTable(foo);

            var bar = new LightTable("Bar");
            bar.AddColumn("a");
            bar.AddColumn("b");
            bar.AddColumn("c");

            bar.AddRow("aaa", "bbb");
            bar.AddRow("ccc", "ddd");
            bar.AddRow("eee", "fff");
            bar.AddRow("ggg", "hhh");
            set.AddTable(bar);

            return set;
        }

        private static LightSet CreateSet2 () {
            var set = new LightSet();
            var foo = new LightTable("Foo");
            foo.AddColumn("1");
            foo.AddColumn("2");
            foo.AddColumn("3");
            foo.AddIndex("1", true);

            foo.AddRow(120, 999, "bzzt");
            foo.AddRow(1000, 780, "bb");
            foo.AddRow(2000, 121, "cc");
            set.AddTable(foo);

            var qux = new LightTable("Qux");
            qux.AddColumn("hello");
            qux.AddColumn("world");

            qux.AddRow("1010101", "010000111");
            set.AddTable(qux);

            return set;
        }
    }
}

