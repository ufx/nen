﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Nen.Tests.Diagnostics {
    public class DebugWriter : TextWriter {
        #region Write
        public override void Write (bool value) {
            Debug.Write(value);
        }

        public override void Write (char value) {
            Debug.Write(value);
        }

        public override void Write (string value) {
            Debug.Write(value);
        }

        public override void Write (object value) {
            Debug.Write(value);
        }

        public override void Write (char[] buffer) {
            Debug.Write(new string(buffer));
        }

        public override void Write (char[] buffer, int index, int count) {
            Debug.Write(new string(buffer, index, count));
        }
        #endregion

        #region WriteLine
        public override void WriteLine (bool value) {
            Debug.WriteLine(value);
        }

        public override void WriteLine (char value) {
            Debug.WriteLine(value);
        }

        public override void WriteLine (string value) {
            Debug.WriteLine(value);
        }

        public override void WriteLine (object value) {
            Debug.WriteLine(value);
        }

        public override void WriteLine (char[] buffer) {
            Debug.WriteLine(new string(buffer));
        }

        public override void WriteLine (char[] buffer, int index, int count) {
            Debug.WriteLine(new string(buffer, index, count));
        }
        #endregion

        #region Encoding
        public override Encoding Encoding {
            get { return UnicodeEncoding.Unicode; }
        }
        #endregion
    }
}
