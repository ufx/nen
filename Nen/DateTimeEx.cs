﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen {
    /// <summary>
    /// Extensions to the System.DateTime class.
    /// </summary>
    public static class DateTimeEx {
        /// <summary>
        /// Strips the milliseconds value of the DateTime.
        /// </summary>
        /// <param name="dateTime">The DateTime to strip milliseconds from.</param>
        /// <returns>A new DateTime with milliseconds stripped.</returns>
        public static DateTime StripMilliseconds (this DateTime dateTime) {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, 0, dateTime.Kind);
        }
    }
}
