﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen {
    /// <summary>
    /// Extensions to the System.Object class.
    /// </summary>
    public static class ObjectEx {
        /// <summary>
        /// Enlists the object as part of an enumerable.
        /// </summary>
        /// <typeparam name="T">The object type.</typeparam>
        /// <param name="obj">The object to enlist.</param>
        /// <returns>An enumerable containing only the given object.</returns>
        public static IEnumerable<T> Enlist<T> (this T obj) {
            yield return obj;
        }
    }
}
