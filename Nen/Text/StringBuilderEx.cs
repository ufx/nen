﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Nen.Text {
    /// <summary>
    /// Extensions to the StringBuilder class.
    /// </summary>
    public static class StringBuilderEx {
        /// <summary>
        /// Appends the separator if necessary, and appends the given value.
        /// </summary>
        /// <param name="text">The text to separate and append.</param>
        /// <param name="separator">The separator.</param>
        /// <param name="value">The value to append.</param>
        /// <returns>The given text.</returns>
        public static StringBuilder Separate (this StringBuilder text, string separator, int value) {
            if (text.Length > 0)
                text.Append(separator);

            return text.Append(value);
        }

        /// <summary>
        /// Append the separator if necessary, and appends a formatted string.
        /// </summary>
        /// <param name="text">The text to separate and append.</param>
        /// <param name="provider">The format provider.</param>
        /// <param name="separator">The separator.</param>
        /// <param name="format">The format string.</param>
        /// <param name="args">The format arguments.</param>
        /// <returns>The given text.</returns>
        public static StringBuilder SeparateFormat (this StringBuilder text, IFormatProvider provider, string separator, string format, params object[] args) {
            if (text.Length > 0)
                text.Append(separator);

            return text.AppendFormat(provider, format, args);
        }
    }
}
