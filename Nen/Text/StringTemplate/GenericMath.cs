// MACHINE GENERATED -- DO NOT MODIFY (see generic_math.rb)
#pragma warning disable 1591

using System;

namespace Nen.Text.StringTemplate {
    internal static class GenericMath {
        public static object Add (object lhs, object rhs) {
            switch (Type.GetTypeCode(lhs.GetType())) {
                case TypeCode.UInt16:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (ushort)lhs + (ushort)rhs;
                        case TypeCode.UInt64:
                            return (ushort)lhs + (ulong)rhs;
                        case TypeCode.UInt32:
                            return (ushort)lhs + (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int16:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (short)lhs + (short)rhs;
                        case TypeCode.Int64:
                            return (short)lhs + (long)rhs;
                        case TypeCode.Decimal:
                            return (short)lhs + (decimal)rhs;
                        case TypeCode.Int32:
                            return (short)lhs + (int)rhs;
                        case TypeCode.Byte:
                            return (short)lhs + (byte)rhs;
                        case TypeCode.Double:
                            return (short)lhs + (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.UInt64:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (ulong)lhs + (ushort)rhs;
                        case TypeCode.UInt64:
                            return (ulong)lhs + (ulong)rhs;
                        case TypeCode.UInt32:
                            return (ulong)lhs + (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int64:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (long)lhs + (short)rhs;
                        case TypeCode.Int64:
                            return (long)lhs + (long)rhs;
                        case TypeCode.Decimal:
                            return (long)lhs + (decimal)rhs;
                        case TypeCode.Int32:
                            return (long)lhs + (int)rhs;
                        case TypeCode.Byte:
                            return (long)lhs + (byte)rhs;
                        case TypeCode.Double:
                            return (long)lhs + (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Decimal:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (decimal)lhs + (short)rhs;
                        case TypeCode.Int64:
                            return (decimal)lhs + (long)rhs;
                        case TypeCode.Decimal:
                            return (decimal)lhs + (decimal)rhs;
                        case TypeCode.Int32:
                            return (decimal)lhs + (int)rhs;
                        case TypeCode.Byte:
                            return (decimal)lhs + (byte)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.UInt32:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (uint)lhs + (ushort)rhs;
                        case TypeCode.UInt64:
                            return (uint)lhs + (ulong)rhs;
                        case TypeCode.UInt32:
                            return (uint)lhs + (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int32:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (int)lhs + (short)rhs;
                        case TypeCode.Int64:
                            return (int)lhs + (long)rhs;
                        case TypeCode.Decimal:
                            return (int)lhs + (decimal)rhs;
                        case TypeCode.Int32:
                            return (int)lhs + (int)rhs;
                        case TypeCode.Byte:
                            return (int)lhs + (byte)rhs;
                        case TypeCode.Double:
                            return (int)lhs + (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Byte:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (byte)lhs + (short)rhs;
                        case TypeCode.Int64:
                            return (byte)lhs + (long)rhs;
                        case TypeCode.Decimal:
                            return (byte)lhs + (decimal)rhs;
                        case TypeCode.Int32:
                            return (byte)lhs + (int)rhs;
                        case TypeCode.Byte:
                            return (byte)lhs + (byte)rhs;
                        case TypeCode.Double:
                            return (byte)lhs + (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Double:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (double)lhs + (short)rhs;
                        case TypeCode.Int64:
                            return (double)lhs + (long)rhs;
                        case TypeCode.Int32:
                            return (double)lhs + (int)rhs;
                        case TypeCode.Byte:
                            return (double)lhs + (byte)rhs;
                        case TypeCode.Double:
                            return (double)lhs + (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                default: throw new ArgumentException("Invalid numeric type.");
            }
        }
        public static object Subtract (object lhs, object rhs) {
            switch (Type.GetTypeCode(lhs.GetType())) {
                case TypeCode.UInt16:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (ushort)lhs - (ushort)rhs;
                        case TypeCode.UInt64:
                            return (ushort)lhs - (ulong)rhs;
                        case TypeCode.UInt32:
                            return (ushort)lhs - (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int16:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (short)lhs - (short)rhs;
                        case TypeCode.Int64:
                            return (short)lhs - (long)rhs;
                        case TypeCode.Decimal:
                            return (short)lhs - (decimal)rhs;
                        case TypeCode.Int32:
                            return (short)lhs - (int)rhs;
                        case TypeCode.Byte:
                            return (short)lhs - (byte)rhs;
                        case TypeCode.Double:
                            return (short)lhs - (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.UInt64:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (ulong)lhs - (ushort)rhs;
                        case TypeCode.UInt64:
                            return (ulong)lhs - (ulong)rhs;
                        case TypeCode.UInt32:
                            return (ulong)lhs - (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int64:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (long)lhs - (short)rhs;
                        case TypeCode.Int64:
                            return (long)lhs - (long)rhs;
                        case TypeCode.Decimal:
                            return (long)lhs - (decimal)rhs;
                        case TypeCode.Int32:
                            return (long)lhs - (int)rhs;
                        case TypeCode.Byte:
                            return (long)lhs - (byte)rhs;
                        case TypeCode.Double:
                            return (long)lhs - (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Decimal:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (decimal)lhs - (short)rhs;
                        case TypeCode.Int64:
                            return (decimal)lhs - (long)rhs;
                        case TypeCode.Decimal:
                            return (decimal)lhs - (decimal)rhs;
                        case TypeCode.Int32:
                            return (decimal)lhs - (int)rhs;
                        case TypeCode.Byte:
                            return (decimal)lhs - (byte)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.UInt32:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (uint)lhs - (ushort)rhs;
                        case TypeCode.UInt64:
                            return (uint)lhs - (ulong)rhs;
                        case TypeCode.UInt32:
                            return (uint)lhs - (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int32:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (int)lhs - (short)rhs;
                        case TypeCode.Int64:
                            return (int)lhs - (long)rhs;
                        case TypeCode.Decimal:
                            return (int)lhs - (decimal)rhs;
                        case TypeCode.Int32:
                            return (int)lhs - (int)rhs;
                        case TypeCode.Byte:
                            return (int)lhs - (byte)rhs;
                        case TypeCode.Double:
                            return (int)lhs - (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Byte:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (byte)lhs - (short)rhs;
                        case TypeCode.Int64:
                            return (byte)lhs - (long)rhs;
                        case TypeCode.Decimal:
                            return (byte)lhs - (decimal)rhs;
                        case TypeCode.Int32:
                            return (byte)lhs - (int)rhs;
                        case TypeCode.Byte:
                            return (byte)lhs - (byte)rhs;
                        case TypeCode.Double:
                            return (byte)lhs - (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Double:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (double)lhs - (short)rhs;
                        case TypeCode.Int64:
                            return (double)lhs - (long)rhs;
                        case TypeCode.Int32:
                            return (double)lhs - (int)rhs;
                        case TypeCode.Byte:
                            return (double)lhs - (byte)rhs;
                        case TypeCode.Double:
                            return (double)lhs - (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                default: throw new ArgumentException("Invalid numeric type.");
            }
        }
        public static object Multiply (object lhs, object rhs) {
            switch (Type.GetTypeCode(lhs.GetType())) {
                case TypeCode.UInt16:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (ushort)lhs * (ushort)rhs;
                        case TypeCode.UInt64:
                            return (ushort)lhs * (ulong)rhs;
                        case TypeCode.UInt32:
                            return (ushort)lhs * (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int16:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (short)lhs * (short)rhs;
                        case TypeCode.Int64:
                            return (short)lhs * (long)rhs;
                        case TypeCode.Decimal:
                            return (short)lhs * (decimal)rhs;
                        case TypeCode.Int32:
                            return (short)lhs * (int)rhs;
                        case TypeCode.Byte:
                            return (short)lhs * (byte)rhs;
                        case TypeCode.Double:
                            return (short)lhs * (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.UInt64:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (ulong)lhs * (ushort)rhs;
                        case TypeCode.UInt64:
                            return (ulong)lhs * (ulong)rhs;
                        case TypeCode.UInt32:
                            return (ulong)lhs * (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int64:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (long)lhs * (short)rhs;
                        case TypeCode.Int64:
                            return (long)lhs * (long)rhs;
                        case TypeCode.Decimal:
                            return (long)lhs * (decimal)rhs;
                        case TypeCode.Int32:
                            return (long)lhs * (int)rhs;
                        case TypeCode.Byte:
                            return (long)lhs * (byte)rhs;
                        case TypeCode.Double:
                            return (long)lhs * (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Decimal:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (decimal)lhs * (short)rhs;
                        case TypeCode.Int64:
                            return (decimal)lhs * (long)rhs;
                        case TypeCode.Decimal:
                            return (decimal)lhs * (decimal)rhs;
                        case TypeCode.Int32:
                            return (decimal)lhs * (int)rhs;
                        case TypeCode.Byte:
                            return (decimal)lhs * (byte)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.UInt32:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (uint)lhs * (ushort)rhs;
                        case TypeCode.UInt64:
                            return (uint)lhs * (ulong)rhs;
                        case TypeCode.UInt32:
                            return (uint)lhs * (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int32:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (int)lhs * (short)rhs;
                        case TypeCode.Int64:
                            return (int)lhs * (long)rhs;
                        case TypeCode.Decimal:
                            return (int)lhs * (decimal)rhs;
                        case TypeCode.Int32:
                            return (int)lhs * (int)rhs;
                        case TypeCode.Byte:
                            return (int)lhs * (byte)rhs;
                        case TypeCode.Double:
                            return (int)lhs * (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Byte:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (byte)lhs * (short)rhs;
                        case TypeCode.Int64:
                            return (byte)lhs * (long)rhs;
                        case TypeCode.Decimal:
                            return (byte)lhs * (decimal)rhs;
                        case TypeCode.Int32:
                            return (byte)lhs * (int)rhs;
                        case TypeCode.Byte:
                            return (byte)lhs * (byte)rhs;
                        case TypeCode.Double:
                            return (byte)lhs * (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Double:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (double)lhs * (short)rhs;
                        case TypeCode.Int64:
                            return (double)lhs * (long)rhs;
                        case TypeCode.Int32:
                            return (double)lhs * (int)rhs;
                        case TypeCode.Byte:
                            return (double)lhs * (byte)rhs;
                        case TypeCode.Double:
                            return (double)lhs * (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                default: throw new ArgumentException("Invalid numeric type.");
            }
        }
        public static object Divide (object lhs, object rhs) {
            switch (Type.GetTypeCode(lhs.GetType())) {
                case TypeCode.UInt16:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (ushort)lhs / (ushort)rhs;
                        case TypeCode.UInt64:
                            return (ushort)lhs / (ulong)rhs;
                        case TypeCode.UInt32:
                            return (ushort)lhs / (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int16:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (short)lhs / (short)rhs;
                        case TypeCode.Int64:
                            return (short)lhs / (long)rhs;
                        case TypeCode.Decimal:
                            return (short)lhs / (decimal)rhs;
                        case TypeCode.Int32:
                            return (short)lhs / (int)rhs;
                        case TypeCode.Byte:
                            return (short)lhs / (byte)rhs;
                        case TypeCode.Double:
                            return (short)lhs / (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.UInt64:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (ulong)lhs / (ushort)rhs;
                        case TypeCode.UInt64:
                            return (ulong)lhs / (ulong)rhs;
                        case TypeCode.UInt32:
                            return (ulong)lhs / (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int64:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (long)lhs / (short)rhs;
                        case TypeCode.Int64:
                            return (long)lhs / (long)rhs;
                        case TypeCode.Decimal:
                            return (long)lhs / (decimal)rhs;
                        case TypeCode.Int32:
                            return (long)lhs / (int)rhs;
                        case TypeCode.Byte:
                            return (long)lhs / (byte)rhs;
                        case TypeCode.Double:
                            return (long)lhs / (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Decimal:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (decimal)lhs / (short)rhs;
                        case TypeCode.Int64:
                            return (decimal)lhs / (long)rhs;
                        case TypeCode.Decimal:
                            return (decimal)lhs / (decimal)rhs;
                        case TypeCode.Int32:
                            return (decimal)lhs / (int)rhs;
                        case TypeCode.Byte:
                            return (decimal)lhs / (byte)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.UInt32:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.UInt16:
                            return (uint)lhs / (ushort)rhs;
                        case TypeCode.UInt64:
                            return (uint)lhs / (ulong)rhs;
                        case TypeCode.UInt32:
                            return (uint)lhs / (uint)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Int32:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (int)lhs / (short)rhs;
                        case TypeCode.Int64:
                            return (int)lhs / (long)rhs;
                        case TypeCode.Decimal:
                            return (int)lhs / (decimal)rhs;
                        case TypeCode.Int32:
                            return (int)lhs / (int)rhs;
                        case TypeCode.Byte:
                            return (int)lhs / (byte)rhs;
                        case TypeCode.Double:
                            return (int)lhs / (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Byte:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (byte)lhs / (short)rhs;
                        case TypeCode.Int64:
                            return (byte)lhs / (long)rhs;
                        case TypeCode.Decimal:
                            return (byte)lhs / (decimal)rhs;
                        case TypeCode.Int32:
                            return (byte)lhs / (int)rhs;
                        case TypeCode.Byte:
                            return (byte)lhs / (byte)rhs;
                        case TypeCode.Double:
                            return (byte)lhs / (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                case TypeCode.Double:
                    switch (Type.GetTypeCode(rhs.GetType())) {
                        case TypeCode.Int16:
                            return (double)lhs / (short)rhs;
                        case TypeCode.Int64:
                            return (double)lhs / (long)rhs;
                        case TypeCode.Int32:
                            return (double)lhs / (int)rhs;
                        case TypeCode.Byte:
                            return (double)lhs / (byte)rhs;
                        case TypeCode.Double:
                            return (double)lhs / (double)rhs;
                        default: throw new ArgumentException("Invalid numeric type.");
                    }
                default: throw new ArgumentException("Invalid numeric type.");
            }
        }
    }
}