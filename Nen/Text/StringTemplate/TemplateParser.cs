// $ANTLR 2.7.5 (20050128): "parser.g" -> "TemplateParser.cs"$

#pragma warning disable 1591

using System.Collections.Generic;

namespace Nen.Text.StringTemplate
{
	// Generate the header common to all output files.
	using System;
	
	using TokenBuffer              = antlr.TokenBuffer;
	using TokenStreamException     = antlr.TokenStreamException;
	using TokenStreamIOException   = antlr.TokenStreamIOException;
	using ANTLRException           = antlr.ANTLRException;
	using LLkParser = antlr.LLkParser;
	using Token                    = antlr.Token;
	using IToken                   = antlr.IToken;
	using TokenStream              = antlr.TokenStream;
	using RecognitionException     = antlr.RecognitionException;
	using NoViableAltException     = antlr.NoViableAltException;
	using MismatchedTokenException = antlr.MismatchedTokenException;
	using SemanticException        = antlr.SemanticException;
	using ParserSharedInputState   = antlr.ParserSharedInputState;
	using BitSet                   = antlr.collections.impl.BitSet;
	
    [CLSCompliant(false)]
	public 	class TemplateParser : antlr.LLkParser
	{
		public const int EOF = 1;
		public const int NULL_TREE_LOOKAHEAD = 3;
		public const int LITERAL = 4;
		public const int ESCAPE = 5;
		public const int SEMI = 6;
		public const int COLON_EQUALS = 7;
		public const int AT = 8;
		public const int AND = 9;
		public const int OR = 10;
		public const int EQUALS = 11;
		public const int NOT_EQUALS = 12;
		public const int LESS = 13;
		public const int LESSEQ = 14;
		public const int GREATER = 15;
		public const int GREATEREQ = 16;
		public const int PLUS = 17;
		public const int MINUS = 18;
		public const int DIV = 19;
		public const int MUL = 20;
		public const int NOT = 21;
		public const int INT = 22;
		public const int IDENT = 23;
		public const int STRING = 24;
		public const int CHAR = 25;
		public const int LPAREN = 26;
		public const int RPAREN = 27;
		public const int DOT = 28;
		public const int COMMA = 29;
		public const int COLON = 30;
		public const int LBRACK = 31;
		public const int RBRACK = 32;
		public const int PIPE = 33;
		public const int LBRACE = 34;
		public const int RBRACE = 35;
		public const int SLITERAL = 36;
		public const int STLITERAL = 37;
		public const int GOBBLE = 38;
		public const int COMMENT = 39;
		public const int ESC_CHAR = 40;
		public const int WS = 41;
		public const int NEWLINE = 42;
		
		
    private ApplyExpression apply (string op, params Expression[] args) {
        return new ApplyExpression(new VariableExpression(op), args);
    }
    
    private ValueExpression lit (object value) {
        return new ValueExpression(value);
    }
		
		protected void initialize()
		{
			tokenNames = tokenNames_;
		}
		
		
		protected TemplateParser(TokenBuffer tokenBuf, int k) : base(tokenBuf, k)
		{
			initialize();
		}
		
		public TemplateParser(TokenBuffer tokenBuf) : this(tokenBuf,2)
		{
		}
		
		protected TemplateParser(TokenStream lexer, int k) : base(lexer,k)
		{
			initialize();
		}
		
		public TemplateParser(TokenStream lexer) : this(lexer,2)
		{
		}
		
		public TemplateParser(ParserSharedInputState state) : base(state,2)
		{
			initialize();
		}
		
	public TemplateExpression  template() //throws RecognitionException, TokenStreamException
{
		TemplateExpression result = new TemplateExpression();
		
		IToken  l = null;
		Expression e;
		
		{    // ( ... )*
			for (;;)
			{
				switch ( LA(1) )
				{
				case LITERAL:
				{
					l = LT(1);
					match(LITERAL);
					result.AddLiteral(l.getText());
					break;
				}
				case ESCAPE:
				{
					match(ESCAPE);
					{
						e=expr_group();
						result.AddExpr(e);
					}
					match(ESCAPE);
					break;
				}
				default:
				{
					goto _loop4_breakloop;
				}
				 }
			}
_loop4_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public Expression  expr_group() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression exp;
		
		switch ( LA(1) )
		{
		case MINUS:
		case NOT:
		case INT:
		case IDENT:
		case STRING:
		case CHAR:
		case LPAREN:
		case LBRACK:
		case LBRACE:
		{
			result=expr();
			{
				switch ( LA(1) )
				{
				case SEMI:
				{
					match(SEMI);
					{
						exp=expr_group();
						result = new SeqExpression(result,exp);
					}
					break;
				}
				case EOF:
				case ESCAPE:
				case RBRACK:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				 }
			}
			break;
		}
		case EOF:
		case ESCAPE:
		case RBRACK:
		{
			result = lit(null);
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		 }
		return result;
	}
	
	public Expression  expr_file() //throws RecognitionException, TokenStreamException
{
		Expression result;
		
		
		result=expr_group();
		return result;
	}
	
	public Expression  expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		
		result=assign_expr();
		return result;
	}
	
	public Expression  assign_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression e;
		
		result=compose_expr();
		{
			switch ( LA(1) )
			{
			case COLON_EQUALS:
			{
				match(COLON_EQUALS);
				e=assign_expr();
				result = new AssignExpression(result, e);
				break;
			}
			case EOF:
			case ESCAPE:
			case SEMI:
			case RPAREN:
			case COMMA:
			case RBRACK:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			 }
		}
		return result;
	}
	
	public Expression  compose_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression e;
		
		result=logior_expr();
		{
			switch ( LA(1) )
			{
			case AT:
			{
				match(AT);
				e=compose_expr();
				result = apply("@", result, e);
				break;
			}
			case EOF:
			case ESCAPE:
			case SEMI:
			case COLON_EQUALS:
			case RPAREN:
			case COMMA:
			case RBRACK:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			 }
		}
		return result;
	}
	
	public Expression  logior_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression e;
		
		result=logand_expr();
		{    // ( ... )*
			for (;;)
			{
				if ((LA(1)==AND))
				{
					match(AND);
					e=logand_expr();
					result = apply("&&", result, e);
				}
				else
				{
					goto _loop16_breakloop;
				}
				
			}
_loop16_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public Expression  logand_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression e;
		
		result=compare_expr();
		{    // ( ... )*
			for (;;)
			{
				if ((LA(1)==OR))
				{
					match(OR);
					e=compare_expr();
					result = apply("||", result, e);
				}
				else
				{
					goto _loop19_breakloop;
				}
				
			}
_loop19_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public Expression  compare_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression e; string op;
		
		result=relational_expr();
		{    // ( ... )*
			for (;;)
			{
				if ((LA(1)==EQUALS||LA(1)==NOT_EQUALS))
				{
					op=compare_op();
					e=relational_expr();
					result = apply(op, result, e);
				}
				else
				{
					goto _loop22_breakloop;
				}
				
			}
_loop22_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public Expression  relational_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression e; string op;
		
		result=additive_expr();
		{    // ( ... )*
			for (;;)
			{
				if (((LA(1) >= LESS && LA(1) <= GREATEREQ)))
				{
					op=rel_op();
					e=additive_expr();
					result = apply(op, result, e);
				}
				else
				{
					goto _loop26_breakloop;
				}
				
			}
_loop26_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public string  compare_op() //throws RecognitionException, TokenStreamException
{
		string rv=null;
		
		
		switch ( LA(1) )
		{
		case EQUALS:
		{
			match(EQUALS);
			rv="=";
			break;
		}
		case NOT_EQUALS:
		{
			match(NOT_EQUALS);
			rv="!=";
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		 }
		return rv;
	}
	
	public Expression  additive_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression e; string op;
		
		result=multiplicative_expr();
		{    // ( ... )*
			for (;;)
			{
				if ((LA(1)==PLUS||LA(1)==MINUS))
				{
					op=add_op();
					e=multiplicative_expr();
					result = apply(op, result, e);
				}
				else
				{
					goto _loop30_breakloop;
				}
				
			}
_loop30_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public string  rel_op() //throws RecognitionException, TokenStreamException
{
		string rv=null;
		
		
		switch ( LA(1) )
		{
		case LESS:
		{
			match(LESS);
			rv="<";
			break;
		}
		case LESSEQ:
		{
			match(LESSEQ);
			rv="<=";
			break;
		}
		case GREATER:
		{
			match(GREATER);
			rv=">";
			break;
		}
		case GREATEREQ:
		{
			match(GREATEREQ);
			rv=">=";
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		 }
		return rv;
	}
	
	public Expression  multiplicative_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression e; string op;
		
		result=unary_expr();
		{    // ( ... )*
			for (;;)
			{
				if ((LA(1)==DIV||LA(1)==MUL))
				{
					op=mul_op();
					e=unary_expr();
					result = apply(op, result, e);
				}
				else
				{
					goto _loop34_breakloop;
				}
				
			}
_loop34_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public string  add_op() //throws RecognitionException, TokenStreamException
{
		string rv=null;
		
		
		switch ( LA(1) )
		{
		case PLUS:
		{
			match(PLUS);
			rv="+";
			break;
		}
		case MINUS:
		{
			match(MINUS);
			rv="-";
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		 }
		return rv;
	}
	
	public Expression  unary_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		Expression exp; string op;
		
		switch ( LA(1) )
		{
		case INT:
		case IDENT:
		case STRING:
		case CHAR:
		case LPAREN:
		case LBRACK:
		case LBRACE:
		{
			result=primary_expr();
			break;
		}
		case MINUS:
		case NOT:
		{
			op=unary_op();
			exp=unary_expr();
			result = apply(op, exp);
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		 }
		return result;
	}
	
	public string  mul_op() //throws RecognitionException, TokenStreamException
{
		string rv=null;
		
		
		switch ( LA(1) )
		{
		case DIV:
		{
			match(DIV);
			rv="/";
			break;
		}
		case MUL:
		{
			match(MUL);
			rv="*";
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		 }
		return rv;
	}
	
	public Expression  primary_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		IToken  x = null;
		List<Expression> a;
		
		result=data_expr();
		{    // ( ... )*
			for (;;)
			{
				switch ( LA(1) )
				{
				case LPAREN:
				{
					{
						a=apply_expr();
						result = new ApplyExpression(result,a);
					}
					break;
				}
				case DOT:
				{
					{
						match(DOT);
						x = LT(1);
						match(IDENT);
						result = new FieldRefExpression(result, x.getText());
					}
					break;
				}
				default:
				{
					goto _loop46_breakloop;
				}
				 }
			}
_loop46_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public string  unary_op() //throws RecognitionException, TokenStreamException
{
		string rv=null;
		
		
		switch ( LA(1) )
		{
		case NOT:
		{
			match(NOT);
			rv="!";
			break;
		}
		case MINUS:
		{
			match(MINUS);
			rv="neg";
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		 }
		return rv;
	}
	
	public Expression  op_literal() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		string op;
		
		{
			switch ( LA(1) )
			{
			case LESS:
			case LESSEQ:
			case GREATER:
			case GREATEREQ:
			{
				op=rel_op();
				break;
			}
			case DIV:
			case MUL:
			{
				op=mul_op();
				break;
			}
			case AT:
			{
				match(AT);
				op="@";
				break;
			}
			case PLUS:
			case MINUS:
			{
				op=add_op();
				break;
			}
			case NOT:
			{
				match(NOT);
				op="!";
				break;
			}
			case AND:
			{
				match(AND);
				op="&&";
				break;
			}
			case OR:
			{
				match(OR);
				op="||";
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			 }
		}
		result = new VariableExpression(op);
		return result;
	}
	
	public Expression  data_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		IToken  i = null;
		IToken  id = null;
		IToken  s = null;
		IToken  c = null;
		
		switch ( LA(1) )
		{
		case INT:
		{
			i = LT(1);
			match(INT);
			result = lit(Int32.Parse(i.getText()));
			break;
		}
		case IDENT:
		{
			id = LT(1);
			match(IDENT);
			result = new VariableExpression(id.getText());
			break;
		}
		case STRING:
		{
			s = LT(1);
			match(STRING);
			result = lit(s.getText());
			break;
		}
		case CHAR:
		{
			c = LT(1);
			match(CHAR);
			result = lit(c.getText()[0]);
			break;
		}
		case LPAREN:
		{
			match(LPAREN);
			{
				if ((tokenSet_0_.member(LA(1))) && (LA(2)==RPAREN))
				{
					result=op_literal();
				}
				else if ((tokenSet_1_.member(LA(1))) && (tokenSet_2_.member(LA(2)))) {
					result=expr();
				}
				else
				{
					throw new NoViableAltException(LT(1), getFilename());
				}
				
			}
			match(RPAREN);
			break;
		}
		case LBRACK:
		{
			result=block_expr();
			break;
		}
		case LBRACE:
		{
			result=template_expr();
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		 }
		return result;
	}
	
	public Expression  block_expr() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		List<string> args=null; Expression exp;
		
		match(LBRACK);
		{
			switch ( LA(1) )
			{
			case PIPE:
			{
				args=formals_list();
				break;
			}
			case MINUS:
			case NOT:
			case INT:
			case IDENT:
			case STRING:
			case CHAR:
			case LPAREN:
			case LBRACK:
			case RBRACK:
			case LBRACE:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			 }
		}
		exp=expr_group();
		match(RBRACK);
		result = new BlockExpression(args, exp);
		return result;
	}
	
	public TemplateExpression  template_expr() //throws RecognitionException, TokenStreamException
{
		TemplateExpression result=null;
		
		List<string> args=null;
		
		match(LBRACE);
		{
			switch ( LA(1) )
			{
			case PIPE:
			{
				args=formals_list();
				break;
			}
			case LITERAL:
			case ESCAPE:
			case RBRACE:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(LT(1), getFilename());
			}
			 }
		}
		result=template();
		match(RBRACE);
		if (args != null) result.Arguments = args;
		return result;
	}
	
	public List<Expression>  apply_expr() //throws RecognitionException, TokenStreamException
{
		List<Expression> result=null;
		
		
		if ((LA(1)==LPAREN) && (LA(2)==RPAREN))
		{
			match(LPAREN);
			match(RPAREN);
			result = new List<Expression>();
		}
		else if ((LA(1)==LPAREN) && (tokenSet_1_.member(LA(2)))) {
			match(LPAREN);
			{
				result=argument_list();
			}
			match(RPAREN);
		}
		else
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		
		return result;
	}
	
	public List<Expression>  argument_list() //throws RecognitionException, TokenStreamException
{
		List<Expression> result = new List<Expression>();
		
		Expression x;
		
		x=argument();
		result.Add(x);
		{    // ( ... )*
			for (;;)
			{
				if ((LA(1)==COMMA))
				{
					match(COMMA);
					x=argument();
					result.Add(x);
				}
				else
				{
					goto _loop51_breakloop;
				}
				
			}
_loop51_breakloop:			;
		}    // ( ... )*
		return result;
	}
	
	public Expression  argument() //throws RecognitionException, TokenStreamException
{
		Expression result=null;
		
		IToken  x = null;
		Expression e;
		
		if ((LA(1)==IDENT) && (LA(2)==COLON))
		{
			x = LT(1);
			match(IDENT);
			match(COLON);
			e=expr();
			result = new BindExpression(x.getText(), e);
		}
		else if ((tokenSet_1_.member(LA(1))) && (tokenSet_3_.member(LA(2)))) {
			result=expr();
		}
		else
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		
		return result;
	}
	
	public List<string>  formals_list() //throws RecognitionException, TokenStreamException
{
		List<string> result=new List<string>();
		
		IToken  x = null;
		
		match(PIPE);
		{    // ( ... )*
			for (;;)
			{
				if ((LA(1)==IDENT))
				{
					x = LT(1);
					match(IDENT);
					result.Add(x.getText());
				}
				else
				{
					goto _loop57_breakloop;
				}
				
			}
_loop57_breakloop:			;
		}    // ( ... )*
		match(PIPE);
		return result;
	}
	
	private void initializeFactory()
	{
	}
	
	public static readonly string[] tokenNames_ = new string[] {
		@"""<0>""",
		@"""EOF""",
		@"""<2>""",
		@"""NULL_TREE_LOOKAHEAD""",
		@"""LITERAL""",
		@"""ESCAPE""",
		@"""SEMI""",
		@"""COLON_EQUALS""",
		@"""AT""",
		@"""AND""",
		@"""OR""",
		@"""EQUALS""",
		@"""NOT_EQUALS""",
		@"""LESS""",
		@"""LESSEQ""",
		@"""GREATER""",
		@"""GREATEREQ""",
		@"""PLUS""",
		@"""MINUS""",
		@"""DIV""",
		@"""MUL""",
		@"""NOT""",
		@"""INT""",
		@"""IDENT""",
		@"""STRING""",
		@"""CHAR""",
		@"""LPAREN""",
		@"""RPAREN""",
		@"""DOT""",
		@"""COMMA""",
		@"""COLON""",
		@"""LBRACK""",
		@"""RBRACK""",
		@"""PIPE""",
		@"""LBRACE""",
		@"""RBRACE""",
		@"""SLITERAL""",
		@"""STLITERAL""",
		@"""GOBBLE""",
		@"""COMMENT""",
		@"""ESC_CHAR""",
		@"""WS""",
		@"""NEWLINE"""
	};
	
	private static long[] mk_tokenSet_0_()
	{
		long[] data = { 4187904L, 0L};
		return data;
	}
	public static readonly BitSet tokenSet_0_ = new BitSet(mk_tokenSet_0_());
	private static long[] mk_tokenSet_1_()
	{
		long[] data = { 19459735552L, 0L};
		return data;
	}
	public static readonly BitSet tokenSet_1_ = new BitSet(mk_tokenSet_1_());
	private static long[] mk_tokenSet_2_()
	{
		long[] data = { 67108863920L, 0L};
		return data;
	}
	public static readonly BitSet tokenSet_2_ = new BitSet(mk_tokenSet_2_());
	private static long[] mk_tokenSet_3_()
	{
		long[] data = { 67645734832L, 0L};
		return data;
	}
	public static readonly BitSet tokenSet_3_ = new BitSet(mk_tokenSet_3_());
	
}
}
