#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Collections;

namespace Nen.Text.StringTemplate {
    public sealed class TemplateUtilities {
        public static string PrettyString (object val) {
            if (val is IEnumerable) {
                StringBuilder sb = new StringBuilder();
                foreach (object ent in (IEnumerable) val) {
                    sb.Append(PrettyString(ent));
                }
                return sb.ToString();
            } else if (val == null) {
                return "";
            } else {
                return val.ToString();
            }
        }

        public static object ApplyObject (object val, TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            if (val is Function)
                return ((Function) val).Apply(e, args, keywords);
            else if (val is IList)
                return ((IList) val)[(int) args[0]];
            else if (val is IDictionary)
                return ((IDictionary) val)[args[0]];
            else if (val is string)
                return ((string) val)[(int) args[0]];
            else
                throw new NonFunctionException(val);
        }

        public static object ApplyObject (object val, TemplateEnvironment e) {
            return ApplyObject(val, e, new List<object>(), new Dictionary<string, object>());
        }

        public static object ApplyUnary (object val, TemplateEnvironment e, object arg) {
            Dictionary<string, object> keys = new Dictionary<string, object>();
            keys.Add("it", arg);
            List<object> args = new List<object>();
            args.Add(arg);

            return ApplyObject(val, e, args, keys);
        }

        public static void LoadTemplates (TemplateEnvironment env, string file) {
            using (StreamReader st = new StreamReader(file)) {
                TemplateLexer lexer = new TemplateLexer(st);
                lexer.StartExpression();
                TemplateParser parser = new TemplateParser(lexer);
                Expression expr = parser.expr_file();

                expr.Evaluate(env);
            }
        }

        public static TemplateEnvironment LoadTemplates (string file) {
            TemplateEnvironment env = TemplateBuiltins.CreateEnvironment();
            LoadTemplates(env, file);
            return env;
        }

        public static TemplateFunction LoadTemplate (TemplateEnvironment e, string file) {
            using (StreamReader st = new StreamReader(file)) {
                TemplateLexer lexer = new TemplateLexer(st);
                TemplateParser parser = new TemplateParser(lexer);

                Expression expr = parser.template();
                return expr.Evaluate(e) as TemplateFunction;
            }
        }

        public static TemplateFunction LoadTemplateString (TemplateEnvironment e, string str) {
            TemplateLexer lexer = new TemplateLexer(new StringReader(str));
            TemplateParser parser = new TemplateParser(lexer);

            Expression expr = parser.template();
            return expr.Evaluate(e) as TemplateFunction;
        }

        public static string ApplyStringTemplate (TemplateEnvironment e, string template, IDictionary<string, object> values) {
            return (string) LoadTemplateString(e, template).Apply(e, new List<object>(), values);
        }
    }
}
