header {
    using System.Collections.Generic;
}

options {
    language="CSharp";
    namespace="Nen.Text.StringTemplate";
}

class TemplateParser extends Parser;
options {k=2; defaultErrorHandler=false;}
{
    private ApplyExpression apply (string op, params Expression[] args) {
        return new ApplyExpression(new VariableExpression(op), args);
    }
    
    private ValueExpression lit (object value) {
        return new ValueExpression(value);
    }
}

template returns [TemplateExpression result = new TemplateExpression()]
{Expression e;}
    : (l:LITERAL {result.AddLiteral(l.getText());} 
      |ESCAPE (e=expr_group {result.AddExpr(e);}) ESCAPE)*
    ;

// Don't ask.
expr_file returns [Expression result]
    : result=expr_group
    ;

expr_group returns [Expression result=null] {Expression exp;}
    : result=expr (SEMI^ (exp=expr_group {result = new SeqExpression(result,exp);}))?
    | {result = lit(null);}
    ;

expr returns [Expression result=null]
    : result=assign_expr 
    ;

assign_expr returns [Expression result=null] {Expression e;}
    : result=compose_expr 
      (COLON_EQUALS e=assign_expr {result = new AssignExpression(result, e);})?
    ;

compose_expr returns [Expression result=null] {Expression e;}
    : result=logior_expr
      (AT e=compose_expr {result = apply("@", result, e);})?
    ;
    
logior_expr returns [Expression result=null] {Expression e;}
    : result=logand_expr
      (AND e=logand_expr {result = apply("&&", result, e);})*
    ;
    
logand_expr returns [Expression result=null] {Expression e;}
    : result=compare_expr
      (OR e=compare_expr {result = apply("||", result, e);})*
    ;

compare_expr returns [Expression result=null] {Expression e; string op;}
    : result=relational_expr
      (op=compare_op e=relational_expr {result = apply(op, result, e);})*
    ;
compare_op returns [string rv=null]
    : EQUALS {rv="=";}
    | NOT_EQUALS {rv="!=";}
    ;
    
relational_expr returns [Expression result=null] {Expression e; string op;}
    : result=additive_expr
      (op=rel_op e=additive_expr {result = apply(op, result, e);})* 
    ;
rel_op returns [string rv=null]
    : LESS {rv="<";}
    | LESSEQ {rv="<=";}
    | GREATER {rv=">";}
    | GREATEREQ {rv=">=";}
    ;

additive_expr returns [Expression result=null] {Expression e; string op;}
    : result=multiplicative_expr
      (op=add_op e=multiplicative_expr {result = apply(op, result, e);})*
    ;
add_op returns [string rv=null]
    : PLUS {rv="+";}
    | MINUS {rv="-";}
    ;
    
multiplicative_expr returns [Expression result=null] {Expression e; string op;}
    : result=unary_expr
      (op=mul_op e=unary_expr {result = apply(op, result, e);})*
    ;
mul_op returns [string rv=null]
    : DIV {rv="/";}
    | MUL {rv="*";}
    ;
    
unary_expr returns [Expression result=null] {Expression exp; string op;}
    : result=primary_expr
    | op=unary_op exp=unary_expr {result = apply(op, exp);}
    ;
unary_op returns [string rv=null]
    : NOT {rv="!";}
    | MINUS {rv="neg";}
    ;

op_literal returns [Expression result=null] {string op;}
    : (op=rel_op|op=mul_op|AT {op="@";}|op=add_op|NOT {op="!";}|AND {op="&&";}
      |OR {op="||";})
      {result = new VariableExpression(op);}
    ;
    
data_expr returns [Expression result=null]
    : i:INT { result = lit(Int32.Parse(i.getText())); } 
    | id:IDENT { result = new VariableExpression(id.getText()); }
    | s:STRING { result = lit(s.getText()); }
    | c:CHAR { result = lit(c.getText()[0]); }
    | LPAREN (result=op_literal|result=expr) RPAREN 
    | result=block_expr
    | result=template_expr
    ;
    
primary_expr returns [Expression result=null] {List<Expression> a;}
    : result=data_expr 
      ( (a=apply_expr {result = new ApplyExpression(result,a);})
      | (DOT x:IDENT {result = new FieldRefExpression(result, x.getText());})
      )*
    ;

apply_expr returns [List<Expression> result=null]
    : LPAREN RPAREN {result = new List<Expression>();}
    | LPAREN (result=argument_list) RPAREN ;

argument_list returns [List<Expression> result = new List<Expression>()]
{Expression x;}
    : x=argument {result.Add(x);} (COMMA! x=argument {result.Add(x);})*
    ;
    
argument returns [Expression result=null] {Expression e;}
    : x:IDENT COLON e=expr {result = new BindExpression(x.getText(), e);}
    | result=expr
    ;
    
block_expr returns [Expression result=null]
{List<string> args=null; Expression exp;}
    : LBRACK (args=formals_list)? exp=expr_group RBRACK
      {result = new BlockExpression(args, exp);}
    ;
    
formals_list returns [List<string> result=new List<string>()]
    : PIPE (x:IDENT {result.Add(x.getText());})* PIPE ;
    
template_expr returns [TemplateExpression result=null]
{List<string> args=null;}
    : LBRACE (args=formals_list)? result=template RBRACE
      {if (args != null) result.Arguments = args;}
    ;
    
class TemplateLexer extends Lexer;
options {
    k = 2;
    charVocabulary = '\u0000'..'\uFFFE';
    defaultErrorHandler = false;
}

{
// WARNING: This is stupid. Too bad ANTLR can't do anything right.
private int _expr = 0;
private enum SubState { Start, Args, Gobble, Body }
private SubState _st;
private bool expr() { return _expr % 2 == 1;}
private bool sub()  { return !expr() && _expr > 0;}
private bool body() { return _st == SubState.Body; }
private bool start() { return _st == SubState.Start; }
private bool gobble() { return _st == SubState.Gobble; }
private bool args() {return _st == SubState.Args; }
private void nextSubState() {
    if (!sub())
        return;

    if (start())
        _st = SubState.Args;
    else if (args())
        _st = SubState.Gobble;
}

private void gosub() { _expr++; _st = SubState.Start; }

public void StartExpression () { _expr = 1; }
public void StartTemplate () {
    _expr = 0; 
    _st = SubState.Body; 
}

}

LITERAL : {!expr() && !sub()}? (ESC_CHAR|NEWLINE|~('\\'|'$'|!'\r'|'\n'))+ ;
    
SLITERAL 
    : {sub() && body()}? (ESC_CHAR|NEWLINE|~('\\'|'$'|!'\r'|'\n'|'}'))+ 
      {$setType(LITERAL);}
    ;
    
STLITERAL
    : {sub() && start()}? (ESC_CHAR|~('\\'|'$'|' '|'\t'|!'\r'|'\n'|'|'|'}'))+
      {$setType(LITERAL); _st = SubState.Body;}
    ;
    
GOBBLE
    : {sub() && gobble()}? (NEWLINE|' '|'\t')* {$setType(Token.SKIP); _st = SubState.Body;}
    ;
    
ESCAPE : '$' 
    { _st = SubState.Body;
        if(expr())
            _expr--;
        else 
            _expr++;
    } ;

LBRACE : {expr()}? '{' {gosub();} ;
    
RBRACE : {sub()}? '}' {_expr--; _st = SubState.Body;} ;

INT : {expr()}? ('0'..'9')+ ;
STRING: {expr()}? '"'! (ESC_CHAR|~'"')* '"'! ; // "
CHAR: {expr()}? '\''! ~'\'' '\''! ;
COMMENT
    : {expr()}? "//" (~('\n'|'\r'))* 
      {$setType(Token.SKIP);}
    | {expr()}? "/*" (options { greedy=false; }: NEWLINE | .)* "*/"
      {$setType(Token.SKIP);}
    ;

protected ESC_CHAR: '\\'! . ;
SEMI: {expr()}? ";";
AT: {expr()}? "@";
COLON_EQUALS: {expr()}? ":=";
PLUS: {expr()}? "+";
MINUS: {expr()}? "-";
LESS: {expr()}? "<";
LESSEQ: {expr()}? "<=";
GREATER: {expr()}? ">";
GREATEREQ: {expr()}? ">=";
EQUALS: {expr()}? "=";
NOT_EQUALS: {expr()}? "!=";
MUL: {expr()}? "*";
DIV: {expr()}? "/";
NOT: {expr()}? "!";
LPAREN: {expr()}? "(";
RPAREN: {expr()}? ")";
COMMA: {expr()}? ",";
LBRACK: {expr()}? "[";
RBRACK: {expr()}? "]";
AND: {expr()}? "&&";
OR:  {expr()}? "||";
PIPE: {expr() || (sub() && (start() || args()))}? "|" {nextSubState();};
COLON: {expr()}? ":";
DOT: {expr()}? ".";
IDENT: {expr() || (sub() && args())}? ('A' .. 'Z' | 'a'..'z' |'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;

WS : {expr() || (sub() && !body())}? (NEWLINE|' '|'\t')+ {$setType(Token.SKIP);} ;
protected
NEWLINE : (!'\r')? '\n' {newline();} ;
