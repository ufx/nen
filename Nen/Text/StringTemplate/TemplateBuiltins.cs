#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Nen.Text.StringTemplate {
    public class TemplateBuiltins {
        public static TemplateEnvironment CreateEnvironment () {
            return CreateEnvironment(null);
        }

        public static TemplateEnvironment CreateEnvironment (TemplateEnvironment parent) {
            TemplateEnvironment e = new TemplateEnvironment(parent);

            e.Extend("@", new DelegateFunction(2, new FunctionDelegate(BuiltinCompose)));
            e.Extend("+", new DelegateFunction(2, new FunctionDelegate(BuiltinAdd)));
            e.Extend("-", new DelegateFunction(2, new FunctionDelegate(BuiltinSub)));
            e.Extend("*", new DelegateFunction(2, new FunctionDelegate(BuiltinMul)));
            e.Extend("/", new DelegateFunction(2, new FunctionDelegate(BuiltinDiv)));
            e.Extend("neg", new DelegateFunction(1, new FunctionDelegate(BuiltinNeg)));
            e.Extend("round", new DelegateFunction(1, BuiltinRound));
            e.Extend("=", new DelegateFunction(2, new FunctionDelegate(BuiltinEql)));
            e.Extend("!=", new DelegateFunction(2, new FunctionDelegate(BuiltinNotEql)));
            e.Extend("<", new DelegateFunction(2, new FunctionDelegate(BuiltinLt)));
            e.Extend("<=", new DelegateFunction(2, new FunctionDelegate(BuiltinLe)));
            e.Extend(">", new DelegateFunction(2, new FunctionDelegate(BuiltinGt)));
            e.Extend(">=", new DelegateFunction(2, new FunctionDelegate(BuiltinGe)));
            e.Extend("!", new DelegateFunction(1, new FunctionDelegate(BuiltinNot)));
            e.Extend("&&", new DelegateFunction(2, new FunctionDelegate(BuiltinAnd)));
            e.Extend("||", new DelegateFunction(2, new FunctionDelegate(BuiltinOr)));
            e.Extend("if", new DelegateFunction(0, new FunctionDelegate(BuiltinIf)));
            e.Extend("switch", new DelegateFunction(0, new FunctionDelegate(BuiltinSwitch)));
            e.Extend("list", new DelegateFunction(0, new FunctionDelegate(BuiltinList)));
            e.Extend("dict", new DelegateFunction(0, new FunctionDelegate(BuiltinDict)));
            e.Extend("array", new DelegateFunction(0, new FunctionDelegate(BuiltinArray)));
            e.Extend("apply", new DelegateFunction(2, new FunctionDelegate(BuiltinApply)));
            e.Extend("map", new DelegateFunction(2, new FunctionDelegate(BuiltinMap)));
            e.Extend("unwind_protect", new DelegateFunction(3, new FunctionDelegate(BuiltinUnwindProtect)));
            e.Extend("bind", new DelegateFunction(0, new FunctionDelegate(BuiltinBind)));
            e.Extend("bold", new DelegateFunction(0, new FunctionDelegate(BuiltinBold)));
            e.Extend("sep", new DelegateFunction(2, new FunctionDelegate(BuiltinSep)));
            e.Extend("wordtable", new DelegateFunction(2, new FunctionDelegate(BuiltinWordtable)));
            e.Extend("time", new DelegateFunction(0, new FunctionDelegate(BuiltinTime)));

            e.Extend("true", true);
            e.Extend("false", false);
            e.Extend("null", null);

            return e;
        }

        private static bool IsTrue (object rv) {
            return !((rv is bool && (bool) rv == false) || rv == null);
        }

        #region Arithmetic
        public static object BuiltinAdd (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            if (args[0] is string)
                return (string) args[0] + (string) args[1];
            else
                return GenericMath.Add(args[0], args[1]);
        }

        public static object BuiltinSub (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return GenericMath.Subtract(args[0], args[1]);
        }

        public static object BuiltinMul (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return GenericMath.Multiply(args[0], args[1]);
        }

        public static object BuiltinDiv (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return GenericMath.Divide(args[0], args[1]);
        }

        public static object BuiltinNeg (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return GenericMath.Subtract(0, args[0]);
        }

        public static object BuiltinRound (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            if (args[0] is decimal)
                return Math.Round((decimal) args[0], MidpointRounding.ToEven);
            else if (args[0] is double)
                return Math.Round((double) args[0], MidpointRounding.ToEven);
            else if (args[0] is float)
                return Math.Round((float) args[0], MidpointRounding.ToEven);
            throw new ArithmeticException("Invalid numeric type.");
        }
        #endregion

        #region Relational
        public static object BuiltinEql (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            //return args[0].Equals(args[1]);
            //this was failing if args[0] was null
            return Equals(args[0], args[1]);
        }

        public static object BuiltinNotEql (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            //return !args[0].Equals(args[1]);
            //this was failing if args[0] was null
            return !Equals(args[0], args[1]);
        }

        public static object BuiltinLt (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return ((IComparable) args[0]).CompareTo(args[1]) < 0;
        }

        public static object BuiltinLe (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return ((IComparable) args[0]).CompareTo(args[1]) <= 0;
        }

        public static object BuiltinGt (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return ((IComparable) args[0]).CompareTo(args[1]) > 0;
        }

        public static object BuiltinGe (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return ((IComparable) args[0]).CompareTo(args[1]) >= 0;
        }
        #endregion

        #region Logical
        public static object BuiltinNot (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return !IsTrue(args[0]);
        }

        public static object BuiltinAnd (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return IsTrue(args[0]) && IsTrue(args[1]);
        }

        public static object BuiltinOr (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return IsTrue(args[0]) || IsTrue(args[1]);
        }
        #endregion

        #region Conditional
        public static object BuiltinIf (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            if (!IsTrue(args[0])) {
                if (args.Count > 2)
                    return TemplateUtilities.ApplyObject(args[2], e);
                else
                    return null;
            } else {
                return TemplateUtilities.ApplyObject(args[1], e);
            }
        }

        public static object BuiltinSwitch (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            for (int i = 0; i < args.Count; i += 2) {
                object rv = TemplateUtilities.ApplyObject(args[i], e);
                if (IsTrue(rv))
                    return TemplateUtilities.ApplyObject(args[i + 1], e);
            }

            if (keywords.ContainsKey("otherwise"))
                return TemplateUtilities.ApplyObject(keywords["otherwise"], e);
            else
                return null;
        }
        #endregion

        #region Data Constructors
        public static object BuiltinList (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return args;
        }

        public static object BuiltinDict (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return keywords;
        }

        public static object BuiltinArray (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return args.ToArray();
        }
        #endregion

        #region Higher-Order Functions
        public static object BuiltinApply (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            List<object> f_args = (List<object>) args[1];
            IDictionary<string, object> keys;
            if (args.Count > 2)
                keys = (IDictionary<string, object>) args[2];
            else
                keys = new Dictionary<string, object>();

            return TemplateUtilities.ApplyObject(args[0], e, f_args, keys);
        }

        public static object BuiltinMap (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            List<object> rv = new List<object>();

            foreach (object o in (IEnumerable) args[1]) {
                IDictionary<string, object> d = new Dictionary<string, object>();
                d.Add("it", o);
                rv.Add(TemplateUtilities.ApplyObject(args[0], e, new List<object>(new object[] { o }), d));
            }

            return rv;
        }

        public static object BuiltinUnwindProtect (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            object rv = null;

            try {
                rv = TemplateUtilities.ApplyObject(args[0], e);
            } finally {
                TemplateUtilities.ApplyObject(args[1], e);
            }

            return rv;
        }

        public static object BuiltinBind (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            Dictionary<string, object> old = new Dictionary<string, object>();
            object rv;
            foreach (KeyValuePair<string, object> d in keywords) {
                old.Add(d.Key, e[(string) d.Key]);
                e[(string) d.Key] = d.Value;
            }

            try {
                rv = TemplateUtilities.ApplyObject(args[0], e);
            } finally {
                foreach (KeyValuePair<string, object> d in old) {
                    e[(string) d.Key] = d.Value;
                }
            }

            return rv;
        }

        public static object BuiltinCompose (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return new ComposedFunction((Function) args[0], (Function) args[1]);
        }
        #endregion

        #region String Manipulation
        public static object BuiltinBold (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            StringBuilder sb = new StringBuilder("<b>");
            IList l;

            l = (IList) args;

            for (int i = 0; i < l.Count; ++i) {
                sb.Append(TemplateUtilities.PrettyString(((TemplateFunction) l[i]).Apply(e)));
            }

            sb.Append("</b>");
            return sb.ToString();
        }

        public static object BuiltinSep (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            StringBuilder sb = new StringBuilder();
            IList l;
            string delim, final;

            l = (IList) args[1];
            delim = final = TemplateUtilities.PrettyString(args[0]);

            if (keywords.ContainsKey("terminator"))
                final = TemplateUtilities.PrettyString(keywords["terminator"]);

            for (int i = 0; i < l.Count - 1; ++i) {
                sb.Append(TemplateUtilities.PrettyString(l[i]));
                sb.Append(i == l.Count - 2 ? final : delim);
            }

            if (l.Count > 0)
                sb.Append(TemplateUtilities.PrettyString(l[l.Count - 1]));

            return sb.ToString();
        }

        public static object BuiltinWordtable (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            StringBuilder buf = new StringBuilder(), lineBuf = new StringBuilder();
            int max = 0, len = 0;
            IList src = null;
            object eachRow = null, eachCol = null;

            src = (IList) args[0];
            len = (int) args[1];

            if (keywords.ContainsKey("min"))
                max = (int) keywords["min"];

            if (keywords.ContainsKey("eachRow"))
                eachRow = keywords["eachRow"];

            if (keywords.ContainsKey("eachCol"))
                eachCol = keywords["eachCol"];

            foreach (string i in (IList) src) {
                if (i.Length > max)
                    max = i.Length;
            }

            foreach (string i in (IList) src) {
                string col = i;

                if (eachCol != null)
                    col = (string) TemplateUtilities.ApplyUnary(eachCol, e, col);

                if (lineBuf.Length + max > len) {
                    string line = lineBuf.ToString();

                    if (eachRow != null) {
                        line = (string) TemplateUtilities.ApplyUnary(eachRow, e, line);
                    }

                    buf.Append(line);
                    buf.Append("\n");
                    lineBuf.Length = 0;
                }

                lineBuf.Append(col.PadRight(max, ' '));
                lineBuf.Append(" ");
            }

            if (lineBuf.Length > 0) {
                string line = lineBuf.ToString();

                if (eachRow != null)
                    line = (string) TemplateUtilities.ApplyUnary(eachRow, e, line);

                buf.Append(line);
                buf.Append("\n");
            }

            return buf.ToString();
        }
        #endregion

        #region Time and Date
        public static object BuiltinTime (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            return DateTime.Now;
        }
        #endregion
    }
}
