# Generates a static class with generic math operations which operate on any type.

$INDENT = 0
$TYPECODES = { 
    "Byte" => "byte", 
    "Decimal" => "decimal", 
    "Double" => "double",
    "Int16" => "short",
    "Int32" => "int",
    "Int64" => "long",
    "UInt16" => "ushort",
    "UInt32" => "uint",
    "UInt64" => "ulong"
}

def indent (&code)
    $INDENT = $INDENT + 1
    yield
    $INDENT = $INDENT - 1
end

def line (text); puts "#{"    " * $INDENT}#{text}"; end
def indent_line (text); indent { line text }; end

def block (opener, &code)
    line opener + " {"
    indent { yield }
    line "}"
end

def invalid_type ()
    line "throw new ArgumentException(\"Invalid numeric type.\");"
end

def invalid_combination (tc1, tc2)
    return (tc1[0] == 'U'[0] && tc2[0] != 'U'[0]) || 
           (tc2[0] == 'U'[0] && tc1[0] != 'U'[0]) ||
           (tc1 == "Double" && tc2 == "Decimal")  ||
           (tc2 == "Double" && tc1 == "Decimal")
end

def each_typecode (val, &code)
    block("switch (#{val})") {
        $TYPECODES.each_key { |key|
            yield key
        }
        
        line "default: throw new ArgumentException(\"Invalid numeric type.\");"
    }
end

def print_generic_operator (op, name)
    block("public static object #{name} (object lhs, object rhs)") {
        each_typecode("Type.GetTypeCode(lhs.GetType())") { |tc1|
            line "case TypeCode.#{tc1}:"
            indent {
                each_typecode("Type.GetTypeCode(rhs.GetType())") { |tc2|
                    if !invalid_combination(tc1, tc2) then
                        line "case TypeCode.#{tc2}:"
                        indent_line "return (#{$TYPECODES[tc1]})lhs #{op} (#{$TYPECODES[tc2]})rhs;"
                    end
                }
            }
        }
    }
end

def print_generic_math_class (ns_name, class_name)
    line "// MACHINE GENERATED -- DO NOT MODIFY (see generic_math.rb)"
    line "using System;"
    line ""
    block("namespace #{ns_name}") {
        block("internal static class #{class_name}") {
            print_generic_operator("+", "Add")
            print_generic_operator("-", "Subtract")
            print_generic_operator("*", "Multiply")
            print_generic_operator("/", "Divide")
        }
    }
end

print_generic_math_class("Nen.Text.StringTemplate", "GenericMath")
