#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Nen.Text.StringTemplate {
    public abstract class Function {
        public abstract object Apply (TemplateEnvironment dynamicEnv, List<object> args, IDictionary<string, object> keywords);
        public object Apply (TemplateEnvironment dynamicEnv) {
            return Apply(dynamicEnv, new List<object>(), new Dictionary<string, object>());
        }
    }

    public abstract class CurryFunction : Function {
        public CurryFunction (int arity) {
            Arity = arity;
        }

        public override object Apply (TemplateEnvironment e, List<object> args, IDictionary<string, object> keywords) {
            if (args.Count >= Arity) {
                if (BoundArgs != null)
                    args.InsertRange(0, BoundArgs);
                if (BoundKeys != null)
                    foreach (KeyValuePair<string, object> d in BoundKeys)
                        keywords[d.Key] = d.Value;

                return Execute(e, args, keywords);
            }

            CurryFunction c = Clone();
            c.Arity = Arity - args.Count;

            if (BoundArgs != null && args.Count > 0) {
                c.BoundArgs = new List<object>(BoundArgs);
                c.BoundArgs.AddRange(args);
            } else if (BoundArgs != null) {
                c.BoundArgs = BoundArgs;
            } else {
                c.BoundArgs = args;
            }

            if (BoundKeys != null && keywords.Count > 0) {
                c.BoundKeys = new Dictionary<string, object>();
                foreach (KeyValuePair<string, object> d in BoundKeys)
                    c.BoundKeys[d.Key] = d.Value;
                foreach (KeyValuePair<string, object> d in keywords)
                    c.BoundKeys[d.Key] = d.Value;
            } else if (BoundKeys != null) {
                c.BoundKeys = BoundKeys;
            } else {
                c.BoundKeys = keywords;
            }

            return c;
        }

        protected abstract CurryFunction Clone ();
        protected abstract object Execute (TemplateEnvironment e, List<object> args, IDictionary<string, object> keys);

        public int Arity { get; protected set; }
        protected IDictionary<string, object> BoundKeys { get; set; }
        protected List<object> BoundArgs { get; set; }
    }

    public abstract class InterpretedFunction : CurryFunction {
        public InterpretedFunction (TemplateEnvironment env, List<string> args)
            : base(args != null ? args.Count : 0) {
            Env = env;
            Args = args;
        }

        protected override object Execute (TemplateEnvironment dynamicEnv, List<object> args, IDictionary<string, object> keywords) {
            TemplateEnvironment frame = new TemplateEnvironment(Env);

            foreach (KeyValuePair<string, object> d in keywords) {
                frame.Extend(d.Key, d.Value);
            }

            if (Args != null) {
                for (int i = 0; i < args.Count && i < Args.Count; ++i)
                    frame.Extend(Args[i], args[i]);
            }

            return Execute(frame);
        }

        protected abstract object Execute (TemplateEnvironment e);

        protected TemplateEnvironment Env { get; set; }
        protected List<string> Args { get; set; }
    }

    public class BlockFunction : InterpretedFunction {
        private Expression _exp;

        public BlockFunction (TemplateEnvironment env, List<string> args, Expression exp)
            : base(env, args) {
            _exp = exp;
        }

        protected override object Execute (TemplateEnvironment e) {
            return _exp.Evaluate(e);
        }

        protected override CurryFunction Clone () {
            return new BlockFunction(Env, Args, _exp);
        }
    }

    public class TemplateFunction : InterpretedFunction {
        private List<Expression> _chunks;

        public TemplateFunction (TemplateEnvironment env, List<string> args, List<Expression> chunks)
            : base(env, args) {
            _chunks = chunks;
        }

        protected override object Execute (TemplateEnvironment e) {
            StringBuilder sb = new StringBuilder();

            foreach (Expression exp in _chunks) {
                object rv = exp.Evaluate(e);
                sb.Append(TemplateUtilities.PrettyString(rv));
            }

            return sb.ToString();
        }

        protected override CurryFunction Clone () {
            return new TemplateFunction(Env, Args, _chunks);
        }
    }

    public class MethodFunction : Function {
        private object _obj;
        private string _name;

        public MethodFunction (object obj, string name) {
            _obj = obj;
            _name = name;
        }

        public override object Apply (TemplateEnvironment dynamicEnv, List<object> args, IDictionary<string, object> keywords) {
            return _obj.GetType().InvokeMember(_name, BindingFlags.InvokeMethod, null, _obj, args.ToArray());
        }
    }

    public delegate object FunctionDelegate (TemplateEnvironment env, List<object> args, IDictionary<string, object> keywords);
    public class DelegateFunction : CurryFunction {
        private FunctionDelegate _delegate;
        public DelegateFunction (int arity, FunctionDelegate del)
            : base(arity) {
            _delegate = del;
        }

        protected override object Execute (TemplateEnvironment env, List<object> args, IDictionary<string, object> keywords) {
            return _delegate(env, args, keywords);
        }

        protected override CurryFunction Clone () {
            return new DelegateFunction(Arity, _delegate);
        }
    }

    public class ComposedFunction : CurryFunction {
        private Function _lhs;
        private Function _rhs;

        public ComposedFunction (Function lhs, Function rhs)
            : base(0) {
            if (lhs is CurryFunction)
                Arity = ((CurryFunction) lhs).Arity;
            _lhs = lhs;
            _rhs = rhs;
        }

        protected override object Execute (TemplateEnvironment dynamicEnv, List<object> args, IDictionary<string, object> keywords) {
            object rv = _lhs.Apply(dynamicEnv, args, keywords);
            return _rhs.Apply(dynamicEnv, new List<object>(new object[] { rv }), new Dictionary<string, object>());
        }

        protected override CurryFunction Clone () {
            return new ComposedFunction(_lhs, _rhs);
        }
    }
}
