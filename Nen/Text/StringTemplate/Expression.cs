#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Nen.Text.StringTemplate {
    public abstract class Expression {
        public abstract object Evaluate (TemplateEnvironment e);
    }

    public class ApplyExpression : Expression {
        private Expression _operator;
        private List<Expression> _arguments = new List<Expression>();

        public ApplyExpression (Expression op, params Expression[] args) {
            _operator = op;
            _arguments.AddRange(args);
        }

        public ApplyExpression (Expression op, List<Expression> args) {
            _operator = op;
            _arguments = args;
        }

        public override object Evaluate (TemplateEnvironment e) {
            List<object> args = new List<object>();
            Dictionary<string, object> keywords = new Dictionary<string, object>();

            if (_arguments != null) {
                foreach (Expression exp in _arguments) {
                    if (exp is BindExpression) {
                        BindExpression b = (BindExpression) exp;
                        keywords.Add(b.Variable, b.Value.Evaluate(e));
                    } else {
                        args.Add(exp.Evaluate(e));
                    }
                }
            }

            object val = _operator.Evaluate(e);
            return TemplateUtilities.ApplyObject(val, e, args, keywords);
        }

        public override string ToString () {
            StringBuilder sb = new StringBuilder();

            sb.Append("\n<apply><op>" + _operator.ToString() + "</op>\n");
            if (_arguments != null)
                foreach (Expression e in _arguments) {
                    sb.Append("\t" + e.ToString() + "\n");
                }
            sb.Append("</apply>\n");

            return sb.ToString();
        }
    }

    public class AssignExpression : Expression {
        public AssignExpression (Expression var, Expression value) {
            if (!(var is VariableExpression))
                throw new TemplateException("Tried to assign to non-variable {0}", var);

            Var = var;
            Value = value;
        }

        public override object Evaluate (TemplateEnvironment e) {
            object val = Value.Evaluate(e);
            e[((VariableExpression) Var).Variable] = val;
            return val;
        }

        public Expression Var { get; set; }
        public Expression Value { get; set; }
    }

    public class BindExpression : Expression {
        public string Variable;
        public Expression Value;

        public BindExpression (string var, Expression val) {
            Variable = var;
            Value = val;
        }

        public override object Evaluate (TemplateEnvironment e) {
            return Value.Evaluate(e);
        }
    }

    public class TemplateExpression : Expression {
        private List<Expression> _chunks = new List<Expression>();
        private List<string> _args = new List<string>();

        public override object Evaluate (TemplateEnvironment e) {
            return new TemplateFunction(e, _args, _chunks);
        }

        public void AddLiteral (string str) {
            _chunks.Add(new ValueExpression(str));
        }

        public void AddExpr (Expression e) {
            _chunks.Add(e);
        }

        public override string ToString () {
            StringBuilder sb = new StringBuilder();

            sb.Append("\n<template>\n");
            foreach (Expression e in _chunks) {
                sb.Append("\t" + e.ToString() + "\n");
            }
            sb.Append("</template>\n");

            return sb.ToString();
        }

        public List<string> Arguments {
            get { return _args; }
            set { _args = value; }
        }
    }

    public class BlockExpression : Expression {
        private List<string> _args;
        private Expression _expr;

        public BlockExpression (List<string> args, Expression expr) {
            _args = args;
            _expr = expr;
        }

        public override object Evaluate (TemplateEnvironment e) {
            return new BlockFunction(e, _args, _expr);
        }
    }

    public class ValueExpression : Expression {
        private object _value;

        public ValueExpression (object value) {
            _value = value;
        }

        public override object Evaluate (TemplateEnvironment e) {
            return _value;
        }

        public override string ToString () {
            return "val: " + (_value == null ? "null" : _value.ToString());
        }
    }

    public class VariableExpression : Expression {
        private string _var;

        public VariableExpression (string var) {
            _var = var;
        }

        public override object Evaluate (TemplateEnvironment e) {
            return e[_var];
        }

        public override string ToString () {
            return "var: " + _var;
        }

        public string Variable {
            get { return _var; }
        }
    }

    public class SeqExpression : Expression {
        private Expression _left;
        private Expression _right;

        public SeqExpression (Expression left, Expression right) {
            _left = left;
            _right = right;
        }

        public override object Evaluate (TemplateEnvironment e) {
            Expression cur = this;
            do {
                SeqExpression exp = (SeqExpression) cur;
                exp._left.Evaluate(e);
                cur = exp._right;
            } while (cur is SeqExpression);

            return cur.Evaluate(e);
        }
    }

    public class FieldRefExpression : Expression {
        private Expression _lhs;
        private string _field;

        public FieldRefExpression (Expression lhs, string field) {
            _lhs = lhs;
            _field = field;
        }

        public override object Evaluate (TemplateEnvironment e) {
            object val = _lhs.Evaluate(e);
            Type type = val.GetType();

            PropertyInfo pi = type.GetProperty(_field);

            if (pi != null)
                return pi.GetValue(val, null);

            FieldInfo fi = type.GetField(_field);

            if (fi != null)
                return fi.GetValue(val);

            MethodBase[] mi = type.GetMethods();
            bool found = false;
            foreach (MemberInfo member in mi) {
                if (member.Name == _field) {
                    found = true;
                    break;
                }
            }

            if (found)
                return new MethodFunction(val, _field);

            pi = type.GetProperty("Item");
            if (pi != null)
                return pi.GetValue(val, new object[] { _field });

            throw new UnboundFieldException(val, _field);

        }
    }
}
