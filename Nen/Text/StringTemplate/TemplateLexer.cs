// $ANTLR 2.7.5 (20050128): "parser.g" -> "TemplateLexer.cs"$

#pragma warning disable 1591

using System.Collections.Generic;

namespace Nen.Text.StringTemplate
{
	// Generate header specific to lexer CSharp file
	using System;
	using Stream                          = System.IO.Stream;
	using TextReader                      = System.IO.TextReader;
	using Hashtable                       = System.Collections.Hashtable;
	using Comparer                        = System.Collections.Comparer;
	
	using TokenStreamException            = antlr.TokenStreamException;
	using TokenStreamIOException          = antlr.TokenStreamIOException;
	using TokenStreamRecognitionException = antlr.TokenStreamRecognitionException;
	using CharStreamException             = antlr.CharStreamException;
	using CharStreamIOException           = antlr.CharStreamIOException;
	using ANTLRException                  = antlr.ANTLRException;
	using CharScanner                     = antlr.CharScanner;
	using InputBuffer                     = antlr.InputBuffer;
	using ByteBuffer                      = antlr.ByteBuffer;
	using CharBuffer                      = antlr.CharBuffer;
	using Token                           = antlr.Token;
	using IToken                          = antlr.IToken;
	using CommonToken                     = antlr.CommonToken;
	using SemanticException               = antlr.SemanticException;
	using RecognitionException            = antlr.RecognitionException;
	using NoViableAltForCharException     = antlr.NoViableAltForCharException;
	using MismatchedCharException         = antlr.MismatchedCharException;
	using TokenStream                     = antlr.TokenStream;
	using LexerSharedInputState           = antlr.LexerSharedInputState;
	using BitSet                          = antlr.collections.impl.BitSet;
	
    [CLSCompliant(false)]
	public 	class TemplateLexer : antlr.CharScanner	, TokenStream
	 {
		public const int EOF = 1;
		public const int NULL_TREE_LOOKAHEAD = 3;
		public const int LITERAL = 4;
		public const int ESCAPE = 5;
		public const int SEMI = 6;
		public const int COLON_EQUALS = 7;
		public const int AT = 8;
		public const int AND = 9;
		public const int OR = 10;
		public const int EQUALS = 11;
		public const int NOT_EQUALS = 12;
		public const int LESS = 13;
		public const int LESSEQ = 14;
		public const int GREATER = 15;
		public const int GREATEREQ = 16;
		public const int PLUS = 17;
		public const int MINUS = 18;
		public const int DIV = 19;
		public const int MUL = 20;
		public const int NOT = 21;
		public const int INT = 22;
		public const int IDENT = 23;
		public const int STRING = 24;
		public const int CHAR = 25;
		public const int LPAREN = 26;
		public const int RPAREN = 27;
		public const int DOT = 28;
		public const int COMMA = 29;
		public const int COLON = 30;
		public const int LBRACK = 31;
		public const int RBRACK = 32;
		public const int PIPE = 33;
		public const int LBRACE = 34;
		public const int RBRACE = 35;
		public const int SLITERAL = 36;
		public const int STLITERAL = 37;
		public const int GOBBLE = 38;
		public const int COMMENT = 39;
		public const int ESC_CHAR = 40;
		public const int WS = 41;
		public const int NEWLINE = 42;
		
		
// WARNING: This is stupid. Too bad ANTLR can't do anything right.
private int _expr = 0;
private enum SubState { Start, Args, Gobble, Body }
private SubState _st;
private bool expr() { return _expr % 2 == 1;}
private bool sub()  { return !expr() && _expr > 0;}
private bool body() { return _st == SubState.Body; }
private bool start() { return _st == SubState.Start; }
private bool gobble() { return _st == SubState.Gobble; }
private bool args() {return _st == SubState.Args; }
private void nextSubState() {
    if (!sub())
        return;

    if (start())
        _st = SubState.Args;
    else if (args())
        _st = SubState.Gobble;
}

private void gosub() { _expr++; _st = SubState.Start; }

public void StartExpression () { _expr = 1; }
public void StartTemplate () {
    _expr = 0; 
    _st = SubState.Body; 
}

		public TemplateLexer(Stream ins) : this(new ByteBuffer(ins))
		{
		}
		
		public TemplateLexer(TextReader r) : this(new CharBuffer(r))
		{
		}
		
		public TemplateLexer(InputBuffer ib)		 : this(new LexerSharedInputState(ib))
		{
		}
		
		public TemplateLexer(LexerSharedInputState state) : base(state)
		{
			initialize();
		}
		private void initialize()
		{
			caseSensitiveLiterals = true;
			setCaseSensitive(true);
			literals = new Hashtable(100, (float) 0.4, null);
		}
		
		override public IToken nextToken()			//throws TokenStreamException
		{
			IToken theRetToken = null;
tryAgain:
			for (;;)
			{
				int _ttype = Token.INVALID_TYPE;
				resetText();
				try     // for char stream error handling
				{
					try     // for lexical error handling
					{
						if (((cached_LA1=='"') && ((cached_LA2 >= '\u0000' && cached_LA2 <= '\ufffe')))&&(expr()))
						{
							mSTRING(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='\'') && (tokenSet_0_.member(cached_LA2)))&&(expr())) {
							mCHAR(true);
							theRetToken = returnToken_;
						}
						else if ((cached_LA1=='/') && (cached_LA2=='*'||cached_LA2=='/')) {
							mCOMMENT(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1==':') && (cached_LA2=='='))&&(expr())) {
							mCOLON_EQUALS(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='<') && (cached_LA2=='='))&&(expr())) {
							mLESSEQ(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='>') && (cached_LA2=='='))&&(expr())) {
							mGREATEREQ(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='!') && (cached_LA2=='='))&&(expr())) {
							mNOT_EQUALS(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='&') && (cached_LA2=='&'))&&(expr())) {
							mAND(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='|') && (cached_LA2=='|'))&&(expr())) {
							mOR(true);
							theRetToken = returnToken_;
						}
						else if (((tokenSet_1_.member(cached_LA1)) && (true))&&(!expr() && !sub())) {
							mLITERAL(true);
							theRetToken = returnToken_;
						}
						else if (((tokenSet_2_.member(cached_LA1)) && (true))&&(sub() && body())) {
							mSLITERAL(true);
							theRetToken = returnToken_;
						}
						else if (((tokenSet_3_.member(cached_LA1)) && (true))&&(sub() && start())) {
							mSTLITERAL(true);
							theRetToken = returnToken_;
						}
						else if ((cached_LA1=='$')) {
							mESCAPE(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='{') && (true))&&(expr())) {
							mLBRACE(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='}') && (true))&&(sub())) {
							mRBRACE(true);
							theRetToken = returnToken_;
						}
						else if ((((cached_LA1 >= '0' && cached_LA1 <= '9')) && (true))&&(expr())) {
							mINT(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1==';') && (true))&&(expr())) {
							mSEMI(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='@') && (true))&&(expr())) {
							mAT(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='+') && (true))&&(expr())) {
							mPLUS(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='-') && (true))&&(expr())) {
							mMINUS(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='<') && (true))&&(expr())) {
							mLESS(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='>') && (true))&&(expr())) {
							mGREATER(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='=') && (true))&&(expr())) {
							mEQUALS(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='*') && (true))&&(expr())) {
							mMUL(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='/') && (true))&&(expr())) {
							mDIV(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='!') && (true))&&(expr())) {
							mNOT(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='(') && (true))&&(expr())) {
							mLPAREN(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1==')') && (true))&&(expr())) {
							mRPAREN(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1==',') && (true))&&(expr())) {
							mCOMMA(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='[') && (true))&&(expr())) {
							mLBRACK(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1==']') && (true))&&(expr())) {
							mRBRACK(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='|') && (true))&&(expr() || (sub() && (start() || args())))) {
							mPIPE(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1==':') && (true))&&(expr())) {
							mCOLON(true);
							theRetToken = returnToken_;
						}
						else if (((cached_LA1=='.') && (true))&&(expr())) {
							mDOT(true);
							theRetToken = returnToken_;
						}
						else if (((tokenSet_4_.member(cached_LA1)) && (true))&&(expr() || (sub() && args()))) {
							mIDENT(true);
							theRetToken = returnToken_;
						}
						else if (((tokenSet_5_.member(cached_LA1)) && (true))&&(expr() || (sub() && !body()))) {
							mWS(true);
							theRetToken = returnToken_;
						}
						else if (( true )&&(sub() && gobble())) {
							mGOBBLE(true);
							theRetToken = returnToken_;
						}
						else
						{
							if (cached_LA1==EOF_CHAR) { uponEOF(); returnToken_ = makeToken(Token.EOF_TYPE); }
				else {throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());}
						}
						
						if ( null==returnToken_ ) goto tryAgain; // found SKIP token
						_ttype = returnToken_.Type;
						_ttype = testLiteralsTable(_ttype);
						returnToken_.Type = _ttype;
						return returnToken_;
					}
					catch (RecognitionException e) {
							throw new TokenStreamRecognitionException(e);
					}
				}
				catch (CharStreamException cse) {
					if ( cse is CharStreamIOException ) {
						throw new TokenStreamIOException(((CharStreamIOException)cse).io);
					}
					else {
						throw new TokenStreamException(cse.Message);
					}
				}
			}
		}
		
	public void mLITERAL(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = LITERAL;
		
		if (!(!expr() && !sub()))
		  throw new SemanticException("!expr() && !sub()");
		{ // ( ... )+
			int _cnt63=0;
			for (;;)
			{
				switch ( cached_LA1 )
				{
				case '\\':
				{
					mESC_CHAR(false);
					break;
				}
				case '\n':  case '\r':
				{
					mNEWLINE(false);
					break;
				}
				default:
					if ((tokenSet_6_.member(cached_LA1)))
					{
						{
							match(tokenSet_6_);
						}
					}
				else
				{
					if (_cnt63 >= 1) { goto _loop63_breakloop; } else { throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());; }
				}
				break; }
				_cnt63++;
			}
_loop63_breakloop:			;
		}    // ( ... )+
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	protected void mESC_CHAR(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = ESC_CHAR;
		
		int _saveIndex = 0;
		_saveIndex = text.Length;
		match('\\');
		text.Length = _saveIndex;
		matchNot(EOF/*_CHAR*/);
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	protected void mNEWLINE(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = NEWLINE;
		
		{
			switch ( cached_LA1 )
			{
			case '\r':
			{
				int _saveIndex = 0;
				_saveIndex = text.Length;
				match('\r');
				text.Length = _saveIndex;
				break;
			}
			case '\n':
			{
				break;
			}
			default:
			{
				throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());
			}
			 }
		}
		match('\n');
		newline();
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mSLITERAL(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = SLITERAL;
		
		if (!(sub() && body()))
		  throw new SemanticException("sub() && body()");
		{ // ( ... )+
			int _cnt67=0;
			for (;;)
			{
				switch ( cached_LA1 )
				{
				case '\\':
				{
					mESC_CHAR(false);
					break;
				}
				case '\n':  case '\r':
				{
					mNEWLINE(false);
					break;
				}
				default:
					if ((tokenSet_7_.member(cached_LA1)))
					{
						{
							match(tokenSet_7_);
						}
					}
				else
				{
					if (_cnt67 >= 1) { goto _loop67_breakloop; } else { throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());; }
				}
				break; }
				_cnt67++;
			}
_loop67_breakloop:			;
		}    // ( ... )+
		_ttype = LITERAL;
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mSTLITERAL(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = STLITERAL;
		
		if (!(sub() && start()))
		  throw new SemanticException("sub() && start()");
		{ // ( ... )+
			int _cnt71=0;
			for (;;)
			{
				if ((cached_LA1=='\\'))
				{
					mESC_CHAR(false);
				}
				else if ((tokenSet_8_.member(cached_LA1))) {
					{
						match(tokenSet_8_);
					}
				}
				else
				{
					if (_cnt71 >= 1) { goto _loop71_breakloop; } else { throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());; }
				}
				
				_cnt71++;
			}
_loop71_breakloop:			;
		}    // ( ... )+
		_ttype = LITERAL; _st = SubState.Body;
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mGOBBLE(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = GOBBLE;
		
		if (!(sub() && gobble()))
		  throw new SemanticException("sub() && gobble()");
		{    // ( ... )*
			for (;;)
			{
				switch ( cached_LA1 )
				{
				case '\n':  case '\r':
				{
					mNEWLINE(false);
					break;
				}
				case ' ':
				{
					match(' ');
					break;
				}
				case '\t':
				{
					match('\t');
					break;
				}
				default:
				{
					goto _loop74_breakloop;
				}
				 }
			}
_loop74_breakloop:			;
		}    // ( ... )*
		_ttype = Token.SKIP; _st = SubState.Body;
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mESCAPE(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = ESCAPE;
		
		match('$');
		_st = SubState.Body;
		if(expr())
		_expr--;
		else 
		_expr++;
		
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mLBRACE(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = LBRACE;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match('{');
		gosub();
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mRBRACE(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = RBRACE;
		
		if (!(sub()))
		  throw new SemanticException("sub()");
		match('}');
		_expr--; _st = SubState.Body;
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mINT(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = INT;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		{ // ( ... )+
			int _cnt80=0;
			for (;;)
			{
				if (((cached_LA1 >= '0' && cached_LA1 <= '9')))
				{
					matchRange('0','9');
				}
				else
				{
					if (_cnt80 >= 1) { goto _loop80_breakloop; } else { throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());; }
				}
				
				_cnt80++;
			}
_loop80_breakloop:			;
		}    // ( ... )+
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mSTRING(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = STRING;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		int _saveIndex = 0;
		_saveIndex = text.Length;
		match('"');
		text.Length = _saveIndex;
		{    // ( ... )*
			for (;;)
			{
				if ((cached_LA1=='\\'))
				{
					mESC_CHAR(false);
				}
				else if ((tokenSet_9_.member(cached_LA1))) {
					matchNot('"');
				}
				else
				{
					goto _loop83_breakloop;
				}
				
			}
_loop83_breakloop:			;
		}    // ( ... )*
		_saveIndex = text.Length;
		match('"');
		text.Length = _saveIndex;
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mCHAR(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = CHAR;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		int _saveIndex = 0;
		_saveIndex = text.Length;
		match('\'');
		text.Length = _saveIndex;
		matchNot('\'');
		_saveIndex = text.Length;
		match('\'');
		text.Length = _saveIndex;
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mCOMMENT(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = COMMENT;
		
		if (((cached_LA1=='/') && (cached_LA2=='/'))&&(expr()))
		{
			match("//");
			{    // ( ... )*
				for (;;)
				{
					if ((tokenSet_10_.member(cached_LA1)))
					{
						{
							match(tokenSet_10_);
						}
					}
					else
					{
						goto _loop88_breakloop;
					}
					
				}
_loop88_breakloop:				;
			}    // ( ... )*
			_ttype = Token.SKIP;
		}
		else if (((cached_LA1=='/') && (cached_LA2=='*'))&&(expr())) {
			match("/*");
			{    // ( ... )*
				for (;;)
				{
					// nongreedy exit test
					if ((cached_LA1=='*') && (cached_LA2=='/')) goto _loop90_breakloop;
					if ((cached_LA1=='\n'||cached_LA1=='\r') && ((cached_LA2 >= '\u0000' && cached_LA2 <= '\ufffe')))
					{
						mNEWLINE(false);
					}
					else if (((cached_LA1 >= '\u0000' && cached_LA1 <= '\ufffe')) && ((cached_LA2 >= '\u0000' && cached_LA2 <= '\ufffe'))) {
						matchNot(EOF/*_CHAR*/);
					}
					else
					{
						goto _loop90_breakloop;
					}
					
				}
_loop90_breakloop:				;
			}    // ( ... )*
			match("*/");
			_ttype = Token.SKIP;
		}
		else
		{
			throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());
		}
		
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mSEMI(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = SEMI;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match(";");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mAT(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = AT;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("@");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mCOLON_EQUALS(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = COLON_EQUALS;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match(":=");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mPLUS(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = PLUS;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("+");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mMINUS(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = MINUS;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("-");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mLESS(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = LESS;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("<");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mLESSEQ(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = LESSEQ;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("<=");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mGREATER(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = GREATER;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match(">");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mGREATEREQ(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = GREATEREQ;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match(">=");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mEQUALS(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = EQUALS;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("=");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mNOT_EQUALS(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = NOT_EQUALS;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("!=");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mMUL(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = MUL;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("*");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mDIV(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = DIV;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("/");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mNOT(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = NOT;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("!");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mLPAREN(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = LPAREN;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("(");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mRPAREN(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = RPAREN;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match(")");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mCOMMA(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = COMMA;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match(",");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mLBRACK(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = LBRACK;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("[");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mRBRACK(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = RBRACK;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("]");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mAND(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = AND;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("&&");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mOR(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = OR;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match("||");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mPIPE(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = PIPE;
		
		if (!(expr() || (sub() && (start() || args()))))
		  throw new SemanticException("expr() || (sub() && (start() || args()))");
		match("|");
		nextSubState();
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mCOLON(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = COLON;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match(":");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mDOT(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = DOT;
		
		if (!(expr()))
		  throw new SemanticException("expr()");
		match(".");
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mIDENT(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = IDENT;
		
		if (!(expr() || (sub() && args())))
		  throw new SemanticException("expr() || (sub() && args())");
		{
			switch ( cached_LA1 )
			{
			case 'A':  case 'B':  case 'C':  case 'D':
			case 'E':  case 'F':  case 'G':  case 'H':
			case 'I':  case 'J':  case 'K':  case 'L':
			case 'M':  case 'N':  case 'O':  case 'P':
			case 'Q':  case 'R':  case 'S':  case 'T':
			case 'U':  case 'V':  case 'W':  case 'X':
			case 'Y':  case 'Z':
			{
				matchRange('A','Z');
				break;
			}
			case 'a':  case 'b':  case 'c':  case 'd':
			case 'e':  case 'f':  case 'g':  case 'h':
			case 'i':  case 'j':  case 'k':  case 'l':
			case 'm':  case 'n':  case 'o':  case 'p':
			case 'q':  case 'r':  case 's':  case 't':
			case 'u':  case 'v':  case 'w':  case 'x':
			case 'y':  case 'z':
			{
				matchRange('a','z');
				break;
			}
			case '_':
			{
				match('_');
				break;
			}
			default:
			{
				throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());
			}
			 }
		}
		{    // ( ... )*
			for (;;)
			{
				switch ( cached_LA1 )
				{
				case 'a':  case 'b':  case 'c':  case 'd':
				case 'e':  case 'f':  case 'g':  case 'h':
				case 'i':  case 'j':  case 'k':  case 'l':
				case 'm':  case 'n':  case 'o':  case 'p':
				case 'q':  case 'r':  case 's':  case 't':
				case 'u':  case 'v':  case 'w':  case 'x':
				case 'y':  case 'z':
				{
					matchRange('a','z');
					break;
				}
				case 'A':  case 'B':  case 'C':  case 'D':
				case 'E':  case 'F':  case 'G':  case 'H':
				case 'I':  case 'J':  case 'K':  case 'L':
				case 'M':  case 'N':  case 'O':  case 'P':
				case 'Q':  case 'R':  case 'S':  case 'T':
				case 'U':  case 'V':  case 'W':  case 'X':
				case 'Y':  case 'Z':
				{
					matchRange('A','Z');
					break;
				}
				case '0':  case '1':  case '2':  case '3':
				case '4':  case '5':  case '6':  case '7':
				case '8':  case '9':
				{
					matchRange('0','9');
					break;
				}
				case '_':
				{
					match('_');
					break;
				}
				default:
				{
					goto _loop119_breakloop;
				}
				 }
			}
_loop119_breakloop:			;
		}    // ( ... )*
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	public void mWS(bool _createToken) //throws RecognitionException, CharStreamException, TokenStreamException
{
		int _ttype; IToken _token=null; int _begin=text.Length;
		_ttype = WS;
		
		if (!(expr() || (sub() && !body())))
		  throw new SemanticException("expr() || (sub() && !body())");
		{ // ( ... )+
			int _cnt122=0;
			for (;;)
			{
				switch ( cached_LA1 )
				{
				case '\n':  case '\r':
				{
					mNEWLINE(false);
					break;
				}
				case ' ':
				{
					match(' ');
					break;
				}
				case '\t':
				{
					match('\t');
					break;
				}
				default:
				{
					if (_cnt122 >= 1) { goto _loop122_breakloop; } else { throw new NoViableAltForCharException(cached_LA1, getFilename(), getLine(), getColumn());; }
				}
				}
				_cnt122++;
			}
_loop122_breakloop:			;
		}    // ( ... )+
		_ttype = Token.SKIP;
		if (_createToken && (null == _token) && (_ttype != Token.SKIP))
		{
			_token = makeToken(_ttype);
			_token.setText(text.ToString(_begin, text.Length-_begin));
		}
		returnToken_ = _token;
	}
	
	
	private static long[] mk_tokenSet_0_()
	{
		long[] data = new long[2048];
		data[0]=-549755813889L;
		for (int i = 1; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_0_ = new BitSet(mk_tokenSet_0_());
	private static long[] mk_tokenSet_1_()
	{
		long[] data = new long[2048];
		data[0]=-68719476737L;
		for (int i = 1; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_1_ = new BitSet(mk_tokenSet_1_());
	private static long[] mk_tokenSet_2_()
	{
		long[] data = new long[2048];
		data[0]=-68719476737L;
		data[1]=-2305843009213693953L;
		for (int i = 2; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_2_ = new BitSet(mk_tokenSet_2_());
	private static long[] mk_tokenSet_3_()
	{
		long[] data = new long[2048];
		data[0]=-73014453761L;
		data[1]=-3458764513820540929L;
		for (int i = 2; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_3_ = new BitSet(mk_tokenSet_3_());
	private static long[] mk_tokenSet_4_()
	{
		long[] data = new long[1025];
		data[0]=0L;
		data[1]=576460745995190270L;
		for (int i = 2; i<=1024; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_4_ = new BitSet(mk_tokenSet_4_());
	private static long[] mk_tokenSet_5_()
	{
		long[] data = new long[1025];
		data[0]=4294977024L;
		for (int i = 1; i<=1024; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_5_ = new BitSet(mk_tokenSet_5_());
	private static long[] mk_tokenSet_6_()
	{
		long[] data = new long[2048];
		data[0]=-68719485953L;
		data[1]=-268435457L;
		for (int i = 2; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_6_ = new BitSet(mk_tokenSet_6_());
	private static long[] mk_tokenSet_7_()
	{
		long[] data = new long[2048];
		data[0]=-68719485953L;
		data[1]=-2305843009482129409L;
		for (int i = 2; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_7_ = new BitSet(mk_tokenSet_7_());
	private static long[] mk_tokenSet_8_()
	{
		long[] data = new long[2048];
		data[0]=-73014453761L;
		data[1]=-3458764514088976385L;
		for (int i = 2; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_8_ = new BitSet(mk_tokenSet_8_());
	private static long[] mk_tokenSet_9_()
	{
		long[] data = new long[2048];
		data[0]=-17179869185L;
		data[1]=-268435457L;
		for (int i = 2; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_9_ = new BitSet(mk_tokenSet_9_());
	private static long[] mk_tokenSet_10_()
	{
		long[] data = new long[2048];
		data[0]=-9217L;
		for (int i = 1; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		for (int i = 1024; i<=2047; i++) { data[i]=0L; }
		return data;
	}
	public static readonly BitSet tokenSet_10_ = new BitSet(mk_tokenSet_10_());
	
}
}
