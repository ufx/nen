#pragma warning disable 1591

using System;
using System.Collections.Specialized;

namespace Nen.Text.StringTemplate {
    public class TemplateEnvironment {
        private HybridDictionary _vars = new HybridDictionary();
        private TemplateEnvironment _parent;

        #region Constructor
        public TemplateEnvironment () {
        }

        public TemplateEnvironment (TemplateEnvironment parent) {
            _parent = parent;
        }
        #endregion

        #region Methods
        public void Extend (string key, object value) {
            _vars[key] = value;
        }
        #endregion

        #region Properties
        public object this[string index] {
            get {
                if (!_vars.Contains(index))
                    if (_parent != null)
                        return _parent[index];
                    else
                        throw new UnboundVariableException(index);
                return _vars[index];
            }

            set {
                for (TemplateEnvironment e = this; e != null; e = e.Parent) {
                    if (e._vars.Contains(index)) {
                        e._vars[index] = value;
                        return;
                    }
                }

                Extend(index, value);
            }
        }

        public TemplateEnvironment Parent {
            get { return _parent; }
            set { _parent = value; }
        }
        #endregion
    }
}
