#pragma warning disable 1591

using System;

namespace Nen.Text.StringTemplate {
    public class TemplateException : Exception {
        public TemplateException () { }
        public TemplateException (string format, params object[] args) : base(string.Format(format, args)) { }
        public TemplateException (Exception inner, string format, params object[] args) : base(string.Format(format, args), inner) { }
    }

    public class UnboundVariableException : TemplateException {
        public UnboundVariableException () { }
        public UnboundVariableException (string format, params object[] args) : base(format, args) { }
        public UnboundVariableException (Exception inner, string format, params object[] args) : base(inner, format, args) { }
    }

    public class UnboundFieldException : TemplateException {
        public object Referent;
        public string Field;

        public UnboundFieldException (object val, string field)
            : base("Unbound field {1} in object {0}.", val, field) {
            Referent = val;
            Field = field;
        }
    }

    public class NonFunctionException : TemplateException {
        public object Value;
        public NonFunctionException (object value)
            : base("The value {0} is not a function.", value) {
            Value = value;
        }
    }
}
