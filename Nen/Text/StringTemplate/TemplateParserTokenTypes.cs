// $ANTLR 2.7.5 (20050128): "parser.g" -> "TemplateParser.cs"$

#pragma warning disable 1591

using System.Collections.Generic;

namespace Nen.Text.StringTemplate
{
    [System.CLSCompliant(false)]
	public class TemplateParserTokenTypes
	{
		public const int EOF = 1;
		public const int NULL_TREE_LOOKAHEAD = 3;
		public const int LITERAL = 4;
		public const int ESCAPE = 5;
		public const int SEMI = 6;
		public const int COLON_EQUALS = 7;
		public const int AT = 8;
		public const int AND = 9;
		public const int OR = 10;
		public const int EQUALS = 11;
		public const int NOT_EQUALS = 12;
		public const int LESS = 13;
		public const int LESSEQ = 14;
		public const int GREATER = 15;
		public const int GREATEREQ = 16;
		public const int PLUS = 17;
		public const int MINUS = 18;
		public const int DIV = 19;
		public const int MUL = 20;
		public const int NOT = 21;
		public const int INT = 22;
		public const int IDENT = 23;
		public const int STRING = 24;
		public const int CHAR = 25;
		public const int LPAREN = 26;
		public const int RPAREN = 27;
		public const int DOT = 28;
		public const int COMMA = 29;
		public const int COLON = 30;
		public const int LBRACK = 31;
		public const int RBRACK = 32;
		public const int PIPE = 33;
		public const int LBRACE = 34;
		public const int RBRACE = 35;
		public const int SLITERAL = 36;
		public const int STLITERAL = 37;
		public const int GOBBLE = 38;
		public const int COMMENT = 39;
		public const int ESC_CHAR = 40;
		public const int WS = 41;
		public const int NEWLINE = 42;
		
	}
}
