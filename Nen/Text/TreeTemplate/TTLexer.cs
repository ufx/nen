﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Nen.Internal;
using Nen.IO;
using System.Reflection;

namespace Nen.Text.TreeTemplate {
	internal enum TTLexerTokenType {
		Invalid, // Placeholder token inserted before and after the input.

		// Indentation / lines
		Indent, // A single level of indentation.
		Dedent, // Ends a level of indentation. One Dedent is generated for each Indent.
		Separator, // Inserted at the end of each line.

		// Literals
		Word,   // Identifier-like strings
		Number, // Numbers (suitable for decimal.Parse)
		String, // Quoted text
		Text,   // Unquoted raw text

		// Grouping
		LeftBrace, RightBrace,
		LeftParen, RightParen,
		LeftBracket, RightBracket,
		LeftCaret, RightCaret,

		// Element syntax
		PercentSign,
		NumberSign,
		Dot,
		Dash,
		DoubleDash,
		Semi,
		Comma,
		Colon,

		// Operators
		Equals,
		EqualsEquals,
		GreaterEquals,
		LessEquals,
		NotEquals,
		ColonEquals,

		Times,
		Plus,
		Divide,
		Dollar,
		Question,
		Bang,
		Tilde,
		At,

		Amp, Pipe,
		LogAnd,
		LogOr,
	}

	internal enum TTLexerMode {
		LineStart,
		Expression,
		PlainText,
	}

	internal class TTLexerToken {
		public TTLexerTokenType Type;
		public string Text;
		public int LeadingWhitespace;
		public int LineNumber;
		public int Character;
		public int IndentLevel;
	}

	internal class TTLexer {
		class LexState {
			public Stream InputStream;
			public StreamReader Reader;
			public int LineNumber;
			public int Character;
			public Stack<int> Indent;
			public int TabWidth;
			public int LeadingWhitespace;
			public TTLexerMode LexMode;
			public Queue<TTLexerToken> Tokens;
			public char CurrentChar;
			public char PreviousChar;
			public long SavedPosition;
			public int SavedCharOffset;
			public StringBuilder Buffer;

			private void FlushText () {
				if (Buffer.Length > 0) {
					var str = Buffer.ToString();
					Buffer.Length = 0;
					Token(TTLexerTokenType.Text, str);
				}
			}

			public void Token (TTLexerTokenType token) {
				FlushText();
				Tokens.Enqueue(new TTLexerToken { Type = token, Character = Character, LineNumber = LineNumber, IndentLevel = Indent.Peek() });
			}

			public void Token (TTLexerTokenType token, string text) {
				FlushText();
				Tokens.Enqueue(new TTLexerToken { Type = token, Text = text, Character = Character, LineNumber = LineNumber, LeadingWhitespace = LeadingWhitespace, IndentLevel = Indent.Peek() });
				LeadingWhitespace = 0;
			}

			public LexState (Stream inputStream) {
				if (!inputStream.CanSeek) {
					InputStream = new MemoryStream();
					inputStream.BoundedCopyTo(InputStream, long.MaxValue);
					InputStream.Position = 0;
				} else {
					InputStream = inputStream;
				}

				Reader = new StreamReader(InputStream);
				Buffer = new StringBuilder();
				Tokens = new Queue<TTLexerToken>();
				TabWidth = 4;
				Indent = new Stack<int>();
				Indent.Push(0);
			}

			private static int GetCharOffset (StreamReader reader) {
				var StreamReader_charPos = typeof(StreamReader).GetField("charPos", BindingFlags.Instance | BindingFlags.NonPublic);

				return (int)StreamReader_charPos.GetValue(reader);
			}

			private static long GetResetPosition (StreamReader reader) {
				var StreamReader_byteLen = typeof(StreamReader).GetField("byteLen", BindingFlags.Instance | BindingFlags.NonPublic);

				var byteLen = (int)StreamReader_byteLen.GetValue(reader);
				return reader.BaseStream.Position - byteLen;
			}

			public LexState (LexState copy) {
				copy.SavedPosition = GetResetPosition(copy.Reader);
				copy.SavedCharOffset = GetCharOffset(copy.Reader);

				InputStream = copy.InputStream;
				Reader = copy.Reader;
				LineNumber = copy.LineNumber;
				Character = copy.Character;
				TabWidth = copy.TabWidth;
				LexMode = copy.LexMode;
				CurrentChar = copy.CurrentChar;
				PreviousChar = copy.PreviousChar;
				LeadingWhitespace = copy.LeadingWhitespace;
				Tokens = new Queue<TTLexerToken>(copy.Tokens);
				Indent = new Stack<int>(copy.Indent.Reverse());
				Buffer = new StringBuilder(copy.Buffer.ToString());
			}
		}

		private Stack<LexState> _lexState;

		public TTLexer (Stream inputStream) {
			_lexState = new Stack<LexState>();
			_lexState.Push(new LexState(inputStream));
		}

		private void LineStart () {
			var state = _lexState.Peek();
			var lineIndent = 0;

			while (CurrentCharacter == ' ' || CurrentCharacter == '\t') {
				lineIndent += CurrentCharacter == ' ' ? 1 : state.TabWidth;
				NextChar();
			}

			if (CurrentCharacter == '\r' || CurrentCharacter == '\n' || CurrentCharacter == '\0')
				return;

			while (lineIndent < state.Indent.Peek()) {
				state.Indent.Pop();
				state.Token(TTLexerTokenType.Dedent);
			}

			if (lineIndent > state.Indent.Peek()) {
				state.Indent.Push(lineIndent);
				state.Token(TTLexerTokenType.Indent, null);
			}

			state.LexMode = TTLexerMode.Expression;
		}

		private void Expression () {
			var state = _lexState.Peek();

			while (char.IsWhiteSpace(CurrentCharacter)) {
				if (CurrentCharacter == '\r' || CurrentCharacter == '\n') {
					state.LeadingWhitespace = 0;
					return;
				}

				state.LeadingWhitespace++;
				NextChar();
			}

			if (char.IsLetter(CurrentCharacter)) {
				StringBuilder sb = new StringBuilder();
				while (char.IsLetterOrDigit(CurrentCharacter) ||
					   CurrentCharacter == '_') {
					sb.Append(CurrentCharacter);
					NextChar();
				}

				state.Token(TTLexerTokenType.Word, sb.ToString());
			} else if (char.IsDigit(CurrentCharacter)) {
				StringBuilder sb = new StringBuilder();
				while (char.IsDigit(CurrentCharacter)) {
					sb.Append(CurrentCharacter);
					NextChar();
				}

				state.Token(TTLexerTokenType.Number, sb.ToString());
			} else if (CurrentCharacter == '"') {
				// TODO, control characters
				var sb = new StringBuilder();
				NextChar();
				while (CurrentCharacter != '"') {
					sb.Append(CurrentCharacter);
					NextChar();
				}

				NextChar();
				state.Token(TTLexerTokenType.String, sb.ToString());
			} else if (CurrentCharacter == '\\') { // TODO... make this part of the Word token handler.
				NextChar();
				state.Token(TTLexerTokenType.Word, CurrentCharacter.ToString());
				NextChar();
			} else if (CurrentCharacter == '+') {
				state.Token(TTLexerTokenType.Plus, "+");
				NextChar();
			} else if (CurrentCharacter == '-') {
				NextChar();
				if (CurrentCharacter == '-') {
					state.Token(TTLexerTokenType.DoubleDash, "--");
					NextChar();
				} else {
					state.Token(TTLexerTokenType.Dash, "-");
				}
			} else if (CurrentCharacter == '*') {
				state.Token(TTLexerTokenType.Times, "*");
				NextChar();
			} else if (CurrentCharacter == '%') {
				state.Token(TTLexerTokenType.PercentSign, "%");
				NextChar();
			} else if (CurrentCharacter == '/') {
				state.Token(TTLexerTokenType.Divide, "/");
				NextChar();
			} else if (CurrentCharacter == '~') {
				state.Token(TTLexerTokenType.Tilde, "~");
				NextChar();
			} else if (CurrentCharacter == '@') {
				state.Token(TTLexerTokenType.At, "@");
				NextChar();
			} else if (CurrentCharacter == '|') {
				NextChar();

				if (CurrentCharacter == '|') {
					state.Token(TTLexerTokenType.LogOr, "||");
					NextChar();
				} else {
					state.Token(TTLexerTokenType.Pipe, "|");
				}
			} else if (CurrentCharacter == '&') {
				NextChar();

				if (CurrentCharacter == '&') {
					state.Token(TTLexerTokenType.LogAnd, "&&");
					NextChar();
				} else {
					state.Token(TTLexerTokenType.Amp, "&");
				}
			} else if (CurrentCharacter == '#') {
				state.Token(TTLexerTokenType.NumberSign, "#");
				NextChar();
			} else if (CurrentCharacter == '$') {
				state.Token(TTLexerTokenType.Dollar, "$");
				NextChar();
			} else if (CurrentCharacter == '.') {
				state.Token(TTLexerTokenType.Dot, ".");
				NextChar();
			} else if (CurrentCharacter == ',') {
				state.Token(TTLexerTokenType.Comma, ",");
				NextChar();
			} else if (CurrentCharacter == '{') {
				state.Token(TTLexerTokenType.LeftBrace, "{");
				NextChar();
			} else if (CurrentCharacter == '}') {
				state.Token(TTLexerTokenType.RightBrace, "}");
				NextChar();
			} else if (CurrentCharacter == '(') {
				state.Token(TTLexerTokenType.LeftParen, "(");
				NextChar();
			} else if (CurrentCharacter == ')') {
				state.Token(TTLexerTokenType.RightParen, ")");
				NextChar();
			} else if (CurrentCharacter == '<') {
				NextChar();

				if (CurrentCharacter == '=') {
					state.Token(TTLexerTokenType.LessEquals, "<=");
					NextChar();
				} else {
					state.Token(TTLexerTokenType.LeftCaret, "<");
				}
			} else if (CurrentCharacter == '>') {
				NextChar();

				if (CurrentCharacter == '=') {
					state.Token(TTLexerTokenType.GreaterEquals, ">=");
					NextChar();
				} else {
					state.Token(TTLexerTokenType.RightCaret);
				}
			} else if (CurrentCharacter == '[') {
				state.Token(TTLexerTokenType.LeftBracket, "[");
				NextChar();
			} else if (CurrentCharacter == ']') {
				state.Token(TTLexerTokenType.RightBracket, "]");
				NextChar();
			} else if (CurrentCharacter == ':') {
				NextChar();

				if (CurrentCharacter == '=') {
					state.Token(TTLexerTokenType.ColonEquals, ":=");
					NextChar();
				} else {
					state.Token(TTLexerTokenType.Colon, ":");
				}
			} else if (CurrentCharacter == '!') {
				NextChar();

				if (CurrentCharacter == '=') {
					state.Token(TTLexerTokenType.NotEquals, "!=");
					NextChar();
				} else {
					state.Token(TTLexerTokenType.Bang, "!");
				}
			} else if (CurrentCharacter == '?') {
				state.Token(TTLexerTokenType.Question, "?");
				NextChar();
			} else if (CurrentCharacter == ';') {
				state.Token(TTLexerTokenType.Semi, ";");
				NextChar();
			} else if (CurrentCharacter == '=') {
				NextChar();
				if (CurrentCharacter == '=') {
					state.Token(TTLexerTokenType.EqualsEquals, "==");
					NextChar();
				} else {
					state.Token(TTLexerTokenType.Equals, "=");
				}
			} else {
				// append unknown chars to the text buffer.
				state.Buffer.Append(CurrentCharacter);
				NextChar();
			}
		}

		private void PlainText () {
			var state = _lexState.Peek();
			var sb = new StringBuilder();

			while (CurrentCharacter != '\r' && CurrentCharacter != '\n' && CurrentCharacter != '\0') {
				sb.Append(CurrentCharacter);
				NextChar();
			}

			state.Token(TTLexerTokenType.Text, sb.ToString());
		}

		public void Advance () {
			var state = _lexState.Peek();

			if (state.Tokens.Count > 0) {
				state.Tokens.Dequeue();

				// if we have backed up tokens, we don't need to do any more parsing yet.
				if (state.Tokens.Count > 0)
					return;
			}

			if (CurrentCharacter == '\0') {
				if (state.PreviousChar == '\0') {
					NextChar(); // get things started.
				} else {
					if (state.LexMode != TTLexerMode.LineStart) {
						state.Token(TTLexerTokenType.Separator);
						state.LexMode = TTLexerMode.LineStart;
					}

					while (state.Indent.Count > 1) {
						state.Token(TTLexerTokenType.Dedent);
						state.Indent.Pop();
					}

					// We had a character, but we don't anymore. We're at EOF.
					state.Token(TTLexerTokenType.Invalid);
					return;
				}
			}

			while (state.Tokens.Count == 0) {
				if (CurrentCharacter == '\r' || CurrentCharacter == '\n') {
					if (state.LexMode != TTLexerMode.LineStart) {
						state.Token(TTLexerTokenType.Separator);
						state.LexMode = TTLexerMode.LineStart;
					}

					NextChar();
					if ((state.PreviousChar == '\r' && CurrentCharacter == '\n') ||
						(state.PreviousChar == '\n' && CurrentCharacter == '\r'))
						NextChar();
				}

				if (state.LexMode == TTLexerMode.LineStart)
					LineStart();
				else if (state.LexMode == TTLexerMode.Expression)
					Expression();
				else if (state.LexMode == TTLexerMode.PlainText)
					PlainText();
			}
		}

		public void PushState () {
			_lexState.Push(new LexState(_lexState.Peek()));
		}

		public void PopState () {
			_lexState.Pop();

			var state = _lexState.Peek();
			state.InputStream.Position = _lexState.Peek().SavedPosition;
			state.Reader.DiscardBufferedData();

			for (var i = 0; i < state.SavedCharOffset; ++i)
				state.Reader.Read();
		}

		public void NipState () {
			var state = _lexState.Pop();

			_lexState.Pop();
			_lexState.Push(state);
		}

		private void NextChar () {
			var state = _lexState.Peek();

			if ((state.CurrentChar == '\r' && state.PreviousChar != '\n') ||
				(state.CurrentChar == '\n' && state.PreviousChar != '\r')) {
				state.LineNumber++;
				state.Character = 0;
			} else {
				state.Character++;
			}

			state.PreviousChar = state.CurrentChar;
			var nextChar = state.Reader.Read();

			state.CurrentChar = nextChar == -1 ? '\0' : (char)nextChar;
		}

		private char CurrentCharacter {
			get {
				return _lexState.Peek().CurrentChar;
			}
		}

		public TTLexerToken CurrentToken {
			get {
				var state = _lexState.Peek();

				return state.Tokens.Count > 0 ? state.Tokens.Peek() : new TTLexerToken() { Type = TTLexerTokenType.Invalid };
			}
		}

		public int TabWidth {
			get { return _lexState.Peek().TabWidth; }
			set { _lexState.Peek().TabWidth = value; }
		}
	}
}
