﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Text.TreeTemplate {
	public struct TTName {
		public string Namespace { get; private set; }
		public string Name { get; private set; }

		public TTName (string ns, string name): this() {
			Namespace = ns;
			Name = name;
		}

		public TTName (string name): this() {
			Namespace = null;
			Name = name;
		}

		public static implicit operator TTName (string name) {
			return new TTName(name);
		}

		public override string ToString () {
			return Namespace != null ? Namespace + ":" + Name : Name;
		}
	}

    public abstract class TTExpression {
        public abstract T Accept<T> (ITTExpressionVisitor<T> visitor);
    }

    public class TTContainerExpression : TTExpression {
        public List<TTExpression> Children { get; set; }

        public TTContainerExpression () {
            Children = new List<TTExpression>();
        }

        public TTContainerExpression (IEnumerable<TTExpression> children): this() {
            Children.AddRange(children);
        }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public class TTElementExpression: TTContainerExpression {
        public TTName Name { get; set; }
        public string ID { get; set; }
        public string Class { get; set; }

        public List<TTAttributeExpression> Attributes { get; set; }

        public TTElementExpression () {
            Attributes = new List<TTAttributeExpression>();
        }

        public TTElementExpression (TTName name, string id, string klass, IEnumerable<TTExpression> children): this() {
            Name = name;
            ID = id;
            Class = klass;

            Children.AddRange(children);
        }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public class TTAttributeExpression: TTExpression {
        public string Name { get; set; }
        public TTExpression Value { get; set; }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public class TTLiteralExpression: TTExpression {
        public object Value { get; set; }

        public TTLiteralExpression () { }
        public TTLiteralExpression (object value) {
            Value = value;
        }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public class TTIdentifierExpression: TTExpression {
        public string Name { get; set; }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public enum TTBinaryOperator {
        Plus,     // +
        Minus,    // -
        Times,    // *
        Divide,   // /
        Modulo,   // %
        Is,       // is
        As,       // as
        BitAnd,   // &
        BitOr,    // |
        BitXor,   // ^
        LogAnd,   // &&
        LogOr,    // ||
        NullAlt,  // ??
        Gt,       // >
        Ge,       // >=
        Lt,       // <
        Le,       // <=
        Eq,       // ==
        Neq,      // !=
		Assign    // :=
    }

    public class TTBinaryExpression: TTExpression {
        public TTExpression Left { get; set; }
        public TTExpression Right { get; set; }
        public TTBinaryOperator Operator { get; set; }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public enum TTUnaryOperator {
        LogNot, // !
        BitNot  // ~
    }

    public class TTUnaryExpression: TTExpression {
        public TTExpression Child { get; set; }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public class TTMemberExpression: TTExpression {
        public TTExpression LeftSide { get; set; }
        public string Member { get; set; }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public class TTApplyExpression: TTExpression {
        public TTExpression LeftSide { get; set; }
        public List<TTExpression> Arguments { get; set; }

        public TTApplyExpression () {
            Arguments = new List<TTExpression>();
        }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

	public class TTCondition {
		public TTExpression Test { get; set; }
		public TTExpression Body { get; set; }
	}

	public class TTConditionExpression : TTExpression {
		public List<TTCondition> Conditions { get; set; }
		public TTExpression Alternative { get; set; }

		public TTConditionExpression () {
			Conditions = new List<TTCondition>();
		}

		public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
			return visitor.Visit(this);
		}
	}

	public class TTForeachExpression : TTContainerExpression {
		public TTIdentifierExpression VariableName { get; set; }
		public TTExpression Source { get; set; }

		public TTForeachExpression () { }

		public TTForeachExpression (TTIdentifierExpression variableName, TTExpression source, IEnumerable<TTExpression> children)
			: base(children) {
				VariableName = variableName;
				Source = source;
		}

		public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
			return visitor.Visit(this);
		}
	}

	public class TTUsingExpression : TTContainerExpression {
		public TTIdentifierExpression VariableName { get; set; }
		public TTExpression Source { get; set; }

		public TTUsingExpression () { }

		public TTUsingExpression (TTIdentifierExpression variableName, TTExpression source, IEnumerable<TTExpression> children)
			: base(children) {
				VariableName = variableName;
				Source = source;
		}

		public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
			return visitor.Visit(this);
		}
	}

    public class TTDirectiveExpression : TTExpression {
        public string DirectiveName { get; set; }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public class TTImportDirectiveExpression : TTDirectiveExpression {
        public string Prefix { get; set; }
        public string Url { get; set; }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

	public class TTDictionaryExpression : TTExpression {
		public Dictionary<string, TTExpression> Members { get; set; }

		public TTDictionaryExpression () {
			Members = new Dictionary<string, TTExpression>();
		}

		public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
			return visitor.Visit(this);
		}
	}

    public class TTCommentExpression : TTExpression {
        public string Comment { get; set; }

        public override T Accept<T> (ITTExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
    }

    public interface ITTExpressionVisitor<T> {
        T Visit (TTApplyExpression expr);
        T Visit (TTAttributeExpression expr);
        T Visit (TTBinaryExpression expr);
        T Visit (TTCommentExpression expr);
        T Visit (TTContainerExpression expr);
        T Visit (TTElementExpression expr);
        T Visit (TTDirectiveExpression expr);
        T Visit (TTIdentifierExpression expr);
        T Visit (TTLiteralExpression expr);
        T Visit (TTMemberExpression expr);
        T Visit (TTImportDirectiveExpression expr);
        T Visit (TTUnaryExpression expr);
		T Visit (TTConditionExpression expr);
		T Visit (TTForeachExpression expr);
		T Visit (TTDictionaryExpression expr);
		T Visit (TTUsingExpression expr);
    }
}
