﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Nen.Internal;

namespace Nen.Text.TreeTemplate {
    public class TTParser {
        private TTLexer _lexer;

        private bool TokenIn (params TTLexerTokenType[] types) {
            return types.Contains(CurrentType);
        }

        private void Error (string message, params object[] args) {
            Error(_lexer.CurrentToken, message, args);
        }

        private void Error (TTLexerToken token, string message, params object[] args) {
            // TODO: proper exceptions
            throw new Exception(LS.T("[At line {0}, character {1}]: {2}", token.LineNumber, token.Character, LS.T(message, args)));
        }

        private TTLexerToken Match () {
            var token = _lexer.CurrentToken;
            _lexer.Advance();
            return token;
        }

        private TTLexerToken Match (params TTLexerTokenType[] types) {
            TTLexerToken result = null;

            foreach (var t in types) {
                if (!TokenIn(t))
                    throw new Exception(LS.T("Match failure. Expected {0}, got {1}. Line: {2}", t, CurrentType, _lexer.CurrentToken.LineNumber)); // TODO: error reporting

                result = Match();
            }

            return result;
        }

        private TTLexerTokenType CurrentType {
            get { return _lexer.CurrentToken.Type; }
        }

		private string CurrentText {
			get { return _lexer.CurrentToken.Text; }
		}

        public TTParser () {
        }

        public TTContainerExpression Parse (string text) {
            return Parse(new MemoryStream(Encoding.UTF8.GetBytes(text)));
        }

        public TTContainerExpression Parse (Stream stream) {
            return Parse(new TTLexer(stream));
        }

        internal TTContainerExpression Parse (TTLexer lexer) {
            _lexer = lexer;
            _lexer.Advance();
            return new TTContainerExpression { Children = Subtree() };
        }

        private List<TTExpression> Subtree () {
            var result = new List<TTExpression>();

            while (_lexer.CurrentToken.Type != TTLexerTokenType.Dedent &&
                   _lexer.CurrentToken.Type != TTLexerTokenType.Invalid)
                result.Add(Line());

            return result;
        }

		private void AppendAndCoalesce (List<TTExpression> list, TTLiteralExpression expr) {
			var last = list.LastOrDefault();

			if (last is TTLiteralExpression && ((TTLiteralExpression)last).Value is string && expr.Value is string)
				((TTLiteralExpression)last).Value = ((string)((TTLiteralExpression)last).Value) + (string)expr.Value;
			else
				list.Add(expr);
		}

		private List<TTExpression> SmartText (Func<bool> condition = null, bool skipLeadingWhitespace = false) {
			var sb = new StringBuilder();
			var result = new List<TTExpression>();
			var isFirstToken = skipLeadingWhitespace;

			while (CurrentType != TTLexerTokenType.Separator && (condition == null || condition())) {
				if (isFirstToken) {
					isFirstToken = false;
					_lexer.CurrentToken.LeadingWhitespace = 0;
				}

				if (CurrentType == TTLexerTokenType.At) {
					sb.Append("".PadRight(_lexer.CurrentToken.LeadingWhitespace));
					AppendAndCoalesce(result, new TTLiteralExpression { Value = sb.ToString() });
					sb.Length = 0;

					Match();

					if (CurrentType == TTLexerTokenType.At && _lexer.CurrentToken.LeadingWhitespace == 0) {
						sb.Append("@");
						Match();
					} else if (CurrentType == TTLexerTokenType.LeftBrace && _lexer.CurrentToken.LeadingWhitespace == 0) {
						Match();
						result.Add(Expression());
						Match(TTLexerTokenType.RightBrace);
					} else {
						result.Add(AccessExpression());
					}
				} else {
					AddLiteralText(sb, _lexer.CurrentToken);
					Match();
				}
			}

			if (sb.Length > 0)
				AppendAndCoalesce(result, new TTLiteralExpression { Value = sb.ToString() });

			return result;
		}

        private TTLiteralExpression PlainText () {
            var sb = new StringBuilder();
            while (CurrentType != TTLexerTokenType.Separator) {
                AddLiteralText(sb, _lexer.CurrentToken);
                Match();
            }

            return new TTLiteralExpression { Value = sb.ToString() };
        }

        private TTLiteralExpression PlainTextEndLine () {
            var result = PlainText();

            Match(TTLexerTokenType.Separator);
            return result;
        }

		private TTLiteralExpression PlainTextBlock () {
			var top = Match(TTLexerTokenType.DoubleDash).IndentLevel;

			var result = PlainTextEndLine();

			if (CurrentType == TTLexerTokenType.Indent) {
				var indentCount = Match().IndentLevel;
				var firstLine = result.Value.ToString();
				var sb = new StringBuilder();

				sb.AppendLine(firstLine);

				while (indentCount > top || CurrentType == TTLexerTokenType.Indent) {
					if (CurrentType == TTLexerTokenType.Indent) {
						indentCount = Match().IndentLevel;
						continue;
					} else if (CurrentType == TTLexerTokenType.Dedent) {
						indentCount = Match().IndentLevel;
						continue;
					}

					var line = PlainTextEndLine();

					if (indentCount > top)
						sb.AppendLine("".PadRight(indentCount - top) + line.Value.ToString());
					else
						sb.AppendLine(line.Value.ToString());
				}

				result = new TTLiteralExpression { Value = sb.ToString() };
			}

			return result;
		}

        private TTExpression Line () {
			TTExpression result = null;

			while (result == null) {
				if (CanElement()) {
					result = Element();
				} else if (CurrentType == TTLexerTokenType.DoubleDash) {
					return PlainTextBlock();
				} else if (CurrentType == TTLexerTokenType.Divide) {
					Match();
					var lit = PlainTextEndLine();
					result = new TTCommentExpression { Comment = (string)lit.Value };
				} else if (CurrentType == TTLexerTokenType.Bang) {
					result = Directive();
				} else if (CurrentType == TTLexerTokenType.Dash) {
					Match(TTLexerTokenType.Dash);
					return CodeBlock();
				} else {
					var text = SmartText();

					Match(TTLexerTokenType.Separator);
					if (text.Count == 1)
						result = text[0];
					else if (text.Count == 0)
						result = null;
					else
						result = new TTContainerExpression(text);
				}
			}

			return result;
        }

        private TTExpression CodeBlock () {
			if (CurrentType == TTLexerTokenType.Word) {
				switch (CurrentText) {
					case "case": return CaseBlock();
					case "foreach": return ForeachBlock();
					case "using": return UsingBlock();
					case "if": return IfBlock();
					case "else":
						Error("Found 'else' statement with no matching 'if'.");
						return null;
				}
			}

			var result = Expression();
			Match(TTLexerTokenType.Separator);
			return result;
        }

		private TTExpression CaseBlock () {
			Match(TTLexerTokenType.Word);

			var result = new TTConditionExpression();
			var conditionQueue = new List<TTExpression>();
			var hasDefault = false;

			Match(TTLexerTokenType.Separator);
			Match(TTLexerTokenType.Indent);

			while (CurrentType != TTLexerTokenType.Dedent) {
				Match(TTLexerTokenType.Dash);
				var keyword = Match(TTLexerTokenType.Word);
				
				switch (keyword.Text) {
					case "when":
						conditionQueue.Add(Expression());
						Match(TTLexerTokenType.Separator);

						break;
					case "default":
						hasDefault = true;
						Match(TTLexerTokenType.Separator);

						break;
					default:
						Error("Expecting 'case' or 'default', got '{0}", keyword.Text);
						break;
				}

				if (CurrentType == TTLexerTokenType.Indent) {
					Match();
					var body = Subtree();
					Match(TTLexerTokenType.Dedent);

					foreach (var cond in conditionQueue)
						result.Conditions.Add(new TTCondition { Test = cond, Body = new TTContainerExpression(body) });

					conditionQueue.Clear();

					if (hasDefault) {
						hasDefault = false;
						result.Alternative = new TTContainerExpression(body);
					}
				}
			}

			Match(TTLexerTokenType.Dedent);

			return result;
		}

		private TTExpression ForeachBlock () {
			Match(TTLexerTokenType.Word);

			var variableName = Match(TTLexerTokenType.Word).Text;
			var keyword = Match();

			if (keyword.Type != TTLexerTokenType.Word || keyword.Text != "in")
				Error("Expected 'in', got '" + keyword.Text + "'");

			var sourceExpr = Expression();

			Match(TTLexerTokenType.Separator);

			if (CurrentType == TTLexerTokenType.Indent) {
				Match();
				var body = Subtree();
				Match(TTLexerTokenType.Dedent);

				return new TTForeachExpression(new TTIdentifierExpression { Name = variableName }, sourceExpr, body);
			}

			return new TTForeachExpression {
				VariableName = new TTIdentifierExpression { Name = variableName },
				Source = sourceExpr
			};
		}

		private TTExpression UsingBlock () {
			Match(TTLexerTokenType.Word);

			var result = new TTUsingExpression();
			var usingExpr = Expression();

			if (usingExpr is TTBinaryExpression && ((TTBinaryExpression)usingExpr).Operator == TTBinaryOperator.Assign) {
				var binExpr = (TTBinaryExpression)usingExpr;

				if (!(binExpr.Left is TTIdentifierExpression))
					Error("Left side of using statement must be a variable name.");

				result.VariableName = (TTIdentifierExpression)binExpr.Left;
				result.Source = binExpr.Right;
			} else {
				result.Source = usingExpr;
			}

			Match(TTLexerTokenType.Separator);

			if (CurrentType == TTLexerTokenType.Indent) {
				result.Children.AddRange(Subtree());
				Match(TTLexerTokenType.Dedent);
			}

			return result;
		}

		private TTExpression IfBlock () {
			Match(TTLexerTokenType.Word);

			var result = new TTConditionExpression();
			var cond = Expression();
			List<TTExpression> body = new List<TTExpression>();

			Match(TTLexerTokenType.Separator);

			if (CurrentType == TTLexerTokenType.Indent) {
				Match();
				body = Subtree();
				Match(TTLexerTokenType.Dedent);
			}

			result.Conditions.Add(new TTCondition { Test = cond, Body = new TTContainerExpression(body) });
			body = new List<TTExpression>();

			while (CurrentType == TTLexerTokenType.Dash) {
				_lexer.PushState();

				Match();

				if (CurrentType == TTLexerTokenType.Word && CurrentText == "else") {
					_lexer.NipState();
					Match();

					if (CurrentType == TTLexerTokenType.Word && CurrentText == "if") {
						Match();
						cond = Expression();
						Match(TTLexerTokenType.Separator);

						if (CurrentType == TTLexerTokenType.Indent) {
							Match();
							body = Subtree();
							Match(TTLexerTokenType.Dedent);
						}

						result.Conditions.Add(new TTCondition { Test = cond, Body = new TTContainerExpression(body) });
						body = new List<TTExpression>();
					} else {
						Match(TTLexerTokenType.Separator);
						Match(TTLexerTokenType.Indent);
						result.Alternative = new TTContainerExpression(Subtree());
						Match(TTLexerTokenType.Dedent);
					}
				} else {
					_lexer.PopState();
					break;
				}
			}

			return result;
		}

        private TTDirectiveExpression Directive () {
            Match(TTLexerTokenType.Bang);

            var directive = Match(TTLexerTokenType.Word);

            switch (directive.Text) {
                case "import":
                    var ident = ElementIdentifier().Name;
                    var url = PlainTextEndLine();

                    return new TTImportDirectiveExpression { DirectiveName = "import", Prefix = ident, Url = ((string)url.Value).Trim() };
				case "tabs":
					var tabWidth = Literal();

					if (!(tabWidth.Value is decimal))
						Error("Tab width must be a number.");

					_lexer.TabWidth = (int)(decimal)tabWidth.Value;
					return null;
                default:
                    Error("Invalid directive.");
                    return null;
            }
        }

        private TTName ElementIdentifier () {
            string result = "";
			string ns = null;
            var first = true;

            while (TokenIn(TTLexerTokenType.Word, TTLexerTokenType.Dash, TTLexerTokenType.Colon) && 
                (_lexer.CurrentToken.LeadingWhitespace == 0 || first)) {

				if (CurrentType == TTLexerTokenType.Colon) {
					ns = result;
					result = "";
				} else {
					result += _lexer.CurrentToken.Text;
				}

                Match();
                first = false;
            }

            if (result == "")
                Error("Expected identifier, got {0}", CurrentType);

            return new TTName(ns, result);
        }

        private bool CanElement () {
            return TokenIn(TTLexerTokenType.PercentSign, TTLexerTokenType.Dot, TTLexerTokenType.NumberSign);
        }

        private TTElementExpression InlineElement () {
            var result = new TTElementExpression();

			bool hasClass = false, hasId = false, hasName = false;

            if (CurrentType == TTLexerTokenType.PercentSign) {
                Match();

				if (!TokenIn(TTLexerTokenType.Dot, TTLexerTokenType.NumberSign)) {
					result.Name = ElementIdentifier();
					hasName = true;
				}
            }

            while (TokenIn(TTLexerTokenType.Dot, TTLexerTokenType.NumberSign) && 
				((!hasClass && !hasId && !hasName) || _lexer.CurrentToken.LeadingWhitespace == 0)) {
                if (CurrentType == TTLexerTokenType.Dot) {
                    Match();
                    var klass = ElementIdentifier();

					if (result.Class != null)
						result.Class += " " + klass.Name;
					else
						result.Class = klass.Name;

					hasClass = true;
                } else if (CurrentType == TTLexerTokenType.NumberSign) {
                    if (hasId)
                        Error("This element already has an identifier.");

                    Match();
                    result.ID = ElementIdentifier().Name;
					hasId = true;
                }
            }

            if (CurrentType == TTLexerTokenType.LeftBrace) {
                result.Attributes = AttributeList();
            }

            if (CurrentType == TTLexerTokenType.DoubleDash) {
                Match();
                result.Children.Add(PlainText());
                return result;  
            }

			var body = SmartText(() => !CanElement() && CurrentType != TTLexerTokenType.Semi, skipLeadingWhitespace: true);

			if (body.Count > 0)
				result.Children.AddRange(body);

            if (CanElement())
                result.Children.AddRange(InlineElementList().Cast<TTExpression>());

            return result;
        }

        private void AddLiteralText (StringBuilder body, TTLexerToken token) {
            body.Append("".PadRight(token.LeadingWhitespace));
            if (token.Type == TTLexerTokenType.String)
                body.AppendFormat("\"{0}\"", token.Text);
            else
                body.Append(token.Text);
        }

        private List<TTElementExpression> InlineElementList () {
            var result = new List<TTElementExpression>();

            while (CanElement()) {
                result.Add(InlineElement());

                if (CurrentType == TTLexerTokenType.Semi)
                    Match();
                else
                    break;
            }

            return result;
        }

        private TTExpression Element () {
            var result = InlineElement();

            Match(TTLexerTokenType.Separator);

            if (CurrentType == TTLexerTokenType.Indent) {
                Match(TTLexerTokenType.Indent);
                // TODO: figure out the right place to add the subtree when there are nested inline elements.
                result.Children.AddRange(Subtree());
                Match(TTLexerTokenType.Dedent);
            }

            return result;
        }

        private List<TTAttributeExpression> AttributeList () {
            var result = new List<TTAttributeExpression>();

            Match(TTLexerTokenType.LeftBrace);

            while (CurrentType != TTLexerTokenType.RightBrace)
                result.Add(OneAttribute());

            Match(TTLexerTokenType.RightBrace);
            return result;
        }

        private TTAttributeExpression OneAttribute () {
            var result = new TTAttributeExpression();

			result.Name = ElementIdentifier().Name;

            if (CurrentType == TTLexerTokenType.Equals) {
                Match();

                result.Value = Expression();
            } else {
                result.Value = new TTLiteralExpression { Value = true };
            }

            return result;
        }

        private TTExpression Expression () {
            return AssignExpression();
        }

        private TTExpression BinaryOperator (TTBinaryOperator[] operators, TTLexerTokenType[] tokens, Func<TTExpression> subExpr) {
            var lhs = subExpr();
            var result = lhs;

            while (TokenIn(tokens)) {
                var oper = operators[Array.IndexOf(tokens, CurrentType)];
                Match();
                var rhs = subExpr();
                result = new TTBinaryExpression { Left = lhs, Operator = oper, Right = rhs };
            }

            return result;
        }

		private TTExpression AssignExpression () {
			return BinaryOperator(new[] { TTBinaryOperator.Assign }, new[] { TTLexerTokenType.ColonEquals }, LogOrExpression);
		}

        private TTExpression LogOrExpression () {
            return BinaryOperator(new[] { TTBinaryOperator.LogOr }, new[] { TTLexerTokenType.LogOr }, LogAndExpression);
        }

        private TTExpression LogAndExpression () {
            return BinaryOperator(new[] { TTBinaryOperator.LogAnd }, new[] { TTLexerTokenType.LogAnd }, CompareExpression);
        }

        private TTExpression CompareExpression () {
            return BinaryOperator(new[] { TTBinaryOperator.Eq, TTBinaryOperator.Neq },
                                  new[] { TTLexerTokenType.EqualsEquals, TTLexerTokenType.NotEquals },
                                  RelationalExpression);
        }

        private TTExpression RelationalExpression () {
            return BinaryOperator(new[] { TTBinaryOperator.Lt, TTBinaryOperator.Le,
                                          TTBinaryOperator.Ge, TTBinaryOperator.Gt },
                                  new[] { TTLexerTokenType.LeftCaret, TTLexerTokenType.LessEquals,
                                          TTLexerTokenType.GreaterEquals, TTLexerTokenType.RightCaret },
                                  AdditiveExpression);
        }

        private TTExpression AdditiveExpression () {
            return BinaryOperator(new[] { TTBinaryOperator.Plus, TTBinaryOperator.Minus },
                                  new[] { TTLexerTokenType.Plus, TTLexerTokenType.Dash },
                                  MultiplicativeExpression);
        }

        private TTExpression MultiplicativeExpression () {
            return BinaryOperator(new[] { TTBinaryOperator.Times, TTBinaryOperator.Divide },
                                  new[] { TTLexerTokenType.Times, TTLexerTokenType.Divide },
                                  UnaryExpression);
        }

        // TODO
        private TTExpression UnaryExpression () {
            var operand = AccessExpression();

            return operand;
        }

		private TTExpression AccessExpression () {
			var data = DataExpression();

			while (true) {
				if (CurrentType == TTLexerTokenType.Dot) {
					Match();
					var word = Match(TTLexerTokenType.Word);
					data = new TTMemberExpression { LeftSide = data, Member = word.Text };
				} else if (CurrentType == TTLexerTokenType.LeftParen) {
					Match();

					var apply = new TTApplyExpression { LeftSide = data };

					while (CurrentType != TTLexerTokenType.RightParen)
						apply.Arguments.Add(Expression());

					Match(TTLexerTokenType.RightParen);
					data = apply;
				} else {
					break;
				}
			}

			return data;
		}

        private TTExpression DataExpression () {
            if (CurrentType == TTLexerTokenType.Word) {
                return new TTIdentifierExpression { Name = Match().Text };
			} else if (CurrentType == TTLexerTokenType.LeftParen) {
				Match();
				var result = Expression();
				Match(TTLexerTokenType.RightParen);
				return result;
			} else if (CurrentType == TTLexerTokenType.LeftBrace) {
				var attrList = AttributeList();
				var dict = new TTDictionaryExpression();

				foreach (var attr in attrList)
					dict.Members[attr.Name] = attr.Value;

				return dict;
			} else {
				return Literal();
			}
        }

        private TTLiteralExpression Literal () {
            var result = new TTLiteralExpression();

            if (TokenIn(TTLexerTokenType.Number, TTLexerTokenType.String)) {
                var token = Match();

                if (token.Type == TTLexerTokenType.Number)
                    result.Value = decimal.Parse(token.Text);
                else
                    result.Value = token.Text;
            } else {
                Error("Expected expression, got {0}.", CurrentType);
            }

            return result;
        }
    }
}
