﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Nen.Collections;
using GM = Nen.Text.StringTemplate.GenericMath;
using System.Collections;
using System.Reflection;

namespace Nen.Text.TreeTemplate {
	public class TTXElementBuilder: ITTExpressionVisitor<object> {
		public IDictionary<string, object> Environment { get; set; }
		private Dictionary<string, TagLibrary> _tagLibraries = new Dictionary<string, TagLibrary>();

		public delegate object TagEvaluator (TTXElementBuilder evaluator, TTElementExpression expr);

		class TagLibrary {
			public Type Type { get; set; }
			public Dictionary<string, TagEvaluator> EvalCache { get; set; }
			public object Instance { get; set; }

			public TagLibrary () {
				EvalCache = new Dictionary<string, TagEvaluator>();
			}

			public TagLibrary (Type type): this() {
				Type = type;

				var instance = Activator.CreateInstance(Type);

				Instance = instance;

				foreach (var method in Type.GetMethods()) {
					if (method.ReturnType == typeof(object) && !method.IsGenericMethod) {
						var parameters = method.GetParameters();

						if (parameters.Length != 2 || parameters[0].ParameterType != typeof(TTXElementBuilder) ||
							parameters[1].ParameterType != typeof(TTElementExpression))
							continue;

						EvalCache[method.Name.ToLower()] =
							(TagEvaluator)Delegate.CreateDelegate(typeof(TagEvaluator), instance, method);
					}
				}

			}
		}

		private static bool IsTrue (object rv) {
			return !((rv is bool && (bool)rv == false) || rv == null);
		}

		public TTXElementBuilder () {
			Environment = new Dictionary<string, object>();
		}

		public TTXElementBuilder (IDictionary<string, object> environment) {
			Environment = environment;
		}

		public XElement Evaluate (TTExpression expr) {
			var result = expr.Accept(this);

			if (result is XElement)
				return (XElement)result;

			if (result is IEnumerable) {
				XElement xelement = null;

				foreach (object obj in (IEnumerable)result)
					if (obj is XElement)
						xelement = (XElement)obj;

				return xelement;
			}

			return null;
		}

		public void AddTagLibrary (string prefix, Type libraryType) {
			_tagLibraries[prefix] = new TagLibrary(libraryType);
		}

		public static XElement Convert (TTExpression expr, IDictionary<string, object> environment = null) {
			if (environment == null)
				environment = new Dictionary<string, object>();


			return new TTXElementBuilder(environment).Evaluate(expr);
		}

		public object Visit (TTDictionaryExpression expr) {
			return null;
		}

		public object Visit (TTConditionExpression expr) {
			return null;
		}

		public object Visit (TTUsingExpression expr) {
			var source = (IDisposable)expr.Source.Accept(this);
			
			try {
				if (expr.VariableName != null)
					Environment[expr.VariableName.Name] = source;

				var result = new List<object>();

				foreach (var child in expr.Children)
					result.Add(child.Accept(this));

				return result;
			} finally {
				source.Dispose();
			}
		}

		public object Visit (TTApplyExpression expr) {
			throw new NotImplementedException();
		}

		public object Visit (TTAttributeExpression expr) {
			var value = expr.Value.Accept(this);

			return new XAttribute(expr.Name, value);
		}

		public object Visit (TTForeachExpression expr) {
			var source = (IEnumerable)expr.Source.Accept(this);
			var result = new List<object>();
			foreach (object obj in source) {
				Environment[expr.VariableName.Name] = obj;

				foreach (var child in expr.Children)
					result.Add(child.Accept(this));
			}

			return result;
		}

		public object Visit (TTBinaryExpression expr) {
			if (expr.Operator == TTBinaryOperator.Assign) {
				var lval = (TTIdentifierExpression)expr.Left;
				var rval = expr.Right.Accept(this);
				Environment[lval.Name] = rval;

				return null;
			}

			var lhs = expr.Left.Accept(this);
			var rhs = expr.Right.Accept(this);

			switch (expr.Operator) {
				case TTBinaryOperator.Plus: return GM.Add(lhs, rhs);
				case TTBinaryOperator.Minus: return GM.Subtract(lhs, rhs);
				case TTBinaryOperator.Times: return GM.Multiply(lhs, rhs);
				case TTBinaryOperator.Divide: return GM.Divide(lhs, rhs);
			}

			throw new NotImplementedException();
		}

		public object Visit (TTCommentExpression expr) {
			throw new NotImplementedException();
		}

		public object Visit (TTContainerExpression expr) {
			return expr.Children.Select(c => c.Accept(this));
		}

		public object Visit (TTElementExpression expr) {
			if (expr.Name.Namespace != null) {
				var nsKey = expr.Name.Namespace.ToLower();
				TagLibrary library;


				if (_tagLibraries.TryGetValue(nsKey, out library)) {
					TagEvaluator eval;

					if (library.EvalCache.TryGetValue(expr.Name.Name.ToLower(), out eval))
						return eval(this, expr);
				}
			}

			return new XElement(expr.Name.Name ?? "div",
				expr.Class != null ? new XAttribute("class", expr.Class) : null,
				expr.ID != null ? new XAttribute("id", expr.ID) : null,
				expr.Attributes.Select(a => a.Accept(this)),
				expr.Children.Select(c => c.Accept(this)));
		}

		public object Visit (TTDirectiveExpression expr) {
			return null;
		}

		public object Visit (TTIdentifierExpression expr) {
			return Environment[expr.Name];
		}

		public object Visit (TTLiteralExpression expr) {
			return expr.Value;
		}

		public object Visit (TTMemberExpression expr) {
			var val = expr.LeftSide.Accept(this);

			Type type = val.GetType();

			PropertyInfo pi = type.GetProperty(expr.Member);

			if (pi != null)
				return pi.GetValue(val, null);

			FieldInfo fi = type.GetField(expr.Member);

			if (fi != null)
				return fi.GetValue(val);

			MethodBase[] mi = type.GetMethods();
			bool found = false;
			foreach (MemberInfo member in mi) {
				if (member.Name == expr.Member) {
					found = true;
					break;
				}
			}

			if (found)
				return new Nen.Text.StringTemplate.MethodFunction(val, expr.Member);

			pi = type.GetProperty("Item");
			if (pi != null)
				return pi.GetValue(val, new object[] { expr.Member });

			throw new Nen.Text.StringTemplate.UnboundFieldException(val, expr.Member);
		}

		public object Visit (TTImportDirectiveExpression expr) {
			_tagLibraries[expr.Prefix] = new TagLibrary(Type.GetType(expr.Url));

			return null;
		}

		public object Visit (TTUnaryExpression expr) {
			throw new NotImplementedException();
		}
	}
}
