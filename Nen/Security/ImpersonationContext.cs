﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Nen.Security {
    /// <summary>
    /// Login to a Windows account by user/password and impersonate it.
    /// </summary>
    public class ImpersonationContext : IDisposable {
        private IntPtr _token = IntPtr.Zero;
        private WindowsImpersonationContext _impersonationContext;

        #region Constructors
        /// <summary>
        /// Constructs a new ImpersonationContext.
        /// </summary>
        /// <param name="user">The user to impersonate.</param>
        /// <param name="pass">The user password.</param>
        public ImpersonationContext (string user, string pass) {
            var components = user.Split('\\');

            var userName = components.Length > 1 ? components[1] : components[0];
            var domainName = components.Length > 1 ? components[0] : Environment.MachineName;

            if (!NativeMethods.LogonUser(userName, domainName,
                pass, NativeMethods.LOGON32_LOGON_INTERACTIVE,
                NativeMethods.LOGON32_PROVIDER_DEFAULT, ref _token)) {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }

            var id = new WindowsIdentity(_token);
            _impersonationContext = id.Impersonate();
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Reverses the impersonation.
        /// </summary>
        public void Dispose () {
            _impersonationContext.Undo();
            NativeMethods.CloseHandle(_token);
        }
        #endregion
    }
}
