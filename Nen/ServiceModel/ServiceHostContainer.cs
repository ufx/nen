﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Nen.ServiceModel {
    public class ServiceHostContainer : IEnumerable<ServiceHost>, IDisposable {
        private List<ServiceHost> _hosts = new List<ServiceHost>();

        #region IDisposable Members
        public void Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose (bool disposing) {
            foreach (var host in _hosts)
                host.Dispose();

            _hosts.Clear();
        }
        #endregion

        #region Host List
        public void Add (ServiceHost host) {
            _hosts.Add(host);
        }

        public void Remove (ServiceHost host) {
            _hosts.Remove(host);
        }
        #endregion

        #region Commands
        /// <summary>
        /// Starts all contained hosts in the order they were added.
        /// </summary>
        public void StartAll () {
            foreach (var host in _hosts)
                host.Start();
        }

        /// <summary>
        /// Stops all contained hosts in the reverse of the order they were added.
        /// </summary>
        public void StopAll () {
            foreach (var host in Enumerable.Reverse(_hosts))
                host.Stop();
        }
        #endregion

        #region IEnumerable Members
        public IEnumerator<ServiceHost> GetEnumerator () {
            return _hosts.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator () {
            return _hosts.GetEnumerator();
        }
        #endregion
    }
}
