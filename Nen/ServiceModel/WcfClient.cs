﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Nen.ServiceModel {
    public abstract class WcfClient<T> : ClientBase<T> where T : class {
        #region Constructors
        protected WcfClient (string uri)
            : this(new Uri(uri)) {
        }

        protected WcfClient (Uri uri)
            : this(new WSHttpBinding(SecurityMode.None), new EndpointAddress(uri)) {
        }

        protected WcfClient (Binding binding, EndpointAddress remoteAddress) :
            base(binding, remoteAddress) {
        }
        #endregion

        public void Do (Action action) {
            try {
                action();
                Close();
            } catch (CommunicationException) {
                Abort();
                throw;
            }
        }

        public TResponse Do<TResponse> (Func<TResponse> func) {
            try {
                var response = func();
                Close();
                return response;
            } catch (CommunicationException) {
                Abort();
                throw;
            }
        }
    }
}
