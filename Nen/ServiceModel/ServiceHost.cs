﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Nen.Internal;

namespace Nen.ServiceModel {
    public enum ServiceHostState {
        Created,
        Starting,
        Started,
        Stopping,
        Stopped,
        Faulted
    }

    public interface IService {
        void Start ();
        void Stop ();
    }

    public abstract class ServiceHost : IDisposable {
        #region IDisposable Members
        public void Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose (bool disposing) {
            Stop();
        }
        #endregion

        public abstract void Start ();
        public abstract bool Stop ();

        #region State
        public event EventHandler<ServiceStateChangedEventArgs> StateChanged;

        protected virtual void OnStateChanged (ServiceHostState oldState, ServiceHostState newState, string message) {
            var e = StateChanged;
            if (e != null)
                e(this, new ServiceStateChangedEventArgs(oldState, newState, message));
        }


        protected void ChangeState (ServiceHostState newState, string message) {
            var oldState = State;
            State = newState;

            OnStateChanged(oldState, newState, message);
        }

        public ServiceHostState State { get; protected set; }
        #endregion
    }

    public class ServiceHost<TService> : ServiceHost where TService : class, IService, new() {
        private AppDomain _domain;
        private object _domainLock = new object();
        private ServiceProxy<TService> _proxy;

        #region Constructors
        public ServiceHost (string friendlyName) {
            FriendlyName = friendlyName;
        }

        public ServiceHost () {
            FriendlyName = typeof(TService).FullName;
        }
        #endregion

        #region ServiceHost Control
        public override void Start () {
            lock (_domainLock) {
                if (_proxy != null || _domain != null)
                    throw new InvalidOperationException("The service is already started.");

                ChangeState(ServiceHostState.Starting, "Service starting.");

                try {
                    _domain = AppDomain.CreateDomain(FriendlyName);

                    var proxyType = typeof(ServiceProxy<TService>);
                    _proxy = (ServiceProxy<TService>) _domain.CreateInstanceAndUnwrap(proxyType.Assembly.FullName, proxyType.FullName);
                    _proxy.Start();
                } catch (ThreadAbortException) {
                    throw;
                } catch (Exception ex) {
                    ChangeState(ServiceHostState.Faulted, ex.ToString());
                    throw;
                }

                ChangeState(ServiceHostState.Started, "Service started.");
            }
        }

        public override bool Stop () {
            lock (_domainLock) {
                if (_proxy == null)
                    return false;

                ChangeState(ServiceHostState.Stopping, "Service stopping.");

                // Reset state so ServiceHost can be started again.

                bool serviceStopped = false;

                try {
                    // Issue the stop command through the proxy.
                    serviceStopped = _proxy.Stop();
                    _proxy = null;

                    // Unload the application domain.
                    var domain = _domain;
                    _domain = null;
                    AppDomain.Unload(domain);
                } catch (ThreadAbortException) {
                    throw;
                } catch (Exception ex) {
                    ChangeState(ServiceHostState.Faulted, ex.ToString());
                    throw;
                }

                ChangeState(ServiceHostState.Stopped, "Service stopped.");

                return serviceStopped;
            }
        }
        #endregion

        #region Accessors
        public string FriendlyName { get; private set; }
        #endregion

        #region Object Members
        public override string ToString () {
            return "ServiceHost [" + typeof(TService).ToString() + "]";
        }
        #endregion
    }

    internal class ServiceProxy<TService> : MarshalByRefObject where TService : class, IService {
        private TService _service;

        public void Start () {
            _service = Activator.CreateInstance<TService>();
            _service.Start();
        }

        public bool Stop () {
            if (_service == null)
                return false;

            _service.Stop();
            _service = null;
            return true;
        }

        public override object InitializeLifetimeService () {
            // Ensure the proxy connection remains open no matter how long the
            // wait between calls.
            return null;
        }
    }

    public class ServiceStateChangedEventArgs : EventArgs {
        public ServiceStateChangedEventArgs (ServiceHostState oldState, ServiceHostState newState, string message) {
            OldState = oldState;
            NewState = newState;
            Message = message;
        }

        #region Accessors
        public ServiceHostState NewState { get; private set; }
        public ServiceHostState OldState { get; private set; }
        public string Message { get; private set; }
        #endregion
    }
}
