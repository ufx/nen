﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Nen.Configuration
{
    /// <summary>
    /// Extensions to the System.Configuration.ConfigurationManager class.
    /// </summary>
    public static class ConfigurationManagerEx
    {
        /// <summary>
        /// This is a horrible hack to set read-only configuration information
        /// at runtime.  Use at your own risk.
        /// </summary>
        /// <param name="key">The configuration key to set.</param>
        /// <param name="value">The configuration value to set.</param>
        public static void SetReadOnlyAppSetting(string key, string value)
        {
            var appSettingsType = ConfigurationManager.AppSettings.GetType();
            var rootField = appSettingsType.GetField("_root", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
            var root = (AppSettingsSection)rootField.GetValue(ConfigurationManager.AppSettings);

            var bReadOnlyField = typeof(ConfigurationElementCollection).GetField("bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
            bReadOnlyField.SetValue(root.Settings, false);

            ConfigurationManager.AppSettings[key] = value;

            bReadOnlyField.SetValue(root.Settings, true);
        }

        public static void ImportConfigurationAppSettings(string path)
        {
            if (path.EndsWith(".config"))
                path = path.Substring(0, path.Length - ".config".Length);

            var config = ConfigurationManager.OpenExeConfiguration(path);

            foreach (var key in config.AppSettings.Settings.AllKeys)
                SetReadOnlyAppSetting(key, config.AppSettings.Settings[key].Value);
        }
    }
}
