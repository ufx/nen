﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Nen.Reflection;
using System.Reflection;
using System.Linq.Expressions;

namespace Nen.Runtime.Serialization {
    /// <summary>
    /// Implements a surrogate type to be used for serializing references to
    /// singletons.
    /// </summary>
    /// <typeparam name="T">The singleton type to wrap.</typeparam>
    public class SingletonSurrogate<T>: IObjectReference {
        public string SingletonVariableName { get; set; }

        public SingletonSurrogate () { }
        public SingletonSurrogate (Expression<Func<T>> expr) {
            if (!(expr.Body is MemberExpression))
                throw new ArgumentOutOfRangeException("expr", "Expression must be a member access.");

            // TODO.
        }

        #region IObjectReference Members

        public object GetRealObject (StreamingContext context) {
            return typeof(T).GetVariable("Instance", BindingFlags.Public | BindingFlags.Static).GetValue(null);
        }

        #endregion
    }
}
