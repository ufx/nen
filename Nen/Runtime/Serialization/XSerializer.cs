﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;

using Nen.Collections;
using Nen.Reflection;
using Nen.Linq.Expressions;
using Nen.Internal;

#if NET4
using TypeWriterDef = System.Action<System.Xml.XmlWriter, object, Nen.Runtime.Serialization.XSerializer, bool>;
using TypeReaderDef = System.Func<System.Xml.XmlReader, Nen.Runtime.Serialization.XSerializer, string, object>;
#endif

// fixme: idrefs and surrogates broken

namespace Nen.Runtime.Serialization {
#if NET4
    public class XSerializer {
        private Dictionary<Type, int> _typeWriterIndexes;
        private Dictionary<string, int> _typeReaderIndexes;
        private bool _isSerializing;
        private bool _headerWritten;
        private XSerializerOptions _options;

        #region Constructors
        public XSerializer (Type root)
            : this(root, false, null) { }

        public XSerializer (Type root, params Type[] additionalTypes)
            : this(root, false, additionalTypes) { }

        public XSerializer (Type root, bool enableIdRefs, params Type[] additionalTypes)
            : this(new XSerializerOptions(root, enableIdRefs, additionalTypes)) { }

        public XSerializer (XSerializerOptions options) {
            _options = options;
            var builder = new XSerializerBuilder(options);
            builder.BuildSerializers();

            _typeWriterIndexes = builder.TypeWriterIndexes;
            _typeReaderIndexes = builder.TypeReaderIndexes.MapKeys(k => GetXDataContractName(k));
            TypeWriters = builder.TypeWriters.ToArray();
            TypeReaders = builder.TypeReaders.ToArray();
        }
        #endregion

        #region Serialization
        public void Serialize (Stream output, object instance, Encoding encoding) {
            using (var writer = XmlDictionaryWriter.CreateTextWriter(output, encoding, false)) {
                Serialize(writer, instance);
            }
        }

        public void Serialize (XmlWriter writer, object instance) {
            if (_options.WriteXmlHeader && !_headerWritten) {
                _headerWritten = true;
                writer.WriteProcessingInstruction("xml", "");
            }

            writer.WriteStartElement(instance.GetType().Name);
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");

            // todo: is that the right URL?
            if (_options.EnableIdRefs)
                writer.WriteAttributeString("xmlns", "z", null, "http://www.w3.org/XML/Schema");

            WriteObject(writer, instance);
            writer.WriteEndElement();
        }

        public void WriteObject (Stream output, object instance) {
            Serialize(output, instance, Encoding.UTF8);
        }

        public void WriteObject (XmlWriter writer, object instance) {
            if (instance == null)
                throw new ArgumentNullException("instance");

            var type = instance.GetType();

            if (!_typeWriterIndexes.ContainsKey(type))
                throw new SerializationException(LS.T("Type '{0}' not anticipated.  Pass it using the additionalTypes parameter.", type));


            var isFirstEntry = !_isSerializing;

            if (isFirstEntry) {
                if (!_options.PreserveIdRefs || IdsByObject == null)
                    IdsByObject = new Dictionary<object, int>();
                _isSerializing = true;
            }

            var index = _typeWriterIndexes[instance.GetType()];

            try {
                TypeWriters[index](writer, instance, this, false);
            } finally {
                if (isFirstEntry) {
                    if (!_options.PreserveIdRefs)
                        IdsByObject = null;
                    _isSerializing = false;
                }
            }
        }

        public string WriteObject (object instance) {
            return WriteObject(instance, Encoding.UTF8);
        }

        public string WriteObject (object instance, Encoding encoding) {
            using (var stream = new MemoryStream()) {
                Serialize(stream, instance, encoding);
                return encoding.GetString(stream.ToArray());
            }
        }
        #endregion

        #region Deserialization
        public object Deserialize (Stream input) {
            return ReadObject(input);
        }

        public object Deserialize (XmlReader reader) {
            return ReadObject(reader);
        }

        public object ReadObject (Stream input) {
            using (var reader = XmlDictionaryReader.CreateTextReader(input, XmlDictionaryReaderQuotas.Max))
                return ReadObject(reader);
        }

        public object ReadObject (XmlReader reader) {
            if (reader == null)
                throw new ArgumentNullException("reader");

            // Read type header to determine reader index to use.
            if (reader.NodeType == XmlNodeType.None && !reader.Read())
                return null;

            var isFirstEntry = !_isSerializing;

            if (isFirstEntry) {
                ObjectsById = new Dictionary<int, object>();
                _isSerializing = true;
            }

            try {
                reader.MoveToContent();

                var typeName = reader.GetAttribute("xsi:type");
                if (typeName == null)
                    typeName = reader.Name;

                return ReadObject(reader, typeName, reader.Name);
            } finally {
                if (isFirstEntry) {
                    ObjectsById = null;
                    _isSerializing = false;
                }
            }
        }

        internal object ReadObject (XmlReader reader, string typeName, string endElementName) {
            if (!_typeReaderIndexes.ContainsKey(typeName))
                throw new SerializationException(LS.T("Type '{0}' not anticipated.  Pass it using the additionalTypes parameter.", typeName));

            var index = _typeReaderIndexes[typeName];
            return TypeReaders[index](reader, this, endElementName);
        }

        public object ReadObject (string text) {
            return ReadObject(text, Encoding.UTF8);
        }

        public object ReadObject (string text, Encoding encoding) {
            using (var stream = new MemoryStream(encoding.GetBytes(text)))
                return ReadObject(stream);
        }
        #endregion

        #region Dynamic Serializer Builder
        private class XSerializerBuilder {
            private Dictionary<Type, Type> _surrogatesByType;
            private HashSet<Type> _types = new HashSet<Type>();
            private Dictionary<Type, bool> _hasSubTypes = new Dictionary<Type, bool>();
            private bool _enableIdRefs;
            private bool _writeOnly;

            // Members
            private MethodInfo _XmlReader_ReadElementContentAs;
            private MethodInfo _ConvertEx_ChangeWeakType;
            private MethodInfo _IList_Add;
            private MethodInfo _XmlReader_GetAttribute;
            private MethodInfo _Object_GetType, _Object_ToString;
            private MethodInfo _Type_GetTypeByName;
            private MethodInfo _ICustomSerializable_ReadXml, _ICustomSerializable_WriteXml;
            private MethodInfo _ISerializationLifecycle_AfterReadXml;
            private MethodInfo _Enum_Parse;
            private MethodInfo _Debug_WriteLine;
            private MethodInfo _XmlWriter_WriteStartElement, _XmlWriter_WriteValue;
            private MethodInfo _XmlWriter_WriteStartAttribute, _XmlWriter_WriteAttributeString;
            private MethodInfo _XSerializer_WriteObject, _XSerializer_ReadObject;
            private MethodInfo _ISerializationSurrogate_WriteObjectData, _ISerializationSurrogate_ReadObjectData;
            private MethodInfo _Dictionary_ObjectByInt_ContainsKey;
            private PropertyInfo _Dictionary_ObjectByInt_Indexer;

            // Parameters
            private ParameterExpression _reader, _writer;
            private ParameterExpression _serializer;
            private ParameterExpression _endElementName;

            // Instance Members
            private MethodCallExpression _reader_Read, _reader_MoveToFirstAttribute, _reader_MoveToNextAttribute, _reader_MoveToElement, _reader_MoveToContent, _reader_GetXsiTypeAttribute;
            private MemberExpression _reader_Name, _reader_Value, _reader_NodeType, _reader_IsEmptyElement, _reader_HasAttributes;
            private MethodCallExpression _serializer_ReadObject;
            private MemberExpression _serializer_ObjectsById, _serializer_IdsByObject;
            private MethodCallExpression _writer_WriteEndElement, _writer_WriteEndAttribute, _writer_WriteXsiNil;

            // Other Expressions
            private BinaryExpression _reader_NodeTypeEqualsEndElement;

            #region Constructors
            public XSerializerBuilder (XSerializerOptions options) {
                _types.Add(options.Root);
                _types.AddRange(options.AdditionalTypes);
                _types.AddRange(options.SurrogatesByType.Values);

                _enableIdRefs = options.EnableIdRefs;
                _surrogatesByType = options.SurrogatesByType;
                _writeOnly = options.WriteOnly;

                TypeReaderIndexes = new Dictionary<Type, int>();
                TypeReaders = new List<TypeReaderDef>();
                TypeWriterIndexes = new Dictionary<Type, int>();
                TypeWriters = new List<TypeWriterDef>();
            }
            #endregion

            public void BuildSerializers () {
                // Method Setup
                _ConvertEx_ChangeWeakType = typeof(ConvertEx).GetMethod("ChangeWeakType", new Type[] { typeof(object), typeof(Type) });
                _IList_Add = typeof(IList).GetMethod("Add", new Type[] { typeof(object) });
                _XmlReader_ReadElementContentAs = typeof(XmlReader).GetMethod("ReadElementContentAs", new Type[] { typeof(Type), typeof(IXmlNamespaceResolver) });
                _XmlReader_GetAttribute = typeof(XmlReader).GetMethod("GetAttribute", new Type[] { typeof(string) });
                _Dictionary_ObjectByInt_Indexer = typeof(Dictionary<int, object>).GetProperty("Item");
                _Dictionary_ObjectByInt_ContainsKey = typeof(Dictionary<int, object>).GetMethod("ContainsKey");
                _Type_GetTypeByName = typeof(Type).GetMethod("GetType", new Type[] { typeof(string), typeof(bool) });
                _Object_GetType = typeof(object).GetMethod("GetType");
                _Object_ToString = typeof(object).GetMethod("ToString");
                _ISerializationLifecycle_AfterReadXml = typeof(ISerializationLifecycle).GetMethod("AfterReadXml");
                _ICustomSerializable_ReadXml = typeof(ICustomSerializable).GetMethod("ReadXml");
                _ICustomSerializable_WriteXml = typeof(ICustomSerializable).GetMethod("WriteXml");
                _Enum_Parse = typeof(Enum).GetMethod("Parse", new Type[] { typeof(Type), typeof(string), typeof(bool) });
                _Debug_WriteLine = typeof(System.Diagnostics.Debug).GetMethod("WriteLine", new Type[] { typeof(string), typeof(object[]) });
                _XmlWriter_WriteStartElement = typeof(XmlWriter).GetMethod("WriteStartElement", new Type[] { typeof(string) });
                _XmlWriter_WriteStartAttribute = typeof(XmlWriter).GetMethod("WriteStartAttribute", new Type[] { typeof(string) });
                _XSerializer_WriteObject = typeof(XSerializer).GetMethod("WriteObject", new Type[] { typeof(XmlWriter), typeof(object) });
                _XmlWriter_WriteValue = typeof(XmlWriter).GetMethod("WriteValue", new Type[] { typeof(object) });
                _XmlWriter_WriteAttributeString = typeof(XmlWriter).GetMethod("WriteAttributeString", new Type[] { typeof(string), typeof(string) });
                _ISerializationSurrogate_ReadObjectData = typeof(ISerializationSurrogate).GetMethod("ReadObjectData");
                _ISerializationSurrogate_WriteObjectData = typeof(ISerializationSurrogate).GetMethod("WriteObjectData");
                _XSerializer_ReadObject = typeof(XSerializer).GetMethod("ReadObject", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance, null, new Type[] { typeof(XmlReader), typeof(string), typeof(string) }, null);

                // Parameter Setup
                _reader = Expression.Parameter(typeof(XmlReader), "reader");
                _writer = Expression.Parameter(typeof(XmlWriter), "writer");
                _serializer = Expression.Parameter(typeof(XSerializer), "serializer");
                _endElementName = Expression.Parameter(typeof(string), "endElementName");

                // Instance Member Setup
                _reader_Read = Expression.Call(_reader, typeof(XmlReader).GetMethod("Read"));
                _reader_MoveToFirstAttribute = Expression.Call(_reader, typeof(XmlReader).GetMethod("MoveToFirstAttribute"));
                _reader_MoveToNextAttribute = Expression.Call(_reader, typeof(XmlReader).GetMethod("MoveToNextAttribute"));
                _reader_MoveToContent = Expression.Call(_reader, typeof(XmlReader).GetMethod("MoveToContent"));
                _reader_MoveToElement = Expression.Call(_reader, typeof(XmlReader).GetMethod("MoveToElement"));
                _reader_Name = Expression.Property(_reader, "Name");
                _reader_Value = Expression.Property(_reader, "Value");
                _reader_NodeType = Expression.Property(_reader, "NodeType");
                _reader_IsEmptyElement = Expression.Property(_reader, "IsEmptyElement");
                _reader_HasAttributes = Expression.Property(_reader, "HasAttributes");
                _serializer_ReadObject = Expression.Call(_serializer, "ReadObject", null, _reader);
                _reader_GetXsiTypeAttribute = Expression.Call(_reader, _XmlReader_GetAttribute, Expression.Constant("xsi:type"));
                _serializer_ObjectsById = Expression.Property(_serializer, typeof(XSerializer).GetProperty("ObjectsById", BindingFlags.Instance | BindingFlags.NonPublic));
                _serializer_IdsByObject = Expression.Property(_serializer, typeof(XSerializer).GetProperty("IdsByObject", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic));
                _writer_WriteEndAttribute = Expression.Call(_writer, typeof(XmlWriter).GetMethod("WriteEndAttribute"));
                _writer_WriteEndElement = Expression.Call(_writer, typeof(XmlWriter).GetMethod("WriteEndElement"));
                _writer_WriteXsiNil = Expression.Call(_writer, _XmlWriter_WriteAttributeString, Expression.Constant("xsi:nil"), Expression.Constant("true"));

                // Other Expression Setup
                _reader_NodeTypeEqualsEndElement = Expression.Equal(_reader_NodeType, Expression.Constant(XmlNodeType.EndElement));

                // Setup hierarchy lookup.
                foreach (var type in _types)
                    HasSubTypes(type);

                // Build readers and writers for all types.
                foreach (var type in _types) {
                    if (!_writeOnly)
                        BuildReader(type);
                    BuildWriter(type);
                }
            }

            private bool HasSubTypes (Type type) {
                if (!_hasSubTypes.ContainsKey(type))
                    _hasSubTypes[type] = _types.Any(t => t != type && type.IsAssignableFrom(t));

                return _hasSubTypes[type];
            }

            public int BuildReader (Type contractType) {
                // Reserve space for this reader, or get the index of a 
                // reader that is already built.
                if (TypeReaderIndexes.ContainsKey(contractType))
                    return TypeReaderIndexes[contractType];

                Type surrogateType = null;
                if (_surrogatesByType.ContainsKey(contractType))
                    surrogateType = _surrogatesByType[contractType];

                if (contractType.GetAttribute<XDataContract>(true) == null && surrogateType != null)
                    throw new ArgumentException(LS.T("Contract type '{0}' must be marked with [XDataContract].", contractType), "contractType");

                TypeReaders.Add(null);
                var thisReaderIndex = TypeReaders.Count - 1;
                TypeReaderIndexes[contractType] = thisReaderIndex;

                // Begin the method.
                var method = new List<Expression>();

                // Locals.
                var locals = new List<ParameterExpression>();
                var instanceLocal = Expression.Parameter(contractType, "instance");
                locals.Add(instanceLocal);

                //method.Add(DebugWriteLine("XSerializer.Reader(Type: {0}, EndElementName: {1}, Name: {2}, NodeType: {3})", Expression.Constant(contractType.Name), _endElementName, _reader_Name, _reader_NodeType)); // DEBUG

                // Labels.
                var endLabel = Expression.Label("END");

                if (surrogateType != null) {
                    // Surrogates short-circuit this whole deal by substituting
                    // another type for serialization.  Read the surrogate instance
                    // and return the actual instance from it.

                    // S surrogateInstance = (S) serializer.TypeReaders[index](reader, serializer, true);
                    var surrogateInstanceLocal = Expression.Parameter(surrogateType, "surrogateInstance");
                    locals.Add(surrogateInstanceLocal);
                    method.Add(Expression.Assign(surrogateInstanceLocal, InvokeReader(surrogateType, _endElementName)));

                    // instance = (T) surrogateInstance.ReadObjectData(serializer);
                    method.Add(Expression.Assign(instanceLocal, Expression.Convert(Expression.Call(surrogateInstanceLocal, _ISerializationSurrogate_ReadObjectData, _serializer), contractType)));
                } else {
                    // For ambiguous types, find the right instance to create.
                    if (HasSubTypes(contractType)) {
                        // Setup

                        var findSubTypeBlock = new List<Expression>();

                        // string typeName = reader.GetAttribute("xsi:type");
                        var typeNameLocal = Expression.Parameter(typeof(string), "typeName");
                        findSubTypeBlock.Add(Expression.Assign(typeNameLocal, _reader_GetXsiTypeAttribute));

                        // if (typeName != null && typeName != "TypeName") {
                        //     instance = (T) serializer.ReadObject(reader, typeName);
                        //     goto END;
                        // }

                        var test = Expression.AndAlso(Expression.NotEqual(typeNameLocal, Expression.Constant(null, typeof(string))), Expression.NotEqual(typeNameLocal, Expression.Constant(contractType.Name)));
                        var ifWrongType = Expression.Block(
                            Expression.Assign(instanceLocal, Expression.Convert(Expression.Call(_serializer, _XSerializer_ReadObject, _reader, typeNameLocal, typeNameLocal), contractType)),
                            Expression.Goto(endLabel)
                            );
                        findSubTypeBlock.Add(Expression.IfThen(test, ifWrongType));

                        method.Add(Expression.Block(new ParameterExpression[] { typeNameLocal }, findSubTypeBlock));
                    } else if (contractType.IsAbstract)
                        throw new SerializationException(LS.T("Type {0} is abstract but has no detected subtypes.  Pass additional subtypes to the serializer.", contractType));

                    if (!contractType.IsAbstract) {
                        // Create the instance.
                        // T instance = new T();
                        method.Add(Expression.Assign(instanceLocal, Expression.New(contractType)));

                        // Read the ID reference.
                        if (_enableIdRefs) {
                            // Setup

                            var idBlock = new List<Expression>();

                            // int id = int.Parse(reader.GetAttribute("z:id"));
                            var intParse = typeof(int).GetMethod("Parse", new Type[] { typeof(string) });
                            var idLocal = Expression.Parameter(typeof(int), "id");
                            idBlock.Add(Expression.Assign(idLocal, Expression.Call(intParse, Expression.Call(_reader, _XmlReader_GetAttribute, Expression.Constant("z:id")))));

                            // if (serializer.ObjectsById.ContainsKey(id)) {
                            //     instance = (T) serializer.ObjectsById[id];
                            //     goto END;
                            // }

                            var untypedObjectByIdIndex = Expression.Property(_serializer_ObjectsById, _Dictionary_ObjectByInt_Indexer, idLocal);
                            var ifExists = Expression.Block(Expression.Assign(instanceLocal, Expression.Convert(untypedObjectByIdIndex, contractType)), Expression.Goto(endLabel));
                            idBlock.Add(Expression.IfThen(Expression.Call(_serializer_ObjectsById, _Dictionary_ObjectByInt_ContainsKey, idLocal), ifExists));

                            // serializer.ObjectsById[id] = instance;
                            idBlock.Add(Expression.Assign(untypedObjectByIdIndex, instanceLocal));

                            method.Add(Expression.Block(new ParameterExpression[] { idLocal }, idBlock));
                        }

                        // Override serialization of ICustomSerializable classes.
                        if (typeof(ICustomSerializable).IsAssignableFrom(contractType)) {
                            // instance.ReadXml(serializer, reader);
                            method.Add(Expression.Call(instanceLocal, _ICustomSerializable_ReadXml, _serializer, _reader, _endElementName));
                        } else {
                            // Read each XDataMember variable.
                            var variablesAndTypes = GetDataVariablesAndTypes(contractType);
                            var attributeVariables = variablesAndTypes.Where(v => v.Item2 == XDataType.Attribute).Select(v => v.Item1).ToArray();
                            var elementVariables = variablesAndTypes.Where(v => v.Item2 == XDataType.Element).Select(v => v.Item1).ToArray();

                            // First read attributes.
                            if (attributeVariables.Length > 0) {
                                var readAttributesLoop = new List<Expression>();
                                foreach (var attributeVariable in attributeVariables) {
                                    // instance.Foo = (int) ConvertEx.ChangeWeakType(reader.Value, typeof(int));
                                    var variableExpression = Expression.PropertyOrField(instanceLocal, attributeVariable.Name);
                                    var readValue = ReadSimpleValue(attributeVariable.MemberType, XDataType.Attribute);
                                    var assignConvertedValue = Expression.Assign(variableExpression, readValue);

                                    // if (reader.Name == "Foo") { ... the above ... }
                                    var readerNameEqualsAttribute = Expression.Equal(_reader_Name, Expression.Constant(attributeVariable.Name));
                                    readAttributesLoop.Add(Expression.IfThen(readerNameEqualsAttribute, assignConvertedValue));
                                }

                                // if (!reader.MoveToNextAttribute()) break;
                                var readAttributesBreak = Expression.Label();
                                readAttributesLoop.Add(Expression.IfThen(Expression.IsFalse(_reader_MoveToNextAttribute), Expression.Goto(readAttributesBreak)));

                                var readAttributesBlock = new List<Expression>();

                                // while (true) { ... the above ... }
                                readAttributesBlock.Add(Expression.Loop(Expression.Block(readAttributesLoop), readAttributesBreak));

                                // reader.MoveToElement();
                                readAttributesBlock.Add(_reader_MoveToElement);

                                // if (reader.HasAttributes && reader.MoveToFirstAttribute()) { ... loop ... }
                                method.Add(Expression.IfThen(Expression.AndAlso(_reader_HasAttributes, _reader_MoveToFirstAttribute), Expression.Block(readAttributesBlock)));
                            }

                            // Then read elements.
                            if (elementVariables.Length > 0) {
                                var readElementsLoop = new List<Expression>();
                                var readElementsBreak = Expression.Label();
                                var readElementsContinue = Expression.Label();

                                // if (!reader.Read()) break;
                                readElementsLoop.Add(Expression.IfThen(Expression.IsFalse(_reader_Read), Expression.Break(readElementsBreak)));

                                // PROCESS_ELEMENT:
                                var processElementLabel = Expression.Label("PROCESS_ELEMENT");
                                readElementsLoop.Add(Expression.Label(processElementLabel));

                                // if (reader.NodeType == XmlNodeType.EndElement && reader.Name == endElementName) break;
                                var readerNameEqualsTypeName = Expression.Equal(_reader_Name, _endElementName);
                                readElementsLoop.Add(Expression.IfThen(Expression.AndAlso(_reader_NodeTypeEqualsEndElement, readerNameEqualsTypeName), Expression.Break(readElementsBreak)));

                                // if (reader.IsEmptyElement && !reader.HasAttributes) continue;
                                var readHasData = Expression.AndAlso(_reader_IsEmptyElement, Expression.IsFalse(_reader_HasAttributes));
                                readElementsLoop.Add(Expression.IfThen(readHasData, Expression.Continue(readElementsContinue)));

                                foreach (var elementVariable in elementVariables) {
                                    var memberType = elementVariable.MemberType;
                                    var instanceVariable = Expression.PropertyOrField(instanceLocal, elementVariable.Name);

                                    var readVariable = new List<Expression>();

                                    //readVariable.Add(DebugWriteLine("XSerializer.ReadVariable(Variable: {0}, Name: {1}, NodeType: {2})", Expression.Constant(elementVariable.Name), _reader_Name, _reader_NodeType)); // DEBUG

                                    if (memberType.IsArray) {
                                        var itemType = memberType.GetElementType();
                                        var listType = typeof(List<>).MakeGenericType(itemType);
                                        var itemsLocal = Expression.Parameter(listType, "items");

                                        // List<T> items = new List<T>();
                                        // <read list>
                                        // instance.Foo = items.ToArray();
                                        readVariable.Add(Expression.Block(new ParameterExpression[] { itemsLocal },
                                            Expression.Assign(itemsLocal, Expression.New(listType)),
                                            ReadListWithImplicitAdvancement(itemsLocal, elementVariable.Name, ReadSimpleValue(itemType, XDataType.Element)),
                                            Expression.Assign(instanceVariable, Expression.Call(itemsLocal, "ToArray", null))));
                                    } else if (typeof(IList).IsAssignableFrom(memberType)) {
                                        // instance.Foo = new IList();
                                        readVariable.Add(Expression.Assign(instanceVariable, Expression.New(memberType)));

                                        var itemType = CollectionEx.GetItemType(memberType);
                                        if (itemType.IsSimple()) {
                                            readVariable.Add(ReadListWithImplicitAdvancement(instanceVariable, elementVariable.Name, ReadSimpleValue(itemType, XDataType.Element)));
                                        } else {
                                            BuildReader(itemType); // ensure reader exists for the base type. polymorphic lists still need subtypes from additionalTypes.
                                            readVariable.Add(ReadListWithExplicitAdvancement(instanceVariable, elementVariable.Name, Expression.Convert(_serializer_ReadObject, itemType)));
                                        }
                                    } else if (memberType.IsSimple()) {
                                        // instance.Foo = reader.ReadElementContentAs(T, null);
                                        var readValue = ReadSimpleValue(memberType, XDataType.Element);
                                        readVariable.Add(Expression.Assign(instanceVariable, readValue));
                                        // goto PROCESS_ELEMENT;
                                        readVariable.Add(Expression.Goto(processElementLabel));
                                    } else if (memberType == typeof(object)) {
                                        // Type.GetType(reader.GetAttribute("xsi:type"), true)
                                        var getDynamicType = Expression.Call(_Type_GetTypeByName, _reader_GetXsiTypeAttribute, Expression.Constant(true));

                                        // fixme: this is broken for enums
                                        // solution: call back into serializer to do a type check and call appropriate methods.

                                        // instance.Foo = reader.ReadElementContentAs(Type.GetType(reader.GetAttribute("xsi:type")), null);
                                        var readerReadElementContentAs = Expression.Call(_reader, _XmlReader_ReadElementContentAs, getDynamicType, Expression.Constant(null, typeof(IXmlNamespaceResolver)));
                                        readVariable.Add(Expression.Assign(instanceVariable, readerReadElementContentAs));

                                        // goto PROCESS_ELEMENT;
                                        readVariable.Add(Expression.Goto(processElementLabel));
                                    } else {
                                        // instance.Foo = (Type) serializer.TypeReaders[index](reader, serializer, "Type");
                                        var callReader = InvokeReader(memberType, Expression.Constant(elementVariable.Name));
                                        readVariable.Add(Expression.Assign(instanceVariable, callReader));
                                    }

                                    readElementsLoop.Add(Expression.IfThen(Expression.Equal(_reader_Name, Expression.Constant(elementVariable.Name)), Expression.Block(readVariable)));
                                }

                                // if (!reader.IsEmptyElement) { while (true) { ... the above ... } }
                                method.Add(
                                    Expression.IfThen(Expression.IsFalse(_reader_IsEmptyElement), 
                                        Expression.Loop(Expression.Block(readElementsLoop), readElementsBreak, readElementsContinue)));
                            } // if (elementVariables.Length > 0)
                        } // else (typeof(ICustomSerializable).IsAssignableFrom(contractType))
                    } // if (!contractType.IsAbstract)

                    // END:
                    method.Add(Expression.Label(endLabel));

                    //method.Add(DebugWriteLine("XSerializer.Reader(Type: {0}, EndElementName: {1}, Name: {2}, NodeType: {3}) END", Expression.Constant(contractType.Name), _endElementName, _reader_Name, _reader_NodeType)); // DEBUG

                    // Call lifecycle methods if applicable.
                    // instance.AfterReadXml(serializer, reader);
                    if (typeof(ISerializationLifecycle).IsAssignableFrom(contractType))
                        method.Add(Expression.Call(instanceLocal, _ISerializationLifecycle_AfterReadXml, _serializer, _reader));
                } // else (surrogateType != null)

                // return instance;
                method.Add(instanceLocal);

                // Construct and compile the method.
                var methodBody = Expression.Block(locals, method);
                var lambda = Expression.Lambda<TypeReaderDef>(methodBody, _reader, _serializer, _endElementName);
                var reader = lambda.Compile();

                // Store the method.
                TypeReaders[thisReaderIndex] = reader;
                return thisReaderIndex;
            }

            public int BuildWriter (Type contractType) {
                // Reserve space for this serializer, or get the index of a
                // serializer that is already built.
                if (TypeWriterIndexes.ContainsKey(contractType))
                    return TypeWriterIndexes[contractType];

                // Surrogates won't fail if they aren't marked with [XDataContract].
                Type surrogateType = null;
                if (_surrogatesByType.ContainsKey(contractType))
                    surrogateType = _surrogatesByType[contractType];

                if (contractType.GetAttribute<XDataContract>(true) == null && surrogateType == null)
                    throw new ArgumentException(LS.T("Contract type '{0}' must be marked with [XDataContract].", contractType), "contractType");

                // Populate a writer index now for recursive graphs.
                TypeWriters.Add(null);
                var thisWriterIndex = TypeWriters.Count - 1;
                TypeWriterIndexes[contractType] = thisWriterIndex;

                // Begin the method.
                var method = new List<Expression>();

                // Parameters.
                var untypedInstanceParameter = Expression.Parameter(typeof(object), "untypedInstance");
                var isAmbiguousParameter = Expression.Parameter(typeof(bool), "isAmbiguous");

                // Locals.
                var parameters = new List<ParameterExpression>();
                var instanceLocal = Expression.Parameter(contractType, "instance");
                parameters.Add(instanceLocal);

                // Labels.
                var endLabel = Expression.Label("END");
                var writeEndElementLabel = Expression.Label("WRITE_END_ELEMENT");

                // If the instance is null, don't do anything.
                // if (untypedInstance == null) return;
                method.Add(Expression.IfThen(ExpressionEx.IsNull(untypedInstanceParameter), Expression.Return(endLabel)));

                if (surrogateType != null) {
                    // Surrogates short-circuit this whole deal by substituting
                    // another type for serialization.  Create the surrogate instance
                    // and call into another serializer.

                    // S surrogateInstance = new S();
                    var surrogateInstanceLocal = Expression.Parameter(surrogateType, "surrogateInstance");
                    parameters.Add(surrogateInstanceLocal);
                    method.Add(Expression.Assign(surrogateInstanceLocal, Expression.New(surrogateType)));

                    // surrogateInstance.WriteObjectData(serializer, untypedInstance);
                    method.Add(Expression.Call(surrogateInstanceLocal, _ISerializationSurrogate_WriteObjectData, _serializer, untypedInstanceParameter));

                    // serializer.WriteObject(writer, surrogateInstance);
                    method.Add(Expression.Call(_serializer, _XSerializer_WriteObject, _writer, surrogateInstanceLocal));
                } else {
                    // If the instance is an ambiguous type, write the xs:type attribute
                    // and find the right subtype serializer to invoke.
                    if (HasSubTypes(contractType)) {
                        var resolveAmbiguousTypeBlock = new List<Expression>();

                        // Type instanceType = untypedInstance.GetType();
                        var instanceTypeLocal = Expression.Parameter(typeof(Type), "instanceType");
                        resolveAmbiguousTypeBlock.Add(Expression.Assign(instanceTypeLocal, Expression.Call(untypedInstanceParameter, _Object_GetType)));

                        // writer.WriteAttributeString("xsi:type", instanceType.Name);
                        var instanceTypeName = Expression.PropertyOrField(instanceTypeLocal, "Name");
                        //fixme: broken for non-system types.  need to assembly qualify names.
                        resolveAmbiguousTypeBlock.Add(Expression.Call(_writer, _XmlWriter_WriteAttributeString, Expression.Constant("xsi:type"), instanceTypeName));

                        // serializer.WriteObject(writer, untypedInstance);
                        resolveAmbiguousTypeBlock.Add(Expression.Call(_serializer, _XSerializer_WriteObject, _writer, untypedInstanceParameter));

                        // return;
                        resolveAmbiguousTypeBlock.Add(Expression.Return(endLabel));

                        // if (isAmbiguous) { ... the above ... }
                        var ifBody = Expression.Block(new ParameterExpression[] { instanceTypeLocal }, resolveAmbiguousTypeBlock.ToArray());
                        method.Add(Expression.IfThen(isAmbiguousParameter, ifBody));
                    }

                    // Convert the untyped instance into its typed version.
                    // T instance = (T) untypedInstance;
                    method.Add(Expression.Assign(instanceLocal, Expression.Convert(untypedInstanceParameter, contractType)));

                    // Write the ID reference.
                    if (_enableIdRefs) {
                        // if (serializer.IdsByObject.ContainsKey(untypedInstance)) {
                        //     writer.WriteAttributeString("z:id", serializer.IdsByObject[untypedInstance].ToString());
                        //     goto WRITE_END_LABEL;
                        // } else {
                        //     int id = serializer.IdsByObject.Count;
                        //     writer.WriteAttributeString("z", "id", null, id.ToString());
                        //     serializer.IdsByObject[untypedInstance] = id;
                        // }

                        // Common

                        var indexer = typeof(Dictionary<object, int>).GetProperty("Item");
                        var untypedInstanceIdIndex = Expression.Property(_serializer_IdsByObject, indexer, untypedInstanceParameter);

                        // Then

                        var untypedInstanceIdIndexToString = Expression.Call(untypedInstanceIdIndex, _Object_ToString);

                        var thenBlock = new List<Expression>();
                        //thenBlock.Add(DebugWriteLine("ID {0} exists for {1}, exiting", untypedInstanceIdIndex, Expression.Constant(contractType)));
                        thenBlock.Add(Expression.Call(_writer, _XmlWriter_WriteAttributeString, Expression.Constant("z:id"), untypedInstanceIdIndexToString));
                        thenBlock.Add(Expression.Return(writeEndElementLabel));

                        // Else

                        var count = Expression.Property(_serializer_IdsByObject, "Count");
                        var idLocal = Expression.Parameter(typeof(int), "id");
                        var idLocalToString = Expression.Call(idLocal, _Object_ToString);

                        var elseBlock = new List<Expression>();
                        elseBlock.Add(Expression.Assign(idLocal, count));
                        elseBlock.Add(Expression.Call(_writer, _XmlWriter_WriteAttributeString, Expression.Constant("z:id"), idLocalToString));
                        elseBlock.Add(Expression.Assign(untypedInstanceIdIndex, idLocal));

                        // Put it all together.
                        var containsKey = typeof(Dictionary<object, int>).GetMethod("ContainsKey");
                        var test = Expression.Call(_serializer_IdsByObject, containsKey, untypedInstanceParameter);
                        method.Add(Expression.IfThenElse(test, Expression.Block(thenBlock), Expression.Block(new ParameterExpression[] { idLocal }, elseBlock)));
                    }

                    //method.Add(DebugWriteLine("Writing {0}", Expression.Constant(contractType)));

                    // Override serialization of ICustomSerializable classes.
                    if (typeof(ICustomSerializable).IsAssignableFrom(contractType)) {
                        // instance.WriteXml(serializer, writer);
                        method.Add(Expression.Call(instanceLocal, _ICustomSerializable_WriteXml, _serializer, _writer));
                    } else {
                        // Write each XDataMember variable.
                        foreach (var variableAndType in GetDataVariablesAndTypes(contractType)) {
                            var writeExpressions = WriteVariable(variableAndType.Item1, variableAndType.Item2, instanceLocal);
                            method.AddRange(writeExpressions);
                        }
                    } // else (typeof(ICustomSerializable).IsAssignableFrom(contractType))

                    // Label just before the end element write.
                    method.Add(Expression.Label(writeEndElementLabel));
                } // else (surrogateType != null)

                // Label the end of the method.
                method.Add(Expression.Label(endLabel));

                // Construct and compile the method.
                var methodBody = Expression.Block(parameters, method);
                var lambda = Expression.Lambda<TypeWriterDef>(methodBody, _writer, untypedInstanceParameter, _serializer, isAmbiguousParameter);
                var writer = lambda.Compile();

                // Store the method.
                TypeWriters[thisWriterIndex] = writer;
                return thisWriterIndex;
            }

            private IEnumerable<Expression> WriteVariable (VariableInfo variable, XDataType dataType, ParameterExpression instanceLocal) {
                var memberType = variable.MemberType;

                //method.Add(DebugWriteLine("Writing variable {0}", Expression.Constant(variable.Name)));

                var writeVariable = new List<Expression>();
                var isNullableType = true;

                // Create a new element / attribute for the variable.
                if (dataType == XDataType.Element)
                    writeVariable.Add(Expression.Call(_writer, _XmlWriter_WriteStartElement, Expression.Constant(variable.Name)));
                else
                    writeVariable.Add(Expression.Call(_writer, _XmlWriter_WriteStartAttribute, Expression.Constant(variable.Name)));

                // Access the variable property or field.
                var instanceVariable = Expression.PropertyOrField(instanceLocal, variable.Name);

                if (typeof(IList).IsAssignableFrom(memberType)) {
                    var itemType = CollectionEx.GetItemType(memberType);

                    BlockExpression serializeChildren;

                    if (itemType.IsSimple()) {
                        var writeValueMethod = typeof(XmlWriter).GetMethod("WriteValue", new Type[] { itemType });

                        serializeChildren = ExpressionEx.ForEach(instanceVariable, e => {
                            return Expression.Block(
                                Expression.Call(_writer, _XmlWriter_WriteStartElement, Expression.Constant("Item")),
                                WriteSimpleValue(e, itemType, _writer, out isNullableType),
                                _writer_WriteEndElement
                                );
                        });

                        // fixme: this will break on serializing a list of nullable values.
                        isNullableType = true;
                    } else {
                        serializeChildren = ExpressionEx.ForEach(instanceVariable, e => {
                            var memberTypeName = Expression.Property(Expression.Call(e, _Object_GetType), "Name");

                            return Expression.Block(
                                Expression.Call(_writer, _XmlWriter_WriteStartElement, memberTypeName),
                                InvokeWriter(CollectionEx.GetItemType(memberType), e, true),
                                _writer_WriteEndElement);
                        });
                    }

                    var checkNullCollectionAndSerialize = Expression.IfThen(Expression.IsFalse(ExpressionEx.IsNull(instanceVariable)), serializeChildren);
                    writeVariable.Add(checkNullCollectionAndSerialize);
                } else if (memberType.IsSimple()) {
                    // Simple types are written directly within the element.
                    writeVariable.Add(WriteSimpleValue(instanceVariable, memberType, _writer, out isNullableType));
                } else if (memberType == typeof(object)) {
                    // writer.WriteAttributeString("xsi:type", instance.Foo.GetType().FullName);
                    var instanceTypeFullName = Expression.Property(Expression.Call(instanceVariable, _Object_GetType), "FullName");
                    writeVariable.Add(Expression.Call(_writer, _XmlWriter_WriteAttributeString, Expression.Constant("xsi:type"), instanceTypeFullName));

                    // writer.WriteValue(instance.Foo);
                    writeVariable.Add(Expression.Call(_writer, _XmlWriter_WriteValue, instanceVariable));
                } else {
                    var invokeTypeSerializer = InvokeWriter(memberType, instanceVariable, true);
                    writeVariable.Add(invokeTypeSerializer);
                }

                // End writing.
                if (dataType == XDataType.Element)
                    writeVariable.Add(_writer_WriteEndElement);
                else
                    writeVariable.Add(_writer_WriteEndAttribute);

                // Check the default value before writing anything.
                var defaultValueAttribute = variable.Member.GetAttribute<System.ComponentModel.DefaultValueAttribute>(true);
                bool isDefaultValueNull = false;
                if (defaultValueAttribute != null) {
                    var defaultValue = defaultValueAttribute.Value;
                    if (defaultValue == null)
                        isDefaultValueNull = true;

                    var isVariableNotDefault = Expression.NotEqual(instanceVariable, Expression.Constant(defaultValue, memberType));
                    ReplaceContents(writeVariable, Expression.IfThen(isVariableNotDefault, Expression.Block(writeVariable)));
                }

                // If the type is nullable, check before writing it out.
                if (isNullableType && !isDefaultValueNull) {
                    var isVariableNotNull = Expression.NotEqual(instanceVariable, Expression.Constant(null, memberType));
                    ReplaceContents(writeVariable, Expression.IfThen(isVariableNotNull, Expression.Block(writeVariable)));
                }

                return writeVariable;
            }

            private void ReplaceContents (List<Expression> list, Expression expression) {
                list.Clear();
                list.Add(expression);
            }

            private Expression InvokeWriter (Type contractType, Expression variableExpression, bool isAmbiguous) {
                var index = BuildWriter(contractType);

                var typeWritersMember = Expression.Property(_serializer, "TypeWriters");
                var typeWriterIndex = Expression.ArrayIndex(typeWritersMember, Expression.Constant(index));
                return Expression.Invoke(typeWriterIndex, _writer, variableExpression, _serializer, Expression.Constant(isAmbiguous));
            }

            private Expression InvokeReader (Type contractType, Expression endElementName) {
                var index = BuildReader(contractType);

                var typeReadersMember = Expression.Property(_serializer, "TypeReaders");
                var typeReaderIndex = Expression.ArrayIndex(typeReadersMember, Expression.Constant(index));
                return Expression.Convert(Expression.Invoke(typeReaderIndex, _reader, _serializer, endElementName), contractType);
            }

            private LoopExpression ReadListWithExplicitAdvancement (Expression list, string endElementName, Expression readExpression) {
                // Setup.

                var readListLoopBreak = Expression.Label();
                var readListLoop = new List<Expression>();

                // Write the loop.

                // if (!reader.Read()) break;
                readListLoop.Add(Expression.IfThen(Expression.IsFalse(_reader_Read), Expression.Break(readListLoopBreak)));

                // reader.MoveToContent();
                readListLoop.Add(_reader_MoveToContent);

                //readListLoop.Add(DebugWriteLine("XSerializer.ReadListExplicit(EndElementName: {0}, Name: {1}, NodeType: {2}) BEFORE", Expression.Constant(endElementName), _reader_Name, _reader_NodeType)); // DEBUG

                // if (reader.NodeType == XmlNodeType.EndElement && reader.Name == endElementName) break;
                var readerNameEqualsVariableName = Expression.Equal(_reader_Name, Expression.Constant(endElementName));
                var readerNodeTypeEqualsEndElement = Expression.Equal(_reader_NodeType, Expression.Constant(XmlNodeType.EndElement));
                var breakAtEndElement = Expression.IfThen(Expression.AndAlso(readerNodeTypeEqualsEndElement, readerNameEqualsVariableName), Expression.Break(readListLoopBreak));
                readListLoop.Add(breakAtEndElement);

                // instance.Foo.Add({ ... read expression ... });
                readListLoop.Add(Expression.Call(list, "Add", null, readExpression));

                //readListLoop.Add(DebugWriteLine("XSerializer.ReadListExplicit(EndElementName: {0}, Name: {1}, NodeType: {2}) AFTER", Expression.Constant(endElementName), _reader_Name, _reader_NodeType)); // DEBUG

                // while (true) { ... the above ... }
                return Expression.Loop(Expression.Block(readListLoop), readListLoopBreak);
            }

            private BlockExpression ReadListWithImplicitAdvancement (Expression list, string endElementName, Expression readExpression) {
                // Setup.

                var readListBlock = new List<Expression>();
                var readListLoopBreak = Expression.Label();
                var readListLoop = new List<Expression>();

                // Write block header.

                // if (!reader.Read()) break;
                readListBlock.Add(Expression.IfThen(Expression.IsFalse(_reader_Read), Expression.Break(readListLoopBreak)));

                // reader.MoveToContent();
                readListBlock.Add(_reader_MoveToContent);

                // Write the loop.
                //readListLoop.Add(DebugWriteLine("XSerializer.ReadListImplicit(EndElementName: {0}, Name: {1}, NodeType: {2}) BEFORE", Expression.Constant(endElementName), _reader_Name, _reader_NodeType)); // DEBUG

                // instance.Foo.Add({ ... read expression ... });
                readListLoop.Add(Expression.Call(list, "Add", null, readExpression));

                //readListLoop.Add(DebugWriteLine("XSerializer.ReadListImplicit(EndElementName: {0}, Name: {1}, NodeType: {2}) AFTER", Expression.Constant(endElementName), _reader_Name, _reader_NodeType)); // DEBUG

                // if (reader.NodeType == XmlNodeType.EndElement && reader.Name == endElementName) break;
                var readerNameEqualsVariableName = Expression.Equal(_reader_Name, Expression.Constant(endElementName));
                var readerNodeTypeEqualsEndElement = Expression.Equal(_reader_NodeType, Expression.Constant(XmlNodeType.EndElement));
                var breakAtEndElement = Expression.IfThen(Expression.AndAlso(readerNodeTypeEqualsEndElement, readerNameEqualsVariableName), Expression.Break(readListLoopBreak));
                readListLoop.Add(breakAtEndElement);

                // [header] while (true) { ... the above ... }
                readListBlock.Add(Expression.Loop(Expression.Block(readListLoop), readListLoopBreak));
                return Expression.Block(readListBlock);
            }

            private Tuple<VariableInfo, XDataType>[] GetDataVariablesAndTypes (Type type) {
                var variables = new List<Tuple<VariableInfo, XDataType>>();

                foreach (var variable in type.GetVariables()) {
                    var dataMember = variable.Member.GetAttribute<XDataMemberAttribute>(true);
                    if (dataMember == null)
                        continue;

                    // It's not possible to use attributes to serialize object
                    // types because the serializer relies on a separate xsi:type
                    // attribute to determine the type of the object.
                    if (variable.MemberType == typeof(object) && dataMember.XDataType == XDataType.Attribute)
                        throw new InvalidDataContractException(LS.T("Dynamic object member '{0}' on type '{1}' can not be serialized as an attribute.", variable, type));

                    variables.Add(new Tuple<VariableInfo, XDataType>(variable, dataMember.XDataType));
                }

                // Order the list by data type (attributes first), then by variable name.
                return variables.OrderBy(v => v.Item2).ThenBy(v => v.Item1.Name).ToArray();
            }

            private Expression WriteSimpleValue (Expression value, Type valueType, Expression writer, out bool isNullable) {
                var unwrappedValueType = valueType.UnwrapIfNullable();
                if (unwrappedValueType == valueType)
                    isNullable = false;
                else {
                    isNullable = true;
                    value = Expression.Property(value, "Value");
                }

                MethodInfo writeValueMethod;
                if (unwrappedValueType.IsEnum) {
                    writeValueMethod = typeof(XmlWriter).GetMethod("WriteValue", new Type[] { typeof(string) });
                    value = Expression.Call(value, _Object_ToString);
                } else
                    writeValueMethod = typeof(XmlWriter).GetMethod("WriteValue", new Type[] { unwrappedValueType });

                var parameterType = writeValueMethod.GetParameters()[0].ParameterType;
                if (value.Type != parameterType)
                    value = Expression.Convert(value, parameterType);

                return Expression.Call(writer, writeValueMethod, value);
            }

            private Expression ReadSimpleValue (Type valueType, XDataType dataType) {
                var unwrappedValueType = valueType.UnwrapIfNullable();
                var readType = valueType.IsEnum ? typeof(string) : unwrappedValueType;
                Expression value;

                switch (dataType) {
                    case XDataType.Attribute:
                        value = Expression.Call(_ConvertEx_ChangeWeakType, _reader_Value, Expression.Constant(readType));
                        break;

                    // reader.ReadElementContentAs(typeof(T), (IXmlNamespaceResolver) null);
                    case XDataType.Element:
                        value = Expression.Call(_reader, _XmlReader_ReadElementContentAs, Expression.Constant(readType), Expression.Constant(null, typeof(IXmlNamespaceResolver)));
                        break;

                    default:
                        throw new NotSupportedException();
                }

                // Enum.Parse(typeof(T), ... the above ..., true);
                if (valueType.IsEnum)
                    value = Expression.Call(_Enum_Parse, Expression.Constant(unwrappedValueType), Expression.Convert(value, typeof(string)), Expression.Constant(true));

                // (T) value;
                return Expression.Convert(value, valueType);
            }

            // todo: generalize
            private Expression DebugWriteLine (string format, params Expression[] args) {
                var debugArgs = new List<Expression>();
                if (args != null)
                    debugArgs.AddRange(args.Select(e => Expression.Convert(e, typeof(object))));

                return Expression.Call(_Debug_WriteLine, Expression.Constant(format), Expression.NewArrayInit(typeof(object), debugArgs));
            }

            #region Accessors
            public Dictionary<Type, int> TypeWriterIndexes { get; private set; }
            public List<TypeWriterDef> TypeWriters { get; private set; }
            public Dictionary<Type, int> TypeReaderIndexes { get; private set; }
            public List<TypeReaderDef> TypeReaders { get; private set; }
            #endregion
        }
        #endregion

        #region Utilities
        private static string GetXDataContractName (Type type) {
            var xdc = type.GetAttribute<XDataContract>(false);
            return xdc != null ? (xdc.ElementName ?? type.Name) : type.Name;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// This state is to be used to communicate extra objects to IXSerializable
        /// classes.  The serializer itself does not use it.
        /// </summary>
        public object State { get; set; }

        internal TypeWriterDef[] TypeWriters { get; private set; }
        internal TypeReaderDef[] TypeReaders { get; private set; }

        // These properties are transient use by the write and read serializers.
        // They are only stored during serialization.
        internal Dictionary<object, int> IdsByObject { get; private set; }
        internal Dictionary<int, object> ObjectsById { get; private set; }
        #endregion
    }

    public class XSerializerOptions {
        #region Constructors
        public XSerializerOptions ()
            : this(null, false, null) {
        }

        public XSerializerOptions (Type root)
            : this(root, false, null) {
        }

        public XSerializerOptions (Type root, params Type[] additionalTypes)
            : this(root, false, additionalTypes) {
        }

        public XSerializerOptions (Type root, bool enableIdRefs, params Type[] additionalTypes) {
            SurrogatesByType = new Dictionary<Type, Type>();
            EnableIdRefs = enableIdRefs;
            Root = root;

            AdditionalTypes = new Collection<Type>();
            if (additionalTypes != null)
                AdditionalTypes.AddRange(additionalTypes);
        }
        #endregion

        public void AddSurrogate<TBase, TSurrogate> () where TSurrogate : ISerializationSurrogate, new() {
            SurrogatesByType[typeof(TBase)] = typeof(TSurrogate);
        }

        public bool RemoveSurrogate<TBase> () {
            return SurrogatesByType.Remove(typeof(TBase));
        }

        public Type Root { get; set; }
        public Collection<Type> AdditionalTypes { get; set; }
        public bool EnableIdRefs { get; set; }
        public bool PreserveIdRefs { get; set; }
        internal Dictionary<Type, Type> SurrogatesByType { get; private set; }
        public bool WriteXmlHeader { get; set; }
        public bool WriteOnly { get; set; }
    }
#endif

    [AttributeUsage(AttributeTargets.Class)]
    public class XDataContract : Attribute {
        public string ElementName { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class XDataMemberAttribute : Attribute {
        #region Constructors
        public XDataMemberAttribute ()
            : this(XDataType.Element) {
        }

        public XDataMemberAttribute (XDataType type) {
            XDataType = type;
        }
        #endregion

        public XDataType XDataType { get; private set; }
    }

    public enum XDataType {
        Element = 1,
        Attribute = 0
    }

    /// <summary>
    /// Custom serializable classes override the serializer to control their
    /// serialization.  Use this when serializing your class isn't as simple as
    /// writing all its properties.
    /// </summary>
    public interface ICustomSerializable {
#if NET4
        void ReadXml (XSerializer serializer, XmlReader reader, string containerElementName);
        void WriteXml (XSerializer serializer, XmlWriter writer);
#endif
    }

    /// <summary>
    /// The serialization lifecycle methods are called at each point during
    /// the serialization or deserialization of the object.
    /// </summary>
    public interface ISerializationLifecycle {
#if NET4
        void AfterReadXml (XSerializer serializer, XmlReader reader);
#endif
    }

    /// <summary>
    /// Allows one object to perform serialization and deserialization of another.
    /// </summary>
    public interface ISerializationSurrogate {
#if NET4
        object ReadObjectData (XSerializer serializer);
        void WriteObjectData (XSerializer serializer, object obj);
#endif
    }
}
