﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Graphics {
    public class Triangle {
        public int V1, V2, V3;

        public Triangle () {
        }

        public Triangle (int v1, int v2, int v3) {
            this.V1 = v1;
            this.V2 = v2;
            this.V3 = v3;
        }
    }

    public struct Distance {
        public int ind;
        public double dist;

        public void copy (Distance distance) {
            ind = distance.ind;
            dist = distance.dist;
        }
    }

    public class Point3F : Tuple3F {
        public Point3F () { }
        public Point3F (float x, float y, float z) : base(x, y, z) { }
    }

    public class Point2F : Tuple2F {
        public Point2F () { }
        public Point2F (float x, float y) : base(x, y) { }
    }

    public class Tuple3F {
        public float X;
        public float Y;
        public float Z;

        public Tuple3F () { }
        public Tuple3F (float x, float y, float z) {
            this.X = x; this.Y = y; this.Z = z;
        }

        public void set (Tuple3F other) {
            X = other.X;
            Y = other.Y;
            Z = other.Z;
        }
    }

    public class Tuple4F {
        public float X;
        public float Y;
        public float Z;
        public float W;

        public Tuple4F () { }
        public Tuple4F (float x, float y, float z, float w) {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.W = w;
        }
    }

    public class Tuple2F {
        public float X;
        public float Y;

        public Tuple2F () {
        }

        public Tuple2F (float x, float y) {
            this.X = x;
            this.Y = y;
        }

        public void set (Tuple2F other) {
            X = other.X;
            Y = other.Y;
        }
    }

    public class Color3F : Tuple3F {
        public Color3F () { }
        public Color3F (float r, float g, float b) : base(r, g, b) { }
    }

    public class Color4F : Tuple4F {
        public Color4F () { }
        public Color4F (float r, float g, float b, float a) : base(r, g, b, a) { }
    }

    public class TexCoord2F : Tuple2F {
        public TexCoord2F () { }
        public TexCoord2F (float x, float y) : base(x, y) { }
        public TexCoord2F (Point2F pt) : base(pt.X, pt.Y) { }
    }

    public class TexCoord3F : Tuple3F {
        public TexCoord3F () { }
        public TexCoord3F (float x, float y, float z) : base(x, y, z) { }
        public TexCoord3F (Point3F pt) : base(pt.X, pt.Y, pt.Z) { }
    }

    public class TexCoord4F : Tuple4F {
        public TexCoord4F () { }
        public TexCoord4F (float x, float y, float z, float w) : base(x, y, z, w) { }
    }

    public struct Point3D {
        public double X;
        public double Y;
        public double Z;
    }
}
