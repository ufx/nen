﻿#pragma warning disable 1591, 1570

// This is ported from Java3D.

/*
 * Copyright (c) 2007 Sun Microsystems, Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistribution of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * - Redistribution in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 *
 * Neither the name of Sun Microsystems, Inc. or the names of
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY
 * EXCLUDED. SUN MICROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR
 * ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL,
 * CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND
 * REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR
 * INABILITY TO USE THIS SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 *
 * You acknowledge that this software is not designed, licensed or
 * intended for use in the design, construction, operation or
 * maintenance of any nuclear facility.
 *
 * $Revision: 1.4 $
 * $Date: 2007/02/09 17:20:21 $
 * $State: Exp $
 */

// ----------------------------------------------------------------------
//
// The reference to Fast Industrial Strength Triangulation (FIST) code
// in this release by Sun Microsystems is related to Sun's rewrite of
// an early version of FIST. FIST was originally created by Martin
// Held and Joseph Mitchell at Stony Brook University and is
// incorporated by Sun under an agreement with The Research Foundation
// of SUNY (RFSUNY). The current version of FIST is available for
// commercial use under a license agreement with RFSUNY on behalf of
// the authors and Stony Brook University.  Please contact the Office
// of Technology Licensing at Stony Brook, phone 631-632-9009, for
// licensing information.
//
// ----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Nen.Internal;

namespace Nen.Graphics {
    internal class ListNode {
        public int index;
        public int prev;
        public int next;
        public int convex;
        public int vcntIndex;

        public ListNode (int ind) {
            index = ind;
            prev = -1;
            next = -1;
            convex = 0;
            vcntIndex = -1;
        }

        public void SetCommonIndex (int comIndex) {
            vcntIndex = comIndex;
        }

        public int GetCommonIndex () {
            return vcntIndex;
        }
    }

    internal class Orientation {
        /**
         * determine the outer polygon and the orientation of the polygons; the
         * default orientation is CCW for the outer-most polygon, and CW for the
         * inner polygons. the polygonal loops are referenced by  loops[i1,..,i2-1].
         */
        public static void AdjustOrientation (Triangulator triRef, int i1, int i2) {

            double area;
            int i, outer;
            int ind;

            if (i1 >= i2)
                Console.WriteLine("Orientation:adjustOrientation Problem i1>=i2 !!!");

            if (triRef._numLoops >= triRef._maxNumPolyArea) {
                // System.out.println("Orientation:adjustOrientation Expanding polyArea array .");
                triRef._maxNumPolyArea = triRef._numLoops;
                double[] old = triRef._polyArea;
                triRef._polyArea = new double[triRef._maxNumPolyArea];
                if (old != null)
                    Array.Copy(old, 0, triRef._polyArea, 0, old.Length);
            }

            // for each contour, compute its signed area, i.e., its orientation. the
            // contour with largest area is assumed to be the outer-most contour.
            for (i = i1; i < i2; ++i) {
                ind = triRef._loops[i];
                triRef._polyArea[i] = PolygonArea(triRef, ind);
            }

            // determine the outer-most contour
            area = Math.Abs(triRef._polyArea[i1]);
            outer = i1;
            for (i = i1 + 1; i < i2; ++i) {
                if (area < Math.Abs(triRef._polyArea[i])) {
                    area = Math.Abs(triRef._polyArea[i]);
                    outer = i;
                }
            }

            // default: the outer contour is referenced by  loops[i1]
            if (outer != i1) {
                ind = triRef._loops[i1];
                triRef._loops[i1] = triRef._loops[outer];
                triRef._loops[outer] = ind;

                area = triRef._polyArea[i1];
                triRef._polyArea[i1] = triRef._polyArea[outer];
                triRef._polyArea[outer] = area;
            }

            // adjust the orientation
            if (triRef._polyArea[i1] < 0.0) triRef.swapLinks(triRef._loops[i1]);
            for (i = i1 + 1; i < i2; ++i) {
                if (triRef._polyArea[i] > 0.0) triRef.swapLinks(triRef._loops[i]);
            }
        }

        /**
         * This function computes twice the signed area of a simple closed polygon.
         */
        static double PolygonArea (Triangulator triRef, int ind) {
            int hook = 0;
            int ind1, ind2;
            int i1, i2;
            double area = 0.0, area1 = 0;

            ind1 = ind;
            i1 = triRef.fetchData(ind1);
            ind2 = triRef.fetchNextData(ind1);
            i2 = triRef.fetchData(ind2);
            area = Numerics.stableDet2D(triRef, hook, i1, i2);

            ind1 = ind2;
            i1 = i2;
            while (ind1 != ind) {
                ind2 = triRef.fetchNextData(ind1);
                i2 = triRef.fetchData(ind2);
                area1 = Numerics.stableDet2D(triRef, hook, i1, i2);
                area += area1;
                ind1 = ind2;
                i1 = i2;
            }

            return area;
        }


        /**
         * Determine the orientation of the polygon. The default orientation is CCW.
         */
        public static void DetermineOrientation (Triangulator triRef, int ind) {
            double area;

            // compute the polygon's signed area, i.e., its orientation.
            area = PolygonArea(triRef, ind);

            // adjust the orientation (i.e., make it CCW)
            if (area < 0.0) {
                triRef.swapLinks(ind);
                triRef._ccwLoop = false;
            }

        }

    }


    internal struct PntNode {
        public int pnt;
        public int next;
    }

    public struct Left {
        public int ind;
        public int index;

        public void copy (Left l) {
            ind = l.ind;
            index = l.index;
        }
    }

    public class Vector3F : Tuple3F {
        public Vector3F () { }
        public Vector3F (float x, float y, float z) : base(x, y, z) { }
    }

    public struct Color3B {
        public byte x;
        public byte y;
        public byte z;
    }

    public struct Color4B {
        public byte x;
        public byte y;
        public byte z;
        public byte w;
    }


    /**
 * Bounding Box class for Triangulator.
 */
    class BBox {
        public int imin;           /* lexicographically smallest point, determines min-x */
        public int imax;           /* lexicographically largest point, determines max-x  */
        public double ymin;        /* minimum y-coordinate                               */
        public double ymax;        /* maximum y-coordinate                               */

        /**
         * This constructor computes the bounding box of a line segment whose end
         * points  i, j  are sorted according to x-coordinates.
         */
        public BBox (Triangulator triRef, int i, int j) {
            imin = Math.Min(i, j);
            imax = Math.Max(i, j);
            ymin = Math.Min(triRef._points[imin].Y, triRef._points[imax].Y);
            ymax = Math.Max(triRef._points[imin].Y, triRef._points[imax].Y);
        }


        public bool pntInBBox (Triangulator triRef, int i) {
            return (((imax < i) ? false :
                 ((imin > i) ? false :
                  ((ymax < triRef._points[i].Y) ? false :
                   ((ymin > triRef._points[i].Y) ? false : true)))));
        }



        public bool BBoxOverlap (BBox bb) {
            return (((imax < (bb).imin) ? false :
                 ((imin > (bb).imax) ? false :
                  ((ymax < (bb).ymin) ? false :
                   ((ymin > (bb).ymax) ? false : true)))));
        }

        public bool BBoxContained (BBox bb) {
            return ((imin <= (bb).imin) && (imax >= (bb).imax) &&
                (ymin <= (bb).ymin) && (ymax >= (bb).ymax));
        }


        public bool BBoxIdenticalLeaf (BBox bb) {
            return ((imin == (bb).imin) && (imax == (bb).imax));
        }


        public void BBoxUnion (BBox bb1, BBox bb3) {
            (bb3).imin = Math.Min(imin, (bb1).imin);
            (bb3).imax = Math.Max(imax, (bb1).imax);
            (bb3).ymin = Math.Min(ymin, (bb1).ymin);
            (bb3).ymax = Math.Max(ymax, (bb1).ymax);
        }


        public double BBoxArea (Triangulator triRef) {
            return (triRef._points[imax].X - triRef._points[imin].X) * (ymax - ymin);
        }
    }

    class BottleNeck {

        static bool checkArea (Triangulator triRef, int ind4, int ind5) {
            int ind1, ind2;
            int i0, i1, i2;
            double area = 0.0, area1 = 0, area2 = 0.0;

            i0 = triRef.fetchData(ind4);
            ind1 = triRef.fetchNextData(ind4);
            i1 = triRef.fetchData(ind1);

            while (ind1 != ind5) {
                ind2 = triRef.fetchNextData(ind1);
                i2 = triRef.fetchData(ind2);
                area = Numerics.stableDet2D(triRef, i0, i1, i2);
                area1 += area;
                ind1 = ind2;
                i1 = i2;
            }

            if (Numerics.le(area1, Triangulator.ZERO)) return false;

            ind1 = triRef.fetchNextData(ind5);
            i1 = triRef.fetchData(ind1);
            while (ind1 != ind4) {
                ind2 = triRef.fetchNextData(ind1);
                i2 = triRef.fetchData(ind2);
                area = Numerics.stableDet2D(triRef, i0, i1, i2);
                area2 += area;
                ind1 = ind2;
                i1 = i2;
            }

            if (Numerics.le(area2, Triangulator.ZERO)) return false;
            else return true;
        }


        // Yet another check needed in order to handle degenerate cases!
        public static bool checkBottleNeck (Triangulator triRef,
                       int i1, int i2, int i3, int ind4) {
            int ind5;
            int i4, i5;
            bool flag;

            i4 = i1;

            ind5 = triRef.fetchPrevData(ind4);
            i5 = triRef.fetchData(ind5);
            if ((i5 != i2) && (i5 != i3)) {
                flag = Numerics.pntInTriangle(triRef, i1, i2, i3, i5);
                if (flag) return true;
            }

            if (i2 <= i3) {
                if (i4 <= i5) flag = Numerics.segIntersect(triRef, i2, i3, i4, i5, -1);
                else flag = Numerics.segIntersect(triRef, i2, i3, i5, i4, -1);
            } else {
                if (i4 <= i5) flag = Numerics.segIntersect(triRef, i3, i2, i4, i5, -1);
                else flag = Numerics.segIntersect(triRef, i3, i2, i5, i4, -1);
            }
            if (flag) return true;

            ind5 = triRef.fetchNextData(ind4);
            i5 = triRef.fetchData(ind5);

            if ((i5 != i2) && (i5 != i3)) {
                flag = Numerics.pntInTriangle(triRef, i1, i2, i3, i5);
                if (flag) return true;
            }

            if (i2 <= i3) {
                if (i4 <= i5) flag = Numerics.segIntersect(triRef, i2, i3, i4, i5, -1);
                else flag = Numerics.segIntersect(triRef, i2, i3, i5, i4, -1);
            } else {
                if (i4 <= i5) flag = Numerics.segIntersect(triRef, i3, i2, i4, i5, -1);
                else flag = Numerics.segIntersect(triRef, i3, i2, i5, i4, -1);
            }

            if (flag) return true;

            ind5 = triRef.fetchNextData(ind4);
            i5 = triRef.fetchData(ind5);
            while (ind5 != ind4) {
                if (i4 == i5) {
                    if (checkArea(triRef, ind4, ind5)) return true;
                }
                ind5 = triRef.fetchNextData(ind5);
                i5 = triRef.fetchData(ind5);
            }

            return false;
        }
    }

    class Project {

        /**
         * This function projects the vertices of the polygons referenced by
         * loops[i1,..,i2-1] to an approximating plane.
         */
        public static void projectFace (Triangulator triRef, int loopMin, int loopMax) {
            Vector3F normal, nr;
            int i, j;
            double d;

            normal = new Vector3F();
            nr = new Vector3F();

            // determine the normal of the plane onto which the points get projected
            determineNormal(triRef, triRef._loops[loopMin], normal);
            j = loopMin + 1;
            if (j < loopMax) {
                for (i = j; i < loopMax; ++i) {
                    determineNormal(triRef, triRef._loops[i], nr);
                    if (Basic.dotProduct(normal, nr) < 0.0) {
                        Basic.invertVector(nr);
                    }
                    Basic.vectorAdd(normal, nr, normal);
                }
                d = Basic.lengthL2(normal);
                if (Numerics.gt(d, Triangulator.ZERO)) {
                    Basic.divScalar(d, normal);
                } else {
                    // System.out.println("*** ProjectFace: zero-length normal vector!? ***\n");
                    normal.X = normal.Y = 0.0f;
                    normal.Z = 1.0f;
                }
            }

            // project the points onto this plane. the projected points are stored in
            // the array `points[0,..,numPoints]'

            // System.out.println("loopMin " + loopMin + " loopMax " + loopMax);
            projectPoints(triRef, loopMin, loopMax, normal);

        }


        /**
         * This function computes the average of all normals defined by triples of
         * successive vertices of the polygon. we'll see whether this is a good
         * heuristic for finding a suitable plane normal...
         */
        static void determineNormal (Triangulator triRef, int ind, Vector3F normal) {
            Vector3F nr, pq, pr;
            int ind0, ind1, ind2;
            int i0, i1, i2;
            double d;

            ind1 = ind;
            i1 = triRef.fetchData(ind1);
            ind0 = triRef.fetchPrevData(ind1);
            i0 = triRef.fetchData(ind0);
            ind2 = triRef.fetchNextData(ind1);
            i2 = triRef.fetchData(ind2);
            pq = new Vector3F();
            Basic.vectorSub((Tuple3F)triRef._vertices[i0], (Tuple3F)triRef._vertices[i1], (Vector3F)pq);
            pr = new Vector3F();
            Basic.vectorSub((Tuple3F)triRef._vertices[i2], (Tuple3F)triRef._vertices[i1], (Vector3F)pr);
            nr = new Vector3F();
            Basic.vectorProduct(pq, pr, nr);
            d = Basic.lengthL2(nr);
            if (Numerics.gt(d, Triangulator.ZERO)) {
                Basic.divScalar(d, nr);
                normal.set(nr);
            } else {
                normal.X = normal.Y = normal.Z = 0.0f;
            }

            pq.set(pr);
            ind1 = ind2;
            ind2 = triRef.fetchNextData(ind1);
            i2 = triRef.fetchData(ind2);
            while (ind1 != ind) {
                Basic.vectorSub((Tuple3F)triRef._vertices[i2], (Tuple3F)triRef._vertices[i1], pr);
                Basic.vectorProduct(pq, pr, nr);
                d = Basic.lengthL2(nr);
                if (Numerics.gt(d, Triangulator.ZERO)) {
                    Basic.divScalar(d, nr);
                    if (Basic.dotProduct(normal, nr) < 0.0) {
                        Basic.invertVector(nr);
                    }
                    Basic.vectorAdd(normal, nr, normal);
                }
                pq.set(pr);
                ind1 = ind2;
                ind2 = triRef.fetchNextData(ind1);
                i2 = triRef.fetchData(ind2);
            }

            d = Basic.lengthL2(normal);
            if (Numerics.gt(d, Triangulator.ZERO)) {
                Basic.divScalar(d, normal);
            } else {
                //System.out.println("*** DetermineNormal: zero-length normal vector!? ***\n");
                normal.X = normal.Y = 0.0f; normal.Z = 1.0f;

            }
        }


        /**
         * This function maps the vertices of the polygon referenced by `ind' to the
         * plane  n3.x * x + n3.y * y + n3.z * z = 0. every mapped vertex  (x,y,z)
         * is then expressed in terms of  (x',y',z'),  where  z'=0.  this is
         * achieved by transforming the original vertices into a coordinate system
         * whose z-axis coincides with  n3,  and whose two other coordinate axes  n1
         * and  n2  are orthonormal on  n3. note that n3 is supposed to be of unit
         * length!
         */
        static void projectPoints (Triangulator triRef, int i1, int i2, Vector3F n3) {
            Matrix4F matrix = new Matrix4F();
            Point3F vtx = new Point3F();
            Vector3F n1, n2;
            double d;
            int ind, ind1;
            int i, j1;


            n1 = new Vector3F();
            n2 = new Vector3F();

            // choose  n1  and  n2  appropriately
            if ((Math.Abs(n3.X) > 0.1) || (Math.Abs(n3.Y) > 0.1)) {
                n1.X = -n3.Y;
                n1.Y = n3.X;
                n1.Z = 0.0f;
            } else {
                n1.X = n3.Z;
                n1.Z = -n3.X;
                n1.Y = 0.0f;
            }
            d = Basic.lengthL2(n1);
            Basic.divScalar(d, n1);
            Basic.vectorProduct(n1, n3, n2);
            d = Basic.lengthL2(n2);
            Basic.divScalar(d, n2);

            // initialize the transformation matrix
            matrix.m00 = n1.X;
            matrix.m01 = n1.Y;
            matrix.m02 = n1.Z;
            matrix.m03 = 0.0f;       // translation of the coordinate system
            matrix.m10 = n2.X;
            matrix.m11 = n2.Y;
            matrix.m12 = n2.Z;
            matrix.m13 = 0.0f;       // translation of the coordinate system
            matrix.m20 = n3.X;
            matrix.m21 = n3.Y;
            matrix.m22 = n3.Z;
            matrix.m23 = 0.0f;       // translation of the coordinate system
            matrix.m30 = 0.0f;
            matrix.m31 = 0.0f;
            matrix.m32 = 0.0f;
            matrix.m33 = 1.0f;

            // transform the vertices and store the transformed vertices in the array
            // `points'
            triRef.initPnts(20);
            for (i = i1; i < i2; ++i) {
                ind = triRef._loops[i];
                ind1 = ind;
                j1 = triRef.fetchData(ind1);
                matrix.transform((Point3F)triRef._vertices[j1], vtx);
                j1 = triRef.storePoint(vtx.X, vtx.Y);
                triRef.updateIndex(ind1, j1);
                ind1 = triRef.fetchNextData(ind1);
                j1 = triRef.fetchData(ind1);
                while (ind1 != ind) {
                    matrix.transform(triRef._vertices[j1], vtx);
                    j1 = triRef.storePoint(vtx.X, vtx.Y);
                    triRef.updateIndex(ind1, j1);
                    ind1 = triRef.fetchNextData(ind1);
                    j1 = triRef.fetchData(ind1);
                }
            }
        }

    }


    class Simple {
        /**
         * Handle a triangle or a quadrangle in a simple and efficient way. if the
         * face is more complex than  false  is returned.
         *
         * warning: the correctness of this function depends upon the fact that
         *          `CleanPolyhedralFace' has not yet been executed; i.e., the
         *          vertex indices have not been changed since the execution of
         *          `CleanPolyhedron'! (otherwise, we would have to get the original
         *          indices via calls to `GetOriginal'...)
         */
        public static bool simpleFace (Triangulator triRef, int ind1) {
            int ind0, ind2, ind3, ind4;
            int i1, i2, i3, i0, i4;

            Point3F pq, pr, nr;

            double x, y, z;
            int ori2, ori4;

            ind0 = triRef.fetchPrevData(ind1);
            i0 = triRef.fetchData(ind0);

            if (ind0 == ind1) {
                // this polygon has only one vertex! nothing to triangulate...
                Console.WriteLine("***** polygon with only one vertex?! *****\n");
                return true;
            }

            ind2 = triRef.fetchNextData(ind1);
            i2 = triRef.fetchData(ind2);
            if (ind0 == ind2) {
                // this polygon has only two vertices! nothing to triangulate...
                Console.WriteLine("***** polygon with only two vertices?! *****\n");
                return true;
            }

            ind3 = triRef.fetchNextData(ind2);
            i3 = triRef.fetchData(ind3);
            if (ind0 == ind3) {
                // this polygon is a triangle! let's triangulate it!
                i1 = triRef.fetchData(ind1);
                // triRef.storeTriangle(i1, i2, i3);
                triRef.storeTriangle(ind1, ind2, ind3);
                return true;
            }

            ind4 = triRef.fetchNextData(ind3);
            i4 = triRef.fetchData(ind4);
            if (ind0 == ind4) {
                // this polygon is a quadrangle! not too hard to triangulate it...
                // we project the corners of the quadrangle onto one of the coordinate
                // planes
                triRef.initPnts(5);
                i1 = triRef.fetchData(ind1);

                pq = new Point3F();
                pr = new Point3F();
                nr = new Point3F();
                /*
                  System.out.println("ind0 " + ind0 + ", ind1 " + ind1 + ", ind2 " +
                  ind2 + ", ind3 " + ind3 + ", ind4 " + ind4);
                  System.out.println("i0 " + i0 +", i1 " + i1 + ", i2 " + i2 +
                  ", i3 " + i3 + ", i4 " + i4);

                  System.out.println("vert[i1] " + triRef.vertices[i1] +
                  "vert[i2] " + triRef.vertices[i2] +
                  "vert[i3] " + triRef.vertices[i3]);
                */

                Basic.vectorSub(triRef._vertices[i1], triRef._vertices[i2], pq);
                Basic.vectorSub(triRef._vertices[i3], triRef._vertices[i2], pr);
                Basic.vectorProduct(pq, pr, nr);

                // System.out.println("pq " + pq + " pr " + pr + " nr " + nr);
                x = Math.Abs(nr.X);
                y = Math.Abs(nr.Y);
                z = Math.Abs(nr.Z);
                if ((z >= x) && (z >= y)) {
                    // System.out.println("((z >= x)  &&  (z >= y))");
                    triRef._points[1].X = triRef._vertices[i1].X;
                    triRef._points[1].Y = triRef._vertices[i1].Y;
                    triRef._points[2].X = triRef._vertices[i2].X;
                    triRef._points[2].Y = triRef._vertices[i2].Y;
                    triRef._points[3].X = triRef._vertices[i3].X;
                    triRef._points[3].Y = triRef._vertices[i3].Y;
                    triRef._points[4].X = triRef._vertices[i4].X;
                    triRef._points[4].Y = triRef._vertices[i4].Y;
                } else if ((x >= y) && (x >= z)) {
                    // System.out.println("((x >= y)  &&  (x >= z))");
                    triRef._points[1].X = triRef._vertices[i1].Z;
                    triRef._points[1].Y = triRef._vertices[i1].Y;
                    triRef._points[2].X = triRef._vertices[i2].Z;
                    triRef._points[2].Y = triRef._vertices[i2].Y;
                    triRef._points[3].X = triRef._vertices[i3].Z;
                    triRef._points[3].Y = triRef._vertices[i3].Y;
                    triRef._points[4].X = triRef._vertices[i4].Z;
                    triRef._points[4].Y = triRef._vertices[i4].Y;
                } else {
                    triRef._points[1].X = triRef._vertices[i1].X;
                    triRef._points[1].Y = triRef._vertices[i1].Z;
                    triRef._points[2].X = triRef._vertices[i2].X;
                    triRef._points[2].Y = triRef._vertices[i2].Z;
                    triRef._points[3].X = triRef._vertices[i3].X;
                    triRef._points[3].Y = triRef._vertices[i3].Z;
                    triRef._points[4].X = triRef._vertices[i4].X;
                    triRef._points[4].Y = triRef._vertices[i4].Z;
                }
                triRef._numPoints = 5;

                // find a valid diagonal
                ori2 = Numerics.orientation(triRef, 1, 2, 3);
                ori4 = Numerics.orientation(triRef, 1, 3, 4);

                /*
                  for(int i=0; i<5; i++)
                  System.out.println("point " + i + ", " + triRef.points[i]);
                  System.out.println("ori2 : " + ori2 + " ori4 : " + ori4);
                */

                if (((ori2 > 0) && (ori4 > 0)) ||
                ((ori2 < 0) && (ori4 < 0))) {

                    // i1, i3  is a valid diagonal;
                    //
                    // encode as a 2-triangle strip: the triangles are  (2, 3, 1)
                    // and  (1, 3, 4).

                    // triRef.storeTriangle(i1, i2, i3);
                    // triRef.storeTriangle(i1, i3, i4);
                    triRef.storeTriangle(ind1, ind2, ind3);
                    triRef.storeTriangle(ind1, ind3, ind4);
                } else {
                    // i2, i4  has to be a valid diagonal. (if this is no valid
                    // diagonal then the corners of the quad form a figure of eight;
                    // shall we apply any heuristics in order to guess which diagonal
                    // is more likely to be the better choice? alternatively, we could
                    // return  false  and subject it to the standard triangulation
                    // algorithm. well, let's see how this brute-force solution works.)

                    // encode as a 2-triangle strip: the triangles are  (1, 2, 4)
                    // and (4, 2, 3).

                    // triRef.storeTriangle(i2, i3, i4);
                    // triRef.storeTriangle(i2, i4, i1);
                    triRef.storeTriangle(ind2, ind3, ind4);
                    triRef.storeTriangle(ind2, ind4, ind1);
                }
                return true;
            }

            return false;
        }

    }

    class Basic {

        const double D_RND_MAX = 2147483647.0;


        public static double detExp (double u_x, double u_y, double u_z,
                 double v_x, double v_y, double v_z,
                 double w_x, double w_y, double w_z) {

            return ((u_x) * ((v_y) * (w_z) - (v_z) * (w_y)) -
                (u_y) * ((v_x) * (w_z) - (v_z) * (w_x)) +
                (u_z) * ((v_x) * (w_y) - (v_y) * (w_x)));
        }


        public static double det3D (Tuple3F u, Tuple3F v, Tuple3F w) {
            return ((u).X * ((v).Y * (w).Z - (v).Z * (w).Y) -
                (u).Y * ((v).X * (w).Z - (v).Z * (w).X) +
                (u).Z * ((v).X * (w).Y - (v).Y * (w).X));
        }


        public static double det2D (Tuple2F u, Tuple2F v, Tuple2F w) {
            return (((u).X - (v).X) * ((v).Y - (w).Y) + ((v).Y - (u).Y) * ((v).X - (w).X));
        }


        public static double length2 (Tuple3F u) {
            return (((u).X * (u).X) + ((u).Y * (u).Y) + ((u).Z * (u).Z));
        }

        public static double lengthL1 (Tuple3F u) {
            return (Math.Abs((u).X) + Math.Abs((u).Y) + Math.Abs((u).Z));
        }

        public static double lengthL2 (Tuple3F u) {
            return Math.Sqrt(((u).X * (u).X) + ((u).Y * (u).Y) + ((u).Z * (u).Z));
        }


        public static double dotProduct (Tuple3F u, Tuple3F v) {
            return (((u).X * (v).X) + ((u).Y * (v).Y) + ((u).Z * (v).Z));
        }


        public static double dotProduct2D (Tuple2F u, Tuple2F v) {
            return (((u).X * (v).X) + ((u).Y * (v).Y));
        }


        public static void vectorProduct (Tuple3F p, Tuple3F q, Tuple3F r) {
            (r).X = (p).Y * (q).Z - (q).Y * (p).Z;
            (r).Y = (q).X * (p).Z - (p).X * (q).Z;
            (r).Z = (p).X * (q).Y - (q).X * (p).Y;
        }


        public static void vectorAdd (Tuple3F p, Tuple3F q, Tuple3F r) {
            (r).X = (p).X + (q).X;
            (r).Y = (p).Y + (q).Y;
            (r).Z = (p).Z + (q).Z;
        }

        public static void vectorSub (Tuple3F p, Tuple3F q, Tuple3F r) {
            (r).X = (p).X - (q).X;
            (r).Y = (p).Y - (q).Y;
            (r).Z = (p).Z - (q).Z;
        }


        public static void vectorAdd2D (Tuple2F p, Tuple2F q, Tuple2F r) {
            (r).X = (p).X + (q).X;
            (r).Y = (p).Y + (q).Y;
        }


        public static void vectorSub2D (Tuple2F p, Tuple2F q, Tuple2F r) {
            (r).X = (p).X - (q).X;
            (r).Y = (p).Y - (q).Y;
        }

        public static void invertVector (Tuple3F p) {
            (p).X = -(p).X;
            (p).Y = -(p).Y;
            (p).Z = -(p).Z;
        }

        public static void divScalar (double scalar, Tuple3F u) {
            (u).X /= (float)scalar;
            (u).Y /= (float)scalar;
            (u).Z /= (float)scalar;
        }

        public static void multScalar2D (double scalar, Tuple2F u) {
            (u).X *= (float)scalar;
            (u).Y *= (float)scalar;
        }


        public static int signEps (double x, double eps) {
            return ((x <= eps) ? ((x < -eps) ? -1 : 0) : 1);
        }
    }

    class Bridge {

        public static void constructBridges (Triangulator triRef, int loopMin, int loopMax) {
            int i, j, numDist, numLeftMost;

            int[] i0 = new int[1];
            int[] ind0 = new int[1];
            int[] i1 = new int[1];
            int[] ind1 = new int[1];

            int[] iTmp = new int[1];
            int[] indTmp = new int[1];

            if (triRef._noHashingEdges != true)
                Console.WriteLine("Bridge:constructBridges noHashingEdges is false");
            if (loopMax <= loopMin)
                Console.WriteLine("Bridge:constructBridges loopMax<=loopMin");
            if (loopMin < 0)
                Console.WriteLine("Bridge:constructBridges loopMin<0");
            if (loopMax > triRef._numLoops)
                Console.WriteLine("Bridge:constructBridges loopMax>triRef.numLoops");

            numLeftMost = loopMax - loopMin - 1;

            if (numLeftMost > triRef._maxNumLeftMost) {
                triRef._maxNumLeftMost = numLeftMost;
                triRef._leftMost = new Left[numLeftMost];
            }

            // For each contour, find the left-most vertex. (we will use the fact
            // that the vertices appear in sorted order!)
            findLeftMostVertex(triRef, triRef._loops[loopMin], ind0, i0);
            j = 0;
            for (i = loopMin + 1; i < loopMax; ++i) {
                findLeftMostVertex(triRef, triRef._loops[i], indTmp, iTmp);
                triRef._leftMost[j] = new Left();
                triRef._leftMost[j].ind = indTmp[0];
                triRef._leftMost[j].index = iTmp[0];

                ++j;
            }

            // sort the inner contours according to their left-most vertex
            sortLeft(triRef._leftMost, numLeftMost);

            // construct bridges. every bridge will eminate at the left-most point of
            // its corresponding inner loop.
            numDist = triRef._numPoints + 2 * triRef._numLoops;
            triRef._maxNumDist = numDist;
            triRef._distances = new Distance[numDist];
            for (int k = 0; k < triRef._maxNumDist; k++)
                triRef._distances[k] = new Distance();


            for (j = 0; j < numLeftMost; ++j) {
                if (!findBridge(triRef, ind0[0], i0[0], triRef._leftMost[j].index, ind1, i1)) {
                    //  if (verbose)
                    // fprintf(stderr, "\n\n***** yikes! the loops intersect! *****\n");
                }
                if (i1[0] == triRef._leftMost[j].index)
                    // the left-most node of the hole coincides with a node of the
                    // boundary
                    simpleBridge(triRef, ind1[0], triRef._leftMost[j].ind);
                else
                    // two bridge edges need to be inserted
                    insertBridge(triRef, ind1[0], i1[0], triRef._leftMost[j].ind,
                             triRef._leftMost[j].index);
            }

        }


        /**
         * We try to find a vertex  i1  on the loop which contains  i  such that  i1
         * is close to  start,  and such that  i1, start  is a valid diagonal.
         */
        public static bool findBridge (Triangulator triRef, int ind, int i, int start,
                      int[] ind1, int[] i1) {
            int i0, i2, j, numDist = 0;
            int ind0, ind2;
            BBox bb;
            Distance[] old = null;
            bool convex, coneOk;

            // sort the points according to their distance from  start.
            ind1[0] = ind;
            i1[0] = i;
            if (i1[0] == start) return true;
            if (numDist >= triRef._maxNumDist) {
                // System.out.println("(1) Expanding distances array ...");
                triRef._maxNumDist += Triangulator.INC_DIST_BK;
                old = triRef._distances;
                triRef._distances = new Distance[triRef._maxNumDist];
                Array.Copy(old, 0, triRef._distances, 0, old.Length);
                for (int k = old.Length; k < triRef._maxNumDist; k++)
                    triRef._distances[k] = new Distance();
            }

            triRef._distances[numDist].dist = Numerics.baseLength(triRef._points[start],
                                         triRef._points[i1[0]]);
            triRef._distances[numDist].ind = ind1[0];
            ++numDist;


            ind1[0] = triRef.fetchNextData(ind1[0]);
            i1[0] = triRef.fetchData(ind1[0]);
            while (ind1[0] != ind) {
                if (i1[0] == start) return true;
                if (numDist >= triRef._maxNumDist) {
                    // System.out.println("(2) Expanding distances array ...");
                    triRef._maxNumDist += Triangulator.INC_DIST_BK;
                    old = triRef._distances;
                    triRef._distances = new Distance[triRef._maxNumDist];
                    Array.Copy(old, 0, triRef._distances, 0, old.Length);
                    for (int k = old.Length; k < triRef._maxNumDist; k++)
                        triRef._distances[k] = new Distance();
                }

                triRef._distances[numDist].dist = Numerics.baseLength(triRef._points[start],
                                         triRef._points[i1[0]]);
                triRef._distances[numDist].ind = ind1[0];
                ++numDist;
                ind1[0] = triRef.fetchNextData(ind1[0]);
                i1[0] = triRef.fetchData(ind1[0]);
            }

            sortDistance(triRef._distances, numDist);

            // find a valid diagonal. note that no node with index  i1 > start  can
            // be feasible!
            for (j = 0; j < numDist; ++j) {
                ind1[0] = triRef._distances[j].ind;
                i1[0] = triRef.fetchData(ind1[0]);
                if (i1[0] <= start) {
                    ind0 = triRef.fetchPrevData(ind1[0]);
                    i0 = triRef.fetchData(ind0);
                    ind2 = triRef.fetchNextData(ind1[0]);
                    i2 = triRef.fetchData(ind2);
                    convex = triRef.getAngle(ind1[0]) > 0;

                    coneOk = Numerics.isInCone(triRef, i0, i1[0], i2, start, convex);
                    if (coneOk) {
                        bb = new BBox(triRef, i1[0], start);
                        if (!NoHash.noHashEdgeIntersectionExists(triRef, bb, -1, -1, ind1[0], -1))
                            return true;
                    }
                }
            }

            // the left-most point of the hole does not lie within the outer
            // boundary!  what is the best bridge in this case??? I make a
            // brute-force decision...  perhaps this should be refined during a
            // revision of the code...
            for (j = 0; j < numDist; ++j) {
                ind1[0] = triRef._distances[j].ind;
                i1[0] = triRef.fetchData(ind1[0]);
                ind0 = triRef.fetchPrevData(ind1[0]);
                i0 = triRef.fetchData(ind0);
                ind2 = triRef.fetchNextData(ind1[0]);
                i2 = triRef.fetchData(ind2);
                bb = new BBox(triRef, i1[0], start);
                if (!NoHash.noHashEdgeIntersectionExists(triRef, bb, -1, -1, ind1[0], -1))
                    return true;
            }

            // still no diagonal??? yikes! oh well, this polygon is messed up badly!
            ind1[0] = ind;
            i1[0] = i;

            return false;
        }


        public static void findLeftMostVertex (Triangulator triRef, int ind, int[] leftInd,
                       int[] leftI) {
            int ind1, i1;

            ind1 = ind;
            i1 = triRef.fetchData(ind1);
            leftInd[0] = ind1;
            leftI[0] = i1;
            ind1 = triRef.fetchNextData(ind1);
            i1 = triRef.fetchData(ind1);
            while (ind1 != ind) {
                if (i1 < leftI[0]) {
                    leftInd[0] = ind1;
                    leftI[0] = i1;
                } else if (i1 == leftI[0]) {
                    if (triRef.getAngle(ind1) < 0) {
                        leftInd[0] = ind1;
                        leftI[0] = i1;
                    }
                }
                ind1 = triRef.fetchNextData(ind1);
                i1 = triRef.fetchData(ind1);
            }

        }

        public static void simpleBridge (Triangulator triRef, int ind1, int ind2) {
            int prev, next;
            int i1, i2, prv, nxt;
            int angle;


            // change the links
            triRef.rotateLinks(ind1, ind2);

            // reset the angles
            i1 = triRef.fetchData(ind1);
            next = triRef.fetchNextData(ind1);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind1);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i1, nxt, ind1);
            triRef.setAngle(ind1, angle);

            i2 = triRef.fetchData(ind2);
            next = triRef.fetchNextData(ind2);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind2);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i2, nxt, ind2);
            triRef.setAngle(ind2, angle);

        }


        public static void insertBridge (Triangulator triRef, int ind1, int i1,
                     int ind3, int i3) {
            int ind2, ind4, prev, next;
            int prv, nxt, angle;
            int vcntIndex;

            // duplicate nodes in order to form end points of the bridge edges
            ind2 = triRef.makeNode(i1);
            triRef.insertAfter(ind1, ind2);

            // Need to get the original data, before setting it.

            vcntIndex = triRef._list[ind1].GetCommonIndex();

            triRef._list[ind2].SetCommonIndex(vcntIndex);


            ind4 = triRef.makeNode(i3);
            triRef.insertAfter(ind3, ind4);

            vcntIndex = triRef._list[ind3].GetCommonIndex();
            triRef._list[ind4].SetCommonIndex(vcntIndex);

            // insert the bridge edges into the boundary loops
            triRef.splitSplice(ind1, ind2, ind3, ind4);

            // reset the angles
            next = triRef.fetchNextData(ind1);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind1);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i1, nxt, ind1);
            triRef.setAngle(ind1, angle);

            next = triRef.fetchNextData(ind2);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind2);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i1, nxt, ind2);
            triRef.setAngle(ind2, angle);

            next = triRef.fetchNextData(ind3);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind3);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i3, nxt, ind3);
            triRef.setAngle(ind3, angle);

            next = triRef.fetchNextData(ind4);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind4);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i3, nxt, ind4);
            triRef.setAngle(ind4, angle);

        }


        public static int l_comp (Left a, Left b) {
            if (a.index < b.index) return -1;
            else if (a.index > b.index) return 1;
            else return 0;
        }

        public static int d_comp (Distance a, Distance b) {
            if (a.dist < b.dist) return -1;
            else if (a.dist > b.dist) return 1;
            else return 0;
        }


        public static void sortLeft (Left[] lefts, int numPts) {
            int i, j;
            Left swap = new Left();

            for (i = 0; i < numPts; i++) {
                for (j = i + 1; j < numPts; j++) {
                    if (l_comp(lefts[i], lefts[j]) > 0) {
                        swap.copy(lefts[i]);
                        lefts[i].copy(lefts[j]);
                        lefts[j].copy(swap);
                    }
                }
            }
        }


        public static void sortDistance (Distance[] distances, int numPts) {
            int i, j;
            Distance swap = new Distance();

            for (i = 0; i < numPts; i++) {
                for (j = i + 1; j < numPts; j++) {
                    if (d_comp(distances[i], distances[j]) > 0) {
                        swap.copy(distances[i]);
                        distances[i].copy(distances[j]);
                        distances[j].copy(swap);
                    }
                }
            }
        }

    }

    class Clean {

        public static void initPUnsorted (Triangulator triRef, int number) {
            if (number > triRef._maxNumPUnsorted) {
                triRef._maxNumPUnsorted = number;
                triRef._pUnsorted = new Point2F[triRef._maxNumPUnsorted];
                for (int i = 0; i < triRef._maxNumPUnsorted; i++)
                    triRef._pUnsorted[i] = new Point2F();
            }
        }


        public static int cleanPolyhedralFace (Triangulator triRef, int i1, int i2) {
            int removed;
            int i, j, numSorted, index;
            int ind1, ind2;

            initPUnsorted(triRef, triRef._numPoints);

            for (i = 0; i < triRef._numPoints; ++i)
                triRef._pUnsorted[i].set(triRef._points[i]);

            // sort points according to lexicographical order
            /*
               System.out.println("Points : (Unsorted)");
               for(i=0; i<triRef._numPoints; i++)
               System.out.println( i + "pt ( " + triRef.points[i].x + ", " +
               triRef.points[i].y + ")");
            */

            //    qsort(points, num_pnts, sizeof(point), &p_comp);

            sort(triRef._points, triRef._numPoints);

            /*
               System.out.println("Points : (Sorted)");
               for(i=0; i<triRef._numPoints; i++)
               System.out.println( i +"pt ( " + triRef.points[i].x + ", " +
               triRef.points[i].y + ")");
            */

            // eliminate duplicate vertices
            i = 0;
            for (j = 1; j < triRef._numPoints; ++j) {
                if (pComp(triRef._points[i], triRef._points[j]) != 0) {
                    ++i;
                    triRef._points[i] = triRef._points[j];
                }
            }
            numSorted = i + 1;
            removed = triRef._numPoints - numSorted;

            /*
              System.out.println("Points : (Sorted and eliminated)");
              for(i=0; i<triRef._numPoints; i++)
              System.out.println( i + "pt ( " + triRef.points[i].x + ", " +
              triRef.points[i].y + ")");
            */

            // renumber the vertices of the polygonal face
            for (i = i1; i < i2; ++i) {
                ind1 = triRef._loops[i];
                ind2 = triRef.fetchNextData(ind1);
                index = triRef.fetchData(ind2);
                while (ind2 != ind1) {
                    j = findPInd(triRef._points, numSorted, triRef._pUnsorted[index]);
                    triRef.updateIndex(ind2, j);
                    ind2 = triRef.fetchNextData(ind2);
                    index = triRef.fetchData(ind2);
                }
                j = findPInd(triRef._points, numSorted, triRef._pUnsorted[index]);
                triRef.updateIndex(ind2, j);
            }

            triRef._numPoints = numSorted;

            return removed;
        }


        public static void sort (Point2F[] points, int numPts) {
            int i, j;
            Point2F swap = new Point2F();

            for (i = 0; i < numPts; i++) {
                for (j = i + 1; j < numPts; j++) {
                    if (pComp(points[i], points[j]) > 0) {
                        swap.set(points[i]);
                        points[i].set(points[j]);
                        points[j].set(swap);
                    }
                }
            }
            /*
               for (i = 0; i < numPts; i++) {
                System.out.println("pt " + points[i]);
               }
            */
        }

        public static int findPInd (Point2F[] sorted, int numPts, Point2F pnt) {
            int i;

            for (i = 0; i < numPts; i++) {
                if ((pnt.X == sorted[i].X) &&
                (pnt.Y == sorted[i].Y)) {
                    return i;
                }
            }
            return -1;
        }

        public static int pComp (Point2F a, Point2F b) {
            if (a.X < b.X)
                return -1;
            else if (a.X > b.X)
                return 1;
            else {
                if (a.Y < b.Y)
                    return -1;
                else if (a.Y > b.Y)
                    return 1;
                else
                    return 0;
            }
        }

    }

    class Degenerate {
        /**
         * This function checks whether the triangle  i1, i2, i3  is an ear, where
         * the vertex  i4  lies on at least one of the two edges  i1, i2  or  i3, i1.
         * basically, we can cut the polygon at  i4  into two pieces. the polygon
         * touches at  i4  back to back if following the next-pointers in one
         * subpolygon and following the prev-pointers in the other subpolygon yields
         * the same orientation for both subpolygons. otherwise,  i4  forms a
         * bottle neck of the polygon, and  i1, i2, i3  is no valid ear.
         *
         * Note that this function may come up with the incorrect answer if the
         * polygon has self-intersections.
         */
        public static bool handleDegeneracies (Triangulator triRef, int i1, int ind1, int i2,
                          int i3, int i4, int ind4) {
            int i0, i5;
            int[] type = new int[1];
            int ind0, ind2, ind5;
            bool flag;
            double area = 0.0, area1 = 0, area2 = 0.0;

            /* assert(InPointsList(i1));
               assert(InPointsList(i2));
               assert(InPointsList(i3));
               assert(InPointsList(i4));
            */

            // first check whether the successor or predecessor of  i4  is inside the
            // triangle, or whether any of the two edges incident at  i4  intersects
            // i2, i3.
            ind5 = triRef.fetchPrevData(ind4);
            i5 = triRef.fetchData(ind5);

            // assert(ind4 != ind5);
            //assert(InPointsList(i5));
            if ((i5 != i2) && (i5 != i3)) {
                flag = Numerics.vtxInTriangle(triRef, i1, i2, i3, i5, type);
                if (flag && (type[0] == 0)) return true;
                if (i2 <= i3) {
                    if (i4 <= i5)
                        flag = Numerics.segIntersect(triRef, i2, i3, i4, i5, -1);
                    else
                        flag = Numerics.segIntersect(triRef, i2, i3, i5, i4, -1);
                } else {
                    if (i4 <= i5)
                        flag = Numerics.segIntersect(triRef, i3, i2, i4, i5, -1);
                    else
                        flag = Numerics.segIntersect(triRef, i3, i2, i5, i4, -1);
                }
                if (flag)
                    return true;
            }

            ind5 = triRef.fetchNextData(ind4);
            i5 = triRef.fetchData(ind5);
            // assert(ind4 != ind5);
            // assert(InPointsList(i5));
            if ((i5 != i2) && (i5 != i3)) {
                flag = Numerics.vtxInTriangle(triRef, i1, i2, i3, i5, type);
                if (flag && (type[0] == 0)) return true;
                if (i2 <= i3) {
                    if (i4 <= i5) flag = Numerics.segIntersect(triRef, i2, i3, i4, i5, -1);
                    else flag = Numerics.segIntersect(triRef, i2, i3, i5, i4, -1);
                } else {
                    if (i4 <= i5) flag = Numerics.segIntersect(triRef, i3, i2, i4, i5, -1);
                    else flag = Numerics.segIntersect(triRef, i3, i2, i5, i4, -1);
                }
                if (flag) return true;
            }

            i0 = i1;
            ind0 = ind1;
            ind1 = triRef.fetchNextData(ind1);
            i1 = triRef.fetchData(ind1);
            while (ind1 != ind4) {
                ind2 = triRef.fetchNextData(ind1);
                i2 = triRef.fetchData(ind2);
                area = Numerics.stableDet2D(triRef, i0, i1, i2);
                area1 += area;
                ind1 = ind2;
                i1 = i2;
            }

            ind1 = triRef.fetchPrevData(ind0);
            i1 = triRef.fetchData(ind1);
            while (ind1 != ind4) {
                ind2 = triRef.fetchPrevData(ind1);
                i2 = triRef.fetchData(ind2);
                area = Numerics.stableDet2D(triRef, i0, i1, i2);
                area2 += area;
                ind1 = ind2;
                i1 = i2;
            }

            if (Numerics.le(area1, Triangulator.ZERO) && Numerics.le(area2, Triangulator.ZERO))
                return false;
            else if (Numerics.ge(area1, Triangulator.ZERO) && Numerics.ge(area2, Triangulator.ZERO))
                return false;
            else
                return true;
        }
    }


    class Desperate {

        /**
         * the functions in this file try to ensure that we always end up with
         * something that (topologically) is a triangulation.
         *
         * the more desperate we get, the more aggressive means we choose for making
         * diagonals "valid".
         */
        public static bool desperate (Triangulator triRef, int ind, int i, bool[] splitted) {
            int[] i1 = new int[1];
            int[] i2 = new int[1];
            int[] i3 = new int[1];
            int[] i4 = new int[1];
            int[] ind1 = new int[1];
            int[] ind2 = new int[1];
            int[] ind3 = new int[1];
            int[] ind4 = new int[1];

            splitted[0] = false;

            // check whether there exist consecutive vertices  i1, i2, i3, i4   such
            // that  i1, i2  and  i3, i4  intersect
            if (existsCrossOver(triRef, ind, ind1, i1, ind2, i2, ind3, i3, ind4, i4)) {
                // insert two new diagonals around the cross-over without checking
                // whether they are intersection-free
                handleCrossOver(triRef, ind1[0], i1[0], ind2[0], i2[0], ind3[0], i3[0],
                        ind4[0], i4[0]);
                return false;
            }

            NoHash.prepareNoHashEdges(triRef, i, i + 1);

            // check whether there exists a valid diagonal that splits the polygon
            // into two parts
            if (existsSplit(triRef, ind, ind1, i1, ind2, i2)) {
                // break up the polygon by inserting this diagonal (which can't be an
                // ear -- otherwise, we would not have ended up in this part of the
                // code). then, let's treat the two polygons separately. hopefully,
                // this will help to handle self-overlapping polygons in the "correct"
                // way.
                handleSplit(triRef, ind1[0], i1[0], ind2[0], i2[0]);
                splitted[0] = true;
                return false;
            }

            return true;
        }


        public static bool existsCrossOver (Triangulator triRef, int ind, int[] ind1, int[] i1,
                       int[] ind2, int[] i2, int[] ind3, int[] i3,
                       int[] ind4, int[] i4) {
            BBox bb1, bb2;

            ind1[0] = ind;
            i1[0] = triRef.fetchData(ind1[0]);
            ind2[0] = triRef.fetchNextData(ind1[0]);
            i2[0] = triRef.fetchData(ind2[0]);
            ind3[0] = triRef.fetchNextData(ind2[0]);
            i3[0] = triRef.fetchData(ind3[0]);
            ind4[0] = triRef.fetchNextData(ind3[0]);
            i4[0] = triRef.fetchData(ind4[0]);

            do {
                bb1 = new BBox(triRef, i1[0], i2[0]);
                bb2 = new BBox(triRef, i3[0], i4[0]);
                if (bb1.BBoxOverlap(bb2)) {
                    if (Numerics.segIntersect(triRef, bb1.imin, bb1.imax, bb2.imin, bb2.imax, -1))
                        return true;
                }
                ind1[0] = ind2[0];
                i1[0] = i2[0];
                ind2[0] = ind3[0];
                i2[0] = i3[0];
                ind3[0] = ind4[0];
                i3[0] = i4[0];
                ind4[0] = triRef.fetchNextData(ind3[0]);
                i4[0] = triRef.fetchData(ind4[0]);

            } while (ind1[0] != ind);

            return false;
        }


        public static void handleCrossOver (Triangulator triRef, int ind1, int i1, int ind2,
                    int i2, int ind3, int i3, int ind4, int i4) {
            double ratio1, ratio4;
            bool first;
            int angle1, angle4;

            // which pair of triangles shall I insert?? we can use either  i1, i2, i3
            // and  i1, i3, i4,  or we can use  i2, i3, i4  and  i1, i2, i4...
            angle1 = triRef.getAngle(ind1);
            angle4 = triRef.getAngle(ind4);
            if (angle1 < angle4) first = true;
            else if (angle1 > angle4) first = false;
            else if (triRef._earsSorted) {
                ratio1 = Numerics.getRatio(triRef, i3, i4, i1);
                ratio4 = Numerics.getRatio(triRef, i1, i2, i4);
                if (ratio4 < ratio1) first = false;
                else first = true;
            } else {
                first = true;
            }

            if (first) {
                // first clip  i1, i2, i3,  then clip  i1, i3, i4
                triRef.deleteLinks(ind2);
                // StoreTriangle(GetOriginal(ind1), GetOriginal(ind2), GetOriginal(ind3));
                triRef.storeTriangle(ind1, ind2, ind3);
                triRef.setAngle(ind3, 1);
                Heap.insertIntoHeap(triRef, 0.0, ind3, ind1, ind4);
            } else {
                // first clip  i2, i3, i4,  then clip  i1, i2, i4
                triRef.deleteLinks(ind3);
                //StoreTriangle(GetOriginal(ind2), GetOriginal(ind3), GetOriginal(ind4));
                triRef.storeTriangle(ind2, ind3, ind4);
                triRef.setAngle(ind2, 1);
                Heap.insertIntoHeap(triRef, 0.0, ind2, ind1, ind4);
            }
        }


        public static bool letsHope (Triangulator triRef, int ind) {
            int ind0, ind1, ind2;
            int i0, i1, i2;

            // let's clip the first convex corner. of course, we know that this is no
            // ear in an ideal world. but this polygon isn't ideal, either!
            ind1 = ind;
            i1 = triRef.fetchData(ind1);

            do {
                if (triRef.getAngle(ind1) > 0) {
                    ind0 = triRef.fetchPrevData(ind1);
                    i0 = triRef.fetchData(ind0);
                    ind2 = triRef.fetchNextData(ind1);
                    i2 = triRef.fetchData(ind2);
                    Heap.insertIntoHeap(triRef, 0.0, ind1, ind0, ind2);
                    return true;
                }
                ind1 = triRef.fetchNextData(ind1);
                i1 = triRef.fetchData(ind1);
            } while (ind1 != ind);

            // no convex corners? so, let's cheat! this code won't stop without some
            // triangulation...  ;-)    g-i-g-o? right! perhaps, this is what you
            // call a robust code?!
            triRef.setAngle(ind, 1);
            ind0 = triRef.fetchPrevData(ind);
            i0 = triRef.fetchData(ind0);
            ind2 = triRef.fetchNextData(ind);
            i2 = triRef.fetchData(ind2);
            Heap.insertIntoHeap(triRef, 0.0, ind, ind0, ind2);
            i1 = triRef.fetchData(ind);

            return true;

            // see, we never have to return "false"...
            /*
              return false;
            */
        }


        static bool existsSplit (Triangulator triRef, int ind, int[] ind1, int[] i1,
                       int[] ind2, int[] i2) {
            int ind3, ind4, ind5;
            int i3, i4, i5;

            if (triRef._numPoints > triRef._maxNumDist) {
                // System.out.println("Desperate: Expanding distances array ...");
                triRef._maxNumDist = triRef._numPoints;
                triRef._distances = new Distance[triRef._maxNumDist];
                for (int k = 0; k < triRef._maxNumDist; k++)
                    triRef._distances[k] = new Distance();
            }
            ind1[0] = ind;
            i1[0] = triRef.fetchData(ind1[0]);
            ind4 = triRef.fetchNextData(ind1[0]);
            i4 = triRef.fetchData(ind4);
            // assert(*ind1 != ind4);
            ind5 = triRef.fetchNextData(ind4);
            i5 = triRef.fetchData(ind5);
            // assert(*ind1 != *ind2);
            ind3 = triRef.fetchPrevData(ind1[0]);
            i3 = triRef.fetchData(ind3);
            // assert(*ind2 != ind3);
            if (foundSplit(triRef, ind5, i5, ind3, ind1[0], i1[0], i3, i4, ind2, i2))
                return true;
            i3 = i1[0];
            ind1[0] = ind4;
            i1[0] = i4;
            ind4 = ind5;
            i4 = i5;
            ind5 = triRef.fetchNextData(ind4);
            i5 = triRef.fetchData(ind5);

            while (ind5 != ind) {
                if (foundSplit(triRef, ind5, i5, ind, ind1[0], i1[0], i3, i4, ind2, i2))
                    return true;
                i3 = i1[0];
                ind1[0] = ind4;
                i1[0] = i4;
                ind4 = ind5;
                i4 = i5;
                ind5 = triRef.fetchNextData(ind4);
                i5 = triRef.fetchData(ind5);
            }

            return false;
        }


        /**
         * This function computes the winding number of a polygon with respect to a
         * point  p.  no care is taken to handle cases where  p  lies on the
         * boundary of the polygon. (this is no issue in our application, as we will
         * always compute the winding number with respect to the mid-point of a
         * valid diagonal.)
         */
        static int windingNumber (Triangulator triRef, int ind, Point2F p) {
            double angle;
            int ind2;
            int i1, i2, number;

            i1 = triRef.fetchData(ind);
            ind2 = triRef.fetchNextData(ind);
            i2 = triRef.fetchData(ind2);
            angle = Numerics.angle(triRef, p, triRef._points[i1], triRef._points[i2]);
            while (ind2 != ind) {
                i1 = i2;
                ind2 = triRef.fetchNextData(ind2);
                i2 = triRef.fetchData(ind2);
                angle += Numerics.angle(triRef, p, triRef._points[i1], triRef._points[i2]);
            }

            angle += Math.PI;
            number = (int)(angle / (Math.PI * 2.0));

            return number;
        }




        static bool foundSplit (Triangulator triRef, int ind5, int i5, int ind, int ind1,
                      int i1, int i3, int i4, int[] ind2, int[] i2) {
            Point2F center;
            int numDist = 0;
            int j, i6, i7;
            int ind6, ind7;
            BBox bb;
            bool convex, coneOk;

            // Sort the points according to their distance from  i1
            do {
                // assert(numDist < triRef.maxNumDist);
                triRef._distances[numDist].dist = Numerics.baseLength(triRef._points[i1],
                                         triRef._points[i5]);
                triRef._distances[numDist].ind = ind5;
                ++numDist;
                ind5 = triRef.fetchNextData(ind5);
                i5 = triRef.fetchData(ind5);
            } while (ind5 != ind);

            Bridge.sortDistance(triRef._distances, numDist);

            // find a valid diagonal.
            for (j = 0; j < numDist; ++j) {
                ind2[0] = triRef._distances[j].ind;
                i2[0] = triRef.fetchData(ind2[0]);
                if (i1 != i2[0]) {
                    ind6 = triRef.fetchPrevData(ind2[0]);
                    i6 = triRef.fetchData(ind6);
                    ind7 = triRef.fetchNextData(ind2[0]);
                    i7 = triRef.fetchData(ind7);

                    convex = triRef.getAngle(ind2[0]) > 0;
                    coneOk = Numerics.isInCone(triRef, i6, i2[0], i7, i1, convex);
                    if (coneOk) {
                        convex = triRef.getAngle(ind1) > 0;
                        coneOk = Numerics.isInCone(triRef, i3, i1, i4, i2[0], convex);
                        if (coneOk) {
                            bb = new BBox(triRef, i1, i2[0]);
                            if (!NoHash.noHashEdgeIntersectionExists(triRef, bb, -1, -1, ind1, -1)) {
                                // check whether this is a good diagonal; we do not want a
                                // diagonal that may create figure-8's!
                                center = new Point2F();
                                Basic.vectorAdd2D(triRef._points[i1], triRef._points[i2[0]], center);
                                Basic.multScalar2D(0.5, center);
                                if (windingNumber(triRef, ind, center) == 1) return true;
                            }
                        }
                    }
                }
            }

            return false;
        }


        static void handleSplit (Triangulator triRef, int ind1, int i1, int ind3, int i3) {
            int ind2, ind4, prev, next;
            int prv, nxt, angle;
            int comIndex = -1;

            // duplicate nodes in order to form end points of the new diagonal
            ind2 = triRef.makeNode(i1);
            triRef.insertAfter(ind1, ind2);

            // Need to get the original data, before setting it.

            comIndex = triRef._list[ind1].GetCommonIndex();

            triRef._list[ind2].SetCommonIndex(comIndex);

            ind4 = triRef.makeNode(i3);
            triRef.insertAfter(ind3, ind4);

            comIndex = triRef._list[ind3].GetCommonIndex();
            triRef._list[ind4].SetCommonIndex(comIndex);

            // insert the diagonal into the boundary loop, thus splitting the loop
            // into two loops
            triRef.splitSplice(ind1, ind2, ind3, ind4);

            // store pointers to the two new loops
            triRef.storeChain(ind1);
            triRef.storeChain(ind3);

            // reset the angles
            next = triRef.fetchNextData(ind1);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind1);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i1, nxt, ind1);
            triRef.setAngle(ind1, angle);

            next = triRef.fetchNextData(ind2);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind2);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i1, nxt, ind2);
            triRef.setAngle(ind2, angle);

            next = triRef.fetchNextData(ind3);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind3);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i3, nxt, ind3);
            triRef.setAngle(ind3, angle);

            next = triRef.fetchNextData(ind4);
            nxt = triRef.fetchData(next);
            prev = triRef.fetchPrevData(ind4);
            prv = triRef.fetchData(prev);
            angle = Numerics.isConvexAngle(triRef, prv, i3, nxt, ind4);
            triRef.setAngle(ind4, angle);
        }
    }

    class EarClip {

        /**
         * Classifies all the internal angles of the loop referenced by  ind.
         * the following classification is used:
         *            0 ... if angle is 180 degrees
         *            1 ... if angle between 0 and 180 degrees
         *            2 ... if angle is 0 degrees
         *           -1 ... if angle between 180 and 360 degrees
         *           -2 ... if angle is 360 degrees
         */
        public static void classifyAngles (Triangulator triRef, int ind) {
            int ind0, ind1, ind2;
            int i0, i1, i2;
            int angle;

            ind1 = ind;
            i1 = triRef.fetchData(ind1);
            ind0 = triRef.fetchPrevData(ind1);
            i0 = triRef.fetchData(ind0);

            do {
                ind2 = triRef.fetchNextData(ind1);
                i2 = triRef.fetchData(ind2);
                angle = Numerics.isConvexAngle(triRef, i0, i1, i2, ind1);
                triRef.setAngle(ind1, angle);
                i0 = i1;
                i1 = i2;
                ind1 = ind2;
            } while (ind1 != ind);

        }


        public static void classifyEars (Triangulator triRef, int ind) {
            int ind1;
            int i1;
            int[] ind0, ind2;
            double[] ratio;

            ind0 = new int[1];
            ind2 = new int[1];
            ratio = new double[1];

            Heap.initHeap(triRef);

            ind1 = ind;
            i1 = triRef.fetchData(ind1);

            do {
                if ((triRef.getAngle(ind1) > 0) &&
                isEar(triRef, ind1, ind0, ind2, ratio)) {

                    Heap.dumpOnHeap(triRef, ratio[0], ind1, ind0[0], ind2[0]);
                }
                ind1 = triRef.fetchNextData(ind1);
                i1 = triRef.fetchData(ind1);
            } while (ind1 != ind);

            // Not using sorted_ear so don't have to do MakeHeap();
            // MakeHeap();

            // Heap.printHeapData(triRef);

        }


        /**                                                                         
         * This function checks whether a diagonal is valid, that is, whether it is
         * locally within the polygon, and whether it does not intersect any other
         * segment of the polygon. also, some degenerate cases get a special
         * handling.
         */
        public static bool isEar (Triangulator triRef, int ind2, int[] ind1, int[] ind3,
                 double[] ratio) {
            int i0, i1, i2, i3, i4;
            int ind0, ind4;
            BBox bb;
            bool convex, coneOk;

            i2 = triRef.fetchData(ind2);
            ind3[0] = triRef.fetchNextData(ind2);
            i3 = triRef.fetchData(ind3[0]);
            ind4 = triRef.fetchNextData(ind3[0]);
            i4 = triRef.fetchData(ind4);
            ind1[0] = triRef.fetchPrevData(ind2);
            i1 = triRef.fetchData(ind1[0]);
            ind0 = triRef.fetchPrevData(ind1[0]);
            i0 = triRef.fetchData(ind0);

            /*
              System.out.println("isEar : i0 " + i0 + " i1 " + i1 + " i2 " + i2 +
              " i3 " + i3 + " i4 " + i4);
            */

            if ((i1 == i3) || (i1 == i2) || (i2 == i3) || (triRef.getAngle(ind2) == 2)) {
                // oops, this is not a simple polygon!
                ratio[0] = 0.0;
                return true;
            }

            if (i0 == i3) {
                // again, this is not a simple polygon!
                if ((triRef.getAngle(ind0) < 0) || (triRef.getAngle(ind3[0]) < 0)) {
                    ratio[0] = 0.0;
                    return true;
                } else
                    return false;
            }

            if (i1 == i4) {
                // again, this is not a simple polygon!
                if ((triRef.getAngle(ind1[0]) < 0) || (triRef.getAngle(ind4) < 0)) {
                    ratio[0] = 0.0;
                    return true;
                } else
                    return false;
            }

            // check whether the new diagonal  i1, i3  locally is within the polygon
            convex = triRef.getAngle(ind1[0]) > 0;
            coneOk = Numerics.isInCone(triRef, i0, i1, i2, i3, convex);
            // System.out.println("isEar :(1) convex " + convex + " coneOk " + coneOk );

            if (!coneOk) return false;
            convex = triRef.getAngle(ind3[0]) > 0;
            coneOk = Numerics.isInCone(triRef, i2, i3, i4, i1, convex);
            // System.out.println("isEar :(2) convex " + convex + " coneOk " + coneOk );

            if (coneOk) {
                // check whether this diagonal is a valid diagonal. this translates to
                // checking either condition CE1 or CE2 (see my paper). If CE1 is to
                // to be checked, then we use a BV-tree or a grid. Otherwise, we use
                // "buckets" (i.e., a grid) or no hashing at all.
                bb = new BBox(triRef, i1, i3);
                // use CE2 + no_hashing
                if (!NoHash.noHashIntersectionExists(triRef, i2, ind2, i3, i1, bb)) {
                    if (triRef._earsSorted) {
                        // determine the quality of the triangle
                        ratio[0] = Numerics.getRatio(triRef, i1, i3, i2);
                    } else {
                        ratio[0] = 1.0;
                    }
                    return true;
                }
            }

            // System.out.println("isEar : false");
            return false;
        }



        /**
         * This is the main function that drives the ear-clipping. it obtains an ear
         * from set of ears maintained in a priority queue, clips this ear, and
         * updates all data structures appropriately. (ears are arranged in the
         * priority queue (i.e., heap) according to a quality criterion that tries
         * to avoid skinny triangles.)
         */
        public static bool clipEar (Triangulator triRef, bool[] done) {

            int ind0, ind1, ind3, ind4;

            int i0, i1, i2, i3, i4;
            int angle1, angle3;

            double[] ratio = new double[1];
            int[] index0 = new int[1];
            int[] index1 = new int[1];
            int[] index2 = new int[1];
            int[] index3 = new int[1];
            int[] index4 = new int[1];
            int[] ind2 = new int[1];

            // Heap.printHeapData(triRef);

            do {

                //	System.out.println("In clipEarloop " + testCnt++);

                if (!Heap.deleteFromHeap(triRef, ind2, index1, index3))
                    // no ear exists?!
                    return false;

                // get the successors and predecessors in the list of nodes and check
                // whether the ear still is part of the boundary
                ind1 = triRef.fetchPrevData(ind2[0]);
                i1 = triRef.fetchData(ind1);
                ind3 = triRef.fetchNextData(ind2[0]);
                i3 = triRef.fetchData(ind3);

            } while ((index1[0] != ind1) || (index3[0] != ind3));

            //System.out.println("Out of clipEarloop ");

            i2 = triRef.fetchData(ind2[0]);

            // delete the clipped ear from the list of nodes, and update the bv-tree
            triRef.deleteLinks(ind2[0]);

            // store the ear in a list of ears which have already been clipped
            // StoreTriangle(GetOriginal(ind1), GetOriginal(ind2), GetOriginal(ind3));
            triRef.storeTriangle(ind1, ind2[0], ind3);

            /*                                                                        */
            /* update the angle classification at  ind1  and  ind3                    */
            /*                                                                        */
            ind0 = triRef.fetchPrevData(ind1);
            i0 = triRef.fetchData(ind0);
            if (ind0 == ind3) {
                // nothing left
                done[0] = true;
                return true;
            }
            angle1 = Numerics.isConvexAngle(triRef, i0, i1, i3, ind1);

            ind4 = triRef.fetchNextData(ind3);
            i4 = triRef.fetchData(ind4);

            angle3 = Numerics.isConvexAngle(triRef, i1, i3, i4, ind3);

            if (i1 != i3) {
                if ((angle1 >= 0) && (triRef.getAngle(ind1) < 0))
                    NoHash.deleteReflexVertex(triRef, ind1);
                if ((angle3 >= 0) && (triRef.getAngle(ind3) < 0))
                    NoHash.deleteReflexVertex(triRef, ind3);
            } else {
                if ((angle1 >= 0) && (triRef.getAngle(ind1) < 0))
                    NoHash.deleteReflexVertex(triRef, ind1);
                else if ((angle3 >= 0) && (triRef.getAngle(ind3) < 0))
                    NoHash.deleteReflexVertex(triRef, ind3);

            }

            triRef.setAngle(ind1, angle1);
            triRef.setAngle(ind3, angle3);

            // check whether either of  ind1  and  ind3  is an ear. (the "ratio" is
            // the length of the triangle's longest side divided by the length of the
            // height normal onto this side; it is used as a quality criterion.)
            if (angle1 > 0) {
                if (isEar(triRef, ind1, index0, index2, ratio)) {
                    // insert the new ear into the priority queue of ears
                    Heap.insertIntoHeap(triRef, ratio[0], ind1, index0[0], index2[0]);
                }
            }

            if (angle3 > 0) {
                if (isEar(triRef, ind3, index2, index4, ratio)) {
                    Heap.insertIntoHeap(triRef, ratio[0], ind3, index2[0], index4[0]);
                }
            }

            // check whether the triangulation is finished.
            ind0 = triRef.fetchPrevData(ind1);
            i0 = triRef.fetchData(ind0);
            ind4 = triRef.fetchNextData(ind3);
            i4 = triRef.fetchData(ind4);
            if (ind0 == ind4) {
                // only one triangle left -- clip it!
                triRef.storeTriangle(ind1, ind3, ind4);
                done[0] = true;
            } else {
                done[0] = false;
            }

            return true;
        }

    }

    public class Matrix4F {
        public float m00, m01, m02, m03;
        public float m10, m11, m12, m13;
        public float m20, m21, m22, m23;
        public float m30, m31, m32, m33;

        public Matrix4F () {
            loadIdentity();
        }

        public void loadIdentity () {
            m00 = 1; m01 = m02 = m03 = 0;
            m11 = 1; m10 = m12 = m13 = 0;
            m22 = 1; m20 = m21 = m23 = 0;
            m33 = 1; m30 = m31 = m32 = 0;
        }

        public Matrix4F (float m00, float m01, float m02, float m03,
                         float m10, float m11, float m12, float m13,
                         float m20, float m21, float m22, float m23,
                         float m30, float m31, float m32, float m33) {
            this.m00 = m00;
            this.m01 = m01;
            this.m02 = m02;
            this.m03 = m03;
            this.m10 = m10;
            this.m11 = m11;
            this.m12 = m12;
            this.m13 = m13;
            this.m20 = m20;
            this.m21 = m21;
            this.m22 = m22;
            this.m23 = m23;
            this.m30 = m30;
            this.m31 = m31;
            this.m32 = m32;
            this.m33 = m33;
        }

        public void transform (Point3F point, Point3F pointOut) {
            pointOut.X = m00 * point.X + m01 * point.Y + m02 * point.Z + m03;
            pointOut.Y = m10 * point.X + m11 * point.Y + m12 * point.Z + m13;
            pointOut.Z = m20 * point.X + m21 * point.Y + m22 * point.Z + m23;
        }
    }

    class NoHash {
        const int NIL = -1;


        static void insertAfterVtx (Triangulator triRef, int iVtx) {
            int size;

            if (triRef._vtxList == null) {
                size = Math.Max(triRef._numVtxList + 1, 100);
                triRef._vtxList = new PntNode[size];
            } else if (triRef._numVtxList >= triRef._vtxList.Length) {
                size = Math.Max(triRef._numVtxList + 1,
                        triRef._vtxList.Length + 100);
                PntNode[] old = triRef._vtxList;
                triRef._vtxList = new PntNode[size];
                Array.Copy(old, 0, triRef._vtxList, 0, old.Length);
            }

            triRef._vtxList[triRef._numVtxList] = new PntNode();
            triRef._vtxList[triRef._numVtxList].pnt = iVtx;
            triRef._vtxList[triRef._numVtxList].next = triRef._reflexVertices;
            triRef._reflexVertices = triRef._numVtxList;
            ++triRef._numVtxList;
            ++triRef._numReflex;
        }

        static void deleteFromList (Triangulator triRef, int i) {
            int indPnt, indPnt1;
            int indVtx;

            if (triRef._numReflex == 0) {
                // System.out.println("NoHash:deleteFromList. numReflex is 0.");
                return;

            }
            indPnt = triRef._reflexVertices;
            if (inVtxList(triRef, indPnt) == false)
                Console.WriteLine("NoHash:deleteFromList. Problem :Not is InVtxList ..." +
                           indPnt);

            indVtx = triRef._vtxList[indPnt].pnt;

            if (indVtx == i) {
                triRef._reflexVertices = triRef._vtxList[indPnt].next;
                --triRef._numReflex;
            } else {
                indPnt1 = triRef._vtxList[indPnt].next;
                while (indPnt1 != NIL) {
                    if (inVtxList(triRef, indPnt1) == false)
                        Console.WriteLine("NoHash:deleteFromList. Problem :Not is InVtxList ..." +
                                   indPnt1);

                    indVtx = triRef._vtxList[indPnt1].pnt;
                    if (indVtx == i) {
                        triRef._vtxList[indPnt].next = triRef._vtxList[indPnt1].next;
                        indPnt1 = NIL;
                        --triRef._numReflex;
                    } else {
                        indPnt = indPnt1;
                        indPnt1 = triRef._vtxList[indPnt].next;
                    }
                }
            }
        }

        static bool inVtxList (Triangulator triRef, int vtx) {
            return ((0 <= vtx) && (vtx < triRef._numVtxList));
        }

        static void freeNoHash (Triangulator triRef) {

            triRef._noHashingEdges = false;
            triRef._noHashingPnts = false;

            triRef._numVtxList = 0;
        }

        public static void prepareNoHashEdges (Triangulator triRef,
                       int currLoopMin, int currLoopMax) {
            triRef._loopMin = currLoopMin;
            triRef._loopMax = currLoopMax;

            triRef._noHashingEdges = true;

            return;
        }


        public static void prepareNoHashPnts (Triangulator triRef, int currLoopMin) {
            int ind, ind1;
            int i1;

            triRef._numVtxList = 0;
            triRef._reflexVertices = NIL;

            // insert the reflex vertices into a list
            ind = triRef._loops[currLoopMin];
            ind1 = ind;
            triRef._numReflex = 0;
            i1 = triRef.fetchData(ind1);

            do {
                if (triRef.getAngle(ind1) < 0)
                    insertAfterVtx(triRef, ind1);

                ind1 = triRef.fetchNextData(ind1);
                i1 = triRef.fetchData(ind1);
            } while (ind1 != ind);

            triRef._noHashingPnts = true;

        }

        public static bool noHashIntersectionExists (Triangulator triRef, int i1, int ind1,
                            int i2, int i3, BBox bb) {
            int indVtx, ind5;
            int indPnt;
            int i4, i5;
            int[] type = new int[1];
            bool flag;
            double y;

            if (triRef._noHashingPnts == false)
                Console.WriteLine("NoHash:noHashIntersectionExists noHashingPnts is false");

            // assert(InPointsList(i1));
            // assert(InPointsList(i2));
            // assert(InPointsList(i3));

            if (triRef._numReflex <= 0) return false;

            // first, let's extend the BBox of the line segment  i2, i3  to  a BBox
            // of the entire triangle.
            if (i1 < bb.imin) bb.imin = i1;
            else if (i1 > bb.imax) bb.imax = i1;
            y = triRef._points[i1].Y;
            if (y < bb.ymin) bb.ymin = y;
            else if (y > bb.ymax) bb.ymax = y;

            // check whether the triangle  i1, i2, i3  contains any reflex vertex; we
            // assume that  i2, i3  is the new diagonal, and that the triangle is
            // oriented CCW.
            indPnt = triRef._reflexVertices;
            flag = false;
            do {
                // assert(InVtxList(ind_pnt));
                indVtx = triRef._vtxList[indPnt].pnt;
                // assert(InPolyList(ind_vtx));
                i4 = triRef.fetchData(indVtx);


                if (bb.pntInBBox(triRef, i4)) {
                    // only if the reflex vertex lies inside the BBox of the triangle.
                    ind5 = triRef.fetchNextData(indVtx);
                    i5 = triRef.fetchData(ind5);
                    if ((indVtx != ind1) && (indVtx != ind5)) {
                        // only if this node isn't  i1,  and if it still belongs to the
                        // polygon
                        if (i4 == i1) {
                            if (Degenerate.handleDegeneracies(triRef, i1, ind1, i2, i3, i4, indVtx))
                                return true;
                        } else if ((i4 != i2) && (i4 != i3)) {
                            flag = Numerics.vtxInTriangle(triRef, i1, i2, i3, i4, type);
                            if (flag) return true;
                        }
                    }
                }
                indPnt = triRef._vtxList[indPnt].next;

            } while (indPnt != NIL);

            return false;
        }




        public static void deleteReflexVertex (Triangulator triRef, int ind) {
            // assert(InPolyList(ind));
            deleteFromList(triRef, ind);
        }




        public static bool noHashEdgeIntersectionExists (Triangulator triRef, BBox bb, int i1,
                            int i2, int ind5, int i5) {
            int ind, ind2;
            int i, i3, i4;
            BBox bb1;

            if (triRef._noHashingEdges == false)
                Console.WriteLine("NoHash:noHashEdgeIntersectionExists noHashingEdges is false");

            triRef._identCntr = 0;

            // check the boundary segments.
            for (i = triRef._loopMin; i < triRef._loopMax; ++i) {
                ind = triRef._loops[i];
                ind2 = ind;
                i3 = triRef.fetchData(ind2);

                do {
                    ind2 = triRef.fetchNextData(ind2);
                    i4 = triRef.fetchData(ind2);
                    // check this segment. we first compute its bounding box.
                    bb1 = new BBox(triRef, i3, i4);
                    if (bb.BBoxOverlap(bb1)) {
                        if (Numerics.segIntersect(triRef, bb.imin, bb.imax, bb1.imin, bb1.imax, i5))
                            return true;
                    }
                    i3 = i4;
                } while (ind2 != ind);
            }

            // oops! this segment shares one endpoint with at least four other
            // boundary segments! oh well, yet another degenerate situation...
            if (triRef._identCntr >= 4) {
                if (BottleNeck.checkBottleNeck(triRef, i5, i1, i2, ind5))
                    return true;
                else
                    return false;
            }

            return false;
        }

    }


    class Numerics {

        public static double max3 (double a, double b, double c) {
            return (((a) > (b)) ? (((a) > (c)) ? (a) : (c))
                : (((b) > (c)) ? (b) : (c)));
        }

        public static double min3 (double a, double b, double c) {
            return (((a) < (b)) ? (((a) < (c)) ? (a) : (c))
                : (((b) < (c)) ? (b) : (c)));
        }

        public static bool lt (double a, double eps) {
            return ((a) < -eps);
        }

        public static bool le (double a, double eps) {
            return (a <= eps);
        }

        public static bool ge (double a, double eps) {
            return (!((a) <= -eps));
        }

        public static bool eq (double a, double eps) {
            return (((a) <= eps) && !((a) < -eps));
        }

        public static bool gt (double a, double eps) {
            return !((a) <= eps);
        }

        public static double baseLength (Tuple2F u, Tuple2F v) {
            double x, y;
            x = (v).X - (u).X;
            y = (v).Y - (u).Y;
            return Math.Abs(x) + Math.Abs(y);
        }

        public static double sideLength (Tuple2F u, Tuple2F v) {
            double x, y;
            x = (v).X - (u).X;
            y = (v).Y - (u).Y;
            return x * x + y * y;
        }

        /**
         * This checks whether  i3,  which is collinear with  i1, i2,  is
         * between  i1, i2. note that we rely on the lexicographic sorting of the
         * points!
         */
        public static bool inBetween (int i1, int i2, int i3) {
            return ((i1 <= i3) && (i3 <= i2));
        }

        public static bool strictlyInBetween (int i1, int i2, int i3) {
            return ((i1 < i3) && (i3 < i2));
        }

        /**
         * this method computes the determinant  det(points[i],points[j],points[k])
         * in a consistent way.
         */
        public static double stableDet2D (Triangulator triRef, int i, int j, int k) {
            double det;
            Point2F numericsHP, numericsHQ, numericsHR;

            //      if((triRef.inPointsList(i)==false)||(triRef.inPointsList(j)==false)||
            // (triRef.inPointsList(k)==false))
            //  System.out.println("Numerics.stableDet2D Not inPointsList " + i + " " + j
            //		     + " " + k);

            if ((i == j) || (i == k) || (j == k)) {
                det = 0.0;
            } else {
                numericsHP = triRef._points[i];
                numericsHQ = triRef._points[j];
                numericsHR = triRef._points[k];

                if (i < j) {
                    if (j < k)            /* i < j < k  */
                        det = Basic.det2D(numericsHP, numericsHQ, numericsHR);
                    else if (i < k)       /* i < k < j  */
                        det = -Basic.det2D(numericsHP, numericsHR, numericsHQ);
                    else                  /* k < i < j  */
                        det = Basic.det2D(numericsHR, numericsHP, numericsHQ);
                } else {
                    if (i < k)            /* j < i < k  */
                        det = -Basic.det2D(numericsHQ, numericsHP, numericsHR);
                    else if (j < k)      /* j < k < i  */
                        det = Basic.det2D(numericsHQ, numericsHR, numericsHP);
                    else                  /* k < j < i */
                        det = -Basic.det2D(numericsHR, numericsHQ, numericsHP);
                }
            }

            return det;
        }

        /**
         * Returns the orientation of the triangle.
         * @return +1 if the points  i, j, k are given in CCW order;
         * -1 if the points  i, j, k are given in CW order;
         * 0 if the points  i, j, k are collinear.
         */
        public static int orientation (Triangulator triRef, int i, int j, int k) {
            int ori;
            double numericsHDet;
            numericsHDet = stableDet2D(triRef, i, j, k);
            // System.out.println("orientation : numericsHDet " + numericsHDet);
            if (lt(numericsHDet, triRef.epsilon)) ori = -1;
            else if (gt(numericsHDet, triRef.epsilon)) ori = 1;
            else ori = 0;
            return ori;
        }

        /**
         * This method checks whether  l  is in the cone defined by  i, j  and  j, k
         */
        public static bool isInCone (Triangulator triRef, int i, int j, int k,
                    int l, bool convex) {
            bool flag;
            int numericsHOri1, numericsHOri2;

            //      if((triRef.inPointsList(i)==false)||(triRef.inPointsList(j)==false)||
            //	 (triRef.inPointsList(k)==false)||(triRef.inPointsList(l)==false))
            //	   System.out.println("Numerics.isInCone Not inPointsList " + i + " " + j
            //	      + " " + k + " " + l);

            flag = true;
            if (convex) {
                if (i != j) {
                    numericsHOri1 = orientation(triRef, i, j, l);
                    // System.out.println("isInCone : i != j, numericsHOri1 = " + numericsHOri1);
                    if (numericsHOri1 < 0) flag = false;
                    else if (numericsHOri1 == 0) {
                        if (i < j) {
                            if (!inBetween(i, j, l)) flag = false;
                        } else {
                            if (!inBetween(j, i, l)) flag = false;
                        }
                    }
                }
                if ((j != k) && (flag == true)) {
                    numericsHOri2 = orientation(triRef, j, k, l);
                    // System.out.println("isInCone : ((j != k)  &&  (flag == true)), numericsHOri2 = " +
                    // numericsHOri2);
                    if (numericsHOri2 < 0) flag = false;
                    else if (numericsHOri2 == 0) {
                        if (j < k) {
                            if (!inBetween(j, k, l)) flag = false;
                        } else {
                            if (!inBetween(k, j, l)) flag = false;
                        }
                    }
                }
            } else {
                numericsHOri1 = orientation(triRef, i, j, l);
                if (numericsHOri1 <= 0) {
                    numericsHOri2 = orientation(triRef, j, k, l);
                    if (numericsHOri2 < 0) flag = false;
                }
            }
            return flag;
        }


        /**
         * Returns convex angle flag.
         * @return 0 ... if angle is 180 degrees <br>
         *         1 ... if angle between 0 and 180 degrees <br>
         *         2 ... if angle is 0 degrees <br>
         *        -1 ... if angle between 180 and 360 degrees <br>
         *        -2 ... if angle is 360 degrees <br>
         */
        public static int isConvexAngle (Triangulator triRef, int i, int j, int k, int ind) {
            int angle;
            double numericsHDot;
            int numericsHOri1;
            Point2F numericsHP, numericsHQ;

            //      if((triRef.inPointsList(i)==false)||(triRef.inPointsList(j)==false)||
            //	 (triRef.inPointsList(k)==false))
            //	  System.out.println("Numerics.isConvexAngle: Not inPointsList " + i + " " + j
            //			     + " " + k);

            if (i == j) {
                if (j == k) {
                    // all three vertices are identical; we set the angle to 1 in
                    // order to enable clipping of  j.
                    return 1;
                } else {
                    // two of the three vertices are identical; we set the angle to 1
                    // in order to enable clipping of  j.
                    return 1;
                }
            } else if (j == k) {
                // two vertices are identical. we could either determine the angle
                // by means of yet another lengthy analysis, or simply set the
                // angle to -1. using -1 means to err on the safe side, as all the
                // incarnations of this vertex will be clipped right at the start
                // of the ear-clipping algorithm. thus, eventually there will be no
                // other duplicates at this vertex position, and the regular
                // classification of angles will yield the correct answer for j.
                return -1;
            } else {
                numericsHOri1 = orientation(triRef, i, j, k);
                // System.out.println("i " + i + " j " + j + " k " + k + " ind " + ind +
                //		   ". In IsConvexAngle numericsHOri1 is " +
                //		   numericsHOri1);
                if (numericsHOri1 > 0) {
                    angle = 1;
                } else if (numericsHOri1 < 0) {
                    angle = -1;
                } else {
                    // 0, 180, or 360 degrees.
                    numericsHP = new Point2F();
                    numericsHQ = new Point2F();
                    Basic.vectorSub2D(triRef._points[i], triRef._points[j], numericsHP);
                    Basic.vectorSub2D(triRef._points[k], triRef._points[j], numericsHQ);
                    numericsHDot = Basic.dotProduct2D(numericsHP, numericsHQ);
                    if (numericsHDot < 0.0) {
                        // 180 degrees.
                        angle = 0;
                    } else {
                        // 0 or 360 degrees? this cannot be judged locally, and more
                        // work is needed.

                        angle = spikeAngle(triRef, i, j, k, ind);
                        // System.out.println("SpikeAngle return is "+ angle);
                    }
                }
            }
            return angle;
        }


        /**
         * This method checks whether point  i4  is inside of or on the boundary
         * of the triangle  i1, i2, i3.
         */
        public static bool pntInTriangle (Triangulator triRef, int i1, int i2, int i3, int i4) {
            bool inside;
            int numericsHOri1;

            inside = false;
            numericsHOri1 = orientation(triRef, i2, i3, i4);
            if (numericsHOri1 >= 0) {
                numericsHOri1 = orientation(triRef, i1, i2, i4);
                if (numericsHOri1 >= 0) {
                    numericsHOri1 = orientation(triRef, i3, i1, i4);
                    if (numericsHOri1 >= 0) inside = true;
                }
            }
            return inside;
        }


        /**
         * This method checks whether point  i4  is inside of or on the boundary
         * of the triangle  i1, i2, i3. it also returns a classification if  i4  is
         * on the boundary of the triangle (except for the edge  i2, i3).
         */
        public static bool vtxInTriangle (Triangulator triRef, int i1, int i2, int i3,
                     int i4, int[] type) {
            bool inside;
            int numericsHOri1;

            inside = false;
            numericsHOri1 = orientation(triRef, i2, i3, i4);
            if (numericsHOri1 >= 0) {
                numericsHOri1 = orientation(triRef, i1, i2, i4);
                if (numericsHOri1 > 0) {
                    numericsHOri1 = orientation(triRef, i3, i1, i4);
                    if (numericsHOri1 > 0) {
                        inside = true;
                        type[0] = 0;
                    } else if (numericsHOri1 == 0) {
                        inside = true;
                        type[0] = 1;
                    }
                } else if (numericsHOri1 == 0) {
                    numericsHOri1 = orientation(triRef, i3, i1, i4);
                    if (numericsHOri1 > 0) {
                        inside = true;
                        type[0] = 2;
                    } else if (numericsHOri1 == 0) {
                        inside = true;
                        type[0] = 3;
                    }
                }
            }
            return inside;
        }


        /**
         * Checks whether the line segments  i1, i2  and  i3, i4  intersect. no
         * intersection is reported if they intersect at a common vertex.
         * the function assumes that  i1 <= i2  and  i3 <= i4. if  i3  or  i4  lies
         * on  i1, i2  then an intersection is reported, but no intersection is
         * reported if  i1  or  i2  lies on  i3, i4. this function is not symmetric!
         */
        public static bool segIntersect (Triangulator triRef, int i1, int i2, int i3,
                    int i4, int i5) {
            int ori1, ori2, ori3, ori4;

            if ((i1 == i2) || (i3 == i4)) return false;
            if ((i1 == i3) && (i2 == i4)) return true;

            if ((i3 == i5) || (i4 == i5)) ++(triRef._identCntr);

            ori3 = orientation(triRef, i1, i2, i3);
            ori4 = orientation(triRef, i1, i2, i4);
            if (((ori3 == 1) && (ori4 == 1)) ||
                ((ori3 == -1) && (ori4 == -1))) return false;

            if (ori3 == 0) {
                if (strictlyInBetween(i1, i2, i3)) return true;
                if (ori4 == 0) {
                    if (strictlyInBetween(i1, i2, i4)) return true;
                } else return false;
            } else if (ori4 == 0) {
                if (strictlyInBetween(i1, i2, i4)) return true;
                else return false;
            }

            ori1 = orientation(triRef, i3, i4, i1);
            ori2 = orientation(triRef, i3, i4, i2);
            if (((ori1 <= 0) && (ori2 <= 0)) ||
                ((ori1 >= 0) && (ori2 >= 0))) return false;

            return true;
        }


        /**
         * this function computes a quality measure of a triangle  i, j, k.
         * it returns the ratio  `base / height', where   base  is the length of the
         * longest side of the triangle, and   height  is the normal distance
         * between the vertex opposite of the base side and the base side. (as
         * usual, we again use the l1-norm for distances.)
         */
        public static double getRatio (Triangulator triRef, int i, int j, int k) {
            double area, a, b, c, bs, ratio;
            Point2F p, q, r;

            p = triRef._points[i];
            q = triRef._points[j];
            r = triRef._points[k];


            a = baseLength(p, q);
            b = baseLength(p, r);
            c = baseLength(r, q);
            bs = max3(a, b, c);

            if ((10.0 * a) < Math.Min(b, c)) return 0.1;

            area = stableDet2D(triRef, i, j, k);
            if (lt(area, triRef.epsilon)) {
                area = -area;
            } else if (!gt(area, triRef.epsilon)) {
                if (bs > a) return 0.1;
                else return Double.MaxValue;
            }

            ratio = bs * bs / area;

            if (ratio < 10.0) return ratio;
            else {
                if (a < bs) return 0.1;
                else return ratio;
            }
        }


        static int spikeAngle (Triangulator triRef, int i, int j, int k, int ind) {
            int ind1, ind2, ind3;
            int i1, i2, i3;

            ind2 = ind;
            i2 = triRef.fetchData(ind2);

            ind1 = triRef.fetchPrevData(ind2);
            i1 = triRef.fetchData(ind1);

            ind3 = triRef.fetchNextData(ind2);
            i3 = triRef.fetchData(ind3);

            return recSpikeAngle(triRef, i, j, k, ind1, ind3);
        }



        static int recSpikeAngle (Triangulator triRef, int i1, int i2, int i3,
                     int ind1, int ind3) {
            int ori, ori1, ori2, i0, ii1, ii2;
            Point2F pq, pr;
            double dot;

            if (ind1 == ind3) {
                // all points are collinear???  well, then it does not really matter
                // which angle is returned. perhaps, -2 is the best bet as my code
                // likely regards this contour as a hole.
                return -2;
            }

            if (i1 != i3) {
                if (i1 < i2) {
                    ii1 = i1;
                    ii2 = i2;
                } else {
                    ii1 = i2;
                    ii2 = i1;
                }
                if (inBetween(ii1, ii2, i3)) {
                    i2 = i3;
                    ind3 = triRef.fetchNextData(ind3);
                    i3 = triRef.fetchData(ind3);

                    if (ind1 == ind3) return 2;
                    ori = orientation(triRef, i1, i2, i3);
                    if (ori > 0) return 2;
                    else if (ori < 0) return -2;
                    else return recSpikeAngle(triRef, i1, i2, i3, ind1, ind3);
                } else {
                    i2 = i1;
                    ind1 = triRef.fetchPrevData(ind1);
                    i1 = triRef.fetchData(ind1);
                    if (ind1 == ind3) return 2;
                    ori = orientation(triRef, i1, i2, i3);
                    if (ori > 0) return 2;
                    else if (ori < 0) return -2;
                    else return recSpikeAngle(triRef, i1, i2, i3, ind1, ind3);
                }
            } else {
                i0 = i2;
                i2 = i1;
                ind1 = triRef.fetchPrevData(ind1);
                i1 = triRef.fetchData(ind1);

                if (ind1 == ind3) return 2;
                ind3 = triRef.fetchNextData(ind3);
                i3 = triRef.fetchData(ind3);
                if (ind1 == ind3) return 2;
                ori = orientation(triRef, i1, i2, i3);
                if (ori > 0) {
                    ori1 = orientation(triRef, i1, i2, i0);
                    if (ori1 > 0) {
                        ori2 = orientation(triRef, i2, i3, i0);
                        if (ori2 > 0) return -2;
                    }
                    return 2;
                } else if (ori < 0) {
                    ori1 = orientation(triRef, i2, i1, i0);
                    if (ori1 > 0) {
                        ori2 = orientation(triRef, i3, i2, i0);
                        if (ori2 > 0) return 2;
                    }
                    return -2;
                } else {
                    pq = new Point2F();
                    Basic.vectorSub2D(triRef._points[i1], triRef._points[i2], pq);
                    pr = new Point2F();
                    Basic.vectorSub2D(triRef._points[i3], triRef._points[i2], pr);
                    dot = Basic.dotProduct2D(pq, pr);
                    if (dot < 0.0) {
                        ori = orientation(triRef, i2, i1, i0);
                        if (ori > 0) return 2;
                        else return -2;
                    } else {
                        return recSpikeAngle(triRef, i1, i2, i3, ind1, ind3);
                    }
                }
            }
        }


        /**
         * computes the signed angle between  p, p1  and  p, p2.
         *
         * warning: this function does not handle a 180-degree angle correctly!
         *          (this is no issue in our application, as we will always compute
         *           the angle centered at the mid-point of a valid diagonal.)
         */
        public static double angle (Triangulator triRef, Point2F p, Point2F p1, Point2F p2) {
            int sign;
            double angle1, angle2, angle;
            Point2F v1, v2;

            sign = Basic.signEps(Basic.det2D(p2, p, p1), triRef.epsilon);

            if (sign == 0) return 0.0;

            v1 = new Point2F();
            v2 = new Point2F();
            Basic.vectorSub2D(p1, p, v1);
            Basic.vectorSub2D(p2, p, v2);

            angle1 = Math.Atan2(v1.Y, v1.X);
            angle2 = Math.Atan2(v2.Y, v2.X);

            if (angle1 < 0.0) angle1 += 2.0 * Math.PI;
            if (angle2 < 0.0) angle2 += 2.0 * Math.PI;

            angle = angle1 - angle2;
            if (angle > Math.PI) angle = 2.0 * Math.PI - angle;
            else if (angle < -Math.PI) angle = 2.0 * Math.PI + angle;

            if (sign == 1) {
                if (angle < 0.0) return -angle;
                else return angle;
            } else {
                if (angle > 0.0) return -angle;
                else return angle;
            }
        }

    }



    /**
     * Triangulator is a utility for turning arbitrary polygons into triangles
     * so they can be rendered by Java 3D.
     * Polygons can be concave, nonplanar, and can contain holes.
     * @see GeometryInfo
     */
    public class Triangulator {

        GeometryInfo gInfo = null;

        internal int[] _faces = null;
        internal int[] _loops = null;
        internal int[] _chains = null;
        internal Point2F[] _points = null;
        internal Triangle[] _triangles = null;
        internal ListNode[] _list = null;

        internal Random _randomGen = null;

        internal int _numPoints = 0;
        internal int _maxNumPoints = 0;
        internal int _numList = 0;
        internal int _maxNumList = 0;
        internal int _numLoops = 0;
        internal int _maxNumLoops = 0;
        internal int _numTriangles = 0;
        internal int _maxNumTriangles = 0;

        internal int _numFaces = 0;
        internal int _numTexSets = 0;

        internal int _firstNode = 0;

        internal int _numChains = 0;
        internal int _maxNumChains = 0;

        // For Clean class.
        internal Point2F[] _pUnsorted = null;
        internal int _maxNumPUnsorted = 0;

        // For NoHash class.
        internal bool _noHashingEdges = false;
        internal bool _noHashingPnts = false;
        internal int _loopMin, _loopMax;
        internal PntNode[] _vtxList = null;
        internal int _numVtxList = 0;
        internal int _numReflex = 0;
        internal int _reflexVertices;

        // For Bridge class.
        internal Distance[] _distances = null;
        internal int _maxNumDist = 0;
        internal Left[] _leftMost = null;
        internal int _maxNumLeftMost = 0;

        // For Heap class.
        internal HeapNode[] _heap = null;
        internal int _numHeap = 0;
        internal int _maxNumHeap = 0;
        internal int _numZero = 0;

        // For Orientation class.
        internal int _maxNumPolyArea = 0;
        internal double[] _polyArea = null;

        internal int[] _stripCounts = null;
        internal int[] _vertexIndices = null;
        internal Point3F[] _vertices = null;
        internal Object[] _colors = null;
        internal Vector3F[] _normals = null;

        internal bool _ccwLoop = true;

        internal bool _earsRandom = true;
        internal bool _earsSorted = true;

        internal int _identCntr;  // Not sure what is this for. (Ask Martin)

        //  double epsilon = 1.0e-12;
        internal double epsilon = 1.0e-12;

        internal const double ZERO = 1.0e-8;
        internal const int EARS_SEQUENCE = 0;
        internal const int EARS_RANDOM = 1;
        internal const int EARS_SORTED = 2;


        internal const int INC_LIST_BK = 100;
        internal const int INC_LOOP_BK = 20;
        internal const int INC_TRI_BK = 50;
        internal const int INC_POINT_BK = 100;
        internal const int INC_DIST_BK = 50;

        internal const int DEBUG = 0;

        /**
         * Creates a new instance of the Triangulator.
         * @deprecated This class is created automatically when needed in
         * GeometryInfo and never needs to be used directly.  Putting data
         * into a GeometryInfo with primitive POLYGON_ARRAY automatically
         * causes the triangulator to be created and used.
         */
        public Triangulator () {
            _earsRandom = false;
            _earsSorted = false;
        }

        /**
         * Creates a new instance of a Triangulator.
         * @deprecated This class is created automatically when needed in
         * GeometryInfo and never needs to be used directly.  Putting data
         * into a GeometryInfo with primitive POLYGON_ARRAY automatically
         * causes the triangulator to be created and used.
         */
        public Triangulator (int earOrder) {
            switch (earOrder) {
                case EARS_SEQUENCE:
                    _earsRandom = false;
                    _earsSorted = false;
                    break;
                case EARS_RANDOM:
                    _randomGen = new Random();
                    _earsRandom = true;
                    _earsSorted = false;
                    break;
                case EARS_SORTED:
                    _earsRandom = false;
                    _earsSorted = true;
                    break;
                default:
                    _earsRandom = false;
                    _earsSorted = false;
                    break;
            }

        }

        /**
         * This routine converts the GeometryInfo object from primitive type
         * POLYGON_ARRAY to primitive type TRIANGLE_ARRAY using polygon
         * decomposition techniques.
         * <p>
         * <pre>
         * Example of usage:
         *   Triangulator tr = new Triangulator();
         *   tr.triangulate(ginfo); // ginfo contains the geometry.
         *   shape.setGeometry(ginfo.getGeometryArray()); // shape is a Shape3D.
         *<p></pre>
         * @param gi Geometry to be triangulated
         **/
        public void Triangulate (GeometryInfo gi) {
            int i, j, k;
            int sIndex = 0, index, currLoop, lastInd, ind;
            bool proceed;
            bool reset = false;

            bool[] done = new bool[1];
            bool[] gotIt = new bool[1];

            if (gi.GetPrimitive() != GeometryInfo.POLYGON_ARRAY)
                throw new ArgumentOutOfRangeException(LS.T("GeometryInfo must have primitive type Polygon."));

            gi.Indexify();

            _vertices = gi.GetCoordinates();
            if (_vertices != null)
                _vertexIndices = gi.GetCoordinateIndices();
            else
                _vertexIndices = null;

            _colors = gi.GetColors();
            _normals = gi.GetNormals();
            gInfo = gi;


            _stripCounts = gi.GetStripCounts();

            _faces = gi.GetContourCounts();
            if (_faces == null) {
                if (_stripCounts == null)
                    Console.WriteLine("StripCounts is null! Don't know what to do.");

                _faces = new int[_stripCounts.Length];
                for (i = 0; i < _stripCounts.Length; i++)
                    _faces[i] = 1;
            }

            _numFaces = _faces.Length;
            _numTexSets = gInfo.GetTexCoordSetCount();

            _maxNumLoops = 0;
            _maxNumList = 0;
            _maxNumPoints = 0;
            _maxNumDist = 0;
            _maxNumLeftMost = 0;
            _maxNumPUnsorted = 0;

            // Compute the length of loops and list.
            for (i = 0; i < _faces.Length; i++) {
                _maxNumLoops += _faces[i];
                for (j = 0; j < _faces[i]; j++, sIndex++) {
                    _maxNumList += (_stripCounts[sIndex] + 1);
                }
            }

            // Add some incase of bridges.
            _maxNumList += 20;

            _loops = new int[_maxNumLoops];
            _list = new ListNode[_maxNumList];
            // maxNumPoints = vertices.length;
            //points = new Point2F[maxNumPoints];


            // Construct data for use in triangulation.
            _numVtxList = 0;
            _numReflex = 0;

            _numTriangles = 0;
            _numChains = 0;
            _numPoints = 0;
            _numLoops = 0;
            _numList = 0;
            sIndex = 0;
            index = 0;

            for (i = 0; i < _faces.Length; i++) {
                for (j = 0; j < _faces[i]; j++, sIndex++) {

                    currLoop = makeLoopHeader();
                    lastInd = _loops[currLoop];

                    for (k = 0; k < _stripCounts[sIndex]; k++) {
                        _list[_numList] = new ListNode(_vertexIndices[index]);
                        ind = _numList++;

                        insertAfter(lastInd, ind);
                        _list[ind].SetCommonIndex(index);

                        lastInd = ind;
                        index++;

                    } // index k.

                    deleteHook(currLoop);

                } // index j.
            } // index i.


            // Done with constructing data. We can start to triangulate now.

            _maxNumTriangles = _maxNumList / 2;
            _triangles = new Triangle[_maxNumTriangles];

            // set the numerical precision threshold
            setEpsilon(ZERO);

            // process the polygonal faces of the polyhedron. for every face, we
            // check whether it is a triangle or a quadrangle in order to weed out
            // simple cases which do not require the full triangulation algorithm

            int i1 = 0;
            int i2 = 0;
            for (j = 0; j < _numFaces; ++j) {
                _ccwLoop = true;
                done[0] = false;
                i2 = i1 + _faces[j];

                if (_faces[j] > 1) {
                    proceed = true;
                } else if (Simple.simpleFace(this, _loops[i1]))
                    proceed = false;
                else
                    proceed = true;

                if (proceed) {
                    // Do some preprocessing here.

                    for (int lpIndex = 0; lpIndex < _faces[j]; lpIndex++)
                        preProcessList(i1 + lpIndex);

                    // project the polygonal face onto a plane
                    Project.projectFace(this, i1, i2);

                    // sort the points of this face in lexicographic order and  discard
                    // duplicates

                    int removed = Clean.cleanPolyhedralFace(this, i1, i2);

                    // determine the orientation of the polygon; the default
                    // orientation is CCW for the outer polygon
                    if (_faces[j] == 1) {
                        Orientation.DetermineOrientation(this, _loops[i1]);
                    } else {
                        Orientation.AdjustOrientation(this, i1, i2);
                    }

                    // CE2 only
                    if (_faces[j] > 1) {
                        NoHash.prepareNoHashEdges(this, i1, i2);
                    } else {
                        _noHashingEdges = false;
                        _noHashingPnts = false;
                    }


                    // mark those vertices whose interior angle is convex
                    for (i = i1; i < i2; ++i) {
                        EarClip.classifyAngles(this, _loops[i]);
                    }

                    // link the holes with the outer boundary by means of "bridges"
                    if (_faces[j] > 1) Bridge.constructBridges(this, i1, i2);

                    // put all ears into a circular linked list
                    resetPolyList(_loops[i1]);
                    NoHash.prepareNoHashPnts(this, i1);
                    EarClip.classifyEars(this, _loops[i1]);
                    done[0] = false;

                    // triangulate the polygon
                    while (!done[0]) {
                        if (!EarClip.clipEar(this, done)) {
                            if (reset) {
                                ind = getNode();
                                resetPolyList(ind);

                                _loops[i1] = ind;
                                if (Desperate.desperate(this, ind, i1, done)) {
                                    if (!Desperate.letsHope(this, ind))
                                        return;
                                } else {
                                    reset = false;
                                }
                            } else {
                                ind = getNode();
                                resetPolyList(ind);

                                EarClip.classifyEars(this, ind);
                                reset = true;
                            }
                        } else {
                            reset = false;
                        }

                        if (done[0]) {
                            // System.out.println("In done[0] is true");
                            ind = getNextChain(gotIt);
                            if (gotIt[0]) {
                                // at some point of the triangulation, we could not find
                                // any ear and the polygon was split into two parts. now
                                // we have to handle (one of) the remaining parts.
                                resetPolyList(ind);
                                _loops[i1] = ind;
                                _noHashingPnts = false;
                                NoHash.prepareNoHashPnts(this, i1);
                                EarClip.classifyEars(this, ind);
                                reset = false;
                                done[0] = false;
                            }
                        }
                    }
                }

                i1 = i2;

            }

            // Output triangles here.
            writeTriangleToGeomInfo();
        }

        void printVtxList () {
            int i;
            Console.WriteLine("numReflex " + _numReflex + " reflexVertices " +
                       _reflexVertices);
            for (i = 0; i < _numVtxList; i++)
                Console.WriteLine(i + " pnt " + _vtxList[i].pnt +
                           ", next " + _vtxList[i].next);
        }

        void printListData () {
            for (int i = 0; i < _numList; i++)
                Console.WriteLine("list[" + i + "].index " + _list[i].index +
                           ", prev " + _list[i].prev +
                           ", next " + _list[i].next +
                           ", convex " + _list[i].convex +
                           ", vertexIndex " + _list[i].vcntIndex);
        }

        void preProcessList (int i1) {
            int tInd, tInd1, tInd2;

            resetPolyList(_loops[i1]);
            tInd = _loops[i1];
            tInd1 = tInd;
            tInd2 = _list[tInd1].next;
            while (tInd2 != tInd) {
                if (_list[tInd1].index == _list[tInd2].index) {
                    if (tInd2 == _loops[i1])
                        _loops[i1] = _list[tInd2].next;
                    deleteLinks(tInd2);
                }
                tInd1 = _list[tInd1].next;
                tInd2 = _list[tInd1].next;
            }
        }

        void writeTriangleToGeomInfo () {
            int i, currIndex;

            // There are 2 approaches to take here : (1) Output all triangles as
            // a single face.(Easy) (2) Preserve the faces of the polyhedron and
            // sets of triangles per face. ( Seems to be the preferred approach, but
            // a check in GeometryInfo and the old GeomInfoConverter doesn't seems
            // to support this. Will check with Dan and Paul. Will take the easy way first.

            gInfo.SetPrimitive(GeometryInfo.TRIANGLE_ARRAY);
            gInfo.SetContourCounts(null);
            gInfo.ForgetOldPrim();
            gInfo.SetStripCounts(null);

            currIndex = 0;
            int[] newVertexIndices = new int[_numTriangles * 3];
            int index;
            for (i = 0; i < _numTriangles; i++) {
                index = _list[_triangles[i].V1].GetCommonIndex();
                newVertexIndices[currIndex++] = _vertexIndices[index];
                index = _list[_triangles[i].V2].GetCommonIndex();
                newVertexIndices[currIndex++] = _vertexIndices[index];
                index = _list[_triangles[i].V3].GetCommonIndex();
                newVertexIndices[currIndex++] = _vertexIndices[index];
            }
            gInfo.SetCoordinateIndices(newVertexIndices);

            if (_normals != null) {
                int[] oldNormalIndices = gInfo.GetNormalIndices();
                int[] newNormalIndices = new int[_numTriangles * 3];
                currIndex = 0;
                for (i = 0; i < _numTriangles; i++) {
                    index = _list[_triangles[i].V1].GetCommonIndex();
                    newNormalIndices[currIndex++] = oldNormalIndices[index];
                    index = _list[_triangles[i].V2].GetCommonIndex();
                    newNormalIndices[currIndex++] = oldNormalIndices[index];
                    index = _list[_triangles[i].V3].GetCommonIndex();
                    newNormalIndices[currIndex++] = oldNormalIndices[index];
                }
                gInfo.SetNormalIndices(newNormalIndices);
            }

            if (_colors != null) {
                currIndex = 0;
                int[] oldColorIndices = gInfo.GetColorIndices();
                int[] newColorIndices = new int[_numTriangles * 3];
                for (i = 0; i < _numTriangles; i++) {
                    index = _list[_triangles[i].V1].GetCommonIndex();
                    newColorIndices[currIndex++] = oldColorIndices[index];
                    index = _list[_triangles[i].V2].GetCommonIndex();
                    newColorIndices[currIndex++] = oldColorIndices[index];
                    index = _list[_triangles[i].V3].GetCommonIndex();
                    newColorIndices[currIndex++] = oldColorIndices[index];
                }
                gInfo.SetColorIndices(newColorIndices);
            }

            for (int j = 0; j < _numTexSets; j++) {
                int[] newTextureIndices = new int[_numTriangles * 3];
                int[] oldTextureIndices = gInfo.GetTextureCoordinateIndices(j);
                currIndex = 0;
                for (i = 0; i < _numTriangles; i++) {
                    index = _list[_triangles[i].V1].GetCommonIndex();
                    newTextureIndices[currIndex++] = oldTextureIndices[index];
                    index = _list[_triangles[i].V2].GetCommonIndex();
                    newTextureIndices[currIndex++] = oldTextureIndices[index];
                    index = _list[_triangles[i].V3].GetCommonIndex();
                    newTextureIndices[currIndex++] = oldTextureIndices[index];
                }
                gInfo.SetTextureCoordinateIndices(j, newTextureIndices);
            }
        }


        void setEpsilon (double eps) {
            epsilon = eps;
        }

        // Methods of handling ListNode.

        bool inPolyList (int ind) {
            return ((ind >= 0) && (ind < _numList) && (_numList <= _maxNumList));
        }



        internal void updateIndex (int ind, int index) {
            //  assert(InPolyList(ind));
            _list[ind].index = index;
        }

        internal int getAngle (int ind) {
            return _list[ind].convex;
        }

        internal void setAngle (int ind, int convex) {
            _list[ind].convex = convex;
        }


        void resetPolyList (int ind) {
            // assert(InPolyList(ind));
            _firstNode = ind;
        }

        int getNode () {
            // assert(InPolyList(first_node));
            return _firstNode;
        }

        bool inLoopList (int loop) {
            return ((loop >= 0) && (loop < _numLoops) && (_numLoops <= _maxNumLoops));
        }


        internal void deleteHook (int currLoop) {
            int ind1, ind2;

            if (!inLoopList(currLoop))
                Console.WriteLine("Triangulator:deleteHook : Loop access out of range.");

            ind1 = _loops[currLoop];
            ind2 = _list[ind1].next;
            if ((inPolyList(ind1)) && (inPolyList(ind2))) {

                deleteLinks(ind1);
                _loops[currLoop] = ind2;

            } else {
                Console.WriteLine("Triangulator:deleteHook : List access out of range.");
            }
        }

        /**
         * Deletes node ind from list (with destroying its data fields)
         */
        internal void deleteLinks (int ind) {
            if (inPolyList(ind) && inPolyList(_list[ind].prev) && inPolyList(_list[ind].next)) {
                if (_firstNode == ind)
                    _firstNode = _list[ind].next;

                _list[_list[ind].next].prev = _list[ind].prev;
                _list[_list[ind].prev].next = _list[ind].next;
                _list[ind].prev = _list[ind].next = ind;

            } else
                Console.WriteLine("Triangulator:deleteLinks : Access out of range.");

        }

        internal void rotateLinks (int ind1, int ind2) {
            int ind;
            int ind0, ind3;

            ind0 = _list[ind1].next;
            ind3 = _list[ind2].next;

            // Swap.
            ind = _list[ind1].next;
            _list[ind1].next = _list[ind2].next;
            _list[ind2].next = ind;

            _list[ind0].prev = ind2;
            _list[ind3].prev = ind1;
        }


        internal void storeChain (int ind) {
            if (_numChains >= _maxNumChains) {
                _maxNumChains += 20;
                int[] old = _chains;
                _chains = new int[_maxNumChains];
                if (old != null)
                    Array.Copy(old, 0, _chains, 0, old.Length);
            }

            _chains[_numChains] = ind;
            ++_numChains;

        }

        int getNextChain (bool[] done) {
            if (_numChains > 0) {
                done[0] = true;
                --_numChains;
                return _chains[_numChains];
            } else {
                done[0] = false;
                _numChains = 0;
                return 0;
            }
        }

        internal void splitSplice (int ind1, int ind2, int ind3, int ind4) {
            _list[ind1].next = ind4;
            _list[ind4].prev = ind1;
            _list[ind2].prev = ind3;
            _list[ind3].next = ind2;
        }

        /**
         * Allocates storage for a dummy list node; pointers are set to itself.
         * @return pointer to node
         */
        int makeHook () {
            int ind;

            ind = _numList;
            if (_numList >= _maxNumList) {
                _maxNumList += INC_LIST_BK;

                ListNode[] old = _list;
                _list = new ListNode[_maxNumList];
                Array.Copy(old, 0, _list, 0, old.Length);
            }

            _list[_numList] = new ListNode(-1);
            _list[_numList].prev = ind;
            _list[_numList].next = ind;
            _list[_numList].index = -1;
            ++_numList;

            return ind;
        }

        int makeLoopHeader () {
            int i;
            int ind;

            ind = makeHook();
            if (_numLoops >= _maxNumLoops) {
                _maxNumLoops += INC_LOOP_BK;
                // System.out.println("Triangulator: Expanding loops array ....");
                int[] old = _loops;
                _loops = new int[_maxNumLoops];
                Array.Copy(old, 0, _loops, 0, old.Length);
            }

            _loops[_numLoops] = ind;
            i = _numLoops;
            ++_numLoops;

            return i;
        }


        /**
          * Allocates storage for a new list node, and stores the index of the point
          * at this node. Pointers are set to -1.
          * @return pointer to node
          */
        internal int makeNode (int index) {
            int ind;

            if (_numList >= _maxNumList) {
                _maxNumList += INC_LIST_BK;
                ListNode[] old = _list;
                _list = new ListNode[_maxNumList];
                Array.Copy(old, 0, _list, 0, old.Length);
            }

            _list[_numList] = new ListNode(index);

            ind = _numList;
            _list[_numList].index = index;
            _list[_numList].prev = -1;
            _list[_numList].next = -1;
            ++_numList;

            return ind;
        }


        /**
         * Inserts node ind2 after node ind1.
         */
        internal void insertAfter (int ind1, int ind2) {
            int ind3;

            if (inPolyList(ind1) && inPolyList(ind2)) {

                _list[ind2].next = _list[ind1].next;
                _list[ind2].prev = ind1;
                _list[ind1].next = ind2;
                ind3 = _list[ind2].next;

                if (inPolyList(ind3))
                    _list[ind3].prev = ind2;
                else
                    Console.WriteLine("Triangulator:deleteHook : List access out of range.");

                return;
            } else
                Console.WriteLine("Triangulator:deleteHook : List access out of range.");

        }

        /**
         * Returns pointer to the successor of ind1.
         */
        internal int fetchNextData (int ind1) {
            return _list[ind1].next;
        }

        /**
         * obtains the data store at ind1
         */
        internal int fetchData (int ind1) {
            return _list[ind1].index;
        }

        /**
         * returns pointer to the successor of ind1.
         */
        internal int fetchPrevData (int ind1) {
            return _list[ind1].prev;
        }

        /**
         * swap the list pointers in order to change the orientation.
         */
        internal void swapLinks (int ind1) {
            int ind2, ind3;

            ind2 = _list[ind1].next;
            _list[ind1].next = _list[ind1].prev;
            _list[ind1].prev = ind2;
            ind3 = ind2;
            while (ind2 != ind1) {
                ind3 = _list[ind2].next;
                _list[ind2].next = _list[ind2].prev;
                _list[ind2].prev = ind3;
                ind2 = ind3;
            }
        }

        // Methods for handling Triangle.

        internal void storeTriangle (int i, int j, int k) {
            if (_numTriangles >= _maxNumTriangles) {
                _maxNumTriangles += INC_TRI_BK;
                Triangle[] old = _triangles;
                _triangles = new Triangle[_maxNumTriangles];
                if (old != null)
                    Array.Copy(old, 0, _triangles, 0, old.Length);
            }

            if (_ccwLoop)
                _triangles[_numTriangles] = new Triangle(i, j, k);
            else
                _triangles[_numTriangles] = new Triangle(j, i, k);
            _numTriangles++;
        }

        // Methods for handling Point.

        internal void initPnts (int number) {
            if (_maxNumPoints < number) {
                _maxNumPoints = number;
                _points = new Point2F[_maxNumPoints];
            }

            for (int i = 0; i < number; i++)
                _points[i] = new Point2F(0.0f, 0.0f);

            _numPoints = 0;
        }

        bool inPointsList (int index) {
            return (index >= 0 && index < _numPoints && _numPoints <= _maxNumPoints);
        }

        internal int storePoint (double x, double y) {
            int i;

            if (_numPoints >= _maxNumPoints) {
                _maxNumPoints += INC_POINT_BK;
                Point2F[] old = _points;
                _points = new Point2F[_maxNumPoints];
                if (old != null)
                    Array.Copy(old, 0, _points, 0, old.Length);
            }

            _points[_numPoints] = new Point2F((float)x, (float)y);
            i = _numPoints;
            ++_numPoints;

            return i;
        }

    }
}
