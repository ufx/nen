﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Graphics {
    class Heap {

        static void printHeapData (Triangulator triRef) {
            int i;
            Console.WriteLine("\nHeap Data : numZero " + triRef._numZero +
                       " numHeap " + triRef._numHeap);
            for (i = 0; i < triRef._numHeap; i++)
                Console.WriteLine(i + " ratio " + triRef._heap[i].ratio + ", index " +
                           triRef._heap[i].index + ", prev " +
                           triRef._heap[i].prev + ", next " +
                           triRef._heap[i].next);

            Console.WriteLine(" ");


        }

        public static void initHeap (Triangulator triRef) {
            // Calculate the maximum bounds : N + (N -2)* 2.
            //    triRef._maxNumHeap = triRef._numPoints * 3;
            triRef._maxNumHeap = triRef._numPoints;
            triRef._heap = new HeapNode[triRef._maxNumHeap];

            triRef._numHeap = 0;
            triRef._numZero = 0;

        }

        public static void storeHeapData (Triangulator triRef, int index, double ratio,
                      int ind, int prev, int next) {
            triRef._heap[index] = new HeapNode();
            triRef._heap[index].ratio = ratio;
            triRef._heap[index].index = ind;
            triRef._heap[index].prev = prev;
            triRef._heap[index].next = next;
        }

        public static void dumpOnHeap (Triangulator triRef, double ratio,
                   int ind, int prev, int next) {
            int index;

            if (triRef._numHeap >= triRef._maxNumHeap) {
                // System.out.println("Heap:dumpOnHeap.Expanding heap array ...");
                HeapNode[] old = triRef._heap;
                triRef._maxNumHeap = triRef._maxNumHeap + triRef._numPoints;
                triRef._heap = new HeapNode[triRef._maxNumHeap];
                Array.Copy(old, 0, triRef._heap, 0, old.Length);
            }
            if (ratio == 0.0) {
                if (triRef._numZero < triRef._numHeap)
                    if (triRef._heap[triRef._numHeap] == null)
                        storeHeapData(triRef, triRef._numHeap, triRef._heap[triRef._numZero].ratio,
                              triRef._heap[triRef._numZero].index,
                              triRef._heap[triRef._numZero].prev,
                              triRef._heap[triRef._numZero].next);
                    else
                        triRef._heap[triRef._numHeap].copy(triRef._heap[triRef._numZero]);
                /*
                  storeHeapData(triRef, triRef._numHeap, triRef._heap[triRef._numZero].ratio,
                  triRef._heap[triRef._numZero].index,
                  triRef._heap[triRef._numZero].prev,
                  triRef._heap[triRef._numZero].next);
                */
                index = triRef._numZero;
                ++triRef._numZero;
            } else {
                index = triRef._numHeap;
            }

            storeHeapData(triRef, index, ratio, ind, prev, next);
            ++triRef._numHeap;

        }


        public static void insertIntoHeap (Triangulator triRef, double ratio,
                       int ind, int prev, int next) {
            dumpOnHeap(triRef, ratio, ind, prev, next);
        }


        public static bool deleteFromHeap (Triangulator triRef, int[] ind,
                      int[] prev, int[] next) {
            double rnd;
            int rndInd;

            // earSorted is not implemented yet.

            if (triRef._numZero > 0) {
                // assert(num_heap >= num_zero);
                --triRef._numZero;
                --triRef._numHeap;

                ind[0] = triRef._heap[triRef._numZero].index;
                prev[0] = triRef._heap[triRef._numZero].prev;
                next[0] = triRef._heap[triRef._numZero].next;
                if (triRef._numZero < triRef._numHeap)
                    triRef._heap[triRef._numZero].copy(triRef._heap[triRef._numHeap]);
                /*
                  storeHeapData( triRef, triRef._numZero, triRef._heap[triRef._numHeap].ratio,
                  triRef._heap[triRef._numHeap].index,
                  triRef._heap[triRef._numHeap].prev,
                  triRef._heap[triRef._numHeap].next);
                */
                return true;
            } else if (triRef._earsRandom) {
                if (triRef._numHeap <= 0) {
                    triRef._numHeap = 0;
                    return false;
                }
                rnd = triRef._randomGen.NextDouble();
                rndInd = (int)(rnd * triRef._numHeap);
                --triRef._numHeap;
                if (rndInd > triRef._numHeap) rndInd = triRef._numHeap;

                ind[0] = triRef._heap[rndInd].index;
                prev[0] = triRef._heap[rndInd].prev;
                next[0] = triRef._heap[rndInd].next;
                if (rndInd < triRef._numHeap)
                    triRef._heap[rndInd].copy(triRef._heap[triRef._numHeap]);
                /*
                  storeHeapData( triRef, rndInd, triRef._heap[triRef._numHeap].ratio,
                  triRef._heap[triRef._numHeap].index,
                  triRef._heap[triRef._numHeap].prev,
                  triRef._heap[triRef._numHeap].next);
                */
                return true;
            } else {
                if (triRef._numHeap <= 0) {
                    triRef._numHeap = 0;
                    return false;
                }
                --triRef._numHeap;
                ind[0] = triRef._heap[triRef._numHeap].index;
                prev[0] = triRef._heap[triRef._numHeap].prev;
                next[0] = triRef._heap[triRef._numHeap].next;

                return true;
            }
        }

    }

    class HeapNode {
        public int index, prev, next;
        public double ratio;

        public HeapNode () {
        }

        public void copy (HeapNode hNode) {
            index = hNode.index;
            prev = hNode.prev;
            next = hNode.next;
            ratio = hNode.ratio;
        }
    }

}
