﻿#pragma warning disable 1591, 1570

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Internal;

namespace Nen.Graphics {
    /*
     * $RCSfile: GeometryInfo.java,v $
     *
     * Copyright (c) 2007 Sun Microsystems, Inc. All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without
     * modification, are permitted provided that the following conditions
     * are met:
     *
     * - Redistribution of source code must retain the above copyright
     *   notice, this list of conditions and the following disclaimer.
     *
     * - Redistribution in binary form must reproduce the above copyright
     *   notice, this list of conditions and the following disclaimer in
     *   the documentation and/or other materials provided with the
     *   distribution.
     *
     * Neither the name of Sun Microsystems, Inc. or the names of
     * contributors may be used to endorse or promote products derived
     * from this software without specific prior written permission.
     *
     * This software is provided "AS IS," without a warranty of any
     * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND
     * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
     * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY
     * EXCLUDED. SUN MICROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL
     * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
     * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
     * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR
     * ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL,
     * CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND
     * REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR
     * INABILITY TO USE THIS SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
     * POSSIBILITY OF SUCH DAMAGES.
     *
     * You acknowledge that this software is not designed, licensed or
     * intended for use in the design, construction, operation or
     * maintenance of any nuclear facility.
     *
     * $Revision: 1.4 $
     * $Date: 2007/02/09 17:20:19 $
     * $State: Exp $
     */

    /**
     * The GeometryInfo object holds data for processing by the Java3D geometry
     * utility tools.<p><blockquote>
     * 
     *         The NormalGenerator adds normals to geometry without normals.<p>
     * 
     *         The Stripifier combines adjacent triangles into triangle strips for
     *         more efficent rendering.<p></blockquote>
     * 
     * Also, the GeometryCompressor can take a set of GeometryInfo objects in a
     * CompressionSteam and generate a CompressedGeometry object from the
     * geometry.<p>
     *         Geometry is loaded into a GeometryInfo in a manner similar to the
     * <a href="../../../../../javax/media/j3d/GeometryArray.html">
     * GeometryArray</a> methods.  The constructor for the GeometryInfo takes a flag
     * that specifies the kind of data being loaded.  The vertex data is
     * specified using methods that are similar to the GeometryArray methods, but
     * with fewer variations.<p>
     *         The major difference between GeometryInfo and GeometryArray is
     * that the number of vertices, vertex format, and other data are specified
     * implictly, rather than as part of the constructor.  The number of verticies
     * comes from the number of coordinates passed to the setCoordinates()
     * method.  The format comes from the set of data components that are
     * specified.  For example, calling the setCoordinates(), setColors3() and
     * setTextureCoordinatesParames(1, 2) methods implies a
     * format of COORDINATES | COLOR_3
     * | TEXTURE_COORDINATE_2.  Indexed representation is specified by calling
     * the methods that specify the indices, for example
     * setCoordinateIndices().<p>
     *         Stripped primitives are loaded using the TRIANGLE_FAN_ARRAY or
     * TRIANGLE_STRIP_ARRAY flags to the constructor.  The setStripCounts()
     * method specifies the length of each strip.<p>
     *         A set of complex polygons is loaded using the POLYGON_ARRAY
     * flag to the constructor.  The setStripCounts() method specifies the length
     * of each contour of the polygons.  The setContourCounts() method specifies
     * the number of countours in each polygon. For example, a triangle with a
     * triangular hole would have strip counts [3, 3] (indicating two contours of
     * three points) and contour counts [2] (indicating a single polygon with two
     * contours).<p>
     *         GeometryInfo itelf contains some simple utilities, such as
     * calculating indices for non-indexed data ("indexifying") and getting rid
     * of unused data in your indexed geometry ("compacting").<p>
     *         The geometry utility tools modify the contents of the
     * GeometryInfo.  After processing, the resulting geometry can be extracted
     * from the GeometryInfo by calling getGeometryArray().  If multiple tools
     * are used, the order of processing should be: generate normals, then
     * stripify.  For example, to convert a general mesh of polygons without
     * normals into an optimized mesh call:
     * <pre><blockquote>
     *         GeometryInfo gi = new GeometryInfo(GeometryInfo.POLYGON_ARRAY);
     *         // initialize the geometry info here
     *         // generate normals
     *         NormalGenerator ng = new NormalGenerator();
     *         ng.generateNormals(gi);
     *         // stripify
     *         Stripifier st = new Stripifier();
     *         st.stripify(gi);
     *         GeometryArray result = gi.getGeometryArray();
     * </blockquote></pre>
     * 
     * @see NormalGenerator
     * @see Stripifier
     * @see com.sun.j3d.utils.compression.CompressionStream
     * @see com.sun.j3d.utils.compression.GeometryCompressor
     * @see javax.media.j3d.GeometryArray
     */



    internal class GeometryArray {
    }

    internal class NormalGenerator {
    }

    public class GeometryInfo {

        /**
         * Send to the constructor to inform that the data will be arranged so
         * that each set of three vertices form an independent triangle
         */
        public const int TRIANGLE_ARRAY = 1;

        /**
         * Send to the constructor to inform that the data will be arranged so
         * that each set of four vertices form an independent quad
         */
        public const int QUAD_ARRAY = 2;

        /**
         * Send to the constructor to inform that the data will be arranged so
         * that the stripCounts array indicates how many vertices to use
         * for each triangle fan.
         */
        public const int TRIANGLE_FAN_ARRAY = 3;

        /**
         * Send to the constructor to inform that the data will be arranged so
         * that the stripCounts array indicates how many vertices to use
         * for each triangle strip.
         */
        public const int TRIANGLE_STRIP_ARRAY = 4;

        /**
         * Send to the constructor to inform that the data is arranged as
         * possibly multi-contour, possible non-planar polygons.
         * The stripCounts array indicates how many vertices to use
         * for each contour, and the contourCounts array indicates how many
         * stripCounts entries to use for each polygon.  The first 
         * contour is the bounding polygon, and subsequent contours are
         * "holes."  If contourCounts is left null, the default is
         * one contour per polygon.
         */
        public const int POLYGON_ARRAY = 5;

        private int _prim;

        // 1 Show indexification details
        private const int DEBUG = 0;

        private Point3F[] _coordinates = null;
        private Color3F[] _colors3 = null;
        private Color4F[] _colors4 = null;
        private Vector3F[] _normals = null;
        private object[] _texCoordSets = null;

        private int[] _coordinateIndices = null;
        private int[] _colorIndices = null;
        private int[] _normalIndices = null;
        private int[][] _texCoordIndexSets = null;

        private int[] _texCoordSetMap = null;
        private int _texCoordSetCount = 0;
        private int _texCoordDim = 0;

        private int[] _stripCounts = null;
        private int[] _contourCounts = null;

        private Triangulator _tr = null;

        private int _oldPrim = 0;
        private int[] _oldStripCounts = null;

        private bool _coordOnly = false;



        /**
         * Constructor.
         * Creates an empty GeometryInfo object.  
         * @param primitive Tells the GeometryInfo object the type of
         * primitive data to be stored
         * in it, so it will know the format of the data. It can be one of
         * TRIANGLE_ARRAY, 
         * QUAD_ARRAY, TRIANGLE_FAN_ARRAY, TRIANGLE_STRIP_ARRAY, or POLYGON_ARRAY.
         */
        public GeometryInfo (int primitive) {
            if ((primitive >= TRIANGLE_ARRAY) && (primitive <= POLYGON_ARRAY)) {
                _prim = primitive;
            } else {
                throw new ArgumentOutOfRangeException("primitive", LS.T("Illegal primitive."));
            }
        } // End of GeometryInfo(int)


        /**
         * Removes all data from the GeometryInfo and resets the primitive.
         * After a call to reset(), the GeometryInfo object will be just like
         * it was when it was newly constructed.
         * @param primitive Either TRIANGLE_ARRAY, QUAD_ARRAY,
         * TRIANGLE_FAN_ARRAY, TRIANGLE_STRIP_ARRAY, or POLYGON_ARRAY.
         * Tells the GeometryInfo object the type of primitive data to be stored
         * in it, so it will know the format of the data.
         */
        public void Reset (int primitive) {
            if ((primitive >= TRIANGLE_ARRAY) && (primitive <= POLYGON_ARRAY)) {
                _prim = primitive;
            } else {
                throw new ArgumentOutOfRangeException("primitive", LS.T("Illegal primitive."));
            }

            _coordinates = null;
            _colors3 = null;
            _colors4 = null;
            _normals = null;

            _coordinateIndices = null;
            _colorIndices = null;
            _normalIndices = null;

            _stripCounts = null;
            _contourCounts = null;

            _oldPrim = 0;
            _oldStripCounts = null;

            _texCoordDim = 0;
            _texCoordSetCount = 0;
            _texCoordSets = null;
            _texCoordIndexSets = null;
            _texCoordSetMap = null;

            _coordOnly = false;

        } // End of reset(int)


        // This method takes an indexed quad array and expands it to
        // a list of indexed triangles.  It is used for the Coordinate
        // indices as well as the color and texture indices.
        private int[] expandQuad (int[] indices) {
            int[] triangles = new int[indices.Length / 4 * 6];

            for (int i = 0; i < indices.Length / 4; i++) {
                triangles[i * 6 + 0] = indices[i * 4];
                triangles[i * 6 + 1] = indices[i * 4 + 1];
                triangles[i * 6 + 2] = indices[i * 4 + 2];
                triangles[i * 6 + 3] = indices[i * 4];
                triangles[i * 6 + 4] = indices[i * 4 + 2];
                triangles[i * 6 + 5] = indices[i * 4 + 3];
            }

            return triangles;
        } // End of expandQuad



        // This method takes an indexed triangle fan and expands it to
        // a list of indexed triangles.  It is used for the Coordinate
        // indices as well as the color and texture indices.
        private int[] expandTriFan (int numTris, int[] indices) {
            int[] triangles = new int[numTris * 3];
            int p = 0;
            int baseIdx = 0;
            for (int f = 0; f < _stripCounts.Length; f++) {
                for (int t = 0; t < _stripCounts[f] - 2; t++) {
                    triangles[p++] = indices[baseIdx];
                    triangles[p++] = indices[baseIdx + t + 1];
                    triangles[p++] = indices[baseIdx + t + 2];
                }
                baseIdx += _stripCounts[f];
            }
            return triangles;
        } // End of expandTriFan



        // This method takes an indexed triangle strip and expands it to
        // a list of indexed triangles.  It is used for the Coordinate
        // indices as well as the color and texture indices.
        private int[] expandTriStrip (int numTris, int[] indices) {
            int[] triangles = new int[numTris * 3];

            int p = 0;
            int baseIdx = 0;
            for (int s = 0; s < _stripCounts.Length; s++) {
                for (int t = 0; t < _stripCounts[s] - 2; t++) {

                    // Use a ping-ponging algorithm to reverse order on every other
                    // triangle to preserve winding
                    if (t % 2 == 0) {
                        triangles[p++] = indices[baseIdx + t + 0];
                        triangles[p++] = indices[baseIdx + t + 1];
                        triangles[p++] = indices[baseIdx + t + 2];
                    } else {
                        triangles[p++] = indices[baseIdx + t + 0];
                        triangles[p++] = indices[baseIdx + t + 2];
                        triangles[p++] = indices[baseIdx + t + 1];
                    }
                }
                baseIdx += _stripCounts[s];
            }

            return triangles;
        } // End of expandTriStrip



        // Used by the NormalGenerator utility.  Informs the GeometryInfo object
        // to remember its current primitive and stripCounts arrays so that
        // they can be used to convert the object back to its original 
        // primitive
        void rememberOldPrim () {
            _oldPrim = _prim;
            _oldStripCounts = _stripCounts;
        } // End of rememberOldPrim



        // The NormalGenerator needs to know the original primitive for
        // facet normal generation for quads
        int getOldPrim () {
            return _oldPrim;
        } // End of getOldPrim



        // Used by the Utility libraries other than the NormalGenerator.
        // Informs the GeometryInfo object that the geometry need not
        // be converted back to the original primitive before returning.
        // For example, if a list of Fans is sent, converted to Triangles
        // for normal generation, and then stripified by the Stripifyer,
        // we want to make sure that GeometryInfo doesn't convert the
        // geometry *back* to fans before creating the output GeometryArray.
        public void ForgetOldPrim () {
            _oldPrim = 0;
            _oldStripCounts = null;
        } // End of forgetOldPrim


        /**
         * Convert the GeometryInfo object to have primitive type TRIANGLE_ARRAY
         * and be indexed.
         * @throws IllegalArgumentException if coordinate data is missing,
         * if the index lists aren't all the
         * same length, if an index list is set and the corresponding data
         * list isn't set, if a data list is set and the corresponding
         * index list is unset (unless all index lists are unset or in
         * USE_COORD_INDEX_ONLY format),
         * if StripCounts or ContourCounts is inconsistent with the current
         * primitive, if the sum of the contourCounts array doesn't equal
         * the length of the StripCounts array, or if the number of vertices
         * isn't a multiple of three (for triangles) or four (for quads).
         */
        public void ConvertToIndexedTriangles () {
            int triangles = 0;

            // This calls checkForBadData
            Indexify();

            if (_prim == TRIANGLE_ARRAY) return;

            switch (_prim) {

                case QUAD_ARRAY:

                    _coordinateIndices = expandQuad(_coordinateIndices);
                    if (_colorIndices != null) _colorIndices = expandQuad(_colorIndices);
                    if (_normalIndices != null)
                        _normalIndices = expandQuad(_normalIndices);
                    for (int i = 0; i < _texCoordSetCount; i++)
                        _texCoordIndexSets[i] = expandQuad(_texCoordIndexSets[i]);
                    break;

                case TRIANGLE_FAN_ARRAY:
                    // Count how many triangles are in the object
                    for (int i = 0; i < _stripCounts.Length; i++) {
                        triangles += _stripCounts[i] - 2;
                    }

                    _coordinateIndices = expandTriFan(triangles, _coordinateIndices);
                    if (_colorIndices != null)
                        _colorIndices = expandTriFan(triangles, _colorIndices);
                    if (_normalIndices != null)
                        _normalIndices = expandTriFan(triangles, _normalIndices);
                    for (int i = 0; i < _texCoordSetCount; i++)
                        _texCoordIndexSets[i] = expandTriFan(triangles,
                                _texCoordIndexSets[i]);
                    break;

                case TRIANGLE_STRIP_ARRAY:
                    // Count how many triangles are in the object
                    for (int i = 0; i < _stripCounts.Length; i++) {
                        triangles += _stripCounts[i] - 2;
                    }

                    _coordinateIndices = expandTriStrip(triangles, _coordinateIndices);
                    if (_colorIndices != null)
                        _colorIndices = expandTriStrip(triangles, _colorIndices);
                    if (_normalIndices != null)
                        _normalIndices = expandTriStrip(triangles, _normalIndices);
                    for (int i = 0; i < _texCoordSetCount; i++)
                        _texCoordIndexSets[i] = expandTriStrip(triangles,
                                _texCoordIndexSets[i]);
                    break;

                case POLYGON_ARRAY:
                    if (_tr == null) _tr = new Triangulator();
                    _tr.Triangulate(this);
                    break;
            }

            _prim = TRIANGLE_ARRAY;
            _stripCounts = null;
        } // End of convertToIndexedTriangles



        /**
         * Get the current primitive.  Some of the utilities may change the
         * primitive type of the data stored in the GeometryInfo object
         * (for example, the stripifyer will change it to TRIANGLE_STRIP_ARRAY).
         */
        public int GetPrimitive () {
            return _prim;
        } // End of getPrimitive()



        /**
         * Set the current primitive.  Some of the utilities may change the
         * primitive type of the data stored in the GeometryInfo object
         * (for example, the stripifyer will change it to TRIANGLE_STRIP_ARRAY).
         * But the user can't change the primitive type - it is set in the 
         * constructor.  Therefore, this method has package scope.
         */
        public void SetPrimitive (int primitive) {
            if ((_prim >= TRIANGLE_ARRAY) && (_prim <= POLYGON_ARRAY)) {
                _prim = primitive;
            } else {
                throw new ArgumentOutOfRangeException("primitive", LS.T("Illegal primitive."));
            }
        } // End of setPrimitive()



        /**
         * Sets the coordinates array.  
         * No data copying is done because a reference to user data is used.
         */
        public void SetCoordinates (Point3F[] coordinates) {
            _coordinates = coordinates;
        } // End of setCoordinates



        /**
         * Sets the coordinates array.
         * The points are copied into the GeometryInfo object.
         */
        public void SetCoordinates (Point3D[] coordinates) {
            if (coordinates == null) _coordinates = null;
            else {
                _coordinates = new Point3F[coordinates.Length];
                for (int i = 0; i < coordinates.Length; i++) {
                    _coordinates[i] = new Point3F(
                        (float)(coordinates[i].X),
                        (float)(coordinates[i].Y),
                        (float)(coordinates[i].Z));
                }
            }
        } // End of setCoordinates



        /**
         * Sets the coordinates array.
         * The points are copied into the GeometryInfo object.
         */
        public void SetCoordinates (float[] coordinates) {
            if (coordinates == null) this._coordinates = null;
            else {
                _coordinates = new Point3F[coordinates.Length / 3];
                for (int i = 0; i < _coordinates.Length; i++) {
                    this._coordinates[i] = new Point3F(coordinates[i * 3],
                                      coordinates[i * 3 + 1],
                                      coordinates[i * 3 + 2]);
                }
            }
        } // End of setCoordinates



        /**
         * Sets the coordinates array.
         * The points are copied into the GeometryInfo object.
         */
        public void SetCoordinates (double[] coordinates) {
            if (coordinates == null) _coordinates = null;
            else {
                _coordinates = new Point3F[coordinates.Length / 3];
                for (int i = 0; i < coordinates.Length / 3; i++) {
                    _coordinates[i] = new Point3F((float)coordinates[i * 3],
                                      (float)coordinates[i * 3 + 1],
                                      (float)coordinates[i * 3 + 2]);
                }
            }
        } // End of setCoordinates



        /**
         * Retrieves a reference to the coordinate array.
         */
        public Point3F[] GetCoordinates () {
            return _coordinates;
        } // End of getCoordinates



        /**
         * Sets the colors array.
         * No data copying is done because a reference to
         * user data is used.
         */
        public void SetColors (Color3F[] colors) {
            _colors3 = colors;
            _colors4 = null;
        } // End of setColors



        /**
         * Sets the colors array.
         * No data copying is done because a reference to
         * user data is used.
         */
        public void SetColors (Color4F[] colors) {
            _colors3 = null;
            _colors4 = colors;
        } // End of setColors



        /**
         * Sets the colors array.
         * The points are copied into the GeometryInfo object.
         */
        public void SetColors (Color3B[] colors) {
            if (colors == null) {
                _colors3 = null;
                _colors4 = null;
            } else {
                _colors3 = new Color3F[colors.Length];
                _colors4 = null;
                for (int i = 0; i < colors.Length; i++) {
                    _colors3[i] = new Color3F((float)(colors[i].x & 0xff) / 255.0f,
                                     (float)(colors[i].y & 0xff) / 255.0f,
                                     (float)(colors[i].z & 0xff) / 255.0f);
                }
            }
        } // End of setColors



        /**
         * Sets the colors array.
         * The points are copied into the GeometryInfo object.
         */
        public void SetColors (Color4B[] colors) {
            if (colors == null) {
                _colors3 = null;
                _colors4 = null;
            } else {
                _colors3 = null;
                _colors4 = new Color4F[colors.Length];
                for (int i = 0; i < colors.Length; i++) {
                    _colors4[i] = new Color4F((float)(colors[i].x & 0xff) / 255.0f,
                                     (float)(colors[i].y & 0xff) / 255.0f,
                                     (float)(colors[i].z & 0xff) / 255.0f,
                                     (float)(colors[i].w & 0xff) / 255.0f);
                }
            }
        } // End of setColors



        /**
         * Sets the colors array.
         * The points are copied into the GeometryInfo object, assuming
         * 3 components (R, G, and B) per vertex.
         */
        public void SetColors3 (float[] colors) {
            if (colors == null) {
                _colors3 = null;
                _colors4 = null;
            } else {
                _colors3 = new Color3F[colors.Length / 3];
                _colors4 = null;
                for (int i = 0; i < colors.Length / 3; i++) {
                    _colors3[i] = new Color3F(colors[i * 3],
                                 colors[i * 3 + 1],
                                 colors[i * 3 + 2]);
                }
            }
        } // End of setColors3



        /**
         * Sets the colors array.
         * The points are copied into the GeometryInfo object, assuming
         * 4 components (R, G, B, and A) per vertex.
         */
        public void SetColors4 (float[] colors) {
            if (colors == null) {
                _colors3 = null;
                _colors4 = null;
            } else {
                _colors3 = null;
                _colors4 = new Color4F[colors.Length / 4];
                for (int i = 0; i < colors.Length / 4; i++) {
                    _colors4[i] = new Color4F(colors[i * 4],
                                     colors[i * 4 + 1],
                                     colors[i * 4 + 2],
                                     colors[i * 4 + 3]);
                }
            }
        } // End of setColors4



        /**
         * Sets the colors array.
         * The points are copied into the GeometryInfo object, assuming
         * 3 components (R, G, and B) per vertex.
         */
        public void SetColors3 (byte[] colors) {
            if (colors == null) {
                _colors3 = null;
                _colors4 = null;
            } else {
                _colors3 = new Color3F[colors.Length / 3];
                _colors4 = null;
                for (int i = 0; i < colors.Length / 3; i++) {
                    _colors3[i] =
                    new Color3F((float)(colors[i * 3] & 0xff) / 255.0f,
                                (float)(colors[i * 3 + 1] & 0xff) / 255.0f,
                            (float)(colors[i * 3 + 2] & 0xff) / 255.0f);
                }
            }
        } // End of setColors3



        /**
         * Sets the colors array.
         * The points are copied into the GeometryInfo object, assuming
         * 4 components (R, G, B, and A) per vertex.
         */
        public void SetColors4 (byte[] colors) {
            if (colors == null) {
                _colors3 = null;
                _colors4 = null;
            } else {
                _colors3 = null;
                _colors4 = new Color4F[colors.Length / 4];
                for (int i = 0; i < colors.Length / 4; i++) {
                    _colors4[i] =
                    new Color4F((float)(colors[i * 4] & 0xff) / 255.0f,
                            (float)(colors[i * 4 + 1] & 0xff) / 255.0f,
                            (float)(colors[i * 4 + 2] & 0xff) / 255.0f,
                            (float)(colors[i * 4 + 3] & 0xff) / 255.0f);
                }
            }
        } // End of setColors4



        /**
         * Retrieves a reference to the colors array.  Will be either
         * <code>Color3f[]</code> or <code>Color4f[]</code> depending on
         * the type of the input data.  Call
         * getNumColorComponents() to find out which version is returned.
         */
        public Object[] GetColors () {
            if (_colors3 != null) return _colors3;
            else return _colors4;
        } // End of getColors



        /**
         * Returns the number of color data components stored per vertex
         * in the current GeometryInfo object (3 for RGB or 4 for RGBA).
         * If no colors are currently defined, 0 is returned.
         */
        public int GetNumColorComponents () {
            if (_colors3 != null) return 3;
            else if (_colors4 != null) return 4;
            else return 0;
        } // End of getNumColorComponents



        /**
         * Sets the normals array.
         * No data copying is done because a reference to
         * user data is used.
         */
        public void SetNormals (Vector3F[] normals) {
            this._normals = normals;
        } // End of setNormals



        /**
         * Sets the normals array.
         * The points are copied into the GeometryInfo object.
         */
        public void SetNormals (float[] normals) {
            if (normals == null) this._normals = null;
            else {
                this._normals = new Vector3F[normals.Length / 3];
                for (int i = 0; i < this._normals.Length; i++) {
                    this._normals[i] = new Vector3F(normals[i * 3],
                                       normals[i * 3 + 1],
                                       normals[i * 3 + 2]);
                }
            }
        } // End of setNormals(float[])



        /**
         * Retrieves a reference to the normal array.
         */
        public Vector3F[] GetNormals () {
            return _normals;
        } // End of getNormals



        /**
         * This method is used to specify the number of texture coordinate sets  
         * and the dimensionality of the texture coordinates.
         * The number of texture coordinate sets must be specified to the GeometryInfo
         * class before any of the sets are specified. The dimensionality of the 
         * texture coordinates may be 2, 3, or 4, corresponding to 2D, 3D, or 4D 
         * texture coordinates respectively.(All sets must have the same 
         * dimensionality.) The default is zero, 2D texture coordinate sets. 
         * This method should be called before any texture coordinate sets are 
         * specified because <b>calling this method will delete all previously
         * specified texture coordinate and texture coordinate index arrays</b> 
         * associated with this GeometryInfo.  For example: 
         * <blockquote><pre>
         *	geomInfo.setTextureCoordinateParams(2, 3);
         *	geomInfo.setTextureCoordinates(0, tex0);
         *	geomInfo.setTextureCoordinates(1, tex1);
         *	geomInfo.setTextureCoordinateParams(1, 2);
         *	geomInfo.getTexCoordSetCount();
         * </blockquote></pre>
         * The second call to <code>setTextureCoordinateParams</code> will erase all 
         * the texture coordinate arrays, so the subsequent call to <code>
         * getTexCoordSetCount</code> will return 1.
         * @param numSets The number of texture coordinate sets that will be 
         * specified for this GeometryInfo object.
         * @param dim The dimensionality of the texture coordinates. Has to be 2, 3 
         * or 4.
         * @throws IllegalArgumentException if the dimensionality of the texture 
         * coordinates is not one of 2, 3 or 4.
         */
        public void SetTextureCoordinateParams (int numSets, int dim) {
            if (dim == 2) {
                _texCoordSets = new TexCoord2F[numSets][];
            } else if (dim == 3) {
                _texCoordSets = new TexCoord3F[numSets][];
            } else if (dim == 4) {
                _texCoordSets = new TexCoord4F[numSets][];
            } else {
                throw new ArgumentOutOfRangeException("dim");
            }

            _texCoordIndexSets = new int[numSets][];
            _texCoordDim = dim;
            _texCoordSetCount = numSets;
        } // End of setTextureCoordinateParams



        /**
         * Returns the number of texture coordinate sets in this GeometryInfo.
         * This value is set with setTextureCoordinateParams().
         * If setTextureCoordinateParams()
         * has not been called, 0 is returned unless one of the deprecated
         * texture coordinate methods has been called.  Calling one of the
         * deprecated texture coordinate methods sets the count to 1.
         * The deprecated texture coordinate methods are those that don't
         * take texCoordSet as the first parameter.
         * @return the number of texture coordinate sets in this
         * GeometryInfo.
         */
        public int GetTexCoordSetCount () {
            return _texCoordSetCount;
        }



        /**
         * Returns the number of texture coordinate components that are stored
         * per vertex.  Returns 2 for ST (2D), 3 for STR (3D),
         * or 4 for STRQ (4D), aslo known as the "dimensionality" of the
         * coordinates.  This value is set with 
         * setTextureCoordinateParams().  If setTextureCoordinateParams()
         * has not been called, 0 is returned unless one of the deprecated
         * texture coordinate methods has been called.  Calling one of the
         * deprecated texture coordinate methods sets the dimensionality
         * explicitly (if you called setTextureCoordinates(Point2F[]) then
         * 2 is returned).
         * The deprecated texture coordinate methods are those that don't
         * take texCoordSet as the first parameter.
         */
        public int GetNumTexCoordComponents () {
            return _texCoordDim;
        } // End of getNumTexCoordComponents



        /**
         * Sets the mapping between texture coordinate sets and texture units.
         * See the 
         * <a href="../../../../../javax/media/j3d/GeometryArray.html#texCoordSetMap">
         * GeometryArray constructor </a> for further details.
         * <p> <b>Note:</b> If the texCoordSetMap is not set, multi-texturing is 
         * turned off. Only the texture coordinate set at index 0 (if set) will be 
         * used. Any other sets specified by the GeometryInfo.setTextureCoordinate* 
         * methods will be ignored.
         */
        public void SetTexCoordSetMap (int[] map) {
            _texCoordSetMap = map;
        }



        /**
         * Returns a reference to the texture coordinate set map.
         * See the 
         * <a href="../../../../../javax/media/j3d/GeometryArray.html#texCoordSetMap">
         * GeometryArray constructor </a> for further details.
         */
        public int[] GetTexCoordSetMap () {
            return _texCoordSetMap;
        }



        /**
         * Sets the 2D texture coordinates for the specified set.
         * No data copying is done - a reference to user data is used. 
         * @param texCoordSet The texture coordinate set for which these 
         * coordinates are being specified.
         * @param texCoords Array of 2D texture coordinates.
         * @throws IllegalArgumentException if <code>texCoordSet </code> < 0 or
         * <code>texCoordSet >= texCoordSetCount</code>,
         * or the texture coordinate parameters were not previously set by
         * calling <code>setTextureCoordinateParams(texCoordSetCount, 2)</code>.
         */
        public void SetTextureCoordinates (int texCoordSet, TexCoord2F[] texCoords) {
            if (_texCoordDim != 2)
                throw new ArgumentOutOfRangeException("texCoords");

            if ((texCoordSet >= _texCoordSetCount) || (texCoordSet < 0))
                throw new ArgumentOutOfRangeException("texCoordSet");

            _texCoordSets[texCoordSet] = texCoords;
        } // End of setTextureCoordinates(int, TexCoord3F[])



        /**
         * Sets the TextureCoordinates array by copying the data
         * into the GeometryInfo object.
         * This method sets the number of texture coordinate sets to 1,
         * sets the dimensionality of the texture coordinates to 2,
         * and sets the coordinates for texture coordinate set 0.
         * @deprecated As of Java 3D 1.3 replaced by 
         * <code>setTextureCoordinates(int texCoordSet, TexCoord2F coords[])</code>
         */
        public void SetTextureCoordinates (Point2F[] texCoords) {
            _texCoordSetCount = 1;
            _texCoordDim = 2;
            _texCoordSets = new TexCoord2F[1][];
            if (texCoords != null) {
                TexCoord2F[] tex = new TexCoord2F[texCoords.Length];
                for (int i = 0; i < texCoords.Length; i++)
                    tex[i] = new TexCoord2F(texCoords[i]);
                _texCoordSets[0] = tex;
            }
        } // End of setTextureCoordinates(Point2F[])



        /**
         * Sets the texture coordinates array for the specified set.
         * No data copying is done - a reference to user data is used. 
         * @param texCoordSet The texture coordinate set for which these coordinates 
         * are being specified.
         * @param texCoords Array of 3D texture coordinates.
         * @throws IllegalArgumentException if <code> texCoordSet </code> < 0 or
         * <code>texCoordSet >= texCoordSetCount</code>,
         * or the texture coordinate parameters were not previously set by
         * calling <code>setTextureCoordinateParams(texCoordSetCount, 3)</code>.
         */
        public void SetTextureCoordinates (int texCoordSet, TexCoord3F[] texCoords) {
            if (_texCoordDim != 3)
                throw new ArgumentOutOfRangeException("texCoords");

            if ((texCoordSet >= _texCoordSetCount) || (texCoordSet < 0))
                throw new ArgumentOutOfRangeException("texCoordSet");

            _texCoordSets[texCoordSet] = texCoords;
        } // End of setTextureCoordinates(int, TexCoord3F[])



        /**
         * Sets the TextureCoordinates array by copying the data
         * into the GeometryInfo object.
         * This method sets the number of texture coordinate sets to 1,
         * sets the dimensionality of the texture coordinates to 3,
         * and sets the coordinates for texture coordinate set 0.
         * @deprecated As of Java 3D 1.3 replaced by 
         * <code>setTextureCoordinates(int texCoordSet, TexCoord3F coords[])</code>
         */
        public void SetTextureCoordinates (Point3F[] texCoords) {
            _texCoordSetCount = 1;
            _texCoordDim = 3;
            _texCoordSets = new TexCoord3F[1][];
            if (texCoords != null) {
                TexCoord3F[] tex = new TexCoord3F[texCoords.Length];
                for (int i = 0; i < texCoords.Length; i++)
                    tex[i] = new TexCoord3F(texCoords[i]);
                _texCoordSets[0] = tex;
            }
        } // End of setTextureCoordinates(Point3F[])



        /**
         * Sets the texture coordinates array for the specified set.
         * No data copying is done - a reference to user data is used. 
         * @param texCoordSet The texture coordinate set for which these coordinates 
         * are being specified.
         * @param texCoords Array of 4D texture coordinates.
         * @throws IllegalArgumentException if <code> texCoordSet </code> < 0 or
         * <code>texCoordSet >= texCoordSetCount</code>,
         * or the texture coordinate parameters were not previously set by
         * calling <code>setTextureCoordinateParams(texCoordSetCount, 4)</code>.
         */
        public void SetTextureCoordinates (int texCoordSet, TexCoord4F[] texCoords) {
            if (_texCoordDim != 4)
                throw new ArgumentOutOfRangeException("texCoords");

            if ((texCoordSet >= _texCoordSetCount) || (texCoordSet < 0))
                throw new ArgumentOutOfRangeException("texCoordSet");

            _texCoordSets[texCoordSet] = texCoords;
        } // End of setTextureCoordinates(int, TexCoord4f[])



        /**
         * Sets the texture coordinates array by copying the data into the
         * GeometryInfo object.  The number of sets and dimensionality of
         * the sets must have been set previously with 
         * setTextureCoordinateParams(texCoordSetCount, dim).
         * @param texCoordSet The texture coordinate set for which these coordinates 
         * are being specified.
         * @param texCoords The float array of texture coordinates. For n texture 
         * coordinates with dimensionality d, there must be d*n floats in the array.
         * @throws IllegalArgumentException if <code>texCoordSet </code> < 0 or
         * <code>texCoordSet >= texCoordSetCount</code>,
         * or the texture coordinate parameters were not previously set by
         * calling <code>setTextureCoordinateParams</code>.
         */
        public void SetTextureCoordinates (int texCoordSet, float[] texCoords) {
            if ((texCoords.Length % _texCoordDim) != 0)
                throw new ArgumentOutOfRangeException("texCoords");

            // Copy the texCoords into this GeometryInfo object
            if (_texCoordDim == 2) {
                TexCoord2F[] tcoords = new TexCoord2F[texCoords.Length / 2];
                for (int i = 0; i < tcoords.Length; i++)
                    tcoords[i] = new TexCoord2F(texCoords[i * 2],
                                texCoords[i * 2 + 1]);
                SetTextureCoordinates(texCoordSet, tcoords);
            } else if (_texCoordDim == 3) {
                TexCoord3F[] tcoords = new TexCoord3F[texCoords.Length / 3];
                for (int i = 0; i < tcoords.Length; i++)
                    tcoords[i] = new TexCoord3F(texCoords[i * 3],
                                texCoords[i * 3 + 1],
                                texCoords[i * 3 + 2]);
                SetTextureCoordinates(texCoordSet, tcoords);
            } else if (_texCoordDim == 4) {
                TexCoord4F[] tcoords = new TexCoord4F[texCoords.Length / 4];
                for (int i = 0; i < tcoords.Length; i++)
                    tcoords[i] = new TexCoord4F(texCoords[i * 4],
                                texCoords[i * 4 + 1],
                                texCoords[i * 4 + 2],
                                texCoords[i * 4 + 3]);
                SetTextureCoordinates(texCoordSet, tcoords);
            } else {
                throw new ArgumentOutOfRangeException("texCoords");
            }
        } // End of setTextureCoordinates(int, float[])



        /**
         * Sets the texture coordinates array by copying the data
         * into the GeometryInfo object, assuming two numbers
         * (S and T) per vertex.
         * This method sets the number of texture coordinate sets to 1,
         * sets the dimensionality of the texture coordinates to 2,
         * and sets the coordinates for texture coordinate set 0.
         * @deprecated As of Java 3D 1.3 replaced by 
         * <code>setTextureCoordinates(int texCoordSet, float texCoords[])</code>
         */
        public void SetTextureCoordinates2 (float[] texCoords) {
            _texCoordSetCount = 1;
            _texCoordDim = 2;
            _texCoordSets = new TexCoord2F[1][];
            SetTextureCoordinates(0, texCoords);
        } // End of setTextureCoordinates2(float[])



        /**
         * Sets the TextureCoordinates array by copying the data
         * into the GeometryInfo object, assuming three numbers
         * (S, T, &amp; R) per vertex.
         * This method sets the number of texture coordinate sets to 1,
         * sets the dimensionality of the texture coordinates to 3,
         * and sets the coordinates for texture coordinate set 0.
         * @deprecated As of Java 3D 1.3 replaced by 
         * <code>setTextureCoordinates(int texCoordSet, float texCoords[])</code>
         */
        public void SetTextureCoordinates3 (float[] texCoords) {
            _texCoordSetCount = 1;
            _texCoordDim = 3;
            _texCoordSets = new TexCoord3F[1][];
            SetTextureCoordinates(0, texCoords);
        } // End of setTextureCoordinates3(float[])



        /**
         * Returns a reference to the indicated texture coordinate array.
         * The return type will be <code>TexCoord2F[]</code>, <code>TexCoord3F[]
         * </code>, or <code>TexCoord4f[]</code> depending on the 
         * current dimensionality of the texture coordinates in the GeometryInfo
         * object.  Use <code>getNumTexCoordComponents()</code> to find out which 
         * version is returned.
         * @param texCoordSet The index of the texture coordinate set to
         * retrieve.
         * @return An array of texture coordinates at the specified index
         * @throws IllegalArgumentException If <code> texCoordSet</code> < 0
         * or <code>texCoordSet >= texCoordSetCount</code>
         */
        public Object[] GetTextureCoordinates (int texCoordSet) {
            if ((texCoordSet >= _texCoordSetCount) || (texCoordSet < 0))
                throw new ArgumentOutOfRangeException("texCoordSet");
            return (object[])_texCoordSets[texCoordSet];
        } // End of getTextureCoordinates(int)



        /**
         * Retrieves a reference to texture coordinate set 0.
         * The return type will be <code>TexCoord2F[]</code>, <code>TexCoord3F[]
         * </code>, or <code>TexCoord4f[]</code> depending on the 
         * current dimensionality of the texture coordinates in the GeometryInfo
         * object.  Use <code>getNumTexCoordComponents()</code> to find out which 
         * version is returned.  Equivalent to <code>getTextureCoordinates(0)</code>.
         * @return An array of texture coordinates for set 0.
         * @deprecated As of Java 3D 1.3 replaced by 
         * <code>getTextureCoordinates(int texCoordSet)</code>
         */
        public Object[] GetTextureCoordinates () {
            return (object[])_texCoordSets[0];
        } // End of getTextureCoordinates()



        /**
         * Sets the array of indices into the Coordinate array.
         * No data copying is done - a reference to user data is used. 
         */
        public void SetCoordinateIndices (int[] coordinateIndices) {
            _coordinateIndices = coordinateIndices;
        } // End of setCoordinateIndices



        /**
         * Retrieves a reference to the array of indices into the
         * coordinate array.</p>
         *
         * This method should be considered for advanced users only.
         * Novice users should just use getGeometryArray() to retrieve
         * their data so that the internal format of GeometryInfo is
         * of no concern.</p>
         *
         * Depending on which of the utility routines you've called
         * on your GeometryInfo object, the results may not be what you
         * expect.  If you've called the Stripifier, your GeometryInfo
         * object's Primitive has been changed to indexed TRIANGLE_STRIP_ARRAY
         * and your data will be formatted accordingly.  Similarly, if
         * you've called the Triangulator, your data is in indexed
         * TRIANGLE_ARRAY format.  Generating normals with the NormalGenerator
         * utility will convert your data to indexed TRIANGLE_ARRAY also,
         * but if you call getGeometryArray without calling the Stripifier or
         * Triangulator, your data will be converted back to the original
         * primitive type when creating the GeometryArray object to pass
         * back.  However, if your creaseAngle was not Math.PI (no creases -
         * smooth shading), then the introduction of
         * creases into your model may have split primitives, lengthening 
         * the StripCounts and index arrays from your original data.
         */
        public int[] GetCoordinateIndices () {
            return _coordinateIndices;
        } // End of getCoordinateIndices



        /**
         * Sets the array of indices into the Color array.
         * No data copying is done - a reference to user data is used. 
         */
        public void SetColorIndices (int[] colorIndices) {
            _colorIndices = colorIndices;
        } // End of setColorIndices



        /**
         * Retrieves a reference to the array of indices into the
         * color array.</p>
         *
         * This method should be considered for advanced users only.
         * Novice users should just use getGeometryArray() to retrieve
         * their data so that the internal format of GeometryInfo is
         * of no concern.</p>
         *
         * Depending on which of the utility routines you've called
         * on your GeometryInfo object, the results may not be what you
         * expect.  If you've called the Stripifier, your GeometryInfo
         * object's Primitive has been changed to indexed TRIANGLE_STRIP_ARRAY
         * and your data will be formatted accordingly.  Similarly, if
         * you've called the Triangulator, your data is in indexed
         * TRIANGLE_ARRAY format.  Generating normals with the NormalGenerator
         * utility will convert your data to indexed TRIANGLE_ARRAY also,
         * but if you call getGeometryArray without calling the Stripifier or
         * Triangulator, your data will be converted back to the original
         * primitive type when creating the GeometryArray object to pass
         * back.  However, if your creaseAngle was not Math.PI (no creases -
         * smooth shading), then the introduction of
         * creases into your model may have split primitives, lengthening
         * the StripCounts and index arrays from your original data.
         */
        public int[] GetColorIndices () {
            return _colorIndices;
        } // End of getColorIndices



        /**
         * Sets the array of indices into the Normal array.
         * No data copying is done - a reference to user data is used. 
         */
        public void SetNormalIndices (int[] normalIndices) {
            _normalIndices = normalIndices;

        } // End of setNormalIndices



        /**
         * Retrieves a reference to the array of indices into the
         * Normal array.</p>
         *
         * This method should be considered for advanced users only.
         * Novice users should just use getGeometryArray() to retrieve
         * their data so that the internal format of GeometryInfo is
         * of no concern.</p>
         *
         * Depending on which of the utility routines you've called
         * on your GeometryInfo object, the results may not be what you
         * expect.  If you've called the Stripifier, your GeometryInfo
         * object's Primitive has been changed to indexed TRIANGLE_STRIP_ARRAY
         * and your data will be formatted accordingly.  Similarly, if
         * you've called the Triangulator, your data is in indexed
         * TRIANGLE_ARRAY format.  Generating normals with the NormalGenerator
         * utility will convert your data to indexed TRIANGLE_ARRAY also,
         * but if you call getGeometryArray without calling the Stripifier or
         * Triangulator, your data will be converted back to the original
         * primitive type when creating the GeometryArray object to pass
         * back.  However, if your creaseAngle was not Math.PI (no creases -
         * smooth shading), then the introduction of
         * creases into your model may have split primitives, lengthening
         * the StripCounts and index arrays from your original data.
         */
        public int[] GetNormalIndices () {
            return _normalIndices;
        } // End of getNormalIndices



        /**
         * Sets one of the texture coordinate index arrays.
         * No data copying is done - a reference to user data is used. 
         * @param texCoordSet The texture coordinate set for which these coordinate 
         * indices are being specified.
         * @param texIndices The integer array of indices into the specified texture 
         * coordinate set
         * @throws IllegalArgumentException If <code> texCoordSet</code> < 0 or
         * <code>texCoordSet >= texCoordSetCount</code>.
         */
        public void SetTextureCoordinateIndices (int texCoordSet, int[] texIndices) {
            if ((texCoordSet >= _texCoordSetCount) || (texCoordSet < 0))
                throw new ArgumentOutOfRangeException("texCoordSet");

            // Texture coordinates are indexed 
            _texCoordIndexSets[texCoordSet] = texIndices;
        } // End of setTextureCoordinateIndices(int, int[])



        /**
         * Sets the array of indices into texture coordinate set 0.  Do not
         * call this method if you are using more than one set of texture
         * coordinates.
         * No data is copied - a reference to the user data is used.
         * @deprecated As of Java 3D 1.3 replaced by 
         * <code>setTextureCoordinateIndices(int texCoordSet, int indices[])</code>
         * @throws IllegalArgumentException If <code>texCoordSetCount > 1</code>.
         */
        public void SetTextureCoordinateIndices (int[] texIndices) {
            if (_texCoordSetCount > 1)
                throw new InvalidOperationException();

            _texCoordIndexSets = new int[1][];
            _texCoordIndexSets[0] = texIndices;
        } // End of setTextureCoordinateIndices(int[])



        /**
         * Retrieves a reference to the specified array of texture
         * coordinate indices.<p>
         *
         * This method should be considered for advanced users only.
         * Novice users should just use getGeometryArray() to retrieve
         * their data so that the internal format of GeometryInfo is
         * of no concern.</p>
         *
         * Depending on which of the utility routines you've called
         * on your GeometryInfo object, the results may not be what you
         * expect.  If you've called the Stripifier, your GeometryInfo
         * object's Primitive has been changed to indexed TRIANGLE_STRIP_ARRAY
         * and your data will be formatted accordingly.  Similarly, if
         * you've called the Triangulator, your data is in indexed
         * TRIANGLE_ARRAY format.  Generating normals with the NormalGenerator
         * utility will convert your data to indexed TRIANGLE_ARRAY also,
         * but if you call getGeometryArray without calling the Stripifier or
         * Triangulator, your data will be converted back to the original
         * primitive type when creating the GeometryArray object to pass
         * back.  However, if your creaseAngle was not Math.PI (no creases -
         * smooth shading), then the introduction of
         * creases into your model may have split primitives, lengthening
         * the StripCounts and index arrays from your original data.
         * @param texCoordSet The texture coordinate index set to be
         * retrieved.
         * @return Integer array of the texture coordinate indices for the specified
         * set.
         */
        public int[] GetTextureCoordinateIndices (int texCoordSet) {
            return _texCoordIndexSets[texCoordSet];
        }


        /**
         * Returns a reference to texture coordinate index set 0.
         * Equivalent to
         * <code>getTextureCoordinateIndices(0)</code>.
         * @deprecated As of Java 3D 1.3 replaced by 
         * <code>int[] getTextureCoordinateIndices(int texCoordSet) </code>
         * @return Integer array of the texture coordinate indices for set 0
         */
        public int[] GetTextureCoordinateIndices () {
            if (_texCoordIndexSets == null) return null;
            return _texCoordIndexSets[0];
        } // End of getTextureCoordinateIndices()



        /**
         * Sets the array of strip counts.  If index lists have been set for
         * this GeomteryInfo object then the data is indexed and the stripCounts
         * are like stripIndexCounts.  If no index lists have been set then
         * the data is non-indexed and the stripCounts are like 
         * stripVertexCounts.
         * @see GeometryStripArray#GeometryStripArray(int, int,
         * int[] stripVertexCounts)
         * @see IndexedGeometryStripArray#IndexedGeometryStripArray(int, int, int,
         * int[] stripIndexCounts)
         */
        public void SetStripCounts (int[] stripCounts) {
            _stripCounts = stripCounts;
        } // End of setStripCounts



        /**
         * Retrieves a reference to the array of stripCounts.</p>
         *
         * This method should be considered for advanced users only.
         * Novice users should just use getGeometryArray() to retrieve
         * their data so that the internal format of GeometryInfo is
         * of no concern.</p>
         *
         * Depending on which of the utility routines you've called
         * on your GeometryInfo object, the results may not be what you
         * expect.  If you've called the Stripifier, your GeometryInfo
         * object's Primitive has been changed to indexed TRIANGLE_STRIP_ARRAY
         * and your data will be formatted accordingly.  Similarly, if
         * you've called the Triangulator, your data is in indexed
         * TRIANGLE_ARRAY format.  Generating normals with the NormalGenerator
         * utility will convert your data to indexed TRIANGLE_ARRAY also,
         * but if you call getGeometryArray without calling the Stripifier or
         * Triangulator, your data will be converted back to the original
         * primitive type when creating the GeometryArray object to pass
         * back.  However, if your creaseAngle was not Math.PI (no creases -
         * smooth shading), then the introduction of
         * creases into your model may have split primitives, lengthening
         * the StripCounts and index arrays from your original data.
         */
        public int[] GetStripCounts () {
            return _stripCounts;
        } // End of getStripCounts



        /**
         * Sets the list of contour counts.  Only used with the POLYGON_ARRAY
         * primitive.  Polygons can be made of several vertex lists 
         * called contours.  The first list is the polygon, and 
         * subsequent lists are "holes" that are removed from the
         * polygon.  All of the holes must be contained entirely
         * within the polygon.
         */
        public void SetContourCounts (int[] contourCounts) {
            _contourCounts = contourCounts;
        } // End of setContourCounts



        /**
         * Retrieves a reference to the array of contourCounts.
         */
        public int[] GetContourCounts () {
            return _contourCounts;
        } // End of getContourCounts



        /*
         * This routine will return an index list for any array of objects.
         */
        int[] getListIndices (Object[] list) {
            // Create list of indices to return
            int[] indices = new int[list.Length];

            // Create hash table with initial capacity equal to the number
            // of components (assuming about half will be duplicates)
            Dictionary<object, int> table = new Dictionary<object, int>(list.Length);

            int idx;
            for (int i = 0; i < list.Length; i++) {
                // Find index associated with this object
                if (!table.TryGetValue(list[i], out idx)) {
                    // We haven't seen this object before
                    indices[i] = i;

                    // Put into hash table and remember the index
                    table[list[i]] = i;
                } else {
                    // We've seen this object
                    indices[i] = idx;
                }
            }

            return indices;
        } // End of getListIndices



        // Class to hash 'size' integers
        private class IndexRow {
            int[] val;
            int size;
            private const int HASHCONST = unchecked((int)0xBABEFACE);

            public int hashCode () {
                int bits = 0;
                for (int i = 0; i < size; i++) {
                    bits ^= (bits * HASHCONST) << 2;
                }
                return bits;
            } // End of IndexRow.hashCode

            public bool equals (Object obj) {
                for (int i = 0; i < size; i++) {
                    if (((IndexRow)obj).get(i) != val[i]) return false;
                }
                return true;
            } // End of IndexRow.equals()

            public int get (int index) {
                return val[index];
            } // End of IndexRow.get

            public void set (int index, int value) {
                val[index] = value;
            } // End of IndexRow.set

            public IndexRow (int numColumns) {
                size = numColumns;
                val = new int[size];
            } // End of IndexRow constructor
        } // End of class IndexRow



        /**
         * Create index lists for all data lists.
         * Identical data entries are guaranteed to
         * use the same index value.  Does not remove unused data values
         * from the object - call compact() to do this.
         * @param useCoordIndexOnly Reformat the data into the
         * GeometryArray.USE_COORD_INDEX_ONLY format where there is only
         * one index list.  If the data is already in the USE_COORD_INDEX_ONLY
         * format, sending false (or calling indexify()) will change
         * it to the normal indexed format.
         * @throws IllegalArgumentException if coordinate data is missing,
         * if the index lists aren't all the
         * same length, if an index list is set and the corresponding data
         * list isn't set, if a data list is set and the corresponding
         * index list is unset (unless all index lists are unset or in
         * USE_COORD_INDEX_ONLY format),
         * if StripCounts or ContourCounts is inconsistent with the current
         * primitive, if the sum of the contourCounts array doesn't equal
         * the length of the StripCounts array, or if the number of vertices
         * isn't a multiple of three (for triangles) or four (for quads).
         */
        public void Indexify (bool useCoordIndexOnly) {
            checkForBadData();

            if (useCoordIndexOnly) {
                // Return if already in this format
                if (_coordOnly) return;

                // Start from normal indexed format
                Indexify(false);

                // Reformat data to USE_COORD_INDEX_ONLY format
                // Need to make an index into the index lists using each
                // row of indexes as one value

                // First, find out how many index lists there are;
                int numLists = 1;		// Always have coordinates
                if (_colorIndices != null) numLists++;
                if (_normalIndices != null) numLists++;
                numLists += _texCoordSetCount;

                // Make single array containing all indices
                int n = _coordinateIndices.Length;
                IndexRow[] ir = new IndexRow[n];
                int j;
                for (int i = 0; i < n; i++) {
                    ir[i] = new IndexRow(numLists);
                    j = 0;
                    ir[i].set(j++, _coordinateIndices[i]);
                    if (_colorIndices != null) ir[i].set(j++, _colorIndices[i]);
                    if (_normalIndices != null) ir[i].set(j++, _normalIndices[i]);
                    for (int k = 0; k < _texCoordSetCount; k++) {
                        ir[i].set(j++, _texCoordIndexSets[k][i]);
                    }
                }

                // Get index into that array
                int[] coordOnlyIndices = getListIndices(ir);

                // Get rid of duplicate rows
                int[] newInd = new int[coordOnlyIndices.Length];
                ir = (IndexRow[])compactData(coordOnlyIndices, ir, newInd);
                coordOnlyIndices = newInd;

                // Reformat data lists to correspond to new index

                // Allocate arrays to hold reformatted data
                Point3F[] newCoords = new Point3F[ir.Length];
                Color3F[] newColors3 = null;
                Color4F[] newColors4 = null;
                Vector3F[] newNormals = null;
                Object[][] newTexCoordSets = null;
                if (_colors3 != null) newColors3 = new Color3F[ir.Length];
                else if (_colors4 != null) newColors4 = new Color4F[ir.Length];
                if (_normals != null) newNormals = new Vector3F[ir.Length];
                for (int i = 0; i < _texCoordSetCount; i++) {
                    if (_texCoordDim == 2) {
                        if (i == 0) newTexCoordSets = new TexCoord2F[_texCoordSetCount][];
                        newTexCoordSets[i] = new TexCoord2F[ir.Length];
                    } else if (_texCoordDim == 3) {
                        if (i == 0) newTexCoordSets = new TexCoord3F[_texCoordSetCount][];
                        newTexCoordSets[i] = new TexCoord3F[ir.Length];
                    } else if (_texCoordDim == 4) {
                        if (i == 0) newTexCoordSets = new TexCoord4F[_texCoordSetCount][];
                        newTexCoordSets[i] = new TexCoord4F[ir.Length];
                    }
                }

                // Copy data into new arrays
                n = ir.Length;
                for (int i = 0; i < n; i++) {
                    j = 0;
                    newCoords[i] = _coordinates[(ir[i]).get(j++)];
                    if (_colors3 != null) {
                        newColors3[i] = _colors3[(ir[i]).get(j++)];
                    } else if (_colors4 != null) {
                        newColors4[i] = _colors4[(ir[i]).get(j++)];
                    }
                    if (_normals != null) newNormals[i] = _normals[(ir[i]).get(j++)];
                    for (int k = 0; k < _texCoordSetCount; k++) {
                        newTexCoordSets[k][i] = ((object[])_texCoordSets[k])[(ir[i]).get(j++)];
                    }
                }

                // Replace old arrays with new arrays
                _coordinates = newCoords;
                _colors3 = newColors3;
                _colors4 = newColors4;
                _normals = newNormals;
                _texCoordSets = newTexCoordSets;
                _coordinateIndices = coordOnlyIndices;
                _colorIndices = null;
                _normalIndices = null;
                _texCoordIndexSets = new int[_texCoordSetCount][];

                _coordOnly = true;
            } else if (_coordOnly) {
                // Need to change from useCoordIndexOnly format to normal
                // indexed format.  Should make a more efficient implementation
                // later.

                int n = _coordinateIndices.Length;
                if ((_colors3 != null) || (_colors4 != null)) {
                    _colorIndices = new int[n];
                    for (int i = 0; i < n; i++) _colorIndices[i] = _coordinateIndices[i];
                }

                if (_normals != null) {
                    _normalIndices = new int[n];
                    for (int i = 0; i < n; i++) _normalIndices[i] = _coordinateIndices[i];
                }

                _texCoordIndexSets = new int[_texCoordSetCount][];
                for (int i = 0; i < _texCoordSetCount; i++) {
                    _texCoordIndexSets[i] = new int[n];
                    for (int j = 0; j < n; j++) {
                        _texCoordIndexSets[i][j] = _coordinateIndices[j];
                    }
                }
                _coordOnly = false;
            } else {

                // No need to indexify if already indexed
                if (_coordinateIndices != null) return;

                _coordinateIndices = getListIndices(_coordinates);

                if (_colors3 != null) _colorIndices = getListIndices(_colors3);
                else if (_colors4 != null) _colorIndices = getListIndices(_colors4);

                if (_normals != null) _normalIndices = getListIndices(_normals);

                _texCoordIndexSets = new int[_texCoordSetCount][];
                for (int i = 0; i < _texCoordSetCount; i++) {
                    _texCoordIndexSets[i] = getListIndices((object[])_texCoordSets[i]);
                }

                _coordOnly = false;
            }
        } // End of indexify



        public void Indexify () {
            Indexify(false);
        } // End of indexify()



        /**
         * Allocates an array of the same type as the input type. This allows us to 
         * use a generic compactData method.
         *
         * @param data Array of coordinate, color, normal or texture coordinate data
         * The data can be in one of the following formats - Point3F, Color3f, 
         * Color4f, TexCoord2F, TexCoord3F, TexCoord4f.
         *
         * @param num The size of the array to be allocated
         *
         * @return An array of size num of the same type as the input type 
         *
         * @exception IllegalArgumentException if the input array is not one of the 
         * types listed above.
         */
        Object[] allocateArray (Object[] data, int num) {
            Object[] newData = null;
            if (data is Point3F[]) {
                newData = new Point3F[num];
            } else if (data is Vector3F[]) {
                newData = new Vector3F[num];
            } else if (data is Color3F[]) {
                newData = new Color3F[num];
            } else if (data is Color4F[]) {
                newData = new Color4F[num];
            } else if (data is TexCoord2F[]) {
                newData = new TexCoord2F[num];
            } else if (data is TexCoord3F[]) {
                newData = new TexCoord3F[num];
            } else if (data is TexCoord4F[]) {
                newData = new TexCoord4F[num];
            } else if (data is IndexRow[]) {
                // Hack so we can use compactData for coordIndexOnly
                newData = new IndexRow[num];
            } else throw new ArgumentOutOfRangeException("data");
            return newData;
        } // End of allocateArray



        /**
         * Generic method that compacts (ie removes unreferenced/duplicate data)
         * any type of indexed data. 
         * Used to compact coordinate, color, normal and texture coordinate data.
         * @param indices Array of indices
         * @param data Array of coordinate, color, normal or texture coordinate data
         * The data can be in one of the following formats - Point3F, Color3f, 
         * Color4f, TexCoord2F, TexCoord3F, TexCoord4f. 
         * @param newInd The new array of indexes after the data has been compacted.
         * This must be allocated by the calling method. On return, this array will 
         * contain the new index data. The size of this array must be equal to 
         * indices.length
         * @return Array of the data with unreferenced and duplicate entries removed. 
         * The return type will be the same as the type that was passed in data. 
         */
        // TODO:  Remove duplicate entries in data lists.
        private Object[] compactData (int[] indices, object[] data, int[] newInd) {
            Object[] newData = null;
            /*
             * This is a three step process.
             * First, find out how many unique indexes are used.  This
             * will be the size of the new data array.
             */
            int numUnique = 0;
            int[] translationTable = new int[data.Length];
            for (int i = 0; i < indices.Length; i++) {
                if (translationTable[indices[i]] == 0) {

                    numUnique++;
                    translationTable[indices[i]] = 1;
                }
            }
            /*
             * Second, build the new data list.  Remember the new indexes so
             * we can use the table to translate the old indexes to the new
             */
            newData = allocateArray(data, numUnique);
            int newIdx = 0;
            for (int i = 0; i < translationTable.Length; i++) {
                if (translationTable[i] != 0) {
                    newData[newIdx] = data[i];
                    translationTable[i] = newIdx++;
                }
            }
            /*
             * Third, make the new index list
             */
            for (int i = 0; i < indices.Length; i++) {
                newInd[i] = translationTable[indices[i]];
            }
            return newData;
        } // End of compactData



        /**
         * Remove unused data from an indexed dataset.
         * Indexed data may contain data entries that are never referenced by
         * the dataset.  This routine will remove those entries where 
         * appropriate and renumber the indices to match the new values.
         * @throws IllegalArgumentException if coordinate data is missing,
         * if the index lists aren't all the
         * same length, if an index list is set and the corresponding data
         * list isn't set, if a data list is set and the corresponding
         * index list is unset (unless all index lists are unset or in
         * USE_COORD_INDEX_ONLY format),
         * if StripCounts or ContourCounts is inconsistent with the current
         * primitive, if the sum of the contourCounts array doesn't equal
         * the length of the StripCounts array, or if the number of vertices
         * isn't a multiple of three (for triangles) or four (for quads).
         */
        public void Compact () {
            checkForBadData();

            // Only usable on indexed data
            if (_coordinateIndices == null) return;

            // USE_COORD_INDEX_ONLY never has unused data
            if (_coordOnly) return;

            int[] newInd = new int[_coordinateIndices.Length];
            _coordinates =
            (Point3F[])compactData(_coordinateIndices, _coordinates, newInd);
            _coordinateIndices = newInd;

            if (_colorIndices != null) {
                newInd = new int[_colorIndices.Length];
                if (_colors3 != null)
                    _colors3 = (Color3F[])compactData(_colorIndices, _colors3, newInd);
                else if (_colors4 != null)
                    _colors4 = (Color4F[])compactData(_colorIndices, _colors4, newInd);
                _colorIndices = newInd;
            }

            if (_normalIndices != null) {
                newInd = new int[_normalIndices.Length];
                _normals = (Vector3F[])compactData(_normalIndices, _normals, newInd);
                _normalIndices = newInd;
            }

            for (int i = 0; i < _texCoordSetCount; i++) {
                newInd = new int[_texCoordIndexSets[i].Length];
                _texCoordSets[i] = compactData(_texCoordIndexSets[i],
                                (object[])_texCoordSets[i], newInd);
                _texCoordIndexSets[i] = newInd;
            }
        } // End of compact



        /**
         * Check the data to make sure everything's consistent.
         */
        private void checkForBadData () {
            bool badData = false;

            //
            // Coordinates are required
            //
            if (_coordinates == null) {
                throw new InvalidOperationException();
            }

            //
            // Check for indices with no data
            //
            if ((_colors3 == null) && (_colors4 == null) && (_colorIndices != null))
                throw new InvalidOperationException();

            if ((_normals == null) && (_normalIndices != null))
                throw new InvalidOperationException();

            //
            // Make sure all TextureCoordinate data is set (indices or not)
            //
            for (int i = 0; i < _texCoordSetCount; i++) {
                if (_texCoordSets[i] == null)
                    throw new InvalidOperationException();
            }

            //
            // Check for Missing Index lists
            //
            bool texInds = false;	// Indicates whether we have texcoord indices
            if (_texCoordIndexSets != null) {
                for (int i = 0; i < _texCoordSetCount; i++) {
                    if (_texCoordIndexSets[i] != null) texInds = true;
                }
            }
            if ((_coordinateIndices != null) ||
            (_colorIndices != null) ||
            (_normalIndices != null) ||
            texInds) {
                // At least one index list is present, so they all must be
                // present (unless coordOnly)
                if (_coordinateIndices == null) badData = true;
                else if (_coordOnly) {
                    if ((_colorIndices != null) ||
                        (_normalIndices != null) ||
                        (texInds == true)) {
                        throw new InvalidOperationException();
                    }
                } else if (((_colors3 != null) || (_colors4 != null)) &&
                         (_colorIndices == null)) badData = true;
                else if ((_normals != null) && (_normalIndices == null)) badData = true;
                else if ((_texCoordSetCount > 0) && !texInds) badData = true;
                if (badData) throw new InvalidOperationException();
            }

            //
            // Make sure index lists are all the same length
            //
            if ((_coordinateIndices != null) && (!_coordOnly)) {
                if (((_colors3 != null) || (_colors4 != null)) &&
                    (_colorIndices.Length != _coordinateIndices.Length))
                    badData = true;
                else if ((_normals != null) &&
                         (_normalIndices.Length != _coordinateIndices.Length))
                    badData = true;
                else {
                    //Check all texCoord indices have the same length
                    for (int i = 0; i < _texCoordSetCount; i++) {
                        if (_texCoordIndexSets[i].Length != _coordinateIndices.Length) {
                            badData = true;
                            break;
                        }
                    }
                }
                if (badData) {
                    throw new InvalidOperationException();
                }
            }

            //
            // For stripped primitives, make sure we have strip counts
            //
            if ((_prim == TRIANGLE_STRIP_ARRAY) ||
            (_prim == TRIANGLE_FAN_ARRAY) ||
            (_prim == POLYGON_ARRAY)) {
                if (_stripCounts == null) badData = true;
            } else if (_stripCounts != null) badData = true;
            if (badData) {
                throw new InvalidOperationException();
            }

            // Find out how much data we have
            int count;
            if (_coordinateIndices == null) count = _coordinates.Length;
            else count = _coordinateIndices.Length;

            //
            // Make sure sum of strip counts equals indexCount (or vertexCount)
            // and check to make sure triangles and quads have the right number
            // of vertices
            //
            if ((_prim == TRIANGLE_STRIP_ARRAY) ||
            (_prim == TRIANGLE_FAN_ARRAY) ||
            (_prim == POLYGON_ARRAY)) {
                int sum = 0;
                for (int i = 0; i < _stripCounts.Length; i++) {
                    sum += _stripCounts[i];
                }
                if (sum != count) {
                    throw new InvalidOperationException();
                }
            } else if (_prim == TRIANGLE_ARRAY) {
                if (count % 3 != 0) {
                    throw new InvalidOperationException();
                }
            } else if (_prim == QUAD_ARRAY) {
                if (count % 4 != 0) {
                    throw new InvalidOperationException();
                }
            }

            //
            // For polygons, make sure the contours add up.
            //
            if (_prim == POLYGON_ARRAY) {
                if (_contourCounts != null) {
                    int c = 0;
                    for (int i = 0; i < _contourCounts.Length; i++)
                        c += _contourCounts[i];
                    if (c != _stripCounts.Length) {
                        throw new InvalidOperationException();
                    }
                }
            } else {
                if (_contourCounts != null) {
                    throw new InvalidOperationException();
                }
            }
        } // End of checkForBadData



        /**
         * Get rid of index lists by reorganizing data into an un-indexed
         * format.  Does nothing if no index lists are set.
         * @throws IllegalArgumentException if coordinate data is missing,
         * if the index lists aren't all the
         * same length, if an index list is set and the corresponding data
         * list isn't set, if a data list is set and the corresponding
         * index list is unset (unless all index lists are unset or in
         * USE_COORD_INDEX_ONLY format),
         * if StripCounts or ContourCounts is inconsistent with the current
         * primitive, if the sum of the contourCounts array doesn't equal
         * the length of the StripCounts array, or if the number of vertices
         * isn't a multiple of three (for triangles) or four (for quads).
         */
        public void Unindexify () {
            checkForBadData();
            if (_coordinateIndices != null) {
                // Switch from USE_COORD_INDEX_ONLY format
                if (_coordOnly) Indexify(false);

                _coordinates =
                  (Point3F[])unindexifyData(_coordinates, _coordinateIndices);
                _coordinateIndices = null;

                if (_colors3 != null) {
                    _colors3 = (Color3F[])unindexifyData(_colors3, _colorIndices);
                } else if (_colors4 != null) {
                    _colors4 = (Color4F[])unindexifyData(_colors4, _colorIndices);
                }
                _colorIndices = null;

                if (_normals != null) {
                    _normals = (Vector3F[])unindexifyData(_normals, _normalIndices);
                    _normalIndices = null;
                }

                for (int i = 0; i < _texCoordSetCount; i++)
                    _texCoordSets[i] = unindexifyData((object[])_texCoordSets[i],
                                     _texCoordIndexSets[i]);
                _texCoordIndexSets = new int[_texCoordSetCount][];
            }
        } // End of unindexify



        /**
         * Generic unindexify method. Can unindex data in any of the following 
         * formats Point3F, Color3f, Color4f, Vector3f, TexCoord2F, TexCoord3F, 
         * TexCoord4f.
         */
        private Object[] unindexifyData (Object[] data, int[] index) {
            Object[] newData = allocateArray(data, index.Length);
            for (int i = 0; i < index.Length; i++) {
                newData[i] = data[index[i]];
            }
            return newData;
        } // End of unindexifyData





        /**
         * Calculate vertexCount based on data
         */
        private int getVertexCount () {
            int vertexCount = _coordinates.Length;

            if (_colors3 != null) {
                if (_colors3.Length > vertexCount) vertexCount = _colors3.Length;
            } else if (_colors4 != null) {
                if (_colors4.Length > vertexCount) vertexCount = _colors4.Length;
            }

            if (_normals != null) {
                if (_normals.Length > vertexCount) vertexCount = _normals.Length;
            }

            // Find max length tex coord set
            for (int i = 0; i < _texCoordSetCount; i++) {
                if (((object[])_texCoordSets[i]).Length > vertexCount)
                    vertexCount = ((object[])_texCoordSets[i]).Length;
            }

            return vertexCount;
        } // End of getVertexCount
 


        /**
         * Redo indexes to guarantee connection information.
         * Use this routine if your original data is in indexed format, but
         * you don't trust that the indexing is correct.  After this 
         * routine it is guaranteed that two points with the same
         * position will have the same coordinate index (for example).  
         * Try this if you see
         * glitches in your normals or stripification, to rule out
         * bad indexing as the source of the problem.  Works with normal
         * indexed format or USE_COORD_INDEX_ONLY format.
         * @throws IllegalArgumentException if coordinate data is missing,
         * if the index lists aren't all the
         * same length, if an index list is set and the corresponding data
         * list isn't set, if a data list is set and the corresponding
         * index list is unset (unless all index lists are unset or in
         * USE_COORD_INDEX_ONLY format),
         * if StripCounts or ContourCounts is inconsistent with the current
         * primitive, if the sum of the contourCounts array doesn't equal
         * the length of the StripCounts array, or if the number of vertices
         * isn't a multiple of three (for triangles) or four (for quads).
         */
        public void RecomputeIndices () {
            bool remember = _coordOnly;

            // Can make more efficient implementation later
            Unindexify();
            Indexify(remember);
        } // End of recomputeIndices



        /**
         * Reverse the order of an array of ints (computer class homework
         * problem).
         */
        private void reverseList (int[] list) {
            int t;

            if (list == null) return;

            for (int i = 0; i < list.Length / 2; i++) {
                t = list[i];
                list[i] = list[list.Length - i - 1];
                list[list.Length - i - 1] = t;
            }
        } // End of reverseList



        /**
         * Reverse the order of all lists.  If your polygons are formatted with 
         * clockwise winding, you will always see the back and never the front.
         * (Java 3D always wants vertices specified with a counter-clockwise
         * winding.)
         * This method will (in effect) reverse the winding of your data by
         * inverting all of the index lists and the stripCounts
         * and contourCounts lists.
         * @throws IllegalArgumentException if coordinate data is missing,
         * if the index lists aren't all the
         * same length, if an index list is set and the corresponding data
         * list isn't set, if a data list is set and the corresponding
         * index list is unset (unless all index lists are unset or in
         * USE_COORD_INDEX_ONLY format),
         * if StripCounts or ContourCounts is inconsistent with the current
         * primitive, if the sum of the contourCounts array doesn't equal
         * the length of the StripCounts array, or if the number of vertices
         * isn't a multiple of three (for triangles) or four (for quads).
         */
        public void Reverse () {
            Indexify();
            reverseList(_stripCounts);
            reverseList(_oldStripCounts);
            reverseList(_contourCounts);
            reverseList(_coordinateIndices);
            reverseList(_colorIndices);
            reverseList(_normalIndices);
            for (int i = 0; i < _texCoordSetCount; i++)
                reverseList(_texCoordIndexSets[i]);
        } // End of reverse



        /**
         * Returns true if the data in this GeometryInfo is currently 
         * formatted in the USE_COORD_INDEX_ONLY format where a single
         * index list is used to index into all data lists.
         * @see GeometryInfo#indexify(bool)
         * @see GeometryInfo#getIndexedGeometryArray(bool, bool, bool,
         * bool, bool)
         */
        public bool GetUseCoordIndexOnly () {
            return _coordOnly;
        } // End of getUseCoordIndexOnly



        /**
         * Tells the GeometryInfo that its data is formatted in the
         * USE_COORD_INDEX_ONLY format with a single index list
         * (the coordinate index list) that indexes into all data
         * lists (coordinates, normals, colors, and texture 
         * coordinates).  NOTE: this will not convert the data
         * for you.  This method is for when you are sending in
         * data useng the setCoordinates, setNormals, setColors,
         * and/or setTextureCoordinates methods, and you are only
         * setting one index using setCoordinateIndices().  If
         * you want GeometryInfo to convert your data to the
         * USE_COORD_INDEX_ONLY format, use indexify(true) or
         * getIndexedGeometryArray with the useCoordIndexOnly
         * parameter set to true.
         * @see GeometryInfo#indexify(bool)
         * @see GeometryInfo#getIndexedGeometryArray(bool, bool, bool,
         * bool, bool)
         */
        public void SetUseCoordIndexOnly (bool useCoordIndexOnly) {
            _coordOnly = useCoordIndexOnly;
        } // End of setUseCoordIndexOnly
    } // End of class GeometryInfo

    // End of file GeometryInfo.java

}
