﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using Nen.Collections;

namespace Nen.Xml.Serialization {
    /// <summary>
    /// This is an XmlSerializer factory intended to replace the XmlSerializer
    /// constructor.  XmlSerializer will often forget about assemblies it has
    /// generated when given certain constructor arguments, causing slowdown
    /// and eventual memory exhaustion.
    /// </summary>
    public static class CachingXmlSerializerFactory {
        private class CachedXmlSerializer {
            #region Constructors
            public CachedXmlSerializer (XmlSerializer xmlSerializer, Type type) {
                XmlSerializer = xmlSerializer;
                Type = type;
            }
            #endregion

            public bool Match (Type type, Type[] extraTypes, XmlRootAttribute root, string defaultNamespace) {
                return Type == type && ExtraTypes.SequenceEqual(extraTypes) && Equals(Root, root) && DefaultNamespace == defaultNamespace;
            }

            #region Accessors
            public string DefaultNamespace { get; set; }
            public Type[] ExtraTypes { get; set; }
            public XmlRootAttribute Root { get; set; }
            public Type Type { get; private set; }
            public XmlSerializer XmlSerializer { get; private set; }
            #endregion
        }

        private static Collection<CachedXmlSerializer> _serializers = new Collection<CachedXmlSerializer>();

        #region Creation
        /// <summary>
        /// Finds or constructs an instance of XmlSerializer with the specified criteria.
        /// </summary>
        /// <param name="type">The type of the object the XmlSerializer can serialize.</param>
        /// <returns>An instance of XmlSerializer.</returns>
        public static XmlSerializer Create (Type type) {
            return Create(type, new Type[0], null, null);
        }

        /// <summary>
        /// Finds or constructs an instance of XmlSerializer with the specified criteria.
        /// </summary>
        /// <param name="type">The type of the object the XmlSerializer can serialize.</param>
        /// <param name="extraTypes">A Type array of additional object types to serialize.</param>
        /// <returns>An instance of XmlSerializer.</returns>
        public static XmlSerializer Create (Type type, Type[] extraTypes) {
            return Create(type, extraTypes, null, null);
        }

        /// <summary>
        /// Finds or constructs an instance of XmlSerializer with the specified criteria.
        /// </summary>
        /// <param name="type">The type of the object the XmlSerializer can serialize.</param>
        /// <param name="extraTypes">A Type array of additional object types to serialize.</param>
        /// <param name="root">An XmlRootAttribute that represents the root element.</param>
        /// <param name="defaultNamespace">The default namespace to use for all the XML elements.</param>
        /// <returns></returns>
        public static XmlSerializer Create (Type type, Type[] extraTypes, XmlRootAttribute root, string defaultNamespace) {
            lock (_serializers) {
                XmlSerializer xmlSerializer = GetXmlSerializer(type, extraTypes, root, defaultNamespace);
                if (xmlSerializer == null) {
                    xmlSerializer = new XmlSerializer(type, null, extraTypes, root, defaultNamespace);

                    CachedXmlSerializer serializer = new CachedXmlSerializer(xmlSerializer, type);
                    serializer.DefaultNamespace = defaultNamespace;
                    serializer.ExtraTypes = extraTypes;
                    serializer.Root = root;

                    _serializers.Add(serializer);
                }

                return xmlSerializer;
            }
        }
        #endregion

        #region Search
        private static XmlSerializer GetXmlSerializer (Type type, Type[] extraTypes, XmlRootAttribute root, string defaultNamespace) {
            var serializer = _serializers.FirstOrDefault(s => s.Match(type, extraTypes, root, defaultNamespace));
            return serializer == null ? null : serializer.XmlSerializer;
        }
        #endregion
    }
}
