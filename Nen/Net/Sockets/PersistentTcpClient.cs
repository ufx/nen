﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace Nen.Net.Sockets {
    public abstract class PersistentTcpClient : IDisposable {
        private bool _closed;

        #region Constructor / Disposer
        protected PersistentTcpClient () {
            SecondsToSleepAfterDisconnect = -30;
            SecondsToSleepAfterDisconnectIncrement = 30;
            MaximumSecondsToSleepAfterDisconnect = 60 * 5;
        }

        public void Dispose () {
            Dispose(true);
        }

        protected virtual void Dispose (bool disposing) {
            if (_closed)
                return;

            _closed = true;

            if (!disposing)
                return;

            if (UnderlyingClient != null) {
                UnderlyingClient.Close();
                UnderlyingClient = null;
                OnDisconnected(null);
            }
        }
        #endregion

        #region Connection State
        public event EventHandler Connected;
        public event EventHandler<ExceptionEventArgs> Disconnected;

        protected virtual void OnConnected () {
            var e = Connected;
            if (e != null)
                e(this, EventArgs.Empty);
        }

        protected virtual void OnDisconnected (Exception ex) {
            var e = Disconnected;
            if (e != null)
                e(this, new ExceptionEventArgs(ex));
        }
        #endregion

        #region Connection Loop
        public void Connect (string hostname, int port) {
            Hostname = hostname;
            Port = port;

            if (UnderlyingClient != null)
                UnderlyingClient.Close();

            UnderlyingClient = new TcpClient(hostname, port);

            if (ReceiveTimeoutMs != 0)
                UnderlyingClient.ReceiveTimeout = ReceiveTimeoutMs;

            OnConnected();
        }

        public void Close () {
            Dispose(true);
        }

        protected T Once<T> (Func<T> operation) {
            while (!_closed) {
                try {
                    while (UnderlyingClient == null || !UnderlyingClient.Connected)
                        Connect(Hostname, Port);

                    SecondsToSleepAfterDisconnect = -SecondsToSleepAfterDisconnectIncrement;

                    return operation();

                } catch (SocketException ex) {
                    HandleRecoverableException(ex);
                } catch (IOException ex) {
                    HandleRecoverableException(ex);
                }
            }

            return default(T);
        }

        protected void Once (Action operation) {
            Once<object>(() => { operation(); return null; });
        }

        protected void Forever (Action action) {
            while (!_closed) {
                try {
                    while (UnderlyingClient == null || !UnderlyingClient.Connected)
                        Connect(Hostname, Port);

                    SecondsToSleepAfterDisconnect = -SecondsToSleepAfterDisconnectIncrement;

                    action();

                } catch (SocketException ex) {
                    HandleRecoverableException(ex);
                } catch (IOException ex) {
                    HandleRecoverableException(ex);
                }
            }
        }

        protected void HandleRecoverableException (Exception ex) {
            if (SecondsToSleepAfterDisconnect <= MaximumSecondsToSleepAfterDisconnect)
                SecondsToSleepAfterDisconnect += SecondsToSleepAfterDisconnectIncrement;

            OnDisconnected(ex);

            if (SecondsToSleepAfterDisconnect > 0)
                System.Threading.Thread.Sleep(1000 * SecondsToSleepAfterDisconnect);
        }
        #endregion

        #region Accessors
        public string Hostname { get; set; }
        public int Port { get; set; }
        public TcpClient UnderlyingClient { get; private set; }
        public int SecondsToSleepAfterDisconnect { get; set; }
        public int SecondsToSleepAfterDisconnectIncrement { get; set; }
        public int MaximumSecondsToSleepAfterDisconnect { get; set; }
        public int ReceiveTimeoutMs { get; set; }
        #endregion
    }

    public class ExceptionEventArgs : EventArgs {
        public ExceptionEventArgs (Exception exception) {
            Exception = exception;
        }

        public Exception Exception { get; set; }
    }
}
