﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Nen.Net.Sockets {
    /// <summary>
    /// A TCP server that asynchronously dispatches new client connections. 
    /// </summary>
    public abstract class AsynchronousTcpServer : IDisposable {
        private TcpListener _listener;
        private object _clientLock = new object();

        #region Constructors
        /// <summary>
        /// Consturcts a new AsynchronousTcpServer.
        /// </summary>
        public AsynchronousTcpServer () {
            TcpClients = new Collection<TcpClient>();
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Closes all existing connections and ceases listening.
        /// </summary>
        public void Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Closes all existing connections and ceases listening.
        /// </summary>
        /// <param name="disposing">true if disposing, false if otherwise.</param>
        protected virtual void Dispose (bool disposing) {
            Stop();
        }
        #endregion

        #region Execution
        /// <summary>
        /// Starts listening for incoming connection requests.
        /// </summary>
        /// <param name="localaddr">An IPAddress that represents the local IP address.</param>
        /// <param name="port">The port on which to listen for incoming connection attempts.</param>
        public void Start (IPAddress localaddr, int port) {
            _listener = new TcpListener(localaddr, port);
            _listener.Start();

            try {
                while (true) {
                    var client = _listener.AcceptTcpClient();

                    lock (_clientLock) {
                        if (TcpClients == null)
                            break;

                        TcpClients.Add(client);
                    }

                    switch (ConnectionMode) {
                        case TcpServerConnectionMode.QueueUserWorkItemPerConnection:
                            ThreadPool.QueueUserWorkItem((s) => ServiceClientAsynchronously(client));
                            break;

                        case TcpServerConnectionMode.ThreadPerConnection:
                            new Thread(() => ServiceClientAsynchronously(client)).Start();
                            break;

                        case TcpServerConnectionMode.Synchronous:
                            ServiceClient(client);
                            break;
                    }
                }
            } catch (SocketException ex) {
                // The socket operation was canceled.
                if (ex.ErrorCode == 10004)
                    return;

                throw;
            }
        }

        /// <summary>
        /// Closes all existing connections and ceases listening.
        /// </summary>
        public void Stop () {
            if (_listener == null)
                return;

            _listener.Stop();
            _listener = null;

            lock (_clientLock) {
                foreach (var client in TcpClients) {
                    if (client.Connected)
                        client.Close();
                }
                TcpClients.Clear();
                TcpClients = null;
            }
        }
        #endregion

        #region Client Management
        /// <summary>
        /// Services the specified client.  When this method exits, the client
        /// connection is closed if it is still active.
        /// </summary>
        /// <param name="client">The client to service.</param>
        protected abstract void ServiceClient (TcpClient client);

        private void ServiceClientAsynchronously (TcpClient client) {
            try {
                ServiceClient(client);
            } catch (System.IO.IOException) {
                // Exit gracefully.
            } catch (SocketException) {
                // Exit gracefully.
            } finally {
                if (client.Connected)
                    client.Close();

                lock (_clientLock) {
                    if (TcpClients != null)
                        TcpClients.Remove(client);
                }
            }
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the connection mode this server will dispatch clients
        /// with.
        /// </summary>
        public TcpServerConnectionMode ConnectionMode { get; set; }

        /// <summary>
        /// Gets the collection of active TcpClients.
        /// </summary>
        public Collection<TcpClient> TcpClients { get; private set; }
        #endregion
    }

    /// <summary>
    /// Represents a connection mode to dispatch clients with.
    /// </summary>
    public enum TcpServerConnectionMode {
        /// <summary>
        /// Clients are dispatched on a new thread for each connection.
        /// </summary>
        ThreadPerConnection,

        /// <summary>
        /// Clients are dispatched as a user work item in the thread pool.
        /// </summary>
        QueueUserWorkItemPerConnection,

        /// <summary>
        /// Clients are dispatched synchronously on the same thread that the
        /// server is listening on.  This is typically used for debugging
        /// purposes only.
        /// </summary>
        Synchronous
    }
}
