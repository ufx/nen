﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace Nen.Net.Sockets {
    /// <summary>
    /// Extensions to the System.Net.Sockets.TcpClient class.
    /// </summary>
    public static class TcpClientEx {
        /// <summary>
        /// Creates a new connection and sends a one shot payload of data before disconnecting.
        /// </summary>
        /// <param name="hostname">The DNS name of the remote host to which you intend to connect.</param>
        /// <param name="port">The port number of the remote host of which you intend to connect.</param>
        /// <param name="data">The data to send.</param>
        public static void SimpleSend (string hostname, int port, byte[] data) {
            var client = new TcpClient(hostname, port);
            try {
                client.Client.Send(data);
            } finally {
                client.GetStream().Dispose();
                client.Close();
            }
        }
    }
}
