﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Nen.Net.Sockets {
    /// <summary>
    /// Extensions to the System.Net.Sockets.TcpListener class.
    /// </summary>
    public static class TcpListenerEx {
        /// <summary>
        /// Starts a new connection and listens for a one shot payload of data before stopping.
        /// </summary>
        /// <param name="localaddr">The local IP address to listen on.</param>
        /// <param name="port">The port on which to listen for incoming connection attempts.</param>
        /// <returns>The retrieved data.</returns>
        public static byte[] SimpleReceive (IPAddress localaddr, int port) {
            var listener = new TcpListener(localaddr, port);
            listener.Start();

            try {
                using (var client = listener.AcceptTcpClient()) {
                    using (var memory = new MemoryStream()) {
                        var buffer = new byte[1024];
                        while (client.Connected) {
                            var bytesReceived = client.Client.Receive(buffer);
                            if (bytesReceived == 0)
                                break;

                            memory.Write(buffer, 0, bytesReceived);
                        }

                        return memory.ToArray();
                    }
                }
            } finally {
                listener.Stop();
            }
        }
    }
}
