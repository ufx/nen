﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Data.SqlModel;
using Nen.Internal;

namespace Nen.Data {
    /// <summary>
    /// Extensions to the DataColumn class.
    /// </summary>
    public static class DataColumnEx {
        /// <summary>
        /// Emits data definitions to create the column.
        /// </summary>
        /// <param name="column">The column to emit data definitions for.</param>
        /// <returns>A create column definition.</returns>
        public static SqlCreateColumn GenerateDataDefinition (this DataColumn column) {
            var createColumn = new SqlCreateColumn();
            createColumn.Expression = new SqlIdentifier(column.ColumnName);
            createColumn.DataType = SqlDataType.ForDataColumn(column);
            createColumn.AutoIncrements = column.AutoIncrement;
            createColumn.AutoIncrementSeed = column.AutoIncrementSeed;
            createColumn.AutoIncrementStep = column.AutoIncrementStep;
            createColumn.Nullable = column.AllowDBNull;

            if (column.DefaultValue == DBNull.Value)
                createColumn.DefaultExpression = column.GetDefaultExpression();
            else
                createColumn.DefaultExpression = new SqlLiteralExpression(column.DefaultValue);


            return createColumn;
        }

        /// <summary>
        /// Gets the default expression used when generating data definitions of this column.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>The default expression used when generating data definitions.</returns>
        public static SqlExpression GetDefaultExpression (this DataColumn column) {
            if (!column.ExtendedProperties.ContainsKey("Nen.Data.DataColumnEx.DefaultExpression"))
                return null;

            return (SqlExpression) column.ExtendedProperties["Nen.Data.DataColumnEx.DefaultExpression"];
        }

        /// <summary>
        /// Sets the default expression used when generating data definitions of this column
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="defaultExpression">The default expression used when generating data definitions.</param>
        public static void SetDefaultExpression (this DataColumn column, SqlExpression defaultExpression) {
            column.ExtendedProperties["Nen.Data.DataColumnEx.DefaultExpression"] = defaultExpression;
        }
        
        /// <summary>
        /// Gets the maximum binary character length of the column.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>The maximum binary character length, or 0 if no length is specified.</returns>
        public static int GetMaxBinaryLength (this DataColumn column) {
            if (!column.ExtendedProperties.ContainsKey("Nen.Data.DataColumnEx.MaxLength"))
                return 0;

            return (int) column.ExtendedProperties["Nen.Data.DataColumnEx.MaxLength"];
        }

        /// <summary>
        /// Sets the maximum binary character length of the column.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="maxBinaryLength">The maximum binary character length.</param>
        public static void SetMaxBinaryLength (this DataColumn column, int maxBinaryLength) {
            column.ExtendedProperties["Nen.Data.DataColumnEx.MaxLength"] = maxBinaryLength;
        }

        public static byte GetPrecision(this DataColumn column)
        {
            if (!column.ExtendedProperties.ContainsKey("Nen.Data.DataColumnEx.Precision"))
                return 0;

            return (byte)column.ExtendedProperties["Nen.Data.DataColumnEx.Precision"];
        }

        public static void SetPrecision(this DataColumn column, byte precision)
        {
            column.ExtendedProperties["Nen.Data.DataColumnEx.Precision"] = precision;
        }

        public static int GetScale(this DataColumn column)
        {
            if (!column.ExtendedProperties.ContainsKey("Nen.Data.DataColumnEx.Scale"))
                return 0;

            return (int)column.ExtendedProperties["Nen.Data.DataColumnEx.Scale"];
        }

        public static void SetScale(this DataColumn column, int scale)
        {
            column.ExtendedProperties["Nen.Data.DataColumnEx.Scale"] = scale;
        }

        public static DbType? GetDbType (this DataColumn column) {
            if (!column.ExtendedProperties.ContainsKey("Nen.Data.DataColumnEx.DbType"))
                return null;

            return (DbType) column.ExtendedProperties["Nen.Data.DataColumnEx.DbType"];
        }

        public static void SetDbType (this DataColumn column, DbType type) {
            column.ExtendedProperties["Nen.Data.DataColumnEx.DbType"] = type;
        }
    }
}
