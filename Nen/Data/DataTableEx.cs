﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.SqlModel;

namespace Nen.Data {
    /// <summary>
    /// Extensions to the DataTable class.
    /// </summary>
    public static class DataTableEx {
        /// <summary>
        /// Emits data definitions to create the table, excluding foreign keys.
        /// </summary>
        /// <param name="table">The table to emit data definitions for.</param>
        /// <returns>A create table definition.</returns>
        public static List<SqlExpression> GenerateDataDefinitions (this DataTable table) {
            var expressions = new List<SqlExpression>();

            var primaryKeySequenceName = (string)table.ExtendedProperties["Nen.Data.TableEx.PrimaryKeySequenceName"];
            if (!string.IsNullOrEmpty(primaryKeySequenceName))
            {
                // Add the PK sequence expression first, so it's available for table defaults.

                var createSequence = new SqlCreateSequence();
                createSequence.Name = new SqlIdentifier(primaryKeySequenceName);

                var schemaName = (string)table.ExtendedProperties["Nen.Data.TableEx.PrimaryKeySequenceSchemaName"];
                if (schemaName != null)
                    createSequence.SchemaName = new SqlIdentifier(schemaName);

                expressions.Add(createSequence);
            }

            var createTable = new SqlCreateTable();

            createTable.Name = new SqlIdentifier(table.TableName);

            if (table.GetSchemaName() != null)
                createTable.SchemaName = new SqlIdentifier(table.GetSchemaName());

            foreach (DataColumn column in table.Columns)
                createTable.Columns.Add(column.GenerateDataDefinition());

            foreach (Constraint constraint in table.Constraints) {
                if (constraint is ForeignKeyConstraint)
                    continue;

                createTable.Constraints.Add(constraint.GenerateDataDefinition());
            }

            expressions.Add(createTable);
            return expressions;
        }

        /// <summary>
        /// Emits data definitions to create the foreign keys for a table.
        /// </summary>
        /// <param name="table">The table to emit foreign key data definitions for.</param>
        /// <returns>An alter table definition.</returns>
        public static SqlAlterTable GenerateForeignKeyConstraintDefinitions (this DataTable table) {
            var constraints = table.Constraints.OfType<ForeignKeyConstraint>().ToArray();
            if (constraints.Length == 0)
                return null;

            var alterTable = new SqlAlterTable();

            if (table.GetSchemaName() != null)
                alterTable.SchemaName = new SqlIdentifier(table.GetSchemaName());

            alterTable.Name = new SqlIdentifier(table.TableName);

            foreach (var constraint in constraints)
                alterTable.Constraints.Add(constraint.GenerateDataDefinition());

            return alterTable;
        }

        /// <summary>
        /// Emits inserts for each row of data in the table.
        /// </summary>
        /// <param name="table">The table to emit inserts for.</param>
        /// <returns>A compound expression containing all of the inserts, or null if the table contained no rows.</returns>
        public static SqlCompoundExpression GenerateInserts (this DataTable table) {
            if (table.Rows.Count == 0)
                return null;

            var inserts = new SqlCompoundExpression();

            foreach (DataRow row in table.Rows)
                inserts.Expressions.Add(row.GenerateInsert());

            return inserts;
        }

        public static bool ContainsSubset (this DataTable table, DataTable subTable) {
            foreach (DataRow subRow in subTable.Rows) {
                var row = table.Rows.Find(subRow.GetPrimaryKeyField());
                if (row == null)
                    return false;

                if (!row.ItemArray.SequenceEqual(subRow.ItemArray))
                    return false;
            }

            return true;
        }

        #region Extended Properties
        /// <summary>
        /// Gets the schema name associated with the table.
        /// </summary>
        /// <param name="table">The table to obtain a schema name for.</param>
        /// <returns>The schema name of the table.</returns>
        public static string GetSchemaName (this DataTable table) {
            if (!table.ExtendedProperties.ContainsKey("Nen.Data.DataTableEx.SchemaName"))
                return null;

            return (string) table.ExtendedProperties["Nen.Data.DataTableEx.SchemaName"];
        }

        /// <summary>
        /// Sets the schema name associated with the table.
        /// </summary>
        /// <param name="table">The table to set a schema name for.</param>
        /// <param name="schemaName">The schema name to set.</param>
        public static void SetSchemaName (this DataTable table, string schemaName) {
            table.ExtendedProperties["Nen.Data.DataTableEx.SchemaName"] = schemaName;
        }
        #endregion
    }
}
