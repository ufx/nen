using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.SqlModel;
using Nen.Internal;
using Nen.Reflection;

namespace Nen.Data {
    /// <summary>
    /// Formats SQL expressions into SQL text.
    /// </summary>
    public class SqlFormatter : ISqlExpressionVisitor {
        private Func<IDbCommand> _createCommand;
        private int _parameterCounter;

        #region Constructor
        /// <summary>
        /// Constructs a new SqlFormatter.
        /// </summary>
        /// <param name="createCommand">An IDbCommand generator for the target database.</param>
        public SqlFormatter (Func<IDbCommand> createCommand) {
            _createCommand = createCommand;
        }
        #endregion

        #region Statement Generation
        /// <summary>
        /// Generates a command for the specified expression.
        /// </summary>
        /// <param name="expression">The expression to format.</param>
        /// <returns>A command for the expression.</returns>
        public IDbCommand Generate (SqlExpression expression) {
            if (expression is SqlCompoundExpression)
                return Generate((SqlCompoundExpression) expression);

            if (expression is SqlSelect)
                return Generate((SqlSelect) expression);

            if (expression is SqlInsert)
                return Generate((SqlInsert) expression);

            if (expression is SqlUpdate)
                return Generate((SqlUpdate) expression);

            if (expression is SqlDelete)
                return Generate((SqlDelete) expression);

            if (expression is SqlProcedure)
                return Generate((SqlProcedure) expression);

            throw new ArgumentException(LS.T("The SqlFormatter can only generate commands for top-level expressions such as select, insert, update and delete."), "expression");
        }

        /// <summary>
        /// Generates a command for the specified compound.
        /// </summary>
        /// <param name="compound">The compound to format.</param>
        /// <returns>A command for the compound.</returns>
        public IDbCommand Generate (SqlCompoundExpression compound) {
            return GenerateStatement(() => Visit(compound));
        }

        /// <summary>
        /// Generates a command for the specified select.
        /// </summary>
        /// <param name="sqlSelect">The select to format.</param>
        /// <returns>A command for the select.</returns>
        public IDbCommand Generate (SqlSelect sqlSelect) {
            return GenerateStatement(() => Visit(sqlSelect));
        }

        /// <summary>
        /// Generates a command for the specified insert.
        /// </summary>
        /// <param name="insert">The insert to format.</param>
        /// <returns>A command for the insert.</returns>
        public IDbCommand Generate (SqlInsert insert) {
            return GenerateStatement(() => Visit(insert));
        }

        /// <summary>
        /// Generates a command for the specified update.
        /// </summary>
        /// <param name="update">The update to format.</param>
        /// <returns>A command for the update.</returns>
        public IDbCommand Generate (SqlUpdate update) {
            return GenerateStatement(() => Visit(update));
        }

        /// <summary>
        /// Generates a command for the specified delete.
        /// </summary>
        /// <param name="delete">The delete to format.</param>
        /// <returns>A command for the delete.</returns>
        public IDbCommand Generate (SqlDelete delete) {
            return GenerateStatement(() => Visit(delete));
        }

        /// <summary>
        /// Generates a command for the specified procedure.
        /// </summary>
        /// <param name="procedure">The procedure to format.</param>
        /// <returns>A command for the procedure.</returns>
        public IDbCommand Generate (SqlProcedure procedure) {
            return GenerateStatement(() => Visit(procedure));
        }

        private IDbCommand GenerateStatement (Action generate) {
            Command = _createCommand();
            Command.CommandType = CommandType.Text;
            Command.CommandText = GenerateText(generate);
            return Command;
        }

        private string GenerateText (Action generate) {
            Text = new StringBuilder();
            _parameterCounter = 0;
            Parameters = new Dictionary<object, IDbDataParameter>();

            generate();

            Parameters = null;

            return Text.ToString();
        }
        #endregion

        #region Statement Visitation
        /// <summary>
        /// Formats a compound statement.
        /// </summary>
        /// <param name="sqlCompound">The compound to format.</param>
        public virtual void Visit (SqlCompoundExpression sqlCompound) {
            if (sqlCompound.Expressions.Count > 0)
            {
                VisitEach("; \r\n", sqlCompound.Expressions);
                Text.Append(";"); // Always trail with a semicolon.
            }
        }

        /// <summary>
        /// Formats a select statement.
        /// </summary>
        /// <param name="sqlSelect">The select to format.</param>
        public virtual void Visit (SqlSelect sqlSelect) {
            // SELECT <column1, column2, ..., columnN>
            Text.Append("SELECT ");

            if (sqlSelect.IsDistinct)
                Text.Append(" DISTINCT ");

            VisitEach(", ", sqlSelect.Columns);

            if (sqlSelect.From.Count > 0) {
                // FROM <query1, query2, ..., queryN>
                Text.Append(" FROM ");
                VisitEach(", ", sqlSelect.From);
            }

            // <join1 join2 ... joinN>
            VisitEach(" ", sqlSelect.Joins);

            // WHERE <expression>
            if (sqlSelect.Where != null) {
                Text.Append(" WHERE ");
                sqlSelect.Where.Accept(this);
            }

            // ORDER BY <query1, query2, ..., queryN>
            GenerateOrderBy(sqlSelect);

            // GROUP BY <query1, query2, ..., queryN>
            if (sqlSelect.GroupBy.Count > 0) {
                Text.Append(" GROUP BY ");
                VisitEach(", ", sqlSelect.GroupBy);
            }
        }

        /// <summary>
        /// Formats an insert statement.
        /// </summary>
        /// <param name="sqlInsert">The insert to format.</param>
        public virtual void Visit (SqlInsert sqlInsert) {
            // INSERT INTO <table>
            Text.Append("INSERT INTO ");
            Visit(sqlInsert.Table);

            // (<column1, column2, ..., columnN>)
            if (sqlInsert.Columns.Count > 0) {
                Text.Append(" (");
                VisitEach(", ", sqlInsert.Columns);
                Text.Append(")");
            }

            Text.Append(" VALUES(");

            // VALUES(value1, value2, ... valueN)
            VisitEach(", ", sqlInsert.Values);
            Text.Append(")");
        }

        /// <summary>
        /// Formats an update statement.
        /// </summary>
        /// <param name="sqlUpdate">The update to format.</param>
        public virtual void Visit (SqlUpdate sqlUpdate) {
            // UPDATE <table>
            Text.Append("UPDATE ");
            Visit(sqlUpdate.Table);
            Text.Append(" SET ");

            // SET column1 = value1, column2 = value2, ..., columnN = valueN
            for (var i = 0; i < sqlUpdate.Columns.Count; i++) {
                var column = sqlUpdate.Columns.ElementAt(i);
                var value = sqlUpdate.Values.ElementAt(i);

                Visit(column);
                Text.Append(" = ");
                value.Accept(this);

                if (i + 1 < sqlUpdate.Columns.Count)
                    Text.Append(", ");
            }

            // WHERE <expression>
            if (sqlUpdate.Where != null) {
                Text.Append(" WHERE ");

                sqlUpdate.Where.Accept(this);
            }
        }

        /// <summary>
        /// Formats a delete statement.
        /// </summary>
        /// <param name="sqlDelete">The delete to format.</param>
        public virtual void Visit (SqlDelete sqlDelete) {
            if (sqlDelete == null)
                throw new ArgumentNullException("sqlDelete");

            // DELETE FROM <table>
            Text.Append("DELETE FROM ");
            Visit(sqlDelete.Table);

            // WHERE <expression>
            if (sqlDelete.Where != null) {
                Text.Append(" WHERE ");
                sqlDelete.Where.Accept(this);
            }
        }

        /// <summary>
        /// Formats a procedure statement.
        /// </summary>
        /// <param name="sqlProcedure">The procedure to format.</param>
        public virtual void Visit (SqlProcedure sqlProcedure) {
            if (sqlProcedure == null)
                throw new ArgumentNullException("sqlProcedure");

            Text.Append("EXEC ");
            Text.Append(sqlProcedure.ProcedureName);

            if (sqlProcedure.ProcedureArguments.Count > 0) {
                Text.Append(" ");
                VisitEach(", ", sqlProcedure.ProcedureArguments);
            }
        }

        public virtual void Visit (SqlSet sqlSet) {
            if (sqlSet == null)
                throw new ArgumentNullException("sqlSet");

            Text.Append("SET ");
            sqlSet.Variable.Accept(this);
            Text.Append(" = ");
            sqlSet.Value.Accept(this);
        }
        #endregion

        #region Operator Visitation
        /// <summary>
        /// Formats a binary operator from left to right.
        /// </summary>
        /// <param name="binaryOperator">The binary operator to format.</param>
        public virtual void Visit (SqlBinaryOperatorExpression binaryOperator) {
            if (binaryOperator == null)
                throw new ArgumentNullException("binaryOperator");

            Enclose(binaryOperator.Left, false);

            // Special case handling of IS NULL and IS NOT NULL.
            var rightIsNull = binaryOperator.Right is SqlLiteralExpression && ((SqlLiteralExpression) binaryOperator.Right).Value == null;
            if (binaryOperator.Operator == "is" && rightIsNull)
                Text.Append(" IS NULL");
            else if (binaryOperator.Operator == "is not" && rightIsNull)
                Text.Append(" IS NOT NULL");
            else {
                Text.AppendFormat(" {0} ", binaryOperator.Operator);
                var alwaysEnclose = (binaryOperator.Operator == "IN" || binaryOperator.Operator == "NOT IN");
                Enclose(binaryOperator.Right, alwaysEnclose);
            }
        }

        /// <summary>
        /// Formats a prefix operator.
        /// </summary>
        /// <param name="prefixOperator">The prefix operator to format.</param>
        public virtual void Visit (SqlPrefixOperatorExpression prefixOperator) {
            if (prefixOperator == null)
                throw new ArgumentNullException("prefixOperator");

            Text.AppendFormat("{0} ", prefixOperator.Operator);
            Enclose(prefixOperator.Operand, false);
        }

        /// <summary>
        /// Formats a postfix operator.
        /// </summary>
        /// <param name="postfixOperator">The postfix operator to format.</param>
        public virtual void Visit (SqlPostfixOperatorExpression postfixOperator) {
            if (postfixOperator == null)
                throw new ArgumentNullException("postfixOperator");

            Enclose(postfixOperator.Operand, false);
            Text.AppendFormat(" {0}", postfixOperator.Operator);
        }
        #endregion

        #region Schema Visitation
        /// <summary>
        /// Formats a create table statement.
        /// </summary>
        /// <param name="createTable">The create table to format.</param>
        public virtual void Visit (SqlCreateTable createTable) {
            if (createTable == null)
                throw new ArgumentNullException("createTable");

            var orderedColumns = createTable.Columns.OrderByDescending(createTable.IsPrimaryKeyColumn).ThenBy(c => c.Expression.ToString());

            Text.Append("CREATE TABLE ");
            GenerateCompoundIdentifierName(createTable.SchemaName, createTable.Name);
            Text.Append(" (\r\n    ");

            VisitEach(",\r\n    ", orderedColumns);

            if (createTable.Constraints.Count > 0)
            {
                Text.Append(",\r\n    ");
                VisitEach(",\r\n    ", createTable.Constraints);
            }

            Text.Append("\r\n)");
        }

        /// <summary>
        /// Formats an alter table statement.
        /// </summary>
        /// <param name="alterTable">The alter table to format.</param>
        public virtual void Visit (SqlAlterTable alterTable) {
            if (alterTable == null)
                throw new ArgumentNullException("alterTable");

            Text.Append("ALTER TABLE ");
            GenerateCompoundIdentifierName(alterTable.SchemaName, alterTable.Name);

            if (alterTable.AddedColumns.Count > 0)
            {
                Text.Append(" ADD \r\n    ");
                VisitEach(", \r\n    ", alterTable.AddedColumns);
            }

            if (alterTable.Constraints.Count > 0)
            {
                Text.Append(" ADD \r\n    ");
                VisitEach(", \r\n    ", alterTable.Constraints);
            }
        }

        /// <summary>
        /// Formats a create column statement.
        /// </summary>
        /// <param name="createColumn">The create column to format.</param>
        public virtual void Visit (SqlCreateColumn createColumn) {
            if (createColumn == null)
                throw new ArgumentNullException("createColumn");

            createColumn.Expression.Accept(this);
            Text.Append(" ");
            createColumn.DataType.Accept(this);

            if (createColumn.Sparse)
                Text.Append(" SPARSE");

            if (createColumn.AutoIncrements) {
                Text.Append(" ");
                GenerateAutoIncrement(createColumn);
            }

            if (createColumn.DefaultExpression != null) {
                Text.Append(" DEFAULT ");
                createColumn.DefaultExpression.Accept(this);
            }

            Text.Append(createColumn.Nullable ? " NULL" : " NOT NULL");
        }

        /// <summary>
        /// Formats a primary key constraint.
        /// </summary>
        /// <param name="primaryKey">The primary key constraint to format.</param>
        public virtual void Visit (SqlPrimaryKeyConstraint primaryKey) {
            Text.Append("CONSTRAINT ");
            Visit(primaryKey.Name);
            Text.Append(" PRIMARY KEY(");
            Text.Append(string.Join(", ", primaryKey.Columns.Select(c => c.Expression.ToString()).ToArray()));
            Text.Append(")");
        }

        /// <summary>
        /// Formats a foreign key constraint.
        /// </summary>
        /// <param name="foreignKey">The foreign key constraint to format.</param>
        public virtual void Visit (SqlForeignKeyConstraint foreignKey) {
            Text.Append("CONSTRAINT ");
            Visit(foreignKey.Name);

            Text.Append(" FOREIGN KEY(");
            Visit(foreignKey.ForeignKeyColumn);
            Text.Append(") REFERENCES ");
            GenerateCompoundIdentifierName(foreignKey.PrimaryKeySchemaName, foreignKey.PrimaryKeyTableName);
            Text.Append(" (");
            Visit(foreignKey.PrimaryKeyColumn);
            Text.Append(")");
        }

        /// <summary>
        /// Formats a create sequence statement.
        /// </summary>
        /// <param name="createSequence">The create sequence to format.</param>
        public virtual void Visit(SqlCreateSequence createSequence)
        {
            if (createSequence == null)
                throw new ArgumentNullException("createSequence");

            Text.Append("CREATE SEQUENCE ");
            GenerateCompoundIdentifierName(createSequence.SchemaName, createSequence.Name);

            if (createSequence.Seed != null)
                Text.Append(" START WITH " + createSequence.Seed.Value);

            if (createSequence.Step != null)
                Text.Append(" INCREMENT BY " + createSequence.Step.Value);

            if (createSequence.MaxValue == int.MaxValue)
                Text.Append(" NOMAXVALUE");
            else if (createSequence.MaxValue != null)
                Text.Append(" MAXVALUE " + createSequence.MaxValue.Value);
        }
        #endregion

        #region DataType and Declaration Visitation
        public virtual void Visit (SqlDeclaration declaration) {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Formats a boolean data type.
        /// </summary>
        /// <param name="boolean">The boolean to format.</param>
        public virtual void Visit (SqlBoolean boolean) {
            Text.Append("BIT(1)");
        }

        /// <summary>
        /// Formats a decimal data type.
        /// </summary>
        /// <param name="decimalValue">The decimal to format.</param>
        public virtual void Visit (SqlDecimal decimalValue) {
            if (decimalValue.Scale == 0 && decimalValue.Precision == 0)
                Text.Append("DECIMAL(18, 4)");
            else {
                int precision = decimalValue.Precision == 0 ? 18 : decimalValue.Precision;
                Text.AppendFormat(CultureInfo.InvariantCulture, "DECIMAL({0}, {1})", precision, decimalValue.Scale);
            }
        }

        /// <summary>
        /// Formats a float data type.
        /// </summary>
        /// <param name="floatValue">The float to format.</param>
        public virtual void Visit (SqlFloat floatValue) {
            if (floatValue.Bits == 0)
                Text.Append("FLOAT");
            else
                Text.AppendFormat(CultureInfo.InvariantCulture, "FLOAT({0})", floatValue.Bits);
        }

        /// <summary>
        /// Formats an 8 bit integer data type.
        /// </summary>
        /// <param name="byteValue">The 8 bit integer to format.</param>
        public virtual void Visit(SqlByte byteValue)
        {
            Text.Append("TINYINT");
        }

        /// <summary>
        /// Formats a 16 bit integer data type.
        /// </summary>
        /// <param name="int16">The 16 bit integer to format.</param>
        public virtual void Visit (SqlInt16 int16) {
            Text.Append("SMALLINT");
        }

        /// <summary>
        /// Formats a 32 bit integer data type.
        /// </summary>
        /// <param name="int32">The 32 bit integer to format.</param>
        public virtual void Visit (SqlInt32 int32) {
            Text.Append("INTEGER");
        }

        /// <summary>
        /// Formats a 64 bit integer data type.
        /// </summary>
        /// <param name="int64">The 64 bit integer to format.</param>
        public virtual void Visit (SqlInt64 int64) {
            Text.Append("INTEGER");
        }

        /// <summary>
        /// Formats a globally unique identifier data type.
        /// </summary>
        /// <param name="guid">The globally unique identifier to format.</param>
        public virtual void Visit (SqlGuid guid) {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Formats a variable-length string data type.
        /// </summary>
        /// <param name="str">The variable-length string to format.</param>
        public virtual void Visit (SqlVariableLengthString str) {
            if (str.Length == int.MaxValue)
                Text.Append("VARCHAR(MAX)");
            else
                Text.AppendFormat(CultureInfo.InvariantCulture, "VARCHAR({0})", str.Length);
        }

        /// <summary>
        /// Formats a variable-length Unicode string data type.
        /// </summary>
        /// <param name="str">The variable-length Unicode string data type to format.</param>
        public virtual void Visit (SqlUnicodeVariableLengthString str) {
            if (str.Length == int.MaxValue)
                Text.Append("NVARCHAR(MAX)");
            else
                Text.AppendFormat(CultureInfo.InvariantCulture, "NVARCHAR({0})", str.Length);
        }

        /// <summary>
        /// Formats a date and time data type.
        /// </summary>
        /// <param name="dateTime">The date and time data type to format.</param>
        public virtual void Visit (SqlDateTime dateTime) {
            Text.Append("TIMESTAMP");
        }

        /// <summary>
        /// Formats a variable-length binary data type.
        /// </summary>
        /// <param name="binary">The variable-length binary data type to format.</param>
        public virtual void Visit (SqlVariableLengthBinary binary) {
            if (binary.Length == int.MaxValue)
                Text.Append("BIT VARYING");
            else
                Text.AppendFormat(CultureInfo.InvariantCulture, "BIT VARYING({0})", binary.Length);
        }

        /// <summary>
        /// Formats a money data type.
        /// </summary>
        /// <param name="money">The money data type to format.</param>
        public virtual void Visit (SqlMoney money) {
            Text.Append("DECIMAL(18, 6)");
        }

		/// <summary>
		/// Formats a time data type.
		/// </summary>
		/// <param name="time">The time data type to format.</param>
		public virtual void Visit (SqlTime time) {
			Text.Append("TIME");
		}
        #endregion

        #region Fragment Visitation
        /// <summary>
        /// Formats a join.
        /// </summary>
        /// <param name="join">The join to format.</param>
        public virtual void Visit (SqlJoin join) {
            if (join == null)
                throw new ArgumentNullException("join");

            switch (join.JoinType) {
                case SqlJoinType.Cross:
                    Text.Append(" CROSS JOIN ");
                    break;

                case SqlJoinType.FullOuter:
                    Text.Append(" FULL OUTER JOIN ");
                    break;

                case SqlJoinType.Inner:
                    Text.Append(" INNER JOIN ");
                    break;

                case SqlJoinType.LeftOuter:
                    Text.Append(" LEFT OUTER JOIN ");
                    break;

                case SqlJoinType.RightOuter:
                    Text.Append(" RIGHT OUTER JOIN ");
                    break;
            }

            Visit(join.Table);

            Text.Append(" ON ");

            join.On.Accept(this);
        }

        /// <summary>
        /// Formats a table.
        /// </summary>
        /// <param name="table">The table to format.</param>
        public virtual void Visit (SqlTable table) {
            Visit((SqlAliasedQueryExpression) table);
        }

        /// <summary>
        /// Formats a column.
        /// </summary>
        /// <param name="column">The column to format.</param>
        public virtual void Visit (SqlColumn column) {
            Visit((SqlAliasedQueryExpression) column);
        }

        /// <summary>
        /// Formats an order specifier.
        /// </summary>
        /// <param name="order">The order specified to format.</param>
        public virtual void Visit (SqlOrder order) {
            if (order == null)
                throw new ArgumentNullException("order");

            Enclose(order.Expression, false);

            switch (order.Direction) {
                case SqlOrderDirection.Ascending:
                    Text.Append(" ASC");
                    break;

                case SqlOrderDirection.Descending:
                    Text.Append(" DESC");
                    break;
            }
        }

        /// <summary>
        /// Formats an identifier.
        /// </summary>
        /// <param name="identifier">The identifier to format.</param>
        public virtual void Visit (SqlIdentifier identifier) {
            if (identifier == null)
                throw new ArgumentNullException("identifier");

            var value = string.Join(".", identifier.Parts.Select(FormatIdentifier));

            if (identifier.IsQuoted)
                Text.Append("'" + value + "'");
            else
                Text.Append(value);
        }

        /// <summary>
        /// Formats a parameter.
        /// </summary>
        /// <param name="parameter">The parameter to format.</param>
        public virtual void Visit (SqlParameter parameter) {
            IDbDataParameter param;
            if (Parameters.ContainsKey(parameter.Name))
                param = Parameters[parameter.Name];
            else {
                param = Command.CreateParameter();
                param.ParameterName = FormatParameterName(parameter.Name);
                param.DbType = GetDbType(parameter.DbType);
                param.Precision = parameter.Precision;
                param.Scale = parameter.Scale;
                param.Size = parameter.Size;

                Command.Parameters.Add(param);
                Parameters[parameter.Name] = param;
            }

            Text.Append(param.ParameterName);
        }

        /// <summary>
        /// Formats a literal.
        /// </summary>
        /// <param name="literal">The literal to format.</param>
        public virtual void Visit (SqlLiteralExpression literal) {
            // Treat non-enumerables, strings, and byte arrays as scalars.
            if (!(literal.Value is IEnumerable) || literal.Value is string || literal.Value is byte[]) {
                var formattedValue = FormatValue(literal.Value);
                AppendLiteralValue(formattedValue, literal.DbType);
                return;
            }

            // Treat all others as a sequence of values.
            var sequence = ((IEnumerable) literal.Value).Cast<object>().ToArray();
            if (sequence.Length == 0) {
                // Treat empty enumerables as NULLs.
                Text.Append("NULL");
            } else {
                bool begin = true;
                foreach (var value in (IEnumerable) literal.Value) {
                    if (begin)
                        begin = false;
                    else
                        Text.Append(", ");

                    var formattedValue = FormatValue(value);
                    AppendLiteralValue(formattedValue, literal.DbType);
                }
            }
        }

        private void AppendLiteralValue (object value, DbType? dbType) {
            if (EmbedLiterals)
                AppendEmbeddedLiteralValue(value, dbType);
            else
                AppendParameterLiteralValue(value, dbType);
        }

        private void AppendEmbeddedLiteralValue (object value, DbType? dbType) {
            string escapedValue;
            if (value == null)
                escapedValue = "NULL";
            else
                escapedValue = "'" + Escape(value.ToString()) + "'";

            Text.Append(escapedValue);
        }

        private void AppendParameterLiteralValue (object value, DbType? dbType) {
            var parameter = GetParameter(value, dbType);
            Text.Append(parameter.ParameterName);
        }

        /// <summary>
        /// Formats an aliased query.
        /// </summary>
        /// <param name="aliasedQuery">The aliased query to format.</param>
        public virtual void Visit (SqlAliasedQueryExpression aliasedQuery) {
            if (aliasedQuery == null)
                throw new ArgumentNullException("aliasedQuery");

            Enclose(aliasedQuery.Expression, false);

            if (!string.IsNullOrEmpty(aliasedQuery.Alias))
                Text.AppendFormat(" {0}", aliasedQuery.Alias);
        }

        /// <summary>
        /// Formats a function.
        /// </summary>
        /// <param name="function">The function to format.</param>
        public virtual void Visit (SqlFunction function) {
            if (function == null)
                throw new ArgumentNullException("function");

            // Special case: Count() with no arguments should be Count(*).
            if (function.FunctionName == "Count" && (function.FunctionArguments == null || function.FunctionArguments.Count == 0)) {
                Text.Append("Count(*)");
                return;
            }

            if (function.FunctionName == "Average") {
                // Average is abbreviated.
                Text.Append("Avg");
            } else {
                // Other cases use the given name.
                Text.Append(function.FunctionName);
            }

            if (!function.IsScalar)
            {
                Text.Append("(");

                if (function.FunctionArguments != null)
                    VisitEach(", ", function.FunctionArguments);

                Text.Append(")");
            }
        }
        #endregion

        #region Fragment Generation
        /// <summary>
        /// Generates an order by clause for a select.
        /// </summary>
        /// <param name="sqlSelect">The select this order by clause is for.</param>
        protected virtual void GenerateOrderBy (SqlSelect sqlSelect) {
            if (sqlSelect.OrderBy.Count > 0) {
                Text.Append(" ORDER BY ");
                VisitEach(", ", sqlSelect.OrderBy);
            }
        }

        /// <summary>
        /// Generates an auto increment fragment.
        /// </summary>
        protected virtual void GenerateAutoIncrement (SqlCreateColumn column) {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Generates a table name fragment.
        /// </summary>
        /// <param name="schemaName">The schema identifier.</param>
        /// <param name="name">The specific identifier.</param>
        protected virtual void GenerateCompoundIdentifierName (SqlIdentifier schemaName, SqlIdentifier name) {
            if (schemaName != null) {
                Visit(schemaName);
                Text.Append(".");
            }

            Visit(name);
        }
        #endregion

        #region Miscellaneous
        /// <summary>
        /// Encloses expression if it is atomic.
        /// </summary>
        /// <param name="expression">The expression to enclose.</param>
        protected virtual void Enclose (SqlExpression expression, bool alwaysEnclose) {
            if (expression.IsAtomic && !alwaysEnclose)
                expression.Accept(this);
            else {
                Text.Append("(");
                expression.Accept(this);
                Text.Append(")");
            }
        }

        /// <summary>
        /// Escapes enclosure characters in the provided string.
        /// </summary>
        /// <param name="value">The string to escape.</param>
        /// <returns>An escaped, safe string.</returns>
        public virtual string Escape (string value) {
            return value == null ? null : value.Replace("'", "''");
        }

        /// <summary>
        /// Gets the value used for data operations from a given value.
        /// </summary>
        /// <param name="value">The value to get a data value for.</param>
        /// <returns>A value that is safe for data operations.</returns>
        protected static object GetDataValue (object value) {
            if (value == null)
                return DBNull.Value;

            return value;
        }

        /// <summary>
        /// Escapes an identifier if necessary.
        /// </summary>
        /// <param name="identifier">The identifier to format.</param>
        /// <returns>A formatted identifier.</returns>
        public virtual string FormatIdentifier (string identifier) {
            if (IdentifierRequiresEscaping(identifier))
                return "\"" + identifier + "\"";
            else
                return identifier;
        }

        public string FormatDataType (SqlDataType dataType) {
            Command = null;
            return GenerateText(() => dataType.Accept(this));
        }

        /// <summary>
        /// Determines if the identifier requires escaping.
        /// </summary>
        /// <param name="identifier">The identifier to check.</param>
        /// <returns>true if the identifier requires escaping, false if otherwise.</returns>
        protected virtual bool IdentifierRequiresEscaping (string identifier) {
            if (IsKeyword(identifier))
                return true;

            // Special case for column *
            if (identifier == "*")
                return false;

            foreach (var c in identifier) {
                if (char.IsLetterOrDigit(c) || c == '_')
                    continue;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the identifier is a keyword.
        /// </summary>
        /// <param name="identifier">The possible keyword identifier.</param>
        /// <returns>true if the identifier is a keyword, false if otherwise.</returns>
        protected virtual bool IsKeyword (string identifier) {
            switch (identifier.ToLower())
            {
                case "order":
                case "key":
                case "transaction":
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Foramts each item in the list by visiting them, and appends the
        /// given separator between visits.
        /// </summary>
        /// <param name="separator">The separator to append between visits.</param>
        /// <param name="list">The list to visit.</param>
        protected void VisitEach (string separator, IEnumerable<SqlExpression> list) {
            var begin = true;
            foreach (var expr in list) {
                if (begin)
                    begin = false;
                else
                    Text.Append(separator);

                expr.Accept(this);
            }
        }

        protected virtual IDbDataParameter GetParameter (object value, DbType? dbType) {
            if (value == null || value == DBNull.Value)
                return CreateParameter(null, dbType);

            // Search the parameter list for an equivalent.  Share that
            // parameter if found.
            if (Parameters.ContainsKey(value))
                return Parameters[value];

            // No equivalent found.  Cache the value.
            var parameter = CreateParameter(value, dbType);
            Parameters[value] = parameter;

            return parameter;
        }

        public virtual string FormatParameterName(string name)
        {
            return "@" + name;
        }

        protected virtual IDbDataParameter CreateParameter (object value, DbType? dbType) {
            var dataValue = GetDataValue(value);

            _parameterCounter++;
            var parameter = Command.CreateParameter();
            parameter.ParameterName = FormatParameterName(_parameterCounter.ToString());

            if (dbType.HasValue)
                parameter.DbType = GetDbType(dbType.Value);
            else if (value != null) {
                var valueDbType = GetDbType(value.GetType());
                if (valueDbType != DbType.Object)
                    parameter.DbType = valueDbType;
            }

            parameter.Value = dataValue;
            Command.Parameters.Add(parameter);
            return parameter;
        }

        public virtual object FormatValue(object value)
        {
            return value;
        }

        protected virtual DbType GetDbType (DbType type) {
            return type;
        }

        private DbType GetDbType (Type type) {
            return GetDbType(DbTypeEx.GetDbType(type));
        }
        #endregion

        #region Command Printing
        public virtual string PrintCommand (IDbCommand command) {
            if (command == null)
                throw new ArgumentNullException("command");

            var message = new StringBuilder();
            foreach (IDataParameter param in command.Parameters)
                message.AppendFormat("{0}{1}", PrintParameter(param), Environment.NewLine);
            message.Append(command.CommandText);
            return message.ToString();
        }

        protected virtual string PrintParameter (IDataParameter param) {
            var type = SqlDataType.ForApproximateDbType(param.DbType);
            var formattedType = FormatDataType(type);

            if (param.Value == DBNull.Value || param.Value == null)
                return string.Format(CultureInfo.InvariantCulture, "DECLARE {0} {1} = NULL", param.ParameterName, formattedType);
            else {
                var valueType = param.Value.GetType();
                if (valueType.IsEnum)
                    return string.Format(CultureInfo.InvariantCulture, "DECLARE {0} {1} = {2} -- {3}", param.ParameterName, formattedType, Convert.ToInt32(param.Value), param.Value);
                else
                    return string.Format(CultureInfo.InvariantCulture, "DECLARE {0} {1} = '{2}'", param.ParameterName, formattedType, param.Value);
            }

        }
        #endregion

        #region Accessors
        /// <summary>
        /// An indicator on whether literals are embedded in the SQL text or as parameters.
        /// </summary>
        public bool EmbedLiterals { get; set; }

        /// <summary>
        /// The current formatter text.
        /// </summary>
        protected StringBuilder Text { get; private set; }

        protected Dictionary<object, IDbDataParameter> Parameters { get; private set; }

        protected IDbCommand Command { get; private set; }
        #endregion
    }
}
