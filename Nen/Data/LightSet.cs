﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Nen.Internal;
using Nen.Collections;
using System.Data;

namespace Nen.Data
{
    public class LightSet
    {
        private Dictionary<string, LightTable> _tables = new Dictionary<string, LightTable>();
        private List<LightRelation> _relations = new List<LightRelation>();

        public void Merge(LightSet set)
        {
            foreach (var table in set.Tables)
            {
                LightTable destination;
                if (_tables.ContainsKey(table.Name))
                    destination = _tables[table.Name];
                else
                {
                    destination = new LightTable(table.Name);
                    AddTable(destination);
                }

                destination.Merge(table);
            }

            foreach (var relation in set._relations)
            {
                if (!ContainsRelation(relation.Parent, relation.Child))
                    AddRelation(relation.Parent.Table.Name, relation.Parent.Name, relation.Child.Table.Name, relation.Child.Name);
            }
        }

        public bool ContainsRelation(LightColumn parentColumn, LightColumn childColumn)
        {
            return _relations.Any(r => r.Parent.Name == parentColumn.Name
                && r.Parent.Table.Name == parentColumn.Table.Name
                && r.Child.Name == childColumn.Name
                && r.Child.Table.Name == childColumn.Table.Name);
        }

        public void AddRelation(string parentTableName, string parentColumnName, string childTableName, string childColumnName)
        {
            var parentTable = this[parentTableName];
            var parentColumn = parentTable[parentColumnName];

            var childTable = this[childTableName];
            var childColumn = childTable[childColumnName];

            var relation = new LightRelation(parentColumn, childColumn);
            _relations.Add(relation);
        }

        #region Tables
        public void AddTable(LightTable table)
        {
            if (_tables.ContainsKey(table.Name))
                throw new ArgumentException(LS.T("Table '{0}' already exists in the set", table.Name), "table");

            table.Set = this;
            _tables[table.Name] = table;
        }

        public bool RemoveTable(LightTable table)
        {
            table.Set = null;
            return _tables.Remove(table.Name);
        }

        public bool RemoveTable(string tableName)
        {
            if (_tables.ContainsKey(tableName))
                return RemoveTable(_tables[tableName]);
            return false;
        }

        public bool ContainsTable(string tableName)
        {
            return _tables.ContainsKey(tableName);
        }

        public IEnumerable<LightTable> Tables
        {
            get { return _tables.Values; }
        }

        public LightTable this[string tableName]
        {
            get { return _tables[tableName]; }
        }
        #endregion
    }

    public class LightTable
    {
        private Collection<LightRow> _rows = new Collection<LightRow>();
        private Dictionary<string, LightColumn> _columnsByName = new Dictionary<string, LightColumn>();
        private List<LightColumn> _columns = new List<LightColumn>();
        // todo: make indexes a list as well
        private Dictionary<string, Dictionary<object, List<LightRow>>> _indexes = new Dictionary<string, Dictionary<object, List<LightRow>>>();
        public LightSet Set { get; internal set; }
        public string Name { get; private set; }
        public string PrimaryKeyColumnName { get; private set; }
        public PropertyCollection ExtendedProperties { get; private set; }

        public LightTable(string name)
        {
            ExtendedProperties = new PropertyCollection();
            Name = name;
        }

        #region Column, Row
        public LightColumn AddColumn(string columnName)
        {
            if (_columnsByName.ContainsKey(columnName))
                throw new ArgumentException(LS.T("Column '{0}' already exists on table '{1}'", columnName, this), "columnName");

            var column = new LightColumn(columnName, _columns.Count, this);
            _columns.Add(column);
            _columnsByName[columnName] = column;
            return column;
        }

        public bool ContainsColumn(string columnName)
        {
            return _columnsByName.ContainsKey(columnName);
        }

        public LightRow CreateRow()
        {
            return new LightRow(this);
        }

        public void AddRow(LightRow row)
        {
            if (row.IsAddedToTable)
                throw new ArgumentException(LS.T("Row is already added to table '{0}'", this));

            row.IsAddedToTable = true;
            _rows.Add(row);
            UpdateIndexes(row);
        }

        public void AddRow(params object[] values)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            var row = CreateRow();
            for (int i = 0; i < values.Length; i++)
                row[_columns[i].Name] = values[i];
            AddRow(row);
        }

        public void RemoveRow(LightRow row)
        {
            if (row == null)
                throw new ArgumentNullException("row");

            if (!row.IsAddedToTable || row.Table != this)
                throw new ArgumentException(LS.T("Can't remove row from table '{0}' because it's already added to table '{1}'", this, row.Table));

            row.IsAddedToTable = false;
            _rows.Remove(row);

            foreach (var index in _indexes)
            {
                var value = row[index.Key];
                if (value != null)
                    index.Value[value].Remove(row);
            }
        }

        public LightRow this[int index]
        {
            get { return _rows[index]; }
        }

        public LightColumn this[string columnName]
        {
            get
            {
                LightColumn column;
                if (!_columnsByName.TryGetValue(columnName, out column))
                    throw new ArgumentException(LS.T("Invalid column '{0}' on table '{1}'", columnName, this), "columnName");
                return column;
            }
        }
        #endregion

        #region Indexes, Search
        internal void UpdateIndexes(LightRow row)
        {
            foreach (var index in _indexes)
            {
                var value = row[index.Key];
                if (value != null)
                    index.Value.Activate(value).Add(row);
            }
        }

        internal void UpdateIndexes(LightRow row, LightColumn column, object oldValue, object newValue)
        {
            if (!_indexes.ContainsKey(column.Name))
                return;

            var index = _indexes[column.Name];
            if (oldValue != null)
            {
                var oldList = index[oldValue];
                oldList.Remove(row);
            }

            index.Activate(newValue).Add(row);
        }

        public Dictionary<object, List<LightRow>> AddIndex(string columnName, bool isPrimaryKey = false)
        {
            var column = this[columnName];
            if (_indexes.ContainsKey(columnName))
                throw new ArgumentException(LS.T("Column '{0}' is already indexed on table '{1}'", columnName, this), "columnName");

            if (isPrimaryKey)
                PrimaryKeyColumnName = columnName;

            // Index all existing rows.
            var index = new Dictionary<object, List<LightRow>>();
            foreach (var row in _rows)
            {
                var value = row[columnName];
                if (value != null)
                    index.Activate(value).Add(row);
            }

            _indexes[columnName] = index;
            return index;
        }

        public LightRow FindFirstRow(string columnName, object value)
        {
            var rows = FindRows(columnName, value);
            return rows.Count == 0 ? null : rows[0];
        }

        public List<LightRow> FindRows(string columnName, object value)
        {
            if (value == null)
                throw new ArgumentException(LS.T("Can't search indexed column '{0}' on table '{1}' with null value", columnName, this), "value");

            // Index rows on the fly if no index exists yet.
            Dictionary<object, List<LightRow>> index;
            if (!_indexes.TryGetValue(columnName, out index))
                index = AddIndex(columnName);

            List<LightRow> rows;
            return index.TryGetValue(value, out rows) ? rows : new List<LightRow>();
        }

        public LightRow FindRowByPrimaryKey(object value)
        {
            if (PrimaryKeyColumnName == null)
                throw new InvalidOperationException(LS.T("Table '{0}' has no primary key defined", this));

            return FindFirstRow(PrimaryKeyColumnName, value);
        }
        #endregion

        public IEnumerable<object> GetPrimaryKeys()
        {
            Dictionary<object, List<LightRow>> index;
            if (_indexes.TryGetValue(PrimaryKeyColumnName, out index))
                return index.Keys;
            else
                return null;
        }

        public void Merge(LightTable table)
        {
            // Match all columns.
            if (_columns.Count > 0)
            {
                if (!_columns.All(c => table.ContainsColumn(c.Name)))
                    throw new ArgumentException(LS.T("Column mismatch merging table '{0}'", this));
            }
            else
            {
                for (var i = 0; i < table._columns.Count; i++)
                {
                    var sourceColumn = table._columns[i];
                    var column = AddColumn(sourceColumn.Name);
                    column.Tag = sourceColumn.Tag;
                }
            }

            // Add the primary key if it doesn't already exist.
            if (table.PrimaryKeyColumnName != null && PrimaryKeyColumnName == null)
                AddIndex(table.PrimaryKeyColumnName, true);
            else if (table.PrimaryKeyColumnName != PrimaryKeyColumnName)
                throw new ArgumentException(LS.T("Primary key mismatch between source '{0}' and destination '{1}' on table '{2}'", table.PrimaryKeyColumnName, this.PrimaryKeyColumnName, this));

            // Copy all indexes.
            foreach (var indexColumn in table._indexes.Keys)
            {
                if (!_indexes.ContainsKey(indexColumn))
                    AddIndex(indexColumn, false);
            }

            // Special case to simply add all rows when the table has no primary key.
            if (PrimaryKeyColumnName == null)
            {
                foreach (var row in table.Rows)
                {
                    var destination = CreateRow();
                    destination.Merge(row);
                    AddRow(destination);
                }
                return;
            }

            // Merge all rows by primary key.
            foreach (var row in table.Rows)
            {
                var primaryKeyValue = row[PrimaryKeyColumnName];
                var destination = FindRowByPrimaryKey(primaryKeyValue);
                if (destination == null)
                {
                    destination = CreateRow();
                    destination.Merge(row);
                    AddRow(destination);
                }
                else
                    destination.Merge(row);
            }
        }

        public LightTable Clone()
        {
            var copy = new LightTable(Name);
            copy.Merge(this);
            return copy;
        }

        public IEnumerable<LightRow> Rows
        {
            get { return _rows; }
        }

        internal List<LightColumn> ColumnsInternal
        {
            get { return _columns; }
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class LightRow
    {
        private string[] _errors;
        private object[] _values;
        public LightTable Table { get; private set; }
        public bool IsAddedToTable { get; internal set; }

        internal LightRow(LightTable table)
        {
            Table = table;
            _values = new object[table.ColumnsInternal.Count];
        }

        public void SetColumnError(string columnName, string error)
        {
            var column = Table[columnName];

            if (_errors == null)
                _errors = new string[Table.ColumnsInternal.Count];

            _errors[column.Ordinal] = error;
        }

        public string GetColumnError(string columnName)
        {
            var column = Table[columnName];

            if (_errors == null)
                return null;

            return _errors[column.Ordinal];
        }

        public void Merge(LightRow row)
        {
            row._values.CopyTo(_values, 0);
            Table.UpdateIndexes(this);
        }

        public object this[LightColumn column]
        {
            get { return _values[column.Ordinal]; }
            set
            {
                if (IsAddedToTable)
                {
                    var oldValue = _values[column.Ordinal];
                    if (Equals(oldValue, value))
                        return; // Nothing to do.

                    // Update the value store, indexes, and relations.
                    _values[column.Ordinal] = value;
                    Table.UpdateIndexes(this, column, oldValue, value);

                    if (column.ChildForeignKeys != null)
                    {
                        foreach (var child in column.ChildForeignKeys)
                        {
                            foreach (var matchingChildRow in child.Table.FindRows(child.Name, oldValue).ToArray())
                                matchingChildRow[child.Name] = value;
                        }
                    }


                }
                else
                    _values[column.Ordinal] = value;
            }
        }

        public object this[string columnName]
        {
            get
            {
                var column = Table[columnName];
                return _values[column.Ordinal];
            }
            set
            {
                var column = Table[columnName];
                this[column] = value;
            }
        }

        public override string ToString()
        {
            return string.Join(", ", Table.ColumnsInternal.Select(c => c.Name + ": " + (_values[c.Ordinal] ?? "(null)")));
        }
    }

    public class LightColumn
    {
        private List<LightColumn> _childForeignKeys;
        public string Name { get; private set; }
        public LightTable Table { get; private set; }
        public int Ordinal { get; private set; }
        public object Tag { get; set; }

        public LightColumn(string name, int ordinal, LightTable table)
        {
            Name = name;
            Ordinal = ordinal;
            Table = table;
        }

        internal void AddChildForeignKey(LightColumn column)
        {
            if (_childForeignKeys == null)
                _childForeignKeys = new List<LightColumn>();
            _childForeignKeys.Add(column);
        }

        internal IEnumerable<LightColumn> ChildForeignKeys
        {
            get { return _childForeignKeys; }
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class LightRelation
    {
        public LightColumn Parent { get; private set; }
        public LightColumn Child { get; private set; }

        internal LightRelation(LightColumn parent, LightColumn child)
        {
            Parent = parent;
            Child = child;

            parent.AddChildForeignKey(child);
        }
    }
}
