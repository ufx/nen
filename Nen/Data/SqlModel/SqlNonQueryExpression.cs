using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using System.Collections.ObjectModel;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Provides a base for non-query SQL expressions.
    /// </summary>
    public abstract class SqlNonQueryExpression : SqlExpression {
        #region Accessors
        /// <summary>
        /// The table the expression applies to.
        /// </summary>
        public SqlIdentifier Table { get; set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL delete.
    /// </summary>
    public class SqlDelete : SqlNonQueryExpression {
        #region Accessors
        /// <summary>
        /// The delete criteria.
        /// </summary>
        public SqlOperatorExpression Where { get; set; }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlDelete).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the delete.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            var str = new StringBuilder();

            str.AppendFormat(CultureInfo.InvariantCulture, "DELETE FROM {0}", Table);

            if (Where != null)
                str.AppendFormat(CultureInfo.InvariantCulture, " WHERE {0}", Where);

            return str.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Provides a base for column/value detail non-query expressions.
    /// </summary>
    public abstract class SqlDetailNonQueryExpression : SqlNonQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlDetailNonQueryExpression.
        /// </summary>
        protected SqlDetailNonQueryExpression () {
            Columns = new List<SqlIdentifier>();
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The columns to change.
        /// </summary>
        public ICollection<SqlIdentifier> Columns { get; private set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL update.
    /// </summary>
    public class SqlUpdate : SqlDetailNonQueryExpression {
        public SqlUpdate () {
            Values = new List<SqlQueryExpression>();
        }

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlUpdate).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        public void Set(SqlIdentifier column, SqlQueryExpression value)
        {
            Columns.Add(column);
            Values.Add(value);
        }

        #region Accessors
        /// <summary>
        /// The update criteria.
        /// </summary>
        public SqlOperatorExpression Where { get; set; }

        /// <summary>
        /// The new values of the columns.
        /// </summary>
        public ICollection<SqlQueryExpression> Values { get; private set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the update.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            var str = new StringBuilder();

            str.AppendFormat(CultureInfo.InvariantCulture, "UPDATE {0}", Table);

            if (Columns.Count > 0)
                str.AppendFormat(CultureInfo.InvariantCulture, " SET {0}", string.Join(", ", Columns.ToStrings().ToArray()));

            if (Values.Count > 0)
                str.AppendFormat(CultureInfo.InvariantCulture, " VALUES({0})", string.Join(", ", Values.ToStrings().ToArray()));

            if (Where != null)
                str.AppendFormat(CultureInfo.InvariantCulture, " WHERE {0}", Where);

            return str.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL insert.
    /// </summary>
    public class SqlInsert : SqlDetailNonQueryExpression {
        public SqlInsert () {
            Values = new Collection<SqlQueryExpression>();
        }

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlInsert).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        public Collection<SqlQueryExpression> Values { get; private set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the insert.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            var str = new StringBuilder();

            str.AppendFormat(CultureInfo.InvariantCulture, "INSERT INTO {0}", Table);

            var assignments = new List<string>();
            for (var i = 0; i < Values.Count && i < Columns.Count; i++)
                assignments.Add(string.Format(CultureInfo.InvariantCulture, "{0} = {1}", Values.ElementAt(i), Columns.ElementAt(i)));

            if (assignments.Count > 0)
                str.Append(string.Join(", ", assignments.ToArray()));

            return str.ToString();
        }
        #endregion
    }
}
