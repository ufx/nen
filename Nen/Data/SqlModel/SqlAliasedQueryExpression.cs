using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Provides a base for query SQL expressions that are aliasable.
    /// </summary>
    public abstract class SqlAliasedQueryExpression : SqlQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlAliasedQueryExpression.
        /// </summary>
        protected SqlAliasedQueryExpression () {
        }

        /// <summary>
        /// Constructs a new SqlAliasedQueryExpression.
        /// </summary>
        /// <param name="expression">The expression to alias.</param>
        protected SqlAliasedQueryExpression (SqlQueryExpression expression) {
            Expression = expression;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The alias of the expression.
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// The expression to alias.
        /// </summary>
        public SqlQueryExpression Expression { get; set; }
        #endregion

        #region SqlExpression Atomicity
        /// <summary>
        /// Retrieves the atomicity of the aliased expression.
        /// </summary>
        public override bool IsAtomic {
            get { return Expression.IsAtomic; }
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the expression.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            if (string.IsNullOrEmpty(Alias))
                return Expression.ToString();
            else
                return string.Format(CultureInfo.InvariantCulture, "{0} AS {1}", Expression, Alias);
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL table.
    /// </summary>
    public class SqlTable : SqlAliasedQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlTable.
        /// </summary>
        public SqlTable () {
            WithHints = new Collection<SqlIdentifier>();
        }

        /// <summary>
        /// Constructs a new SqlTable.
        /// </summary>
        /// <param name="expression">The table expression.</param>
        public SqlTable (SqlQueryExpression expression)
            : base(expression) {
            WithHints = new Collection<SqlIdentifier>();
        }

        /// <summary>
        /// Constructs a new SqlTable.
        /// </summary>
        /// <param name="tableNameParts">The parts of the table identifier.</param>
        public SqlTable (params string[] tableNameParts)
            : this(new SqlIdentifier(tableNameParts)) { }

        public SqlTable (string tableNamePart)
            : this(new SqlIdentifier(tableNamePart)) { }

        public SqlTable (string tableNamePart1, string tableNamePart2)
            : this(new SqlIdentifier(tableNamePart1, tableNamePart2)) { }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlTable).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        public ICollection<SqlIdentifier> WithHints { get; private set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL column.
    /// </summary>
    public class SqlColumn : SqlAliasedQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlColumn.
        /// </summary>
        public SqlColumn () {
        }

        /// <summary>
        /// Constructs a new SqlColumn.
        /// </summary>
        /// <param name="expression">The column expression.</param>
        public SqlColumn (SqlQueryExpression expression)
            : base(expression) {
        }

        /// <summary>
        /// Constructs a new SqlColumn.
        /// </summary>
        /// <param name="columnNameParts">The parts of the column identifier.</param>
        public SqlColumn (params string[] columnNameParts)
            : base(new SqlIdentifier(columnNameParts)) {
        }

        public SqlColumn (string columnNamePart)
            : base(new SqlIdentifier(columnNamePart)) {
        }

        public SqlColumn (string columnNamePart1, string columnNamePart2)
            : base(new SqlIdentifier(columnNamePart1, columnNamePart2)) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlColumn).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion
    }
}
