﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Represents a SQL constraint.
    /// </summary>
    public abstract class SqlConstraint : SqlExpression {
        #region SqlExpression Atomicity
        /// <summary>
        /// Retrusn true, as a constraint is always atomic.
        /// </summary>
        public override bool IsAtomic {
            get { return true; }
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the name of the constraint.
        /// </summary>
        public SqlIdentifier Name { get; set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL primary key constraint.
    /// </summary>
    public class SqlPrimaryKeyConstraint : SqlConstraint {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlPrimaryKeyConstraint.
        /// </summary>
        public SqlPrimaryKeyConstraint () {
            Columns = new List<SqlColumn>();
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlPrimaryKeyConstraint).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the collection of columns that make up the primary key constraint.
        /// </summary>
        public ICollection<SqlColumn> Columns { get; private set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL foreign key constraint.
    /// </summary>
    public class SqlForeignKeyConstraint : SqlConstraint {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlForeignKeyConstraint).
        /// </summary>
        /// <param name="visitor"></param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the primary key column.
        /// </summary>
        public SqlColumn PrimaryKeyColumn { get; set; }

        /// <summary>
        /// Gets or sets the schema name of the primary key.
        /// </summary>
        public SqlIdentifier PrimaryKeySchemaName { get; set; }

        /// <summary>
        /// Gets or sets the table name of the primary key.
        /// </summary>
        public SqlIdentifier PrimaryKeyTableName { get; set; }

        /// <summary>
        /// Gets or sets the foreign key column.
        /// </summary>
        public SqlColumn ForeignKeyColumn { get; set; }
        #endregion
    }
}
