using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Represents a SQL join.
    /// </summary>
    public class SqlJoin : SqlExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlJoin.
        /// </summary>
        public SqlJoin () {
        }

        /// <summary>
        /// Constructs a new SqlJoin.
        /// </summary>
        /// <param name="table">The table to join.</param>
        /// <param name="on">The expression to join on.</param>
        public SqlJoin (SqlTable table, SqlOperatorExpression on) {
            Table = table;
            On = on;
        }

        /// <summary>
        /// Constructs a new SqlJoin.
        /// </summary>
        /// <param name="table">The table to join.</param>
        /// <param name="on">The expression to join on.</param>
        /// <param name="joinType">The type of join.</param>
        public SqlJoin (SqlTable table, SqlOperatorExpression on, SqlJoinType joinType)
            : this(table, on) {
            JoinType = joinType;
        }

        /// <summary>
        /// Constructs a new SqlJoin.
        /// </summary>
        /// <param name="tableName">The table name to join.</param>
        /// <param name="on">The expression to join on.</param>
        public SqlJoin (string tableName, SqlOperatorExpression on)
            : this(new SqlTable(tableName), on) {
        }

        /// <summary>
        /// Constructs a new SqlJoin.
        /// </summary>
        /// <param name="tableName">The table name to join.</param>
        /// <param name="on">The expression to join on.</param>
        /// <param name="joinType">The type of join.</param>
        public SqlJoin (string tableName, SqlOperatorExpression on, SqlJoinType joinType)
            : this(new SqlTable(tableName), on, joinType) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlJoin).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The type of join.
        /// </summary>
        public SqlJoinType JoinType { get; set; }

        /// <summary>
        /// The expression to join on.
        /// </summary>
        public SqlOperatorExpression On { get; set; }

        /// <summary>
        /// The table to join.
        /// </summary>
        public SqlTable Table { get; set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the join.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            var str = new StringBuilder();

            str.AppendFormat("{0} JOIN {1}", JoinType, Table);

            if (On != null)
                str.AppendFormat(CultureInfo.InvariantCulture, " ON {0}", On);

            return str.ToString();
        }
        #endregion
    }

    /// <summary>
    /// The type of SQL join.
    /// </summary>
    public enum SqlJoinType {
        /// <summary>
        /// An inner join.
        /// </summary>
        Inner = 0,
        
        /// <summary>
        /// A cross join.
        /// </summary>
        Cross,

        /// <summary>
        /// A left outer join.
        /// </summary>
        LeftOuter,

        /// <summary>
        /// A right outer join.
        /// </summary>
        RightOuter,

        /// <summary>
        /// A full outer join.
        /// </summary>
        FullOuter
    }
}
