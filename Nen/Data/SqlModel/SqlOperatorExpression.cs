using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Provides a base for SQL operator expressions.
    /// </summary>
    public abstract class SqlOperatorExpression : SqlExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlOperatorExpression.
        /// </summary>
        protected SqlOperatorExpression () {
        }

        /// <summary>
        /// Constructs a new SqlOperatorExpression.
        /// </summary>
        /// <param name="operatorValue">The operator to use.</param>
        protected SqlOperatorExpression (string operatorValue) {
            Operator = operatorValue;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The operator to use.
        /// </summary>
        public string Operator { get; set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL binary operator expression.
    /// </summary>
    public class SqlBinaryOperatorExpression : SqlOperatorExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlBinaryOperatorExpression.
        /// </summary>
        public SqlBinaryOperatorExpression () {
        }

        /// <summary>
        /// Constructs a new SqlBinaryOperatorExpression.
        /// </summary>
        /// <param name="left">The left hand side of the expression.</param>
        /// <param name="operatorValue">The operator to use.</param>
        /// <param name="right">The right hand side of the expression.</param>
        public SqlBinaryOperatorExpression (SqlExpression left, string operatorValue, SqlExpression right)
            : base(operatorValue) {
            Left = left;
            Right = right;
        }
        #endregion

        #region Operator Joining
        /// <summary>
        /// Joins one or two expressions with a binary operator.
        /// </summary>
        /// <param name="left">The left hand side of the expression.</param>
        /// <param name="operatorValue">The operator to use.</param>
        /// <param name="right">The right hand side of the expression.</param>
        /// <returns>The right expression if there was no left, and a joined expression otherwise.</returns>
        public static SqlBinaryOperatorExpression Join (SqlExpression left, string operatorValue, SqlBinaryOperatorExpression right) {
            if (left == null)
                return right;
            else
                return new SqlBinaryOperatorExpression(left, operatorValue, right);
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlBinaryOperatorExpression).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The left hand side of the expression.
        /// </summary>
        public SqlExpression Left { get; internal set; }

        /// <summary>
        /// The right hand side of the expression.
        /// </summary>
        public SqlExpression Right { get; internal set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the binary operator expression.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "{0} {1} {2}", Left, Operator, Right);
        }
        #endregion
    }

    /// <summary>
    /// Provides a base for SQL unary operator expressions.
    /// </summary>
    public abstract class SqlUnaryOperatorExpression : SqlOperatorExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlUnaryOperatorExpression.
        /// </summary>
        protected SqlUnaryOperatorExpression () {
        }
        
        /// <summary>
        /// Constructs a new SqlUnaryOperatorExpression.
        /// </summary>
        /// <param name="operatorValue">The operator to use.</param>
        /// <param name="operand">The operand expression.</param>
        protected SqlUnaryOperatorExpression (string operatorValue, SqlExpression operand)
            : base(operatorValue) {
            Operand = operand;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The operand expression.
        /// </summary>
        public SqlExpression Operand { get; internal set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL prefix operator expression.
    /// </summary>
    public class SqlPrefixOperatorExpression : SqlUnaryOperatorExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlPrefixOperatorExpression.
        /// </summary>
        public SqlPrefixOperatorExpression () {
        }

        /// <summary>
        /// Constructs a new SqlPrefixOperatorExpression.
        /// </summary>
        /// <param name="operatorValue">The operator to use.</param>
        /// <param name="operand">The operand expression.</param>
        public SqlPrefixOperatorExpression (string operatorValue, SqlExpression operand)
            : base(operatorValue, operand) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlPrefixOperatorExpression).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the prefix operator expression.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "{0} {1}", Operator, Operand);
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL postfix operator expression.
    /// </summary>
    public class SqlPostfixOperatorExpression : SqlUnaryOperatorExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlPostfixOperatorExpression.
        /// </summary>
        public SqlPostfixOperatorExpression () {
        }

        /// <summary>
        /// Constructs a new SqlPostfixOperatorExpression.
        /// </summary>
        /// <param name="operatorValue">The operator to use.</param>
        /// <param name="operand">The operand expression.</param>
        public SqlPostfixOperatorExpression (string operatorValue, SqlExpression operand)
            : base(operatorValue, operand) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlPostfixOperatorExpression).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the postfix operator expression.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "{0} {1}", Operand, Operator);
        }
        #endregion
    }
}
