﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Data.SqlModel {
    public class SqlDeclaration : SqlExpression {
        #region Constructors
        public SqlDeclaration () {
        }

        public SqlDeclaration (string variableName, SqlDataType dataType) {
            DataType = dataType;
            Variable = new SqlIdentifier(variableName);
        }
        #endregion

        #region SqlExpression Members
        public override bool IsAtomic {
            get { return true; }
        }

        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        public SqlDataType DataType { get; set; }
        public SqlIdentifier Variable { get; set; }
        #endregion

        public override string ToString () {
            return string.Format("DECLARE {0} {1}", Variable, DataType);
        }
    }

    public class SqlSet : SqlExpression {
        #region Constructors
        public SqlSet () {
        }

        public SqlSet (SqlIdentifier variable, SqlQueryExpression value) {
            Variable = variable;
            Value = value;
        }
        #endregion

        #region SqlExpression Members
        public override bool IsAtomic {
            get { return true; }
        }

        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        public SqlIdentifier Variable { get; set; }
        public SqlQueryExpression Value { get; set; }
        #endregion

        public override string ToString () {
            return string.Format("SET {0} = {1}", Variable, Value);
        }
    }
}
