﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.Globalization;
using Nen.Internal;
using Nen.Validation;
using Nen.Reflection;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Represents a SQL data type.
    /// </summary>
    public abstract class SqlDataType : SqlExpression {
        #region SqlExpression Atomicity
        /// <summary>
        /// Returns true, as a data type is always atomic.
        /// </summary>
        public override bool IsAtomic {
            get { return true; }
        }
        #endregion

        #region Factories
        private static SqlDataType ForSimpleDbType (DbType dbType) {
            switch (dbType) {
                case DbType.Byte: return new SqlByte();
                case DbType.Int16: return new SqlInt16();
                case DbType.Int32: return new SqlInt32();
                case DbType.Int64: return new SqlInt64();
                case DbType.DateTime2:
                case DbType.DateTime: return new SqlDateTime();
                case DbType.Guid: return new SqlGuid();
                case DbType.Boolean: return new SqlBoolean();
                case DbType.Currency: return new SqlMoney();
            }

            return null;
        }

        /// <summary>
        /// Creates a SqlDataType for the approximate dbType given.  This method
        /// is only a helper used for printing parameters and should not be used
        /// when precise information is needed.
        /// </summary>
        /// <param name="dbType">The approximate dbType to use.</param>
        /// <returns>The approximate SqlDataType analog to the given dbType.</returns>
        public static SqlDataType ForApproximateDbType (DbType dbType) {
            var simpleType = ForSimpleDbType(dbType);
            if (simpleType != null)
                return simpleType;

            switch (dbType) {
                case DbType.String:
                case DbType.StringFixedLength:
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                    return new SqlVariableLengthString(int.MaxValue);

                case DbType.Decimal:
                case DbType.Single:
                    return new SqlDecimal(19, 7);

                case DbType.Binary:
                    return new SqlVariableLengthBinary(int.MaxValue);

                case DbType.Time:
                case DbType.DateTimeOffset:
                    return new SqlTime();

                default:
                    return new SqlVariableLengthString(int.MaxValue);
            }
        }

        /// <summary>
        /// Creates a SqlDataType for a DataColumn.
        /// </summary>
        /// <param name="column">The column to create a SqlDataType for.</param>
        /// <returns>A type containing all of the information contained in the column.</returns>
        public static SqlDataType ForDataColumn (DataColumn column) {
            var dbType = column.GetDbType();
            if (dbType == null)
                dbType = DbTypeEx.GetDbType(column.DataType);

            var simpleType = ForSimpleDbType(dbType.Value);
            if (simpleType != null)
                return simpleType;

            if (column.DataType == typeof(string)) {
                if (column.MaxLength <= 0)
                    throw new ArgumentException(LS.T("String DataColumn {0}.{1} must have a positive MaxLength specified.", column.Table, column.ColumnName), "column");

                return new SqlUnicodeVariableLengthString(column.MaxLength);
            }

            if (column.DataType == typeof(decimal)) {
                var scale = column.ExtendedProperties.ContainsKey("Scale") ? (int) column.ExtendedProperties["Scale"] : 0;
                var precision = column.ExtendedProperties.ContainsKey("Precision") ? (int) column.ExtendedProperties["Precision"] : 0;
                return new SqlDecimal(precision, scale);
            }

            if (column.DataType == typeof(byte[])) {
                var maxBinaryLength = column.GetMaxBinaryLength();
                if (maxBinaryLength <= 0)
                    throw new ArgumentException(LS.T("Binary DataColumn {0}.{1} must have a positive MaxLength specified.", column.Table, column.ColumnName), "column");

                return new SqlVariableLengthBinary(maxBinaryLength);
            }

            if (column.DataType == typeof(float))
                return new SqlFloat(24); // 7 precision in MS SQL.

            if (column.DataType == typeof(double))
                return new SqlFloat(53); // 15 precision in MS SQL.

			if (column.DataType == typeof(TimeSpan))
				return new SqlTime();

            throw new NotSupportedException(LS.T("Column type '{0}' is unsupported for '{1}'.", column.DataType, column.ColumnName));
        }

        /// <summary>
        /// Creates a SqlDataType for a PropertyInfo.
        /// </summary>
        /// <param name="propertyInfo">The property to create a SqlDataType for.</param>
        /// <returns>A type containing all of the information contained in the property.</returns>
        public static SqlDataType ForProperty (PropertyInfo propertyInfo) {
            var simpleType = ForSimpleDbType(DbTypeEx.GetDbType(propertyInfo.PropertyType));
            if (simpleType != null)
                return simpleType;

            if (propertyInfo.PropertyType == typeof(string)) {
                var maxLength = ValidationInfo.MaximumLength(propertyInfo);
                if (!maxLength.HasValue)
                    throw new ArgumentException(LS.T("Property {0}.{1} must have a StringLength.", propertyInfo.DeclaringType.Name, propertyInfo.Name), "propertyInfo");

                return new SqlUnicodeVariableLengthString(maxLength.Value);
            }

            if (propertyInfo.PropertyType == typeof(byte[])) {
                var maxLength = ValidationInfo.MaximumValue(propertyInfo) as int?;
                if (!maxLength.HasValue)
                    throw new ArgumentException(LS.T("Property {0}.{1} must have a Range.", propertyInfo.DeclaringType.Name, propertyInfo.Name), "propertyInfo");

                return new SqlVariableLengthBinary(maxLength.Value);
            }

            var type = propertyInfo.PropertyType;

            if (propertyInfo.PropertyType.IsNullable())
                type = Nullable.GetUnderlyingType(propertyInfo.PropertyType);

            if (type == typeof(decimal))
                return new SqlDecimal();

            if (type == typeof(float))
                return new SqlFloat(24); // 7 precision in MS SQL.

            if (type == typeof(double))
                return new SqlFloat(53); // 15 precision in MS SQL.

            if (type == typeof(TimeSpan))
                return new SqlTime();

            throw new NotSupportedException(LS.T("Property type '{0}' is unsupported.", propertyInfo.PropertyType));
        }
        #endregion
    }

    #region Exact Numerics
    /// <summary>
    /// Represents a SQL boolean type.
    /// </summary>
    public class SqlBoolean : SqlDataType {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlBoolean).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL decimal type.
    /// </summary>
    public class SqlDecimal : SqlDataType {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlDecimal.
        /// </summary>
        public SqlDecimal () {
        }

        /// <summary>
        /// Constructs a new SqlDecimal.
        /// </summary>
        /// <param name="precision">The total number of digits the decimal may store.</param>
        /// <param name="scale">The maximum number of digits to the right of the decimal point.</param>
        public SqlDecimal (int precision, int scale) {
            Scale = scale;
            Precision = precision;
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlDecimal).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the total number of digits the decimal may store.
        /// </summary>
        public int Precision { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of digits to the right of the decimal point.
        /// </summary>
        public int Scale { get; set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL float type.
    /// </summary>
    public class SqlFloat : SqlDataType {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlFloat.
        /// </summary>
        public SqlFloat () {
        }

        /// <summary>
        /// Constructs a new SqlFloat.
        /// </summary>
        /// <param name="bits">The number of bits that are used to store the mantissa of the float number.</param>
        public SqlFloat (int bits) {
            Bits = bits;
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlFloat).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion

        /// <summary>
        /// Gets or sets the number of bits that are used to store the mantissa of the float number.
        /// </summary>
        public int Bits { get; set; }
    }

    /// <summary>
    /// Represents an 8 bit SQL integer.
    /// </summary>
    public class SqlByte : SqlDataType
    {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlByte);
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept(ISqlExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
        #endregion
    }

    /// <summary>
    /// Represents a 16 bit SQL integer.
    /// </summary>
    public class SqlInt16 : SqlDataType {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlInt16);
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }

    /// <summary>
    /// Represents a 32 bit SQL integer.
    /// </summary>
    public class SqlInt32 : SqlDataType {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlInt32).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }

    /// <summary>
    /// Represents a 64 bit SQL integer.
    /// </summary>
    public class SqlInt64 : SqlDataType {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlInt64).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL money value.
    /// </summary>
    public class SqlMoney : SqlDataType {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlMoney).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }

	/// <summary>
	/// Represents a SQL time value.
	/// </summary>
	public class SqlTime : SqlDataType {
		#region SqlExpression Visitation
		/// <summary>
		/// Accepts a visitor and calls Visit(SqlTime).
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		public override void Accept (ISqlExpressionVisitor visitor) {
			visitor.Visit(this);
		}
		#endregion
	}
    #endregion

    #region Date and Time
    /// <summary>
    /// Represents SQL date and time information.
    /// </summary>
    public class SqlDateTime : SqlDataType {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlDateTime).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }
    #endregion

    #region Strings
    /// <summary>
    /// Represents a SQL string of characters.
    /// </summary>
    public abstract class SqlString : SqlDataType {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlString.
        /// </summary>
        protected SqlString () {
        }

        /// <summary>
        /// Constructs a new SqlString.
        /// </summary>
        /// <param name="length">The length of the string.</param>
        protected SqlString (int length) {
            Length = length;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the length of the string.
        /// </summary>
        public int Length { get; set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL variable-length string of characters.
    /// </summary>
    public class SqlVariableLengthString : SqlString {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlVariableLengthString.
        /// </summary>
        public SqlVariableLengthString () {
        }

        /// <summary>
        /// Constructs a new SqlVariableLengthString.
        /// </summary>
        /// <param name="length">The maximum length of the string.</param>
        public SqlVariableLengthString (int length)
            : base(length) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlVariableLengthString).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL variable-length string of Unicode characters.
    /// </summary>
    public class SqlUnicodeVariableLengthString : SqlVariableLengthString {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlUnicodeVariableLengthString.
        /// </summary>
        public SqlUnicodeVariableLengthString () {
        }

        /// <summary>
        /// Constructs a new SqlUnicodeVariableLengthString.
        /// </summary>
        /// <param name="length">The maximum length of the string.</param>
        public SqlUnicodeVariableLengthString (int length)
            : base(length) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlUnicodeVariableLengthString).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }
    #endregion

    #region Binary
    /// <summary>
    /// Represents a sequence of SQL binary values.
    /// </summary>
    public abstract class SqlBinary : SqlDataType {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlBinary.
        /// </summary>
        protected SqlBinary () {
        }

        /// <summary>
        /// Constructs a new SqlBinary.
        /// </summary>
        /// <param name="length">The length of the binary value sequence.</param>
        protected SqlBinary (int length) {
            Length = length;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the length of the string.
        /// </summary>
        public int Length { get; set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL variable-length sequence of binary values.
    /// </summary>
    public class SqlVariableLengthBinary : SqlBinary {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlVariableLengthBinary.
        /// </summary>
        public SqlVariableLengthBinary () {
        }

        /// <summary>
        /// Constructs a new SqlVariableLengthBinary.
        /// </summary>
        /// <param name="length">The maximum length of the binary value sequence.</param>
        public SqlVariableLengthBinary (int length)
            : base(length) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlVariableLengthBinary).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }
    #endregion

    #region Other
    /// <summary>
    /// Represents a SQL globally unique identifier.
    /// </summary>
    public class SqlGuid : SqlDataType {
        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlGuid).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion
    }
    #endregion
}
