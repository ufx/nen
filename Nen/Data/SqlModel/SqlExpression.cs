using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Text;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Provides a base for any kind of SQL expression.
    /// </summary>
    public abstract class SqlExpression {
        #region Visitation
        /// <summary>
        /// Accepts a visitor and calls the most specific Visit method on it.
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public abstract void Accept (ISqlExpressionVisitor visitor);
        #endregion

        #region Atomicity
        /// <summary>
        /// Returns true if the expression is atomic and can be evaluated
        /// unambiguously without some kind of enclosure, false if otherwise.
        /// </summary>
        public virtual bool IsAtomic {
            get { return false; }
        }
        #endregion

        #region Queries
        public virtual int NumberOfStatements {
            get { return 1; }
        }
        #endregion
    }

    /// <summary>
    /// Represents a compound SQL statement.
    /// </summary>
    public class SqlCompoundExpression : SqlExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlCompoundExpression.
        /// </summary>
        public SqlCompoundExpression () {
            Expressions = new List<SqlExpression>();
        }

        public SqlCompoundExpression (IEnumerable<SqlExpression> expressions) {
            Expressions = new List<SqlExpression>(expressions);
        }
        #endregion

        #region Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlCompoundExpression).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The expressions in the compound.
        /// </summary>
        public ICollection<SqlExpression> Expressions { get; private set; }

        public override int NumberOfStatements {
            get { return Expressions.Count; }
        }
        #endregion
    }

    /// <summary>
    /// Visits specific kinds of SQL expressions.
    /// </summary>
    public interface ISqlExpressionVisitor {
        // Statements

        /// <summary>
        /// Visits a select statement.
        /// </summary>
        /// <param name="sqlSelect">The select to visit.</param>
        void Visit (SqlSelect sqlSelect);

        /// <summary>
        /// Visits an update statement.
        /// </summary>
        /// <param name="sqlUpdate">The update to visit.</param>
        void Visit (SqlUpdate sqlUpdate);

        /// <summary>
        /// Visits an insert statement.
        /// </summary>
        /// <param name="sqlInsert">The insert to visit.</param>
        void Visit (SqlInsert sqlInsert);

        /// <summary>
        /// Visits a delete statement.
        /// </summary>
        /// <param name="sqlDelete">The delete to visit.</param>
        void Visit (SqlDelete sqlDelete);

        /// <summary>
        /// Visits a compound statement.
        /// </summary>
        /// <param name="sqlCompound">The compound to visit.</param>
        void Visit (SqlCompoundExpression sqlCompound);

        void Visit (SqlSet sqlSet);

        // Operators

        /// <summary>
        /// Visits a binary operator expression.
        /// </summary>
        /// <param name="binaryOperator">The binary operator to visit.</param>
        void Visit (SqlBinaryOperatorExpression binaryOperator);

        /// <summary>
        /// Visits a prefix operator expression.
        /// </summary>
        /// <param name="prefixOperator">The prefix operator to visit.</param>
        void Visit (SqlPrefixOperatorExpression prefixOperator);

        /// <summary>
        /// Visits a postfix operator expression.
        /// </summary>
        /// <param name="postfixOperator">The postfix operator to visit.</param>
        void Visit (SqlPostfixOperatorExpression postfixOperator);

        // Fragments

        /// <summary>
        /// Visits a join.
        /// </summary>
        /// <param name="join">The join to visit.</param>
        void Visit (SqlJoin join);

        /// <summary>
        /// Visits a table.
        /// </summary>
        /// <param name="table">The table visit.</param>
        void Visit (SqlTable table);

        /// <summary>
        /// Visits a column.
        /// </summary>
        /// <param name="column">The column.</param>
        void Visit (SqlColumn column);

        /// <summary>
        /// Visits an order specifier.
        /// </summary>
        /// <param name="order">The order specifier to visit.</param>
        void Visit (SqlOrder order);

        /// <summary>
        /// Visits an identifier.
        /// </summary>
        /// <param name="identifier">The identifier to visit.</param>
        void Visit (SqlIdentifier identifier);

        /// <summary>
        /// Visits a parameter.
        /// </summary>
        /// <param name="parameter">The parameter to visit.</param>
        void Visit (SqlParameter parameter);

        /// <summary>
        /// Visits a literal expression.
        /// </summary>
        /// <param name="literal">The literal to visit.</param>
        void Visit (SqlLiteralExpression literal);

        /// <summary>
        /// Visits a function.
        /// </summary>
        /// <param name="function">The function to visit.</param>
        void Visit (SqlFunction function);

        /// <summary>
        /// Visits a procedure.
        /// </summary>
        /// <param name="procedure">The procedure to visit.</param>
        void Visit (SqlProcedure procedure);

        /// <summary>
        /// Visits a declaration.
        /// </summary>
        /// <param name="declaration">The declaration to visit.</param>
        void Visit (SqlDeclaration declaration);

        // Schema Definition

        /// <summary>
        /// Visits a create table statement.
        /// </summary>
        /// <param name="createTable">The create table to visit.</param>
        void Visit (SqlCreateTable createTable);

        /// <summary>
        /// Visits an alter table statement.
        /// </summary>
        /// <param name="alterTable">The alter table to visit.</param>
        void Visit (SqlAlterTable alterTable);

        /// <summary>
        /// Visits a create column statement.
        /// </summary>
        /// <param name="createColumn">The create column to visit.</param>
        void Visit (SqlCreateColumn createColumn);

        /// <summary>
        /// Visits a primary key constraint.
        /// </summary>
        /// <param name="primaryKey">The primary key constraint to visit.</param>
        void Visit (SqlPrimaryKeyConstraint primaryKey);

        /// <summary>
        /// Visits a foreign key constraint.
        /// </summary>
        /// <param name="foreignKey">The foreign key constraint to visit.</param>
        void Visit (SqlForeignKeyConstraint foreignKey);

        /// <summary>
        /// Visits a create sequence statement.
        /// </summary>
        /// <param name="sequence">The create sequence to visit.</param>
        void Visit(SqlCreateSequence createSequence);

        // Data types

        /// <summary>
        /// Visits a boolean.
        /// </summary>
        /// <param name="boolean">The boolean to visit.</param>
        void Visit (SqlBoolean boolean);

        /// <summary>
        /// Visits a datetime.
        /// </summary>
        /// <param name="dateTime">The datetime to visit.</param>
        void Visit (SqlDateTime dateTime);

        /// <summary>
        /// Visits a decimal.
        /// </summary>
        /// <param name="decimalValue">The decimal to visit.</param>
        void Visit (SqlDecimal decimalValue);

        /// <summary>
        /// Visits a float.
        /// </summary>
        /// <param name="floatValue">The float to visit.</param>
        void Visit (SqlFloat floatValue);

        /// <summary>
        /// Visits a GUID.
        /// </summary>
        /// <param name="guid">The GUID to visit.</param>
        void Visit (SqlGuid guid);

        /// <summary>
        /// Visits an 8 bit integer.
        /// </summary>
        /// <param name="byteValue">The 8 bit integer to visit.</param>
        void Visit(SqlByte byteValue);
        
        /// <summary>
        /// Visits a 16 bit integer.
        /// </summary>
        /// <param name="int16">The 16 bit integer to visit.</param>
        void Visit (SqlInt16 int16);

        /// <summary>
        /// Visits a 32 bit integer.
        /// </summary>
        /// <param name="int32">The 32 bit integer to visit.</param>
        void Visit (SqlInt32 int32);

        /// <summary>
        /// Visits a 64 bit integer.
        /// </summary>
        /// <param name="int64">The 64 bit integer to visit.</param>
        void Visit (SqlInt64 int64);

        /// <summary>
        /// Visits a variable length string.
        /// </summary>
        /// <param name="str">The variable length string to visit.</param>
        void Visit (SqlVariableLengthString str);

        /// <summary>
        /// Visits a variable length unicode string.
        /// </summary>
        /// <param name="str">The variable length unicode string to visit.</param>
        void Visit (SqlUnicodeVariableLengthString str);

        /// <summary>
        /// Visits a variable length binary type.
        /// </summary>
        /// <param name="binary">The variable length binary type to visit.</param>
        void Visit (SqlVariableLengthBinary binary);

        /// <summary>
        /// Visits a money type.
        /// </summary>
        /// <param name="money">The money type to visit.</param>
        void Visit (SqlMoney money);

		/// <summary>
		/// Visits a time type.
		/// </summary>
		/// <param name="time">The time type to visit.</param>
		void Visit (SqlTime time);
    }
}
