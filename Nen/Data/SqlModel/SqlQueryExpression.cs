using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Provides a base for SQL expressions that return values.
    /// </summary>
    public abstract class SqlQueryExpression : SqlExpression {
    }

    /// <summary>
    /// Represents a SQL select.
    /// </summary>
    public class SqlSelect : SqlQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlSelect.
        /// </summary>
        public SqlSelect () {
            Columns = new List<SqlColumn>();
            From = new List<SqlTable>();
            Joins = new List<SqlJoin>();
            OrderBy = new List<SqlOrder>();
            GroupBy = new List<SqlQueryExpression>();
        }

        /// <summary>
        /// Constructs a new SqlSelect.
        /// </summary>
        /// <param name="fromTable">The table to select from.</param>
        public SqlSelect (SqlTable fromTable)
            : this() {
            From.Add(fromTable);
        }

        /// <summary>
        /// Constructs a new SqlSelect.
        /// </summary>
        /// <param name="fromTableName">The table name to select from.</param>
        public SqlSelect (string fromTableName)
            : this(new SqlTable(fromTableName)) {
        }

        /// <summary>
        /// Constructs a new SqlSelect.
        /// </summary>
        /// <param name="schemaName">The schema name to select from.</param>
        /// <param name="fromTableName">The table name to select from.</param>
        public SqlSelect (string schemaName, string fromTableName) 
            : this(new SqlTable(schemaName, fromTableName)) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlSelect).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The columns to select.
        /// </summary>
        public ICollection<SqlColumn> Columns { get; private set; }

        /// <summary>
        /// The tables to select from.
        /// </summary>
        public ICollection<SqlTable> From { get; private set; }

        /// <summary>
        /// The joins on this select.
        /// </summary>
        public ICollection<SqlJoin> Joins { get; private set; }

        /// <summary>
        /// The select criteria.
        /// </summary>
        public SqlOperatorExpression Where { get; set; }

        /// <summary>
        /// The order specifiers.
        /// </summary>
        public ICollection<SqlOrder> OrderBy { get; private set; }

        /// <summary>
        /// The grouping specifiers.
        /// </summary>
        public ICollection<SqlQueryExpression> GroupBy { get; private set; }

        /// <summary>
        /// The total number of rows to select.
        /// </summary>
        public int? Limit { get; set; }

        /// <summary>
        /// The offset into the total number of rows to select.
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// The select is for distinct values.
        /// </summary>
        public bool IsDistinct { get; set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the select.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            var str = new StringBuilder("SELECT");

            if (Columns.Count > 0)
                str.AppendFormat(CultureInfo.InvariantCulture, " {0}", string.Join(", ", Columns.ToStrings().ToArray()));

            if (From.Count > 0)
                str.AppendFormat(CultureInfo.InvariantCulture, " FROM {0}", string.Join(" ", From.ToStrings().ToArray()));

            if (Joins.Count > 0)
                str.AppendFormat(CultureInfo.InvariantCulture, " {0}", Joins.ToStrings().ToArray());

            if (Where != null)
                str.AppendFormat(CultureInfo.InvariantCulture, " WHERE {0}", Where);

            if (OrderBy.Count > 0)
                str.AppendFormat(CultureInfo.InvariantCulture, " ORDER BY {0}", OrderBy.ToStrings().ToArray());

            if (GroupBy.Count > 0)
                str.AppendFormat(CultureInfo.InvariantCulture, " GROUP BY {0}", GroupBy.ToStrings().ToArray());

            if (Limit.HasValue)
                str.AppendFormat(CultureInfo.InvariantCulture, " LIMIT {0}", Limit.Value);

            if (Offset.HasValue)
                str.AppendFormat(CultureInfo.InvariantCulture, " OFFSET {0}", Offset.Value);

            return str.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL function.
    /// </summary>
    public class SqlFunction : SqlQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlFunction.
        /// </summary>
        /// <param name="functionName">The function name.</param>
        public SqlFunction (string functionName) {
            FunctionName = functionName;
        }

        public SqlFunction(string functionName, params SqlQueryExpression[] args)
            : this(functionName)
        {
            FunctionArguments = args;
        }
        #endregion

        #region SqlExpression Atomicity
        /// <summary>
        /// Returns true, as a function is always atomic.
        /// </summary>
        public override bool IsAtomic {
            get { return true; }
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlFunction).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the function name.
        /// </summary>
        public string FunctionName { get; set; }

        /// <summary>
        /// Gets or sets the collection of function arguments.
        /// </summary>
        public ICollection<SqlQueryExpression> FunctionArguments { get; set; }

        /// <summary>
        /// Gets or sets the scalar indicator, which excludes parens for function calls.
        /// </summary>
        public bool IsScalar { get; set; }
        #endregion

        public override string ToString () {
            return string.Format("{0}(...)", FunctionName);
        }
    }

    /// <summary>
    /// Represents a SQL procedure.
    /// </summary>
    public class SqlProcedure : SqlQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlProcedure.
        /// </summary>
        /// <param name="procedureName">The name of the procedure.</param>
        public SqlProcedure (string procedureName) {
            ProcedureName = procedureName;
            ProcedureArguments = new List<SqlQueryExpression>();
        }
        #endregion

        #region SqlExpression Atomicity
        /// <summary>
        /// Returns true, as a procedure is always atomic.
        /// </summary>
        public override bool IsAtomic {
            get { return true; }
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlProcedure).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the procedure name.
        /// </summary>
        public string ProcedureName { get; set; }

        /// <summary>
        /// Gets or sets the collection of procedure arguments.
        /// </summary>
        public ICollection<SqlQueryExpression> ProcedureArguments { get; set; }
        #endregion
    }

    /// <summary>
    /// Represents a literal SQL expression.
    /// </summary>
    public class SqlLiteralExpression : SqlQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlLiteralExpression.
        /// </summary>
        public SqlLiteralExpression () {
        }

        /// <summary>
        /// Constructs a new SqlLiteralExpression.
        /// </summary>
        /// <param name="value">The literal value.</param>
        public SqlLiteralExpression (object value) {
            Value = value;
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlLiteralExpression).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The data type of the value.
        /// </summary>
        public DbType? DbType { get; set; }

        /// <summary>
        /// The literal value.
        /// </summary>
        public object Value { get; set; }
        #endregion

        #region SqlExpression Atomicity
        /// <summary>
        /// Returns true if the literal contains a collection, false if otherwise.
        /// </summary>
        public override bool IsAtomic {
            get { return !(Value is IEnumerable<object>); }
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the literal.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return Value == null ? "null" : Value.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL identifier.
    /// </summary>
    public class SqlIdentifier : SqlQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlIdentifier.
        /// </summary>
        public SqlIdentifier () {
            Parts = new List<string>();
        }

        public SqlIdentifier (string part): this() {
            if (part != null)
                Parts.Add(part);
        }

        public SqlIdentifier (string part1, string part2): this() {
            if (part1 != null)
                Parts.Add(part1);

            if (part2 != null)
                Parts.Add(part2);
        }

        /// <summary>
        /// Constructs a new SqlIdentifier.
        /// </summary>
        /// <param name="parts">The parts of the identifier</param>
        public SqlIdentifier (params string[] parts): this() {
            Parts.AddRange(parts.WhereNotNull());
        }
        #endregion

        #region SqlExpression Members
        /// <summary>
        /// Returns true, as an identifier is always atomic.
        /// </summary>
        public override bool IsAtomic {
            get { return true; }
        }

        /// <summary>
        /// Accepts a visitor and calls Visit(SqlIdentifier).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        public bool IsDeclared { get; set; }
        public bool IsQuoted { get; set; }

        /// <summary>
        /// The parts of the identifier.
        /// </summary>
        public ICollection<string> Parts { get; private set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the identifier.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return string.Join(".", Parts.ToArray());
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL parameter.
    /// </summary>
    public class SqlParameter : SqlQueryExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlParameter.
        /// </summary>
        public SqlParameter () {
        }

        public SqlParameter (string name, DbType dbType) {
            DbType = dbType;
            Name = name;
        }
        #endregion

        #region SqlExpression Members
        /// <summary>
        /// Returns true, as an identifier is always atomic.
        /// </summary>
        public override bool IsAtomic {
            get { return true; }
        }

        /// <summary>
        /// Accepts a visitor and calls Visit(SqlParameter).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        public DbType DbType { get; set; }
        public string Name { get; set; }
        public byte Precision { get; set; }
        public byte Scale { get; set; }
        public int Size { get; set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the parameter.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return "@" + Name;
        }
        #endregion
    }

    /// <summary>
    /// Represents a SQL order specifier.
    /// </summary>
    public class SqlOrder : SqlExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlOrder.
        /// </summary>
        public SqlOrder () {
        }

        /// <summary>
        /// Constructs a new SqlOrder.
        /// </summary>
        /// <param name="expression">The query expression to order by.</param>
        public SqlOrder (SqlQueryExpression expression) {
            Expression = expression;
        }

        /// <summary>
        /// Constructs a new SqlOrder.
        /// </summary>
        /// <param name="direction">The direction of the ordering.</param>
        /// <param name="expression">The query expression to order by.</param>
        public SqlOrder (SqlOrderDirection direction, SqlQueryExpression expression)
            : this(expression) {
            Direction = direction;
        }

        /// <summary>
        /// Constructs a new SqlOrder.
        /// </summary>
        /// <param name="expressionParts">The identifier parts to order by.</param>
        public SqlOrder (params string[] expressionParts)
            : this(new SqlIdentifier(expressionParts)) {
        }

        /// <summary>
        /// Constructs a new SqlOrder.
        /// </summary>
        /// <param name="direction">The direction of the ordering.</param>
        /// <param name="expressionParts">The identifier parts to order by.</param>
        public SqlOrder (SqlOrderDirection direction, params string[] expressionParts)
            : this(direction, new SqlIdentifier(expressionParts)) {
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlOrder).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The direction of the ordering.
        /// </summary>
        public SqlOrderDirection Direction { get; set; }

        /// <summary>
        /// The query expression to order by.
        /// </summary>
        public SqlQueryExpression Expression { get; set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the order specifier.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "{0} {1}", Expression, Direction);
        }
        #endregion
    }

    /// <summary>
    /// Specifies the direction of the ordering.
    /// </summary>
    public enum SqlOrderDirection {
        /// <summary>
        /// Order values from lowest to highest.
        /// </summary>
        Ascending = 0,

        /// <summary>
        /// Order values from highest to lowest.
        /// </summary>
        Descending
    }
}
