﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Data.SqlModel {
    /// <summary>
    /// Represents a SQL table schema.
    /// </summary>
    public abstract class SqlTableSchemaExpression : SqlExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlTableSchemaExpression.
        /// </summary>
        protected SqlTableSchemaExpression () {
            Constraints = new List<SqlConstraint>();
        }
        #endregion

        #region SqlExpression Atomicity
        /// <summary>
        /// Returns true, as a table schema statement is always atomic.
        /// </summary>
        public override bool IsAtomic {
            get { return true; }
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the collection of constraints to establish.
        /// </summary>
        public ICollection<SqlConstraint> Constraints { get; private set; }

        /// <summary>
        /// Gets or sets the name of the table.
        /// </summary>
        public SqlIdentifier Name { get; set; }

        /// <summary>
        /// Gets or sets the schema name of the table.
        /// </summary>
        public SqlIdentifier SchemaName { get; set; }
        #endregion

    }

    /// <summary>
    /// Represents a SQL create table.
    /// </summary>
    public class SqlCreateTable : SqlTableSchemaExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlCreateTable.
        /// </summary>
        public SqlCreateTable () {
            Columns = new List<SqlCreateColumn>();
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlCreateTable).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion

        #region Queries
        /// <summary>
        /// Determines whether the given create column expression creates a primary key.
        /// </summary>
        /// <param name="column">The column to check.</param>
        /// <returns>true if the column matches the expression of the primary key constraint, false if otherwise.</returns>
        public bool IsPrimaryKeyColumn (SqlCreateColumn column) {
            var primaryKeyConstraints = Constraints.OfType<SqlPrimaryKeyConstraint>();
            return primaryKeyConstraints.Any(p => p.Columns.Any(c => c.Expression.ToString() == column.Expression.ToString()));
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the collection of columns to create.
        /// </summary>
        public ICollection<SqlCreateColumn> Columns { get; private set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL alter table.
    /// </summary>
    public class SqlAlterTable : SqlTableSchemaExpression
    {
        #region Constructors
        public SqlAlterTable()
        {
            AddedColumns = new List<SqlCreateColumn>();
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlAlterTable).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion

        public ICollection<SqlCreateColumn> AddedColumns { get; private set; }
    }

    /// <summary>
    /// Represents a SQL create column.
    /// </summary>
    public class SqlCreateColumn : SqlColumn {
        #region SqlExpression Atomicity
        /// <summary>
        /// Returns true, as a create column statement is always atomic.
        /// </summary>
        public override bool IsAtomic {
            get { return true; }
        }
        #endregion

        #region SqlExpression Visitation
        /// <summary>
        /// Accepts a visitor and calls Visit(SqlCreateColumn).
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        public override void Accept (ISqlExpressionVisitor visitor) {
            visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets an indicator on whether the column auto increments.
        /// </summary>
        public bool AutoIncrements { get; set; }

        /// <summary>
        /// Gets or sets the initial seed value to start auto incrementing from.
        /// </summary>
        public long AutoIncrementSeed { get; set; }

        /// <summary>
        /// Gets or sets the step to auto increment by.
        /// </summary>
        public long AutoIncrementStep { get; set; }

        /// <summary>
        /// Gets or sets the data type.
        /// </summary>
        public SqlDataType DataType { get; set; }

        /// <summary>
        /// Gets or sets the default expression.
        /// </summary>
        public SqlExpression DefaultExpression { get; set; }

        /// <summary>
        /// Gets or sets an indicator on whether column is nullable.
        /// </summary>
        public bool Nullable { get; set; }

        /// <summary>
        /// Gets or sets an indicator on whether column is sparse.
        /// 
        /// For database types that do not support sparse columns, this property is ignored.
        /// </summary>
        public bool Sparse { get; set; }
        #endregion
    }

    /// <summary>
    /// Represents a SQL create sequence.
    /// </summary>
    public class SqlCreateSequence : SqlExpression
    {
        #region SqlExpression Atomicity
        /// <summary>
        /// Returns true, as a sequence schema statement is always atomic.
        /// </summary>
        public override bool IsAtomic
        {
            get { return true; }
        }
        #endregion

        public override void Accept(ISqlExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }

        /// <summary>
        /// Gets or sets the name of the sequence.
        /// </summary>
        public SqlIdentifier Name { get; set; }

        /// <summary>
        /// Gets or sets the schema name of the sequence.
        /// </summary>
        public SqlIdentifier SchemaName { get; set; }

        /// <summary>
        /// Gets or sets the starting value of the sequence.
        /// </summary>
        public int? Seed { get; set; }

        /// <summary>
        /// Gets or sets the increment value of the sequence.
        /// </summary>
        public int? Step { get; set; }

        /// <summary>
        /// Gets or sets the maximum value of the sequence.  int.MaxValue means no maximum value.
        /// </summary>
        public int? MaxValue { get; set; }
    }
}
