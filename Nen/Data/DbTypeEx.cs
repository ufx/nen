﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Nen.Reflection;

namespace Nen.Data {
    public static class DbTypeEx {
        private static Dictionary<Type, DbType> _dbTypeMap = new Dictionary<Type, DbType>();

        static DbTypeEx () {
            _dbTypeMap[typeof(string)] = DbType.String;
            _dbTypeMap[typeof(byte[])] = DbType.Binary;
            _dbTypeMap[typeof(short)] = DbType.Int16;
            _dbTypeMap[typeof(int)] = DbType.Int32;
            _dbTypeMap[typeof(long)] = DbType.Int64;
            _dbTypeMap[typeof(sbyte)] = DbType.SByte;
            _dbTypeMap[typeof(byte)] = DbType.Byte;
            _dbTypeMap[typeof(ushort)] = DbType.UInt16;
            _dbTypeMap[typeof(uint)] = DbType.UInt32;
            _dbTypeMap[typeof(ulong)] = DbType.UInt64;
            _dbTypeMap[typeof(DateTime)] = DbType.DateTime;
            _dbTypeMap[typeof(decimal)] = DbType.Decimal;
            _dbTypeMap[typeof(Guid)] = DbType.Guid;
            _dbTypeMap[typeof(float)] = DbType.Single;
            _dbTypeMap[typeof(double)] = DbType.Double;
            _dbTypeMap[typeof(bool)] = DbType.Boolean;
            _dbTypeMap[typeof(Money)] = DbType.Currency;
            _dbTypeMap[typeof(TimeSpan)] = DbType.Time;
        }

        /// <summary>
        /// Gets the database type corresponding to a type.
        /// </summary>
        /// <param name="type">The type to find a database type for.</param>
        /// <returns>The corresponding DbType value.</returns>
        public static DbType GetDbType (Type type) {
            if (_dbTypeMap.ContainsKey(type))
                return _dbTypeMap[type];
            else if (type.IsEnum)
                return GetDbType(Enum.GetUnderlyingType(type));
            else if (type.IsNullable())
                return GetDbType(Nullable.GetUnderlyingType(type));
            else
                return DbType.Object;
        }
    }
}
