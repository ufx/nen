﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Nen.Data.SqlModel;
using Nen.Collections;

namespace Nen.Data {
    /// <summary>
    /// Extensions to the DataRow class.
    /// </summary>
    public static class DataRowEx {
        /// <summary>
        /// Generates an insert expression for the data within the row.
        /// </summary>
        /// <param name="row">The row to emit an insert for.</param>
        /// <returns>A SqlInsert containing the row data.</returns>
        public static SqlInsert GenerateInsert (this DataRow row) {
            if (row == null)
                throw new ArgumentNullException("row");

            var schemaName = row.Table.GetSchemaName();

            var insert = new SqlInsert();
            if (schemaName == null)
                insert.Table = new SqlIdentifier(row.Table.TableName);
            else
                insert.Table = new SqlIdentifier(schemaName, row.Table.TableName);

            var values = new List<SqlQueryExpression>();
            foreach (DataColumn column in row.Table.Columns) {
                if (column.AutoIncrement)
                    continue;

                var value = row[column];
                if (value == DBNull.Value)
                    value = null;

                insert.Columns.Add(new SqlIdentifier(column.ColumnName));
                values.Add(new SqlLiteralExpression(value));
            }

            insert.Values.AddRange(values);

            return insert;
        }

        public static object GetPrimaryKeyField (this DataRow row) {
            if (row == null)
                throw new ArgumentNullException("row");

            var primaryKeyColumn = row.Table.PrimaryKey[0];
            return row[primaryKeyColumn];
        }
    }
}
