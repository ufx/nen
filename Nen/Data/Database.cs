using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Linq;

using Nen.Collections;
using Nen.Internal;

namespace Nen.Data {
    /// <summary>
    /// Represents a relational database.
    /// </summary>
    [DataContract]
    [KnownType(typeof(SqlClient.SqlDatabase))]
    public abstract class Database {
        #region Constructors
        /// <summary>
        /// Constructs a new Database.
        /// </summary>
        /// <param name="culture">The culture of the data stored in the database.</param>
        protected Database (CultureInfo culture)
            : this(null, culture) {
        }

        /// <summary>
        /// Constructs a new Database.
        /// </summary>
        /// <param name="connectionString">The connection string to use when executing commands.</param>
        /// <param name="culture">The culture of the data stored in the database.</param>
        protected Database (string connectionString, CultureInfo culture) {
            DateTimeMode = DataSetDateTime.UnspecifiedLocal;
            ConnectionString = connectionString;
            Culture = culture;
        }

        /// <summary>
        /// Constructs a new Database with the current culture.
        /// </summary>
        protected Database ()
            : this(null, CultureInfo.CurrentCulture) {
        }

        #endregion

        #region Parameters
        /// <summary>
        /// Converts an object array into an array of parameters for a procedure by ordinal.
        /// </summary>
        /// <param name="command">The command this procedure will run on.</param>
        /// <param name="procedureName">The name of the procedure.</param>
        /// <param name="args">The array of arguments to convert.</param>
        /// <returns>An array of IDbDataParameter values corresponding to each item in the args array.</returns>
        public IDbDataParameter[] ConvertParameters (IDbCommand command, params object[] args) {
            if (args == null)
                throw new ArgumentNullException("args");

            var parameters = DeriveParameters(command.CommandText);
            if (parameters.Length != args.Length)
                throw new ArgumentException(LS.T("The number of arguments ({0}) does not match the number of procedure parameters ({1}).", args.Length, parameters.Length), "args");

            for (var i = 0; i < parameters.Length; i++)
            {
                var param = parameters[i];
                param.Value = args[i] ?? DBNull.Value;
            }
            return parameters;
        }
        #endregion

        #region Execution
        #region Command Execution
        /// <summary>
        /// Executes the command and returns an IDataReader for the results.
        /// This method can be called on commands that have no current
        /// connection.  A new connection will be opened for the command.
        /// </summary>
        /// <param name="cmd">The command to execute.</param>
        /// <returns>An IDataReader for the command results.</returns>
        public IDataReader ExecuteDataReader (IDbCommand cmd) {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            try {
                if (cmd.Transaction != null || cmd.Connection != null)
                    return cmd.ExecuteReader();
                else {
                    cmd.Connection = OpenConnection();
                    return cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
            } catch (Exception ex) {
                try
                {
                    ex.Data["Nen.Database.ExecutingCommand"] = CreateFormatter().PrintCommand(cmd);
                }
                catch (Exception) { } // Squelch.

                throw;
            }
        }

        /// <summary>
        /// Executes the command and returns a DataSet containing the results.
        /// This method can be called on commands that have no current
        /// connection.  A new connection will be opened for the command.
        /// </summary>
        /// <param name="cmd">The command to execute.</param>
        /// <returns>A DataSet containing the command results.</returns>
        public DataSet ExecuteDataSet (IDbCommand cmd) {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            try {
                var adapter = CreateDataAdapter(cmd);
                var results = new DataSet();
                results.Locale = Culture;

                if (DateTimeMode != DataSetDateTime.UnspecifiedLocal)
                    results.Tables.CollectionChanged += SetDateTimeMode;

                if (cmd.Transaction != null || cmd.Connection != null)
                    adapter.Fill(results);
                else {
                    using (var conn = OpenConnection()) {
                        cmd.Connection = conn;
                        adapter.Fill(results);
                    }
                }

                return results;
            } catch (Exception ex) {
                try
                {
                    ex.Data["Nen.Database.ExecutingCommand"] = CreateFormatter().PrintCommand(cmd);
                }
                catch (Exception) { } // Squelch.

                throw;
            }
        }

        /// <summary>
        /// Executes the non-query and returns the number of rows affected.
        /// This method can be called on commands that have no current
        /// connection.  A new connection will be opened for the command.
        /// </summary>
        /// <param name="cmd">The non-query command to execute.</param>
        /// <returns>The number of rows affected.</returns>
        public int ExecuteNonQuery (IDbCommand cmd) {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            try {
                if (cmd.Transaction != null || cmd.Connection != null)
                    return cmd.ExecuteNonQuery();
                else {
                    using (var conn = OpenConnection()) {
                        cmd.Connection = conn;
                        return cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception ex) {
                try
                {
                    ex.Data["Nen.Database.ExecutingCommand"] = CreateFormatter().PrintCommand(cmd);
                }
                catch (Exception) { } // Squelch.

                throw;
            }
        }

        /// <summary>
        /// Executes the command and returns an XmlReader for the results.
        /// This method can be called on commands that have no current
        /// connection.  A new connection will be opened for the command.
        /// </summary>
        /// <param name="cmd">The command to execute.</param>
        /// <returns>An XmlReader for the command results.</returns>
        public virtual XmlReader ExecuteXmlReader (IDbCommand cmd) {
            throw new NotImplementedException(LS.T("This database does not support XmlReaders."));
        }

        private void SetDateTimeMode (object sender, CollectionChangeEventArgs item) {
            var table = (DataTable) item.Element;
            foreach (DataColumn column in table.Columns) {
                if (column.DataType == typeof(DateTime))
                    column.DateTimeMode = DateTimeMode;
            }
        }
        #endregion

        #region Procedure Execution
        #region DataReader
        /// <summary>
        /// Executes the procedure with the specified criteria and returns an
        /// IDataReader for the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>An IDataReader for the results.</returns>
        public IDataReader ExecuteDataReader (string procedureName, params object[] args) {
            return ExecuteDataReader(procedureName, CommandType.StoredProcedure, null, args);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns an
        /// IDataReader for the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the procedure under.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>An IDataReader for the results.</returns>
        public IDataReader ExecuteDataReader (string procedureName, IDbTransaction tran, params object[] args) {
            return ExecuteDataReader(procedureName, CommandType.StoredProcedure, tran, args);
        }

        /// <summary>
        /// Executes the commnd with the specified criteria and returns an
        /// IDataReader for the results.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>An IDataReader for the results.</returns>
        public IDataReader ExecuteDataReader (string commandText, CommandType commandType, IDbTransaction tran, params object[] args) {
            using (var cmd = CreateCommand(commandText, commandType, tran, args))
                return ExecuteDataReader(cmd);
        }
        #endregion

        #region DataRow
        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first row of the first table of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <returns>The first row of the first table of the results.</returns>
        public DataRow ExecuteDataRow (string procedureName) {
            return ExecuteDataRow(procedureName, CommandType.StoredProcedure, null);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first row of the first table of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>The first row of the first table of the results.</returns>
        public DataRow ExecuteDataRow (string procedureName, params object[] args) {
            return ExecuteDataRow(procedureName, CommandType.StoredProcedure, null, args);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first row of the first table of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the procedure under.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>The first row of the first table of the results.</returns>
        public DataRow ExecuteDataRow (string procedureName, IDbTransaction tran, params object[] args) {
            return ExecuteDataRow(procedureName, CommandType.StoredProcedure, tran, args);
        }

        /// <summary>
        /// Executes the command with the specified criteria and returns the
        /// first row of the first table of the results.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>The first row of the first table of the results.</returns>
        public DataRow ExecuteDataRow (string commandText, CommandType commandType, IDbTransaction tran, params object[] args) {
            using (var cmd = CreateCommand(commandText, commandType, tran, args))
                return ExecuteDataRow(cmd);
        }

        /// <summary>
        /// Executes the command and returns the first row of the first table
        /// of the results.
        /// </summary>
        /// <param name="cmd">The command to execute.</param>
        /// <returns>The first row of the first table of the results.</returns>
        public DataRow ExecuteDataRow (IDbCommand cmd) {
            var results = ExecuteDataTable(cmd);
            return results == null || results.Rows.Count == 0 ? null : results.Rows[0];
        }
        #endregion

        #region DataSet
        /// <summary>
        /// Executes the procedure with the specified criteria and returns a
        /// DataSet containing the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>A DataSet containing the results.</returns>
        public DataSet ExecuteDataSet (string procedureName, params object[] args) {
            return ExecuteDataSet(procedureName, CommandType.StoredProcedure, null, args);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns a
        /// DataSet containing the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the procedure under.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>A DataSet containing the results.</returns>
        public DataSet ExecuteDataSet (string procedureName, IDbTransaction tran, params object[] args) {
            return ExecuteDataSet(procedureName, CommandType.StoredProcedure, tran, args);
        }

        /// <summary>
        /// Executes the command with the specified criteria and returns a
        /// DataSet containing the results.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>A DataSet containing the results.</returns>
        public DataSet ExecuteDataSet (string commandText, CommandType commandType, IDbTransaction tran, params object[] args) {
            using (var cmd = CreateCommand(commandText, commandType, tran, args))
                return ExecuteDataSet(cmd);
        }
        #endregion

        #region DataTable
        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first DataTable of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <returns>The first DataTable of the results.</returns>
        public DataTable ExecuteDataTable (string procedureName) {
            return ExecuteDataTable(procedureName, CommandType.StoredProcedure, null);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first DataTable of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>The first DataTable of the results.</returns>
        public DataTable ExecuteDataTable (string procedureName, params object[] args) {
            return ExecuteDataTable(procedureName, CommandType.StoredProcedure, null, args);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first DataTable of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the procedure under.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>The first DataTable of the results.</returns>
        public DataTable ExecuteDataTable (string procedureName, IDbTransaction tran, params object[] args) {
            return ExecuteDataTable(procedureName, CommandType.StoredProcedure, tran, args);
        }

        /// <summary>
        /// Executes the command with the specified criteria and returns the
        /// first DataTable of the results.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>The first DataTable of the results.</returns>
        public DataTable ExecuteDataTable (string commandText, CommandType commandType, IDbTransaction tran, params object[] args) {
            using (var cmd = CreateCommand(commandText, commandType, tran, args))
                return ExecuteDataTable(cmd);
        }

        /// <summary>
        /// Executes the command and returns the first DataTable of the
        /// results.
        /// </summary>
        /// <param name="cmd">The command to execute.</param>
        /// <returns>The first DataTable of the results.</returns>
        public DataTable ExecuteDataTable (IDbCommand cmd) {
            var results = ExecuteDataSet(cmd);
            return results.Tables.Count == 0 ? null : results.Tables[0];
        }
        #endregion

        #region NonQuery
        /// <summary>
        /// Executes the non-query procedure with the specified criteria and
        /// returns the number of rows affected.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>The number of rows affected.</returns>
        public int ExecuteNonQuery (string procedureName, params object[] args) {
            return ExecuteNonQuery(procedureName, CommandType.StoredProcedure, null, args);
        }

        /// <summary>
        /// Executes the non-query procedure with the specified criteria and
        /// returns the number of rows affected.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the procedure under.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>The number of rows affected.</returns>
        public int ExecuteNonQuery (string procedureName, IDbTransaction tran, params object[] args) {
            return ExecuteNonQuery(procedureName, CommandType.StoredProcedure, tran, args);
        }

        /// <summary>
        /// Executes the non-query command with the specified criteria and
        /// returns the number of rows affected.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>The number of rows affected.</returns>
        public int ExecuteNonQuery (string commandText, CommandType commandType, IDbTransaction tran, params object[] args) {
            using (var cmd = CreateCommand(commandText, commandType, tran, args))
                return ExecuteNonQuery(cmd);
        }
        #endregion

        #region Scalars
        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first column of the first row of the first table of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <returns>The first column of the first row of the first table of the results.</returns>
        public object ExecuteScalar (string procedureName) {
            return ExecuteScalar(procedureName, CommandType.StoredProcedure, null);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first column of the first row of the first table of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>The first column of the first row of the first table of the results.</returns>
        public object ExecuteScalar (string procedureName, params object[] args) {
            return ExecuteScalar(procedureName, CommandType.StoredProcedure, null, args);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns the
        /// first column of the first row of the first table of the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the procedure under.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>The first column of the first row of the first table of the results.</returns>
        public object ExecuteScalar (string procedureName, IDbTransaction tran, params object[] args) {
            return ExecuteScalar(procedureName, CommandType.StoredProcedure, tran, args);
        }

        /// <summary>
        /// Executes the command with the specified criteria and returns the
        /// first column of the first row of the first table of the results.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>The first column of the first row of the first table of the results.</returns>
        public object ExecuteScalar (string commandText, CommandType commandType, IDbTransaction tran, params object[] args) {
            using (var cmd = CreateCommand(commandText, commandType, tran, args))
                return ExecuteScalar(cmd);
        }

        /// <summary>
        /// Executes the command and returns the first column of the first row
        /// of the first table of the results.
        /// </summary>
        /// <param name="cmd">The command to execute.</param>
        /// <returns>The first column of the first row of the first table of the results.</returns>
        public object ExecuteScalar (IDbCommand cmd) {
            using (var reader = ExecuteDataReader(cmd))
                return reader.Read() ? reader.GetValue(0) : null;
        }
        #endregion

        #region XmlReader
        /// <summary>
        /// Executes the procedure with the specified criteria and returns an
        /// XmlReader for the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <returns>An XmlReader for the results.</returns>
        public XmlReader ExecuteXmlReader (string procedureName, IDbTransaction tran) {
            return ExecuteXmlReader(procedureName, CommandType.StoredProcedure, tran);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns an
        /// XmlReader for the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the procedure under.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>An XmlReader for the results.</returns>
        public XmlReader ExecuteXmlReader (string procedureName, IDbTransaction tran, params object[] args) {
            return ExecuteXmlReader(procedureName, CommandType.StoredProcedure, tran, args);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns an
        /// XmlReader for the results.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>An XmlReader for the results.</returns>
        public XmlReader ExecuteXmlReader (string commandText, CommandType commandType, IDbTransaction tran, params object[] args) {
            using (var cmd = CreateCommand(commandText, commandType, tran, args))
                return ExecuteXmlReader(cmd);
        }
        #endregion

        #region RowsExist
        /// <summary>
        /// Executes the procedure with the specified criteria and returns an
        /// indicator on whether any rows exist in the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <returns>true if any rows exist in the results, false if otherwise.</returns>
        public bool RowsExist (string procedureName) {
            return RowsExist(procedureName, CommandType.StoredProcedure, null);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns an
        /// indicator on whether any rows exist in the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>true if any rows exist in the results, false if otherwise.</returns>
        public bool RowsExist (string procedureName, params object[] args) {
            return RowsExist(procedureName, CommandType.StoredProcedure, null, args);
        }

        /// <summary>
        /// Executes the procedure with the specified criteria and returns an
        /// indicator on whether any rows exist in the results.
        /// </summary>
        /// <param name="procedureName">The procedure name to execute.</param>
        /// <param name="tran">The transaction to execute the procedure under.</param>
        /// <param name="args">The procedure arguments.</param>
        /// <returns>true if any rows exist in the results, false if otherwise.</returns>
        public bool RowsExist (string procedureName, IDbTransaction tran, params IDbDataParameter[] args) {
            return RowsExist(procedureName, CommandType.StoredProcedure, tran, args);
        }

        /// <summary>
        /// Executes the command with the specified criteria and returns an
        /// indicator on whether any rows exist in the results.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>true if any rows exist in the results, false if otherwise.</returns>
        public bool RowsExist (string commandText, CommandType commandType, IDbTransaction tran, params IDbDataParameter[] args) {
            var row = ExecuteDataRow(commandText, commandType, tran, args);
            return row != null;
        }
        #endregion
        #endregion

        #region SQL Execution
        #region DataReader
        /// <summary>
        /// Executes the SQL and returns an IDataReader for the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <returns>An IDataReader for the results.</returns>
        public IDataReader ExecuteDataReaderSql (string sql) {
            return ExecuteDataReaderSql(sql, null);
        }

        /// <summary>
        /// Executes the SQL and returns an IDataReader for the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <param name="tran">The transaction to execute the SQL under.</param>
        /// <returns>An IDataReader for the results.</returns>
        public IDataReader ExecuteDataReaderSql (string sql, IDbTransaction tran) {
            return ExecuteDataReader(sql, CommandType.Text, tran);
        }
        #endregion

        #region DataRow
        /// <summary>
        /// Executes the SQL and returns the first row of the first table of
        /// the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <returns>The first row of the first table of the results.</returns>
        public DataRow ExecuteDataRowSql (string sql) {
            return ExecuteDataRowSql(sql, null);
        }

        /// <summary>
        /// Executes the SQL and returns the first row of the first table of
        /// the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <param name="tran">The transaction to execute the SQL under.</param>
        /// <returns>The first row of the first table of the results.</returns>
        public DataRow ExecuteDataRowSql (string sql, IDbTransaction tran) {
            return ExecuteDataRow(sql, CommandType.Text, tran);
        }
        #endregion

        #region DataSet
        /// <summary>
        /// Executes the SQL and returns a DataSet containing the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <returns>A DataSet containing the results.</returns>
        public DataSet ExecuteDataSetSql (string sql) {
            return ExecuteDataSetSql(sql, null);
        }

        /// <summary>
        /// Executes the SQL and returns a DataSet containing the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <param name="tran">The transaction to execute the SQL under.</param>
        /// <returns>A DataSet containing the results.</returns>
        public DataSet ExecuteDataSetSql (string sql, IDbTransaction tran) {
            return ExecuteDataSet(sql, CommandType.Text, tran);
        }
        #endregion

        #region DataTable
        /// <summary>
        /// Executes the SQL and returns the first table of the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <returns>The first table of the results.</returns>
        public DataTable ExecuteDataTableSql (string sql) {
            return ExecuteDataTableSql(sql, null);
        }

        /// <summary>
        /// Executes the SQL and returns the first table of the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <param name="tran">The transaction to execute the SQL under.</param>
        /// <returns>The first table of the results.</returns>
        public DataTable ExecuteDataTableSql (string sql, IDbTransaction tran) {
            return ExecuteDataTable(sql, CommandType.Text, tran);
        }
        #endregion

        #region NonQuery
        /// <summary>
        /// Executes the non-query SQL and returns the number of rows affected.
        /// </summary>
        /// <param name="sql">The non-query SQL text to execute.</param>
        /// <returns>The number of rows affected.</returns>
        public int ExecuteNonQuerySql (string sql) {
            return ExecuteNonQuerySql(sql, null);
        }

        /// <summary>
        /// Executes the non-query SQL and returns the number of rows affected.
        /// </summary>
        /// <param name="sql">The non-query SQL text to execute.</param>
        /// <param name="tran">The transaction to execute the SQL under.</param>
        /// <returns>The number of rows affected.</returns>
        public int ExecuteNonQuerySql (string sql, IDbTransaction tran) {
            return ExecuteNonQuery(sql, CommandType.Text, tran);
        }
        #endregion

        #region Scalars
        /// <summary>
        /// Executes the SQL and returns the first column of the first row of
        /// the first table of the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <returns>The first column of the first row of the first table of the results.</returns>
        public object ExecuteScalarSql (string sql) {
            return ExecuteScalarSql(sql, null);
        }

        /// <summary>
        /// Executes the SQL and returns the first column of the first row of
        /// the first table of the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <param name="tran">The transaction to execute the SQL under.</param>
        /// <returns>The first column of the first row of the first table of the results.</returns>
        public object ExecuteScalarSql (string sql, IDbTransaction tran) {
            return ExecuteScalar(sql, CommandType.Text, tran);
        }
        #endregion

        #region RowsExist
        /// <summary>
        /// Executes the SQL and returns an indicator on whether any rows exist
        /// in the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <returns>true if any rows exist in the results, false if otherwise.</returns>
        public bool RowsExistSql (string sql) {
            return RowsExistSql(sql, null);
        }

        /// <summary>
        /// Executes the SQL and returns an indicator on whether any rows exist
        /// in the results.
        /// </summary>
        /// <param name="sql">The SQL text to execute.</param>
        /// <param name="tran">The transaction to execute the SQL under.</param>
        /// <returns>true if any rows exist in the results, false if otherwise.</returns>
        public bool RowsExistSql (string sql, IDbTransaction tran) {
            return RowsExist(sql, CommandType.Text, tran);
        }
        #endregion
        #endregion
        #endregion

        #region Generation
        #region Adapter Generation
        /// <summary>
        /// Creates a data adapter to be used with this database.
        /// </summary>
        /// <param name="cmd">The select command this data adapter is for.</param>
        /// <returns>A data adapter to be used with this database.</returns>
        public abstract IDataAdapter CreateDataAdapter (IDbCommand cmd);
        #endregion

        #region Command Generation
        /// <summary>
        /// Creates a command to be used with this database.
        /// </summary>
        /// <returns>A command to be used with this database.</returns>
        protected abstract IDbCommand CreateCommandCore ();

        /// <summary>
        /// Creates a command ready to be used with this database.
        /// </summary>
        /// <returns>A command ready to be used with this database.</returns>
        public IDbCommand CreateCommand () {
            var cmd = CreateCommandCore();
            if (CommandTimeout != null)
                cmd.CommandTimeout = CommandTimeout.Value;
            return cmd;
        }

        /// <summary>
        /// Creates a command to be used with this database.
        /// </summary>
        /// <param name="commandType">The type of command.</param>
        /// <returns>A command to be used with this database.</returns>
        public IDbCommand CreateCommand (CommandType commandType) {
            var cmd = CreateCommand();
            cmd.CommandType = commandType;
            return cmd;
        }

        /// <summary>
        /// Creates a command to be used with this database.
        /// </summary>
        /// <param name="commandText">The text of the command.</param>
        /// <param name="commandType">The type of command.</param>
        /// <param name="tran">The transaction to execute the command under.</param>
        /// <param name="args">The command arguments.</param>
        /// <returns>A command to be used with this database.</returns>
        public IDbCommand CreateCommand (string commandText, CommandType commandType, IDbTransaction tran, params object[] args) {
            var cmd = CreateCommand(commandType);
            cmd.CommandText = commandText;

            if (tran != null) {
                cmd.Connection = tran.Connection;
                cmd.Transaction = tran;
            }

            if (commandType == CommandType.StoredProcedure)
            {
                var converted = ConvertParameters(cmd, args);
                ListEx.AddRange(cmd.Parameters, converted);
            }

            return cmd;
        }
        #endregion

        #region Connection Generation
        /// <summary>
        /// Creates a connection to be used with this database.
        /// </summary>
        /// <returns>A connection to be used with this database.</returns>
        public abstract IDbConnection CreateConnection ();

        /// <summary>
        /// Creates and opens a new connection to be used with this database.
        /// </summary>
        /// <returns>An open connection to be used with this database.</returns>
        public IDbConnection OpenConnection () {
            var conn = CreateConnection();
            conn.ConnectionString = ConnectionString;
            conn.Open();
            return conn;
        }
        #endregion

        #region Parameter Generation
        /// <summary>
        /// Derives the list of parameters needed to execute a procedure.
        /// </summary>
        /// <param name="procedureName">The procedure name.</param>
        /// <returns>An array of parameters to be used with the procedure.</returns>
        protected abstract IDbDataParameter[] DeriveParameters (string procedureName);
        #endregion

        /// <summary>
        /// Creates a formatter to be used with this database.
        /// </summary>
        /// <returns>A formatter to be used with this database.</returns>
        public virtual SqlFormatter CreateFormatter () {
            throw new NotSupportedException();
        }
        #endregion

        #region Maintenance
        /// <summary>
        /// Completely clears the database schema.  Use with caution!
        /// </summary>
        public virtual void Clear () {
            throw new NotImplementedException(LS.T("This database does not implement clearing."));
        }

        /// <summary>
        /// Initiates database cleanup.
        /// </summary>
        public virtual void Vacuum () {
            throw new NotImplementedException(LS.T("This database does not implement vacuuming."));
        }
        #endregion

        #region Shortcuts
        public string Escape (string value) {
            var formatter = CreateFormatter();
            return formatter.Escape(value);
        }

        public IDbCommand Format(SqlModel.SqlSelect sqlSelect)
        {
            var formatter = CreateFormatter();
            formatter.EmbedLiterals = true;
            return formatter.Generate(sqlSelect);
        }
        #endregion

        #region Schema
        public virtual DataSchema GetSchema()
        {
            throw new NotImplementedException(LS.T("This database does not implement schema inspection."));
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the default timeout of any command created for use with this database.
        /// </summary>
        [DataMember]
        public int? CommandTimeout { get; set; }

        /// <summary>
        /// Gets or sets the connection string to use when executing commands.
        /// </summary>
        [DataMember]
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the culture of the data stored in the database.
        /// </summary>
        public CultureInfo Culture { get; set; }

        // For serialization purposes only.
        [DataMember]
        private string CultureName {
            get { return Culture == null ? null : Culture.ToString(); }
            set { Culture = (value == null ? null : CultureInfo.GetCultureInfo(value)); }
        }

        /// <summary>
        /// Gets or sets the default DataSet DateTimeMode.
        /// </summary>
        [DataMember]
        public DataSetDateTime DateTimeMode { get; set; }

        public virtual bool AreModifiedRowsReturnedAccurate { get { return true; } }

        public virtual bool IsPrepareSupported { get { return true; } }
        #endregion
    }
}
