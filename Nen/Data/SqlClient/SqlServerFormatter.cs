using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.SqlModel;

namespace Nen.Data.SqlClient
{
    /// <summary>
    /// Formats SQL expressions into MS SQL Server SQL text.
    /// </summary>
    public class SqlServerFormatter : SqlFormatter
    {
        #region Constructor
        /// <summary>
        /// Constucts a new SqlServerFormatter.
        /// </summary>
        public SqlServerFormatter()
            : this(() => new System.Data.SqlClient.SqlCommand())
        { }


        public SqlServerFormatter(Func<IDbCommand> createCommand)
            : base(createCommand)
        {
            DateTimeDbType = DbType.DateTime2;
        }
        #endregion

        #region SqlFormatter Statement Visitation
        /// <summary>
        /// Formats a select statement.
        /// </summary>
        /// <param name="sqlSelect">The select to format.</param>
        public override void Visit(SqlSelect sqlSelect)
        {
            // Window structure:

            // SELECT TOP <limit> <*, or <sole column>>
            // FROM (
            //  SELECT <columns>, ROW_NUMBER() OVER(<orderbys>) AS __ROW_NUMBER__
            //  FROM <queries> <joins>
            //  WHERE <expression>
            // ) AS __WINDOW__
            // WHERE __ROW_NUMBER__ > <offset>

            // SELECT <column1, column2, ..., columnN>
            Text.Append("SELECT ");

            if (sqlSelect.IsDistinct)
                Text.Append(" DISTINCT ");

            if (sqlSelect.Limit.HasValue)
                Text.AppendFormat("TOP {0} ", sqlSelect.Limit.Value);

            if (sqlSelect.Offset.HasValue)
            {
                // An extra row number in the result set isn't a big deal, unless
                // this is a subselect that must return only one expression.
                // Handle this case.
                if (sqlSelect.Columns.Count == 1)
                {
                    Text.Append(sqlSelect.Columns.First().Alias);
                }
                else
                    Text.Append("*");

                Text.Append(" FROM (SELECT ");
            }

            VisitEach(", ", sqlSelect.Columns);

            if (sqlSelect.Offset.HasValue)
            {
                Text.Append(", ROW_NUMBER() OVER(");
                GenerateOrderBy(sqlSelect);
                Text.Append(") AS __ROW_NUMBER__");
            }

            if (sqlSelect.From.Count > 0)
            {
                // FROM <query1, query2, ..., queryN>
                Text.Append(" FROM ");
                VisitEach(", ", sqlSelect.From);
            }

            // <join1 join2 ... joinN>
            VisitEach(" ", sqlSelect.Joins);

            // WHERE <expression>
            if (sqlSelect.Where != null)
            {
                Text.Append(" WHERE ");
                sqlSelect.Where.Accept(this);
            }

            // ORDER BY is not necessary if ROW_NUMBER() is present.
            if (!sqlSelect.Offset.HasValue)
            {
                // ORDER BY <query1, query2, ..., queryN>
                GenerateOrderBy(sqlSelect);
            }

            if (sqlSelect.Offset.HasValue)
                Text.AppendFormat(") AS __WINDOW__ WHERE __ROW_NUMBER__ > {0}", sqlSelect.Offset.Value);

            // GROUP BY <query1, query2, ..., queryN>
            if (sqlSelect.GroupBy.Count > 0)
            {
                Text.Append(" GROUP BY ");
                VisitEach(", ", sqlSelect.GroupBy);
            }
        }
        #endregion

        #region SqlFormatter DataType and Declaration Visitation
        public override void Visit(SqlDeclaration declaration)
        {
            if (declaration == null)
                throw new ArgumentNullException("declaration");

            Text.Append("DECLARE @");
            declaration.Variable.Accept(this);
            Text.Append(" ");
            declaration.DataType.Accept(this);
        }

        /// <summary>
        /// Formats a globally unique identifier data type.
        /// </summary>
        /// <param name="guid">The globally unique identifier to format.</param>
        public override void Visit(SqlGuid guid)
        {
            Text.Append("UNIQUEIDENTIFIER");
        }

        /// <summary>
        /// Formats a date and time data type.
        /// </summary>
        /// <param name="dateTime">The date and time data type to format.</param>
        public override void Visit(SqlDateTime dateTime)
        {
            if (DateTimeDbType == DbType.DateTime2)
                Text.Append("DATETIME2");
            else
                Text.Append("DATETIME");
        }

        /// <summary>
        /// Formats a variable-length binary data type.
        /// </summary>
        /// <param name="binary">The variable-length binary data type to format.</param>
        public override void Visit(SqlVariableLengthBinary binary)
        {
            if (binary.Length == int.MaxValue)
                Text.Append("VARBINARY(MAX)");
            else
                Text.AppendFormat(CultureInfo.InvariantCulture, "VARBINARY({0})", binary.Length);
        }

        /// <summary>
        /// Formats a boolean data type.
        /// </summary>
        /// <param name="boolean">The boolean to format.</param>
        public override void Visit(SqlBoolean boolean)
        {
            Text.Append("BIT");
        }

        /// <summary>
        /// Formats a time data type.
        /// </summary>
        /// <param name="time">The time data type to format.</param>
        public override void Visit(SqlTime time)
        {
            Text.Append("TIME(7)");
        }

        /// <summary>
        /// Formats a 64 bit integer data type.
        /// </summary>
        /// <param name="int64">The 64 bit integer to format.</param>
        public override void Visit(SqlInt64 int64)
        {
            Text.Append("BIGINT");
        }
        #endregion

        #region SqlFormatter Column Visitation
        /// <summary>
        /// Formats a create column statement.
        /// </summary>
        /// <param name="createColumn">The create column to format.</param>
        public override void Visit(SqlCreateColumn createColumn)
        {
            base.Visit(createColumn);
        }
        #endregion

        #region SqlFormatter Fragment Visitation and Generation
        public override void Visit(SqlTable table)
        {
            base.Visit(table);

            if (table.WithHints.Count > 0)
            {
                Text.Append(" WITH (");
                VisitEach(", ", table.WithHints);
                Text.Append(")");
           }
        }

        public override void Visit(SqlIdentifier identifier)
        {
            if (identifier == null)
                throw new ArgumentNullException("identifier");

            if (identifier.IsDeclared)
                Text.Append("@");

            base.Visit(identifier);
        }

        public override void Visit(SqlParameter parameter)
        {
            base.Visit(parameter);

            if (parameter.DbType == DbType.Time)
            {
                var dbParam = (System.Data.SqlClient.SqlParameter)Parameters[parameter.Name];
                dbParam.SqlDbType = SqlDbType.Time;
            }
        }

        public override void Visit(SqlFunction function)
        {
            if (function == null)
                throw new ArgumentNullException("function");

            // Translate the first datediff/dateadd argument to something sql server can handle.
            if (function.FunctionName == "datediff" || function.FunctionName == "dateadd")
            {
                if (function.FunctionArguments == null || function.FunctionArguments.Count != 3)
                    throw new ArgumentException("dateadd and datediff take only three arguments", "function");

                Text.Append(function.FunctionName);
                Text.Append("(");

                var arguments = function.FunctionArguments.ToArray();
                var timeSpanProperty = (SqlLiteralExpression)arguments[0];
                Text.Append(GenerateDatePart((string)timeSpanProperty.Value));
                Text.Append(", ");
                arguments[1].Accept(this);
                Text.Append(", ");
                arguments[2].Accept(this);

                Text.Append(")");

                return;
            }

            base.Visit(function);
        }

        protected virtual string GenerateDatePart(string timeSpanProperty)
        {
            switch (timeSpanProperty) {
                case "Days": return "day";
                case "Hours": return "hour";
                case "Milliseconds": return "millisecond";
                case "Minutes": return "minute";
                case "Seconds": return "second";
            }

            throw new NotSupportedException();
        }

        /// <summary>
        /// Generates an identity fragment.
        /// </summary>
        protected override void GenerateAutoIncrement(SqlCreateColumn column)
        {
            var seed = column.AutoIncrementSeed == 0 ? 1 : column.AutoIncrementSeed;
            var step = column.AutoIncrementStep == 0 ? 1 : column.AutoIncrementStep;

            Text.AppendFormat(CultureInfo.InvariantCulture, "IDENTITY({0},{1})", seed, step);
        }
        #endregion

        #region SqlFormatter Miscellaneous
        /// <summary>
        /// Escapes an identifier if necessary.
        /// </summary>
        /// <param name="identifier">The identifier to format.</param>
        /// <returns>A formatted identifier.</returns>
        public override string FormatIdentifier(string identifier)
        {
            if (IdentifierRequiresEscaping(identifier))
                return string.Format(CultureInfo.InvariantCulture, "[{0}]", identifier);
            else
                return identifier;
        }

        /// <summary>
        /// Determines whether the identifier is a keyword.
        /// </summary>
        /// <param name="identifier">The possible keyword identifier.</param>
        /// <returns>true if the identifier is a keyword, false if otherwise.</returns>
        protected override bool IsKeyword(string identifier)
        {
            if (base.IsKeyword(identifier))
                return true;

            switch (identifier.ToLower())
            {
                case "user":
                case "open":
                case "read":
                case "public":
                    return true;
            }

            return false;
        }

        protected override DbType GetDbType(DbType type)
        {
            if (type == DbType.DateTime)
                return DateTimeDbType;
            else
                return base.GetDbType(type);
        }

        protected override IDbDataParameter CreateParameter(object value, DbType? dbType)
        {
            var parameter = (System.Data.SqlClient.SqlParameter)base.CreateParameter(value, dbType);

            if (dbType.HasValue && dbType == DbType.Time || (!dbType.HasValue && value is TimeSpan))
                ((System.Data.SqlClient.SqlParameter)parameter).SqlDbType = SqlDbType.Time;

            return parameter;
        }
        #endregion

        #region Types
        public DbType DateTimeDbType { get; set; }
        #endregion
    }
}
