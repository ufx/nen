using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Collections.ObjectModel;
using Nen.Collections;

namespace Nen.Data.SqlClient
{
    /// <summary>
    /// Represents a Microsoft SqlServer database.
    /// </summary>
    [DataContract]
    public class SqlDatabase : Database
    {
        #region Constructors
        /// <summary>
        /// Constructs a new SqlDatabase.
        /// </summary>
        public SqlDatabase()
            : this(CultureInfo.CurrentCulture)
        {
        }

        /// <summary>
        /// Constructs a new SqlDatabase.
        /// </summary>
        /// <param name="connectionString">The connection string to use when executing commands.</param>
        public SqlDatabase(string connectionString)
            : this(connectionString, CultureInfo.CurrentCulture)
        {
        }

        /// <summary>
        /// Constructs a new SqlDatabase.
        /// </summary>
        /// <param name="culture">The culture of the data stored in the database.</param>
        public SqlDatabase(CultureInfo culture)
            : this(null, culture)
        {
        }

        /// <summary>
        /// Constructs a new SqlDatabase.
        /// </summary>
        /// <param name="connectionString">The connection string to use when executing commands.</param>
        /// <param name="culture">The culture of the data stored in the database.</param>
        public SqlDatabase(string connectionString, CultureInfo culture)
            : base(connectionString, culture)
        {
            DateTimeDbType = DbType.DateTime2;
        }
        #endregion

        #region Command Execution
        /// <summary>
        /// Executes the command and returns an XmlReader for the results.
        /// This method can be called on commands that have no current
        /// connection.  A new connection will be opened for the command.
        /// </summary>
        /// <param name="cmd">The command to execute.</param>
        /// <returns>An XmlReader for the command results.</returns>
        public override XmlReader ExecuteXmlReader(IDbCommand cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            var sqlCmd = (SqlCommand)cmd;

            try
            {
                if (sqlCmd.Transaction != null)
                    return sqlCmd.ExecuteXmlReader();
                else
                {
                    using (var conn = (SqlConnection)OpenConnection())
                    {
                        sqlCmd.Connection = conn;
                        return sqlCmd.ExecuteXmlReader();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data["Nen.Database.ExecutingCommand"] = CreateFormatter().PrintCommand(cmd);
                throw;
            }
        }
        #endregion

        #region Generation
        /// <summary>
        /// Creates a data adapter to be used with this database.
        /// </summary>
        /// <param name="cmd">The select command this data adapter is for.</param>
        /// <returns>A data adapter to be used with this database.</returns>
        public override IDataAdapter CreateDataAdapter(IDbCommand cmd)
        {
            return new SqlDataAdapter((SqlCommand)cmd);
        }

        /// <summary>
        /// Creates a command to be used with this database.
        /// </summary>
        /// <returns>A command to be used with this database.</returns>
        protected override IDbCommand CreateCommandCore()
        {
            return new SqlCommand();
        }

        /// <summary>
        /// Creates a connection to be used with this database.
        /// </summary>
        /// <returns>A connection to be used with this database.</returns>
        public override IDbConnection CreateConnection()
        {
            return new SqlConnection();
        }

        /// <summary>
        /// Creates a formatter to be used with this database.
        /// </summary>
        /// <returns>A formatter to be used with this database.</returns>
        public override SqlFormatter CreateFormatter()
        {
            return new SqlServerFormatter() { DateTimeDbType = DateTimeDbType };
        }

        /// <summary>
        /// Derives the list of parameters needed to execute a procedure.
        /// </summary>
        /// <param name="procedureName">The procedure name.</param>
        /// <returns>An array of parameters to be used with the procedure.</returns>
        protected override IDbDataParameter[] DeriveParameters(string procedureName)
        {
            //todo: cache these
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = procedureName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;
                    SqlCommandBuilder.DeriveParameters(cmd);
                    return cmd.Parameters.Cast<SqlParameter>().Skip(1).Select(p => (SqlParameter)((ICloneable)p).Clone()).ToArray();
                }
            }
        }
        #endregion

        #region Types
        public DbType DateTimeDbType { get; set; }
        #endregion

        #region Schema
        public override DataSchema GetSchema()
        {
            var schema = new DataSchema();

            // Fetch tables and their schema
            schema.Tables = GetSchemaTables();

            return schema;
        }

        private Collection<DataTable> GetSchemaTables()
        {
            // todo: indexes
            // todo: other constraints

            var tablesByName = new Dictionary<string, DataTable>();

            var sql = @"
select TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE
from INFORMATION_SCHEMA.COLUMNS
order by COLUMN_NAME
";

            var columnInfo = ExecuteDataTableSql(sql);
            foreach (DataRow row in columnInfo.Rows)
            {
                // Create table if it doesn't exist.
                var fullTableName = (string)row["TABLE_SCHEMA"] + "." + (string)row["TABLE_NAME"];
                DataTable table;
                if (tablesByName.ContainsKey(fullTableName))
                    table = tablesByName[fullTableName];
                else
                {
                    table = new DataTable((string)row["TABLE_NAME"]);
                    table.SetSchemaName((string)row["TABLE_SCHEMA"]);
                    tablesByName[fullTableName] = table;
                }

                // Add this column's data.
                var column = table.Columns.Add((string)row["COLUMN_NAME"]);
                //column.SetOrdinal((int)row["ORDINAL_POSITION"]);

                if (!row.IsNull("COLUMN_DEFAULT")) // todo: not a literal...
                    column.SetDefaultExpression(new Nen.Data.SqlModel.SqlLiteralExpression((string)row["COLUMN_DEFAULT"]));

                column.AllowDBNull = (string)row["IS_NULLABLE"] == "YES";
                column.SetDbType(MapDbTypeString((string)row["DATA_TYPE"]));

                if (!row.IsNull("CHARACTER_MAXIMUM_LENGTH"))
                {
                    var len = (int)row["CHARACTER_MAXIMUM_LENGTH"];
                    column.MaxLength = len;
                    column.SetMaxBinaryLength(len);
                }

                if (!row.IsNull("NUMERIC_PRECISION"))
                    column.SetPrecision((byte)row["NUMERIC_PRECISION"]);

                if (!row.IsNull("NUMERIC_SCALE"))
                    column.SetScale((int)row["NUMERIC_SCALE"]);
            }

            GetTableConstraints(tablesByName);

            var tables = tablesByName.Values
                .OrderBy(d => d.GetSchemaName())
                .ThenBy(d => d.TableName);

            return new Collection<DataTable>(tables.ToList());
        }

        private void GetTableConstraints(Dictionary<string, DataTable> tablesByName)
        {
            var pkConstraintsByName = new Dictionary<string, List<DataColumn>>();
            var fkConstraintsByName = new Dictionary<string, List<Tuple<string, DataColumn>>>();

            var sql = @"
select ccu.TABLE_SCHEMA as ConstraintSchemaName, ccu.TABLE_NAME as ConstraintTableName, ccu.COLUMN_NAME as ConstraintColumnName, ccu.CONSTRAINT_NAME as ConstraintName,
pkr.UNIQUE_CONSTRAINT_SCHEMA AS PKSchemaName, pkr.UNIQUE_CONSTRAINT_NAME AS PKConstraintName, tc.CONSTRAINT_TYPE AS ConstraintType
from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
join INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc ON tc.TABLE_SCHEMA = ccu.TABLE_SCHEMA AND tc.CONSTRAINT_NAME = ccu.CONSTRAINT_NAME
left join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS pkr ON ccu.TABLE_SCHEMA = pkr.CONSTRAINT_SCHEMA AND ccu.CONSTRAINT_NAME = pkr.CONSTRAINT_NAME
";
            var constraintInfo = ExecuteDataTableSql(sql);
            foreach (DataRow row in constraintInfo.Rows)
            {
                var fullConstraintTableName = (string)row["ConstraintSchemaName"] + "." + (string)row["ConstraintTableName"];
                var fullConstraintName = (string)row["ConstraintSchemaName"] + "_" + (string)row["ConstraintName"];
                var constraintTable = tablesByName[fullConstraintTableName];

                var table = tablesByName[fullConstraintTableName];
                var column = table.Columns[(string)row["ConstraintColumnName"]];

                var constraintType = (string)row["ConstraintType"];
                if (constraintType == "PRIMARY KEY")
                    pkConstraintsByName.Activate(fullConstraintName).Add(column);
                else if (constraintType == "FOREIGN KEY")
                {
                    var fullPKConstraintName = (string)row["PKSchemaName"] + "_" + (string)row["PKConstraintName"];
                    fkConstraintsByName.Activate(fullConstraintName).Add(Tuple.Create(fullPKConstraintName, column));
                }
            }

            // Roll all constraints into each table, starting with primary keys.
            foreach (var entry in pkConstraintsByName)
                entry.Value[0].Table.PrimaryKey = entry.Value.ToArray();

            // Now create foreign keys.
            foreach (var entry in fkConstraintsByName)
            {
                var pks = new List<DataColumn>();
                var fks = new List<DataColumn>();
                foreach (var association in entry.Value)
                {
                    pks.AddRange(pkConstraintsByName[association.Item1]);
                    fks.Add(association.Item2);
                }

                var table = fks[0].Table;
                //                var fkConstraints = table.Constraints.OfType<ForeignKeyConstraint>();
                //                if (!fkConstraints.Any(c => c.Columns.SequenceEqual(fks) && c.RelatedColumns.SequenceEqual(pks)))

                try
                {
                    table.Constraints.Add(entry.Key, pks.ToArray(), fks.ToArray());
                }
                catch (Exception)
                {
                    // Squelch constraint exceptions.
                }
            }
        }

        private static DbType MapDbTypeString(string value)
        {
            switch (value)
            {
                case "bit": return DbType.Boolean;
                case "decimal": return DbType.Decimal;
                case "int": return DbType.Int32;
                case "smallint": return DbType.Int16;
                case "tinyint": return DbType.Byte;
                case "varbinary": return DbType.Binary;
                case "time": return DbType.Time;
                case "uniqueidentifer": return DbType.Guid;
                case "datetime": return DbType.DateTime;
                case "datetime2": return DbType.DateTime2;
                case "nvarchar": return DbType.String;
                case "nchar": return DbType.StringFixedLength;
                case "varchar": return DbType.AnsiString;
                case "char": return DbType.AnsiStringFixedLength;
                case "money": return DbType.Currency;
                default: return DbType.Object;
            }
        }
        #endregion
    }
}
