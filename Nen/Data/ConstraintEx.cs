﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.SqlModel;
using Nen.Internal;

namespace Nen.Data {
    /// <summary>
    /// Extensions to the Constraint class.
    /// </summary>
    public static class ConstraintEx {
        /// <summary>
        /// Emits data definitions to create the constraint.
        /// </summary>
        /// <param name="constraint">The constraint to emit data definitions for.</param>
        /// <returns>A constraint definition.</returns>
        public static SqlConstraint GenerateDataDefinition (this Constraint constraint) {
            if (constraint == null)
                throw new ArgumentNullException("constraint");

            if (constraint is UniqueConstraint)
                return GenerateDataDefinition((UniqueConstraint) constraint);
            else if (constraint is ForeignKeyConstraint)
                return GenerateDataDefinition((ForeignKeyConstraint) constraint);
            else
                throw new ArgumentException(LS.T("Unknown constraint type '{0}'", constraint.GetType()), "constraint");
        }

        private static SqlConstraint GenerateDataDefinition (ForeignKeyConstraint constraint) {
            var sqlConstraint = new SqlForeignKeyConstraint();

            sqlConstraint.Name = new SqlIdentifier(constraint.ConstraintName);
            sqlConstraint.ForeignKeyColumn = new SqlColumn(constraint.Columns[0].ColumnName);
            sqlConstraint.PrimaryKeyColumn = new SqlColumn(constraint.RelatedColumns[0].ColumnName);
            sqlConstraint.PrimaryKeyTableName = new SqlIdentifier(constraint.RelatedTable.TableName);

            if (constraint.RelatedTable.GetSchemaName() != null)
                sqlConstraint.PrimaryKeySchemaName = new SqlIdentifier(constraint.RelatedTable.GetSchemaName());

            return sqlConstraint;
        }

        private static SqlConstraint GenerateDataDefinition (UniqueConstraint constraint) {
            if (constraint.IsPrimaryKey) {
                var sqlConstraint = new SqlPrimaryKeyConstraint();
                sqlConstraint.Name = new SqlIdentifier(constraint.ConstraintName);
                sqlConstraint.Columns.AddRange(constraint.Columns.Select(c => new SqlColumn(c.ColumnName)));
                return sqlConstraint;
            } else
                throw new NotImplementedException();
        }
    }
}
