﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.SqlModel;

namespace Nen.Data {
    /// <summary>
    /// Extensions to the DataSet class.
    /// </summary>
    public static class DataSetEx {
        /// <summary>
        /// Emits SQL to create all tables and insert data in the data set.
        /// </summary>
        /// <param name="set">The set to emit SQL for.</param>
        /// <returns>A compound expression containing all SQL.</returns>
        public static SqlCompoundExpression GenerateSql (this DataSet set) {
            var compound = new SqlCompoundExpression();

            var orderedTables = set.Tables.OfType<DataTable>().OrderBy(t => t.GetSchemaName()).ThenBy(t => t.TableName).ToList();

            // First create the tables...
            foreach (var table in orderedTables)
                compound.Expressions.AddRange(table.GenerateDataDefinitions());

            // ... Then establish foreign key constraints ...
            foreach (var table in orderedTables) {
                var alterTable = table.GenerateForeignKeyConstraintDefinitions();
                if (alterTable != null)
                    compound.Expressions.Add(alterTable);
            }

            // ... Then insert data.
            foreach (var table in orderedTables) {
                var inserts = table.GenerateInserts();
                if (inserts != null)
                    compound.Expressions.AddRange(inserts.Expressions);
            }

            return compound;
        }

        public static bool ContainsSubset (this DataSet set, DataSet subSet) {
            foreach (DataTable subTable in subSet.Tables) {
                if (!set.Tables.Contains(subTable.TableName))
                    return false;

                var table = set.Tables[subTable.TableName];
                if (!table.ContainsSubset(subTable))
                    return false;
            }

            return true;
        }
    }
}
