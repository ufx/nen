﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Reflection;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Data.ObjectRelationalMapper.Mapping;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Defers resolving of a keyed computation.
    /// </summary>
    public interface IDataRef : IFuture {
        /// <summary>
        /// Gets the key of the computation.
        /// </summary>
        object Key { get; }
    }

    /// <summary>
    /// Defers resolving of a keyed computation.
    /// </summary>
    /// <typeparam name="T">The type of result.</typeparam>
    public interface IDataRef<T> : IDataRef, IFuture<T> {
    }

    /// <summary>
    /// Defers resolving of a keyed object.
    /// </summary>
    /// <typeparam name="T">The type of result.</typeparam>
    public class DataRef<T> : FutureCore<T>, IDataRef<T> {
        private MapConfiguration _configuration;
        private object _key;
        private DirectVariableMap _target;

        #region Constructors
        /// <summary>
        /// Constructs a new DataRef for the given key.
        /// </summary>
        /// <param name="key">The key of the object to lazily load.</param>
        /// <param name="configuration">The configuration that contains mapping information for the result type.</param>
        public DataRef (object key, MapConfiguration configuration) {
            _configuration = configuration;
            _key = key;
        }

        /// <summary>
        /// Constructs a new DataRef for the given key.
        /// </summary>
        /// <param name="key">The key of the declaring object.</param>
        /// <param name="configuration">The configuration that contains mapping information for the declaring type.</param>
        /// <param name="target">The target variable to lazy load.</param>
        public DataRef (object key, MapConfiguration configuration, DirectVariableMap target) {
            _configuration = configuration;
            _key = key;
            _target = target;
        }

        /// <summary>
        /// Constructs a new DataRef with a default result and no key.
        /// </summary>
        public DataRef ()
            : base(default(T)) {
        }

        /// <summary>
        /// Constructs a new DataRef with the given result and no key.
        /// </summary>
        /// <param name="value">The not so lazy result.</param>
        public DataRef (T value)
            : base(value) {
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the key of the computation.
        /// </summary>
        public object Key {
            get { return _key; }
            set {
                if (!Equals(_key, value)) {
                    _key = value;
                    Result = default(T);
                }
            }
        }

        /// <summary>
        /// Determines whether the result has been loaded or provided.
        /// </summary>
        public override bool IsResolved {
            get { return _key == null || Result != null; }
        }

        /// <summary>
        /// Gets or sets the lazy result.  Retrieving this value resolves the
        /// computation and loads the object.
        /// </summary>
        public T Value {
            get { return Resolve(); }
            set {
                _key = null;
                Result = value;
            }
        }
        #endregion

        #region Lazy Loading
        /// <summary>
        /// Retrieves the value if is not already been retrieved.
        /// </summary>
        /// <returns>The object with the specified value or key.</returns>
        public override T Resolve () {
            if (!IsResolved) {
                using (var context = _configuration.CreateContext()) {
                    if (_target == null)
                        Result = context.Load<T>(_key);
                    else {
                        var type = _target.Variable.Member.DeclaringType;
                        var tableMap = _configuration.TableMaps[type];

                        var query = new QueryExpression(context);
                        var pk = new VariableExpression(tableMap.PrimaryKey.VariableMap.Variable, query);
                        var target = new VariableExpression(_target.Variable, query);
                        query.Subject = new TableExpression(_configuration.TableMaps[type]);
                        query.Projection.Items.Add(target);
                        query.WhereCriteria = new BinaryOperatorExpression(pk, BinaryOperatorType.Equal, new LiteralExpression(_key));
                        query.Advice.Add(new AdviceExpression(AdviceRequests.Include, target));
                        query.Shape = QueryShape.FirstOrDefault;
                        query.IsScalar = true;
                        query.IsGenerated = true;

                        Result = (T) query.ExecuteScalar();

                        // Merge the result into the change tracker if applicable.
                        if (_target is LazyPrimitiveVariableMap && context.TrackedData != null) {
                            if (context.TrackedData.ContainsTable(tableMap.TableName)) {
                                var table = context.TrackedData[tableMap.TableName];
                                var row = table.FindRowByPrimaryKey(_key);
                                if (row != null)
                                    row[_target.ColumnName] = Result;
                            }
                        }

                        // Removing the key will cause the DataRef to consider
                        // itself resolved if the result is null.
                        _key = null;
                    }
                    OnResolved();
                }
            }

            return Result;
        }

        /// <summary>
        /// Raised when the object has been loaded.
        /// </summary>
        public override event EventHandler<ComputationResultEventArgs<T>> Resolved;

        /// <summary>
        /// Called when the object has been loaded.
        /// </summary>
        protected virtual void OnResolved () {
            if (Resolved != null)
                Resolved(this, new ComputationResultEventArgs<T>(Result));
        }
        #endregion

        #region Key Equality
        /// <summary>
        /// Determines whether the key of an object equals the key stored in
        /// this Lazy.  If a key is not specified, the instances are checked
        /// for equality instead.
        /// </summary>
        /// <param name="obj">The object to determine key equality for.</param>
        /// <returns>true if the keys were equal, false if otherwise.</returns>
        public bool KeyEquals (T obj) {
            // Directly check results when no key exists.
            if (Key == null)
                return Equals(Result, obj);

            // A key for a value exists but the passed object is its default,
            // therefore the values can't be equal.
            if (Equals(obj, default(T)))
                return false;

            // Compare keys.
            var otherKey = _configuration.GetKey(obj);
            return Equals(Key, otherKey);
        }
        #endregion
    }

    internal static class DataRef {
        public static IDataRef Create (Type dataRefArgumentType, object key, MapConfiguration configuration) {
            return (IDataRef) ActivatorEx.CreateGenericInstance(typeof(DataRef<>), dataRefArgumentType, new Type[] { typeof(object), typeof(MapConfiguration) }, key, configuration);
        }

        public static IDataRef Create (Type dataRefArgumentType, object key, MapConfiguration configuration, DirectVariableMap target) {
            return (IDataRef) ActivatorEx.CreateGenericInstance(typeof(DataRef<>), new Type[] { dataRefArgumentType }, key, configuration, target);
        }

        public static IDataRef Create (Type dataRefArgumentType, object value) {
            return (IDataRef) ActivatorEx.CreateGenericInstance(typeof(DataRef<>), dataRefArgumentType, dataRefArgumentType, value);
        }
    }
}
