using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Transactions;

using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// A Store executes queries with some underlying source of data.
    /// </summary>
    public abstract class Store {
        #region Constructor
        /// <summary>
        /// Constructs a new Store.
        /// </summary>
        /// <param name="configuration">The configuration used by the store.</param>
        protected Store (MapConfiguration configuration) {
            Configuration = configuration;
        }
        #endregion

        #region Transactions
        /// <summary>
        /// Persists a set of changes, wrapping the persistence in PreCommit
        /// and PostCommit calls and a transaction.
        /// </summary>
        /// <param name="changes">The changes to commit.</param>
        public void Commit (ChangeSet changes) {
            WithTransaction<object>(changes.Context, (conn, tran) =>
            {
                PreCommit(changes);
                CommitCore(changes, conn, tran);
                PostCommit(changes);
                return null;
            });
        }

        /// <summary>
        /// Core implementation to persist a set of changes.
        /// </summary>
        /// <param name="changes">The changes to commit.</param>
        protected internal abstract void CommitCore (ChangeSet changes, IDbConnection conn, IDbTransaction tran);

        /// <summary>
        /// Tells all operations in the change set to execute pre-commit operations.
        /// </summary>
        /// <param name="changes">The changes to pre-commit.</param>
        protected virtual void PreCommit (ChangeSet changes) {
            foreach (var op in changes.OperationsByInstance.Values)
                op.PreCommit();
        }

        /// <summary>
        /// Tells all operations in the change set to execute post-commit operations.
        /// </summary>
        /// <param name="changes">The changes to post-commit.</param>
        protected virtual void PostCommit (ChangeSet changes) {
            foreach (var op in changes.OperationsByInstance.Values)
                op.PostCommit();
        }

        /// <summary>
        /// Executes a query within a transaction.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <returns>The result of the query.</returns>
        public DataQueryResults Query (QueryExpression query) {
            return WithTransaction(query.Context, (conn, tran) => QueryCore(query, conn, tran));
        }

        private T WithTransaction<T>(DataContext context, Func<IDbConnection, IDbTransaction, T> action)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            IDbConnection transientConnection = null;
            try
            {
                var operationConnection = context.DbConnection;
                if (operationConnection == null)
                {
                    transientConnection = OpenDbConnection();
                    operationConnection = transientConnection;
                }

                using (var tran = SetupTransaction(context, operationConnection))
                {
                    var result = action(operationConnection, tran);
                    if (tran != null)
                        tran.Commit();
                    return result;
                }
            }
            finally
            {
                if (transientConnection != null)
                    transientConnection.Close();
            }
        }

        /// <summary>
        /// Core implementation to execute a query.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <returns>The result of the query.</returns>
        protected abstract DataQueryResults QueryCore(QueryExpression query, IDbConnection conn, IDbTransaction tran);

        private IDbTransaction SetupTransaction(DataContext context, IDbConnection conn)
        {
            var scope = context.TransactionScopeOption ?? Configuration.TransactionScopeOption;
            if (scope == TransactionScopeOption.Suppress)
                return null;

            var isolationLevel = context.IsolationLevel ?? Configuration.IsolationLevel;
            return conn.BeginTransaction(isolationLevel);
        }
        #endregion

        #region Database
        /// <summary>
        /// Opens a database connection, if applicable.  For non-database
        /// stores this method returns null.
        /// </summary>
        /// <returns>An open connection, or null if the store is not connected to a database.</returns>
        public abstract IDbConnection OpenDbConnection();


        /// <summary>
        /// Indicates if prepared commands are supported by the store.
        /// </summary>
        public abstract bool IsPrepareSupported { get; }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the configuration used by the store.
        /// </summary>
        public MapConfiguration Configuration { get; private set; }
        #endregion
    }
}
