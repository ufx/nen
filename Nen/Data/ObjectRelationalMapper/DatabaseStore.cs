using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Linq;

using Nen.Data.SqlModel;
using Nen.Data.ObjectRelationalMapper.QueryEngine;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Executes queries against a database.
    /// </summary>
    public class DatabaseStore : Store {
        public Database Database { get; private set; }

        #region Constructor
        /// <summary>
        /// Constructs a new DatabaseStore.
        /// </summary>
        /// <param name="configuration">The configuration used by the store.</param>
        /// <param name="database">The database used by the store.</param>
        public DatabaseStore (MapConfiguration configuration, Database database)
            : base(configuration) {
            if (database == null)
                throw new ArgumentNullException("database");

            Database = database;
        }
        #endregion

        /// <summary>
        /// Persists the changes to the database.
        /// </summary>
        /// <param name="changes">The changes to commit.</param>
        protected internal override void CommitCore (ChangeSet changes, IDbConnection conn, IDbTransaction tran) {
            var context = changes.Context;
            var formatter = Database.CreateFormatter();

            var operations = changes.GetOrderedOperations();
            foreach (var operation in operations) {
                int numberOfStatements;
                bool isPrepared;
                var command = operation.GenerateCommand(formatter, conn, tran, out numberOfStatements, out isPrepared);

                try
                {
                    if (changes.Context.Log != null)
                        changes.Context.Log.WriteLine(formatter.PrintCommand(command));

                    command.Connection = conn;
                    command.Transaction = tran;

                    var primaryKey = operation.TableMap.PrimaryKey;
                    if (operation is InsertChangeOperation)
                        CommitInsert((InsertChangeOperation)operation, command);
                    else if (operation is UpdateChangeOperation)
                        CommitUpdate((UpdateChangeOperation)operation, command, numberOfStatements);
                    else
                        Database.ExecuteNonQuery(command);

                    changes.Context.OnChangeOperationCommitted(operation);
                }
                finally
                {
                    if (!isPrepared)
                        command.Dispose();
                }
            }
        }

        private void CommitInsert (InsertChangeOperation insert, IDbCommand command) {
            if (insert.TableMap.InsertReturnsValue) {
                using (var reader = Database.ExecuteDataReader(command))
                    insert.ProcessReturnedValue(reader);
            } else
                Database.ExecuteNonQuery(command);
        }

        private void CommitUpdate (UpdateChangeOperation update, IDbCommand command, int numStatements) {
            var rowsAffected = Database.ExecuteNonQuery(command);
            if (rowsAffected < numStatements && Database.AreModifiedRowsReturnedAccurate)
                throw new UpdateException(LS.T("Update to '{0}' hierarchy with key '{1}' affected {2}/{3} rows.", update.TableMap, update.Key, rowsAffected, numStatements));
        }

        /// <summary>
        /// Fulfills the query using the data in the database.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <returns>The result of the query.</returns>
        protected override DataQueryResults QueryCore(QueryExpression query, IDbConnection conn, IDbTransaction tran)
        {
            var engine = new DataQueryEngine(query, Database);
            return engine.Execute(conn, tran);
        }

        public override IDbConnection OpenDbConnection()
        {
            return Database.OpenConnection();
        }

        public override bool IsPrepareSupported
        {
            get { return Database.IsPrepareSupported; }
        }
    }
}
