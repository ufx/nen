using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;
using Nen.Reflection;

namespace Nen.Data.ObjectRelationalMapper {
    internal interface ILazyCollection {
        IFuture CreateListFuture (IFuture obj, CollectionVariableMap collectionVariableMap);
    }

    internal class LazyCollection<T> : ILazyCollection {
        public IFuture CreateListFuture (IFuture obj, CollectionVariableMap collectionVariableMap) {
            return new ListFuture<T>(delegate { return LoadList(obj, collectionVariableMap); });
        }

        private static List<T> LoadList (IFuture obj, CollectionVariableMap collectionVariableMap) {
            var configuration = collectionVariableMap.CollectionTypeMap.Configuration;
            if (configuration.DefaultStore == null)
                throw new MapConfigurationException(LS.T("A DefaultStore must be specified in order to load a LazyCollection."));

            var keyVariable = collectionVariableMap.KeyVariableMap.Variable;

            var instance = obj.Resolve();

            // Create query for [CollectionType] where KeyVariableName = declaring instance PK
            using (var context = configuration.CreateContext()) {
                var query = new QueryExpression(context, collectionVariableMap.CollectionArgumentTableMap);
                query.IsGenerated = true;
                query.OrderBy.AddRange(collectionVariableMap.OrderBy);

                var key = collectionVariableMap.PhysicalTableMap.GetKey(instance);
                query.WhereCriteria = new BinaryOperatorExpression(new VariableExpression(keyVariable, query), BinaryOperatorType.Equal, new LiteralExpression(key));

                if (collectionVariableMap.Filter != null)
                    query.WhereCriteria = BinaryOperatorExpression.Join(query.WhereCriteria, BinaryOperatorType.And, collectionVariableMap.Filter);

                // Ignore the list backreference, as they will be overwritten below.
                query.Advice.Add(new AdviceExpression(AdviceRequests.Ignore, new VariableExpression(keyVariable, query)));

                // Set key variable and add results to the result list.
                var list = new List<T>();
                var results = query.ExecuteSequence();

                // Wrap the key in a future accessing interface if necessary.
                if (keyVariable.MemberType.IsInstanceOfGenericType(typeof(DataRef<>)))
                    keyVariable = new FutureVariableInfo(keyVariable);

                var resultPrimaryKeyVariable = collectionVariableMap.CollectionArgumentTableMap.PrimaryKey.VariableMap.Variable;

                foreach (var item in results) {
                    list.Add((T) item);
                    keyVariable.SetValue(item, instance);

                    // Backfill change tracking row information, in case this reference
                    // ever needs to be set to null.
                    if (context.TrackedData != null)
                    {
                        var table = context.TrackedData[collectionVariableMap.CollectionArgumentTableMap.TableName];
                        var pk = resultPrimaryKeyVariable.GetValue(item);
                        var row = table.FindRowByPrimaryKey(pk);
                        row[collectionVariableMap.KeyVariableMap.ColumnName] = key;
                    }
                }

                return list;
            }
        }
    }

    internal static class LazyCollection {
        public static IFuture Create (Type collectionArgumentType, IFuture obj, CollectionVariableMap collectionVariableMap) {
            var lazyCollection = (ILazyCollection) ActivatorEx.CreateGenericInstance(typeof(LazyCollection<>), collectionArgumentType);
            return lazyCollection.CreateListFuture(obj, collectionVariableMap);
        }
    }
}
