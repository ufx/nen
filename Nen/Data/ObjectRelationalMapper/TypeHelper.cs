﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using Nen.Internal;
using Nen.Reflection;

namespace Nen.Data.ObjectRelationalMapper {
    internal static class TypeHelper {
        private static Type GetResultType (Type expressionType) {
            var enumerableType = GetEnumerableType(expressionType);
            if (enumerableType == null)
                return expressionType;
            return enumerableType.GetGenericArguments()[0];
        }

        private static Type GetEnumerableType (Type type) {
            if (type == null || type == typeof(string))
                return null;

            if (type.IsArray)
                return typeof(IEnumerable<>).MakeGenericType(new Type[] { type.GetElementType() });

            if (type.IsGenericType) {
                foreach (var genericArgument in type.GetGenericArguments()) {
                    var enumerableGeneric = typeof(IEnumerable<>).MakeGenericType(new Type[] { genericArgument });
                    if (enumerableGeneric.IsAssignableFrom(type))
                        return enumerableGeneric;
                }
            }

            var interfaces = type.GetInterfaces();
            if (interfaces != null && interfaces.Length > 0) {
                foreach (var interfaceType in interfaces) {
                    var enumerableInterface = GetEnumerableType(interfaceType);
                    if (enumerableInterface != null)
                        return enumerableInterface;
                }
            }

            if (type.BaseType != typeof(object))
                return GetEnumerableType(type.BaseType);

            return null;
        }

        public static IQueryable MakeQueryableFromExpression (Expression expression, Type instanceType, DataContext context) {
            var resultType = TypeHelper.GetResultType(expression.Type);

            // The below check may be important to something, but it stops
            // scalar expressions from being pumped through this function.
            // Let's find something more suitable if this check is important.

            //var queryableResult = typeof(IQueryable<>).MakeGenericType(new Type[] { resultType });
            //if (!queryableResult.IsAssignableFrom(expression.Type))
            //    throw new ArgumentException(LS.T("Expression must have an IQueryable type."), "expression");

            return (IQueryable) ActivatorEx.CreateGenericInstance(instanceType, resultType, new Type[] { typeof(DataContext), typeof(Expression) }, context, expression);
        }
    }
}
