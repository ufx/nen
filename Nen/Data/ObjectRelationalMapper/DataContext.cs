﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;
using Nen.Reflection;
using Nen.Runtime.Serialization;
using Nen.Caching;
using System.Transactions;
using System.Collections.ObjectModel;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Specifies options for all operations under the DataContext.
    /// </summary>
    [Flags]
    public enum ContextOptions {
        /// <summary>
        /// No options specified.  No change tracking, and all object caches
        /// will be inherited.
        /// </summary>
        None = 0,

        /// <summary>
        /// Track changes made to objects retrieved from or attached to the
        /// DataContext.  A copy of the raw object data is kept for this
        /// purpose.  Use the GetTrackedChanges() method on DataContext to
        /// receive a ChangeSet containing changed data.
        /// </summary>
        TrackChanges = 1,

        /// <summary>
        /// Create a new identity map, and thus ignores all previously loaded
        /// data under a parent context.  Also establishes a new set of tracked
        /// data to prevent conflicts.  Other inherited properties, like the Log,
        /// are still used.
        /// </summary>
        NewObjectCache = 2,

        /// <summary>
        /// The context will be isolated from others.  It will have no Parent,
        /// and will not affect DataContext.Current or DataContext.Scope in any
        /// way.
        /// </summary>
        Isolated = 4,

        /// <summary>
        /// The context will not open database connections when created.  Instead,
        /// a connection is opened only before a data operation runs, and is closed
        /// immediately afterward.  Prepared statements are not usable with this mode.
        /// You must not mix parent contexts that do not use this option with
        /// children that do.  Without this option the context stores a db connection
        /// for its entire life.
        /// </summary>
        CloseDbConnectionImmediately = 8
    }

    /// <summary>
    /// Represents a context in which data operations take place.  All ORM
    /// data should flow through this class.  Multiple DataContexts can be
    /// nested on the same thread.  A sub DataContext will always inherit the
    /// identity map of the parent.
    /// </summary>
    /// <example>
    /// using (var context = new DataContext(myConfiguration)) {
    ///     var query = from type in context.Get&lt;MyType&gt;()
    ///                 where type.Foo == "Value";
    ///                 select type.Bar;
    ///                 
    ///     using (var subContext = new DataContext(myConfiguration)) {
    ///         ...
    ///     }
    /// }
    /// </example>
    public class DataContext : IDisposable {
#if NET4
        private static XSerializerOptions _serializerOptions;
#endif

        private Dictionary<string, StoredCommand> _preparedCommands;
        private Dictionary<TableMap, Dictionary<object, object>> _identityMap;

        #region Constructors
        static DataContext () {
            Scope = new ThreadStaticDataContextScope();
        }

        /// <summary>
        /// Constructs a new DataContext.
        /// </summary>
        /// <param name="configuration">The configuration data for this context.</param>
        /// <param name="options">The options for this context.</param>
        public DataContext (MapConfiguration configuration, ContextOptions options) {
            if (configuration == null)
                throw new ArgumentNullException("configuration");

            Configuration = configuration;

            if ((options & ContextOptions.Isolated) != ContextOptions.Isolated)
            {
                Parent = Current;
                Scope.Current = this;

                if (Parent != null)
                {
                    _preparedCommands = Parent._preparedCommands;
                    Log = Parent.Log;
                    DbConnection = Parent.DbConnection;
                    IsolationLevel = Parent.IsolationLevel;
                    TransactionScopeOption = Parent.TransactionScopeOption;
                    CommandTimeout = Parent.CommandTimeout;
                    TableHints = Parent.TableHints;

                    if ((options & ContextOptions.NewObjectCache) != ContextOptions.NewObjectCache)
                    {
                        TrackedData = Parent.TrackedData;
                        _identityMap = Parent._identityMap;

                        // Inherit TrackChanges mode from parent context.
                        if ((Parent.Options & ContextOptions.TrackChanges) == ContextOptions.TrackChanges)
                            options |= ContextOptions.TrackChanges;
                    }
                }
            }

            Options = options;

            if (_identityMap == null)
                _identityMap = new Dictionary<TableMap, Dictionary<object, object>>();

            if (_preparedCommands == null)
                _preparedCommands = new Dictionary<string, StoredCommand>();

            if (DbConnection == null && (options & ContextOptions.CloseDbConnectionImmediately) != ContextOptions.CloseDbConnectionImmediately)
                DbConnection = configuration.DefaultStore.OpenDbConnection();

            if ((options & ContextOptions.TrackChanges) == ContextOptions.TrackChanges && TrackedData == null)
                TrackedData = new LightSet();
        }

        /// <summary>
        /// Constructs a new DataContext that does not track changes.
        /// </summary>
        /// <param name="configuration">The configuration data for this context.</param>
        public DataContext (MapConfiguration configuration)
            : this(configuration, ContextOptions.None) {
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Clears any tracked data, the identity map cache, and sets the
        /// current DataContext reference to the immediate parent DataContext.
        /// </summary>
        public void Dispose () {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Clears any tracked data, the identity map cache, complete shared
        /// connections, and sets the current DataContext reference to the
        /// immediate parent DataContext.
        /// </summary>
        /// <param name="disposing">true if disposing; false if otherwise.</param>
        protected virtual void Dispose (bool disposing) {
            if (disposing) {
                _identityMap = null;
                TrackedData = null;

                // Dispose of the connection only if the parent doesn't share it.
                // Otherwise it's the parent's responsibility.
                if (DbConnection != null && (Parent == null || Parent.DbConnection != DbConnection))
                {
                    foreach (var prepared in _preparedCommands.Values)
                        prepared.Command.Dispose();
                    DbConnection.Dispose();
                }

                _preparedCommands = null;
                DbConnection = null;

                if ((Options & ContextOptions.Isolated) != ContextOptions.Isolated)
                    Scope.Current = Parent;
            }
        }
        #endregion

        #region Events
        protected internal virtual void OnQueryExecuting (QueryExpression query) {
            // Do nothing.
        }

        protected internal virtual void OnQueryExecuted (QueryExpression query, DataQueryResults results, object values) {
            LastQueryStatistics = results.Query.Statistics;

            // Merge query change tracking data.
            if ((Options & ContextOptions.TrackChanges) == ContextOptions.TrackChanges)
                TrackedData.Merge(results.Data);
        }

        protected internal virtual void OnChangeSetCommitting (ChangeSet changes) {
            // Do nothing.
        }

        protected internal virtual void OnChangeSetCommitted (ChangeSet changes) {
            // Cache references to inserted objects.
            foreach (var insert in changes.OperationsByInstance.Values.OfType<InsertChangeOperation>())
                StoreObject(insert.Instance, insert.Key, insert.TableMap);

            // Merge the now-committed change tracking data.
            if ((Options & ContextOptions.TrackChanges) == ContextOptions.TrackChanges) {
                TrackedData.Merge(changes.Data);

                foreach (var deleteOperation in changes.OperationsByInstance.Values.OfType<DeleteChangeOperation>())
                {
                    deleteOperation.RemoveDeletedData(TrackedData);
                    Detach(deleteOperation.Instance);
                }
            }
        }

        protected internal virtual void OnChangeOperationCommitted (ChangeOperation operation) {
            // Do nothing.
        }
        #endregion

        #region Table Reads
        /// <summary>
        /// Creates a dynamic DataQuery that can read data for a type.
        /// </summary>
        /// <param name="type">The dynamic type to read data from.</param>
        /// <returns>A dynamic DataQuery that can read data.</returns>
        public DataQuery Get (Type type) {
            ThrowIfDisposed();

            return (DataQuery) ActivatorEx.CreateGenericInstance(typeof(DataQuery<>), type, typeof(DataContext), this);
        }

        /// <summary>
        /// Creates a DataQuery that can read data for a type.
        /// </summary>
        /// <typeparam name="T">The type to read data from.</typeparam>
        /// <returns>A DataQuery that can read data.</returns>
        public DataQuery<T> Get<T> () {
            ThrowIfDisposed();

            return new DataQuery<T>(this);
        }

        /// <summary>
        /// Creates a DataQuery that can read data from a database procedure.
        /// This query does not generate any SQL statements except those needed
        /// to execute the procedure and fill dependent data, if any.
        /// </summary>
        /// <typeparam name="T">The type of data the procedure returns.</typeparam>
        /// <param name="procedureName">The procedure name.</param>
        /// <param name="procedureArgs">Arguments passed to the procedure.</param>
        /// <returns>A DataQuery that can read data from a database procedure.</returns>
        public DataQuery<T> Get<T> (string procedureName, params object[] procedureArgs) {
            ThrowIfDisposed();

            return new DataQuery<T>(this, procedureName, procedureArgs);
        }

        /// <summary>
        /// Loads a single instance of an object with the given primary key.
        /// </summary>
        /// <typeparam name="T">The type of the object to load.</typeparam>
        /// <param name="key">The primary key of the object to load.</param>
        /// <returns>A fully loaded instance, or null if the object was not found in the database.</returns>
        public T Load<T> (object key) {
            return (T) Load(key, typeof(T));
        }

        /// <summary>
        /// Loads a single instance of an object with the given primary key.
        /// </summary>
        /// <param name="key">The primary key of the object to load.</param>
        /// <param name="type">The type of the object to load.</param>
        /// <returns>A fully loaded instance, or null if the object was not found in the database.</returns>
        public virtual object Load (object key, Type type) {
            ThrowIfDisposed();

            var tableMap = Configuration.TableMaps[type];

            // First check the identity map for the object.
            var storedObject = GetStoredObject(tableMap, key);
            if (storedObject != null)
                return storedObject;

            // Not found in the map, query it instead.
            // Retrieve a list with the PK criteria.
            var query = tableMap.PrimaryKey.VariableMap.CreateKeyQuery(new object[] { key }, this, tableMap);
            var result = query.ExecuteSequence().SingleOrDefault();
            return result;
        }
        #endregion

        #region Table Writes
        /// <summary>
        /// Creates a ChangeSet that can be used to record changed data that
        /// this DataContext is aware of.
        /// </summary>
        /// <returns>A new ChangeSet.</returns>
        public ChangeSet CreateChangeSet () {
            return CreateChangeSet(ChangeOptions);
        }

        public virtual ChangeSet CreateChangeSet(ChangeSetOptions changeOptions)
        {
            if ((Options & ContextOptions.TrackChanges) == ContextOptions.TrackChanges)
                changeOptions |= ChangeSetOptions.TrackChanges;

            if ((changeOptions & ChangeSetOptions.TrackChanges) == ChangeSetOptions.TrackChanges)
                return new TrackingChangeSet(this, changeOptions);
            else
                return new ChangeSet(this, changeOptions);
        }

        /// <summary>
        /// Deletes the given instance from the database.
        /// </summary>
        /// <param name="instance">The instance to delete.</param>
        public virtual void Delete (object instance) {
            ThrowIfDisposed();

            if (instance == null)
                throw new ArgumentNullException("instance");

            var changes = CreateChangeSet();
            changes.Delete(instance);
            changes.Commit();
        }

        /// <summary>
        /// Saves the given instances to the database.
        /// </summary>
        /// <param name="instance">The instance to save.</param>
        public virtual void Save (object instance) {
            ThrowIfDisposed();

            if (instance == null)
                throw new ArgumentNullException("instance");

            var changes = CreateChangeSet();
            changes.Add(instance);
            changes.Commit();
        }
        #endregion

        #region Change Tracking
        /// <summary>
        /// Creates a new TrackingChangeSet and inserts all reachable data into
        /// it.
        /// </summary>
        /// <returns>A new TrackingChangeSet with all reachable data inserted.</returns>
        public TrackingChangeSet GetTrackedChanges()
        {
            return GetTrackedChanges(ChangeOptions);
        }

        /// <summary>
        /// Creates a new TrackingChangeSet and inserts all reachable data into
        /// it.
        /// </summary>
        /// <param name="changeOptions">Options to be passed to the ChangeSet upon creation.</param>
        /// <returns>A new TrackingChangeSet with all reachable data inserted.</returns>
        public TrackingChangeSet GetTrackedChanges(ChangeSetOptions changeOptions)
        {
            if ((Options & ContextOptions.TrackChanges) != ContextOptions.TrackChanges)
                throw new InvalidOperationException(LS.T("GetTrackedChanges may only be called when ContextOptions has TrackChanges."));

            var changes = (TrackingChangeSet)CreateChangeSet(changeOptions);
            changes.AddTrackedChanges();
            return changes;
        }
        #endregion

        #region Identity Map Access
        /// <summary>
        /// Attaches an instance with a primary key to this DataContext.  The
        /// instance will be treated like any other data the DataContext is
        /// aware of.  Do NOT call this method for new objects.  Instead, add
        /// those objects to a ChangeSet and they will be attached when their
        /// primary key is available.
        /// </summary>
        /// <param name="instance">The instance with a primary key to attach.</param>
        public void Attach (object instance) {
            ThrowIfDisposed();

            if (instance == null)
                throw new ArgumentNullException("instance");

            // Find the instance key to attach.
            var type = instance.GetType();
            if (!Configuration.TableMaps.ContainsKey(type))
                throw new MapConfigurationException(LS.T("Attempt to attach type '{0}' which is not configured.", type));

            var tableMap = Configuration.TableMaps[type];
            var key = tableMap.GetKey(instance);

            // Attaching an instance that is already attached is an error.
            var existingInstance = GetStoredObject(tableMap, key);
            if (existingInstance != null)
                throw new InvalidOperationException(LS.T("Attempt to attach an instance that is already attached."));

            StoreObject(instance, key, tableMap);
        }

        public ICollection<object> GetAttachedObjects () {
            var objects = new HashSet<object>();
            foreach (var maps in _identityMap.Values)
                objects.AddRange(maps.Values);
            return objects;
        }

        internal object GetStoredObject (TableMap tableMap, object key) {
            ThrowIfDisposed();

            var root = tableMap.RootTableMap;

            // First check the identity map.
            var objects = _identityMap.Activate(root);
            if (objects.ContainsKey(key))
                return objects[key];

            // Next check the cache, if one exists.
            if (Cache != null)
            {
                var obj = Cache.Get(GetCacheKey(root.OriginTypeMap.Type, key));
                if (obj != null)
                {
                    // Object found in the cache.  Stash in the identity map so
                    // change tracking works, and so this reference is never lost.
                    objects[key] = obj;
                    return obj;
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if the instance is new, i.e. it has never been saved to the
        /// database.  Implement the ILifecycle interface to control the return
        /// value of this method.
        /// </summary>
        /// <param name="instance">The instance to check for newness.</param>
        /// <returns>true if the object has been loaded from the database or attached; false if otherwise.</returns>
        public virtual bool IsNew (object instance) {
            ThrowIfDisposed();

            if (instance is ILifecycle)
                return ((ILifecycle) instance).IsNew;

            var type = instance.GetType();
            TableMap tableMap;
            if (!Configuration.TableMaps.TryGetValue(type, out tableMap))
                throw new MapConfigurationException(LS.T("No mapping was configured for type '{0}'", type));

            var key = GetKey(instance);
            if (GetStoredObject(tableMap, key) == null)
                return true;

            return false;
        }

        internal void StoreObject (object instance, object key, TableMap tableMap) {
            var lifecycle = instance as ILifecycle;
            if (lifecycle != null)
                lifecycle.IsNew = false;

            StoreValue(instance, key, tableMap);
        }

        internal void StorePlaceholder(object placeholder, object key, TableMap tableMap)
        {
            StoreValue(placeholder, key, tableMap);
        }

        private void StoreValue (object value, object key, TableMap tableMap) {
            ThrowIfDisposed();

            var root = tableMap.RootTableMap;
            var objects = _identityMap.Activate(root);
            objects[key] = value;

            if (Cache != null && tableMap.OriginTypeMap.CacheOptions != null)
                Cache.Insert(GetCacheKey(root.OriginTypeMap.Type, key), value, tableMap.OriginTypeMap.CacheOptions);
        }

        private static string GetCacheKey(Type rootType, object key)
        {
            return "Nen.Data.ObjectRelationalMapper.Type: " + rootType.FullName + ":" + key.ToString();
        }

        public bool Detach(object instance)
        {
            ThrowIfDisposed();

            if (instance == null)
                throw new ArgumentNullException("instance");

            // Find the instance key to detach.
            var type = instance.GetType();
            if (!Configuration.TableMaps.ContainsKey(type))
                throw new MapConfigurationException(LS.T("Attempt to detach type '{0}' which is not configured.", type));

            var tableMap = Configuration.TableMaps[type];
            var key = tableMap.GetKey(instance);

            var root = tableMap.RootTableMap;
            var objects = _identityMap.Activate(root);
            
            var cacheKey = GetCacheKey(root.OriginTypeMap.Type, key);
            
            var identityMapRemoved = objects.Remove(cacheKey);
            var cacheRemoved = Cache == null ? false : Cache.Remove(cacheKey);
            return identityMapRemoved || cacheRemoved;
        }

        /// <summary>
        /// Retrieves the current primary key value of the given instance.
        /// </summary>
        /// <param name="instance">The instance to retrieve a primary key value for.</param>
        /// <returns>The primary key value.</returns>
        public object GetKey (object instance) {
            var type = instance.GetType();
            if (!Configuration.TableMaps.ContainsKey(type))
                throw new MapConfigurationException(LS.T("Attempt to read key for type '{0}' which is not configured.", type));

            var tableMap = Configuration.TableMaps[type];
            return tableMap.GetKey(instance);
        }
        #endregion

        #region Serialization
#if NET4
        public string Serialize (IQueryable queryable) {
            var dataQuery = (DataQuery) queryable;
            var query = dataQuery.TranslateQuery();
            return Serialize(query);
        }

        public string Serialize (QueryExpression query) {
            var serializer = new XSerializer(GetSerializerOptions());
            serializer.State = this;
            return serializer.WriteObject(query);
        }

        public QueryExpression Deserialize (string queryXml) {
            var serializer = new XSerializer(GetSerializerOptions());
            serializer.State = this;
            return (QueryExpression) serializer.ReadObject(queryXml);
        }

        private static XSerializerOptions GetSerializerOptions () {
            if (_serializerOptions == null) {
                var options = new XSerializerOptions(typeof(QueryExpression), true,
                    typeof(LiteralExpression), typeof(AdviceExpression), typeof(OrderExpression), typeof(ProjectionExpression),
                    typeof(BinaryOperatorExpression), typeof(UnaryOperatorExpression), typeof(ApplyExpression), typeof(VariableExpression),
                    typeof(TableExpression),
                    typeof(ReflectionFieldVariableInfo), typeof(ReflectionPropertyVariableInfo),
                    typeof(DynamicFieldVariableInfo), typeof(DynamicPropertyVariableInfo));
                options.AddSurrogate<VariableInfo, PortableVariableInfo>();
                _serializerOptions = options;
            }
            return _serializerOptions;
        }
#endif
        #endregion

        #region DbCommand Caching
        internal StoredCommand GetStoredCommand (string key) {
            ThrowIfDisposed();

            return _preparedCommands.ContainsKey(key) ? _preparedCommands[key] : null;
        }

        internal void StoreCommand (string key, IDbCommand command, int numberOfStatements) {
            _preparedCommands[key] = new StoredCommand() { Command = command, NumberOfStatements = numberOfStatements };
        }
        #endregion

        #region Disposal Checks
        private void ThrowIfDisposed () {
            if (_identityMap == null)
                throw new ObjectDisposedException("DataContext");
        }
        #endregion

        #region Scope
        /// <summary>
        /// Gets the most current DataContext on the stack.  This property will
        /// be null if there are none.
        /// </summary>
        public static DataContext Current {
            get { return Scope.Current; }
        }

        /// <summary>
        /// Gets or sets the method used to scope the DataContext.  The default
        /// is ThreadStaticDataContextScope.
        /// </summary>
        public static DataContextScope Scope { get; set; }
        #endregion

        #region Hints
        public void AddTableHint<T>(string hint)
        {
            if (TableHints == null)
                TableHints = new Collection<TableHint>();

            var type = typeof(T);
            if (!Configuration.TableMaps.ContainsKey(type))
                throw new MapConfigurationException(LS.T("Hint for type '{0}' which is not configured", type));

            var tableMap = Configuration.TableMaps[type];
            TableHints.Add(new TableHint(tableMap, hint));
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets a separate object cache for the object identity map.
        /// Use Type.FullName-primarykey as your cache key.
        /// </summary>
        public ICache Cache { get; set; }
        /// <summary>
        /// Gets the configuration data for this context.
        /// </summary>
        public MapConfiguration Configuration { get; private set; }
        /// <summary>
        /// Gets or sets the options to be passed to every ChangeSet.  Overridable
        /// by passing options to CreateChangeSet() or GetTrackedChanges().
        /// </summary>
        public ChangeSetOptions ChangeOptions { get; set; }
        /// <summary>
        /// Gets or sets an optional log of activity happening in this context.
        /// Generated SQL is written to this log.
        /// </summary>
        public TextWriter Log { get; set; }
        /// <summary>
        /// Gets the set of tracked data that has been read by this DataContext.
        /// </summary>
        public LightSet TrackedData { get; private set; }
        /// <summary>
        /// Gets the options for this context.
        /// </summary>
        public ContextOptions Options { get; private set; }
        /// <summary>
        /// Gets the performance statistics recorded by the last query.
        /// </summary>
        public QueryStatistics LastQueryStatistics { get; internal set; }
        /// <summary>
        /// Gets the parent context in scope.
        /// </summary>
        public DataContext Parent { get; private set; }
        /// <summary>
        /// Gets or sets the isolation level for created transactions, if applicable.
        /// </summary>
        public System.Data.IsolationLevel? IsolationLevel { get; set; }
        /// <summary>
        /// Get or sets the scope of any created transactions, if applicable.
        /// </summary>
        public TransactionScopeOption? TransactionScopeOption { get; set; }
        /// <summary>
        /// The database connection to be used by this context, if applicable.
        /// This connection is inherited from any parent context in scope.
        /// </summary>
        public IDbConnection DbConnection { get; internal set; }
        /// <summary>
        /// Gets or sets the command timeout for all commands issued by this context.  Inherited.
        /// </summary>
        public int? CommandTimeout { get; set; }
        /// <summary>
        /// Gets or sets the table hints applied to this context.  Inherited.
        /// </summary>
        public Collection<TableHint> TableHints { get; private set; }
        #endregion 
    }

    internal class StoredCommand {
        public int NumberOfStatements { get; set; }
        public IDbCommand Command { get; set; }
    }

    public class TableHint
    {
        public TableMap ForTableMap { get; private set; }
        public string Hint { get; private set; }

        public TableHint() { }

        public TableHint(TableMap forTableMap, string hint)
        {
            ForTableMap = forTableMap;
            Hint = hint;
        }
    }

    #region Scoping
    /// <summary>
    /// Provides a scope for the DataContext hierarchy.  DataContexts created
    /// while another DataContext in scope is not disposed will use the last
    /// created DataContext as its parent, inheriting configuration options
    /// such as tracked data, the identity map cache, the log and any shared
    /// connections.  The default DataContextScope is ThreadStaticDataContextScope.
    /// </summary>
    public abstract class DataContextScope {
        /// <summary>
        /// Gets the current DataContext in scope.
        /// </summary>
        public abstract DataContext Current { get; protected internal set; }
    }

    /// <summary>
    /// Scopes the DataContext hierarchy to the current thread.  Any DataContext
    /// created within the thread will be accessible and inherited.
    /// </summary>
    public class ThreadStaticDataContextScope : DataContextScope {
        [ThreadStatic]
        private static DataContext _current;

        public override DataContext Current {
            get { return _current; }
            protected internal set { _current = value; }
        }
    }

    /// <summary>
    /// Scopes the DataContext hierarchy to the current HttpContext, which is
    /// bound to an HTTP request.  The DataContext will be accessible anywhere
    /// in the request.
    /// 
    /// If the current thread is not servicing an HTTP request, the behavior
    /// is the same as ThreadStaticDataContextScope.
    /// </summary>
    public class HttpDataContextScope : DataContextScope {
        [ThreadStatic]
        private static DataContext _threadCurrent;

        private const string _key = "__Nen_HttpDataContextScope__";

        public override DataContext Current {
            get {
                var httpContext = System.Web.HttpContext.Current;
                if (httpContext == null)
                    return _threadCurrent;

                var items = System.Web.HttpContext.Current.Items;
                return items.Contains(_key) ? (DataContext) items[_key] : null;
            }

            protected internal set {
                var httpContext = System.Web.HttpContext.Current;

                if (httpContext != null)
                    httpContext.Items[_key] = value;
                else
                    _threadCurrent = value;
            }
        }
    }
    #endregion
}
