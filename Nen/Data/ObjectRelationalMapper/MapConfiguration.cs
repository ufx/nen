using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using Nen.Collections;
using Nen.Data.SqlModel;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryEngine;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;
using Nen.Linq.Expressions;
using Nen.Reflection;
using System.Transactions;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Configures and acts as a repository for information necessary for
    /// the functioning of the object/relational mapper.
    /// </summary>
    public class MapConfiguration {
        #region Constructors
        /// <summary>
        /// Constructs a new MapConfiguration with default conventions.
        /// </summary>
        public MapConfiguration ()
            : this(new Conventions(), CultureInfo.CurrentCulture) {
        }

        /// <summary>
        /// Constructs a new MapConfiguration with the specified conventions.
        /// </summary>
        /// <param name="conventions">The mapping conventions to use.</param>
        public MapConfiguration (Conventions conventions)
            : this(conventions, CultureInfo.CurrentCulture) {
        }

        /// <summary>
        /// Constructs a new MapConfiguration with the specified conventions and culture.
        /// </summary>
        /// <param name="conventions">The mapping conventions to use.</param>
        /// <param name="locale">The data locale.</param>
        public MapConfiguration (Conventions conventions, CultureInfo locale) {
            if (conventions == null)
                throw new ArgumentNullException("conventions");

            if (locale == null)
                throw new ArgumentNullException("locale");

            IsolationLevel = System.Data.IsolationLevel.ReadCommitted;
            Conventions = conventions;
            Locale = locale;
            TableMaps = new Dictionary<Type, TableMap>();
            TableMapsByTableName = new Dictionary<string, TableMap>();
            TableMapsByTypeName = new Dictionary<string, TableMap>();
            TypeMaps = new Dictionary<Type, TypeMap>();
            TypeOverrides = new Dictionary<Type, Type>();
        }

        /// <summary>
        /// Quickly initializes a configuration against a database.
        /// </summary>
        /// <param name="database">The database to use with the default data store.</param>
        public MapConfiguration(Database database)
            : this()
        {
            DefaultStore = new DatabaseStore(this, database);
            Initialize();
        }
        #endregion

        #region Initialization
        /// <summary>
        /// Initializes the configuration so it may be used.
        /// </summary>
        public void Initialize () {
            var assembliesToUse = AppDomain.CurrentDomain.GetAssemblies().Where(a => Conventions.HasPersistentTypes(a));
            Initialize(assembliesToUse);
        }

        public void Initialize (IEnumerable<Assembly> assembliesToUse) {
            foreach (var assembly in assembliesToUse)
                Configure(assembly);

            PostConfigureTypeMaps();
            ConfigureTableMaps();
            CallInitializers();
        }

        private void CallInitializers () {
            foreach (var typeMap in TypeMaps.Values) {
                // Call configuration initializers.
                var initializers = typeMap.Type.FindMembersWithAttribute<MapConfigurationInitializerAttribute>(MemberTypes.Method, TypeEx.StaticBindingFlags, false);
                foreach (var initializer in initializers.Cast<MethodInfo>())
                    initializer.Invoke(null, new object[] { this });
            }
        }
        #endregion

        #region TableMap Configuration
        private void ConfigureTableMaps () {
            // Establish the physical hierarchy of table maps.
            foreach (var typeMap in TypeMaps.Values.Where(t => t.IsPhysical))
                ConfigureTableMapStructure(typeMap);

            var distinctTableMaps = TableMaps.Values.Distinct().ToArray();

            // Set special variables and other properties.
            var configuredTableMaps = new Dictionary<TableMap, bool>();
            foreach (var tableMap in distinctTableMaps)
                ConfigureTableMapProperties(tableMap, configuredTableMaps);

            // Update reference types now that all have been configured.
            foreach (var tableMap in distinctTableMaps)
                ConfigureTableMapReferenceTypes(tableMap);

            // Determine whether queries directly to tables are ambiguous.
            foreach (var tableMap in distinctTableMaps.Where(tm => tm.DiscriminatorVariableMap != null)) {
                foreach (var subTableMap in tableMap.SubTableMaps) {
                    if (subTableMap.VariableMaps.FindAll<DirectVariableMap>().Length > 1) {
                        tableMap.IsAmbiguous = true;
                        break;
                    }
                }
            }

            // Ensure everything is still OK.
            foreach (var tableMap in distinctTableMaps)
                SanityCheck(tableMap);
        }

        private TableMap ConfigureTableMapStructure (TypeMap originTypeMap) {
            // Return existing table map if it's already been configured.
            if (TableMaps.ContainsKey(originTypeMap.Type))
                return TableMaps[originTypeMap.Type];

            // Find or create the table map to use.
            TableMap tableMap;

            var baseStructureTypeMap = originTypeMap.GetStructureTypeMap();

            if (baseStructureTypeMap.HierarchyMapping == HierarchyMapping.TablePerType) {
                tableMap = new TableMap(this, originTypeMap);
                tableMap.TableName = GetTableName(originTypeMap);
            } else if (baseStructureTypeMap.HierarchyMapping == HierarchyMapping.TablePerHierarchy) {
                if (baseStructureTypeMap == originTypeMap) {
                    // For the first table in this hierarchy, create the table
                    // map as usual.
                    tableMap = new TableMap(this, originTypeMap);
                    tableMap.TableName = GetTableName(originTypeMap);
                } else {
                    // For all others, use the base table of the structure.
                    tableMap = baseStructureTypeMap.PhysicalTableMap;
                    tableMap.ImportVariableMaps(originTypeMap);
                }
            } else
                throw new NotImplementedException(LS.T("A HierarchyMapping of '{0}' is not supported.", baseStructureTypeMap.HierarchyMapping));

            // Store the table map.
            TableMaps[originTypeMap.Type] = tableMap;
            TableMapsByTypeName[originTypeMap.Type.Name] = tableMap;
            TableMapsByTableName[tableMap.TableName] = tableMap;

            originTypeMap.PhysicalTableMap = tableMap;

            // Setup view relationships.
            if (originTypeMap.UnderlyingViewTypeMap != null)
                tableMap.UnderlyingViewTableMap = ConfigureTableMapStructure(originTypeMap.UnderlyingViewTypeMap);

            // Setup base table relationships.
            var baseTypeMap = originTypeMap.BaseTypeMap;
            if (baseTypeMap != null && baseStructureTypeMap.HierarchyMapping != HierarchyMapping.TablePerHierarchy) {
                if (baseTypeMap.IsFlattened) {
                    foreach (var flattenedTypeMap in baseTypeMap.GetHierarchy())
                        tableMap.ImportVariableMaps(flattenedTypeMap);
                } else if (baseTypeMap.IsPhysical)
                    tableMap.BaseTableMap = ConfigureTableMapStructure(baseTypeMap);
            }

            return tableMap;
        }

        private void ConfigureTableMapReferenceTypes(TableMap tableMap) {
            foreach (var referenceVariableMap in tableMap.VariableMaps.FindAll<ReferenceVariableMap>()) {
                var rpk = referenceVariableMap.ReferencedTableMap.PrimaryKey;
                if (rpk.VariableMap == null)
                    throw new MapConfigurationException(LS.T("Variable '{0}' on table '{1}' references table '{2}' which has no key.", referenceVariableMap, tableMap, referenceVariableMap.ReferencedTableMap));
                referenceVariableMap.DbType = rpk.VariableMap.DbType;
            }
        }

        private void ConfigureTableMapProperties (TableMap tableMap, Dictionary<TableMap, bool> configuredTableMaps) {
            if (configuredTableMaps.ContainsKey(tableMap))
                return;

            configuredTableMaps[tableMap] = true;

            // Setup sub table map structure.
            var discriminatorValue = tableMap.DiscriminatorValue;

            foreach (var baseTableMap in tableMap.GetBaseHierarchy()) {
                // Check for a conflict in existing sub table maps.
                if (discriminatorValue != null) {
                    var conflictingTableMap = baseTableMap.GetDiscriminatedSubTableMapCore(discriminatorValue);
                    if (conflictingTableMap != null)
                        throw new MapConfigurationException(LS.T("Base table '{0}' has conflicting discriminator value '{1}' for types '{2}' and '{3}'", baseTableMap, discriminatorValue, tableMap, conflictingTableMap));
                }

                // No errors found, add the sub table map.
                baseTableMap.SubTableMaps.Add(tableMap);
            }

            // Ensure base table map is setup before sub table map.
            if (tableMap.BaseTableMap != null) {
                ConfigureTableMapProperties(tableMap.BaseTableMap, configuredTableMaps);

				if (tableMap.BaseTableMap.DiscriminatorVariableMap != null && discriminatorValue == null && !tableMap.OriginTypeMap.Type.IsAbstract)
					throw new MapConfigurationException(LS.T("Concrete table '{0}' with base '{1}' has no discriminator value.", tableMap, tableMap.BaseTableMap));
            }

            // Setup special variables.
            var originTypeMap = tableMap.OriginTypeMap;

            tableMap.TimeStampVariableMap = GetTimeStampVariableMap(tableMap);
            tableMap.ReferenceBase = Conventions.GetReferenceBase(originTypeMap.Type);
            tableMap.SchemaName = Conventions.GetSchemaName(originTypeMap.Type);
            tableMap.PrimaryKey = ConfigurePrimaryKey(tableMap);
            tableMap.IsOptimizedOutForReads = tableMap.CheckIfOptimizedOutForReads();

            if (originTypeMap.DiscriminatorVariableMap != null)
                tableMap.DiscriminatorVariableMap = tableMap.VariableMaps.FindDirect(originTypeMap.DiscriminatorVariableMap.Variable.Name, VariableStructure.BaseContainers);

            // Setup variable relationships to this table map.

            // Setup reference map tables.
            foreach (var referenceVariableMap in tableMap.VariableMaps.FindAll<ReferenceVariableMap>()) {
                var referencedType = referenceVariableMap.ReferencedTypeMap.Type;
                if (!TableMaps.ContainsKey(referencedType))
                    throw new MapConfigurationException(LS.T("Variable '{0}.{1}' references type '{2}' which has no physical representation (is it [Flatten]ed?)", tableMap, referenceVariableMap.Variable, referenceVariableMap, referencedType));

                var referencedTableMap = TableMaps[referencedType];

                // Can't reference a table with no primary key.
                if (referencedTableMap.PrimaryKey is NoPrimaryKey)
                    throw new MapConfigurationException(LS.T("Variable '{0}.{1}' references table '{2}', which has no primary key.", tableMap, referenceVariableMap.Variable, referenceVariableMap.ReferencedTableMap));

                referenceVariableMap.ReferencedTableMap = referencedTableMap;
            }

            // Setup subordinate map tables.
            foreach (var subordinateVariableMap in tableMap.VariableMaps.FindAll<SubordinateVariableMap>()) {
                subordinateVariableMap.ReferencedTableMap = TableMaps[subordinateVariableMap.ReferencedTypeMap.Type];
                subordinateVariableMap.KeyVariableMap = subordinateVariableMap.ReferencedTableMap.VariableMaps.FindReference(subordinateVariableMap.KeyVariableMap.Variable.Name, VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            }

            // Setup collection map tables.
            foreach (var collectionVariableMap in tableMap.VariableMaps.FindAll<CollectionVariableMap>()) {
                collectionVariableMap.CollectionTableMap = TableMaps[collectionVariableMap.CollectionTypeMap.Type];
                collectionVariableMap.KeyVariableMap = collectionVariableMap.CollectionTableMap.VariableMaps.FindReference(collectionVariableMap.KeyVariableMap.Variable.Name, VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            }

            // Setup component columns.
            foreach (var componentVariableMap in tableMap.VariableMaps.FindAll<ComponentVariableMap>())
                ConfigureTableMapComponentVariables(componentVariableMap, tableMap, new Stack<VariableInfo>());

            // Find new constructor parameters.
            if (originTypeMap.ConstructorParameters != null) {
                var tableVariableMaps = tableMap.VariableMaps.FindAll<VariableMap>(VariableStructure.BaseContainers);
                var constructorVariables = originTypeMap.ConstructorParameters.Select(v => v.Variable);
                var constructorParameters = tableVariableMaps.Where(v => constructorVariables.Contains(v.Variable));
                tableMap.ConstructorParameters = constructorParameters.ToArray();
            }
        }

        private void ConfigureTableMapComponentVariables (ComponentVariableMap componentVariableMap, TableMap tableMap, Stack<VariableInfo> path) {
            using (path.PushScope(componentVariableMap.Variable)) {
                // Set physical table.
                foreach (var variableMap in componentVariableMap.ComponentTypeMap.VariableMaps)
                    variableMap.PhysicalTableMap = tableMap;

                foreach (var directVariableMap in componentVariableMap.ComponentTypeMap.VariableMaps.FindAll<DirectVariableMap>()) {
                    // Set actual column name and path.
                    using (path.PushScope(directVariableMap.Variable))
                        directVariableMap.ColumnName = Conventions.GetComponentColumnName(path, directVariableMap);
                    directVariableMap.Path = new Stack<VariableInfo>(path.Reverse());

                    // Set constraints.
                    directVariableMap.IsRequired &= !path.Any(v => !Conventions.IsRequired(v));
                }

                // Configure sub-components.
                foreach (var subComponentVariableMap in componentVariableMap.ComponentTypeMap.VariableMaps.FindAll<ComponentVariableMap>())
                    ConfigureTableMapComponentVariables(subComponentVariableMap, tableMap, path);
            }
        }

        private void SanityCheck (TableMap tableMap) {
            foreach (var referenceVariableMap in tableMap.VariableMaps.FindAll<ReferenceVariableMap>()) {
                var referencedTableMap = referenceVariableMap.ReferencedTableMap;
                if (referencedTableMap.IsAmbiguous && referencedTableMap.DiscriminatorVariableMap == null)
                    throw new MapConfigurationException(LS.T("Reference '{0}.{1}' to table '{2}' is ambiguous and has no discriminator.", tableMap, referenceVariableMap.Variable, referencedTableMap));
            }
        }

        internal PrimaryKey ConfigurePrimaryKey (TableMap tableMap) {
            if (tableMap.UnderlyingViewTableMap != null)
                return ConfigureViewPrimaryKey(tableMap);

            var keyVariableName = Conventions.GetPrimaryKeyVariableName(tableMap.OriginTypeMap.Type);
            var keyVariableMap = tableMap.VariableMaps.FindDirect(keyVariableName, VariableStructure.BaseContainers, false);

            if (keyVariableMap == null) {
                // When the type map has an underlying view type there is a
                // possibility the primary key won't be declared.  Make sure it's
                // still accessible but unmapped.

                var underlyingViewTypeMap = tableMap.OriginTypeMap.UnderlyingViewTypeMap;
                if (underlyingViewTypeMap == null)
                    return new NoPrimaryKey();

                throw new InvalidOperationException("Primary keys for views must be configured with ConfigureViewPrimaryKey");
            } else if (keyVariableMap.PhysicalTableMap != tableMap) {
                // Duplicate the primary key to each sub table map.

                keyVariableMap = (DirectVariableMap) keyVariableMap.Clone();
                keyVariableMap.ColumnName = Conventions.GetPrimaryKeyColumnName(keyVariableMap.Variable, tableMap);
                keyVariableMap.PhysicalTableMap = tableMap;
                tableMap.VariableMaps.Add(keyVariableMap);

                // Don't map keys on tables that are optimized out.  Rely on base table for it.
                keyVariableMap.IsMapped = !tableMap.CheckIfOptimizedOutForReads();
            } else {
                // Set proper column name of PK.

                keyVariableMap.ColumnName = Conventions.GetPrimaryKeyColumnName(keyVariableMap.Variable, tableMap);
            }

            var primaryKey = Conventions.GetPrimaryKey(keyVariableMap);
            if (primaryKey.IsDatabaseGenerated)
                keyVariableMap.IsDatabaseGenerated = true;
            return primaryKey;
        }

        private PrimaryKey ConfigureViewPrimaryKey (TableMap tableMap) {
            // Replicate tables without primary keys.
            if (tableMap.UnderlyingViewTableMap.PrimaryKey is NoPrimaryKey)
                return new NoPrimaryKey();

            // If the primary key is inaccessible in this view, create it and
            // mark it unmapped in case references (like collections or
            // subordinates) require it.

            // Look for the variable mapped to the underlying view primary key variable.
            var underlyingViewKeyVariableMap = tableMap.UnderlyingViewTableMap.PrimaryKey.VariableMap;
            DirectVariableMap keyVariableMap = null;

            foreach (var variableMap in tableMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.BaseContainers)) {
                var fromVariable = variableMap.FromExpression as VariableExpression;
                if (fromVariable == null)
                    continue;

                // The primary key is referenced.  Use this.
                if (fromVariable.Variable.Member == underlyingViewKeyVariableMap.Variable.Member) {
                    keyVariableMap = variableMap;
                    break;
                }
            }

            // Create an unmapped clone key if none was found.
            if (keyVariableMap == null) {
                keyVariableMap = (DirectVariableMap) underlyingViewKeyVariableMap.Clone();
                keyVariableMap.IsMapped = false;
                keyVariableMap.FromExpression = new VariableExpression(underlyingViewKeyVariableMap.Variable, null);
                keyVariableMap.PhysicalTableMap = tableMap;
                tableMap.VariableMaps.Add(keyVariableMap);
            }

            // View primary keys are always considered assigned.
            return new AssignedPrimaryKey(keyVariableMap);
        }
        #endregion

        #region TypeMap Configuration
        private void Configure (Assembly assembly) {
            var persistentTypes = assembly.GetExportedTypes().Where(t => Conventions.IsPersistent(t));
            foreach (var type in persistentTypes)
                ConfigureTypeMap(type, false);
        }

        private TypeMap ConfigureTypeMap (Type type, bool throwIfNotPersistent, bool resolveOverrides = true) {
            if (type == null)
                throw new ArgumentNullException("type");

            if (resolveOverrides && TypeOverrides.ContainsKey(type))
                return ConfigureTypeMap(TypeOverrides[type], throwIfNotPersistent);

            if (!Conventions.IsPersistent(type)) {
                if (throwIfNotPersistent)
                    throw new ArgumentException(LS.T("Essential type '{0}' is not persistent.", type), "type");
                else
                    return null;
            }

            if (TypeMaps.ContainsKey(type))
                return TypeMaps[type];

            // Sanity check type.
            if (type == typeof(object) || type.IsPrimitive)
                throw new ArgumentException(LS.T("Unsupported type to configure '{0}'.", type), "type");

            // Construct and immediately store TypeMap for recursive graphs.
            var typeMap = new TypeMap(this, type);
            TypeMaps[type] = typeMap;

            // Structural configuration.
            typeMap.HierarchyMapping = Conventions.GetHierarchyMapping(type);
            typeMap.IsComponent = IsComponent(type);
            typeMap.IsFlattened = IsFlattened(type);
            typeMap.DiscriminatorValue = GetDiscriminatorValue(type);
            typeMap.CacheOptions = Conventions.GetCacheOptions(type);

            if (!type.IsTopOfHierarchy())
            {
                typeMap.BaseTypeMap = ConfigureTypeMap(type.BaseType, false, false);

                // Inherit cache options from the base type if none are specified for this type.
                if (typeMap.CacheOptions == null && typeMap.BaseTypeMap != null)
                    typeMap.CacheOptions = typeMap.BaseTypeMap.CacheOptions;
            }

            if (typeMap.HierarchyMapping == HierarchyMapping.Inherit && (typeMap.BaseTypeMap == null || typeMap.BaseTypeMap.IsFlattened))
                typeMap.HierarchyMapping = HierarchyMapping.TablePerType;

            var underlyingViewType = GetUnderlyingViewType(type);
            if (underlyingViewType != null)
                typeMap.UnderlyingViewTypeMap = ConfigureTypeMap(underlyingViewType, true);

            ConfigureVariableMaps(typeMap);

            return typeMap;
        }

        private void PostConfigureTypeMaps () {
            foreach (var typeMap in TypeMaps.Values) {
                // Setup collection key variables.
                foreach (var collectionVariableMap in typeMap.VariableMaps.FindAll<CollectionVariableMap>()) {
                    var keyVariableName = Conventions.GetCollectionKeyName(collectionVariableMap.Variable);

                    try {
                        collectionVariableMap.KeyVariableMap = collectionVariableMap.CollectionTypeMap.VariableMaps.FindReference(keyVariableName, VariableStructure.BaseContainers);
                    } catch (MissingVariableException ex) {
                        throw new MapConfigurationException(LS.T("Couldn't load backreference '{0}' on container '{1}' for type '{2}'.", keyVariableName, typeMap, collectionVariableMap.CollectionTypeMap), ex);
                    }
                }

                // Set subordinate target variables.
                foreach (var subordinateVariableMap in typeMap.VariableMaps.FindAll<SubordinateVariableMap>()) {
                    var keyVariableName = Conventions.GetSubordinateTargetName(subordinateVariableMap.Variable);
                    subordinateVariableMap.KeyVariableMap = subordinateVariableMap.ReferencedTypeMap.VariableMaps.FindReference(keyVariableName, VariableStructure.BaseContainers);
                }

                // Configure the discriminator.
                typeMap.DiscriminatorVariableMap = GetDiscriminatorVariableMap(typeMap);

                if (typeMap.DiscriminatorValue != null) {
                    if (typeMap.DiscriminatorVariableMap == null)
                        throw new MapConfigurationException(LS.T("Discriminator value specified for type '{0}' but no discriminator found.", typeMap));

                    // Since discriminator values need to be specified as strings
                    // due to limitations of the .Net attribute system, convert
                    // them now.
                    typeMap.DiscriminatorValue = ConvertEx.ChangeWeakType(typeMap.DiscriminatorValue, typeMap.DiscriminatorVariableMap.PhysicalVariableType);
                }
            }
        }
        #endregion

        #region VariableMap Configuration
        private void ConfigureVariableMaps (TypeMap typeMap) {
            // Setup all persistent properties to map.
            foreach (var property in GetPersistentProperties(typeMap.Type))
                ConfigureVariableMap(property, typeMap);

            // Find the constructor to use, if not the default.
            var immutableVariableMaps = typeMap.VariableMaps.Where(v => v.ConstructorParameterName != null).ToList();
            if (immutableVariableMaps.Count > 0) {
                var constructorParameterNames = immutableVariableMaps.Select(v => v.ConstructorParameterName);
                var constructorParameters = new VariableMap[immutableVariableMaps.Count];

                foreach (var constructor in typeMap.Type.GetConstructors()) {
                    var parameters = constructor.GetParameters();
                    var found = false;

                    foreach (var variableMap in immutableVariableMaps) {
                        found = false;
                        for (var i = 0; i < parameters.Length; i++) {
                            var parameter = parameters[i];
                            if (string.Compare(parameter.Name, variableMap.ConstructorParameterName, true) == 0) {
                                found = true;
                                constructorParameters[i] = variableMap;
                                break;
                            }
                        }

                        if (!found)
                            break;
                    }

                    if (found) {
                        typeMap.Constructor = constructor;
                        typeMap.ConstructorParameters = constructorParameters.ToArray();
                        break;
                    }
                }

                if (typeMap.Constructor == null)
                    throw new MissingMethodException(LS.T("No constructor could be found for type '{0}' that matches the immutable variables it contains.", typeMap));
            }
        }

        private void ConfigureVariableMap (VariableInfo variable, TypeMap declaringTypeMap) {
            // Handle variable maps with special structures.
            if (IsComponent(variable)) {
                var componentVariableMap = ConfigureComponentVariableMap(variable, declaringTypeMap);
                declaringTypeMap.VariableMaps.Add(componentVariableMap);
                return;
            }

            // Variable maps that reside directly on the declaring type map.
            VariableMap variableMap;

            var userMap = GetUserMap(variable);
            if (userMap != null)
            {
                variableMap = userMap.Create(variable, variable.MemberType, declaringTypeMap);

                var primitive = variableMap as PrimitiveVariableMap;
                if (primitive != null)
                {
                    ConfigureDirectVariableMap(primitive);
                    primitive.ColumnName = Conventions.GetPrimitiveColumnName(variable);
                }
            }
            else if (variable.MemberType.IsSimple())
                variableMap = ConfigurePrimitiveVariableMap(variable, declaringTypeMap);
            else if (CollectionEx.IsCollection(variable.MemberType))
                variableMap = ConfigureCollectionVariableMap(variable, declaringTypeMap);
            else if (IsSubordinate(variable))
                variableMap = ConfigureSubordinateVariableMap(variable, declaringTypeMap);
            else
                variableMap = ConfigureReferenceVariableMap(variable, declaringTypeMap);

            // Common configurations.
            var conventionAdvice = Conventions.GetAdvice(variableMap);
            if (conventionAdvice != AdviceRequests.None)
                variableMap.Advice = conventionAdvice;
            variableMap.FromExpression = GetFromExpression(variable, declaringTypeMap);
            variableMap.Computation = Conventions.GetComputation(variable);
            variableMap.ConstructorParameterName = Conventions.GetConstructorParameterName(variable);
            variableMap.IsRequired = Conventions.IsRequired(variable);

            declaringTypeMap.VariableMaps.Add(variableMap);
        }

        private ComponentVariableMap ConfigureComponentVariableMap (VariableInfo variable, TypeMap declaringTypeMap) {
            var componentTypeMap = ConfigureTypeMap(variable.MemberType.UnwrapIfNullable(), true);
            var componentVariableMap = new ComponentVariableMap(variable, declaringTypeMap);
            componentVariableMap.ComponentTypeMap = componentTypeMap;
            return componentVariableMap;
        }

        private PrimitiveVariableMap ConfigurePrimitiveVariableMap (VariableInfo variable, TypeMap declaringTypeMap) {
            PrimitiveVariableMap primitiveVariableMap;

            var dataRefField = GetDataRefField(variable);
            if (dataRefField == null)
                primitiveVariableMap = new PrimitiveVariableMap(variable, variable.MemberType, declaringTypeMap);
            else
                primitiveVariableMap = new LazyPrimitiveVariableMap(variable, dataRefField, declaringTypeMap);

            ConfigureDirectVariableMap(primitiveVariableMap);
            primitiveVariableMap.ColumnName = Conventions.GetPrimitiveColumnName(variable);
            return primitiveVariableMap;
        }

        private CollectionVariableMap ConfigureCollectionVariableMap (VariableInfo variable, TypeMap declaringTypeMap) {
            var collectionVariableMap = new CollectionVariableMap(variable, declaringTypeMap);
            collectionVariableMap.CollectionTypeMap = ConfigureTypeMap(collectionVariableMap.CollectionArgumentType, true);
            collectionVariableMap.DeclaringVariable = variable.MemberType.GetVariable(declaringTypeMap.Type.Name, TypeEx.InstanceBindingFlags, false);

            // Key variables are setup once all type maps are loaded.
            return collectionVariableMap;
        }

        private void ConfigureDirectVariableMap (DirectVariableMap variableMap) {
            variableMap.IsDiscriminator = IsDiscriminator(variableMap.Variable);
            variableMap.IsDatabaseGenerated = IsDatabaseGenerated(variableMap.Variable);

            if (variableMap.IsDiscriminator)
                variableMap.DeclaringTypeMap.DiscriminatorVariableMap = variableMap;
        }

        private ReferenceVariableMap ConfigureReferenceVariableMap (VariableInfo variable, TypeMap declaringTypeMap) {
            // Foreign key references directly on the declared type.

            ReferenceVariableMap referenceVariableMap;

            if (variable.MemberType.IsInstanceOfGenericType(typeof(DataRef<>)))
                referenceVariableMap = new LazyReferenceVariableMap(variable, null, declaringTypeMap);
            else {
                var lazyField = GetDataRefField(variable);
                if (lazyField == null)
                    referenceVariableMap = new ReferenceVariableMap(variable, variable.MemberType, declaringTypeMap);
                else
                    referenceVariableMap = new LazyReferenceVariableMap(variable, lazyField, declaringTypeMap);
            }

            ConfigureDirectVariableMap(referenceVariableMap);
            referenceVariableMap.ReferencedTypeMap = ConfigureTypeMap(referenceVariableMap.PhysicalVariableType, true);
            referenceVariableMap.ColumnName = Conventions.GetReferenceColumnName(variable);

            // Self references must be deferred.
            if (referenceVariableMap.IsSelfReference && referenceVariableMap.Advice != AdviceRequests.Ignore)
                referenceVariableMap.Advice = AdviceRequests.DeferJoin;

            return referenceVariableMap;
        }

        private SubordinateVariableMap ConfigureSubordinateVariableMap (VariableInfo variable, TypeMap declaringTypeMap) {
            SubordinateVariableMap subordinateVariableMap = new SubordinateVariableMap(variable, declaringTypeMap);

            subordinateVariableMap.ReferencedTypeMap = ConfigureTypeMap(variable.MemberType, true);

            return subordinateVariableMap;
        }

        private IEnumerable<VariableInfo> GetPersistentProperties (Type type) {
            var properties = type.GetVariables(TypeEx.DeclaredInstanceBindingFlags, MemberTypes.Property);
            return properties.Where(p => p.CanRead && p.CanWrite && Conventions.IsPersistent(p.Member));
        }
        #endregion

        #region Ad Hoc Configuration
        internal TypeMap CreateAdHocTypeMap (Type anonymousType, TableMap underlyingViewTableMap, OrmExpression[] argumentExpressions, ConstructorInfo constructor, MemberInfo[] constructorParameters) {
            // Construct an anonymous type map as a view for the last method subject.
            var typeMap = new TypeMap(this, anonymousType);
            typeMap.Constructor = constructor;
            typeMap.UnderlyingViewTypeMap = underlyingViewTableMap.OriginTypeMap;
            typeMap.HierarchyMapping = HierarchyMapping.TablePerType;

            // Create new variables referencing those found in the arguments.
            for (var i = 0; i < argumentExpressions.Length; i++) {
                var argumentExpression = argumentExpressions[i];
                var constructorParameter = constructorParameters[i];

                // Locate or create the variable map.
                var parameterName = constructorParameter.Name;
#if !NET4
                // previously took Substring(4)?
				parameterName = parameterName.Substring(4);
#endif
                var variable = anonymousType.GetVariable(parameterName);

                VariableMap variableMap;
                if (argumentExpression is VariableExpression) {
                    // Search for the actual result of variable expressions.
                    var variableExpression = (VariableExpression) argumentExpression;
                    var argumentVariableMap = underlyingViewTableMap.VariableMaps.Find(variableExpression.Variable, VariableStructure.BaseContainers);
                    variableMap = argumentVariableMap.Clone();
                    variableMap.Variable = variable;
                    variableMap.DeclaringTypeMap = typeMap;
                } else if (argumentExpression is QueryExpression) {
                    // Other expressions must be primitive scalars.
                    var primitiveVariableMap = new PrimitiveVariableMap(variable, variable.MemberType, typeMap);
                    // Since generated queries have no column names, give it
                    // one matching the source variable name.
                    primitiveVariableMap.ColumnName = variable.Name;
                    variableMap = primitiveVariableMap;
                } else
                    throw new NotImplementedException(LS.T("Can't create an ad hoc type map with an argument expression of type '{0}'.", argumentExpression.GetType()));

                // Configure common properties of the new variable.
                variableMap.Computation = null;
                variableMap.Advice = AdviceRequests.None;
                variableMap.ConstructorParameterName = "<anonymous, unused>";
                variableMap.FromExpression = argumentExpression;
                typeMap.VariableMaps.Add(variableMap);

                if (variableMap is ComponentVariableMap)
                    ConfigureAdHocComponentVariables((ComponentVariableMap) variableMap);
            }

            return typeMap;
        }

        internal TableMap CreateAdHocTableMap (TypeMap adHocTypeMap, TableMap underlyingViewTableMap) {
            // Construct an anonymous table map from this type.
            var adHocTableMap = new TableMap(this, adHocTypeMap);
            adHocTypeMap.PhysicalTableMap = adHocTableMap;
            adHocTableMap.UnderlyingViewTableMap = underlyingViewTableMap;
            adHocTableMap.PrimaryKey = ConfigurePrimaryKey(adHocTableMap);
            adHocTableMap.ConstructorParameters = adHocTableMap.VariableMaps.Where(v => v.ConstructorParameterName != null).ToArray();
            adHocTableMap.TableName = GetTableName(adHocTypeMap);
            return adHocTableMap;
        }

        private void ConfigureAdHocComponentVariables (ComponentVariableMap componentVariableMap) {
            var components = componentVariableMap.ComponentTypeMap.VariableMaps.FindAllDirectsAndComponents();
            foreach (var variableMap in components.Where(c => c.FromExpression is VariableExpression)) {
                var variableFromExpression = (VariableExpression) variableMap.FromExpression;
                variableMap.FromExpression = new VariableExpression(new CompositeVariableInfo(variableFromExpression.Variable, variableMap.Variable), null);

                if (variableMap is ComponentVariableMap)
                    ConfigureAdHocComponentVariables((ComponentVariableMap) variableMap);
            }
        }
        #endregion

        #region Configuration Fragments
        internal string GetTableName (TypeMap typeMap) {
            var underlyingViewTypeMap = typeMap.UnderlyingViewTypeMap;
            if (underlyingViewTypeMap == null)
                return Conventions.GetTableName(typeMap.Type);

            return "new<" + underlyingViewTypeMap.Type.Name + "> { " + string.Join(", ", typeMap.VariableMaps) + " }";
        }

        private static Type GetUnderlyingViewType (Type type) {
            var view = type.GetAttribute<ViewAttribute>(false);
            return view == null ? null : view.UnderlyingType;
        }

        private static object GetDiscriminatorValue (Type type) {
            var discriminatorValue = type.GetAttribute<DiscriminatorValueAttribute>(false);
            return discriminatorValue == null ? null : discriminatorValue.Value;
        }

        private static DirectVariableMap GetDiscriminatorVariableMap (TypeMap typeMap) {
            var directs = typeMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.BaseContainers);
            return directs.FirstOrDefault(v => v.IsDiscriminator);
        }

        private static VariableExpression GetFromExpression (VariableInfo variable, TypeMap declaringTypeMap) {
            var from = variable.Member.GetAttribute<FromAttribute>(true);
            if (from != null) {
                var actualTypeMap = declaringTypeMap.UnderlyingViewTypeMap ?? declaringTypeMap;
                return new VariableExpression(actualTypeMap.Type.GetVariable(from.VariablePath.Split('.')), null);
            }

            // All members that are part of a view are derived somehow.  Take
            // the variable if not otherwise specified.
            if (declaringTypeMap.UnderlyingViewTypeMap != null)
                return new VariableExpression(variable, null);

            return null;
        }

        private static VariableInfo GetDataRefField (VariableInfo variable) {
			var instrumentedDataRefName = "<" + variable.Member.Name + ">__dataRef";
			var instrumentedDataRef = variable.Member.DeclaringType.GetVariable(instrumentedDataRefName, TypeEx.InstanceBindingFlags, false);

			if (instrumentedDataRef != null)
				return instrumentedDataRef;

            var dataRef = variable.Member.GetAttribute<DataRefAttribute>(false);
            if (dataRef == null)
                return null;

            var dataRefVariable = variable.Member.DeclaringType.GetVariable(dataRef.DataRefFieldName);
            if (!dataRefVariable.MemberType.IsInstanceOfGenericType(typeof(DataRef<>)))
                throw new MapConfigurationException(LS.T("DataRefAttribute for '{0}' does not specify a DataRef<> member.", dataRefVariable.Name));

            return dataRefVariable;
        }

        private static DirectVariableMap GetTimeStampVariableMap (TableMap tableMap) {
            var directs = tableMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            return directs.FirstOrDefault(v => IsTimeStamp(v.Variable));
        }

        private UserMapAttribute GetUserMap(VariableInfo variable)
        {
            return variable.Member.GetAttribute<UserMapAttribute>(true);
        }

        private static bool IsDiscriminator (VariableInfo variable) {
            return variable.Member.IsDefined(typeof(DiscriminatorAttribute), true);
        }

        private static bool IsDatabaseGenerated (VariableInfo variable) {
            return variable.Member.IsDefined(typeof(DatabaseGeneratedAttribute), true);
        }

        private static bool IsComponent (VariableInfo variable) {
            return IsComponent(variable.MemberType) || variable.Member.IsDefined(typeof(ComponentAttribute), true);
        }

        private static bool IsComponent (Type type) {
            return type.UnwrapIfNullable().IsDefined(typeof(ComponentAttribute), true);
        }

        private static bool IsFlattened (Type type) {
            return type.IsDefined(typeof(FlattenAttribute), false);
        }

        private static bool IsSubordinate (VariableInfo variable) {
            return variable.Member.IsDefined(typeof(SubordinateAttribute), false);
        }

        private static bool IsTimeStamp (VariableInfo variable) {
            return variable.Member.IsDefined(typeof(TimeStampAttribute), true);
        }
        #endregion

        #region User Configuration
        /// <summary>
        /// Associates an arbitrary expression to be applied for all uses of
        /// actors within the expression.  A default filter, ordering, or 
        /// performance advice can be specified with this method.
        /// </summary>
        /// <typeparam name="T">The table type to start with.</typeparam>
        /// <param name="expression">The expression to associate.</param>
        public void AssociateWith<T> (Expression<Func<T, object>> expression) {
            var translatedExpression = LinqExpressionTranslator.Translate(expression, this);

            CollectionVariableMap subject;
            if (translatedExpression.Subject is VariableExpression) {
                var tableMap = TableMaps[typeof(T)];
                var variableExpressionSubject = (VariableExpression) translatedExpression.Subject;
                var variableMap = tableMap.VariableMaps.Find(variableExpressionSubject.Variable, VariableStructure.BaseContainers);
                if (variableMap is CollectionVariableMap)
                    subject = (CollectionVariableMap) variableMap;
                else
                    throw new MapConfigurationException(LS.T("Association subject '{0}' is not supported.", variableMap));
            } else
                throw new MapConfigurationException(LS.T("Association subject '{0}' is not supported.", translatedExpression.Subject));

            if (translatedExpression.WhereCriteria != null)
                subject.Filter = translatedExpression.WhereCriteria;

            if (translatedExpression.OrderBy.Count > 0)
                subject.OrderBy.AddRange(translatedExpression.OrderBy);
        }

        /// <summary>
        /// Constructs a new default DataContext using this configuration.
        /// </summary>
        /// <returns>A new DataContext bound to the this configuration.</returns>
        public virtual DataContext CreateContext () {
            return new DataContext(this);
        }
        #endregion

        #region Key Queries
        /// <summary>
        /// Retrieves the primary key of an instance.
        /// </summary>
        /// <param name="instance">The instance to retrieve the primary key for.</param>
        /// <returns>The primary key of the instance.</returns>
        public object GetKey (object instance) {
            if (instance == null)
                throw new ArgumentNullException("instance");

            var type = instance.GetType();
            if (!TableMaps.ContainsKey(type))
                throw new ArgumentException(LS.T("Type '{0}' has no corresponding physical table.", type), "instance");

            var tableMap = TableMaps[type];
            return tableMap.GetKey(instance);
        }
        #endregion

        #region Schema Generation
        /// <summary>
        /// Emits a DataSet containing schema information for every physical
        /// type the configuration has knowledge of.
        /// </summary>
        /// <returns>A DataSet with tables, constraints, and some reference data.</returns>
        public DataSet GenerateSchemaDataSet () {
            var set = new CachingDataSet();

            var tableMaps = TableMaps.Values.Distinct().Where(t => !t.ReferenceBase && t.UnderlyingViewTableMap == null);

            var enumData = new SqlCompoundExpression();

            // Generate tables.
            set.Value.Tables.AddRange(tableMaps.Select(t => t.CreateDataTable()).ToArray());

            // Generate auxillary enum tables.
            foreach (var tableMap in tableMaps) {
                var directs = tableMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents);
                var enums = directs.Select(d => d.PhysicalVariableType.UnwrapIfNullable()).Where(t => t.IsEnum);

                foreach (var enumType in enums)
                    AddEnumDataTable(set, enumType);
            }

            // Generate table constraints.
            foreach (var tableMap in tableMaps)
                tableMap.ConfigureDataTableConstraints(set);

            return set.Value;
        }

        private void AddEnumDataTable (CachingDataSet set, Type enumType) {
            var tableName = Conventions.GetTableName(enumType);
            if (set.ContainsTable(tableName))
                return;

            // Generate a table to store enum data.
            var table = new DataTable(tableName);
            var schemaName = Conventions.GetSchemaName(enumType);
            if (!string.IsNullOrEmpty(schemaName))
                table.SetSchemaName(schemaName);

            var primaryKeyColumn = table.Columns.Add(Conventions.GetEnumPrimaryKeyColumnName(enumType), enumType);

            var name = table.Columns.Add(Conventions.GetDataIdentifierName("Name", DataIdentifierType.Column), typeof(string));
            name.MaxLength = 100;
            name.AllowDBNull = false;

            table.Columns.Add(Conventions.GetDataIdentifierName("ShortDescription", DataIdentifierType.Column), typeof(string)).MaxLength = 300;

            table.PrimaryKey = new DataColumn[] { primaryKeyColumn };
            var pkConstraint = table.Constraints.OfType<Constraint>().Last();
            var pkConstraintName = "PK_" + (schemaName == null ? "" : schemaName + "_") + table.TableName;
            pkConstraint.ConstraintName = Conventions.GetDataIdentifierName(pkConstraintName, DataIdentifierType.Constraint);

            set.Value.Tables.Add(table);

            // Store enum data.
            foreach (var value in Enum.GetValues(enumType)) {
                var row = table.NewRow();

                row[primaryKeyColumn] = (int) value;
                row["Name"] = Enum.GetName(enumType, value);
                // todo: add enum description

                table.Rows.Add(row);
            }
        }
        #endregion

        #region Helper Queries
        /// <summary>
        /// Determines whether or not the given member is a list back
        /// reference member.
        /// </summary>
        /// <param name="member">The member to check.</param>
        /// <returns>true if the member is a list back reference, false if otherwise.</returns>
        public bool IsBackReference (MemberInfo member) {
            var collections = TableMaps.Values.SelectMany(t => t.VariableMaps.FindAll<CollectionVariableMap>(VariableStructure.WithinComponents));
            return collections.Any(c => c.KeyVariableMap.Variable.Member.MetadataToken == member.MetadataToken);
        }
        #endregion

        #region Serialization
        public string Serialize () {
#if NET4
            var options = new Nen.Runtime.Serialization.XSerializerOptions(typeof(TableMap), VariableInfo.GetSubTypes().ToArray());
            options.EnableIdRefs = true;
            options.PreserveIdRefs = true;
            options.WriteOnly = true;

            var serializer = new Nen.Runtime.Serialization.XSerializer(options);
            using (var memory = new System.IO.MemoryStream()) {
                foreach (var tableMap in TableMaps.Values) {
                    serializer.Serialize(memory, tableMap, Encoding.UTF8);
                }

                return Encoding.UTF8.GetString(memory.ToArray());
            }
#else
            throw new NotImplementedException("Serialization requires .Net Framework 4.");
#endif
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the mapping conventions used on initialization.
        /// </summary>
        public Conventions Conventions { get; private set; }

        /// <summary>
        /// Gets or sets the default store to be used with this configuration.
        /// </summary>
        public Store DefaultStore { get; set; }

        /// <summary>
        /// Gets or sets the data locale.
        /// </summary>
        public CultureInfo Locale { get; private set; }

        /// <summary>
        /// Gets a set of table maps indexed by their origin type.
        /// </summary>
        public Dictionary<Type, TableMap> TableMaps { get; private set; }

        /// <summary>
        /// Gets a set of table maps indexed by their physical table name.
        /// </summary>
        public Dictionary<string, TableMap> TableMapsByTableName { get; private set; }

        /// <summary>
        /// Gets a set of table maps indexed by their origin type name.
        /// </summary>
        public Dictionary<string, TableMap> TableMapsByTypeName { get; private set; }

        /// <summary>
        /// Gets or sets the transaction isolation level used by any created transactions.
        /// </summary>
        public System.Data.IsolationLevel IsolationLevel { get; set; }

        /// <summary>
        /// Get or sets the scope of any created transactions.
        /// </summary>
        public TransactionScopeOption TransactionScopeOption { get; set; }

        /// <summary>
        /// Gets a set of type maps indexed by their origin type.
        /// </summary>
        public Dictionary<Type, TypeMap> TypeMaps { get; private set; }

        /// <summary>
        /// Gets a set of overridden types.
        /// </summary>
        public Dictionary<Type, Type> TypeOverrides { get; private set; }
        #endregion
    }
}
