using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// The exception that is thrown when an error occurs while querying.
    /// </summary>
    [Serializable]
    public class QueryException : OrmException {
        /// <summary>
        /// Constructs a new QueryException.
        /// </summary>
        public QueryException () {
        }

        /// <summary>
        /// Constructs a new QueryException with the specified message and inner exception.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public QueryException (string message, Exception inner)
            : base(message, inner) {
        }

        /// <summary>
        /// Constructs a new QueryException with the specified message.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public QueryException (string message)
            : base(message) {
        }

        /// <summary>
        /// Constructs a new QueryException with the serialized data.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The serialization context.</param>
        protected QueryException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }

    /// <summary>
    /// The exception that is thrown when an error occurs during configuration
    /// of internal mapping structures.
    /// </summary>
    [Serializable]
    public class MapConfigurationException : OrmException {
        /// <summary>
        /// Constructs a new MapConfigurationException.
        /// </summary>
        public MapConfigurationException () {
        }

        /// <summary>
        /// Constructs a new MapConfigurationException with the specified message and inner exception.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public MapConfigurationException (string message, Exception inner)
            : base(message, inner) {
        }

        /// <summary>
        /// Constructs a new MapConfigurationException with the specified message.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public MapConfigurationException (string message)
            : base(message) {
        }

        /// <summary>
        /// Constructs a new MapConfigurationException with the serialized data.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The serialization context.</param>
        protected MapConfigurationException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }

    /// <summary>
    /// The exception that is thrown when an unrecoverable cycle is detected
    /// while mapping data.
    /// </summary>
    [Serializable]
    public class CycleException : OrmException {
        /// <summary>
        /// Constructs a new CycleException.
        /// </summary>
        public CycleException () {
        }

        /// <summary>
        /// Constructs a new CycleException with the specified message and inner exception.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public CycleException (string message, Exception inner)
            : base(message, inner) {
        }

        /// <summary>
        /// Constructs a new CycleException with the specified message.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public CycleException (string message)
            : base(message) {
        }

        /// <summary>
        /// Constructs a new CycleException with the serialized data.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The serialization context.</param>
        protected CycleException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }

    /// <summary>
    /// The exception that is thrown when an attempt is made to update a row
    /// and the update could not be completed.  This can happen because due to
    /// concurrency or security reasons.
    /// </summary>
    [Serializable]
    public class UpdateException : OrmException {
        /// <summary>
        /// Constructs a new UpdateException.
        /// </summary>
        public UpdateException () {
        }

        /// <summary>
        /// Constructs a new UpdateException with the specified message and inner exception.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public UpdateException (string message, Exception inner)
            : base(message, inner) {
        }

        /// <summary>
        /// Constructs a new UpdateException with the specified message.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public UpdateException (string message)
            : base(message) {
        }

        /// <summary>
        /// Constructs a new UpdateException with the serialized data.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The serialization context.</param>
        protected UpdateException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }

    /// <summary>
    /// The exception that is thrown when an error occurs while mapping query results.
    /// </summary>
    [Serializable]
    public class MapException : OrmException {
        /// <summary>
        /// Constructs a new MapException.
        /// </summary>
        public MapException () {
        }

        /// <summary>
        /// Constructs a new MapException with the specified message and inner exception.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public MapException (string message, Exception inner)
            : base(message, inner) {
        }

        /// <summary>
        /// Constructs a new MapException with the specified message.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public MapException (string message)
            : base(message) {
        }

        /// <summary>
        /// Constructs a new MapException with the serialized data.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The serialization context.</param>
        protected MapException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }

    /// <summary>
    /// The base exception that is thrown when an error occurs in the object/relational mapper.
    /// </summary>
    [Serializable]
    public class OrmException : Exception {
        /// <summary>
        /// Constructs a new OrmException.
        /// </summary>
        public OrmException () {
        }

        /// <summary>
        /// Constructs a new OrmException with the specified message and inner exception.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public OrmException (string message, Exception inner)
            : base(message, inner) {
        }

        /// <summary>
        /// Constructs a new OrmException with the specified message.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public OrmException (string message)
            : base(message) {
        }

        /// <summary>
        /// Constructs a new MapException with the serialized data.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The serialization context.</param>
        protected OrmException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }
}
