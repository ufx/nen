using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.ComponentModel.DataAnnotations;

using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Reflection;
using Nen.Validation;
using Nen.Caching;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Answers simple object&lt;-&gt;physical mapping questions such as, "What column
    /// does this variable belong to?", "Should this object reference be joined
    /// or ignored?", etc.
    /// </summary>
    public class Conventions {
        #region Computation Conventions
        /// <summary>
        /// Retrieves any server computation attributes that apply to the
        /// variable.
        /// </summary>
        /// <param name="variable">Tbe variable to retrieve a computation for.</param>
        /// <returns>Returns any instances of the ComputedAttribute on the variable.</returns>
        public virtual ComputedAttribute GetComputation (VariableInfo variable) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            return variable.Member.GetAttribute<ComputedAttribute>(true);
        }
        #endregion

        #region Naming Conventions
        /// <summary>
        /// Retrieves the collection key for a collection variable.  A
        /// collection key is a reference to the object that contains the
        /// collection within the collection type.
        /// </summary>
        /// <param name="variable">The collection variable to retrieve the key for.</param>
        /// <returns>The CollectionKeyAttribute value if applicable, or the declaring type name.</returns>
        public virtual string GetCollectionKeyName (VariableInfo variable) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            var collectionKey = variable.Member.GetAttribute<CollectionKeyAttribute>(true);
            if (collectionKey != null)
                return collectionKey.VariableName;

            return variable.Member.DeclaringType.Name;
        }

        /// <summary>
        /// Retrieves the column name of the primary key of an enum table.
        /// This convention is only used by the GenerateSchemaDataSet family
        /// of methods.
        /// </summary>
        /// <param name="enumType">The enum type.</param>
        /// <returns>Always returns Id.</returns>
        public virtual string GetEnumPrimaryKeyColumnName (Type enumType) {
            return GetDataIdentifierName("Id", DataIdentifierType.Column);
        }

        /// <summary>
        /// Retrieves the column name of the primary key.
        /// </summary>
        /// <param name="primaryKeyVariable">The primary key variable.</param>
        /// <param name="tableMap">The table map to which this primary key belongs.</param>
        /// <returns>The output of GetPrimitiveColumnName.</returns>
        public virtual string GetPrimaryKeyColumnName (VariableInfo primaryKeyVariable, TableMap tableMap) {
            return GetPrimitiveColumnName(primaryKeyVariable);
        }

        /// <summary>
        /// Retrieves the variable name that represents the primary key of a type.
        /// </summary>
        /// <param name="type">The type to find the primary key variable name for.</param>
        /// <returns>Always returns Id.</returns>
        public virtual string GetPrimaryKeyVariableName (Type type) {
            return "Id";
        }

        /// <summary>
        /// Retrieves the column name of a primitive column that requires
        /// little mapping work such as a string, integer, DateTime, etc.
        /// </summary>
        /// <param name="variable">The primitive variable.</param>
        /// <returns>The variable name.</returns>
        public virtual string GetPrimitiveColumnName (VariableInfo variable) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            var nameOverride = GetColumnNameOverride(variable);
            return nameOverride == null ? GetDataIdentifierName(variable.Name.Replace(".", ""), DataIdentifierType.Column) : nameOverride;
        }

        /// <summary>
        /// Retrieves the column name of a reference to another physical table.
        /// </summary>
        /// <param name="variable">The reference variable.</param>
        /// <returns>The variable name + Id.</returns>
        public virtual string GetReferenceColumnName (VariableInfo variable) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            var nameOverride = GetColumnNameOverride(variable);
            return nameOverride == null ? GetDataIdentifierName(variable.Name.Replace(".", "") + "Id", DataIdentifierType.Column) : nameOverride;
        }

        public virtual string GetColumnNameOverride (VariableInfo variable) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            var column = variable.Member.GetAttribute<ColumnAttribute>(true);
            return column == null ? null : column.ColumnName;
        }

        public virtual string GetComponentColumnName(Stack<VariableInfo> path, DirectVariableMap edge)
        {
            string name = string.Join("", path.Select(v => v.Name).Skip(1).Reverse());
            return GetDataIdentifierName(new string[] { name, edge.ColumnName }, DataIdentifierType.Column);
        }

        /// <summary>
        /// Retrieves the target variable name of a subordinate.
        /// </summary>
        /// <param name="variable">The subordinate variable.</param>
        /// <returns>The name specified by SubordinateAttribute, or the declaring type name.</returns>
        public virtual string GetSubordinateTargetName (VariableInfo variable) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            var subordinate = variable.Member.GetAttribute<SubordinateAttribute>(true);
            if (subordinate != null && !string.IsNullOrEmpty(subordinate.TargetVariableName))
                return subordinate.TargetVariableName;

            return variable.Member.DeclaringType.Name;
        }

        /// <summary>
        /// Retrieves the table name of a physical type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The name specified by TableAttribute, or the type name.</returns>
        public virtual string GetTableName (Type type) {
            if (type == null)
                throw new ArgumentNullException("type");

            string name;

            var table = type.GetAttribute<TableAttribute>(false);
            if (table == null || string.IsNullOrEmpty(table.TableName))
                name = type.Name;
            else
                name = table.TableName;

            return GetDataIdentifierName(name, DataIdentifierType.Table);
        }

        /// <summary>
        /// Retrieves the schema name of a physical type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The name specified by TableAttribute, or null.</returns>
        public virtual string GetSchemaName (Type type) {
            if (type == null)
                throw new ArgumentNullException("type");

            var table = type.GetAttribute<TableAttribute>(false);
            return table == null ? null : GetDataIdentifierName(table.SchemaName, DataIdentifierType.Schema);
        }

        public string GetDataIdentifierName(string identifier, DataIdentifierType type)
        {
            return GetDataIdentifierName(new string[] { identifier }, type);
        }

        public virtual string GetDataIdentifierName(string[] parts, DataIdentifierType type)
        {
            return string.Join("", parts);
        }

        public virtual int? GetIdentifierLengthLimit ()
        {
            // By default we assume there is no limit.
            return null;
        }
        #endregion

        #region Performance Conventions
        /// <summary>
        /// Retrieves performance advice requests for a variable, including
        /// requests specified by the AdviceAttribute.  By default the
        /// JoinOnLoadWithOnly is specified for LazyReferenceVariableMaps.  
        /// Member advice takes precedence over type advice.
        /// </summary>
        /// <param name="variableMap">The variable map to find advice for.</param>
        /// <returns>Only advice specified by the AdviceAttribute.</returns>
        public virtual AdviceRequests GetAdvice (VariableMap variableMap) {
            if (variableMap == null)
                throw new ArgumentNullException("variableMap");

            var memberAttribute = variableMap.Variable.Member.GetAttribute<AdviceAttribute>(true);
            if (memberAttribute != null)
                return memberAttribute.Requests;

            // If the variableMap is lazy, ignore DeferJoin attribute on the type.
            var isLazy = variableMap is LazyReferenceVariableMap;

            var typeAttribute = variableMap.Variable.MemberType.GetAttribute<AdviceAttribute>(true);
            if (typeAttribute != null)
            {
                if (isLazy && typeAttribute.Requests == AdviceRequests.DeferJoin)
                    return AdviceRequests.JoinOnIncludeOnly;
                else
                    return typeAttribute.Requests;
            }

            return isLazy ? AdviceRequests.JoinOnIncludeOnly : AdviceRequests.None;
        }

        public virtual CacheOptions GetCacheOptions(Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            var cacheAttribute = type.GetAttribute<QueryCacheAttribute>(true);
            if (cacheAttribute != null)
                return cacheAttribute.Options;

            return null;
        }
        #endregion

        #region Persistence Conventions
        /// <summary>
        /// Retrieves an indicator on whether the specified member is
        /// physically persistent somehow.
        /// </summary>
        /// <param name="member">The member to find the persistence indicator for.</param>
        /// <returns>The value of the PersistentAttribute, or true if the attribute is not found, and false if otherwise.</returns>
        public virtual bool IsPersistent (MemberInfo member) {
            var attribute = member.GetAttribute<PersistentAttribute>(false);
            if (attribute != null)
                return attribute.IsPersistent;

            return !(member is Type);
        }

        /// <summary>
        /// Retrieves an indicator on whether the specified member must be
        /// present when inserting new rows.
        /// </summary>
        /// <param name="variable">The variable to find the required indicator for.</param>
        /// <returns>true if the variable must be present, false if otherwise.</returns>
        public virtual bool IsRequired (VariableInfo variable) {
            var required = variable.Member.GetAttribute<RequiredAttribute>(true);
            if (required != null)
                return true;

            var type = variable.MemberType;
            if ((type.IsPrimitive || type.IsStruct()) && !type.IsNullable())
                return true;

            return false;
        }

        /// <summary>
        /// Retrieves an indicator on whether the assembly should be searched
        /// for persistent types to configure.
        /// </summary>
        /// <param name="assembly">The assembly to find the persistent type indicator for.</param>
        /// <returns>false if the assembly is a microsoft assembly, true if otherwise.</returns>
        public virtual bool HasPersistentTypes (Assembly assembly) {
            return !assembly.IsMicrosoftAssembly();
        }
        #endregion

        #region Structure Conventions
        /// <summary>
        /// Retrieves the constructor parameter name for an immutable variable.
        /// </summary>
        /// <param name="variable">The immutable variable.</param>
        /// <returns>The name specified by ImmutableAttribute, or the variable name.</returns>
        public virtual string GetConstructorParameterName (VariableInfo variable) {
            var attribute = variable.Member.GetAttribute<ImmutableAttribute>(false);
            if (attribute == null)
                return null;

            return string.IsNullOrEmpty(attribute.ConstructorParameterName) ? variable.Member.Name : attribute.ConstructorParameterName;
        }

        public virtual HierarchyMapping GetHierarchyMapping (Type type) {
            var hierarchyAttribute = type.GetAttribute<HierarchyAttribute>(false);
            return hierarchyAttribute != null ? hierarchyAttribute.Mapping : HierarchyMapping.Inherit;
        }

        /// <summary>
        /// Retrieves a primary key associated with the variable map.
        /// </summary>
        /// <param name="variableMap">The variable mapping for the primary key.</param>
        /// <returns>AutoIncrementingPrimaryKey for integer types, AssignedPrimaryKey for Guids and everything else.</returns>
        public virtual PrimaryKey GetPrimaryKey (DirectVariableMap variableMap) {
            var type = variableMap.PhysicalVariableType;

            // Guids are typically chosen because they're easily assignable.
            if (type == typeof(Guid))
                return new AssignedPrimaryKey(variableMap);

            // Integers are typically chosen for some kind of sequential key.
            // Assume it's an auto incrementing key.
            if (type == typeof(Int32) || type == typeof(Int64) || type == typeof(Int16))
                return new AutoIncrementingPrimaryKey(variableMap);

            // Default to an assigned key.
            return new AssignedPrimaryKey(variableMap);
        }

        /// <summary>
        /// Retrieves an indicator on whether all references to the type are
        /// redirected to the base physical table of that type.  This is useful
        /// when a subclass that contains no extra information does not need to
        /// exist as the target of a foreign key.  It is assumed the base type
        /// will take the role of foreign key target.
        /// </summary>
        /// <param name="type">The possibly non-physical type.</param>
        /// <returns>true if ReferenceBaseAttribute is specified, false if otherwise.</returns>
        public virtual bool GetReferenceBase (Type type) {
            var attr = type.GetAttribute<ReferenceBaseAttribute>(false);
            return attr != null;
        }
        #endregion
    }

    public class SequenceConventions : Conventions
    {
        public override PrimaryKey GetPrimaryKey(DirectVariableMap variableMap)
        {
            var key = base.GetPrimaryKey(variableMap);
            if (key is AutoIncrementingPrimaryKey)
            {
                // Convert auto incrementing keys to sequence keys.
                var sequence = new SequencePrimaryKey(variableMap);
                sequence.SchemaName = GetSchemaName(variableMap.DeclaringTypeMap.Type);
                sequence.Name = GetPrimaryKeySequenceName(variableMap);
                return sequence;
            }
            return key;
        }

        public virtual string GetPrimaryKeySequenceName(DirectVariableMap variableMap)
        {
            if (variableMap == null)
                throw new ArgumentNullException("variableMap");

            var type = variableMap.PhysicalTableMap.RootTableMap.OriginTypeMap.Type;
            return GetDataIdentifierName("PKSEQ_" + type.Name, DataIdentifierType.Sequence);
        }
    }

    public enum DataIdentifierType
    {
        Schema = 1,
        Table,
        Column,
        Sequence,
        Constraint
    }
}
