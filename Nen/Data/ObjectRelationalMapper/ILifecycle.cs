﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Provides data about the persistence lifecycle of an object.
    /// </summary>
    public interface ILifecycle {
        /// <summary>
        /// Gets or sets an indicator on whether the object is new.
        /// </summary>
        bool IsNew { get; set; }
    }
}
