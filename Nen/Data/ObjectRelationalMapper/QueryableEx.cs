﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Linq.Expressions;

namespace Nen.Data.ObjectRelationalMapper
{
    public static class EnumerableEx
    {
        public static IQueryable<TSource> Include<TSource, TValue>(this IEnumerable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            return source.AsQueryable();
        }

        public static IQueryable<TSource> LoadWith<TSource, TValue>(this IEnumerable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            return source.AsQueryable();
        }

        public static IQueryable<TSource> Defer<TSource, TValue>(this IEnumerable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            return source.AsQueryable();
        }

        public static IQueryable<TSource> Ignore<TSource, TValue>(this IEnumerable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            return source.AsQueryable();
        }

        public static IQueryable<TSource> JoinOnLoadWithOnly<TSource, TValue>(this IEnumerable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            return source.AsQueryable();
        }

        public static IQueryable<TSource> JoinOnIncludeOnly<TSource, TValue>(this IEnumerable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            return source.AsQueryable();
        }

        public static IQueryable<TBase> WhenType<TBase, TSub>(this IEnumerable<TBase> source, Expression<Func<IEnumerable<TSub>, object>> action)
        {
            var func = action.Compile();
            func(source.OfType<TSub>());
            return source.AsQueryable();
        }
    }

    /// <summary>
    /// Object/Relational mapping extensions to the System.Linq.IQueryable interface.
    /// </summary>
    public static class QueryableEx
    {
        /// <summary>
        /// Defer loading of an object to a later stage.  Useful for objects
        /// that are large or often NULL.  Reduces query size, sometimes
        /// increases number of queries.
        /// </summary>
        /// <typeparam name="TSource">The type of element of source.</typeparam>
        /// <typeparam name="TValue">The type of key returned by keySelector.</typeparam>
        /// <param name="source">A query to specify defer advice for.</param>
        /// <param name="selector">A function to extract a key from an element.</param>
        /// <returns>A new expression that will defer loading of the key.</returns>
        public static IQueryable<TSource> Defer<TSource, TValue>(this IQueryable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            var method = typeof(EnumerableEx).GetMethod("Defer").MakeGenericMethod(typeof(TSource), typeof(TValue));
            var args = new Expression[] { source.AsQueryable().Expression, Expression.Quote(selector) };
            return source.Provider.CreateQuery<TSource>(Expression.Call(null, method, args));
        }

        public static IQueryable<TSource> Ignore<TSource, TValue>(this IQueryable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            var method = typeof(EnumerableEx).GetMethod("Ignore").MakeGenericMethod(typeof(TSource), typeof(TValue));
            var args = new Expression[] { source.AsQueryable().Expression, Expression.Quote(selector) };
            return source.Provider.CreateQuery<TSource>(Expression.Call(null, method, args));
        }

        /// <summary>
        /// Always loads an object or collection with a referencing or
        /// containing object.  Reduces client->server round trips,
        /// increases query size, number of queries, and server->client
        /// result size.
        /// </summary>
        /// <typeparam name="TSource">The type of element of source.</typeparam>
        /// <typeparam name="TValue">The type of key returned by keySelector.</typeparam>
        /// <param name="source">A query to specify eager loading advice for.</param>
        /// <param name="selector">A function to extract a key from an element.</param>
        /// <returns>A new expression that will load the key eagerly.</returns>
        public static IQueryable<TSource> LoadWith<TSource, TValue>(this IQueryable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            var method = typeof(EnumerableEx).GetMethod("LoadWith").MakeGenericMethod(typeof(TSource), typeof(TValue));
            var args = new Expression[] { source.AsQueryable().Expression, Expression.Quote(selector) };
            return source.Provider.CreateQuery<TSource>(Expression.Call(null, method, args));
        }

        public static IQueryable<TSource> Include<TSource, TValue>(this IQueryable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            var method = typeof(EnumerableEx).GetMethod("Include").MakeGenericMethod(typeof(TSource), typeof(TValue));
            var args = new Expression[] { source.AsQueryable().Expression, Expression.Quote(selector) };
            return source.Provider.CreateQuery<TSource>(Expression.Call(null, method, args));
        }

        public static IQueryable<TSource> JoinOnLoadWithOnly<TSource, TValue>(this IQueryable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            var method = typeof(EnumerableEx).GetMethod("JoinOnLoadWithOnly").MakeGenericMethod(typeof(TSource), typeof(TValue));
            var args = new Expression[] { source.AsQueryable().Expression, Expression.Quote(selector) };
            return source.Provider.CreateQuery<TSource>(Expression.Call(null, method, args));
        }

        public static IQueryable<TSource> JoinOnIncludeOnly<TSource, TValue>(this IQueryable<TSource> source, Expression<Func<TSource, TValue>> selector)
        {
            var method = typeof(EnumerableEx).GetMethod("JoinOnIncludeOnly").MakeGenericMethod(typeof(TSource), typeof(TValue));
            var args = new Expression[] { source.AsQueryable().Expression, Expression.Quote(selector) };
            return source.Provider.CreateQuery<TSource>(Expression.Call(null, method, args));
        }

        public static IQueryable<TBase> WhenType<TBase, TSub>(this IQueryable<TBase> source, Expression<Func<IEnumerable<TSub>, object>> action)
            where TSub : TBase
        {
            var method = typeof(EnumerableEx).GetMethod("WhenType").MakeGenericMethod(typeof(TBase), typeof(TSub));
            var args = new Expression[] { source.AsQueryable().Expression, Expression.Quote(action) };
            return source.Provider.CreateQuery<TBase>(Expression.Call(null, method, args));
        }
    }
}
