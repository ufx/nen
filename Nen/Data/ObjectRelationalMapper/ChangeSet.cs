﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.SqlModel;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Data.ObjectRelationalMapper.QueryEngine.SqlGenerators;
using Nen.Reflection;
using Nen.Internal;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Specifies options for all operations by the ChangeSet.
    /// </summary>
    [Flags]
    public enum ChangeSetOptions
    {
        /// <summary>
        /// No options specified (default.)
        /// </summary>
        None = 0,

        /// <summary>
        /// Tracks all changes flowing through the ChangeSet.
        /// </summary>
        TrackChanges = 1,

        /// <summary>
        /// Automatically adds all tracked changes to the ChangeSet whenever
        /// Commit is called.  Requires the ChangeSet is created by a DataContext
        /// with the TrackChanges option set.
        /// </summary>
        ImplicitlyCommitTrackedChanges = 2,
    }

    /// <summary>
    /// Records all change operations for a transactional commit to a Store.
    /// </summary>
    public class ChangeSet {
        private int _sequentialKeyCounter = -1000;
        private Dictionary<object, object> _keysByInstance = new Dictionary<object, object>();
        private object _operationsLock = new object();
        private Dictionary<Type, IgnoredComputation> _ignoredComputations;

        #region Constructors
        /// <summary>
        /// Constructs a new ChangeSet.
        /// </summary>
        /// <param name="context">The context of the changes.</param>
        protected internal ChangeSet (DataContext context, ChangeSetOptions options) {
            ChangedInstances = new Dictionary<object, bool>();
            OperationsByInstance = new Dictionary<object, ChangeOperation>();
            Context = context;
            Options = options;
            Data = new LightSet();
        }
        #endregion

        #region Committing
        /// <summary>
        /// Commits the changes in this set in a single transaction.
        /// </summary>
        public virtual void Commit () {
            if (IsCommitted)
                throw new OrmException("Attempt to commit ChangeSet that was previously committed.");

            Context.OnChangeSetCommitting(this);

            // Prepare operations and data for commit.
            SetDataRelationships();

            // Perform commit.
            Context.Configuration.DefaultStore.Commit(this);

            IsCommitted = true;

            Context.OnChangeSetCommitted(this);
        }

        private void SetDataRelationships () {
            var configuration = Context.Configuration;
            foreach (var table in Data.Tables) {
                var tableMap = (TableMap) table.ExtendedProperties["Nen.TableMap"];

                foreach (var referenceVariableMap in tableMap.VariableMaps.FindAll<ReferenceVariableMap>(VariableStructure.WithinComponents)) {
                    if (!Data.ContainsTable(referenceVariableMap.ReferencedTableMap.TableName))
                        continue;

                    Data.AddRelation(referenceVariableMap.ReferencedTableMap.TableName, referenceVariableMap.ReferencedTableMap.PrimaryKey.VariableMap.ColumnName, referenceVariableMap.PhysicalTableMap.TableName, referenceVariableMap.ColumnName);
                }
            }
        }
        #endregion

        #region Keys
        internal object GetKey (object instance) {
            object key;
            if (_keysByInstance.TryGetValue(instance, out key))
                return key;

            var tableMap = Context.Configuration.TableMaps[instance.GetType()];
            key = tableMap.GetKey(instance);

            // When inserts return the value of the key, the key may not exist
            // yet, so use a temporary generated number.
            if (tableMap.PrimaryKey.IsDatabaseGenerated && tableMap.PrimaryKey.IsDefaultKey(key)) {
                var temporaryKey = _sequentialKeyCounter--;
                _keysByInstance[instance] = temporaryKey;
                OnTemporaryKeyCreated(instance, temporaryKey);
                return temporaryKey;
            }

            // Otherwise, just return the key stored on the instance.
            _keysByInstance[instance] = key;
            return key;
        }

        protected virtual void OnTemporaryKeyCreated (object instance, object key) {
            // Do nothing.
        }
        #endregion

        #region Operations
        /// <summary>
        /// Adds a changed instance to be committed.
        /// </summary>
        /// <typeparam name="T">The type of the instance to add.</typeparam>
        /// <param name="instance">The changed instance to add.</param>
        /// <returns>The added instance.</returns>
        public T Add<T>(T instance)
        {
            AddCore(instance);
            return instance;
        }

        /// <summary>
        /// Adds a changed instance to be committed.
        /// </summary>
        /// <param name="instance">The changed instance to add.</param>
        /// <returns>The added instance.</returns>
        public object Add (object instance)
        {
            AddCore(instance);
            return instance;
        }

        private void AddCore(object instance)
        {
            ChangedInstances[instance] = true;

            if (Context.IsNew(instance))
                Insert(instance);
            else
                Update(instance);
        }

        /// <summary>
        /// Adds an enumerable of changed instances to be committed.
        /// </summary>
        /// <param name="enumerable">The enumerable of changed instances to add.</param>
        public void AddRange (System.Collections.IEnumerable enumerable) {
            foreach (var item in enumerable)
                Add(item);
        }

        /// <summary>
        /// Deletes an instance from the Store.
        /// </summary>
        /// <param name="instance">The instance to delete.</param>
        public void Delete (object instance) {
            // New instances need not be deleted, but remove any other pending operations for them.
            if (Context.IsNew(instance))
            {
                OperationsByInstance.Remove(instance);
                return;
            }

            var delete = CreateChangeOperation<DeleteChangeOperation>(instance);
            AddOperation(delete);
        }

        internal Collection<ChangeOperation> GetOrderedOperations () {
            // Create an operations table for checking references, use this to topologically sort the operations.
            var operationsByTableByKey = new Dictionary<TableMap, Dictionary<object, ChangeOperation>>();
            foreach (var value in OperationsByInstance.Values) {
                foreach (var tableMap in value.TableMap.GetPhysicalHierarchy())
                    operationsByTableByKey.Activate(tableMap).Add(value.Key, value);
            }

            var topologicallySortedOperations = OperationsByInstance.Values.TopologicalSort(op => op.GetReferences(operationsByTableByKey));

            // Order of operations after topological sort:
            // 1. All inserts, in reference order.
            // 2. All updates, in reference order.
            // 3. All deletes, in reverse reference order.

            var orderedOperations = new List<ChangeOperation>();
            orderedOperations.AddRange(topologicallySortedOperations.OfType<InsertChangeOperation>());
            orderedOperations.AddRange(topologicallySortedOperations.OfType<UpdateChangeOperation>());
            orderedOperations.AddRange(topologicallySortedOperations.OfType<DeleteChangeOperation>().Reverse());
            return new Collection<ChangeOperation>(orderedOperations);
        }

        protected virtual void Insert (object instance) {
            var insert = CreateDataChangeOperation<InsertChangeOperation>(instance);
            AddOperation(insert);
        }

        protected virtual void Update (object instance) {
            var update = CreateDataChangeOperation<UpdateChangeOperation>(instance);
            if (update != null)
                AddOperation(update);
        }

        private T CreateDataChangeOperation<T> (object instance) where T : DataChangeOperation, new() {
            var op = CreateChangeOperation<T>(instance);
            return op.FillData(Context.TrackedData) ? op : null;
        }

        private T CreateChangeOperation<T> (object instance) where T : ChangeOperation, new() {
            var type = instance.GetType();
            var tableMaps = Context.Configuration.TableMaps;
            if (!tableMaps.ContainsKey(type))
                throw new OrmException(LS.T("Attempt to save unconfigured type '{0}'.  Classes must be marked [Persistent].", type));

            var op = new T();
            op.ChangeSet = this;
            op.Instance = instance;
            op.TableMap = tableMaps[type];
            op.Key = GetKey(instance);
            return op;
        }

        private void AddOperation (ChangeOperation op) {
            lock (_operationsLock) {
                // Always replace an existing operation with a new one.
                OperationsByInstance[op.Instance] = op;
            }
        }
        #endregion

        #region Change Tracking Convenience
        /// <summary>
        /// Adds all reachable, altered data to the ChangeSet.  Only available
        /// if the ChangeSet was created from a DataContext with the TrackChanges
        /// option specified.
        /// </summary>
        public virtual void AddTrackedChanges()
        {
            throw new InvalidOperationException(LS.T("AddTrackedChanges may only be called with a TrackingChangeSet.  Set TrackChanges in your ContextOptions."));
        }
        #endregion

        #region Ignored Computations
        public void IgnoreComputation(Type attributeType, Type instanceType = null)
        {
            if (_ignoredComputations == null)
                _ignoredComputations = new Dictionary<Type, IgnoredComputation>();

            var ignored = _ignoredComputations.Activate(attributeType);
            if (instanceType == null)
                ignored.IsForAllTypes = true;
            else
                ignored.InstanceTypes.Add(instanceType);
        }

        internal bool IsComputationIgnored(ComputedAttribute attribute, Type instanceType)
        {
            if (_ignoredComputations == null)
                return false;

            IgnoredComputation ignored;
            if (_ignoredComputations.TryGetValue(attribute.GetType(), out ignored))
                return ignored.IsForAllTypes || ignored.InstanceTypes.Any(t => t.IsAssignableFrom(instanceType));

            return false;
        }

        internal bool IsComputationIgnored (ComputedAttribute attribute, object instance)
        {
            return IsComputationIgnored(attribute, instance.GetType());
        }
        #endregion

        #region Accessors
        public Dictionary<object, bool> ChangedInstances { get; private set; }
        public Dictionary<object, ChangeOperation> OperationsByInstance { get; private set; }
        public ChangeSetOptions Options { get; private set; }
        public bool IsCommitted { get; private set; }

        /// <summary>
        /// Gets the context of the changes.
        /// </summary>
        public DataContext Context { get; private set; }

        public LightSet Data { get; private set; }
        #endregion
    }

    public class TrackingChangeSet : ChangeSet {
        #region Constructors
        public TrackingChangeSet (DataContext context, ChangeSetOptions options)
            : base(context, options) {
        }
        #endregion

        public override void Commit()
        {
            if ((Options & ChangeSetOptions.ImplicitlyCommitTrackedChanges) == ChangeSetOptions.ImplicitlyCommitTrackedChanges)
                AddTrackedChanges();

            base.Commit();
        }

        #region Change Tracking
        private bool IsDeleted(object instance)
        {
            ChangeOperation existingOperation;
            return OperationsByInstance.TryGetValue(instance, out existingOperation) && existingOperation is DeleteChangeOperation;
        }

        /// <summary>
        /// Adds all reachable, altered data to the ChangeSet.
        /// </summary>
        public override void AddTrackedChanges()
        {
            foreach (var obj in Context.GetAttachedObjects())
            {
                // Refresh the object data if it isn't marked for deletion.
                if (!IsDeleted(obj))
                {
                    Add(obj);
                    AddUnattachedReferences(obj);
                }
            }

            // Refresh inserted objects.
            foreach (var insert in this.OperationsByInstance.Values.OfType<InsertChangeOperation>().ToArray())
            {
                if (!IsDeleted(insert))
                    Add(insert.Instance);
            }
        }

        protected override void OnTemporaryKeyCreated (object instance, object key) {
            base.OnTemporaryKeyCreated(instance, key);

            // Automatically track referenced inserts if this instance isn't
            // already being tracked.

            if (!ChangedInstances.ContainsKey(instance))
                Add(instance);
        }

        protected override void Insert (object instance) {
            base.Insert(instance);
            AddUnattachedReferences(instance);
        }

        protected override void Update(object instance)
        {
            base.Update(instance);
            AddUnattachedReferences(instance);
        }

        internal void AddUnattachedReferences (object instance) {
            var tableMap = Context.Configuration.TableMaps[instance.GetType()];

            var collectionVariableMaps = tableMap.VariableMaps.FindAll<CollectionVariableMap>(VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            foreach (var collectionVariableMap in collectionVariableMaps) {
                var collection = (System.Collections.ICollection) collectionVariableMap.Variable.GetValue(instance);
                if (collection == null)
                    continue;

                var futureItems = collection.GetInternalItems() as IFuture;
                if (futureItems != null && !futureItems.IsResolved)
                    continue; // Skip unresolved futures.

                foreach (var item in collection) {
                    if (!ChangedInstances.ContainsKey(item) && Context.IsNew(item))
                        Add(item);
                }
            }

            var subordinateVariableMaps = tableMap.VariableMaps.FindAll<SubordinateVariableMap>(VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            foreach (var subordinateVariableMap in subordinateVariableMaps) {
                var dominantValue = subordinateVariableMap.Variable.GetValue(instance);
                if (dominantValue == null)
                    continue;

                var futureDominantValue = dominantValue as IFuture;
                if (futureDominantValue != null) {
                    if (futureDominantValue.IsResolved)
                        dominantValue = futureDominantValue.Resolve();
                    else
                        continue; // Skip unresolved futures;
                }

                if (Context.IsNew(dominantValue))
                    Add(dominantValue);
            }
        }
        #endregion
    }

    /// <summary>
    /// Records information needed to create, change, or delete an object.
    /// </summary>
    public abstract class ChangeOperation {
        #region Committing
        public virtual void PreCommit () {
            // Do nothing by default.
        }

        internal virtual void PostCommit () {
            // Do nothing by default.
        }
        #endregion

        #region Queries
        internal abstract IEnumerable<ChangeOperation> GetReferences (Dictionary<TableMap, Dictionary<object, ChangeOperation>> operationsByTableByKey);
        #endregion

        #region Command Generation
        public abstract SqlExpression GenerateSql ();
        protected abstract string GetStoredCommandKeyCore ();
        internal abstract SqlGenerator CreateCommandGenerator ();
        protected abstract void FillCommandData (IDbCommand command, SqlFormatter formatter);

        protected string GetStoredCommandKey()
        {
            // No keys when prepare isn't supported.
            return ChangeSet.Context.Configuration.DefaultStore.IsPrepareSupported ? GetStoredCommandKeyCore() : null;
        }

        public IDbCommand GenerateCommand (SqlFormatter formatter, IDbConnection conn, IDbTransaction tran, out int numberOfStatements, out bool isPrepared) {
            var context = ChangeSet.Context;

            var commandKey = GetStoredCommandKey();

            if (commandKey != null) {
                var storedCommand = context.GetStoredCommand(commandKey);
                if (storedCommand != null) {
                    isPrepared = true;
                    FillCommandData(storedCommand.Command, formatter);
                    numberOfStatements = storedCommand.NumberOfStatements;
                    return storedCommand.Command;
                }
            }

            var generator = CreateCommandGenerator();
            var sql = generator.Generate();
            var command = formatter.Generate(sql);

            if (context.CommandTimeout != null)
                command.CommandTimeout = context.CommandTimeout.Value;
            
            FillCommandData(command, formatter);
            numberOfStatements = sql.NumberOfStatements;

            if (commandKey != null)
            {
                isPrepared = true;
                command.Connection = conn;
                command.Transaction = tran;
                command.Prepare();
                context.StoreCommand(commandKey, command, sql.NumberOfStatements);
            }
            else
                isPrepared = false;

            return command;
        }
        #endregion

        #region Object Members
        public override string ToString () {
            return TableMap == null ? "no table" : TableMap.ToString();
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the ChangeSet this operation belongs to.
        /// </summary>
        public ChangeSet ChangeSet { get; internal set; }

        /// <summary>
        /// Gets the object instance this operation is acting on.
        /// </summary>
        public object Instance { get; internal set; }

        public object Key { get; internal set; }

        /// <summary>
        /// Gets the table map this operation is performed on.
        /// </summary>
        public TableMap TableMap { get; internal set; }
        #endregion
    }

    /// <summary>
    /// Records information needed to delete an object.
    /// </summary>
    public class DeleteChangeOperation : ChangeOperation {
        #region Constructors
        /// <summary>
        /// Constructs a new DeleteChangeOperation.
        /// </summary>
        public DeleteChangeOperation () {
        }
        #endregion

        #region Cache consistency
        internal void RemoveDeletedData(LightSet data)
        {
            foreach (var tableMap in TableMap.GetPhysicalHierarchy())
            {
                if (data.ContainsTable(tableMap.TableName))
                {
                    var table = data[tableMap.TableName];
                    var row = table.FindRowByPrimaryKey(Key);
                    if (row != null)
                        table.RemoveRow(row);
                }
            }
        }
        #endregion

        #region Command Generation
        internal override SqlGenerator CreateCommandGenerator () {
            return new DeleteSqlGenerator(TableMap);
        }

        protected override void FillCommandData (IDbCommand command, SqlFormatter formatter) {
            var parameter = (IDbDataParameter)command.Parameters[formatter.FormatParameterName("N__PrimaryKey__")];
            parameter.Value = formatter.FormatValue(Key);
        }

        protected override string GetStoredCommandKeyCore () {
            return "delete_" + TableMap.OriginTypeMap.Type.FullName;
        }

        internal override IEnumerable<ChangeOperation> GetReferences (Dictionary<TableMap, Dictionary<object, ChangeOperation>> operationsByTableByKey) {
            var references = TableMap.VariableMaps.FindAll<ReferenceVariableMap>(VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            foreach (var reference in references)
            {
                var foreignKey = reference.GetValueForDatabaseCore(Instance, ChangeSet);

                if (foreignKey == null || foreignKey == DBNull.Value)
                    continue;

                if (operationsByTableByKey.ContainsKey(reference.ReferencedTableMap))
                {
                    var operationsByKey = operationsByTableByKey[reference.ReferencedTableMap];
                    if (operationsByKey.ContainsKey(foreignKey))
                        yield return operationsByKey[foreignKey];
                }
            }
        }

        public override SqlExpression GenerateSql () {
            return new DeleteSqlGenerator(TableMap).Generate();
        }
        #endregion
    }

    /// <summary>
    /// Records information needed to create or change an object with data.
    /// </summary>
    public abstract class DataChangeOperation : ChangeOperation {
        #region Constructors
        /// <summary>
        /// Constructs a new DataChangeOperation.
        /// </summary>
        protected DataChangeOperation () {
            RemapData = new List<Tuple<VariableInfo[], object>>();
            Rows = new Dictionary<TableMap, LightRow>();
        }
        #endregion

        #region Data Filling
        internal bool FillData (LightSet currentData) {
            var changeRows = new List<LightRow>();
            FillTables(ChangeSet.Data, currentData, TableMap, changeRows);

            if (changeRows.Count == 0)
                return false;

            foreach (var row in changeRows) {
                var tableMap = (TableMap) row.Table.ExtendedProperties["Nen.TableMap"];
                Rows[tableMap] = row;
            }

            return true;
        }

        protected void FillTables (LightSet changeData, LightSet currentData, TableMap tableMap, List<LightRow> changeRows) {
            if (tableMap.BasePhysicalTableMap != null)
                FillTables(changeData, currentData, tableMap.BasePhysicalTableMap, changeRows);

            if (!tableMap.ReferenceBase) {
                var changeTable = GetTableToFill(changeData, tableMap);
                var changeRow = GetRowToFill(changeTable, tableMap);
                var currentRow = GetCurrentRow(currentData, tableMap);

                var hasColumnChanges = FillChangedColumns(changeRow, currentRow, tableMap);
                if (hasColumnChanges) {
                    if (!changeRow.IsAddedToTable)
                        changeTable.AddRow(changeRow);

                    changeRows.Add(changeRow);
                }
            }
        }

        protected virtual bool FillChangedColumns (LightRow changeRow, LightRow currentRow, TableMap tableMap) {
            foreach (var variableMap in tableMap.VariableMaps.FindAllDirectsAndComponents())
                variableMap.StoreValueOnRow(Instance, changeRow, ChangeSet);

            return true;
        }

        protected static LightTable GetTableToFill (LightSet data, TableMap tableMap) {
            if (data.ContainsTable(tableMap.TableName))
                return data[tableMap.TableName];

            var table = tableMap.CreateLightTable();
            table.ExtendedProperties["Nen.TableMap"] = tableMap;
            data.AddTable(table);
            return table;
        }

        protected LightRow GetRowToFill (LightTable table, TableMap tableMap) {
            LightRow row = null;
            if (Key != null)
                row = table.FindRowByPrimaryKey(Key);

            return row ?? table.CreateRow();
        }

        private LightRow GetCurrentRow (LightSet data, TableMap tableMap) {
            if (data == null || !data.ContainsTable(tableMap.TableName))
                return null;

            var table = data[tableMap.TableName];
            return table.FindRowByPrimaryKey(Key);
        }
        #endregion

        #region Data
        internal object GetData (DirectVariableMap variableMap) {
            if (!Rows.ContainsKey(variableMap.PhysicalTableMap))
                return null;

            var row = Rows[variableMap.PhysicalTableMap];
            var value = row[variableMap.ColumnName];
            return value == DBNull.Value ? null : value;
        }

        internal virtual bool HasData (DirectVariableMap variableMap) {
            return true;
        }

        protected void SetData (DirectVariableMap variableMap, object value) {
            value = value ?? DBNull.Value;

            // Special case: Handle set data for copied primary keys.
            if (variableMap.IsPrimaryKey) {
                foreach (var tableMap in TableMap.GetPhysicalHierarchy())
                    SetDataCore(tableMap.PrimaryKey.VariableMap, value);
            } else
                SetDataCore(variableMap, value);
        }

        protected abstract void SetDataCore (DirectVariableMap variableMap, object value);

        internal override IEnumerable<ChangeOperation> GetReferences (Dictionary<TableMap, Dictionary<object, ChangeOperation>> operationsByTableByKey) {
            var references = TableMap.VariableMaps.FindAll<ReferenceVariableMap>(VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            foreach (var reference in references) {
                var foreignKey = GetData(reference);
                if (foreignKey == null || foreignKey == DBNull.Value)
                    continue;

                if (operationsByTableByKey.ContainsKey(reference.ReferencedTableMap)) {
                    var operationsByKey = operationsByTableByKey[reference.ReferencedTableMap];
                    if (operationsByKey.ContainsKey(foreignKey))
                        yield return operationsByKey[foreignKey];
                }
            }
        }
        #endregion

        #region Committing
        public override void PreCommit () {
            if (IsPreCommitted)
                return;

            // Setup computed variables.
            var variableMaps = TableMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            var computedVariableMaps = variableMaps.Where(v => v.Computation != null && !ChangeSet.IsComputationIgnored(v.Computation, Instance));
            foreach (var variableMap in computedVariableMaps) {
                var value = variableMap.Computation.Compute(TableMap);
                AddRemap(variableMap, value);
            }

            IsPreCommitted = true;
        }

        internal override void PostCommit () {
            if (IsPostCommitted || Instance == null)
                return;

            foreach (var item in RemapData) {
                // Remap information to the instance.
                var composite = new CompositeVariableInfo(item.Item1);
                composite.InstantiateNullComponents = true;
                composite.SetValue(Instance, item.Item2);

                // Remap information to the data store in case it's needed
                // later.  The reason the data is potentially set twice is
                // because the remap information may come from another server,
                // so this is the only place we're guarunteed to have all the
                // remap information.
                var variableMap = TableMap.VariableMaps.FindDirect(item.Item1.Last().Name, VariableStructure.BaseContainers | VariableStructure.WithinComponents);
                SetData(variableMap, item.Item2);

                // Remap primary key.
                if (variableMap.IsPrimaryKey)
                    Key = item.Item2;
            }

            IsPostCommitted = true;
        }
        #endregion

        #region Remapping
        internal void AddRemap (DirectVariableMap variableMap, object value) {
            if (variableMap.Path.Count == 0)
                RemapData.Add(new Tuple<VariableInfo[], object>(new VariableInfo[] { variableMap.Variable }, value));
            else {
                var path = new Stack<VariableInfo>(variableMap.Path.Reverse());
                path.Push(variableMap.Variable);
                RemapData.Add(new Tuple<VariableInfo[], object>(path.Reverse().ToArray(), value));
            }

            SetData(variableMap, value);
        }
        #endregion

        #region Accessors
        public bool IsPreCommitted { get; private set; }
        public bool IsPostCommitted { get; private set; }

        /// <summary>
        /// Gets or sets the data to be remapped onto the changed object if the commit succeeds.
        /// </summary>
        public List<Tuple<VariableInfo[], object>> RemapData { get; protected set; }

        public Dictionary<TableMap, LightRow> Rows { get; private set; }
        #endregion
    }

    /// <summary>
    /// Records information needed to create an object.
    /// </summary>
    public class InsertChangeOperation : DataChangeOperation {
        #region Command Generation
        internal override SqlGenerator CreateCommandGenerator () {
            return new InsertSqlGenerator(TableMap);
        }

        protected override void FillCommandData (IDbCommand command, SqlFormatter formatter) {
            foreach (var currentTableMap in TableMap.GetPhysicalHierarchy()) {
                foreach (var variableMap in currentTableMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents)) {
                    if (variableMap.IsDatabaseGenerated)
                        continue;

                    var parameterName = formatter.FormatParameterName(variableMap.IsPrimaryKey ? "N__PrimaryKey__" : variableMap.ColumnName);
                    var parameter = (IDbDataParameter) command.Parameters[parameterName];
                    parameter.Value = formatter.FormatValue(GetData(variableMap)) ?? DBNull.Value;
                }
            }
        }

        protected override string GetStoredCommandKeyCore () {
            return "insert_" + TableMap.OriginTypeMap.Type.FullName;
        }

        public override SqlExpression GenerateSql () {
            var generator = new InsertSqlGenerator(TableMap);
            var sql = (SqlCompoundExpression) generator.Generate();
            var result = new SqlCompoundExpression();
            var variableMaps = TableMap.GetPhysicalHierarchy()
                .SelectMany(tm => tm.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents))
                .Where(dvm => !dvm.IsDatabaseGenerated);

            foreach (var insert in sql.Expressions.OfType<SqlInsert>()) {
                foreach (var variableMap in variableMaps) {
                    var parameterName = (variableMap.IsPrimaryKey ? "N__PrimaryKey__" : variableMap.ColumnName);

                    for (var i = 0; i < insert.Values.Count; ++i) {
                        var parameter = insert.Values[i] as SqlParameter;

                        if (parameter == null)
                            continue;

                        if (parameterName == parameter.Name) {
                            insert.Values[i] = new SqlLiteralExpression(GetData(variableMap) ?? DBNull.Value);
                            break;
                        }
                    }
                }
            }

            result.Expressions.AddRange(sql.Expressions.OfType<SqlInsert>());
            return result;
        }
        #endregion

        #region Remapping
        internal void ProcessReturnedValue (IDataReader reader) {
            if (!reader.Read())
                throw new OrmException(LS.T("Reader that was supposed to return a value did not."));

            do {
                for (var i = 0; i < reader.FieldCount; i++) {
                    var field = reader.GetValue(i);
                    var name = reader.GetName(i);

                    DirectVariableMap variableMap;

                    if (string.Compare(name, "N__PrimaryKey__", true) == 0) {
                        if (field == null)
                            continue;

                        variableMap = TableMap.PrimaryKey.VariableMap;
                    } else {
                        variableMap = TableMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.BaseContainers).FirstOrDefault(v => v.ColumnName == name);
                        if (variableMap == null)
                            continue;
                    }

                    var convertedValue = Convert.ChangeType(field, variableMap.PhysicalVariableType);
                    AddRemap(variableMap, convertedValue);
                }
            } while (reader.Read());
        }
        #endregion

        #region Data
        protected override void SetDataCore (DirectVariableMap variableMap, object value) {
            var row = Rows[variableMap.PhysicalTableMap];
            row[variableMap.ColumnName] = value;
        }
        #endregion
    }

    /// <summary>
    /// Records information needed to change an object.
    /// </summary>
    public class UpdateChangeOperation : DataChangeOperation {
        private object _previousTimeStamp;

        #region Constructors
        public UpdateChangeOperation () {
            UnchangedColumns = new HashSet<DirectVariableMap>();
        }
        #endregion

        #region Command Generation
        internal override SqlGenerator CreateCommandGenerator () {
            return new UpdateSqlGenerator(this);
        }

        protected override void FillCommandData (IDbCommand command, SqlFormatter formatter) {
            var variableMaps = TableMap.GetUpdateHierarchy().SelectMany(t => t.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents))
                .Where(v => !v.IsPrimaryKey && !v.IsDatabaseGenerated && HasData(v));

            foreach (var variableMap in variableMaps) {
                var parameter = (IDbDataParameter) command.Parameters[formatter.FormatParameterName(variableMap.ColumnName)];
                parameter.Value = formatter.FormatValue(GetData(variableMap)) ?? DBNull.Value;
            }

            if (_previousTimeStamp != null) {
                var timeStampParameter = (IDbDataParameter) command.Parameters[formatter.FormatParameterName("N__PreviousTimeStamp__")];
                timeStampParameter.Value = formatter.FormatValue(_previousTimeStamp);
            }

            var primaryKeyParameter = (IDbDataParameter) command.Parameters[formatter.FormatParameterName("N__PrimaryKey__")];
            primaryKeyParameter.Value = formatter.FormatValue(Key);
        }

        protected override string GetStoredCommandKeyCore () {
            // Do not return a key when there are unchanged columns, as there
            // is little benefit to storing these updates.  May changes this
            // later and incorporate the columns into the key, however.
            if (this.UnchangedColumns.Count > 0)
                return null;

            return "update_" + TableMap.OriginTypeMap.Type.FullName;
        }

        public override SqlExpression GenerateSql () {
            return new UpdateSqlGenerator(this).Generate();
        }

        private SqlQueryExpression GetSqlValue (DirectVariableMap variableMap, object value) {
            SqlLiteralExpression literal;

            // fixme: delete future section?  may not be necessary for literals
            if (value is IFuture)
                literal = new SqlLiteralExpression(((IFuture) value).Resolve());
            else
                literal = new SqlLiteralExpression(value);

            if (variableMap is PrimitiveVariableMap)
                literal.DbType = variableMap.DbType.Value;

            return literal;
        }
        #endregion

        #region Commits
        public override void PreCommit () {
            if (IsPreCommitted)
                return;

            // Record timestamp before it's overwritten, if applicable.
            // But only if the timestamp is not ignored.
            if (TableMap.TimeStampVariableMap != null && !ChangeSet.IsComputationIgnored(TableMap.TimeStampVariableMap.Computation, TableMap.OriginTypeMap.Type))
                _previousTimeStamp = GetData(TableMap.TimeStampVariableMap);

            base.PreCommit();
        }
        #endregion

        #region Queries
        internal override bool HasData (DirectVariableMap variableMap) {
            if (UnchangedColumns.Contains(variableMap))
                return false;

            if (!Rows.ContainsKey(variableMap.PhysicalTableMap))
                throw new ArgumentException(LS.T("Variable map {0}.{1} has no row information for operation on '{2}'", variableMap.PhysicalTableMap, variableMap, Instance), "variableMap");

            var row = Rows[variableMap.PhysicalTableMap];
            if (row.GetColumnError(variableMap.ColumnName) != null)
                return false;

            return true;
        }
        #endregion

        #region Data
        protected override bool FillChangedColumns (LightRow changeRow, LightRow currentRow, TableMap tableMap) {
            foreach (var variableMap in tableMap.VariableMaps.FindAllDirectsAndComponents())
                variableMap.StoreValueOnRow(Instance, changeRow, ChangeSet);

            if (currentRow == null)
                return true;

            var changed = false;
            foreach (var column in currentRow.Table.ColumnsInternal) {
                var variableMap = (DirectVariableMap)column.Tag;

                if (variableMap.IsDatabaseGenerated)
                    continue;

                if (variableMap.Computation != null && !ChangeSet.IsComputationIgnored(variableMap.Computation, Instance))
                    continue;

                var currentValue = currentRow[column];
                var changeValue = changeRow[column];

                // Check when both are null.
                if (currentValue == null && changeValue == null) {
                    UnchangedColumns.Add(variableMap);
                    continue;
                }

                // Short circuit equality checks when either are null.
                if (currentValue == null || changeValue == null) {
                    changed = true;
                    continue;
                }

                // Check value equality.
                if (variableMap.PhysicalVariableType.IsArray) {
                    var currentValueArray = ((Array) currentValue).Cast<object>();
                    var changeValueArray = ((Array) changeValue).Cast<object>();
                    if (changeValueArray.SequenceEqual(currentValueArray)) {
                        UnchangedColumns.Add(variableMap);
                        continue;
                    }
                } else if (Equals(changeValue, currentValue)) {
                    UnchangedColumns.Add(variableMap);
                    continue;
                }

                changed = true;
            }

            return changed;
        }

        protected override void SetDataCore (DirectVariableMap variableMap, object value) {
            var tableMap = variableMap.PhysicalTableMap;

            LightRow row;
            if (!Rows.ContainsKey(tableMap)) {
                var table = GetTableToFill(ChangeSet.Data, tableMap);
                row = GetRowToFill(table, tableMap);

                if (Key != null)
                    row[variableMap.PhysicalTableMap.PrimaryKey.VariableMap.ColumnName] = Key;

                UnchangedColumns.Remove(variableMap);

                Rows[tableMap] = row;
            } else
                row = Rows[tableMap];

            row[variableMap.ColumnName] = value;
        }
        #endregion

        #region Accessors
        public HashSet<DirectVariableMap> UnchangedColumns { get; private set; }
        #endregion
    }

    public class IgnoredComputation
    {
        public IgnoredComputation()
        {
            InstanceTypes = new List<Type>();
        }

        public bool IsForAllTypes { get; set; }
        public List<Type> InstanceTypes { get; set; }
    }
}
