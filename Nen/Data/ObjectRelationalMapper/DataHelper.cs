﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Provides data utilities that make use of mapper metadata.
    /// </summary>
    public static class DataHelper {
        /// <summary>
        /// Instantiates a derived instance of a discriminated type.
        /// </summary>
        /// <typeparam name="T">The known base discriminated type.</typeparam>
        /// <param name="config">The configuration holding discriminated type information.</param>
        /// <param name="discriminatorValue">The value of the derived type.</param>
        /// <returns>An instance of the derived type with the given discriminator.</returns>
        public static T InstantiateDiscriminatedType<T> (MapConfiguration config, object discriminatorValue) where T : class {
            var tableMap = config.TableMaps[typeof(T)];
            var subTableMap = tableMap.GetDiscriminatedSubTableMap(discriminatorValue);
            return (T) Activator.CreateInstance(subTableMap.OriginTypeMap.Type);
        }
    }
}
