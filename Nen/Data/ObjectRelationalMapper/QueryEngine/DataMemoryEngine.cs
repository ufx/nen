﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper.QueryModel;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine {
    internal class DataMemoryEngine {
        #region Constructor
        public DataMemoryEngine (QueryExpression query, CachingDataSet data) {
            Data = data;
            Query = query;
        }
        #endregion

        #region Query Execution
        public DataQueryResults Execute () {
            var analyzer = new MemoryQueryAnalyzer(Query, Data);
            return analyzer.Analyze();
        }
        #endregion

        #region Accessors
        public CachingDataSet Data { get; private set; }
        public QueryExpression Query { get; private set; }
        #endregion
    }
}
