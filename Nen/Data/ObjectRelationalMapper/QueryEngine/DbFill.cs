using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryEngine.SqlGenerators;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Reflection;
using Nen.Data.SqlModel;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine {
    internal abstract class DbFill {
        private IQuerySource _source;

        #region Constructor
        protected DbFill (DataQueryEngine engine, IQuerySource source) {
            Engine = engine;
            _source = source;
        }
        #endregion

        #region Execution
        public virtual bool Combine (DbFill fill) {
            return false;
        }

        public virtual bool Prepare () {
            return true;
        }

        public void Execute (IDbConnection connection, IDbTransaction tran, Database database) {
            Engine.Query.Statistics.NumberOfDatabaseQueries++;

            // Planning stage
            var queryBeginTime = DateTime.Now;

            var query = GetQuery();
            query.Source = _source;

            var plan = QueryAnalyzer.Analyze(query);

            plan.WriteStatistics(Engine.Query.Statistics);

            // SQL generation stage
            var sqlExpression = plan.GenerateSql();

            var formatter = database.CreateFormatter();
            using (var command = formatter.Generate(sqlExpression)) {
                command.Connection = connection;
                command.Transaction = tran;

                if (query.Context.CommandTimeout != null)
                    command.CommandTimeout = query.Context.CommandTimeout.Value;

                var log = Engine.Query.Context.Log;
                if (log != null)
                {
                    if (_source == null)
                        log.WriteLine("-- Hydrating for {0}", GetType().Name);
                    else
                        log.WriteLine("-- Hydrating from {0} for {1}", _source, GetType().Name);

                    log.WriteLine(formatter.PrintCommand(command));
                    log.WriteLine("GO");
                }

                Engine.Query.Statistics.PlanningTime += DateTime.Now - queryBeginTime;

                // Database stage
                var databaseBeginTime = DateTime.Now;

                using (var reader = database.ExecuteDataReader(command)) {
                    Engine.Query.Statistics.DatabaseTime += DateTime.Now - databaseBeginTime;

                    // Remove the query plan now the query is complete.  This will
                    // ensure later references to the query use a new plan context.
                    query.SelectQueryPlan = null;

                    // Merge stage
                    var mergeBeginTime = DateTime.Now;

                    Merge(plan, reader);

                    Engine.Query.Statistics.MergeTime += DateTime.Now - mergeBeginTime;
                }
            }
        }

        protected abstract QueryExpression GetQuery ();

        protected virtual void Merge (QueryPlan plan, IDataReader reader) {
            plan.Merge(reader, Engine);
        }
        #endregion

        #region Accessors
        protected DataQueryEngine Engine { get; private set; }
        #endregion
    }

    internal abstract class KeySetDbFill : DbFill {
        #region Constructors
        protected KeySetDbFill (DataQueryEngine engine, IQuerySource source, DirectVariableMap keyVariableMap, object key)
            : base(engine, source) {

            if (key == null)
                throw new ArgumentNullException("key");

            Keys = new HashSet<object>();
            Keys.Add(key);

            KeyVariableMap = keyVariableMap;
        }
        #endregion

        #region Fill Execution
        public override bool Combine (DbFill fill) {
            if (MaximumKeysReached)
                return false;

            if (fill.GetType() != GetType())
                return false;

            var keySetFill = (KeySetDbFill) fill;

            if (keySetFill.KeyVariableMap != KeyVariableMap)
                return false;

            Keys.AddRange(keySetFill.Keys);
            return true;
        }

        public override bool Prepare () {
            var remainingKeys = new HashSet<object>(Keys);

            var table = Engine.Results.GetTable(KeyVariableMap.PhysicalTableMap);
            if (KeyVariableMap == KeyVariableMap.PhysicalTableMap.PrimaryKey.VariableMap) {
                // Remove previously filled data from the set of keys to fill.
                remainingKeys.RemoveRange(Keys.Where(k => table.FindRowByPrimaryKey(k) != null));

                // Also remove previosuly mapped data.
                var context = Engine.Query.Context;
                var tableMap = KeyVariableMap.PhysicalTableMap;
                remainingKeys.RemoveRange(Keys.Where(k => context.GetStoredObject(tableMap, k) != null));
            } else {
                var selectedKeys = table.Rows.Cast<DataRow>().Select(r => r[KeyVariableMap.ColumnName]);
                remainingKeys.RemoveRange(selectedKeys);
            }

            Engine.Query.Statistics.NumberOfDeferredHits += (Keys.Count - remainingKeys.Count);
            Engine.Query.Statistics.NumberOfDeferredMisses += remainingKeys.Count;

            Keys = remainingKeys;

            // Execute the fill only if any keys remain.
            return Keys.Count > 0;
        }

        protected override QueryExpression GetQuery () {
            var query = KeyVariableMap.CreateKeyQuery(Keys.ToArray(), Engine.Query.Context);
            query.Advice.Add(new AdviceExpression(AdviceRequests.DeferJoin, new VariableExpression(KeyVariableMap.Variable, query)));
            return query;
        }
        #endregion

        #region Accessors
        protected HashSet<object> Keys { get; set; }
        protected DirectVariableMap KeyVariableMap { get; set; }

        protected bool MaximumKeysReached {
            get { return Keys.Count > 1000; }
        }
        #endregion

        #region Object Members
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "KeySetDbFill{{{0}, {1}}}", KeyVariableMap, Keys.Count);
        }
        #endregion
    }

    internal class OriginDbFill : DbFill {
        private QueryExpression _query;

        #region Constructors
        public OriginDbFill (DataQueryEngine engine, QueryExpression query)
            : base(engine, null) {
            _query = query;
        }
        #endregion

        #region Fill Execution
        protected override QueryExpression GetQuery () {
            return _query;
        }

        protected override void Merge (QueryPlan plan, IDataReader reader) {
            var keys = plan.Merge(reader, Engine);
            var queryResults = (SequenceDataQueryResults) Engine.Results;
            queryResults.FillResults.Add(new OriginDbFillResult(keys));
        }
        #endregion

        #region Object Members
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "OriginDbFill{{{0}}}", _query);
        }
        #endregion
    }

    internal class ScalarOriginDbFill : OriginDbFill {
        #region Constructors
        public ScalarOriginDbFill (DataQueryEngine engine, QueryExpression query)
            : base(engine, query) {
        }
        #endregion

        #region Fill Execution
        protected override void Merge (QueryPlan plan, IDataReader reader) {
            var result = plan.GetScalarResult(reader);
            var queryResults = (ScalarDataQueryResults) Engine.Results;
            queryResults.FillResult = new ScalarDbFillResult(result);
        }
        #endregion
    }

    internal class DeferredDbFill : KeySetDbFill {
        #region Constructors
        public DeferredDbFill (DataQueryEngine engine, IQuerySource source, DirectVariableMap keyVariableMap, object key)
            : base(engine, source, keyVariableMap, key) {
        }
        #endregion

        #region Object Members
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "DeferredDbFill{{{0}, {1}}}", KeyVariableMap, Keys.Count);
        }
        #endregion
    }

    internal class CollectionDbFill : DbFill {
        private HashSet<QueryExpression> _sourceQueries = new HashSet<QueryExpression>();
        private CollectionVariableMap _variableMap;
        private object[] _keys;

        #region Constructors
        public CollectionDbFill (DataQueryEngine engine, IQuerySource source, CollectionVariableMap variableMap, QueryExpression sourceQuery)
            : base(engine, source) {
            _sourceQueries.Add(sourceQuery);
            _variableMap = variableMap;
        }
        #endregion

        #region Fill Execution
        public override bool Combine (DbFill fill) {
            var collectionFill = fill as CollectionDbFill;
            if (collectionFill == null)
                return false;

            if (collectionFill._variableMap != _variableMap)
                return false;

            _sourceQueries.AddRange(collectionFill._sourceQueries);
            return true;
        }

        public override bool Prepare () {
            // todo: Record which queries have already been executed in their
            // FillResults, so they can be filtered.

            // TODO: Some of these keys may have been filled already.  Remove them from the fetch.

            // Nothing to do if there are no source queries.
            if (_sourceQueries.Count == 0)
                return false;

            // Find all relevant keys for this collection.
            var tableMap = _variableMap.PhysicalTableMap.IsOptimizedOutForReads ? _variableMap.PhysicalTableMap.BasePhysicalTableMap : _variableMap.PhysicalTableMap;
            var table = Engine.Results.Data[tableMap.TableName];
            var keysEnumerable = table.GetPrimaryKeys();
            if (keysEnumerable == null)
                throw new InvalidOperationException("Attempt to fill collection to empty table.");

            _keys = keysEnumerable.ToArray();
            return _keys.Length > 0;
        }

        protected override QueryExpression GetQuery () {
            // Create the query.
            var query = _variableMap.CreateCollectionQuery(_sourceQueries.First().Context);
            var left = new VariableExpression(_variableMap.KeyVariableMap.Variable, query);

            OperatorExpression criteria = null;
            if (_keys.Length > 1000)
            {
                // FIXME: When the source query is also too large, this generates too many parameters.

                // When the number of keys is too large, fall back to where in (source query)
                foreach (var sourceQuery in _sourceQueries)
                {
                    var expr = new BinaryOperatorExpression(left, BinaryOperatorType.In, sourceQuery);
                    criteria = BinaryOperatorExpression.Join(criteria, BinaryOperatorType.Or, expr);
                }
            }
            else
            {
                var right = new LiteralExpression(_keys);
                criteria = new BinaryOperatorExpression(left, BinaryOperatorType.In, right);
            }

            query.WhereCriteria = BinaryOperatorExpression.Join(query.WhereCriteria, BinaryOperatorType.And, criteria);

            return query;
        }

        protected override void Merge (QueryPlan plan, IDataReader reader) {
            var keys = plan.Merge(reader, Engine);
            var queryResults = (SequenceDataQueryResults) Engine.Results;
            var result = queryResults.FillResults.OfType<CollectionDbFillResult>().FirstOrDefault(r => r.CollectionVariableMap == _variableMap);
            if (result != null)
                result.Keys.AddRange(keys);
            else
                queryResults.FillResults.Add(new CollectionDbFillResult(keys, _variableMap));
        }
        #endregion

        #region Object Members
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "CollectionDbFill{{{0}, {1}}}", _variableMap, _sourceQueries.Count);
        }
        #endregion
    }

    internal class DiscriminatedDbFill : KeySetDbFill {
        #region Constructors
        public DiscriminatedDbFill (DataQueryEngine engine, IQuerySource source, TableMap tableMap, DirectVariableMap keyVariableMap, object key, object discriminatorValue)
            : base(engine, source, keyVariableMap, key) {
            TableMap = tableMap;
        }
        #endregion

        #region Fill Execution
        public override bool Combine (DbFill fill) {
            if (MaximumKeysReached)
                return false;

            if (fill.GetType() != GetType())
                return false;

            var discriminatedDbFill = (DiscriminatedDbFill) fill;

            if (discriminatedDbFill.KeyVariableMap != KeyVariableMap || discriminatedDbFill.TableMap != TableMap)
                return false;

            Keys.AddRange(discriminatedDbFill.Keys);
            return true;
        }

        protected override QueryExpression GetQuery () {
            var query = base.GetQuery();
            query.Subject = new TableExpression(TableMap);
            query.IsPossiblyAmbiguous = false;
            return query;
        }
        #endregion

        #region Accessors
        public TableMap TableMap { get; set; }
        #endregion

        #region Object Members
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "DiscriminatedDbFill{{{0}, {1}, {2}}}", TableMap, KeyVariableMap, Keys.Count);
        }
        #endregion
    }
}
