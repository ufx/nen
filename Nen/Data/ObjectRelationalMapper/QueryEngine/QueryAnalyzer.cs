using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;
using Nen.Reflection;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine {
    internal static class QueryAnalyzer {
        internal static QueryPlan Analyze (QueryExpression query) {
            QueryPlan plan;
            if (query.Subject is TableExpression) {
                var analyzer = new SelectQueryAnalyzer(query);
                plan = analyzer.Analyze();
            } else if (query.Subject is ProcedureExpression) {
                var analyzer = new ProcedureQueryAnalyzer(query);
                plan = analyzer.Analyze();
            } else
                throw new ArgumentException(LS.T("Unexpected subject type '{0}'", query.Subject), "query");

            plan.Optimize();
            return plan;
        }
    }

    internal class SelectQueryAnalyzer : IOrmExpressionVisitor<Column> {
        private int _columnCounter;
        private int _joinCounter;
        private SelectQueryPlan _plan;
        private QueryExpression _query;

        #region Constructors
        public SelectQueryAnalyzer (QueryExpression query) {
            _query = query;
        }
        #endregion

        #region Query Analysis
        public SelectQueryPlan Analyze () {
            // Reset the state.
            _plan = _query.SelectQueryPlan = new SelectQueryPlan(_query);
            _columnCounter = 0;
            _joinCounter = 0;

            // Some simple constant values.
            _plan.Limit = _query.Limit;
            _plan.Offset = _query.Offset;

            // Analyze the origin and everything should flow from there.
            var queryTableMap = _query.GetResultTableMap();
            _plan.OriginTuple = AnalyzeTable(queryTableMap, queryTableMap, null, _query.IsPossiblyAmbiguous);
            _plan.OriginTuple.Alias = _plan.OriginTuple.TableName;

            // When the origin tuple is ambiguous or optimized out for reads,
            // constrain the query by the list of possible types in the result.
            // Constraints are unnecessary when the origin table map is the root.
            if ((_plan.OriginTuple.IsAmbiguous || queryTableMap.IsOptimizedOutForReads) && queryTableMap.BaseTableMap != null) {
                //Only not constrain if the queried table has a discriminator.
                if (queryTableMap.DiscriminatorVariableMap != null) {
                    var subDiscriminatorValues = queryTableMap.SubTableMaps.Union(queryTableMap).Select(t => t.DiscriminatorValue).ToArray();
                    var right = new LiteralExpression(subDiscriminatorValues);
                    var left = new VariableExpression(queryTableMap.DiscriminatorVariableMap.Variable, _query);
                    _plan.ExtraCriteria = new BinaryOperatorExpression(left, BinaryOperatorType.In, right);
                    _plan.ExtraCriteria.Accept(this);
                }
            }

            // Visit the query structure.
            if (_query.Projection.Items.Count > 0) {
                var nonVariableProjection = _query.Projection.Items.Where(o => !(o is VariableExpression));
                foreach (var projection in nonVariableProjection)
                    projection.Accept(this);
            }

            _query.Accept(this);

            // Order joins so that child joins appear first.
            var orderedJoins = _plan.Joins.OrderByChildrenFirst((j1, j2) => j1.OnLeft.Tuple == j2.Tuple || j1.OnRight.Tuple == j2.Tuple);
            _plan.Joins = orderedJoins.ToList();

            return _plan;
        }
        #endregion

        #region Table Analysis
        private Tuple AnalyzeTable (TableMap tableMap, TableMap originTableMap, Column sourceReference, bool analysisIsPossiblyAmbiguous) {
            // Tables with sub tables are ambiguous, as we don't know which
            // specific type is being requested. 
            var analysisIsAmbiguous = analysisIsPossiblyAmbiguous && tableMap.IsAmbiguous;

            // Redirect references to the base table when told so or when the
            // table offers no extra value.
            if (tableMap.IsOptimizedOutForReads)
                return AnalyzeTable(tableMap.BasePhysicalTableMap, originTableMap, sourceReference, analysisIsAmbiguous);

            // Read only the discriminators that match the query to find the
            // actual type.
            if (analysisIsAmbiguous) {
                var ambiguousTuple = CreateTuple(tableMap, originTableMap, sourceReference);
                ambiguousTuple.IsAmbiguous = true;

                var currentAmbiguousTuple = ambiguousTuple;
                foreach (var baseTableMap in tableMap.GetBaseUpdateHierarchy()) {
                    currentAmbiguousTuple.BaseTuple = CreateTuple(baseTableMap, baseTableMap, sourceReference);

                    currentAmbiguousTuple.PrimaryKey.IsQueried = false;
                    currentAmbiguousTuple.PrimaryKey.IsMerged = false;

                    var join = AnalyzeInheritance(currentAmbiguousTuple);
                    join.IsMerged = false;

                    currentAmbiguousTuple = currentAmbiguousTuple.BaseTuple;

                    if (tableMap.DiscriminatorVariableMap.PhysicalTableMap == baseTableMap)
                        ambiguousTuple.Discriminator = currentAmbiguousTuple.CreateColumn(tableMap.DiscriminatorVariableMap, ++_columnCounter);
                }

                if (ambiguousTuple.BaseTuple == null)
                    ambiguousTuple.Discriminator = ambiguousTuple.CreateColumn(tableMap.DiscriminatorVariableMap, ++_columnCounter);

                return ambiguousTuple;
            }

            var tuple = CreateTuple(tableMap, originTableMap, sourceReference);

            // Analyze the inheritance hierarchy of this table map.
            if (tableMap.UnderlyingViewTableMap == null) {
                var basePhysicalTableMap = tableMap.BasePhysicalTableMap;
                if (basePhysicalTableMap != null) {
                    tuple.BaseTuple = AnalyzeTable(basePhysicalTableMap, basePhysicalTableMap, sourceReference, false);

                    // Tuples with a base will inherit the base primary key as well,
                    // but the sub key must still be accessible.
                    tuple.PrimaryKey.IsQueried = false;
                    tuple.PrimaryKey.IsMerged = false;

                    // Refer the sub-table's primary key tuple to the base tuple, 
                    // since its column will not be queried but will be accessed at
                    // merge time. This is needed when the base primary key column name
                    // differs from the sub-table primary key column name.
                    tuple.PrimaryKey.DestinationReference = tuple.BaseTuple.PrimaryKey;

                    AnalyzeInheritance(tuple);
                }
            } else {
                // Shortcut to add all inherited joins from this view, ignoring their PK columns.
                Tuple currentTuple = tuple;
                for (var currentTableMap = tableMap.UnderlyingViewTableMap.BasePhysicalTableMap; currentTableMap != null; currentTableMap = currentTableMap.BasePhysicalTableMap) {
                    currentTuple.BaseTuple = CreateTuple(currentTableMap, tableMap, sourceReference);
                    currentTuple.PrimaryKey.IsQueried = false;
                    currentTuple.PrimaryKey.IsMerged = false;
                    currentTuple.PrimaryKey.DestinationReference = currentTuple.BaseTuple.PrimaryKey;
                    AnalyzeInheritance(currentTuple);
                    currentTuple = currentTuple.BaseTuple;
                }
            }

            AnalyzeColumns(tuple);

            return tuple;
        }
        #endregion

        #region Column Analysis
        private void AnalyzeColumns (Tuple tuple) {
            var variableMaps = tuple.TableMap.VariableMaps.FindAllDirectsAndSubordinates(VariableStructure.WithinComponents);
            foreach (var variableMap in variableMaps)
                AnalyzeColumn(tuple, variableMap);
        }

        private void AnalyzeColumn (Tuple tuple, VariableMap variableMap) {
            // Skip primary keys, as they are already in the tuple.
            if (tuple.TableMap.PrimaryKey.VariableMap == variableMap)
                return;

            // Don't create columns for ignored variables.
            var advice = _plan.GetAdvice(variableMap, tuple);
            if (advice == AdviceRequests.Ignore)
                return;

            // Don't create columns for lazy primitives.
            // The DeferJoin check is a stupid hack, to be destroyed later.
            if ((advice != AdviceRequests.Include && advice != AdviceRequests.DeferJoin) && variableMap is LazyPrimitiveVariableMap)
                return;

            // Proceed
            bool circular = tuple.SourceReference != null && variableMap.IsCircularReference(tuple.SourceReference.VariableMap);
            if (variableMap is SubordinateVariableMap)
                AnalyzeSubordinateColumn(tuple, (SubordinateVariableMap) variableMap, advice, circular);
            else
                AnalyzeDirectColumn(tuple, (DirectVariableMap) variableMap, advice, circular);
        }

        private void AnalyzeSubordinateColumn (Tuple tuple, SubordinateVariableMap variableMap, AdviceRequests advice, bool circular) {
            var column = tuple.CreateColumn(variableMap, ++_columnCounter);
            column.IsMerged = false;
            column.IsQueried = false;

            if (!circular) {
                if (advice == AdviceRequests.DeferJoin) {
                    column.IsDeferred = true;
                    column.IsMerged = true;
                } else
                    AnalyzeSubordinateReference(column, true);
            }
        }

        private void AnalyzeDirectColumn (Tuple tuple, DirectVariableMap variableMap, AdviceRequests advice, bool circular) {
            Column column;
            if (!circular && advice == AdviceRequests.JoinOnly)
                column = AnalyzeJoinOnlyColumn(tuple, variableMap);
            else
                column = AnalyzeQueriedColumn(tuple, variableMap, advice, circular);

            // Analyze destination references.
            if (column != null && variableMap.FromExpression != null) {
                if (variableMap.FromExpression is VariableExpression)
                    AnalyzeVariableFromExpression((VariableExpression) variableMap.FromExpression, column, tuple);
                else if (variableMap.FromExpression is QueryExpression)
                    AnalyzeQueryFromExpression((QueryExpression) variableMap.FromExpression, column, tuple);
                else
                    throw new NotImplementedException(LS.T("Can't analyze variable from expression type '{0}'.", variableMap.FromExpression.GetType()));
            }
        }

        private void AnalyzeVariableFromExpression (VariableExpression fromExpression, Column column, Tuple tuple) {
            var destinationReference = FindEdgeColumn(fromExpression, _plan, tuple);
            if (destinationReference != column) {
                column.DestinationReference = destinationReference;
                column.IsQueried = false;
                column.IsMerged = true;
                destinationReference.IsQueried = true;
                destinationReference.IsMerged = false;
            }
        }

        private void AnalyzeQueryFromExpression (QueryExpression fromExpression, Column column, Tuple tuple) {
            // When the subject isn't a variable, simply pass the query
            // through to the column.

            if (!(fromExpression.Subject is VariableExpression)) {
                column.Expression = fromExpression;
                return;
            }

            // The subject varies, so construct the correct query to fill the
            // column.

            Tuple collectionTuple;
            var subjectEdgeVariableMap = FindEdgeVariableMap((VariableExpression) fromExpression.Subject, tuple, out collectionTuple);
            var collectionVariableMap = (CollectionVariableMap) subjectEdgeVariableMap;

            var query = collectionVariableMap.CreateCollectionQuery(_query.Context);
            query.Projection.Items.AddRange(fromExpression.Projection.Items);
            query.OrderBy.Clear();

            // Left side comes from the new query.
            var left = new VariableExpression(collectionVariableMap.KeyVariableMap.Variable, query);
            // And right side comes from this analyzer's query.  Subtle!
            var right = new VariableExpression(tuple.PrimaryKey.VariableMap.Variable, _query);
            var criteria = new BinaryOperatorExpression(left, BinaryOperatorType.Equal, right);
            query.WhereCriteria = BinaryOperatorExpression.Join(query.WhereCriteria, BinaryOperatorType.And, criteria);
            query.WhereCriteria = BinaryOperatorExpression.Join(query.WhereCriteria, BinaryOperatorType.And, fromExpression.WhereCriteria);

            column.Expression = query;
        }

        private Column AnalyzeQueriedColumn (Tuple tuple, DirectVariableMap variableMap, AdviceRequests advice, bool circular) {
            var column = tuple.CreateColumn(variableMap, ++_columnCounter);
            var deferredColumn = circular || (advice == AdviceRequests.DeferJoin);

            // Short circuit columns which aren't loaded or aren't references.
            if (advice == AdviceRequests.JoinOnIncludeOnly || !(variableMap is ReferenceVariableMap))
                return column;

            if (deferredColumn)
                column.IsDeferred = true;
            else
                AnalyzeReference(column, true);

            return column;
        }

        private Column AnalyzeJoinOnlyColumn (Tuple tuple, DirectVariableMap variableMap) {
            // Only references are created for join only columns, even the
            // lazy ones.  Derivatives of unqueried columns aren't queried.
            if (!(variableMap is ReferenceVariableMap))
                return null;

            var column = tuple.CreateColumn(variableMap, ++_columnCounter);
            column.IsMerged = false;
            column.IsQueried = false;

            AnalyzeReference(column, true);

            return column;
        }
        #endregion

        #region Relationship Analysis
        private Join AnalyzeInheritance (Tuple tuple) {
            var join = CreateJoin();

            join.SourceReference = tuple.SourceReference;
            join.Tuple = tuple.BaseTuple;
            join.OnLeft = tuple.PrimaryKey;
            join.OnRight = tuple.BaseTuple.PrimaryKey;

            tuple.BaseTuple.Alias = join.Alias;

            return join;
        }

        private Join AnalyzeReference (Column reference, bool recursivelyAnalyze) {
            var referenceVariableMap = (ReferenceVariableMap) reference.VariableMap;

            var join = CreateJoin();
            join.SourceReference = reference;
            join.OnLeft = reference;

            if (recursivelyAnalyze) {
                join.Tuple = AnalyzeTable(referenceVariableMap.ReferencedTableMap, referenceVariableMap.ReferencedTableMap, reference, true);
                join.Tuple.Alias = join.Alias;
                join.OnRight = join.Tuple.PrimaryKey;
            } else {
                join.IsMerged = false;

                var tuple = CreateTuple(referenceVariableMap.ReferencedTableMap, referenceVariableMap.ReferencedTableMap, reference);
                tuple.PrimaryKey.IsQueried = false;
                tuple.PrimaryKey.IsMerged = false;

                join.Tuple = tuple;
                join.Tuple.Alias = join.Alias;
                join.OnRight = tuple.PrimaryKey;

                // Join inherited tables.
                foreach (var tableMap in referenceVariableMap.ReferencedTableMap.GetBasePhysicalHierarchy()) {
                    tuple.BaseTuple = CreateTuple(tableMap, tableMap, reference);
                    tuple.BaseTuple.PrimaryKey.IsQueried = false;
                    tuple.BaseTuple.PrimaryKey.IsMerged = false;

                    var inheritedJoin = AnalyzeInheritance(tuple);
                    inheritedJoin.IsMerged = false;

                    tuple = tuple.BaseTuple;
                }
            }

            return join;
        }

        private Join AnalyzeSubordinateReference (Column subordinateReference, bool recursivelyAnalyze) {
            var subordinateVariableMap = (SubordinateVariableMap) subordinateReference.VariableMap;

            var join = CreateJoin();
            join.SourceReference = subordinateReference;
            join.OnLeft = subordinateReference.Tuple.PrimaryKey;

            if (recursivelyAnalyze)
                join.Tuple = AnalyzeTable(subordinateVariableMap.ReferencedTableMap, subordinateVariableMap.ReferencedTableMap, subordinateReference, true);
            else {
                join.IsMerged = false;

                var tuple = CreateTuple(subordinateVariableMap.ReferencedTableMap, subordinateVariableMap.ReferencedTableMap, subordinateReference);
                tuple.PrimaryKey.IsQueried = false;
                tuple.PrimaryKey.IsMerged = false;

                join.Tuple = tuple;

                // fixme: join inherited tables just like AnalyzeReference?
            }

            join.Tuple.Alias = join.Alias;
            join.OnRight = join.Tuple.FindOrCreateUnqueriedColumn(subordinateVariableMap.KeyVariableMap, ++_columnCounter);

            return join;
        }

        private Join FindJoin (Column referenceColumn) {
            return _plan.Joins.FirstOrDefault(j => j.OnLeft == referenceColumn || j.OnRight == referenceColumn);
        }

        private Join FindOrCreateNonMergedJoin (Column referenceColumn) {
            var join = FindJoin(referenceColumn);
            if (join == null) {
                if (referenceColumn.VariableMap is SubordinateVariableMap)
                    join = AnalyzeSubordinateReference(referenceColumn, false);
                else
                    join = AnalyzeReference(referenceColumn, false);
            }
            return join;
        }
        #endregion

        #region Expression Analysis
        #region Ignored / Invalid Expressions
        Column IOrmExpressionVisitor<Column>.Visit (LiteralExpression literal) {
            //return null; // Does nothing for now.
            var column = _plan.OriginTuple.CreateColumn(null, 0);
            column.IsMerged = false;
            column.IsQueried = false;
            column.Expression = literal;
            return column;
        }

        Column IOrmExpressionVisitor<Column>.Visit (AdviceExpression advice) {
            return null; // Does nothing;
        }

        Column IOrmExpressionVisitor<Column>.Visit (ProjectionExpression projection) {
            throw new NotImplementedException("Projection expressions must not be visited from the query analyzer.");
        }
        #endregion

        Column IOrmExpressionVisitor<Column>.Visit (QueryExpression query) {
            // Do not recursively visit QueryExpressions, as these are planned
            // as standalone entities.
            if (query != _query)
                return null;

            var subjectEdge = query.Subject.Accept(this);

            if (query.WhereCriteria != null)
                query.WhereCriteria.Accept(this);

            foreach (var order in query.OrderBy)
                order.Accept(this);

            return subjectEdge;
        }

        Column IOrmExpressionVisitor<Column>.Visit (OrderExpression order) {
            return order.Over.Accept(this);
        }

        Column IOrmExpressionVisitor<Column>.Visit (BinaryOperatorExpression binaryOperator) {
            binaryOperator.Left.Accept(this);
            binaryOperator.Right.Accept(this);

            return null;
        }

        Column IOrmExpressionVisitor<Column>.Visit (UnaryOperatorExpression unaryOperator) {
            return unaryOperator.Operand.Accept(this);
        }

        Column IOrmExpressionVisitor<Column>.Visit (ApplyExpression apply) {
            if (_plan.ExpressionEdgeColumns.ContainsKey(apply))
                return _plan.ExpressionEdgeColumns[apply];

            var tuple = _plan.OriginTuple;

            var column = tuple.CreateColumn(null, ++_columnCounter);
            column.Function = new Function();
            column.Function.Name = apply.Operator;
            column.Function.Arguments = apply.Arguments.Select(a => a.Accept(this)).ToArray();

            // If the function arguments are functions themselves, do not query them.
            foreach (var arg in column.Function.Arguments)
            {
                if (arg.Function != null)
                {
                    arg.IsQueried = false;
                    arg.IsMerged = false;
                }
            }

            // HACK: Do not query datediff or dateadd for now.
            if (column.Function.Name == "datediff" || column.Function.Name == "dateadd")
            {
                column.IsQueried = false;
                column.IsMerged = false;
            }

            _plan.ExpressionEdgeColumns[apply] = column;
            return column;
        }

        Column IOrmExpressionVisitor<Column>.Visit (VariableExpression variable) {
            var plan = variable.GetSelectQueryPlan() ?? _plan;
            return FindEdgeColumn(variable, plan, plan.OriginTuple);
        }

        private Column FindEdgeColumn (VariableExpression variable, SelectQueryPlan plan, Tuple originTuple) {
            if (plan.ExpressionEdgeColumns.ContainsKey(variable))
                return plan.ExpressionEdgeColumns[variable];

            Tuple edgeTuple;
            var edgeVariableMap = FindEdgeVariableMap(variable, originTuple, out edgeTuple);

            var column = edgeTuple.FindOrCreateUnqueriedColumn(edgeVariableMap, ++_columnCounter);
            column = column.DestinationReference ?? column;

            if (edgeVariableMap is SubordinateVariableMap) {
                // For subordinates the edge really needs to redirect to the
                // subordinate key.  Create a join and use that instead.
                var subordinateEdge = (SubordinateVariableMap) edgeVariableMap;
                var join = FindOrCreateNonMergedJoin(column);
                column = join.Tuple.FindOrCreateUnqueriedColumn(subordinateEdge.KeyVariableMap, ++_columnCounter);
            }

            plan.ExpressionEdgeColumns[variable] = column;
            return column;
        }

        private VariableMap FindEdgeVariableMap (VariableExpression variable, Tuple originTuple, out Tuple edgeTuple) {
            edgeTuple = originTuple;
            var variableMapParts = originTuple.TableMap.VariableMaps.FindAllParts(variable.Variable, VariableStructure.BaseContainers | VariableStructure.ViewContainers | VariableStructure.WithinComponents);
            var relevantVariableMaps = variableMapParts.Where(v => !(v is ComponentVariableMap)).ToArray();

            Column column = null;
            VariableMap lastVariableMap = null;

            foreach (var variableMap in relevantVariableMaps) {
                // Create a join on the last column walked.
                if (lastVariableMap != null) {
                    var join = FindOrCreateNonMergedJoin(column);
                    edgeTuple = join.Tuple;
                }

                if (variableMap == relevantVariableMaps.Last())
                    return variableMap;

                // This must be a pass-through variable map of some sort.
                // Establish a column for it.
                column = edgeTuple.FindOrCreateUnqueriedColumn(variableMap, ++_columnCounter);

                if (column.DestinationReference != null) {
                    column = column.DestinationReference;
                    edgeTuple = column.Tuple;
                }

                lastVariableMap = variableMap;
            }

            throw new InvalidOperationException("Should never get here.");
        }
        #endregion

        #region Analyzed Model Creation
        private Tuple CreateTuple (TableMap tableMap, TableMap originTableMap, Column sourceReference) {
            var tuple = new Tuple(_query);
            tuple.OriginTableMap = originTableMap;
            tuple.TableMap = tableMap;
            tuple.SourceReference = sourceReference;

            var actualTableMap = tableMap.UnderlyingViewTableMap == null ? tableMap : tableMap.UnderlyingViewTableMap;
            tuple.TableName = actualTableMap.TableName;
            tuple.SchemaName = actualTableMap.SchemaName;

            // Add primary key if one exists.
            var keyVariableMap = tableMap.PrimaryKey.VariableMap;
            if (keyVariableMap != null) {
                tuple.PrimaryKey = tuple.CreateColumn(keyVariableMap, ++_columnCounter);

                var advice = _plan.GetAdvice(keyVariableMap, tuple.Source);
                if (advice == AdviceRequests.Ignore) {
                    tuple.PrimaryKey.IsQueried = false;
                    tuple.PrimaryKey.IsMerged = false;
                }
            }

            return tuple;
        }

        private Join CreateJoin () {
            var join = new Join();
            _plan.Joins.Add(join);

            join.Ordinal = ++_joinCounter;

            return join;
        }
        #endregion
    }

    internal class ProcedureQueryAnalyzer {
        private ProcedureQueryPlan _plan;
        private QueryExpression _query;

        #region Constructors
        public ProcedureQueryAnalyzer (QueryExpression query) {
            _query = query;
        }
        #endregion

        #region Query Analysis
        public ProcedureQueryPlan Analyze () {
            // Reset the state.
            _plan = new ProcedureQueryPlan(_query);

            // Analyze the origin
            var queryTableMap = _query.GetResultTableMap();
            _plan.OriginTuple = AnalyzeTable(queryTableMap);

            return _plan;
        }
        #endregion

        #region Table Analysis
        private Tuple AnalyzeTable (TableMap tableMap) {
            // Create tuple.
            var tuple = CreateTuple(tableMap);

            // Create columns.
            var variableMaps = tuple.TableMap.VariableMaps.FindAllDirectsAndSubordinates(VariableStructure.BaseContainers | VariableStructure.WithinComponents);
            foreach (var variableMap in variableMaps)
                AnalyzeColumn(tuple, variableMap);

            return tuple;
        }

        private void AnalyzeColumn (Tuple tuple, VariableMap variableMap) {
            // Skip primary keys, as they are already in the tuple.
            if (tuple.TableMap.PrimaryKey.VariableMap == variableMap)
                return;

            if (variableMap is SubordinateVariableMap) {
                // Do nothing for now.
            } else
                AnalyzeDirectColumn(tuple, (DirectVariableMap) variableMap);
        }

        private void AnalyzeDirectColumn (Tuple tuple, DirectVariableMap variableMap) {
            var column = tuple.CreateColumn(variableMap, 0);

            // Analyze reference options.
            if (variableMap is ReferenceVariableMap && !(variableMap is LazyReferenceVariableMap)) {
                column.IsDeferred = true;

                // Do no further analysis on references for now.
            }
        }
        #endregion

        #region Analyzed Model Creation
        private Tuple CreateTuple (TableMap tableMap) {
            var tuple = new Tuple(_query);
            tuple.OriginTableMap = tableMap;
            tuple.TableMap = tableMap;
            tuple.TableName = string.Format(CultureInfo.InvariantCulture, "[EXEC {0}]", tableMap);

            var keyVariableMap = tableMap.PrimaryKey.VariableMap;
            if (keyVariableMap != null)
                tuple.PrimaryKey = tuple.CreateColumn(keyVariableMap, 0);

            return tuple;
        }
        #endregion
    }

    internal class MemoryQueryAnalyzer : IOrmExpressionVisitor<string> {
        private CachingDataSet _data;
        private QueryExpression _query;

        #region Constructors
        public MemoryQueryAnalyzer (QueryExpression query, CachingDataSet data) {
            _data = data;
            _query = query;
        }
        #endregion

        #region Query Analysis
        public DataQueryResults Analyze () {
            var results = new SequenceDataQueryResults(_query);

            // Analyze the origin and everything should flow from there.
            var queryTableMap = _query.GetResultTableMap();
            AnalyzeTable(queryTableMap);

            // todo
            return results;
        }
        #endregion

        #region Table Analysis
        private void AnalyzeTable (TableMap tableMap) {
            // Find the tables matching this query.

            foreach (var matchingRowTableMap in EachMatchingRowTableMap(tableMap)) {
            }

            // todo: find matching rows
        }

        private IEnumerable<Tuple<DataRow, TableMap>> EachMatchingRowTableMap (TableMap tableMap) {
            foreach (var currentTableMap in tableMap.GetUpdateHierarchy()) {
                if (!_data.ContainsTable(currentTableMap.TableName))
                    continue;

                var table = _data.GetTable(currentTableMap.TableName);
                foreach (var row in table.Select(GetFilterFor(table)))
                    yield return System.Tuple.Create(row, currentTableMap);
            }
        }
        #endregion

        #region Filtering
        private string GetFilterFor (DataTable table) {
            throw new NotImplementedException("todo");
        }
        #endregion

        #region IOrmExpressionVisitor<string> Members
        string IOrmExpressionVisitor<string>.Visit (LiteralExpression literal) {
            throw new NotImplementedException();
        }

        string IOrmExpressionVisitor<string>.Visit (QueryExpression query) {
            throw new NotImplementedException();
        }

        string IOrmExpressionVisitor<string>.Visit (AdviceExpression advice) {
            throw new NotImplementedException();
        }

        string IOrmExpressionVisitor<string>.Visit (OrderExpression order) {
            throw new NotImplementedException();
        }

        string IOrmExpressionVisitor<string>.Visit (ProjectionExpression projection) {
            throw new NotImplementedException();
        }

        string IOrmExpressionVisitor<string>.Visit (BinaryOperatorExpression binaryOperator) {
            var left = binaryOperator.Left.Accept(this);
            var right = binaryOperator.Right.Accept(this);
            var op = binaryOperator.GetSqlOperatorType();

            return string.Format(CultureInfo.InvariantCulture, "{0} {1} {2}", left, op, right);
        }

        string IOrmExpressionVisitor<string>.Visit (UnaryOperatorExpression unaryOperator) {
            throw new NotImplementedException();
        }

        string IOrmExpressionVisitor<string>.Visit (ApplyExpression apply) {
            throw new NotImplementedException();
        }

        string IOrmExpressionVisitor<string>.Visit (VariableExpression variable) {
            throw new NotImplementedException();
        }
        #endregion
    }
}
