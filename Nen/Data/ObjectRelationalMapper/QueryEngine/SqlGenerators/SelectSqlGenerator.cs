using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Data.SqlModel;
using Nen.Internal;
using System.Collections.ObjectModel;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine.SqlGenerators {
    internal class SelectSqlGenerator : SqlGenerator {
        private SelectQueryPlan _plan;
        private SqlSelect _select;

        #region Constructors
        public SelectSqlGenerator (SelectQueryPlan plan) {
            _plan = plan;
        }
        #endregion

        #region SQL Generation
        public override SqlExpression Generate () {
            _select = new SqlSelect(_plan.OriginTuple.SchemaName, _plan.OriginTuple.TableName);

            GenerateColumns(_plan.OriginTuple);
            GenerateJoins();

            if (_plan.Query.WhereCriteria != null || _plan.ExtraCriteria != null) {
                var criteria = BinaryOperatorExpression.Join(_plan.Query.WhereCriteria, BinaryOperatorType.And, _plan.ExtraCriteria);
                _select.Where = (SqlOperatorExpression) criteria.Accept(this);
            }

            foreach (var order in _plan.Query.OrderBy)
                _select.OrderBy.Add((SqlOrder) order.Accept(this));

            _select.Limit = _plan.Limit;
            _select.Offset = _plan.Offset;
            _select.IsDistinct = _plan.Query.IsDistinct;

            return _select;
        }

        private void GenerateColumns (Tuple tuple) {
            foreach (var column in tuple.Columns.Where(c => c.IsQueried)) {
                var sqlColumn = GenerateColumn(column);
                _select.Columns.Add(sqlColumn);
            }
        }

        private SqlColumn GenerateColumn (Column column) {
            if (column == null)
                throw new ArgumentNullException("column");

            if (column.Function != null)
                return GenerateFunctionColumn(column);

            if (column.Expression != null)
                return GenerateExpressionColumn(column);

            return GenerateDirectColumn(column);
        }

        private SqlColumn GenerateDirectColumn (Column column) {
            var columnName = ((DirectVariableMap) column.VariableMap).ColumnName;
            var sqlColumn = new SqlColumn(column.Tuple.Alias, columnName);

            if (columnName != column.Alias)
                sqlColumn.Alias = column.Alias;

            return sqlColumn;
        }

        private SqlColumn GenerateExpressionColumn (Column column) {
            var expression = column.Expression.Accept(this);
            var sqlColumn = new SqlColumn((SqlQueryExpression) expression);
            sqlColumn.Alias = column.Alias;
            return sqlColumn;
        }

        private SqlColumn GenerateFunctionColumn (Column column) {
            var sqlFunction = new SqlFunction(column.Function.Name);
            var sqlFunctionArguments = column.Function.Arguments.Select(a => GenerateColumn(a).Expression).Cast<SqlQueryExpression>();
            sqlFunction.FunctionArguments = sqlFunctionArguments.ToArray();
            return new SqlColumn(sqlFunction);
        }

        private void GenerateJoins () {
            foreach (var join in _plan.Joins) {
                GenerateColumns(join.Tuple);

                var left = new SqlIdentifier(join.OnLeft.Tuple.Alias, ((DirectVariableMap) join.OnLeft.VariableMap).ColumnName);
                var right = new SqlIdentifier(join.Alias, ((DirectVariableMap) join.OnRight.VariableMap).ColumnName);
                var on = new SqlBinaryOperatorExpression(left, "=", right);

                var table = new SqlTable(join.Tuple.SchemaName, join.Tuple.TableName);
                table.Alias = join.Alias;

                var isInner = join.SourceReference == null || join.SourceReference.CanInnerJoinOn;
                var sqlJoin = new SqlJoin(table, on, isInner ? SqlJoinType.Inner : SqlJoinType.LeftOuter);
                _select.Joins.Add(sqlJoin);
            }
        }
        #endregion

        #region QueryLanguage Expression -> SqlExpression Generation
        public override SqlExpression Visit(ApplyExpression apply)
        {
            var edge = _plan.ExpressionEdgeColumns[apply];
            return GenerateFunctionColumn(edge);
        }

        public override SqlExpression Visit (VariableExpression variable) {
            var plan = variable.GetSelectQueryPlan() ?? _plan;
            var edge = plan.ExpressionEdgeColumns[variable];
            return new SqlColumn(edge.Tuple.Alias, ((DirectVariableMap) edge.VariableMap).ColumnName);
        }
        #endregion
    }
}
