﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Data.SqlModel;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine.SqlGenerators {
    internal class UpdateSqlGenerator : SqlGenerator {
        private UpdateChangeOperation _operation;

        #region Constructors
        public UpdateSqlGenerator (UpdateChangeOperation operation) {
            _operation = operation;
        }
        #endregion

        #region SQL Generation
        public override SqlExpression Generate () {
            var hierarchy = _operation.TableMap.GetUpdateHierarchy().Reverse();
            var statements = hierarchy.Select(t => GenerateCore(t)).Where(u => u.Columns.Count > 0).ToArray();
            if (statements.Length == 1)
                return statements[0];

            return new SqlCompoundExpression(statements);
        }

        private SqlUpdate GenerateCore (TableMap tableMap) {
            var update = new SqlUpdate() { Table = new SqlIdentifier(tableMap.SchemaName, tableMap.TableName) };
            update.Where = GenerateWhere(tableMap);
            FillSqlParameters(update, tableMap);
            return update;
        }

        private SqlOperatorExpression GenerateWhere (TableMap currentTableMap) {
            var primaryKeyVariableMap = currentTableMap.PrimaryKey.VariableMap;
            var primaryKeyParameter = new SqlParameter("N__PrimaryKey__", primaryKeyVariableMap.DbType.Value);

            var primaryKeyExpression = new SqlBinaryOperatorExpression(new SqlIdentifier(currentTableMap.PrimaryKey.VariableMap.ColumnName), "=", primaryKeyParameter);
            var timeStampVariableMap = currentTableMap.TimeStampVariableMap;
            if (timeStampVariableMap == null || _operation.ChangeSet.IsComputationIgnored(timeStampVariableMap.Computation, currentTableMap.OriginTypeMap.Type))
                return primaryKeyExpression;

            // Fill the timestamp criteria too if applicable.
            var left = new SqlIdentifier(timeStampVariableMap.ColumnName);
            var right = new SqlParameter("N__PreviousTimeStamp__", timeStampVariableMap.DbType.Value) { Size = 8 };
            var criteria = new SqlBinaryOperatorExpression(left, "<=", right);
            return new SqlBinaryOperatorExpression(primaryKeyExpression, "AND", criteria);
        }

        private void FillSqlParameters (SqlUpdate update, TableMap tableMap) {
            var variableMaps = tableMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents)
                .Where(v => !v.IsPrimaryKey && !v.IsDatabaseGenerated && _operation.HasData(v));

            foreach (var variableMap in variableMaps) {
                update.Columns.Add(new SqlIdentifier(variableMap.ColumnName));
                update.Values.Add(GetParameter(variableMap.ColumnName, variableMap));
            }
        }
        #endregion
    }
}
