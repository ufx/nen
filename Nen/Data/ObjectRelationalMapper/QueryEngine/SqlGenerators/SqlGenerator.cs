﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Data.SqlModel;
using Nen.Internal;
using System.Data;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine.SqlGenerators {
    internal abstract class SqlGenerator : IOrmExpressionVisitor<SqlExpression> {
        public abstract SqlExpression Generate ();

        #region QueryLanguage Expression -> SqlExpression Generation
        #region Ignored / Invalid Expressions
        SqlExpression IOrmExpressionVisitor<SqlExpression>.Visit (AdviceExpression advice) {
            throw new NotImplementedException("Advice expressions must not be visited from the SQL generator.");
        }

        public virtual SqlExpression Visit (VariableExpression variable) {
            throw new NotImplementedException("Variable expressions are not supported by default.");
        }

        public virtual SqlExpression Visit (ApplyExpression apply) {
            throw new NotImplementedException("Apply expressions must not be visited from the SQL generator.");
        }

        SqlExpression IOrmExpressionVisitor<SqlExpression>.Visit (ProjectionExpression projection) {
            throw new NotImplementedException("Projection expressions must not be visited from the SQL generator.");
        }
        #endregion

        SqlExpression IOrmExpressionVisitor<SqlExpression>.Visit (QueryExpression query) {
            var plan = QueryAnalyzer.Analyze(query);
            return plan.GenerateSql();
        }

        SqlExpression IOrmExpressionVisitor<SqlExpression>.Visit (BinaryOperatorExpression binaryOperator) {
            var sqlExpression = new SqlBinaryOperatorExpression();
            sqlExpression.Left = binaryOperator.Left.Accept(this);
            sqlExpression.Right = binaryOperator.Right.Accept(this);
            sqlExpression.Operator = binaryOperator.GetSqlOperatorType();

            // Use "is null" instead of "= null" and "is not null" instead of "!= null"
            var rightIsNull = sqlExpression.Right is SqlLiteralExpression && ((SqlLiteralExpression) sqlExpression.Right).Value == null;
            if (rightIsNull && sqlExpression.Operator == "=")
                sqlExpression.Operator = "is";

            if (rightIsNull && sqlExpression.Operator == "!=")
                sqlExpression.Operator = "is not";

            return sqlExpression;
        }

        SqlExpression IOrmExpressionVisitor<SqlExpression>.Visit (UnaryOperatorExpression unaryOperator) {
            var operand = unaryOperator.Operand.Accept(this);

            switch (unaryOperator.Operator) {
                case UnaryOperatorType.Not:
                    if (operand is SqlBinaryOperatorExpression) {
                        var binaryOperand = (SqlBinaryOperatorExpression) operand;

                        // Transform (NOT (foo IN bar)) to (foo NOT IN bar)
                        if (string.Compare(binaryOperand.Operator, "in", true) == 0) {
                            binaryOperand.Operator = "NOT IN";
                            return binaryOperand;
                        } else if (string.Compare(binaryOperand.Operator, "like", true) == 0) {
                            binaryOperand.Operator = "NOT LIKE";
                            return binaryOperand;
                        }
                    }
                    else if (unaryOperator.Operand is VariableExpression)
                        return new SqlBinaryOperatorExpression(operand, "=", new SqlLiteralExpression(false));
                        
                    return new SqlPrefixOperatorExpression("NOT", operand);

                case UnaryOperatorType.Exists:
                    return new SqlPrefixOperatorExpression("EXISTS", operand);

                case UnaryOperatorType.Evaluate:
                    return new SqlBinaryOperatorExpression(operand, "=", new SqlLiteralExpression(true));

                case UnaryOperatorType.ToLower:
                    return new SqlFunction("LOWER", (SqlQueryExpression)operand);

                case UnaryOperatorType.ToUpper:
                    return new SqlFunction("UPPER", (SqlQueryExpression)operand);

                default:
                    throw new NotImplementedException(LS.T("Invalid prefix operator specified."));
            }
        }

        SqlExpression IOrmExpressionVisitor<SqlExpression>.Visit (LiteralExpression literal) {
            return new SqlLiteralExpression(literal.Value);
        }

        SqlExpression IOrmExpressionVisitor<SqlExpression>.Visit (OrderExpression order) {
            var sqlOrder = new SqlOrder();
            sqlOrder.Expression = ((SqlColumn) order.Over.Accept(this)).Expression;

            switch (order.Direction) {
                case OrderDirection.Ascending:
                    sqlOrder.Direction = SqlOrderDirection.Ascending;
                    break;

                case OrderDirection.Descending:
                    sqlOrder.Direction = SqlOrderDirection.Descending;
                    break;
            }

            return sqlOrder;
        }
        #endregion

        protected SqlParameter GetParameter (string parameterName, VariableMap variableMap) {
            var parameter = new SqlParameter();
            parameter.Name = parameterName;
            parameter.DbType = variableMap.DbType.Value;

            // todo: find a better way to declare this on the VariableMap.
            if (parameter.DbType == DbType.String)
                parameter.Size = int.MaxValue;
            else if (parameter.DbType == DbType.Binary)
                parameter.Size = int.MaxValue;
            else if (parameter.DbType == DbType.Time)
                parameter.Size = 5;
            else if (parameter.DbType == DbType.DateTime || parameter.DbType == DbType.DateTime2)
                parameter.Size = 8;
            else if (parameter.DbType == DbType.Decimal || parameter.DbType == DbType.Double || parameter.DbType == DbType.Single) {
                parameter.Precision = 18;
                parameter.Scale = 4;
            }

            return parameter;
        }
    }
}
