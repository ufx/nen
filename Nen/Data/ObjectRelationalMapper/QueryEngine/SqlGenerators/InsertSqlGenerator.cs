﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.SqlModel;
using Nen.Internal;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine.SqlGenerators
{
    internal class InsertSqlGenerator : SqlGenerator
    {
        private TableMap _tableMap;

        #region Constructors
        public InsertSqlGenerator(TableMap tableMap)
        {
            _tableMap = tableMap;
        }
        #endregion

        #region SQL Generation
        public override SqlExpression Generate()
        {
            var expressions = new List<SqlExpression>();
            var returnStatements = new List<SqlExpression>();


            var hierarchy = _tableMap.GetPhysicalHierarchy().Reverse();
            foreach (var tableMap in hierarchy)
                expressions.AddRange(GenerateCore(tableMap, returnStatements));

            if (expressions.Count == 1 && returnStatements.Count == 0)
                return expressions[0];

            // fixme: return statements should be rolled into one query.
            // this is bugged with mulitiple computed variables at different levels of hierarchy.
            expressions.AddRange(returnStatements);
            return new SqlCompoundExpression(expressions);
        }

        private List<SqlExpression> GenerateCore(TableMap tableMap, List<SqlExpression> returnStatements)
        {
            var expressions = new List<SqlExpression>();
            var insert = new SqlInsert() { Table = new SqlIdentifier(tableMap.SchemaName, tableMap.TableName) };
            expressions.Add(insert);
            FillSqlParameters(insert, tableMap);

            // Find extra work to do upon insert.
            var computedVariables = tableMap.VariableMaps.FindAll<DirectVariableMap>().Where(v => v.IsDatabaseGenerated && !v.IsPrimaryKey).ToArray();
            bool returnsPK = tableMap.BaseTableMap == null && tableMap.PrimaryKey.IsDatabaseGenerated;

            // Return this stock insert if the database gives us nothing.
            if (computedVariables.Length == 0 && !returnsPK)
                return expressions;

            // Create the insert, return, and possibly storage for important values.
            var resultQuery = new SqlSelect();
            SqlQueryExpression keyExpression;

            if (returnsPK)
            {
                if (tableMap.PrimaryKey is AutoIncrementingPrimaryKey)
                {
                    var autoIncrementPK = (AutoIncrementingPrimaryKey)tableMap.PrimaryKey;
                    expressions.Add(new SqlDeclaration("N__PrimaryKey__", new SqlInt32()));
                    keyExpression = new SqlIdentifier("N__PrimaryKey__") { IsDeclared = true };
                    expressions.Add(new SqlSet((SqlIdentifier)keyExpression, new SqlFunction("SCOPE_IDENTITY")));
                }
                else if (tableMap.PrimaryKey is SequencePrimaryKey)
                {
                    var sequencePK = (SequencePrimaryKey)tableMap.PrimaryKey;
                    keyExpression = new SqlFunction("currval", new SqlIdentifier(sequencePK.SchemaName, sequencePK.Name) { IsQuoted = true });
                }
                else
                    throw new NotImplementedException("Unknown database-generated primary key type.");

                resultQuery.Columns.Add(new SqlColumn(keyExpression) { Alias = "N__PrimaryKey__" });
            }
            else // Assume PK is passed in with this parameter.  Rife with frailty!
                keyExpression = new SqlParameter("N__PrimaryKey__", tableMap.PrimaryKey.VariableMap.DbType.Value);

            // Configure other database-generated values.
            if (computedVariables.Length > 0)
            {
                resultQuery.From.Add(new SqlTable(tableMap.TableName));

                foreach (var variable in computedVariables)
                    resultQuery.Columns.Add(new SqlColumn(variable.ColumnName));

                resultQuery.Where = new SqlBinaryOperatorExpression(new SqlIdentifier(tableMap.PrimaryKey.VariableMap.ColumnName), "=", keyExpression);
            }

            returnStatements.Add(resultQuery);
            return expressions;
        }

        private void FillSqlParameters(SqlInsert insert, TableMap tableMap)
        {
            var insertedVariableMaps = tableMap.VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents);
            foreach (var variableMap in insertedVariableMaps)
            {
                if (variableMap.IsDatabaseGenerated)
                    continue;

                var parameterName = variableMap.IsPrimaryKey ? "N__PrimaryKey__" : variableMap.ColumnName;
                insert.Columns.Add(new SqlIdentifier(variableMap.ColumnName));
                insert.Values.Add(GetParameter(parameterName, variableMap));
            }

            if (tableMap.PrimaryKey is SequencePrimaryKey)
            {
                var sequencePK = (SequencePrimaryKey)tableMap.PrimaryKey;
                insert.Columns.Add(new SqlIdentifier(tableMap.PrimaryKey.VariableMap.ColumnName));
                var sequenceProperty = tableMap.BaseTableMap == null ? "nextval" : "currval";
                insert.Values.Add(new SqlFunction(sequenceProperty, new SqlIdentifier(sequencePK.SchemaName, sequencePK.Name) { IsQuoted = true }));
            }
            else if (tableMap.PrimaryKey is AutoIncrementingPrimaryKey)
            {
                if (tableMap.BaseTableMap != null)
                {
                    var primaryKeyVariableMap = tableMap.PrimaryKey.VariableMap;
                    insert.Columns.Add(new SqlIdentifier(primaryKeyVariableMap.ColumnName));
                    insert.Values.Add(new SqlIdentifier("N__PrimaryKey__") { IsDeclared = true });
                }
            }
        }
        #endregion
    }
}
