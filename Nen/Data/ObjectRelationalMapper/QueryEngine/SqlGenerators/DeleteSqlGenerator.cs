﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.SqlModel;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine.SqlGenerators {
    internal class DeleteSqlGenerator : SqlGenerator {
        private TableMap _tableMap;

        #region Constructors
        public DeleteSqlGenerator (TableMap tableMap) {
            _tableMap = tableMap;
        }
        #endregion

        #region SQL Generation
        public override SqlExpression Generate () {
            var hierarchy = _tableMap.GetPhysicalHierarchy();
            var statements = hierarchy.Select(t => GenerateCore(t)).ToArray();
            if (statements.Length == 1)
                return statements[0];

            return new SqlCompoundExpression(statements);
        }

        private SqlDelete GenerateCore (TableMap currentTableMap) {
            var primaryKeyVariableMap = currentTableMap.PrimaryKey.VariableMap;
            var primaryKeyParameter = new SqlParameter("N__PrimaryKey__", primaryKeyVariableMap.DbType.Value);

            var delete = new SqlDelete() { Table = new SqlIdentifier(currentTableMap.SchemaName, currentTableMap.TableName) };
            delete.Where = new SqlBinaryOperatorExpression(new SqlIdentifier(primaryKeyVariableMap.ColumnName), "=", primaryKeyParameter);
            return delete;
        }
        #endregion
    }
}
