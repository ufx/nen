using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Reflection;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine {
    internal class Column : IQuerySource {
        #region Constructors
        public Column (Tuple tuple, VariableMap variableMap) {
            if (tuple == null)
                throw new ArgumentNullException("tuple");

            IsMerged = true;
            IsQueried = true;

            Tuple = tuple;
            VariableMap = variableMap;
        }
        #endregion

        #region Accessors
        private string _alias = "[none]";
        private int _ordinal;
        private VariableMap _variableMap;

        public Column DestinationReference { get; set; }
        public OrmExpression Expression { get; set; }
        public Function Function { get; set; }
        public bool IsDeferred { get; set; }
        public bool IsQueried { get; set; }
        public bool IsMerged { get; set; }
        public Tuple Tuple { get; private set; }

        public int Ordinal
        {
            get { return _ordinal; }
            set { _ordinal = value; UpdateAlias(); }
        }


        public VariableMap VariableMap
        {
            get { return _variableMap; }
            private set { _variableMap = value; UpdateAlias(); }
        }

        public string Alias {
            get { return _alias; }
        }

        public bool CanInnerJoinOn {
            get {
                if (VariableMap == null)
                    return false;

                return VariableMap.IsRequired && (Tuple.SourceReference == null || Tuple.SourceReference.CanInnerJoinOn);
            }
        }
        #endregion

        private void UpdateAlias ()
        {       
            var directVariableMap = VariableMap as DirectVariableMap;
            if (directVariableMap == null)
                _alias = "[none]";
            else
                _alias = Ordinal == 0 ? directVariableMap.ColumnName : directVariableMap.ColumnName + "_" + Ordinal;
        }

        #region Destinations
        public Column GetFinalDestinationColumn () {
            return DestinationReference == null ? this : DestinationReference.GetFinalDestinationColumn();
        }
        #endregion

        #region IQuerySource
        public Stack<VariableInfo> GetPath () {
            var path = new Stack<VariableInfo>();
            path.PushAll(Tuple.GetPath().Reverse());

            if (VariableMap != null)
                path.Push(VariableMap.Variable);
            return path;
        }

        public QueryExpression GetOriginQuery () {
            return Tuple.GetOriginQuery();
        }
        #endregion

        #region Object Members
        public override string ToString () {
            var modifiers = (IsQueried || IsMerged || IsDeferred) ? " [" + (IsQueried ? "Q" : "") + (IsMerged ? "M" : "") + (IsDeferred ? "D" : "") + "]" : "";

            if (Function != null)
                return Function.ToString() + modifiers;
            else if (VariableMap != null)
                return VariableMap.ToString() + modifiers;
            else
                return base.ToString();
        }
        #endregion
    }

    internal class Join {
        #region Constructors
        public Join () {
            IsMerged = true;
        }
        #endregion

        #region Accessors
        public bool IsMerged { get; set; }
        public Column OnLeft { get; set; }
        public Column OnRight { get; set; }
        public int Ordinal { get; set; }
        public Column SourceReference { get; set; }
        public Tuple Tuple { get; set; }
        public bool HasData { get; set; }

        public string Alias {
            get { return string.Format(CultureInfo.InvariantCulture, "{0}_{1}", Tuple.TableMap.TableName, Ordinal); }
        }
        #endregion
    }

    internal class Tuple : IQuerySource {
        private Column _sourceReference;
        public string Alias { get; set; }
        public Tuple BaseTuple { get; set; }
        public ICollection<Column> Columns { get; private set; }
        public Column Discriminator { get; set; }
        public bool IsAmbiguous { get; set; }
        public TableMap OriginTableMap { get; set; }
        public QueryExpression OriginQuery { get; set; }
        public Column PrimaryKey { get; set; }
        public string SchemaName { get; set; }
        public TableMap TableMap { get; set; }
        public string TableName { get; set; }
        public IQuerySource Source { get; private set; }

        #region Constructors
        public Tuple (QueryExpression originQuery) {
            Columns = new List<Column>();
            OriginQuery = originQuery;
            Source = originQuery;
        }
        #endregion

        #region Column Creation
        public Column CreateColumn (VariableMap variableMap, int ordinal) {
            var column = new Column(this, variableMap);
            column.Ordinal = ordinal;

            Columns.Add(column);

            return column;
        }

        public Column FindOrCreateUnqueriedColumn (VariableMap variableMap, int ordinal) {
            var destinationTuple = FindTupleFor(variableMap);
            return destinationTuple.FindOrCreateUnqueriedColumnOnTuple(variableMap, ordinal);
        }

        private Column FindOrCreateUnqueriedColumnOnTuple (VariableMap variableMap, int ordinal) {
            var column = FindColumnFor(variableMap);
            if (column == null) {
                column = CreateColumn(variableMap, ordinal);
                column.IsQueried = false;
                column.IsMerged = false;
            }
            return column;
        }
        #endregion

        #region Structure Searching
        private Tuple FindTupleFor (VariableMap variableMap) {
            if (TableMap.VariableMaps.FindAll<VariableMap>(VariableStructure.ViewContainers | VariableStructure.WithinComponents).Contains(variableMap))
                return this;

            if (BaseTuple != null)
                return BaseTuple.FindTupleFor(variableMap);

            return null;
        }

        private Column FindColumnFor (VariableMap variableMap) {
            return Columns.FirstOrDefault(c => c.VariableMap == variableMap);
        }
        #endregion

        #region Querying
        public QueryExpression MakeSourceQuery (QueryExpression originQuery) {
            var query = (QueryExpression) originQuery.Clone();
            query.IsPossiblyAmbiguous = false;

            if (_sourceReference == null)
                query.Projection.Items.Add(new VariableExpression(OriginTableMap.PrimaryKey.VariableMap.Variable, query));
            else {
                // Make sure to project only a relative path.
                var sourcePath = GetOriginQuery().GetPath();
                var relativePath = GetPath().Reverse().Skip(sourcePath.Count).ToArray();
                var composite = new CompositeVariableInfo(relativePath);
                query.Projection.Items.Add(new VariableExpression(composite, query));
            }

            // Ordering is not necessary in subqueries with no windowing parameters.
            if (query.Offset == null && query.Limit == null)
                query.OrderBy.Clear();

            return query;
        }
        #endregion

        #region Accessors
        public Column SourceReference {
            get { return _sourceReference; }
            set {
                _sourceReference = value;
                Source = (IQuerySource) value ?? OriginQuery;
            }
        }

        public Column PhysicalPrimaryKey {
            get { return BaseTuple == null ? PrimaryKey : BaseTuple.PhysicalPrimaryKey; }
        }
        #endregion

        #region Object Members
        public override string ToString () {
            return TableMap == null ? base.ToString() : TableMap.ToString();
        }
        #endregion

        #region IQuerySource
        public Stack<VariableInfo> GetPath () {
            return Source.GetPath();
        }

        public QueryExpression GetOriginQuery () {
            return OriginQuery;
        }
        #endregion
    }

    internal class Function {
        #region Accessors
        public Column[] Arguments { get; set; }
        public string Name { get; set; }
        #endregion

        public override string ToString()
        {
            if (Arguments == null)
                return Name + "()";
            else
                return Name + "(" + string.Join(", ", Arguments.Select(a => a.ToString()).ToArray()) + ")";
        }
    }

    internal class IndirectQuerySource : IQuerySource {
        private Tuple _tuple;
        private VariableMap _variableMap;

        public IndirectQuerySource (Tuple tuple, VariableMap variableMap) {
            _tuple = tuple;
            _variableMap = variableMap;
        }

        public Stack<VariableInfo> GetPath () {
            var path = new Stack<VariableInfo>();
            path.PushAll(_tuple.GetPath().Reverse());
            path.Push(_variableMap.Variable);
            return path;
        }

        public QueryExpression GetOriginQuery () {
            return _tuple.GetOriginQuery();
        }

        #region Object Members
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "{0}.{1}", _tuple, _variableMap);
        }
        #endregion
    }
}
