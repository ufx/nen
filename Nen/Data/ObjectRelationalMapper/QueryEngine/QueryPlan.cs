using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryEngine.SqlGenerators;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Data.SqlModel;
using Nen.Reflection;
using Nen.Internal;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine {
    internal abstract class QueryPlan {
        #region Constructors
        protected QueryPlan (QueryExpression query) {
            Query = query;
        }
        #endregion

        #region SQL Generation
        public abstract SqlExpression GenerateSql ();
        #endregion

        #region Merging
        public HashSet<object> Merge (IDataReader reader, DataQueryEngine engine) {
            if (reader == null)
                throw new ArgumentNullException("reader");

            var keys = new HashSet<object>();

            while (reader.Read()) {
                var key = MergeTuple(reader, engine);
                keys.Add(key);
            }

            if (keys.Count > 0)
                OnRowsMerged(engine);

            return keys;
        }

        public virtual void OnRowsMerged (DataQueryEngine engine) {
            FillCollections(OriginTuple, engine);
        }

        public virtual object MergeTuple (IDataReader reader, DataQueryEngine engine) {
            return MergeTuple(reader, OriginTuple, engine);
        }

        public object MergeTuple (IDataReader reader, Tuple tuple, DataQueryEngine engine) {
            var table = engine.Results.GetTable(tuple.TableMap);

            // Find the primary key in the results.
            var physicalPrimaryKey = tuple.PhysicalPrimaryKey;
            object primaryKeyValue = null;
            if (physicalPrimaryKey != null) {
                primaryKeyValue = reader[physicalPrimaryKey.Alias];
                if (primaryKeyValue == DBNull.Value)
                    return null; // Row doesn't actually exist.

                // Convert the PK to the right destination type before any lookups.
                if (physicalPrimaryKey.VariableMap != null)
                    primaryKeyValue = physicalPrimaryKey.VariableMap.ConvertDatabaseValue(primaryKeyValue);

                if (table.FindRowByPrimaryKey(primaryKeyValue) != null)
                    return primaryKeyValue; // Key already exists.

                if (tuple.IsAmbiguous) {
                    FillDiscriminatedRow(primaryKeyValue, reader, tuple, engine);
                    return primaryKeyValue;
                }
            }

            // Create a new row and merge all columns that are mergable.
            var newRow = table.CreateRow();

            foreach (var column in tuple.Columns.Where(c => c.IsMerged))
                MergeColumn(reader, newRow, column, engine);

            // Merge the actual physical primary key.
            if (physicalPrimaryKey != tuple.PrimaryKey)
                MergeColumn(reader, newRow, tuple.PrimaryKey, engine);

            // Merge the key of this table into origin tables that are 
            // optimized out.
            if (tuple.OriginTableMap != tuple.TableMap) {
                for (var tableMap = tuple.OriginTableMap; tableMap.IsOptimizedOutForReads && tableMap != null; tableMap = tableMap.BaseTableMap) {
                    var physicalPrimaryKeyVariableMap = tableMap.PrimaryKey.VariableMap;
                    var optimizedOutTable = engine.Results.GetTableCore(tableMap);
                    var optimizedOutRow = optimizedOutTable.CreateRow();
                    optimizedOutRow[physicalPrimaryKeyVariableMap.ColumnName] = primaryKeyValue;
                    optimizedOutTable.AddRow(optimizedOutRow);
                }
            }

            table.AddRow(newRow);

            return physicalPrimaryKey == null ? table.Rows.Count() - 1 : primaryKeyValue;
        }

        private void MergeColumn (IDataReader reader, LightRow newRow, Column column, DataQueryEngine engine) {
            if (column.VariableMap is SubordinateVariableMap && column.IsDeferred) {
                var subordinateVariableMap = (SubordinateVariableMap)column.VariableMap;
                var key = reader[column.Tuple.PrimaryKey.Alias];
                key = subordinateVariableMap.ConvertDatabaseValue(key);
                engine.EnqueueFill(new DeferredDbFill(engine, column, subordinateVariableMap.KeyVariableMap, key));
                return;
            }

            var alias = column.GetFinalDestinationColumn().Alias;
            var variableMap = (DirectVariableMap) column.VariableMap;
            object convertedValue, value;

            try {
                value = reader[alias];

                // Convert the value to its destination type before storage, as the value may be involved in indexes.
                convertedValue = variableMap.ConvertDatabaseValue(value);
                newRow[variableMap.ColumnName] = convertedValue;
            } catch (ThreadAbortException) {
                throw;
            } catch (Exception ex) {
                throw new MapException(LS.T("Error merging column value '{0}' on table '{1}' with alias '{2}'.", variableMap, variableMap.PhysicalTableMap.TableName, alias), ex);
            }

            if (column.IsDeferred && value != DBNull.Value) {
                var referenceVariableMap = (ReferenceVariableMap) variableMap;
                engine.EnqueueFill(new DeferredDbFill(engine, column, referenceVariableMap.ReferencedTableMap.PrimaryKey.VariableMap, convertedValue));
            }
        }
        #endregion

        #region Fill Generation
        private void FillDiscriminatedRow (object key, IDataReader reader, Tuple tuple, DataQueryEngine engine) {
            var discriminatorValue = reader[tuple.Discriminator.Alias];
            var subTableMap = tuple.TableMap.GetDiscriminatedSubTableMap(discriminatorValue);
            engine.EnqueueFill(new DiscriminatedDbFill(engine, tuple, subTableMap, subTableMap.PrimaryKey.VariableMap, key, discriminatorValue));
        }

        public void FillCollections (Tuple tuple, DataQueryEngine engine) {
            foreach (var variableMap in tuple.OriginTableMap.VariableMaps.FindAll<CollectionVariableMap>(VariableStructure.BaseContainers)) {
                var advice = GetAdvice(variableMap, tuple);
                if (advice == AdviceRequests.Include)
                    FillCollection(engine, tuple, variableMap);
            }
        }

        public virtual void FillCollection (DataQueryEngine engine, Tuple tuple, CollectionVariableMap variableMap) {
            var source = new IndirectQuerySource(tuple, variableMap);
            var collectionSourceQuery = tuple.MakeSourceQuery(Query);
            engine.EnqueueFill(new CollectionDbFill(engine, source, variableMap, collectionSourceQuery));
        }
        #endregion

        #region Scalar Results
        public object GetScalarResult (IDataReader reader) {
            if (!reader.Read())
                return null;

            var value = reader[0];
            return value == DBNull.Value ? null : value;
        }
        #endregion

        #region Advice Access
        internal AdviceRequests GetAdvice (VariableMap variableMap, IQuerySource source) {
            var path = new Stack<VariableInfo>();
            if (source != null)
                path.PushAll(source.GetPath().Reverse());
            path.Push(variableMap.Variable);

            return Query.GetAdvice(path, variableMap);
        }
        #endregion

        #region Statistics
        public abstract void WriteStatistics (QueryStatistics statistics);
        #endregion

        #region Optimizing
        public abstract void Optimize ();
        #endregion

        #region Accessors
        public Tuple OriginTuple { get; set; }
        public QueryExpression Query { get; private set; }
        #endregion
    }

    internal class SelectQueryPlan : QueryPlan {
        #region Constructors
        public SelectQueryPlan (QueryExpression query)
            : base(query) {

            ExpressionEdgeColumns = new Dictionary<OrmExpression, Column>();
            Joins = new List<Join>();
        }
        #endregion

        #region QueryPlan Methods
        public override SqlExpression GenerateSql () {
            var generator = new SelectSqlGenerator(this);
            return generator.Generate();
        }

        public override void OnRowsMerged (DataQueryEngine engine) {
            base.OnRowsMerged(engine);

            foreach (var join in Joins.Where(j => j.IsMerged && j.HasData))
                FillCollections(join.Tuple, engine);
        }

        public override object MergeTuple (IDataReader reader, DataQueryEngine engine) {
            var key = base.MergeTuple(reader, engine);

            foreach (var join in Joins.Where(j => j.IsMerged))
            {
                var joinKey = MergeTuple(reader, join.Tuple, engine);
                if (joinKey != null)
                    join.HasData = true;
            }

            return key;
        }

        public override void WriteStatistics (QueryStatistics statistics) {
            statistics.NumberOfJoins += Joins.Count;
        }

        public override void Optimize () {
            RemoveUnnecessaryColumnAliases();
        }

        private void RemoveUnnecessaryColumnAliases () {
            if (Limit.HasValue || Offset.HasValue)
                return;

            if (Joins.Count > 0)
                return;

            var queriedColumns = OriginTuple.Columns.Where(c => c.IsQueried).ToList();
            if (queriedColumns.Count > 1)
                return;

            if (queriedColumns.Count == 0)
                throw new QueryException(LS.T("Internal error querying '{0}'.  Tuple has no queried columns.", OriginTuple));

            queriedColumns[0].Ordinal = 0;
        }
        #endregion

        #region Accessors
        public Dictionary<OrmExpression, Column> ExpressionEdgeColumns { get; private set; }
        public ICollection<Join> Joins { get; set; }
        public int? Limit { get; set; }
        public int? Offset { get; set; }
        public OperatorExpression ExtraCriteria { get; set; }
        #endregion
    }

    internal class ProcedureQueryPlan : QueryPlan {
        private TableMap _tableMap;

        #region Constructors
        public ProcedureQueryPlan (QueryExpression query)
            : base(query) {
            _tableMap = query.GetResultTableMap();
        }
        #endregion

        #region QueryPlan Methods
        public override SqlExpression GenerateSql () {
            var subject = (ProcedureExpression) Query.Subject;

            var procedure = new SqlProcedure(subject.ProcedureName);

            if (subject.ProcedureArguments != null)
                procedure.ProcedureArguments.AddRange(subject.ProcedureArguments.Select(a => (SqlQueryExpression) new SqlLiteralExpression(a)));

            return procedure;
        }

        public override void WriteStatistics (QueryStatistics statistics) {
            // Nothing to write.
        }

        public override void FillCollection (DataQueryEngine engine, Tuple tuple, CollectionVariableMap variableMap) {
            throw new NotImplementedException("Collection fills are not yet implemented.");
        }

        public override void Optimize () {
            // Nothing to optimize.
        }
        #endregion
    }
}
