﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine {
    internal abstract class DbFillResult {
        #region Constructors
        protected DbFillResult (HashSet<object> keys) {
            Keys = keys;
        }
        #endregion

        #region Accessors
        public HashSet<object> Keys { get; private set; }
        #endregion
    }

    internal class OriginDbFillResult : DbFillResult {
        #region Constructors
        public OriginDbFillResult (HashSet<object> keys)
            : base(keys) {
        }
        #endregion
    }

    internal class CollectionDbFillResult : DbFillResult {
        private Dictionary<object, List<object>> _matchingPrimaryKeysByCollectionKey;

        #region Constructors
        public CollectionDbFillResult (HashSet<object> keys, CollectionVariableMap collectionVariableMap)
            : base(keys) {
            CollectionVariableMap = collectionVariableMap;
        }
        #endregion

        public List<object> GetMatches (object key, LightSet data, DataContext context) {
            if (_matchingPrimaryKeysByCollectionKey == null) {
                _matchingPrimaryKeysByCollectionKey = new Dictionary<object, List<object>>();

                var keyColumnName = CollectionVariableMap.KeyVariableMap.ColumnName;
                var table = data[CollectionVariableMap.KeyVariableMap.PhysicalTableMap.TableName];

                foreach (var childKey in Keys) {
                    var row = table.FindRowByPrimaryKey(childKey);
                    if (row == null) {
                        var child = context.GetStoredObject(CollectionVariableMap.CollectionTableMap, childKey);
                        var parent = CollectionVariableMap.KeyVariableMap.Variable.GetValue(child);
                        var parentPrimaryKey = CollectionVariableMap.PhysicalTableMap.PrimaryKey.VariableMap.Variable.GetValue(parent);
                        _matchingPrimaryKeysByCollectionKey.Activate(childKey).Add(parentPrimaryKey);
                    } else {
                        var collectionKey = row[keyColumnName];
                        _matchingPrimaryKeysByCollectionKey.Activate(collectionKey).Add(childKey);
                    }
                }
            }

            List<object> matches;
            if (_matchingPrimaryKeysByCollectionKey.TryGetValue(key, out matches))
                return matches;
            return null;
        }

        #region Accessors
        public CollectionVariableMap CollectionVariableMap { get; private set; }
        #endregion
    }

    internal class ScalarDbFillResult : DbFillResult {
        #region Constructors
        public ScalarDbFillResult (object value)
            : base(null) {
            Value = value;
        }
        #endregion

        #region Accessors
        public object Value { get; private set; }
        #endregion
    }
}
