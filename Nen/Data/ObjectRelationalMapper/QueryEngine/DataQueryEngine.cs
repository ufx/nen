using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper.QueryModel;

namespace Nen.Data.ObjectRelationalMapper.QueryEngine {
    internal class DataQueryEngine {
        #region Constructor
        public DataQueryEngine (QueryExpression query, Database database) {
            if (database == null)
                throw new ArgumentNullException("database");

            Database = database;
            Query = query;
            PendingFills = new Queue<DbFill>();
        }
        #endregion

        #region Query Execution
        public DataQueryResults Execute (IDbConnection conn, IDbTransaction tran) {
            if (Query.IsScalar) {
                Results = new ScalarDataQueryResults(Query);
                EnqueueFill(new ScalarOriginDbFill(this, Query));
            } else {
                Results = new SequenceDataQueryResults(Query);
                EnqueueFill(new OriginDbFill(this, Query));
            }

            do {
                var fill = PendingFills.Dequeue();
                if (fill.Prepare())
                    fill.Execute(conn, tran, Database);
            } while (PendingFills.Count > 0);

            return Results;
        }
        #endregion

        #region Fill Management
        public void EnqueueFill (DbFill fill) {
            // Enqueue the fill iff any existing fills do not combine with it.
            if (!PendingFills.Any(f => f.Combine(fill)))
                PendingFills.Enqueue(fill);
        }
        #endregion

        #region Accessors
        public Database Database { get; private set; }
        public QueryExpression Query { get; private set; }
        public DataQueryResults Results { get; private set; }
        protected Queue<DbFill> PendingFills { get; private set; }
        #endregion
    }
}
