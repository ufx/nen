﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Data.ObjectRelationalMapper.QueryEngine;

namespace Nen.Data.ObjectRelationalMapper {
    public class MemoryStore : Store {
        #region Constructors
        public MemoryStore (MapConfiguration configuration)
            : base(configuration) {
            Data = new CachingDataSet();
            Data.Value.EnforceConstraints = false;
        }
        #endregion

        protected internal override void CommitCore(ChangeSet changes, IDbConnection conn, IDbTransaction tran)
        {
            throw new NotImplementedException();
        }

        protected override DataQueryResults QueryCore (QueryExpression query, IDbConnection conn, IDbTransaction tran)
        {
            var engine = new DataMemoryEngine(query, Data);
            return engine.Execute();
        }

        public override IDbConnection OpenDbConnection()
        {
            return null;
        }

        public override bool IsPrepareSupported
        {
            get { throw new NotImplementedException(); }
        }

        #region Accessors
        public CachingDataSet Data { get; private set; }
        #endregion
    }
}
