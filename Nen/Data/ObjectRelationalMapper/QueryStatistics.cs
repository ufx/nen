﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Records statistics on all aspects of a query.
    /// </summary>
    public class QueryStatistics {
        #region Accessors
        /// <summary>
        /// Gets the total time spent executing and mapping the query.
        /// </summary>
        public TimeSpan TotalTime {
            get { return TotalQueryTime + MappingTime; }
        }

        /// <summary>
        /// Gets the total time spent gathering information for the query.
        /// </summary>
        public TimeSpan TotalQueryTime {
            get { return DatabaseTime + PlanningTime + MergeTime; }
        }

        /// <summary>
        /// Gets the time spent requesting query data from a database.
        /// </summary>
        public TimeSpan DatabaseTime { get; internal set; }

        /// <summary>
        /// Gets the time spent mapping query data into an object graph.
        /// </summary>
        public TimeSpan MappingTime { get; internal set; }

        /// <summary>
        /// Gets the time spent planning and analyzing the query.
        /// </summary>
        public TimeSpan PlanningTime { get; internal set; }

        /// <summary>
        /// Gets the time spent merging query data with its cache.
        /// </summary>
        public TimeSpan MergeTime { get; internal set; }

        /// <summary>
        /// Gets the number of instances found in the context cache.  
        /// </summary>
        /// <remarks>A high number here means the query is retrieving a lot of
        /// objects that were preivously retrieved.</remarks>
        public int NumberOfContextCacheHits { get; internal set; }

        /// <summary>
        /// Gets the number of instances not found in the context cache.
        /// </summary>
        /// <remarks>A high number here means the query is retrieving a lot of
        /// objects that were not previously retrieved.</remarks>
        public int NumberOfContextCacheMisses { get; internal set; }

        /// <summary>
        /// Gets the number of deferred rows found in the data cache.
        /// </summary>
        /// <remarks>A high number here means the query is optimizing out
        /// access of unnecessary data.</remarks>
        public int NumberOfDeferredHits { get; internal set; }

        /// <summary>
        /// Gets the number of deferred rows not found in the data cache.
        /// </summary>
        /// <remarks>A high number here means the query is inefficiently
        /// accessing necessary data.</remarks>
        public int NumberOfDeferredMisses { get; internal set; }

        /// <summary>
        /// Gets the number of database queries needed to fill all query data.
        /// </summary>
        public int NumberOfDatabaseQueries { get; internal set; }

        /// <summary>
        /// Gets the number of joins needed to fill all query data.
        /// </summary>
        public int NumberOfJoins { get; internal set; }
        #endregion
    }
}
