﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Linq.Expressions;
using Nen.Internal;
using Nen.Reflection;

namespace Nen.Data.ObjectRelationalMapper {
    internal interface ITable : IQueryProvider, IQueryable {
        OrmExpression CreateQuerySubject ();
        DataContext Context { get; }
    }

    /// <summary>
    /// DataQueries are manipulatable, queryable sets of objects.
    /// </summary>
    public abstract class DataQuery : IQueryProvider, IOrderedQueryable, ITable {
        private Expression _expression;
        private object[] _procedureArgs;
        private string _procedureName;
        private Type _type;

        #region Constructors
        internal DataQuery (DataContext context, Type type) {
            Context = context;
            _type = type;
            _expression = Expression.Constant(this);
        }

        internal DataQuery (DataContext context, Expression expression, Type type) {
            Context = context;
            _expression = expression;
            _type = type;
        }

        internal DataQuery (DataContext context, Type type, string procedureName, params object[] procedureArgs)
            : this(context, type) {
            _procedureName = procedureName;
            _procedureArgs = procedureArgs;
        }
        #endregion

        #region Query Execution / Translation
        /// <summary>
        /// Executes and returns the sequence of objects for a query.
        /// </summary>
        /// <returns>The sequence of mapped objects yielded by the query.</returns>
        protected IEnumerable<object> ExecuteSequence () {
            var query = TranslateQuery();
            return query.ExecuteSequence();
        }

        internal QueryExpression TranslateQuery () {
            return TranslateQuery(_expression);
        }

        internal QueryExpression TranslateQuery (Expression expr) {
            return LinqExpressionTranslator.Translate(expr, Context);
        }
        #endregion

        #region IEnumerable Members
        /// <summary>
        /// Returns an enumerator that iterates through the query.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the query.</returns>
        public IEnumerator GetEnumerator () {
            return ExecuteSequence().GetEnumerator();
        }
        #endregion

        #region IQueryable Members
        /// <summary>
        /// Gets the type of object returned by the query.
        /// </summary>
        public Type ElementType {
            get { return _type; }
        }

        /// <summary>
        /// Gets an expression for the query.
        /// </summary>
        public Expression Expression {
            get { return _expression; }
        }

        /// <summary>
        /// Gets a query provider that can access the query.
        /// </summary>
        public IQueryProvider Provider {
            get { return this; }
        }
        #endregion

        #region IQueryProvider Members
        /// <summary>
        /// Creates a query for the given expression using the query context.
        /// </summary>
        /// <typeparam name="TElement">The type of element the query returns.</typeparam>
        /// <param name="expression">The query expression.</param>
        /// <returns>A DataQuery for the given expression.</returns>
        public IQueryable<TElement> CreateQuery<TElement> (Expression expression) {
            return new DataQuery<TElement>(Context, expression);
        }

        /// <summary>
        /// Creates a query for the given expression using the query context.
        /// </summary>
        /// <param name="expression">The query expression.</param>
        /// <returns>A DataQuery for the given expression.</returns>
        public IQueryable CreateQuery (Expression expression) {
            return TypeHelper.MakeQueryableFromExpression(expression, typeof(DataQuery<>), Context);
        }

        /// <summary>
        /// Executes a query for the given expression using the query context.
        /// </summary>
        /// <typeparam name="TResult">The type of result the execution returns.</typeparam>
        /// <param name="expression">The execution expression.</param>
        /// <returns>The result of the execution.</returns>
        public TResult Execute<TResult> (Expression expression) {
            var result = Execute(expression);
            if (result is IConvertible && !typeof(TResult).IsNullable())
                return (TResult) Convert.ChangeType(result, typeof(TResult));
            else
                return (TResult) result;
        }

        /// <summary>
        /// Executes a query for the given expression using the query context.
        /// </summary>
        /// <param name="expression">The execution expression.</param>
        /// <returns>The result of the execution.</returns>
        public object Execute (Expression expression) {
            var query = TranslateQuery(expression);
            return query.ExecuteSingleton();
        }
        #endregion

        #region ITable Members
        OrmExpression ITable.CreateQuerySubject () {
            var configuredTableMaps = Context.Configuration.TableMaps;
            if (!configuredTableMaps.ContainsKey(_type))
                throw new InvalidOperationException(LS.T("Queried type '{0}' is not persistent.", _type));

            var tableMap = configuredTableMaps[_type];
            var table = new TableExpression(tableMap);

            if (_procedureName == null)
                return table;
            else
                return new ProcedureExpression(table, _procedureName, _procedureArgs);
        }

        #endregion

        #region Accessors
        public DataContext Context { get; private set; }
        #endregion
    }

    /// <summary>
    /// DataQueries are manipulatable, queryable sets of objects.
    /// </summary>
    /// <typeparam name="T">The type of object returned by the query.</typeparam>
    public class DataQuery<T> : DataQuery, IOrderedQueryable<T> {
        #region Constructors
        internal DataQuery (DataContext context)
            : base(context, typeof(T)) {
        }

        internal DataQuery (DataContext context, Expression expression)
            : base(context, expression, typeof(T)) {
        }

        internal DataQuery (DataContext context, string procedureName, params object[] procedureArgs)
            : base(context, typeof(T), procedureName, procedureArgs) {
        }
        #endregion

        #region IEnumerable<T> Members
        /// <summary>
        /// Returns an enumerator that iterates through the query.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the query.</returns>
        public new IEnumerator<T> GetEnumerator () {
            return ExecuteSequence().Cast<T>().GetEnumerator();
        }
        #endregion

        #region Operations
        /// <summary>
        /// Deletes an instance from the table.
        /// </summary>
        /// <param name="instance">The instance to delete.</param>
        public void Delete (T instance) {
            Context.Delete(instance);
        }

        /// <summary>
        /// Loads a single instance from the table.
        /// </summary>
        /// <param name="key">The key of the instance to load.</param>
        /// <returns>The instance indicated by the key.</returns>
        public T Load (object key) {
            return Context.Load<T>(key);
        }
        #endregion
    }
}
