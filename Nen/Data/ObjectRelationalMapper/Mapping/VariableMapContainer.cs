using System;
using System.Collections.Generic;
using System.Text;

using Nen.Runtime.Serialization;

namespace Nen.Data.ObjectRelationalMapper.Mapping {
    /// <summary>
    /// Abstract base for classes which contain VariableMaps.
    /// </summary>
    public abstract class VariableMapContainer {
        #region Constructors
        /// <summary>
        /// Constructs a new VariableMapContainer.
        /// </summary>
        /// <param name="configuration">The configuration of the conatined variable maps.</param>
        protected VariableMapContainer (MapConfiguration configuration) {
            Configuration = configuration;
            VariableMaps = new VariableMapCollection(this);
        }

        /// <summary>
        /// Constructs a new VariableMapContainer.
        /// </summary>
        /// <param name="copy">The container to copy.</param>
        protected VariableMapContainer (VariableMapContainer copy) {
            Configuration = copy.Configuration;
            VariableMaps = copy.VariableMaps.Clone(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the base container.
        /// </summary>
        public abstract VariableMapContainer BaseContainer { get; }

        /// <summary>
        /// Gets the view container.
        /// </summary>
        public abstract VariableMapContainer ViewContainer { get; }

        /// <summary>
        /// Gets the configuration of the contained variable maps.
        /// </summary>
        public MapConfiguration Configuration { get; private set; }
        
        /// <summary>
        /// Gets the collection of variable maps.
        /// </summary>
        [XDataMember]
        public VariableMapCollection VariableMaps { get; private set; }
        #endregion
    }
}
