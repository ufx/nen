using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Reflection;
using Nen.Internal;

namespace Nen.Data.ObjectRelationalMapper.Mapping
{
    /// <summary>
    /// Contains a number of VariableMaps that are tied to a
    /// VariableMapContainer.
    /// </summary>
    public class VariableMapCollection : Collection<VariableMap>
    {
        private VariableMapContainer _container;

        #region Constructors
        /// <summary>
        /// Constructs a new VariableMapCollection.
        /// </summary>
        /// <param name="container">The container of these variable maps.</param>
        public VariableMapCollection(VariableMapContainer container)
        {
            _container = container;
        }

        /// <summary>
        /// Constructs a new deep copy of a VariableMapCollection.
        /// </summary>
        /// <param name="copy">The collection to copy.</param>
        /// <param name="container">The new container of the copied variable maps.</param>
        protected VariableMapCollection(VariableMapCollection copy, VariableMapContainer container)
            : this(container)
        {
            this.AddRange(copy.Select(v => v.Clone()));
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Deeply clones the collection.
        /// </summary>
        /// <param name="container">The new container for the cloned collection.</param>
        /// <returns>The cloned collection.</returns>
        public virtual VariableMapCollection Clone(VariableMapContainer container)
        {
            return new VariableMapCollection(this, container);
        }
        #endregion

        #region Sanity Checking
        /// <summary>
        /// Inserts a variable map into the collection, provided a map to the
        /// variable does not already exist.
        /// </summary>
        /// <param name="index">The new index of the item to insert.</param>
        /// <param name="item">The item to insert.</param>
        protected override void InsertItem(int index, VariableMap item)
        {
            foreach (var variableMap in this)
            {
                if (Equals(item.Variable, variableMap.Variable))
                    throw new ArgumentException(LS.T("Could not insert duplicate variable map '{0}' into container '{1}'", item, _container), "item");
            }

            base.InsertItem(index, item);
        }
        #endregion

        #region Search Results Cache
        private Dictionary<int, List<SearchResults>> _cache = new Dictionary<int, List<SearchResults>>();

        private T[] CacheLookup<T>(int hash, VariableStructure structureCriteria) where T : VariableMap
        {
            List<SearchResults> matches;
            if (!_cache.TryGetValue(hash, out matches))
                return null;

            var type = typeof(T);
            var match = matches.FirstOrDefault(r => r.StructureCriteria == structureCriteria && r.VariableMapType == type);
            return match == null ? null : (T[])match.VariableMaps;
        }

        #endregion

        #region Search
        private class SearchResults
        {
            public VariableStructure StructureCriteria;
            public VariableMap[] VariableMaps;
            public Type VariableMapType;
        }

        /// <summary>
        /// Finds the variable map with the given criteria.
        /// </summary>
        /// <param name="variableName">The name of the variable map to find.</param>
        /// <returns>The variable map with the given name.</returns>
        public VariableMap Find(string variableName)
        {
            return Find<VariableMap>(variableName, VariableStructure.None, true);
        }

        /// <summary>
        /// Finds the variable map with the given criteria.
        /// </summary>
        /// <param name="variableName">The name of the variable map to find.</param>
        /// <param name="structureCriteria">The structure to yield.</param>
        /// <param name="throwOnError">true if MissingVariableException should be thrown when the variable can't be found, false if null should be returned instead.</param>
        /// <returns>The variable map meeting all of the criteria and structure.</returns>
        public T Find<T>(string variableName, VariableStructure structureCriteria, bool throwOnError) where T : VariableMap
        {
            var set = FindAll<T>(structureCriteria);
            var result = set.FirstOrDefault(v => v.Variable.Member.Name == variableName);
            if (result == null && throwOnError)
                throw new MissingVariableException(LS.T("Could not find variable map '{0}' in container '{1}'", variableName, _container));

            return result;
        }

        /// <summary>
        /// Finds the variable map with the given criteria.
        /// </summary>
        /// <param name="variable">The variable backing the variable map.</param>
        /// <param name="structureCriteria">The structure to yield.</param>
        /// <returns>The variable map meeting all of the criteria and structure.</returns>
        public VariableMap Find(VariableInfo variable, VariableStructure structureCriteria)
        {
            if (variable is CompositeVariableInfo)
                return FindAllParts(variable, structureCriteria).Last();
            else
                return Find<VariableMap>(variable.Name, structureCriteria, true);
        }

        public T[] FindAll<T>() where T : VariableMap
        {
            return FindAll<T>(VariableStructure.None);
        }

        /// <summary>
        /// Finds all variable maps with the given criteria.
        /// </summary>
        /// <param name="structureCriteria">The structure to yield.</param>
        /// <returns>All variable maps meeting the type criteria and structure.</returns>
        public T[] FindAll<T>(VariableStructure structureCriteria) where T : VariableMap
        {
            lock (_cache)
            {
                var type = typeof(T);
                var hash = type.GetHashCode() ^ structureCriteria.GetHashCode();
                var cachedResults = CacheLookup<T>(hash, structureCriteria);
                if (cachedResults != null)
                    return cachedResults;

                var variableMaps = FindAllCore<T>(structureCriteria).ToArray();

                var results = new SearchResults();
                results.StructureCriteria = structureCriteria;
                results.VariableMaps = variableMaps;
                results.VariableMapType = type;
                _cache.Activate(hash).Add(results);

                return variableMaps;
            }
        }

        private IEnumerable<T> FindAllCore<T>(VariableStructure structureCriteria) where T : VariableMap
        {
            foreach (var variableMap in this)
            {
                if (variableMap is T)
                    yield return (T)variableMap;

                if (variableMap is ComponentVariableMap && (structureCriteria & VariableStructure.WithinComponents) == VariableStructure.WithinComponents)
                {
                    var componentVariableMaps = ((ComponentVariableMap)variableMap).ComponentTypeMap.VariableMaps.FindAll<T>(structureCriteria);
                    foreach (var componentVariableMap in componentVariableMaps)
                        yield return componentVariableMap;
                }
            }

            if ((structureCriteria & VariableStructure.ViewContainers) == VariableStructure.ViewContainers && _container.ViewContainer != null)
            {
                var viewVariableMaps = _container.ViewContainer.VariableMaps.FindAll<T>(structureCriteria);
                foreach (var viewVariableMap in viewVariableMaps)
                    yield return viewVariableMap;
            }

            if ((structureCriteria & VariableStructure.BaseContainers) == VariableStructure.BaseContainers && _container.BaseContainer != null)
            {
                var baseVariableMaps = _container.BaseContainer.VariableMaps.FindAll<T>(structureCriteria);
                foreach (var baseVariableMap in baseVariableMaps)
                    yield return baseVariableMap;
            }
        }
        #endregion

        #region Search Specializations
        /// <summary>
        /// Finds all instance variable maps.
        /// </summary>
        /// <returns>All variable maps that are mapped and not part of the constructor.</returns>
        public IEnumerable<VariableMap> FindAllInstanceVariables()
        {
            return this.Where(v => v.IsMapped && v.ConstructorParameterName == null);
        }

        /// <summary>
        /// Finds all parts of a potentially compound variable map with the given criteria.
        /// </summary>
        /// <param name="variable">The potentially compound variable for a compound variable map.</param>
        /// <param name="structureCriteria">The structure to yield.</param>
        /// <returns>All parts of the compound variable map with the given structure.</returns>
        public IEnumerable<VariableMap> FindAllParts(VariableInfo variable, VariableStructure structureCriteria)
        {
            if (variable is CompositeVariableInfo)
            {
                var composite = (CompositeVariableInfo)variable;

                var container = _container;
                var i = 0;

                foreach (var component in composite.Components)
                {
                    VariableMap variableMap = null;

                    foreach (var subComponent in container.VariableMaps.FindAllParts(component, structureCriteria))
                    {
                        variableMap = subComponent;
                        yield return subComponent;
                    }

                    if (variableMap is ReferenceVariableMap)
                        container = ((ReferenceVariableMap)variableMap).ReferencedTableMap;
                    else if (variableMap is ComponentVariableMap)
                        container = ((ComponentVariableMap)variableMap).ComponentTypeMap;
                    else if (variableMap is SubordinateVariableMap)
                        container = ((SubordinateVariableMap)variableMap).ReferencedTableMap;
                    else if (i + 1 != composite.Components.Length)
                        throw new MissingVariableException(LS.T("All parts of a variable list must be subordinate, reference or component types until terminator, but '{0}' is not.", component.Name));

                    i++;
                }
            }
            else
                yield return Find(variable, structureCriteria);
        }

        /// <summary>
        /// Finds the direct variable map with the given criteria.
        /// </summary>
        /// <param name="variableName">The name of the direct variable map to find.</param>
        /// <param name="structureCriteria">The structure to yield.</param>
        /// <returns>The direct variable map meeting all of the criteria and structure.</returns>
        public DirectVariableMap FindDirect(string variableName, VariableStructure structureCriteria)
        {
            return FindDirect(variableName, structureCriteria, true);
        }

        /// <summary>
        /// Finds the direct variable map with the given criteria.
        /// </summary>
        /// <param name="variableName">The name of the direct variable map to find.</param>
        /// <param name="structureCriteria">The structure to yield.</param>
        /// <param name="throwOnError">true if MissingVariableException should be thrown when the variable can't be found, false if null should be returned instead.</param>
        /// <returns>The direct variable map meeting all of the criteria and structure.</returns>
        public DirectVariableMap FindDirect(string variableName, VariableStructure structureCriteria, bool throwOnError)
        {
            return Find<DirectVariableMap>(variableName, structureCriteria, throwOnError);
        }

        /// <summary>
        /// Finds the reference variable map with the given criteria.
        /// </summary>
        /// <param name="variableName">The name of the reference variable map to find.</param>
        /// <param name="structureCriteria">The structure to yield.</param>
        /// <returns>The reference variable map meeting all of the criteria and structure.</returns>
        public ReferenceVariableMap FindReference(string variableName, VariableStructure structureCriteria)
        {
            return Find<ReferenceVariableMap>(variableName, structureCriteria, true);
        }

        public IEnumerable<VariableMap> FindAllDirectsAndComponents()
        {
            return FindAllDirectsAndComponents(VariableStructure.None);
        }

        public IEnumerable<VariableMap> FindAllDirectsAndComponents(VariableStructure structureCriteria)
        {
            foreach (var variableMap in FindAll<VariableMap>(structureCriteria))
            {
                if (variableMap is DirectVariableMap || variableMap is ComponentVariableMap)
                    yield return variableMap;
            }
        }

        public IEnumerable<VariableMap> FindAllDirectsAndSubordinates(VariableStructure structureCriteria)
        {
            foreach (var variableMap in FindAll<VariableMap>(structureCriteria))
            {
                if (variableMap is DirectVariableMap || variableMap is SubordinateVariableMap)
                    yield return variableMap;
            }
        }
        #endregion
    }

    /// <summary>
    /// Structures the set of variable maps returned.
    /// </summary>
    [Flags]
    public enum VariableStructure
    {
        /// <summary>
        /// Yields only default variable maps.
        /// </summary>
        None = 0,

        /// <summary>
        /// Yields default variable maps and those in the base container.
        /// </summary>
        BaseContainers = 1,

        /// <summary>
        /// Yields default variable maps and those in the view container.
        /// </summary>
        ViewContainers = 2,

        /// <summary>
        /// Yields default variable map and those within components.  Does not
        /// yield the actual components.
        /// </summary>
        WithinComponents = 4
    }
}
