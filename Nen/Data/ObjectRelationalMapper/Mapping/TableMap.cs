using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

using Nen.Collections;
using Nen.Data.SqlModel;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;
using Nen.Reflection;
using Nen.Runtime.Serialization;

// fixme: UnderlyingViewTableMap should be serialized as name

namespace Nen.Data.ObjectRelationalMapper.Mapping {
    /// <summary>
    /// TableMap represents the physical structure of a table.
    /// </summary>
    [XDataContract]
    public class TableMap : VariableMapContainer {
        private DataTable _dataTable;
        private LightTable _lightTable;

        #region Constructors
        /// <summary>
        /// Constructs a new TableMap.
        /// </summary>
        /// <param name="configuration">The configuration that hosts this table map.</param>
        /// <param name="originTypeMap">The origin type map of this table map.</param>
        public TableMap (MapConfiguration configuration, TypeMap originTypeMap)
            : base(configuration) {
            OriginTypeMap = originTypeMap;
            DiscriminatorValue = originTypeMap.DiscriminatorValue;
            SubTableMaps = new Collection<TableMap>();

            ImportVariableMaps(originTypeMap);
        }
        #endregion

        #region Configuration
        internal void ImportVariableMaps (TypeMap typeMap) {
            foreach (var variableMap in typeMap.VariableMaps) {
                var newVariableMap = variableMap.Clone();
                newVariableMap.PhysicalTableMap = this;

                VariableMaps.Add(newVariableMap);
            }
        }
        #endregion

        #region LightTable Building
        internal LightTable CreateLightTable () {
            if (_lightTable != null)
                return _lightTable.Clone();

            var table = new LightTable(TableName);

            // Add columns.
            foreach (var variableMap in VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents)) {
                if (!table.ContainsColumn(variableMap.ColumnName))
                {
                    var column = table.AddColumn(variableMap.ColumnName);
                    column.Tag = variableMap;
                }

                // Index the primary key.
                if (variableMap == PrimaryKey.VariableMap)
                    table.AddIndex(variableMap.ColumnName, true);
            }

            _lightTable = table;
            return table.Clone();
        }
        #endregion

        #region DataTable Building
        internal DataTable CreateDataTable () {
            if (_dataTable != null)
                return _dataTable.Clone();

            var table = new DataTable(TableName);
            table.Locale = Configuration.Locale;

            if (SchemaName != null)
                table.SetSchemaName(SchemaName);

            // Add columns.
            foreach (var variableMap in VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents))
                AddColumn(table, variableMap);

            if (PrimaryKey.VariableMap != null) {
                var primaryKeyColumn = table.Columns[PrimaryKey.VariableMap.ColumnName];
                table.PrimaryKey = new DataColumn[] { primaryKeyColumn };
                
                // Find and name this PK constraint.
                var pkConstraint = table.Constraints.OfType<Constraint>().Last();
                var pkConstraintName = "PK_" + (SchemaName == null ? "" : SchemaName + "_") + TableName;
                pkConstraint.ConstraintName = Configuration.Conventions.GetDataIdentifierName(pkConstraintName, DataIdentifierType.Constraint);

                // Configure PK attributes.
                if (BaseTableMap == null)
                {
                    if (PrimaryKey is AutoIncrementingPrimaryKey)
                        primaryKeyColumn.AutoIncrement = true;
                    else if (PrimaryKey is SequencePrimaryKey)
                    {
                        var sequence = (SequencePrimaryKey)PrimaryKey;
                        table.ExtendedProperties["Nen.Data.TableEx.PrimaryKeySequenceName"] = sequence.Name;
                        table.ExtendedProperties["Nen.Data.TableEx.PrimaryKeySequenceSchemaName"] = sequence.SchemaName;
                    }
                }
            }

            // Call initializers.
            var typeMap = OriginTypeMap;
            if (typeMap != null) {
                do {
                    var initializers = typeMap.Type.FindMembersWithAttribute<DataTableInitializerAttribute>(MemberTypes.Method, TypeEx.StaticBindingFlags, false);
                    foreach (var initializer in initializers.Cast<MethodInfo>())
                        initializer.Invoke(null, new object[] { table });

                    typeMap = typeMap.BaseTypeMap;
                } while (typeMap != null && typeMap.IsFlattened);
            }

            _dataTable = table;
            return table.Clone();
        }

        internal void ConfigureDataTableConstraints (CachingDataSet set) {
            var table = set.GetTable(TableName);

            // Add base table constraints.
            var basePhysicalTableMap = BasePhysicalTableMap;
            if (basePhysicalTableMap != null) {
                var baseTable = set.GetTable(basePhysicalTableMap.TableName);
                var constraintName = GenerateForeignKeyName(basePhysicalTableMap.GetFullTableNameForConstraint(), "PK");
                var constraint = (ForeignKeyConstraint) AddConstraint(table.Constraints, constraintName, baseTable.PrimaryKey[0], table.Columns[PrimaryKey.VariableMap.ColumnName]);
                constraint.UpdateRule = Rule.Cascade;
                constraint.DeleteRule = Rule.Cascade;
            }

            // Add reference constraints.
            foreach (var reference in VariableMaps.FindAll<ReferenceVariableMap>(VariableStructure.WithinComponents)) {
                var physicalTableMap = reference.ReferencedTableMap.GetPhysicalHierarchy().First();
                var referencedTable = set.GetTable(physicalTableMap.TableName);
                var constraintName = GenerateForeignKeyName(physicalTableMap.GetFullTableNameForConstraint(), reference.ColumnName);
                AddConstraint(table.Constraints, constraintName, referencedTable.PrimaryKey[0], table.Columns[reference.ColumnName]);
            }

            // Add enum constraints.
            foreach (var enumDirect in VariableMaps.FindAll<DirectVariableMap>(VariableStructure.WithinComponents)) {
                var type = enumDirect.PhysicalVariableType.UnwrapIfNullable();
                if (!type.IsEnum)
                    continue;

                var enumTableName = Configuration.Conventions.GetTableName(type);
                var enumTable = set.GetTable(enumTableName);
                var constraintName = GenerateForeignKeyName(enumTableName, enumDirect.ColumnName);
                AddConstraint(table.Constraints, constraintName, enumTable.PrimaryKey[0], table.Columns[enumDirect.ColumnName]);
            }
        }

        private static Constraint AddConstraint(ConstraintCollection constraints, string constraintName, DataColumn pk, DataColumn fk)
        {
            try
            {
                return constraints.Add(constraintName, pk, fk);
            }
            catch (InvalidOperationException ex)
            {
                throw new OrmException(LS.T("Error adding fk constraint '{0}' with pk '{1}.{2}' on '{3}.{4}'", constraintName, pk.Table.TableName, pk.ColumnName, fk.Table.TableName, fk.ColumnName), ex);
            }
        }

        private string GenerateForeignKeyName(string primaryKeyTableName, string columnName)
        {
            var identifierLengthLimit = Configuration.Conventions.GetIdentifierLengthLimit();
            if (identifierLengthLimit == null)
                return Configuration.Conventions.GetDataIdentifierName("FK_" + GetFullTableNameForConstraint() + "_" + primaryKeyTableName + "_" + columnName, DataIdentifierType.Constraint);

            // When the identifier space is limited, this just has to be random.
            var key = Configuration.Conventions.GetDataIdentifierName("FK_" + Guid.NewGuid().ToString().Replace("-", ""), DataIdentifierType.Constraint);
            return key.Substring(0, identifierLengthLimit.Value);
        }

        private string GetFullTableNameForConstraint () {
            if (SchemaName != null)
                return string.Format(CultureInfo.InvariantCulture, "{0}_{1}", SchemaName, TableName);
            else
                return TableName;
        }

        private void AddColumn (DataTable table, DirectVariableMap variableMap) {
            if (variableMap.ColumnName == null)
                throw new MapConfigurationException(LS.T("VariableMap '{0}.{1}' has no column name", variableMap.PhysicalTableMap, variableMap));

            if (table.Columns.Contains(variableMap.ColumnName))
                return; // Nothing to do.

            var column = table.Columns.Add(variableMap.ColumnName);

            // Subclass columns participating in a TablePerHierarchy are always nullable.
            // todo: But a check constraint can ensure they're required when necessary.

            var structureTypeMap = variableMap.DeclaringTypeMap.GetStructureTypeMap();
            if (structureTypeMap != variableMap.DeclaringTypeMap && structureTypeMap.HierarchyMapping == HierarchyMapping.TablePerHierarchy)
                column.AllowDBNull = true;
            else
                column.AllowDBNull = !variableMap.IsRequired;

            if (variableMap is ReferenceVariableMap) {
                // Add the type of primary key instead for references.
                var referenceVariableMap = (ReferenceVariableMap) variableMap;
                var key = referenceVariableMap.ReferencedTableMap.PrimaryKey;
                column.DataType = key.VariableMap.PhysicalVariableType;
            } else {
                // Add the variable member type.
                var memberType = variableMap.PhysicalVariableType;

                // Use "Money" type for the money value.
                if (variableMap.Variable.Member.DeclaringType == typeof(Money) && variableMap.Variable.Name == "Value")
                    column.SetDbType(DbType.Currency);

                column.DataType = memberType.UnwrapIfNullable();

                // Specify a sensible default for timestamps.
                if (TimeStampVariableMap == variableMap)
                    column.SetDefaultExpression(new SqlFunction("CURRENT_TIMESTAMP") { IsScalar = true });

                // Specify a MaxLength if one is given via a MaximumLengthAttribute.
                if (memberType == typeof(string)) {
                    var length = variableMap.Variable.Member.GetAttribute<System.ComponentModel.DataAnnotations.StringLengthAttribute>(true);
                    if (length != null)
                        column.MaxLength = length.MaximumLength;
                }

                if (memberType == typeof(byte[])) {
                    var length = variableMap.Variable.Member.GetAttribute<System.ComponentModel.DataAnnotations.RangeAttribute>(true);
                    if (length != null)
                        column.SetMaxBinaryLength((int)length.Maximum);
                    else
                    {
                        var stringLength = variableMap.Variable.Member.GetAttribute<System.ComponentModel.DataAnnotations.StringLengthAttribute>(true);
                        if (stringLength != null)
                            column.SetMaxBinaryLength((int)stringLength.MaximumLength);
                    }
                }
            }
        }
        #endregion

        #region Key Helpers
        /// <summary>
        /// Retrieves the primary key of an instance of this table.
        /// </summary>
        /// <param name="instance">The instance to retrieve a primary key for.</param>
        /// <returns>The primary key of the instance.</returns>
        public object GetKey (object instance) {
            if (instance == null)
                throw new ArgumentNullException("instance");

            if (PrimaryKey.VariableMap == null)
                throw new Exception(LS.T("Attempt to access primary key that does not exist on type '{0}'.", OriginTypeMap.Type));

            return PrimaryKey.VariableMap.Variable.GetValue(instance);
        }
        #endregion

        #region Structure Searching
        /// <summary>
        /// Gets the hierarchy of this table and all base tables.
        /// </summary>
        /// <returns>An enumerable containing this table and all base tables.</returns>
        public IEnumerable<TableMap> GetHierarchy () {
            for (var tableMap = this; tableMap != null; tableMap = tableMap.BaseTableMap)
                yield return tableMap;
        }

        /// <summary>
        /// Gets the hierarchy of all base tables.
        /// </summary>
        /// <returns>An enumerable containing base tables.</returns>
        public IEnumerable<TableMap> GetBaseHierarchy () {
            return GetHierarchy().Skip(1);
        }

        /// <summary>
        /// Gets the physical hierarchy of all base tables.
        /// </summary>
        /// <returns>An enumerable containing physical base tables</returns>
        public IEnumerable<TableMap> GetBasePhysicalHierarchy () {
            return GetPhysicalHierarchy().Skip(1);
        }

        public IEnumerable<TableMap> GetBaseUpdateHierarchy () {
            return GetUpdateHierarchy().Skip(1);
        }

        /// <summary>
        /// Gets the physical hierarchy of this table and all base tables.
        /// </summary>
        /// <returns>An enumerable containing this table if it is physical, and all physical base tables.</returns>
        public IEnumerable<TableMap> GetPhysicalHierarchy () {
            return GetHierarchy().Where(t => !t.ReferenceBase);
        }

        public IEnumerable<TableMap> GetUpdateHierarchy () {
            return GetPhysicalHierarchy().Where(t => !t.IsOptimizedOutForReads);
        }

        /// <summary>
        /// Retrieves the discriminated sub table map with the specified value.
        /// </summary>
        /// <param name="discriminatorValue">The discriminator value of the sub table map to retrieve.</param>
        /// <returns>The sub table map declaring the discriminator value.</returns>
        public TableMap GetDiscriminatedSubTableMap (object discriminatorValue) {
            var subTableMap = GetDiscriminatedSubTableMapCore(discriminatorValue);
            if (subTableMap == null)
               throw new QueryException(LS.T("Unable to find discriminator value '{0}' within '{1}'.", discriminatorValue, this));
            return subTableMap;
        }

        internal TableMap GetDiscriminatedSubTableMapCore (object discriminatorValue) {
            if (discriminatorValue == null)
                throw new ArgumentNullException("discriminatorValue");

            if (DiscriminatorValue != null) {
                var myConvertedDiscriminatorValue = Convert.ChangeType(DiscriminatorValue, discriminatorValue.GetType());
                if (Equals(myConvertedDiscriminatorValue, discriminatorValue))
                    return this;
            }

            foreach (var subTableMap in SubTableMaps) {
                var subDiscriminatorValue = subTableMap.DiscriminatorValue;
                if (subDiscriminatorValue == null)
                    continue;

                var convertedDiscriminatorValue = Convert.ChangeType(subDiscriminatorValue, discriminatorValue.GetType());
                if (Equals(convertedDiscriminatorValue, discriminatorValue))
                    return subTableMap;
            }

            return null;
        }

        internal bool CheckIfOptimizedOutForReads()
        {
            return ReferenceBase || (BasePhysicalTableMap != null && VariableMaps.FindAll<DirectVariableMap>().Length == 1);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the base table map for inherited tables.
        /// </summary>
        [XDataMember]
        public TableMap BaseTableMap { get; internal set; }

        /// <summary>
        /// Gets the variable maps used to specify immutable values in a constructor.
        /// </summary>
        [XDataMember]
        public VariableMap[] ConstructorParameters { get; internal set; }

        /// <summary>
        /// Gets the discriminator variable map of a table with a discriminated inheritance hierarchy.
        /// </summary>
        [XDataMember]
        public DirectVariableMap DiscriminatorVariableMap { get; internal set; }

        /// <summary>
        /// Gets the discriminator value used to determine the sub table from the root of an inheritance hierarchy.
        /// </summary>
        // fixme: make this serializable
        public object DiscriminatorValue { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether queries to the table map are
        /// ambiguous.  Queries are ambiguous when the table has sub-tables
        /// that contain data.
        /// </summary>
        [XDataMember]
        public bool IsAmbiguous { get; internal set; }

        /// <summary>
        /// Gets the type map that is the origin for this table.
        /// </summary>
        [XDataMember]
        public TypeMap OriginTypeMap { get; private set; }

        /// <summary>
        /// Gets the primary key that uniquely identifies the table.
        /// </summary>
        [XDataMember]
        public PrimaryKey PrimaryKey { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether references to this table are
        /// redirected to the base table.
        /// </summary>
        [XDataMember]
        public bool ReferenceBase { get; internal set; }

        /// <summary>
        /// Gets the sub table maps of this base table.
        /// </summary>
        [XDataMember]
        public Collection<TableMap> SubTableMaps { get; private set; }

        /// <summary>
        /// Gets the name of the database schema the physical table resides in, if any.
        /// </summary>
        [XDataMember]
        public string SchemaName { get; internal set; }

        /// <summary>
        /// Gets the physical name of the table.
        /// </summary>
        [XDataMember(XDataType.Attribute)]
        public string TableName { get; internal set; }

        /// <summary>
        /// Gets the time stamp used for optimistic locking on this table.
        /// </summary>
        [XDataMember]
        public DirectVariableMap TimeStampVariableMap { get; internal set; }

        /// <summary>
        /// Gets the underlying view table map for this derived table.
        /// </summary>
        [XDataMember]
        public TableMap UnderlyingViewTableMap { get; internal set; }

        /// <summary>
        /// Gets the base table map.
        /// </summary>
        public override VariableMapContainer BaseContainer {
            get { return BaseTableMap; }
        }

        /// <summary>
        /// Gets the base table map that physically exists and isn't optimized out.
        /// </summary>
        public TableMap BasePhysicalTableMap {
            get {
                if (BaseTableMap == null)
                    return null;

                if (BaseTableMap.ReferenceBase)
                    return BaseTableMap.BasePhysicalTableMap;

                return BaseTableMap;
            }
        }

        /// <summary>
        /// Gets an indicator on whether inserts for this table return a value.
        /// This is true for any database-generated values in the table.
        /// </summary>
        public bool InsertReturnsValue {
            get {
                if (VariableMaps.OfType<DirectVariableMap>().Any(v => v.IsDatabaseGenerated))
                    return true;

                if (BaseTableMap != null)
                    return BaseTableMap.InsertReturnsValue;

                return false;
            }
        }

        /// <summary>
        /// Gets an indicator on whether the table will be passed over for
        /// reads and updates.
        /// </summary>
        public bool IsOptimizedOutForReads { get; internal set; }

        /// <summary>
        /// Gets the underlying view table map.
        /// </summary>
        public override VariableMapContainer ViewContainer {
            get { return UnderlyingViewTableMap; }
        }

        public TableMap RootTableMap
        {
            get { return BaseTableMap == null ? this : BaseTableMap.RootTableMap; }
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves the table name surrounded by brackets.
        /// </summary>
        /// <returns>The [TableName].</returns>
        public override string ToString () {
            return "[" + TableName + "]";
        }
        #endregion
    }
}
