﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper.QueryEngine;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;
using Nen.Reflection;

namespace Nen.Data.ObjectRelationalMapper.Mapping {
    internal class ResultMapper {
        internal class CyclePlaceholder {
            public static CyclePlaceholder Value = new CyclePlaceholder();
        }

        private Queue<ObjectFill> _pendingFills;

        #region Constructors
        public ResultMapper (SequenceDataQueryResults results) {
            Results = results;
        }
        #endregion

        #region Mapping
        internal ICollection<object> Map () {
            var mapBeginTime = DateTime.Now;

            _pendingFills = new Queue<ObjectFill>();

            var originTableMap = Results.Query.GetResultTableMap();
            var originFillResults = (OriginDbFillResult) Results.FillResults.First();

            var results = originFillResults.Keys.Select(k => MapInstance(originTableMap, null, k, new Stack<VariableInfo>(), true)).ToArray();

            while (_pendingFills.Count > 0) {
                var fill = _pendingFills.Dequeue();
                fill.Execute(this);
            }

            Results.Query.Statistics.MappingTime += DateTime.Now - mapBeginTime;

            return results;
        }

        internal object MapInstance (TableMap originTableMap, VariableMap originVariableMap, object key, Stack<VariableInfo> path, bool mappingIsPossiblyAmbiguous) {
            // First return any instances that've already been created, if applicable.
            object instance;
            if (!(originTableMap.PrimaryKey is NoPrimaryKey)) {
                instance = Results.Query.Context.GetStoredObject(originTableMap, key);
                if (instance != null) {
                    if (instance == CyclePlaceholder.Value)
                        throw new CycleException(LS.T("A mapping cycle has been detected on table '{0}' with key '{1}'.", originTableMap, key));

                    Results.Query.Statistics.NumberOfContextCacheHits++;
                    return instance;
                }
            }

            // Find the most derived subclass to map this instance into.
            if (mappingIsPossiblyAmbiguous && originTableMap.SubTableMaps.Count > 0) {
                if (originTableMap.DiscriminatorVariableMap == null)
                    throw new MapException(LS.T("Attempt to instantiate ambiguous reference to {0}", originTableMap));

                var discriminatorRootTableMap = originTableMap.DiscriminatorVariableMap.PhysicalTableMap;
                var table = Results.Data[discriminatorRootTableMap.TableName];
                if (table == null)
                    throw new MapException(LS.T("Discriminator root table '{0}' from origin '{1}.{2}' for key '{3}' not found.", discriminatorRootTableMap, originTableMap, originVariableMap, key));

                var row = table.FindRowByPrimaryKey(key);
                if (row == null)
                    throw new MapException(LS.T("Discriminator root row of table '{0}' from origin '{1}.{2}' with key '{3}' not found.", discriminatorRootTableMap, originVariableMap.PhysicalTableMap, originVariableMap, key));

                var discriminatorValue = row[originTableMap.DiscriminatorVariableMap.ColumnName];
                var subTableMap = originTableMap.GetDiscriminatedSubTableMap(discriminatorValue);

                return MapInstance(subTableMap, originVariableMap, key, path, false);
            }

            // There is no need to create an instance or do anything more if
            // the reference is not loaded with the results.
            if (originVariableMap != null) {
                var advice = Results.Query.GetAdvice(path, originVariableMap);
                if (advice == AdviceRequests.JoinOnIncludeOnly && !Results.ContainsKey(originTableMap, key))
                    return null;
            }

            // Not stored and not ambiguous, so activate and map a new object.
            Results.Query.Statistics.NumberOfContextCacheMisses++;
            instance = CreateInstance(originTableMap, key, path);

            EnqueueFill(new VariableObjectFill(instance, originTableMap, key, path));

            return instance;
        }

        private object CreateInstance (TableMap tableMap, object key, Stack<VariableInfo> path) {
            object instance;

            // Use the default constructor if no other constructors are specified.
            if (tableMap.OriginTypeMap.Constructor == null)
                instance = Activator.CreateInstance(tableMap.OriginTypeMap.Type, true);
            else
                instance = CreateImmutableInstance(tableMap, key, path);

            if (!(tableMap.PrimaryKey is NoPrimaryKey) && tableMap.UnderlyingViewTableMap == null)
                Results.Query.Context.StoreObject(instance, key, tableMap);
            return instance;
        }

        private object CreateImmutableInstance (TableMap tableMap, object key, Stack<VariableInfo> path) {
            // Store a placeholder to be detected in case of a cycle.
            if (!(tableMap.PrimaryKey is NoPrimaryKey) && tableMap.UnderlyingViewTableMap == null)
                Results.Query.Context.StorePlaceholder(CyclePlaceholder.Value, key, tableMap);

            var originTypeMap = tableMap.OriginTypeMap;

            var tableMapRows = Results.EachRowTableMap(tableMap, key).ToArray();
            var variableMapRows = tableMap.ConstructorParameters.ToDictionary(v => v, v => tableMapRows.First(p => p.Item2 == v.PhysicalTableMap));
            var parameters = tableMap.ConstructorParameters.Select(v => ConvertEx.ChangeWeakType(v.GetValueForInstance(variableMapRows[v].Item1, this, path), v.Variable.MemberType)).ToArray();

            return tableMap.OriginTypeMap.Constructor.Invoke(parameters);
        }

        internal void EnqueueFill (ObjectFill fill) {
            _pendingFills.Enqueue(fill);
        }
        #endregion

        #region Accessors
        public SequenceDataQueryResults Results { get; private set; }
        #endregion
    }
}
