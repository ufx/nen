using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.QueryEngine;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;
using Nen.Reflection;
using Nen.Runtime.Serialization;

namespace Nen.Data.ObjectRelationalMapper.Mapping {
    /// <summary>
    /// Contains mapping information for one variable on a type.
    /// </summary>
    [XDataContract]
    public abstract class VariableMap {
        #region Constructor
        /// <summary>
        /// Constructs a new VariableMap.
        /// </summary>
        /// <param name="variable">The variable this map is based on.</param>
        /// <param name="physicalVariableType">The physical type this variable uses, inside any wrappers.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        protected VariableMap (VariableInfo variable, Type physicalVariableType, TypeMap declaringTypeMap) {
            DeclaringTypeMap = declaringTypeMap;
            Path = new Stack<VariableInfo>();
            Variable = variable;
            PhysicalVariableType = physicalVariableType;

            IsMapped = true;
        }

        /// <summary>
        /// Constructs a new copy of a VariableMap.
        /// </summary>
        /// <param name="copy">The variable map to copy.</param>
        protected VariableMap (VariableMap copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            Advice = copy.Advice;
            Computation = copy.Computation;
            ConstructorParameterName = copy.ConstructorParameterName;
            DeclaringTypeMap = copy.DeclaringTypeMap;
            FromExpression = copy.FromExpression;
            IsMapped = copy.IsMapped;
            IsRequired = copy.IsRequired;
            Path = new Stack<VariableInfo>(copy.Path.Reverse());
            PhysicalTableMap = copy.PhysicalTableMap;
            Variable = copy.Variable;
            PhysicalVariableType = copy.PhysicalVariableType;
            DbType = copy.DbType;
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the variable map.
        /// </summary>
        /// <returns>The cloned variable map.</returns>
        public abstract VariableMap Clone ();
        #endregion

        #region Value Access
        internal abstract object GetValueForInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path);

        protected virtual object ConvertValueForInstance(object value)
        {
            return value;
        }

        internal virtual void SetValue (object obj, object value) {
            try {
                Variable.SetValue(obj, value);
            } catch (System.Threading.ThreadAbortException) {
                throw;
            } catch (Exception ex) {
                throw new MapException(LS.T("Error setting variable {0}.{1} value", Variable.Member.DeclaringType, Variable), ex);
            }
        }

        internal void StoreValueOnInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path, object obj) {
            if (ConstructorParameterName != null)
                throw new MapException(LS.T("Invalid attempt to set immutable variable '{0}'.", this));

            var value = GetValueForInstance(row, mapper, path);
            value = ConvertValueForInstance(value);
            if (value == null)
                return;

            try {
                SetValue(obj, value);
            } catch (ArgumentException ex) {
                throw new MapException(LS.T("Can't store value on member '{0}' of type '{1}'.", Variable, PhysicalVariableType), ex);
            }
        }

        internal abstract void StoreValueOnRow (object obj, LightRow row, ChangeSet changeSet);

        internal object ConvertDatabaseValue(object value)
        {
            return value == DBNull.Value ? null : ConvertDatabaseValueCore(value);
        }

        protected internal abstract object ConvertDatabaseValueCore(object value);
        #endregion

        #region Structure
        /// <summary>
        /// Determines if this variable map forms a circular reference with the
        /// specified variable map.
        /// </summary>
        /// <param name="variableMap">The variable map to check circular references with.</param>
        /// <returns>true if a circular reference is formed, false if otherwise.</returns>
        public bool IsCircularReference (VariableMap variableMap) {
            if (InnerTypeMap == null || variableMap.InnerTypeMap == null)
                return false;

            return InnerTypeMap == variableMap.DeclaringTypeMap && variableMap.InnerTypeMap == DeclaringTypeMap;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the applied advice requests.
        /// </summary>
        [XDataMember]
        public AdviceRequests Advice { get; internal set; }

        /// <summary>
        /// Gets the computation to derive the value of this variable map from.
        /// </summary>
        public ComputedAttribute Computation { get; internal set; }

        /// <summary>
        /// Gets the constructor parameter name of this immutable variable map.
        /// </summary>
        [XDataMember]
        public string ConstructorParameterName { get; internal set; }

        /// <summary>
        /// Gets the type map where this variable map is declared.
        /// </summary>
        [XDataMember]
        public TypeMap DeclaringTypeMap { get; internal set; }

        /// <summary>
        /// Gets or sets the expression to derive the value of this view variable map from.
        /// </summary>
        [XDataMember]
        internal OrmExpression FromExpression { get; set; }

        /// <summary>
        /// Gets an indicator on whether the variable map is mapped to a physical variable on any resulting objects.
        /// </summary>
        [XDataMember]
        public bool IsMapped { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether the variable must be present when inserting new rows.
        /// </summary>
        [XDataMember]
        public bool IsRequired { get; internal set; }

        /// <summary>
        /// Gets the path taken to find this component variable map.
        /// </summary>
        public Stack<VariableInfo> Path { get; internal set; }

        /// <summary>
        /// Gets the physical table map this variable map lives in.
        /// </summary>
        [XDataMember]
        public TableMap PhysicalTableMap { get; internal set; }

        /// <summary>
        /// Gets the variable to map.
        /// </summary>
        [XDataMember]
        public VariableInfo Variable { get; internal set; }

        [XDataMember]
        public Type PhysicalVariableType { get; internal set; }

        [XDataMember]
        public DbType? DbType { get; protected internal set; }

        /// <summary>
        /// Gets the inner type map for indirect variables.
        /// </summary>
        public virtual TypeMap InnerTypeMap {
            get { return null; }
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves the variable string.
        /// </summary>
        /// <returns>The variable string.</returns>
        public override string ToString () {
            return Variable.ToString();
        }
        #endregion
    }

    #region Direct Variable Maps
    /// <summary>
    /// Represents a variable mapping that is stored directly on the declaring
    /// type table as a column.
    /// </summary>
    [XDataContract]
    public abstract class DirectVariableMap : VariableMap {
        #region Constructors
        /// <summary>
        /// Constructs a new DirectVariableMap.
        /// </summary>
        /// <param name="direct">The direct variable this map is based on.</param>
        /// <param name="physicalVariableType">The physical type this variable uses, inside any wrappers.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        protected DirectVariableMap (VariableInfo direct, Type physicalVariableType, TypeMap declaringTypeMap)
            : base(direct, physicalVariableType, declaringTypeMap) {
        }

        /// <summary>
        /// Constructs a new copy of a DirectVariableMap.
        /// </summary>
        /// <param name="copy">The variable map to copy.</param>
        protected DirectVariableMap (DirectVariableMap copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            ColumnName = copy.ColumnName;
            IsDatabaseGenerated = copy.IsDatabaseGenerated;
        }
        #endregion

        #region Queries
        internal QueryExpression CreateKeyQuery (object[] keys, DataContext context) {
            return CreateKeyQuery(keys, context, PhysicalTableMap);
        }

        internal QueryExpression CreateKeyQuery (object[] keys, DataContext context, TableMap tableMap) {
            var query = new QueryExpression(context, tableMap);

            var left = new VariableExpression(Variable, query);

            if (keys.Length == 1)
                query.WhereCriteria = new BinaryOperatorExpression(left, BinaryOperatorType.Equal, new LiteralExpression(keys[0]));
            else
                query.WhereCriteria = new BinaryOperatorExpression(left, BinaryOperatorType.In, new LiteralExpression(keys));

            return query;
        }
        #endregion

        #region Value Access
        internal override void StoreValueOnRow (object obj, LightRow row, ChangeSet changeSet) {
            if (obj == null)
                row[ColumnName] = null;
            else
                row[ColumnName] = GetValueForDatabase(obj, changeSet);
        }

        private object GetValueForDatabase (object obj, ChangeSet changeSet) {
            return GetValueForDatabaseCore(obj, changeSet);
        }

        protected internal abstract object GetValueForDatabaseCore (object obj, ChangeSet changeSet);
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the column name.
        /// </summary>
        [XDataMember]
        public string ColumnName { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether the variable is a discriminator for sub types.
        /// </summary>
        [XDataMember]
        public bool IsDiscriminator { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether the variable is automatically generated by the database.
        /// </summary>
        [XDataMember]
        public bool IsDatabaseGenerated { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether the variable is the primary key of this table.
        /// </summary>
        public bool IsPrimaryKey {
            get { return PhysicalTableMap.PrimaryKey.VariableMap == this; }
        }
        #endregion
    }

    /// <summary>
    /// Represents a mapping for a straightforward primitive variable.
    /// This mapping usually handles things like integers, strings, DateTimes,
    /// etc.
    /// </summary>
    [XDataContract]
    public class PrimitiveVariableMap : DirectVariableMap {
        #region Constructors
        /// <summary>
        /// Constructs a new PrimitiveVariableMap.
        /// </summary>
        /// <param name="primitive">The primitive variable this map is based on.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        public PrimitiveVariableMap (VariableInfo primitive, Type physicalVariableType, TypeMap declaringTypeMap)
            : base(primitive, physicalVariableType, declaringTypeMap) {
            DbType = DbTypeEx.GetDbType(physicalVariableType);
        }

        /// <summary>
        /// Constructs a new copy of a PrimitiveVariableMap.
        /// </summary>
        /// <param name="copy">The variable map to copy.</param>
        protected PrimitiveVariableMap (PrimitiveVariableMap copy)
            : base(copy) {
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the primitive variable map.
        /// </summary>
        /// <returns>The cloned primitive variable map.</returns>
        public override VariableMap Clone () {
            return new PrimitiveVariableMap(this);
        }
        #endregion

        #region VariableMap Value Access
        protected internal override object GetValueForDatabaseCore (object obj, ChangeSet changeSet) {
            // Ensure we receive the key used by the changeset.
            var primaryKey = PhysicalTableMap.PrimaryKey;
            if (primaryKey.IsDatabaseGenerated && primaryKey.VariableMap == this)
                return changeSet.GetKey(obj);

            // Retrieve non-key primitive value.
            return Variable.GetValue(obj);
        }

        internal override object GetValueForInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path) {
            return row[ColumnName];
        }

        protected internal override object ConvertDatabaseValueCore(object value)
        {
            return ConvertEx.ChangeWeakType(value, PhysicalVariableType);
        }
        #endregion
    }

    /// <summary>
    /// Represents a mapping for a direct variable reference to another table.
    /// This mapping usually handles other user-defined classes.
    /// </summary>
    [XDataContract]
    public class ReferenceVariableMap : DirectVariableMap {
        #region Constructors
        /// <summary>
        /// Constructs a new reference variable map.
        /// </summary>
        /// <param name="reference">The reference variable this map is based on.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        public ReferenceVariableMap (VariableInfo reference, Type physicalVariableType, TypeMap declaringTypeMap)
            : base(reference, physicalVariableType, declaringTypeMap) {
        }

        /// <summary>
        /// Constructs a new copy of a ReferenceVariableMap.
        /// </summary>
        /// <param name="copy">The variable map to copy.</param>
        protected ReferenceVariableMap (ReferenceVariableMap copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            ReferencedTableMap = copy.ReferencedTableMap;
            ReferencedTypeMap = copy.ReferencedTypeMap;
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the reference variable map.
        /// </summary>
        /// <returns>The cloned reference variable map.</returns>
        public override VariableMap Clone () {
            return new ReferenceVariableMap(this);
        }
        #endregion

        #region VariableMap Value Access
        protected internal override object GetValueForDatabaseCore (object obj, ChangeSet changeSet) {
            var reference = Variable.GetValue(obj);
            return reference == null ? null : changeSet.GetKey(reference);
        }

        internal override object GetValueForInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path) {
            var key = row[ColumnName];
            if (key == null)
                return null;

            using (path.PushScope(Variable))
                return mapper.MapInstance(ReferencedTableMap, this, key, path, true);
        }

        protected internal override object ConvertDatabaseValueCore(object value)
        {
            return ReferencedTableMap.PrimaryKey.VariableMap.ConvertDatabaseValue(value);
        }
        #endregion

        #region Accessors
        private TableMap _referencedTableMap;

        /// <summary>
        /// Gets the table map referenced by this variable map.
        /// </summary>
        [XDataMember]
        public TableMap ReferencedTableMap
        {
            get { return _referencedTableMap; }
            set { _referencedTableMap = value; }
        }

        /// <summary>
        /// Gets the type map referenced by this variable map.
        /// </summary>
        [XDataMember]
        public TypeMap ReferencedTypeMap { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether this variable map references the type it is declared on.
        /// </summary>
        public bool IsSelfReference {
            get { return DeclaringTypeMap.Type.IsAssignableFrom(ReferencedTypeMap.Type); }
        }

        /// <summary>
        /// Gets the referenced type map.
        /// </summary>
        public override TypeMap InnerTypeMap {
            get { return ReferencedTypeMap; }
        }
        #endregion
    }

    /// <summary>
    /// Represents a mapping for a lazy direct variable reference to another
    /// table.  Only the keys of a lazy reference are retrieved by default.
    /// Lazy references must be initialized with a backing DataRef field.
    /// </summary>
    [XDataContract]
    public class LazyReferenceVariableMap : ReferenceVariableMap {
        #region Constructors
        /// <summary>
        /// Constructs a new LazyReferenceVariableMap.
        /// </summary>
        /// <param name="reference">The reference variable this map is based on.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        /// <param name="refField">The backing DataRef&lt;&gt; field where the actual values are stored.</param>
        public LazyReferenceVariableMap (VariableInfo reference, VariableInfo refField, TypeMap declaringTypeMap)
            : base(reference, GetPhysicalVariableType(reference, refField), declaringTypeMap) {
            RefField = refField;
        }

        /// <summary>
        /// Constructs a new copy of a LazyReferenceVariableMap.
        /// </summary>
        /// <param name="copy">The variable map to copy.</param>
        protected LazyReferenceVariableMap (LazyReferenceVariableMap copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            RefField = copy.RefField;
        }

        private static Type GetPhysicalVariableType (VariableInfo reference, VariableInfo refField) {
            if (refField == null)
                return reference.MemberType.GetGenericArguments()[0];
            else
                return reference.MemberType;
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the lazy reference variable map.
        /// </summary>
        /// <returns>The cloned lazy reference variable map.</returns>
        public override VariableMap Clone () {
            return new LazyReferenceVariableMap(this);
        }
        #endregion

        #region VariableMap Value Access
        protected internal override object GetValueForDatabaseCore (object obj, ChangeSet changeSet) {
            IDataRef lazyReference;
            if (RefField == null)
                lazyReference = (IDataRef) Variable.GetValue(obj);
            else
                lazyReference = (IDataRef) RefField.GetValue(obj);

            if (lazyReference == null)
                return null;

            if (lazyReference.IsResolved) {
                var reference = lazyReference.Resolve();
                if (reference != null)
                    return changeSet.GetKey(reference);
            } else if (lazyReference.Key != null)
                return lazyReference.Key;

            return null;
        }

        internal override object GetValueForInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path) {
            // Note, return the value directly for immutable types, as it is
            // always implicitly included.

            var key = row[ColumnName];
            if (key == null)
            {
                if (ConstructorParameterName == null)
                    return DataRef.Create(ReferencedTypeMap.Type, null);
                else
                    return null;
            }

            if (mapper.Results.ContainsKey(ReferencedTableMap, key)) {
                using (path.PushScope(Variable)) {
                    var value = mapper.MapInstance(ReferencedTableMap, this, key, path, true);
                    if (ConstructorParameterName == null)
                        return DataRef.Create(ReferencedTypeMap.Type, value);
                    else
                        return value;
                }
            }

            return DataRef.Create(ReferencedTypeMap.Type, key, DeclaringTypeMap.Configuration);
        }

        internal override void SetValue (object obj, object value) {
            if (RefField == null)
                Variable.SetValue(obj, value);
            else
                RefField.SetValue(obj, value);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the backing DataRef field where the actual values are stored.
        /// </summary>
        [XDataMember]
        public VariableInfo RefField { get; internal set; }
        #endregion
    }

    /// <summary>
    /// Represents a mapping for a lazy direct variable primitive. Nothing
    /// is retrieved by default.  Lazy primitives must be initialized with a
    /// backing DataRef field.
    /// </summary>
    [XDataContract]
    public class LazyPrimitiveVariableMap : PrimitiveVariableMap {
        #region Constructors
        /// <summary>
        /// Constructs a new LazyPrimitiveVariableMap.
        /// </summary>
        /// <param name="primitive">The lazy primitive variable this map is based on.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        /// /// <param name="dataRefField">The backing DataRef&lt;&gt; field where the actual values are stored.</param>
        public LazyPrimitiveVariableMap (VariableInfo primitive, VariableInfo refField, TypeMap declaringTypeMap)
            : base(primitive, GetPhysicalVariableType(primitive, refField), declaringTypeMap) {
            RefField = refField;
        }

        /// <summary>
        /// Constructs a new copy of a LazyPrimitiveVariableMap.
        /// </summary>
        /// <param name="copy">The lazy variable map to copy.</param>
        protected LazyPrimitiveVariableMap (LazyPrimitiveVariableMap copy)
            : base(copy) {
            RefField = copy.RefField;
        }

        private static Type GetPhysicalVariableType (VariableInfo primitive, VariableInfo refField) {
            if (refField == null)
                return primitive.MemberType.GetGenericArguments()[0];
            else
                return primitive.MemberType;
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the lazy primitive variable map.
        /// </summary>
        /// <returns>The cloned lazy primitive variable map.</returns>
        public override VariableMap Clone () {
            return new LazyPrimitiveVariableMap(this);
        }
        #endregion

        #region VariableMap Value Access
        protected internal override object GetValueForDatabaseCore (object obj, ChangeSet changeSet) {
            throw new NotImplementedException();
        }

        internal override void StoreValueOnRow (object obj, LightRow row, ChangeSet changeSet) {
            if (obj == null)
                row[ColumnName] = null;
            else {
                IDataRef lazyPrimitive;
                if (RefField == null)
                    lazyPrimitive = (IDataRef) Variable.GetValue(obj);
                else
                    lazyPrimitive = (IDataRef) RefField.GetValue(obj);

                if (lazyPrimitive == null)
                    row[ColumnName] = null;
                else {
                    if (lazyPrimitive.IsResolved)
                        row[ColumnName] = lazyPrimitive.Resolve();
                    else {
                        row[ColumnName] = null;
                        row.SetColumnError(ColumnName, "Unresolved");
                    }
                }
            }
        }

        internal override object GetValueForInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path) {
            // Short-circuit everything when this lazy primitive is a part of
            // an immutable type.
            if (ConstructorParameterName != null)
                return row[ColumnName];

            using (path.PushScope(Variable)) {
                var advice = mapper.Results.Query.GetAdvice(path, this);
                if (advice == AdviceRequests.Include) {
                    var value = row[ColumnName];
                    return DataRef.Create(PhysicalVariableType, value);
                } else {
                    var primaryKey = row[PhysicalTableMap.PrimaryKey.VariableMap.ColumnName];
                    return DataRef.Create(PhysicalVariableType, primaryKey, DeclaringTypeMap.Configuration, this);
                }
            }
        }

        internal override void SetValue (object obj, object value) {
            if (RefField == null)
                Variable.SetValue(obj, value);
            else
                RefField.SetValue(obj, value);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the backing DataRef field where the actual values are stored.
        /// </summary>
        [XDataMember]
        public VariableInfo RefField { get; internal set; }
        #endregion
    }
    #endregion

    /// <summary>
    /// Represents a mapping for a collection variable reference to another
    /// table.  This table must hold a reference back to the table where the
    /// collection is declared.
    /// </summary>
    [XDataContract]
    public class CollectionVariableMap : VariableMap {
        #region Constructors
        /// <summary>
        /// Constructs a new CollectionVariableMap.
        /// </summary>
        /// <param name="collection">The collection variable this map is based on.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        public CollectionVariableMap (VariableInfo collection, TypeMap declaringTypeMap)
            : base(collection, collection.MemberType, declaringTypeMap) {

            CollectionArgumentType = GetCollectionArgumentType(collection.MemberType);
            OrderBy = new Collection<OrderExpression>();
        }

        /// <summary>
        /// Constructs a new copy of a CollectionVariableMap.
        /// </summary>
        /// <param name="copy">The variable map to copy.</param>
        protected CollectionVariableMap (CollectionVariableMap copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            CollectionArgumentType = copy.CollectionArgumentType;
            CollectionTableMap = copy.CollectionTableMap;
            CollectionTypeMap = copy.CollectionTypeMap;
            DeclaringVariable = copy.DeclaringVariable;
            Filter = copy.Filter;
            KeyVariableMap = copy.KeyVariableMap;
            OrderBy = new Collection<OrderExpression>(copy.OrderBy);
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the collection variable map.
        /// </summary>
        /// <returns>The cloned collection variable map.</returns>
        public override VariableMap Clone () {
            return new CollectionVariableMap(this);
        }
        #endregion

        #region Collection Management
        private static Type GetCollectionArgumentType (Type collectionType) {
            var baseCollectionType = collectionType.GetGenericTypeInstance(typeof(Collection<>));
            return baseCollectionType.GetGenericArguments()[0];
        }
        #endregion

        #region VariableMap Value Access
        internal override void StoreValueOnRow (object obj, LightRow row, ChangeSet changeSet) {
            throw new NotImplementedException("Database values of collections can never be stored on a row.");
        }

        internal override object GetValueForInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path) {
            using (path.PushScope(Variable)) {
                var advice = mapper.Results.Query.GetAdvice(path, this);
				var physicalTableMap = PhysicalTableMap;

				if (physicalTableMap.IsOptimizedOutForReads)
					physicalTableMap = physicalTableMap.GetUpdateHierarchy().First();

                var primaryKey = row[physicalTableMap.PrimaryKey.VariableMap.ColumnName];

                object items;

                if (advice == AdviceRequests.Include) {
                    items = ListEx.CreateList(CollectionTypeMap.Type);
                    mapper.EnqueueFill(new EagerCollectionObjectFill(this, (System.Collections.IList) items, primaryKey, path));
                } else {
                    var obj = new VariableFuture<object>();
                    items = LazyCollection.Create(CollectionArgumentType, obj, this);
                    mapper.EnqueueFill(new StoredObjectFill(obj, PhysicalTableMap, this, primaryKey, path));
                }

                var instance = DynamicActivator.CreateInstance(PhysicalVariableType, items.GetType(), items);

                if (DeclaringVariable != null)
                    mapper.EnqueueFill(new CollectionDeclaringVariableFill(this, (System.Collections.ICollection) instance, primaryKey));

                return instance;
            }
        }

        protected internal override object ConvertDatabaseValueCore(object value)
        {
            // This wouldn't have anything to return.
            throw new InvalidOperationException();
        }
        #endregion

        #region Queries
        internal QueryExpression CreateCollectionQuery (DataContext context) {
            var query = new QueryExpression(context, CollectionTableMap);
            query.Advice.Add(new AdviceExpression(AdviceRequests.DeferJoin, new VariableExpression(KeyVariableMap.Variable, query)));
            query.OrderBy.AddRange(OrderBy);
            query.WhereCriteria = Filter;
            return query;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the generic type argument of the collection, which is the type
        /// the collection holds.
        /// </summary>
        [XDataMember]
        public Type CollectionArgumentType { get; private set; }

        /// <summary>
        /// Gets the table map referenced by this variable map.
        /// </summary>
        [XDataMember]
        public TableMap CollectionTableMap { get; internal set; }

        /// <summary>
        /// Gets the type map referenced by this variable map.
        /// </summary>
        [XDataMember]
        public TypeMap CollectionTypeMap { get; internal set; }

        [XDataMember]
        internal OperatorExpression Filter { get; set; }

        /// <summary>
        /// Gets a reference to the declaring variable on the actual collection
        /// object.  This variable is usually used when parent/child
        /// relationships are enforced by the collection.
        /// </summary>
        [XDataMember]
        public VariableInfo DeclaringVariable { get; internal set; }

        /// <summary>
        /// Gets the back reference key variable map.  This variable is the
        /// foreign key queried to find all the elements of the collection.
        /// </summary>
        [XDataMember]
        public ReferenceVariableMap KeyVariableMap { get; internal set; }

        [XDataMember]
        internal Collection<OrderExpression> OrderBy { get; set; }

        /// <summary>
        /// Gets the table map of the generic type argument of the collection.
        /// </summary>
        public TableMap CollectionArgumentTableMap {
            get { return DeclaringTypeMap.Configuration.TableMaps[CollectionArgumentType]; }
        }

        /// <summary>
        /// Gets the collection type map.
        /// </summary>
        public override TypeMap InnerTypeMap {
            get { return CollectionTypeMap; }
        }
        #endregion
    }

    /// <summary>
    /// Represents a mapping for a component variable.  Because components
    /// exist separately on each table they're declared on, this map is cloned
    /// for every table.
    /// </summary>
    [XDataContract]
    public class ComponentVariableMap : VariableMap {
        #region Constructors
        /// <summary>
        /// Constructs a new ComponentVariableMap.
        /// </summary>
        /// <param name="component">The component variable this map is based on.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        public ComponentVariableMap (VariableInfo component, TypeMap declaringTypeMap)
            : base(component, component.MemberType, declaringTypeMap) {
        }

        /// <summary>
        /// Constructs a new copy of a ComponentVariableMap.
        /// </summary>
        /// <param name="copy">The variable map to copy.</param>
        protected ComponentVariableMap (ComponentVariableMap copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            ComponentTypeMap = copy.ComponentTypeMap.Clone();
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the component variable map.
        /// </summary>
        /// <returns>The cloned component variable map.</returns>
        public override VariableMap Clone () {
            return new ComponentVariableMap(this);
        }
        #endregion

        #region VariableMap Value Access
        internal override void StoreValueOnRow (object obj, LightRow row, ChangeSet changeSet) {
            var component = obj == null ? null : Variable.GetValue(obj);

            foreach (var variableMap in ComponentTypeMap.VariableMaps.FindAllDirectsAndComponents())
                variableMap.StoreValueOnRow(component, row, changeSet);
        }

        internal override object GetValueForInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path) {
            var component = Activator.CreateInstance(ComponentTypeMap.Type);

            using (path.PushScope(Variable)) {
                foreach (var variableMap in ComponentTypeMap.VariableMaps.FindAllInstanceVariables())
                    variableMap.StoreValueOnInstance(row, mapper, path, component);
            }

            return component;
        }

        protected internal override object ConvertDatabaseValueCore(object value)
        {
            // This wouldn't have anything to return.
            throw new InvalidOperationException();
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the type map referenced by this variable map.
        /// </summary>
        [XDataMember]
        public TypeMap ComponentTypeMap { get; internal set; }

        /// <summary>
        /// Gets the component type map.
        /// </summary>
        public override TypeMap InnerTypeMap {
            get { return ComponentTypeMap; }
        }
        #endregion
    }

    /// <summary>
    /// Represents a mapping for an indirect variable reference to another
    /// table.  Subordinates do not exist on the tables they are declared upon.
    /// </summary>
    [XDataContract]
    public class SubordinateVariableMap : VariableMap {
        #region Constructors
        /// <summary>
        /// Constructs a new SubordinateVariableMap.
        /// </summary>
        /// <param name="reference">The reference variable this map is based on.</param>
        /// <param name="declaringTypeMap">The type map where this variable map is declared.</param>
        public SubordinateVariableMap (VariableInfo reference, TypeMap declaringTypeMap)
            : base(reference, reference.MemberType, declaringTypeMap) {
        }

        /// <summary>
        /// Constructs a new copy of a SubordinateVariableMap.
        /// </summary>
        /// <param name="copy">The variable map to copy.</param>
        protected SubordinateVariableMap (SubordinateVariableMap copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            ReferencedTableMap = copy.ReferencedTableMap;
            ReferencedTypeMap = copy.ReferencedTypeMap;
            KeyVariableMap = copy.KeyVariableMap;
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the subordinate variable map.
        /// </summary>
        /// <returns>The cloned subordinate variable map.</returns>
        public override VariableMap Clone () {
            return new SubordinateVariableMap(this);
        }
        #endregion

        #region VariableMap Value Access
        internal override void StoreValueOnRow (object obj, LightRow row, ChangeSet changeSet) {
            throw new NotImplementedException("Database values of subordinates can never be stored.");
        }

        internal override object GetValueForInstance (LightRow row, ResultMapper mapper, Stack<VariableInfo> path) {
            var data = mapper.Results.Data;
            if (!data.ContainsTable(ReferencedTableMap.TableName))
                return null;

            var rowKey = row[PhysicalTableMap.PrimaryKey.VariableMap.ColumnName];

            // fixme: optimize with LightTable indexes
            var primaryTable = data[ReferencedTableMap.TableName];
            var primaryRow = primaryTable.Rows.FirstOrDefault(r => Equals(rowKey, r[KeyVariableMap.ColumnName]));
            if (primaryRow == null)
                return null;

            var primaryRowKey = primaryRow[ReferencedTableMap.PrimaryKey.VariableMap.ColumnName];

            using (path.PushScope(Variable))
                return mapper.MapInstance(ReferencedTableMap, this, primaryRowKey, path, true);
        }

        protected internal override object ConvertDatabaseValueCore(object value)
        {
            return ReferencedTableMap.PrimaryKey.VariableMap.ConvertDatabaseValue(value);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the table map referenced by this variable map.
        /// </summary>
        [XDataMember]
        public TableMap ReferencedTableMap { get; internal set; }

        /// <summary>
        /// Gets the type map referenced by this variable map.
        /// </summary>
        [XDataMember]
        public TypeMap ReferencedTypeMap { get; internal set; }

        /// <summary>
        /// Gets the physical key variable map.  This variable is the foreign
        /// key back to the declaring object on the table referenced by the
        /// subordinate.
        /// </summary>
        [XDataMember]
        public ReferenceVariableMap KeyVariableMap { get; internal set; }

        /// <summary>
        /// Gets the referenced type map.
        /// </summary>
        public override TypeMap InnerTypeMap {
            get { return ReferencedTypeMap; }
        }
        #endregion
    }
}
