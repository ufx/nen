using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

using Nen.Runtime.Serialization;
using Nen.Caching;

namespace Nen.Data.ObjectRelationalMapper.Mapping {
    /// <summary>
    /// TypeMap represents the object structure of a type.
    /// </summary>
    [XDataContract]
    public class TypeMap : VariableMapContainer, Nen.Runtime.Serialization.ICustomSerializable {
        #region Constructor
        /// <summary>
        /// Constructs a new TypeMap.
        /// </summary>
        /// <param name="configuration">The configuration that hosts this type map.</param>
        /// <param name="type">The type to map.</param>
        public TypeMap (MapConfiguration configuration, Type type)
            : base(configuration) {
            Type = type;
        }

        /// <summary>
        /// Constructs a new copy of a TypeMap.
        /// </summary>
        /// <param name="copy">The type map to copy.</param>
        protected TypeMap (TypeMap copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            BaseTypeMap = copy.BaseTypeMap;
            CacheOptions = copy.CacheOptions;
            Constructor = copy.Constructor;
            ConstructorParameters = copy.ConstructorParameters;
            DiscriminatorValue = copy.DiscriminatorValue;
            DiscriminatorVariableMap = copy.DiscriminatorVariableMap;
            IsComponent = copy.IsComponent;
            IsFlattened = copy.IsFlattened;
            PhysicalTableMap = copy.PhysicalTableMap;
            Type = copy.Type;
            UnderlyingViewTypeMap = copy.UnderlyingViewTypeMap;
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones the type map.
        /// </summary>
        /// <returns>The cloned type map.</returns>
        public virtual TypeMap Clone () {
            return new TypeMap(this);
        }
        #endregion

        #region Structure Searching
        /// <summary>
        /// Gets the hierarchy of this type map and all base type maps.
        /// </summary>
        /// <returns>An enumerable containing this table and all base type maps.</returns>
        public IEnumerable<TypeMap> GetHierarchy () {
            for (var typeMap = this; typeMap != null; typeMap = typeMap.BaseTypeMap)
                yield return typeMap;
        }

        /// <summary>
        /// Gets the hierarchy of all base type maps.
        /// </summary>
        /// <returns>An enumerable containing base type maps.</returns>
        public IEnumerable<TypeMap> GetBaseHierarchy () {
            return GetHierarchy().Skip(1);
        }

        public TypeMap GetStructureTypeMap () {
            return GetHierarchy().First(t => t.HierarchyMapping != HierarchyMapping.Inherit);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the base type map for inherited types.
        /// </summary>
        public TypeMap BaseTypeMap { get; internal set; }

        /// <summary>
        /// Gets the cache options specified for this type.
        /// </summary>
        public CacheOptions CacheOptions { get; internal set; }

        /// <summary>
        /// Gets the constructor used to create types with immutable values.
        /// </summary>
        public ConstructorInfo Constructor { get; internal set; }

        /// <summary>
        /// Gets the variable maps used to specify immutable values in a constructor.
        /// </summary>
        public VariableMap[] ConstructorParameters { get; internal set; }

        /// <summary>
        /// Gets the discriminator value used to determine the sub type from the root of an inheritance hierarchy.
        /// </summary>
        public object DiscriminatorValue { get; internal set; }

        /// <summary>
        /// Gets the discriminator variable map of a table with a discriminated inheritance hierarchy.
        /// </summary>
        public DirectVariableMap DiscriminatorVariableMap { get; internal set; }

        /// <summary>
        /// Gets the style of inheritance hierarchy mapping to use.
        /// </summary>
        public HierarchyMapping HierarchyMapping { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether the type map is a component of other type maps.
        /// </summary>
        public bool IsComponent { get; internal set; }

        /// <summary>
        /// Gets an indicator on whether the type map is flattened down into inheriting type maps.
        /// </summary>
        public bool IsFlattened { get; internal set; }

        /// <summary>
        /// Gets the physical table map derived from this type map.
        /// </summary>
        public TableMap PhysicalTableMap { get; internal set; }

        /// <summary>
        /// Gets the type to map.
        /// </summary>
        public Type Type { get; private set; }

        /// <summary>
        /// Gets the underlying view type map for this derived type.
        /// </summary>
        public TypeMap UnderlyingViewTypeMap { get; internal set; }

        /// <summary>
        /// Gets the base type map.
        /// </summary>
        public override VariableMapContainer BaseContainer {
            get { return BaseTypeMap; }
        }

        /// <summary>
        /// Gets an indicator on whether the type map has its own physical form.
        /// </summary>
        public bool IsPhysical {
            get { return !IsComponent && !IsFlattened; }
        }

        /// <summary>
        /// Gets the underlying view type map.
        /// </summary>
        public override VariableMapContainer ViewContainer {
            get { return UnderlyingViewTypeMap; }
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves the type name surrounded by parentheses.
        /// </summary>
        /// <returns>The (Type).</returns>
        public override string ToString () {
            return string.Format(CultureInfo.InvariantCulture, "({0})", Type);
        }
        #endregion

#if NET4
        #region ICustomSerializable Members
        void ICustomSerializable.ReadXml (XSerializer serializer, System.Xml.XmlReader reader, string containerElementName) {
            throw new NotImplementedException();
        }

        void ICustomSerializable.WriteXml (XSerializer serializer, System.Xml.XmlWriter writer) {
            writer.WriteAttributeString("Type", Type.AssemblyQualifiedName);
        }
        #endregion
#endif
    }
}
