﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.QueryEngine;
using Nen.Internal;
using Nen.Reflection;

namespace Nen.Data.ObjectRelationalMapper.Mapping {
    internal abstract class ObjectFill {
        public abstract void Execute (ResultMapper mapper);
    }

    internal class VariableObjectFill : ObjectFill {
        private object _instance;
        private TableMap _tableMap;
        private object _key;
        private Stack<VariableInfo> _path;

        #region Constructors
        public VariableObjectFill (object instance, TableMap tableMap, object key, Stack<VariableInfo> path) {
            if (instance == null)
                throw new ArgumentNullException("instance");

            if (tableMap == null)
                throw new ArgumentNullException("tableMap");

            if (key == null)
                throw new ArgumentNullException("key");

            if (path == null)
                throw new ArgumentNullException("path");

            _instance = instance;
            _key = key;
            _path = new Stack<VariableInfo>(path.Reverse());
            _tableMap = tableMap;
        }
        #endregion

        #region Execution
        public override void Execute (ResultMapper mapper) {
            foreach (var rowTableMap in mapper.Results.EachRowTableMap(_tableMap, _key))
                MapVariables(mapper, rowTableMap.Item2, rowTableMap.Item1);
        }


        private void MapVariables (ResultMapper mapper, TableMap tableMap, LightRow row) {
            if (row == null)
                throw new ArgumentNullException("row");

            foreach (var variableMap in tableMap.VariableMaps.FindAllInstanceVariables()) {
                using (_path.PushScope(variableMap.Variable)) {
                    var advice = mapper.Results.Query.GetAdvice(_path, variableMap);
                    if (advice == AdviceRequests.Ignore)
                        continue;
                }

                variableMap.StoreValueOnInstance(row, mapper, _path, _instance);
            }
        }
        #endregion
    }

    internal class EagerCollectionObjectFill : ObjectFill {
        private CollectionVariableMap _variableMap;
        private System.Collections.IList _items;
        private object _key;
        private Stack<VariableInfo> _path;

        #region Constructors
        public EagerCollectionObjectFill (CollectionVariableMap variableMap, System.Collections.IList items, object key, Stack<VariableInfo> path) {
            if (variableMap == null)
                throw new ArgumentNullException("variableMap");

            _variableMap = variableMap;
            _items = items;
            _key = key;
            _path = new Stack<VariableInfo>(path.Reverse());
        }
        #endregion

        #region Execution
        public override void Execute (ResultMapper mapper) {
            var context = mapper.Results.Query.Context;
            var collectionKeyTableMap = _variableMap.KeyVariableMap.PhysicalTableMap;
            var collectionTableMap = _variableMap.CollectionTableMap;

            var result = mapper.Results.FillResults.OfType<CollectionDbFillResult>().FirstOrDefault(r => r.CollectionVariableMap == _variableMap);
            if (result == null || !mapper.Results.Data.ContainsTable(collectionKeyTableMap.TableName))
                return;

            var matchingChildKeys = result.GetMatches(_key, mapper.Results.Data, context);
            if (matchingChildKeys != null) {
                foreach (var childKey in matchingChildKeys) {
                    var item = mapper.MapInstance(collectionTableMap, _variableMap, childKey, _path, true);
                    _items.Add(item);
                }
            }
        }
        #endregion
    }

    internal class StoredObjectFill : ObjectFill {
        private TableMap _tableMap;
        private object _key;
        private VariableFuture<object> _future;
        private Stack<VariableInfo> _path;
        private VariableMap _originVariableMap;

        #region Constructors
        public StoredObjectFill (VariableFuture<object> future, TableMap tableMap, VariableMap originVariableMap, object key, Stack<VariableInfo> path) {
            _tableMap = tableMap;
            _originVariableMap = originVariableMap;
            _key = key;
            _future = future;
            _path = new Stack<VariableInfo>(path.Reverse());
        }
        #endregion

        #region Execution
        public override void Execute (ResultMapper mapper) {
            var obj = mapper.MapInstance(_tableMap, _originVariableMap, _key, _path, _tableMap.SubTableMaps.Count > 0);
            _future.Value = obj;
        }
        #endregion
    }

    internal class CollectionDeclaringVariableFill : ObjectFill {
        private System.Collections.ICollection _collection;
        private object _key;
        private CollectionVariableMap _variableMap;

        #region Constructors
        public CollectionDeclaringVariableFill (CollectionVariableMap variableMap, System.Collections.ICollection collection, object key) {
            _variableMap = variableMap;
            _key = key;
            _collection = collection;
        }
        #endregion

        #region Execution
        public override void Execute (ResultMapper mapper) {
            // Do not use a stored path because this instance should always
            // exist already.

            var instance = mapper.MapInstance(_variableMap.PhysicalTableMap, _variableMap, _key, null, true);
            _variableMap.DeclaringVariable.SetValue(_collection, instance);
        }
        #endregion
    }
}
