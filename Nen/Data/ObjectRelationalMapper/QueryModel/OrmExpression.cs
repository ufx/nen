using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Xml.Linq;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Internal;
using Nen.Runtime.Serialization;

namespace Nen.Data.ObjectRelationalMapper.QueryModel {
    /// <summary>
    /// Provides a base for any kind of ORM expression.
    /// </summary>
    [XDataContract]
    public abstract class OrmExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new OrmExpression.
        /// </summary>
        protected OrmExpression () {
        }

        /// <summary>
        /// Constructs a new OrmExpression.
        /// </summary>
        /// <param name="copy">The expression to copy.</param>
        protected OrmExpression (OrmExpression copy) {
        }
        #endregion

        #region Operations
        /// <summary>
        /// Accepts a visitor and calls the most specific Visit method on it.
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public abstract T Accept<T> (IOrmExpressionVisitor<T> visitor);

        /// <summary>
        /// Deeply clones the expression.
        /// </summary>
        /// <returns>A deep clone of the expression.</returns>
        public abstract OrmExpression Clone ();

        /// <summary>
        /// Attempts to combine this expression with another.
        /// </summary>
        /// <param name="expression">The expression to attempt a combination with.</param>
        /// <returns>true if the expression was combined into this expression, false if otherwise.</returns>
        public virtual bool TryCombine (OrmExpression expression) {
            return false; // Combinations are not supported by default.
        }

        /// <summary>
        /// Retrieves the resulting table map.
        /// </summary>
        /// <returns>The table map used for results of this expression.</returns>
        public virtual TableMap GetResultTableMap () {
            throw new NotImplementedException(LS.T("This expression does not support result types."));
        }

        public virtual bool ValueEquals (object obj) {
            return Equals(obj);
        }
        #endregion
    }

    /// <summary>
    /// Represents a literal ORM expression.
    /// </summary>
    [XDataContract]
    public class LiteralExpression : OrmExpression {
        #region Constructors
        internal LiteralExpression () {
        }

        /// <summary>
        /// Constructs a new LiteralExpression.
        /// </summary>
        /// <param name="value">The literal value.</param>
        public LiteralExpression (object value) {
            Value = value;
        }

        /// <summary>
        /// Constructs a new LiteralExpression.
        /// </summary>
        /// <param name="copy">The expression to copy.</param>
        protected LiteralExpression (LiteralExpression copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            Value = copy.Value;
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Accepts a visitor and calls Visit(LiteralExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }

        /// <summary>
        /// Deeply clones the expression.
        /// </summary>
        /// <returns>A deep clone of the expression.</returns>
        public override OrmExpression Clone () {
            return new LiteralExpression(this);
        }

        /// <summary>
        /// Attempts to combine this expression with another.  Two nulls may be
        /// combined into one.  All other values are combined into a list.
        /// </summary>
        /// <param name="expression">The expression to attempt a combination with.</param>
        /// <returns>true if the expression was combined into this expression, false if otherwise.</returns>
        public override bool TryCombine (OrmExpression expression) {
            var literal = expression as LiteralExpression;
            if (literal == null)
                return false;

            if (Value == null && literal.Value == null)
                return true;

            var values = new List<object>(2);
            if (Value is IEnumerable<object>)
                values.AddRange((IEnumerable<object>) Value);
            else
                values.Add(Value);

            if (literal.Value is IEnumerable<object>)
                values.AddRange((IEnumerable<object>) literal.Value);
            else
                values.Add(literal.Value);

            Value = values.ToArray();
            return true;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the literal value.
        /// </summary>
        [XDataMember]
        public object Value { get; internal set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the literal.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            string str = null;
            if (Value is string)
                return "'" + (string)Value + "'";
            else if (Value is IEnumerable)
                str = string.Join(",", ((IEnumerable)Value).Cast<object>());
            else
                str = Value.ToString();

            return Value == null ? "null" : str;
        }
        #endregion
    }

    /// <summary>
    /// Visits specific kinds of ORM expressions.
    /// </summary>
    /// <typeparam name="T">The return type of each Visit method.</typeparam>
    public interface IOrmExpressionVisitor<T> {
        // Other Expressions

        /// <summary>
        /// Visits a literal expression.
        /// </summary>
        /// <param name="literal">The literal to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (LiteralExpression literal);

        // Query Expressions

        /// <summary>
        /// Visits a query expression.
        /// </summary>
        /// <param name="query">The query to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (QueryExpression query);

        /// <summary>
        /// Visits an advice expression.
        /// </summary>
        /// <param name="advice">The advice to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (AdviceExpression advice);

        /// <summary>
        /// Visits a specified order.
        /// </summary>
        /// <param name="order">The specified order to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (OrderExpression order);

        /// <summary>
        /// Visits a query projection.
        /// </summary>
        /// <param name="projection">The projection to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (ProjectionExpression projection);

        // Operators

        /// <summary>
        /// Visits a binary operator expression.
        /// </summary>
        /// <param name="binaryOperator">The binary operator to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (BinaryOperatorExpression binaryOperator);

        /// <summary>
        /// Visits a unary operator expression.
        /// </summary>
        /// <param name="unaryOperator">The unary operator to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (UnaryOperatorExpression unaryOperator);

        /// <summary>
        /// Visits an apply expression.
        /// </summary>
        /// <param name="apply">The apply function to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (ApplyExpression apply);

        // Variables

        /// <summary>
        /// Visits a variable expression.
        /// </summary>
        /// <param name="variable">The variable to visit.</param>
        /// <returns>The result of the visit operation.</returns>
        T Visit (VariableExpression variable);
    }
}
