using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

using Nen.Internal;
using Nen.Runtime.Serialization;

namespace Nen.Data.ObjectRelationalMapper.QueryModel {
    /// <summary>
    /// Provides a base for ORM operation expressions.
    /// </summary>
    [XDataContract]
    public abstract class OperatorExpression : OrmExpression {
        #region Constructors
        /// <summary>
        /// Constructs a new OperatorExpression.
        /// </summary>
        protected OperatorExpression () {
        }

        /// <summary>
        /// Constructs a new OperatorExpression.
        /// </summary>
        /// <param name="copy">The expression to copy.</param>
        protected OperatorExpression (OperatorExpression copy)
            : base(copy) {
        }
        #endregion
    }

    /// <summary>
    /// Represents an ORM binary operator expression.
    /// </summary>
    [XDataContract]
    public class BinaryOperatorExpression : OperatorExpression {
        #region Constructors
        internal BinaryOperatorExpression () {
        }

        /// <summary>
        /// Constructs a new BinaryOperatorExpression.
        /// </summary>
        /// <param name="left">The left hand side of the expression.</param>
        /// <param name="operatorValue">The operator to use.</param>
        /// <param name="right">The right hand side of the expression.</param>
        public BinaryOperatorExpression (OrmExpression left, BinaryOperatorType operatorValue, OrmExpression right) {
            if (left == null)
                throw new ArgumentNullException("left");

            if (right == null)
                throw new ArgumentNullException("right");

            Left = left;
            Right = right;
            Operator = operatorValue;
        }

        /// <summary>
        /// Constructs a new BinaryOperatorExpression.
        /// </summary>
        /// <param name="copy">The expression to copy.</param>
        protected BinaryOperatorExpression (BinaryOperatorExpression copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            Left = copy.Left.Clone();
            Right = copy.Right.Clone();
            Operator = copy.Operator;
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Deeply clones the expression.
        /// </summary>
        /// <returns>A deep clone of the expression.</returns>
        public override OrmExpression Clone () {
            return new BinaryOperatorExpression(this);
        }

        /// <summary>
        /// Attempts to combine this expression with another.  Like terms can
        /// be grouped with an extra OR expression.
        /// </summary>
        /// <param name="expression">The expression to attempt a combination with.</param>
        /// <returns>true if the expression was combined into this expression, false if otherwise.</returns>
        public override bool TryCombine (OrmExpression expression) {
            var op = expression as BinaryOperatorExpression;
            if (op == null)
                return false;

            if (TryCombineRightTerms(op))
                return true;

            // Right terms couldn't be combined, so join this expression with
            // an OR.

            Left = Clone();
            Operator = BinaryOperatorType.Or;
            Right = op;

            return true;
        }

        private bool TryCombineRightTerms (BinaryOperatorExpression op) {
            // To combine a binary expression both the operator and left
            // hand side must be equal.

            // Only combine terms of an In or Equal statement.
            if (Operator != BinaryOperatorType.Equal && Operator != BinaryOperatorType.In)
                return false;

            if (op.Operator != BinaryOperatorType.Equal && op.Operator != BinaryOperatorType.In)
                return false;

            if (!Left.ValueEquals(op.Left))
                return false;

            var combined = Right.TryCombine(op.Right);
            // Equal must be changed to In when combinations succeed.
            if (combined && Operator == BinaryOperatorType.Equal)
                Operator = BinaryOperatorType.In;

            return combined;
        }

        /// <summary>
        /// Accepts a visitor and calls Visit(BinaryOperatorExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
        #endregion

        #region Operator Joining
        /// <summary>
        /// Joins two binary operator expressions into a single binary
        /// operator expression.  If either side of the expression is null,
        /// only that side is returned as the resulting expression.
        /// </summary>
        /// <param name="left">The left expression to join.</param>
        /// <param name="operatorValue">The operator value to separate the two expressions.</param>
        /// <param name="right">The right expression to join.</param>
        /// <returns>A joined expression.</returns>
        public static OperatorExpression Join (OperatorExpression left, BinaryOperatorType operatorValue, OperatorExpression right) {
            if (left == null)
                return right;

            if (right == null)
                return left;

            // Try to combine like terms in logical or joins.
            if (operatorValue == BinaryOperatorType.Or) {
                if (left.TryCombine(right))
                    return left;
            }

            return new BinaryOperatorExpression(left, operatorValue, right);
        }
        #endregion

        #region SQL Conversion
        public string GetSqlOperatorType () {
            switch (Operator) {
                case BinaryOperatorType.And: return "AND";
                case BinaryOperatorType.Or: return "OR";
                case BinaryOperatorType.GreaterThan: return ">";
                case BinaryOperatorType.GreaterThanOrEqual: return ">=";
                case BinaryOperatorType.LessThan: return "<";
                case BinaryOperatorType.LessThanOrEqual: return "<=";
                case BinaryOperatorType.Equal: return "=";
                case BinaryOperatorType.NotEqual: return "!=";
                case BinaryOperatorType.Multiply: return "*";
                case BinaryOperatorType.Add: return "+";
                case BinaryOperatorType.Subtract: return "-";
                case BinaryOperatorType.Divide: return "/";
                case BinaryOperatorType.In: return "IN";
                case BinaryOperatorType.Like: return "LIKE";

                default:
                    throw new NotImplementedException(LS.T("Invalid binary operator specified."));
            }
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the left hand side of the expression.
        /// </summary>
        [XDataMember]
        public OrmExpression Left { get; internal set; }

        /// <summary>
        /// Gets the operator to use.
        /// </summary>
        [XDataMember(XDataType.Attribute)]
        public BinaryOperatorType Operator { get; private set; }

        /// <summary>
        /// Gets the right hand side of the expression.
        /// </summary>
        [XDataMember]
        public OrmExpression Right { get; internal set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the binary operator expression.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return "(" + Left.ToString() + " " + Operator.ToString() + " " + Right.ToString() + ")";
        }
        #endregion
    }

    /// <summary>
    /// Provides a base for ORM unary operator expressions.
    /// </summary>
    [XDataContract]
    public class UnaryOperatorExpression : OperatorExpression {
        #region Constructors
        internal UnaryOperatorExpression () {
        }

        /// <summary>
        /// Constructs a new UnaryOperatorExpression.
        /// </summary>
        /// <param name="operand">The operand expression.</param>
        /// <param name="operatorValue">The operator value.</param>
        public UnaryOperatorExpression (OrmExpression operand, UnaryOperatorType operatorValue) {
            if (operand == null)
                throw new ArgumentNullException("operand");

            Operand = operand;
            Operator = operatorValue;
        }

        /// <summary>
        /// Constructs a new UnaryOperatorExpression.
        /// </summary>
        /// <param name="copy">The expression to copy.</param>
        protected UnaryOperatorExpression (UnaryOperatorExpression copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            Operand = copy.Operand.Clone();
            Operator = copy.Operator;
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Deeply clones the expression.
        /// </summary>
        /// <returns>A deep clone of the expression.</returns>
        public override OrmExpression Clone () {
            return new UnaryOperatorExpression(this);
        }

        /// <summary>
        /// Accepts a visitor and calls Visit(UnaryOperatorExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the operand expression.
        /// </summary>
        [XDataMember]
        public OrmExpression Operand { get; internal set; }

        /// <summary>
        /// Gets the operator value.
        /// </summary>
        [XDataMember(XDataType.Attribute)]
        public UnaryOperatorType Operator { get; private set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the unary operator expression.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return Operator.ToString() + " " + Operand.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Represents a function application operator.
    /// </summary>
    [XDataContract]
    public class ApplyExpression : OperatorExpression {
        #region Constructors
        internal ApplyExpression () {
        }

        /// <summary>
        /// Constructs a new ApplyExpression.
        /// </summary>
        /// <param name="operatorValue">The operator value.</param>
        /// <param name="arguments">The arguments to apply.</param>
        public ApplyExpression (string operatorValue, params OrmExpression[] arguments) {
            if (operatorValue == null)
                throw new ArgumentNullException("operatorValue");

            Operator = operatorValue;
            Arguments = arguments;
        }

        /// <summary>
        /// Constructs a new ApplyExpression.
        /// </summary>
        /// <param name="copy">The expression to copy.</param>
        protected ApplyExpression (ApplyExpression copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            Operator = copy.Operator;

            if (copy.Arguments != null)
                Arguments = copy.Arguments.Select(a => a.Clone()).ToArray();
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Deeply clones the expression.
        /// </summary>
        /// <returns>A deep clone of the expression.</returns>
        public override OrmExpression Clone () {
            return new ApplyExpression(this);
        }

        /// <summary>
        /// Accepts a visitor and calls Visit(ApplyExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the arguments to apply.
        /// </summary>
        [XDataMember]
        public OrmExpression[] Arguments { get; private set; }

        /// <summary>
        /// Gets the operator value.
        /// </summary>
        [XDataMember]
        public string Operator { get; private set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the apply expression.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            if (Arguments == null)
                return Operator + "()";
            else
                return Operator + "(" + string.Join(", ", Arguments.Cast<object>()) + ")";
        }
        #endregion
    }

    /// <summary>
    /// A list of binary operators that are acceptable for use in
    /// BinaryOperatorExpressions.
    /// </summary>
    public enum BinaryOperatorType {
        /// <summary>
        /// The logical And operator.
        /// </summary>
        And,

        /// <summary>
        /// The logical Or operator.
        /// </summary>
        Or,

        /// <summary>
        /// The relational greater than operator.
        /// </summary>
        GreaterThan,

        /// <summary>
        /// The relational greater than or equal operator.
        /// </summary>
        GreaterThanOrEqual,

        /// <summary>
        /// The relational less than operator.
        /// </summary>
        LessThan,

        /// <summary>
        /// The relational less than or equal operator.
        /// </summary>
        LessThanOrEqual,

        /// <summary>
        /// The equality operator.
        /// </summary>
        Equal,

        /// <summary>
        /// The inequality operator.
        /// </summary>
        NotEqual,

        /// <summary>
        /// The add operator.
        /// </summary>
        Add,

        /// <summary>
        /// The subtract operator.
        /// </summary>
        Subtract,

        /// <summary>
        /// The multiply operator.
        /// </summary>
        Multiply,

        /// <summary>
        /// The divide operator.
        /// </summary>
        Divide,

        /// <summary>
        /// The matching like operator.
        /// </summary>
        Like,

        /// <summary>
        /// The subset in operator.
        /// </summary>
        In
    }

    /// <summary>
    /// A list of unary operators that are acceptable for use in
    /// UnaryOperatorExpressions.
    /// </summary>
    public enum UnaryOperatorType {
        /// <summary>
        /// The negation operator.
        /// </summary>
        Not,

        /// <summary>
        /// The evaluate operator.
        /// </summary>
        Evaluate,

        /// <summary>
        /// The exists operator.
        /// </summary>
        Exists,

        /// <summary>
        /// The lowercase string conversion operator.
        /// </summary>
        ToLower,

        /// <summary>
        /// The uppercase string conversion operator.
        /// </summary>
        ToUpper
    }
}
