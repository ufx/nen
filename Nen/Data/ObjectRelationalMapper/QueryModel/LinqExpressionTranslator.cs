﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Internal;
using Nen.Linq.Expressions;
using Nen.Reflection;

using LinqExpr = System.Linq.Expressions;
using System.Runtime.CompilerServices;

// todo: unwrap values of literals, e.g., string methods

namespace Nen.Data.ObjectRelationalMapper.QueryModel {
    public class LinqExpressionTranslator : IExpressionVisitor<OrmExpression> {
        internal ExpressionFrame _frame;
        private MapConfiguration _configuration;
        private LinqExpr.Expression _initialExpression;

        #region Constructors
        public LinqExpressionTranslator (LinqExpr.Expression initialExpression, MapConfiguration configuration) {
            _configuration = configuration;
            _initialExpression = initialExpression;
        }
        #endregion

        #region Translation
        public static QueryExpression Translate (LinqExpr.Expression expression, MapConfiguration configuration) {
            var translator = new LinqExpressionTranslator(expression, configuration);
            var result = expression.Accept(translator);

            var postProcessor = new ExpressionPostProcessor(configuration);
            result.Accept(postProcessor);

            return (QueryExpression) result;
        }

        public static QueryExpression Translate (LinqExpr.Expression expression, DataContext context) {
            return Translate(expression, context.Configuration);
        }
        #endregion

        #region Expression Frames
        // The below must be placed in a try/finally block!

        private void BeginFrame () {
            _frame = new ExpressionFrame(_frame);
        }

        private void EndFrame () {
            if (_frame != null)
                _frame = _frame.Parent;
        }
        #endregion

        #region Operator Translations
        private BinaryOperatorType TranslateBinaryOperator (LinqExpr.ExpressionType type) {
            switch (type) {
                case LinqExpr.ExpressionType.Add:
                    return BinaryOperatorType.Add;

                case LinqExpr.ExpressionType.AndAlso:
                    return BinaryOperatorType.And;

                case LinqExpr.ExpressionType.Divide:
                    return BinaryOperatorType.Divide;

                case LinqExpr.ExpressionType.Equal:
                    return BinaryOperatorType.Equal;

                case LinqExpr.ExpressionType.GreaterThan:
                    return BinaryOperatorType.GreaterThan;

                case LinqExpr.ExpressionType.GreaterThanOrEqual:
                    return BinaryOperatorType.GreaterThanOrEqual;

                case LinqExpr.ExpressionType.LessThan:
                    return BinaryOperatorType.LessThan;

                case LinqExpr.ExpressionType.LessThanOrEqual:
                    return BinaryOperatorType.LessThanOrEqual;

                case LinqExpr.ExpressionType.Multiply:
                    return BinaryOperatorType.Multiply;

                case LinqExpr.ExpressionType.NotEqual:
                    return BinaryOperatorType.NotEqual;

                case LinqExpr.ExpressionType.OrElse:
                    return BinaryOperatorType.Or;

                case LinqExpr.ExpressionType.Subtract:
                    return BinaryOperatorType.Subtract;

                default:
                    throw new NotSupportedException(LS.T("The node type '{0}' is not supported as a binary operator.", type));
            }
        }

        private UnaryOperatorType TranslateUnaryOperator (LinqExpr.ExpressionType type) {
            switch (type) {
                case LinqExpr.ExpressionType.Not:
                    // I have no idea if this is accurate.
                    return UnaryOperatorType.Not;

                default:
                    throw new NotSupportedException(LS.T("The node type '{0}' is not supported as a unary operator.", type));
            }
        }
        #endregion

        #region Expression Visitation
        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.BinaryExpression expression) {
            var left = expression.Left.Accept(this);
            var right = expression.Right.Accept(this);

            if (expression.NodeType == LinqExpr.ExpressionType.ArrayIndex) {
                var array = (Array) ((LiteralExpression) left).Value;
                var index = (int) ((LiteralExpression) right).Value;
                return new LiteralExpression(array.GetValue(index));
            }

            // Handle coalesce as a function
            if (expression.NodeType == LinqExpr.ExpressionType.Coalesce)
                return new ApplyExpression("Coalesce", left, right);

            var op = TranslateBinaryOperator(expression.NodeType);
            // Wrap logical operator criteria in unary expressions.
            switch (op) {
                case BinaryOperatorType.And:
                case BinaryOperatorType.Or:
                    left = WrapUnaryLogicalExpression(left);
                    right = WrapUnaryLogicalExpression(right);
                    break;
            }


            return new BinaryOperatorExpression(left, op, right);
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.ConditionalExpression expression) {
            throw new NotSupportedException();
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.TypeBinaryExpression expression) {
            throw new NotSupportedException();
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.UnaryExpression expression) {
            if (expression.Method != null)
                throw new NotSupportedException(LS.T("Unary expression '{0}' is not supported", expression));

            var operand = expression.Operand.Accept(this);

            switch (expression.NodeType) {
                case LinqExpr.ExpressionType.Not:
                    {
                        // When negating an empty contains criteria, replace with true.
                        var binaryOperand = operand as BinaryOperatorExpression;
                        if (binaryOperand != null && binaryOperand.Operator == BinaryOperatorType.In)
                        {
                            var literalRight = binaryOperand.Right as LiteralExpression;
                            if (literalRight != null && literalRight.Value is Array && ((Array)literalRight.Value).Length == 0)
                                return new UnaryOperatorExpression(new LiteralExpression(true), UnaryOperatorType.Evaluate);
                        }

                        return new UnaryOperatorExpression(operand, UnaryOperatorType.Not);
                    }

                default:
                    return operand;
            }
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.ListInitExpression expression) {
            throw new NotSupportedException();
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.NewArrayExpression expression) {
            throw new NotSupportedException();
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.NewExpression expression) {
            if (expression.Type.IsAnonymous()) {
                // Create an ad hoc table.
                var arguments = expression.Arguments.Select(e => e.Accept(this)).ToArray();
                var underlyingViewTableMap = arguments[0].GetResultTableMap();
                var configuration = underlyingViewTableMap.Configuration;
                var adHocTypeMap = configuration.CreateAdHocTypeMap(expression.Type, underlyingViewTableMap, arguments, expression.Constructor, expression.Members.ToArray());
                var adHocTableMap = configuration.CreateAdHocTableMap(adHocTypeMap, underlyingViewTableMap);
                return new TableExpression(adHocTableMap);
            } else {
                // Probably a literal - just execute this.
                var lambda = LinqExpr.Expression.Lambda(expression);
                var del = lambda.Compile();
                var result = del.DynamicInvoke();
                return new LiteralExpression(result);
            }
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.MemberInitExpression expression) {
            throw new NotSupportedException();
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.MemberExpression expression) {
            if (expression.Expression == null)
                return VisitLiteralMember(expression);

            var container = expression.Expression.Accept(this);

            if (expression.Expression.Type == typeof(TimeSpan) && expression.Expression.NodeType == LinqExpr.ExpressionType.Subtract)
            {
                // This is likely some date math - translate into a datediff call.
                var binaryExpression = (BinaryOperatorExpression)container;
                var datepart = new LiteralExpression(expression.Member.Name);
                return new ApplyExpression("datediff", datepart, binaryExpression.Left, binaryExpression.Right);
            }

            var variable = VariableInfo.Create(expression.Member);

            if (container is ProjectionExpression) {
                var projection = (ProjectionExpression) container;
                return new VariableExpression(variable, projection.Parent);
            }

            if (container is LiteralExpression) {
                var literal = (LiteralExpression) container;
                var value = variable.GetValue(literal.Value);
                return ExtractLiteralValue(value);
            }

            if (container is VariableExpression) {
                var containerVariable = (VariableExpression) container;
                var composite = new CompositeVariableInfo(containerVariable.Variable, variable);
                return new VariableExpression(composite, containerVariable.Container);
            }

            throw new NotSupportedException();
        }

        private OrmExpression VisitLiteralMember (LinqExpr.MemberExpression expression) {
            var variable = VariableInfo.Create(expression.Member);
            var value = variable.GetValue(null);
            return new LiteralExpression(value);
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.ParameterExpression expression) {
            return _frame.GetParameterValue(expression);
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.LambdaExpression expression) {
            if (_frame == null) {
                // When no frame exists, this expression is a fragment.  Create
                // temporary parameter bindings for it.

                try {
                    BeginFrame();

                    foreach (var parameter in expression.Parameters) {
                        var tableMap = _configuration.TableMaps[parameter.Type];
                        var query = new QueryExpression(null, tableMap);

                        _frame.Arguments.Add(query.Projection);
                    }

                    return VisitLambdaCore(expression);
                } finally {
                    EndFrame();
                }
            } else
                return VisitLambdaCore(expression);
        }

        private OrmExpression VisitLambdaCore (LinqExpr.LambdaExpression expression) {
            // Bind parameters to arguments.
            for (int i = 0; i < expression.Parameters.Count; i++)
                _frame.SetParameterValue(expression.Parameters[i], _frame.Arguments[i]);

            return expression.Body.Accept(this);
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.InvocationExpression expression) {
            throw new NotSupportedException();
        }

        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.MethodCallExpression expression) {
            try {
                BeginFrame();

                if (expression.Object != null)
                    return VisitObjectMethod(expression);
                else
                    return VisitStaticMethod(expression);
            } finally {
                EndFrame();
            }
        }

        #region Object Methods
        private OrmExpression VisitObjectMethod (LinqExpr.MethodCallExpression expression) {
            var container = expression.Object.Accept(this);

            var type = expression.Object.Type;
            if (type == typeof(string))
                return VisitStringMethod(expression, container);
            else if (type == typeof(DateTime) || type == typeof(DateTime?))
                return VisitDateTimeMethod(expression, container);

            if (container is VariableExpression) {
                var variableExpression = (VariableExpression) container;
                var variableType = variableExpression.Variable.MemberType;

                if (CollectionEx.IsCollection(variableType))
                    return VisitCollectionMethod(expression, variableExpression);
            }

            throw new NotSupportedException(LS.T("Object method call '{0}' is not supported.", expression));
        }

        private OrmExpression VisitDateTimeMethod(LinqExpr.MethodCallExpression expression, OrmExpression objectExpression)
        {
            var argument = (LiteralExpression)expression.Arguments[0].Accept(this);

            if (expression.Method.Name.StartsWith("Add"))
            {
                var part = new LiteralExpression(expression.Method.Name.Substring(3));
                return new ApplyExpression("dateadd", part, argument, objectExpression);
            }

            throw new NotSupportedException(LS.T("DateTime method call '{0}' is not supported.", expression));
        }

        private OrmExpression VisitCollectionMethod (LinqExpr.MethodCallExpression expression, VariableExpression variableExpression) {
            switch (expression.Method.Name) {
                case "Contains":
                    return VisitCollectionContainsMethod(expression, variableExpression);
            }

            throw new NotSupportedException(LS.T("Collection method call '{0}' is not supported.", expression));
        }

        private OrmExpression VisitCollectionContainsMethod (LinqExpr.MethodCallExpression expression, VariableExpression variableExpression) {
            var container = variableExpression.Container;

            var resultTableMap = container.GetResultTableMap();
            var collectionVariableMap = (CollectionVariableMap) resultTableMap.VariableMaps.Find(variableExpression.Variable, VariableStructure.BaseContainers | VariableStructure.WithinComponents);

            // Find the declaring table of the collection's primary key.
            VariableExpression primaryKey;
            var composite = variableExpression.Variable as CompositeVariableInfo;
            if (composite != null) {
                var pathToCollection = composite.Components.Take(composite.Components.Length - 1).ToArray();
                primaryKey = new VariableExpression(new CompositeVariableInfo(pathToCollection), container);
            } else
                primaryKey = new VariableExpression(resultTableMap.PrimaryKey.VariableMap.Variable, container);

            // Create the subquery with the contained value.
            var subQuery = collectionVariableMap.CreateCollectionQuery(container.Context);
            subQuery.Projection.Items.Add(new VariableExpression(collectionVariableMap.KeyVariableMap.Variable, subQuery));

            var criteriaLeft = new VariableExpression(collectionVariableMap.CollectionTableMap.PrimaryKey.VariableMap.Variable, subQuery);
            var criteriaRight = expression.Arguments[0].Accept(this);
            var criteria = new BinaryOperatorExpression(criteriaLeft, BinaryOperatorType.Equal, criteriaRight);
            subQuery.WhereCriteria = BinaryOperatorExpression.Join(subQuery.WhereCriteria, BinaryOperatorType.And, criteria);

            return new BinaryOperatorExpression(primaryKey, BinaryOperatorType.In, subQuery);
        }

        private OrmExpression VisitStringMethod (LinqExpr.MethodCallExpression expression, OrmExpression objectExpression) {
            if (expression.Arguments.Count == 0)
            {
                switch (expression.Method.Name)
                {
                    case "ToLowerInvariant":
                    case "ToLower":
                        return new UnaryOperatorExpression(objectExpression, UnaryOperatorType.ToLower);
                    case "ToUpperInvariant":
                    case "ToUpper":
                        return new UnaryOperatorExpression(objectExpression, UnaryOperatorType.ToUpper);
                }
            }
            else if (expression.Arguments.Count > 0)
            {
                var argument = (LiteralExpression)expression.Arguments[0].Accept(this);

                switch (expression.Method.Name)
                {
                    case "StartsWith":
                        return new BinaryOperatorExpression(objectExpression, BinaryOperatorType.Like, new LiteralExpression((string)argument.Value + "%"));
                    case "EndsWith":
                        return new BinaryOperatorExpression(objectExpression, BinaryOperatorType.Like, new LiteralExpression("%" + (string)argument.Value));
                    case "Contains":
                        return new BinaryOperatorExpression(objectExpression, BinaryOperatorType.Like, new LiteralExpression("%" + (string)argument.Value + "%"));
                }
            }

            throw new NotSupportedException(LS.T("String method call '{0}' is not supported.", expression));
        }
        #endregion

        #region Static Methods
        private OrmExpression VisitStaticMethod (LinqExpr.MethodCallExpression expression) {
            if (expression.Method.GetAttribute<QueryMacroAttribute>(true) != null)
                return Macroexpand(expression);

            var firstParameter = expression.Method.GetParameters().FirstOrDefault();
            if (firstParameter.ParameterType.GetInterfaces().Contains(typeof(System.Collections.IEnumerable)))
                return VisitEnumerableStaticMethod(expression);

            // Find an instance of the projection in the list.
            var arguments = expression.Arguments.Select(a => a.Accept(this)).ToArray();
            ProjectionExpression projection = null;
            int projectionIndex;
            for (projectionIndex = 0; projectionIndex < arguments.Length; projectionIndex++)
            {
                projection = arguments[projectionIndex] as ProjectionExpression;
                if (projection != null)
                    break;
            }

            if (projection != null)
            {
                if (projection.Results == null)
                {

                    // One of the arguments is the projection, which means this
                    // method needs to be called with the result of the queries
                    // at the end of the process.
                    projection.ResultTranslatorState = new TranslatorState(_frame, expression, _initialExpression);
                    return projection.Parent;
                }

                // This projection has results, so we have already executed the query
                // and now the method needs to be called.

                // First collect parameters and create the delegate.
                var parameters = new List<LinqExpr.ParameterExpression>();
                foreach (var arg in expression.Arguments)
                    parameters.Add(arg as LinqExpr.ParameterExpression ?? LinqExpr.Expression.Parameter(arg.Type));

                var lambda = LinqExpr.Expression.Lambda(expression, parameters);
                var del = lambda.Compile();

                // Now invoke the delegate with each of the project results.
                var results = new List<object>();
                foreach (var obj in ((System.Collections.IEnumerable)projection.Results))
                {
                    var args = expression.Arguments.Cast<object>().ToArray();
                    args[projectionIndex] = obj;
                    results.Add(del.DynamicInvoke(args));
                }
                return new LiteralExpression(results);
            }

            throw new NotSupportedException(LS.T("Static method call '{0}' is not supported", expression.Method.Name));
        }

        private OrmExpression VisitEnumerableStaticMethod(LinqExpr.MethodCallExpression expression)
        {
            switch (expression.Method.Name)
            {
                case "Select":
                    return VisitSelectMethod(expression);

                case "Any":
                    return VisitAnyMethod(expression);
                case "All":
                    return VisitAllMethod(expression);
                case "Where":
                    return VisitWhereMethod(expression);

                case "Include":
                case "LoadWith":
                    return VisitAdviceExpression(expression, AdviceRequests.Include);
                case "Defer":
                    return VisitAdviceExpression(expression, AdviceRequests.DeferJoin);
                case "Ignore":
                    return VisitAdviceExpression(expression, AdviceRequests.Ignore);
                case "JoinOnIncludeOnly":
                case "JoinOnLoadWithOnly":
                    return VisitAdviceExpression(expression, AdviceRequests.JoinOnIncludeOnly);

                case "ThenBy":
                case "OrderBy":
                    return VisitOrderByMethod(expression, OrderDirection.Ascending);

                case "ThenByDescending":
                case "OrderByDescending":
                    return VisitOrderByMethod(expression, OrderDirection.Descending);

                case "Skip":
                    return VisitSkipMethod(expression);
                case "Take":
                    return VisitTakeMethod(expression);

                case "Count":
                case "Min":
                case "Max":
                case "Sum":
                case "Average":
                case "Distinct":
                    return VisitScalarMethod(expression);

                case "OfType":
                case "Cast":
                    return VisitOfTypeMethod(expression);

                case "First":
                    return VisitSingletonMethod(expression, QueryShape.First);
                case "FirstOrDefault":
                    return VisitSingletonMethod(expression, QueryShape.FirstOrDefault);
                case "Single":
                    return VisitSingletonMethod(expression, QueryShape.Single);
                case "SingleOrDefault":
                    return VisitSingletonMethod(expression, QueryShape.SingleOrDefault);

                case "WhenType":
                    return VisitWhenTypeMethod(expression);

                case "Contains":
                    return VisitEnumerableContainsMethod(expression);
            }

            throw new NotSupportedException(LS.T("Static enumerable method call '{0}' is not supported", expression.Method.Name));
        }

        private OrmExpression VisitEnumerableContainsMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = expression.Arguments[0].Accept(this);
            var property = expression.Arguments[1].Accept(this);

            // If the property is a projection, make sure to project only the
            // primary key, and use that as the property criteria.
            if (sequence is QueryExpression) {
                var query = (QueryExpression) sequence;
                var resultTableMap = query.GetResultTableMap();
                query.Projection.Items.Add(new VariableExpression(resultTableMap.PrimaryKey.VariableMap.Variable, query));
                query.IsPossiblyAmbiguous = false; // PK is never ambiguous
                query.OrderBy.Clear();
            }

            return new BinaryOperatorExpression(property, BinaryOperatorType.In, sequence);
        }

        private OrmExpression VisitOfTypeMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = VisitSequenceMethod(expression);
            
            var subType = expression.Type.GetGenericArguments()[0];
            if (subType != typeof(object))
            {
                var subTableMap = _configuration.TableMaps[subType];
                sequence.Subject = new TableExpression(subTableMap);
            }

            return sequence;
        }

        private OrmExpression VisitWhenTypeMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = VisitSequenceMethod(expression);

            // Temporarily change the subject of the sequence to the
            // requested type, proceed, then change the subject back.
            var originalSubject = sequence.Subject;

            var unary = (LinqExpr.UnaryExpression) expression.Arguments[1];
            var lambda = (LinqExpr.LambdaExpression) unary.Operand;
            var parameter = lambda.Parameters[0];
            var type = parameter.Type.GetGenericArguments()[0];
            var tableMap = _configuration.TableMaps[type];
            sequence.Subject = new TableExpression(tableMap);

            expression.Arguments[1].Accept(this);

            sequence.Subject = originalSubject;

            return sequence;
        }

        private OrmExpression VisitDistinctMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = VisitSequenceMethod(expression);
            if (!sequence.IsScalar)
                throw new NotSupportedException("Distinct is only supported on scalar queries that select primitives.");

            sequence.IsDistinct = true;
            return sequence;
        }

        private OrmExpression VisitScalarMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = VisitSequenceMethod(expression);
            sequence.Shape = QueryShape.FirstOrDefault;
            sequence.IsScalar = true;
            sequence.IsPossiblyAmbiguous = false; // Can't think of a case where this is ambiguous.

            var arguments = new OrmExpression[0];
            if (expression.Method.Name == "Count") {
                if (expression.Arguments.Count == 2)
                {
                    var countCriteria = (OperatorExpression)expression.Arguments[1].Accept(this);
                    sequence.WhereCriteria = BinaryOperatorExpression.Join(sequence.WhereCriteria, BinaryOperatorType.And, countCriteria);
                }
                else
                {
                    // Ordering is not required for counts, so get rid of it if any exists.
                    sequence.OrderBy.Clear();
                }
            } else
                arguments = expression.Arguments.Skip(1).Select(a => a.Accept(this)).ToArray();

            ApplyExpression apply;

            // When the projection contains something and the method does not, add it as a method argument.
            if (sequence.Projection.Items.Count > 0 && arguments.Length == 0)
            {
                apply = new ApplyExpression(expression.Method.Name, sequence.Projection.Items.ToArray());
                sequence.Projection.Items.Clear();
            }
            else
                apply = new ApplyExpression(expression.Method.Name, arguments);

            sequence.Projection.Items.Add(apply);
            return sequence;
        }

        private OrmExpression VisitSingletonMethod (LinqExpr.MethodCallExpression expression, QueryShape shape) {
            var sequence = VisitSequenceMethod(expression);

            sequence.Limit = 1;
            sequence.Shape = shape;

            if (expression.Arguments.Count > 1) {
                var criteria = WrapUnaryLogicalExpression(expression.Arguments[1].Accept(this));
                sequence.WhereCriteria = BinaryOperatorExpression.Join(sequence.WhereCriteria, BinaryOperatorType.And, criteria);
            }

            return sequence;
        }

        private QueryExpression VisitSequenceMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = expression.Arguments[0].Accept(this);

            // The sequence might be a collection variable.  In that case, the
            // collection becomes a new sequence with a modified subject.
            if (sequence is VariableExpression) {
                var variableExpression = (VariableExpression) sequence;
                sequence = new QueryExpression(variableExpression.Container.Context, variableExpression);
            }

            // The sequence might be a projection.  Unwrap the projection
            // and use its parent query as is.
            else if (sequence is ProjectionExpression) {
                var projectionExpression = (ProjectionExpression) sequence;
                sequence = projectionExpression.Parent;
            }
            
            // Literals need new query expressions.  They have no context.
            else if (sequence is LiteralExpression)
                sequence = new QueryExpression(null, sequence);

            var query = (QueryExpression) sequence;
            _frame.Arguments.Add(query.Projection);
            return query;
        }

        private OrmExpression VisitSelectMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = VisitSequenceMethod(expression);

            var projection = expression.Arguments[1].Accept(this);
            if (projection is TableExpression)
                sequence.Subject = projection;
            else if (projection is VariableExpression) {
                var variableProjection = (VariableExpression) projection;

                // Project the variable as it will become the sub-criteria for
                // the query which selects the data.
                sequence.Projection.Items.Add(projection);

                // Projections against references must be rewritten to retrieve
                // the correct table with the criteria in this sequence.
                // Simple projections don't need any rewriting as the table in
                // this sequence is correct.
                if (variableProjection.Variable.MemberType.IsSimple()) {
                    sequence.IsScalar = true;
                } else {
                    var projectedTableMap = _configuration.TableMaps[variableProjection.Variable.MemberType];
                    var projectedQuery = new QueryExpression(sequence.Context, projectedTableMap);

                    var projectedPrimaryKey = new VariableExpression(projectedTableMap.PrimaryKey.VariableMap.Variable, projectedQuery);
                    projectedQuery.WhereCriteria = new BinaryOperatorExpression(projectedPrimaryKey, BinaryOperatorType.In, sequence);

                    return projectedQuery;
                }
            }

            return sequence;
        }

        private static OperatorExpression WrapUnaryLogicalExpression (OrmExpression criteria) {
            if (criteria is OperatorExpression) {
                // Most common case.  Do nothing.
                return (OperatorExpression) criteria;
            } else if (criteria is VariableExpression) {
                // Wrap the variable in a unary expression.
                return new UnaryOperatorExpression(criteria, UnaryOperatorType.Evaluate);
            } else
                throw new NotSupportedException(LS.T("Logical expression type of '{0}' is not supported.", criteria.GetType()));
        }

        private OrmExpression VisitWhereMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = VisitSequenceMethod(expression);
            var criteria = WrapUnaryLogicalExpression(expression.Arguments[1].Accept(this));
            sequence.WhereCriteria = BinaryOperatorExpression.Join(sequence.WhereCriteria, BinaryOperatorType.And, criteria);
            return sequence;
        }

        private OrmExpression VisitAllMethod (LinqExpr.MethodCallExpression expression) {
            // This is a very special case for queries of the form:
            // foo.Where(f => list.All(item => f.Collection.Any(c => c.Value == item)));

            var sequence = VisitSequenceMethod(expression);

            var negate = false;
            var allCriteria = (OperatorExpression) expression.Arguments[1].Accept(this);
            BinaryOperatorExpression allBinaryCriteria;
            if (allCriteria is UnaryOperatorExpression) {
                var unaryAllCriteria = (UnaryOperatorExpression) allCriteria;
                negate = unaryAllCriteria.Operator == UnaryOperatorType.Not;
                allBinaryCriteria = (BinaryOperatorExpression) unaryAllCriteria.Operand;
            } else
                allBinaryCriteria = (BinaryOperatorExpression) allCriteria;

            var collectionVariable = (VariableExpression) allBinaryCriteria.Left;

            var rootSequence = collectionVariable.Container;

            var containerTableMap = rootSequence.GetResultTableMap();

            // Split the combined query into multiple criteria for the container.
            var collectionQuery = (QueryExpression) allBinaryCriteria.Right;
            var collectionQueryCriteria = (BinaryOperatorExpression) collectionQuery.WhereCriteria;
            var collectionQueryProjectionQuery = ((ProjectionExpression) collectionQueryCriteria.Right).Parent;
            var externalCollection = (IEnumerable<object>) ((LiteralExpression) collectionQueryProjectionQuery.Subject).Value;

            // Deconstruct the criteria into a set of clauses.
            OperatorExpression result = null;
            foreach (var item in externalCollection) {
                var deconstructedQuery = new QueryExpression(collectionVariable.Container.Context, collectionQuery.Subject);
                deconstructedQuery.Projection.Items.AddRange(collectionQuery.Projection.Items);
                deconstructedQuery.WhereCriteria = new BinaryOperatorExpression(collectionQueryCriteria.Left, collectionQueryCriteria.Operator, new LiteralExpression(item));
                OperatorExpression deconstructedAllCriteria = new BinaryOperatorExpression(allBinaryCriteria.Left, allBinaryCriteria.Operator, deconstructedQuery);
                if (negate)
                    deconstructedAllCriteria = new UnaryOperatorExpression(deconstructedAllCriteria, UnaryOperatorType.Not);

                result = BinaryOperatorExpression.Join(result, BinaryOperatorType.And, deconstructedAllCriteria);
            }

            return result;
        }

        private OrmExpression VisitAnyMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = (QueryExpression) VisitWhereMethod(expression);

            // collection.Any()
            if (sequence.Subject is VariableExpression) {
                var subject = (VariableExpression) sequence.Subject;

                var containerTableMap = subject.Container.GetResultTableMap();
                var pathToCollection = containerTableMap.VariableMaps.FindAllParts(subject.Variable, VariableStructure.BaseContainers | VariableStructure.WithinComponents).ToArray();

                // Project the key of the collection.
                var collection = (CollectionVariableMap) pathToCollection.Last();
                var collectionKey = new VariableExpression(collection.KeyVariableMap.Variable, sequence);
                sequence.Projection.Items.Add(collectionKey);

                // Reset the subject.
                sequence.Subject = new TableExpression(collection.CollectionTableMap);

                // Ordering is not valid in a subselect.
                sequence.OrderBy.Clear();

                // Return an expression which uses this collection, using a new composite leading up
                // to the object the owns the collection.
                VariableInfo containerKey;
                if (pathToCollection.Length == 1)
                    containerKey = containerTableMap.PrimaryKey.VariableMap.Variable;
                else
                    containerKey = new CompositeVariableInfo(pathToCollection.Take(pathToCollection.Length - 1).Select(m => m.Variable).ToArray());

                return new BinaryOperatorExpression(new VariableExpression(containerKey, subject.Container), BinaryOperatorType.In, sequence);
            } 

            // query.Any()
            if (sequence.Subject is QueryExpression || sequence.Subject is TableExpression) {
                var resultTableMap = sequence.GetResultTableMap();
                //sequence.Projection.Items.Add(new LiteralExpression(1));
                sequence.Projection.Items.Add(new VariableExpression(resultTableMap.PrimaryKey.VariableMap.Variable, sequence));
                return new UnaryOperatorExpression(sequence, UnaryOperatorType.Exists);
            }

            throw new NotImplementedException(LS.T("Any expression is not supported against sequence '{0}' of type '{1}'", sequence, sequence.Subject.GetType().Name));
        }

        private OrmExpression VisitOrderByMethod (LinqExpr.MethodCallExpression expression, OrderDirection direction) {
            var sequence = VisitSequenceMethod(expression);
            var over = (VariableExpression) expression.Arguments[1].Accept(this);
            sequence.OrderBy.Add(new OrderExpression(direction, over));
            return sequence;
        }

        private OrmExpression VisitAdviceExpression (LinqExpr.MethodCallExpression expression, AdviceRequests advice) {
            var sequence = VisitSequenceMethod(expression);
            var target = expression.Arguments[1].Accept(this);

            if (target is VariableExpression) {
                var variable = (VariableExpression) target;
                sequence.Advice.Add(new AdviceExpression(advice, (VariableExpression) variable));
            } else if (target is QueryExpression) {
                // Sub-queries in advice occur when advice is applied to
                // collections within the initial advice suggestion.
                var query = (QueryExpression) target;
                sequence.Advice.AddRange(query.Advice);
                sequence.Advice.Add(new AdviceExpression(advice, (VariableExpression) query.Subject));
            } else
                throw new NotSupportedException(LS.T("Advice target of type '{0}' is not supported.", target.GetType()));

            return sequence;
        }

        private OrmExpression VisitSkipMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = VisitSequenceMethod(expression);

            var value = (int) ((LiteralExpression) expression.Arguments[1].Accept(this)).Value;
            if (value != 0) {
                if (sequence.Limit != null)
                    sequence.Limit = sequence.Limit - value;

                sequence.Offset = (sequence.Offset.HasValue ? sequence.Offset.Value : 0) + value;
            }

            return sequence;
        }

        private OrmExpression VisitTakeMethod (LinqExpr.MethodCallExpression expression) {
            var sequence = VisitSequenceMethod(expression);
            sequence.Limit = (int) ((LiteralExpression) expression.Arguments[1].Accept(this)).Value;
            return sequence;
        }
        #endregion

        #region Constants
        OrmExpression IExpressionVisitor<OrmExpression>.Visit (LinqExpr.ConstantExpression expression) {
            return ExtractLiteralValue(expression.Value);
        }

        private OrmExpression ExtractLiteralValue (object value) {
            if (value is IQueryable)
                return VisitConstantQuery((IQueryable) value);
            return new LiteralExpression(value);
        }

        private OrmExpression VisitConstantQuery (IQueryable query) {
            if (query is DataQuery) {
                return VisitDataQuery((DataQuery) query);
            }

            throw new NotSupportedException();
        }

        private OrmExpression VisitDataQuery (DataQuery dataQuery) {
            // If this is our initial expression, simply let the translator
            // run its course against the query subject.
            if (dataQuery.Expression == _initialExpression) {
                var query = new QueryExpression(dataQuery.Context);
                query.Subject = ((ITable) dataQuery).CreateQuerySubject();
                return query;
            }

            // If not, this is a subquery.  Recurse into another translator for it.
            return dataQuery.TranslateQuery();
        }
        #endregion

        #region Macros
        OrmExpression Macroexpand(LinqExpr.Expression expression)
        {
            while (expression is LinqExpr.MethodCallExpression)
            {
                var call = (LinqExpr.MethodCallExpression)expression;

                if (!call.Method.IsDefined(typeof(QueryMacroAttribute), true) ||
                    !call.Method.IsDefined(typeof(ExtensionAttribute), true) || 
                    call.Object != null)
                    break;

                var expander = call.Method.ReflectedType.GetMethod(call.Method.Name + "Macro");

                if (expander == null)
                    throw new NotSupportedException(LS.T("Cannot find macro expander for '{0}.{1}'.", call.Method.ReflectedType.Name, call.Method.Name));

                var args = new List<object>();
                var i = 0;

                foreach (var param in expander.GetParameters())
                {
                    if (typeof(LinqExpr.Expression).IsAssignableFrom(param.ParameterType))
                    {
                        args.Add(call.Arguments[i + 1]);
                    }
                    else
                    {
                        args.Add(LinqExpr.Expression.Lambda<Func<object>>(call.Arguments[i + 1]).Compile()());
                    }

                    ++i;
                }

                var expr = expander.Invoke(null, args.ToArray());

                if (!(expr is LinqExpr.LambdaExpression))
                    throw new InvalidOperationException(LS.T("Macro expander must return a lambda expression."));

                var lambda = (LinqExpr.LambdaExpression)expr;
                var substDict = new Dictionary<string, LinqExpr.Expression>();
                
                i = 0;
                foreach (var paramExpr in lambda.Parameters)
                    substDict[paramExpr.Name] = call.Arguments[i++];

                var subst = new SubstituteParametersTransform(substDict);

                expression = subst.Transform(lambda).Body;
            }

            return expression.Accept(this);
        }
        #endregion
        #endregion
    }

    internal class TranslatorState
    {
        public ExpressionFrame Frame { get; private set; }
        public LinqExpr.Expression ResumeExpression { get; private set; }
        public LinqExpr.Expression InitialExpression { get; private set; }

        public TranslatorState(ExpressionFrame frame, LinqExpr.Expression resumeExpression, LinqExpr.Expression initialExpression)
        {
            Frame = frame;
            ResumeExpression = resumeExpression;
            InitialExpression = initialExpression;
        }

        public OrmExpression Resume(MapConfiguration configuration)
        {
            var translator = new LinqExpressionTranslator(InitialExpression, configuration);
            translator._frame = Frame;
            return ResumeExpression.Accept(translator);
        }
    }

    internal class ExpressionFrame {
        private Dictionary<LinqExpr.ParameterExpression, OrmExpression> _parameterValues = new Dictionary<LinqExpr.ParameterExpression, OrmExpression>();

        public ExpressionFrame (ExpressionFrame parent) {
            Arguments = new Collection<OrmExpression>();
            Parent = parent;
        }

        public void SetParameterValue (LinqExpr.ParameterExpression parameter, OrmExpression value) {
            _parameterValues[parameter] = value;
        }

        public OrmExpression GetParameterValue (LinqExpr.ParameterExpression parameter) {
            if (_parameterValues.ContainsKey(parameter))
                return _parameterValues[parameter];

            if (Parent == null)
                return null;

            return Parent.GetParameterValue(parameter);
        }

        public Collection<OrmExpression> Arguments { get; private set; }
        public ExpressionFrame Parent { get; private set; }
    }

    internal class ExpressionPostProcessor : IOrmExpressionVisitor<OrmExpression> {
        private MapConfiguration _configuration;

        #region Constructors
        public ExpressionPostProcessor (MapConfiguration configuration) {
            _configuration = configuration;
        }
        #endregion

        #region Reference/Key Translation
        private object ExtractKey (object value) {
            if (value != null) {
                var type = value.GetType();
                if (Attribute.IsDefined(type, typeof(PersistentAttribute))) {
                    var tableMap = _configuration.TableMaps[type];
                    return tableMap.GetKey(value);
                }
            }

            return value;
        }
        #endregion

        #region IOrmExpressionVisitor<OrmExpression> Members
        public OrmExpression Visit (LiteralExpression literal) {
            // Replace literal references with references to primary keys.
            literal.Value = ExtractKey(literal.Value);
            return literal;
        }

        public OrmExpression Visit (QueryExpression query) {
            if (query.WhereCriteria != null)
                query.WhereCriteria.Accept(this);

            return query;
        }

        public OrmExpression Visit (ProjectionExpression projection) {
            // Replace projections with primary key variable expressions.
            var tableMap = projection.Parent.GetResultTableMap();
            var variableMap = tableMap.PrimaryKey.VariableMap;
            return new VariableExpression(variableMap.Variable, projection.Parent);
        }

        public OrmExpression Visit (AdviceExpression advice) {
            return advice;
        }

        public OrmExpression Visit (OrderExpression order) {
            return order;
        }

        public OrmExpression Visit (BinaryOperatorExpression binaryOperator) {
            binaryOperator.Left = binaryOperator.Left.Accept(this);
            binaryOperator.Right = binaryOperator.Right.Accept(this);
            return binaryOperator;
        }

        public OrmExpression Visit (UnaryOperatorExpression unaryOperator) {
            unaryOperator.Operand = unaryOperator.Operand.Accept(this);
            return unaryOperator;
        }

        public OrmExpression Visit (ApplyExpression apply) {
            return apply;
        }

        public OrmExpression Visit (VariableExpression variable) {
            return variable;
        }
        #endregion
    }
}
