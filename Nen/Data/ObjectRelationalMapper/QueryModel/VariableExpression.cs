using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

using Nen.Internal;
using Nen.Reflection;
using Nen.Runtime.Serialization;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryEngine;

namespace Nen.Data.ObjectRelationalMapper.QueryModel {
    /// <summary>
    /// Represents a variable ORM expression.
    /// </summary>
    [XDataContract]
    public class VariableExpression : OrmExpression {
        #region Constructors
        internal VariableExpression () {
        }

        /// <summary>
        /// Constructs a new VariableExpression.
        /// </summary>
        /// <param name="variable">The variable of the expression.</param>
        /// <param name="container">The container query of the variable.</param>
        public VariableExpression (VariableInfo variable, QueryExpression container) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            Container = container;
            Variable = variable;
        }

        /// <summary>
        /// Constructs a new VariableExpression.
        /// </summary>
        /// <param name="copy">The expression to copy.</param>
        protected VariableExpression (VariableExpression copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            Container = copy.Container;
            Variable = copy.Variable;
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Accepts a visitor and calls Visit(VariableExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }

        /// <summary>
        /// Deeply clones the expression.
        /// </summary>
        /// <returns>A deep clone of the expression.</returns>
        public override OrmExpression Clone () {
            return new VariableExpression(this);
        }

        public override TableMap GetResultTableMap () {
            if (Container == null)
                throw new InvalidOperationException(LS.T("Can't get the result table map of a free variable expression."));

            return Container.GetResultTableMap();
        }
        #endregion

        #region Container / Pathing Queries
        internal SelectQueryPlan GetSelectQueryPlan () {
            if (Container != null && Container.SelectQueryPlan != null)
                return Container.SelectQueryPlan;
            return null;
        }

        internal string GetPathName () {
            if (Container == null)
                return Variable.Name;
            
            var basePath = Container.GetPathName();
            return basePath == null ? Variable.Name : basePath + "." + Variable.Name;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the container query of the variable.
        /// </summary>
        [XDataMember]
        public QueryExpression Container { get; set; }

        /// <summary>
        /// Gets or sets the variable of the expression.
        /// </summary>
        [XDataMember]
        public VariableInfo Variable { get; set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Retrieves a string representation of the variable.
        /// </summary>
        /// <returns>A formatted version of the expression.</returns>
        public override string ToString () {
            return Variable.ToString();
        }

        /// <summary>
        /// Determines if the specified object is equal to this variable
        /// expression.
        /// </summary>
        /// <param name="obj">The object to check for equality.</param>
        /// <returns>true if the object is equal to this one, false if otherwise.</returns>
        public override bool ValueEquals (object obj) {
            var variableExpression = obj as VariableExpression;
            if (variableExpression == null)
                return false;

            return Variable.Equals(variableExpression.Variable);
        }
        #endregion
    }
}
