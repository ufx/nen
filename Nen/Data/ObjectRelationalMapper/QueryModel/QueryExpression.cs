using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Linq;

using Nen.Collections;
using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryEngine;
using Nen.Internal;
using Nen.Reflection;
using Nen.Runtime.Serialization;

namespace Nen.Data.ObjectRelationalMapper.QueryModel {
    /// <summary>
    /// Indicates the shape of data returned by a query.
    /// </summary>
    public enum QueryShape {
        /// <summary>
        /// The query returns a sequence of object results.
        /// </summary>
        Sequence,

        /// <summary>
        /// The query returns the only object result.
        /// </summary>
        Single,

        /// <summary>
        /// The query returns the only object result, or null.
        /// </summary>
        SingleOrDefault,

        /// <summary>
        /// The query returns the first object result in a sequence.
        /// </summary>
        First,

        /// <summary>
        /// The query returns the first object result in a sequence, or null.
        /// </summary>
        FirstOrDefault,
    }

    /// <summary>
    /// Represents an ORM query.
    /// </summary>
    [XDataContract]
    public class QueryExpression : OrmExpression, IQuerySource
#if NET4
        , ISerializationLifecycle
#endif
 {
        private Dictionary<string, AdviceRequests> _adviceByPath;

        #region Constructors
        internal QueryExpression () {
            Initialize();
        }

        /// <summary>
        /// Constructs a new QueryExpression.
        /// </summary>
        /// <param name="context">The context in which the query is run.</param>
        public QueryExpression (DataContext context) {
            Context = context;

            Initialize();
        }

        /// <summary>
        /// Constructs a new QueryExpression.
        /// </summary>
        /// <param name="context">The context in which the query is run.</param>
        /// <param name="subject">The subject of the query.</param>
        public QueryExpression (DataContext context, OrmExpression subject)
            : this(context) {
            Subject = subject;
        }

        /// <summary>
        /// Constructs a new QueryExpression.
        /// </summary>
        /// <param name="context">The context in which the query is run.</param>
        /// <param name="subjectTableMap">The subject table map of the query.</param>
        public QueryExpression (DataContext context, TableMap subjectTableMap)
            : this(context, new TableExpression(subjectTableMap)) {
        }

        /// <summary>
        /// Constructs a new copy of a QueryExpression.
        /// </summary>
        /// <param name="copy">The query to copy.</param>
        protected QueryExpression (QueryExpression copy)
            : base(copy) {

            Initialize();

            Context = copy.Context;
            IsDistinct = copy.IsDistinct;
            IsPossiblyAmbiguous = copy.IsPossiblyAmbiguous;
            Limit = copy.Limit;
            Offset = copy.Offset;
            Projection = (ProjectionExpression) copy.Projection.Clone();
            Projection.Parent = this;
            Shape = copy.Shape;
            Source = copy.Source;

            if (copy.Subject != null)
                Subject = copy.Subject.Clone();

            if (copy.WhereCriteria != null)
                WhereCriteria = (OperatorExpression) copy.WhereCriteria.Clone();

            foreach (var order in copy.OrderBy)
                OrderBy.Add((OrderExpression) order.Clone());

            foreach (var advice in copy.Advice)
                Advice.Add((AdviceExpression) advice.Clone());
        }

        private void Initialize () {
            Advice = new Collection<AdviceExpression>();
            OrderBy = new Collection<OrderExpression>();
            Projection = new ProjectionExpression(this);
            IsPossiblyAmbiguous = true;
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Deeply clones the query.
        /// </summary>
        /// <returns>A deep clone of the query.</returns>
        public override OrmExpression Clone () {
            return new QueryExpression(this);
        }

        /// <summary>
        /// Attempts to combine this query with another.  Only where criteria
        /// against the same subject and projection can be combined.
        /// </summary>
        /// <param name="expression">The expression to attempt a combination with.</param>
        /// <returns>true if the expression was combined into this expression, false if otherwise.</returns>
        public override bool TryCombine (OrmExpression expression) {
            var query = expression as QueryExpression;
            if (query == null)
                return false;

            if (!Subject.ValueEquals(query.Subject))
                return false;

            if (!Projection.ValueEquals(query.Projection))
                return false;

            if (Limit != query.Limit || Offset != query.Offset)
                return false;

            // There should be more checks here for completeness, but hopefully
            // this serves our purpose.

            // Combining with no criteria will remove the criteria.
            if (WhereCriteria == null || query.WhereCriteria == null) {
                WhereCriteria = null;
                return true;
            }

            return WhereCriteria.TryCombine(query.WhereCriteria);
        }

        /// <summary>
        /// Retrieves the resulting table map of the subject.
        /// </summary>
        /// <returns>The subject table map.</returns>
        public override TableMap GetResultTableMap () {
            return Subject.GetResultTableMap();
        }

        /// <summary>
        /// Accepts a visitor and calls Visit(QueryOperatorExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
        #endregion

        #region Advice
        internal AdviceRequests GetAdvice (Stack<VariableInfo> path, VariableMap variableMap) {
            // This is still not quite right - there may be multiple source queries, each with their own
            // advice that must be applied to the right chunk of path here.

            // Precedence (in order of what will be used):
            // Path projection
            // Current query advice
            // Source query advice
            // VariableMap advice

            var absolutePathName = GetPathName(path); // Includes full source of path.
            var relativePathName = absolutePathName;
            if (Source != null) {
                // Get the path relative to the current query only.
                var sourcePathName = GetPathName(Source.GetPath());
                if (sourcePathName != "")
                    relativePathName = absolutePathName.Substring(sourcePathName.Length + 1);
            }

            // Projection

            // If there is no projection, assume all variables are projected
            // and keep the default advice.  Otherwise, ignore variables that
            // do not appear in the list.  Defer variables found in the list,
            // as any information from that reference must be projected explicitly.
            if (Projection.Items.Count > 0) {
                var projectionAdvice = AdviceRequests.Ignore;
                var projectedVariables = Projection.Items.OfType<VariableExpression>();
                var projectedPaths = projectedVariables.Select(v => v.Variable.Name).ToList();

                if (projectedPaths.Count != 0) {
                    foreach (var projectedPath in projectedPaths) {
                        if (projectedPath.StartsWith(relativePathName)) {
                            if (relativePathName == projectedPath)
                                projectionAdvice = AdviceRequests.DeferJoin;
                            else
                                projectionAdvice = AdviceRequests.JoinOnly;
                        }
                    }
                }

                return projectionAdvice;
            }

            // Current query advice

            var currentQueryAdvice = GetAdviceCore(relativePathName);
            if (currentQueryAdvice != AdviceRequests.None)
                return currentQueryAdvice;
            
            // Root query advice

            if (Source != null) {
                QueryExpression rootQuery = Source.GetOriginQuery();
                while (rootQuery.Source != null)
                    rootQuery = rootQuery.Source.GetOriginQuery();

                var rootQueryAdvice = rootQuery.GetAdviceCore(absolutePathName);
                if (rootQueryAdvice != AdviceRequests.None)
                    return rootQueryAdvice;
            }

            // VariableMap advice

            return variableMap.Advice;
        }

        private static string GetPathName (Stack<VariableInfo> path) {
            return string.Join(".", new Stack<VariableInfo>(path).Select(v => v.Name));
        }

        private AdviceRequests GetAdviceCore (string pathName) {
            // Populate the path dictionary used to lookup advice.
            if (_adviceByPath == null) {
                _adviceByPath = new Dictionary<string, AdviceRequests>();
                foreach (var adviceExpression in Advice) {
                    var expressionPathName = adviceExpression.Variable.GetPathName();
                    _adviceByPath[expressionPathName] = adviceExpression.Requests;
                }
            }

            if (_adviceByPath.ContainsKey(pathName))
                return _adviceByPath[pathName];
            return AdviceRequests.None;
        }

        internal string GetPathName () {
            if (Subject is VariableExpression)
                return ((VariableExpression) Subject).GetPathName();
            return null;
        }
        #endregion

        #region Execution
        private DataQueryResults ExecuteResults () {
            Statistics = new QueryStatistics();
            return Context.Configuration.DefaultStore.Query(this);
        }

        private object ExecuteCore()
        {
            // Circumvent this query if it's cacheable.
            var resultTableMap = GetResultTableMap();
            var cache = Context.Cache;
            var cacheOptions = resultTableMap.OriginTypeMap.CacheOptions;
            string cacheKey = null;
            if (cache != null && cacheOptions != null)
            {
                // todo: cache key may conflict with anon types from different assemblies
                // that each request the same fields from the same type.

                cacheKey = "Nen.Data.ObjectRelationalMapper.Query: " + ToString();
                var cachedValue = cache.Get(cacheKey);
                if (cachedValue != null)
                    return cachedValue;
            }

            // Not cached, execute the query.
            Context.OnQueryExecuting(this);
            var results = ExecuteResults();
            var value = results.GetResults();

            // Run the result expression if applicable.
            var resultState = Projection.ResultTranslatorState;
            if (Projection.ResultTranslatorState != null)
            {
                Projection.Results = value;
                var literal = (LiteralExpression) Projection.ResultTranslatorState.Resume(Context.Configuration);
                value = literal.Value;
            }

            if (cache != null && cacheOptions != null)
                cache.Insert(cacheKey, value, cacheOptions);

            Context.OnQueryExecuted(this, results, value);
            return value;
        }

        internal object ExecuteScalar () {
            return ExecuteCore();
        }

        internal IEnumerable<object> ExecuteSequence () {
            return (IEnumerable<object>)ExecuteCore();
        }

        internal object ExecuteSingleton () {
            if (IsScalar)
                return ExecuteScalar();

            var results = ExecuteSequence();

            switch (Shape) {
                case QueryShape.First:
                    return results.First();

                case QueryShape.FirstOrDefault:
                    return results.FirstOrDefault();

                case QueryShape.Single:
                    return results.Single();

                case QueryShape.SingleOrDefault:
                    return results.SingleOrDefault();

                default:
                    throw new QueryException(LS.T("ExecuteSingleton called against an unknown query shape."));
            }
        }
        #endregion

        #region IQuerySource
        public Stack<VariableInfo> GetPath () {
            return Source == null ? new Stack<VariableInfo>() : Source.GetPath();
        }

        public QueryExpression GetOriginQuery () {
            return Source == null ? null : Source.GetOriginQuery();
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the query advice.
        /// </summary>
        [XDataMember]
        public Collection<AdviceExpression> Advice { get; private set; }

        /// <summary>
        /// Gets the context in which the query is run.
        /// </summary>
        public DataContext Context { get; private set; }

        /// <summary>
        /// Gets or sets a limit on the number of results queried.
        /// </summary>
        [XDataMember]
        public int? Limit { get; set; }

        /// <summary>
        /// Gets or sets an offset into the query results.
        /// </summary>
        [XDataMember]
        public int? Offset { get; set; }

        internal IQuerySource Source { get; set; }

        /// <summary>
        /// Gets the query projection.  If no projection is present, all
        /// values are queried.
        /// </summary>
        [XDataMember]
        public ProjectionExpression Projection { get; set; }

        /// <summary>
        /// Gets an indicator on whether the query is generated by internal
        /// methods, usually to fill a lazy load request.
        /// </summary>
        public bool IsGenerated { get; internal set; }

        /// <summary>
        /// Gets or sets an indicator on whether the query is possibly
        /// ambiguous.  Ambiguous queries require an extra database trip to
        /// determine the actual result type.
        /// </summary>
        [XDataMember]
        public bool IsPossiblyAmbiguous { get; set; }

        internal SelectQueryPlan SelectQueryPlan { get; set; }

        /// <summary>
        /// Gets or sets the shape of data returned.
        /// </summary>
        [XDataMember(XDataType.Attribute)]
        public QueryShape Shape { get; set; }

        /// <summary>
        /// Gets or sets whether the query returns distinct values.  Can only be used with scalar queries.
        /// </summary>
        [XDataMember(XDataType.Element)]
        public bool IsDistinct { get; set; }

        /// <summary>
        /// Gets or sets whether the query returns a single primitive value.
        /// </summary>
        [XDataMember(XDataType.Element)]
        public bool IsScalar { get; set; }

        /// <summary>
        /// Gets or sets statistics on the query operation.
        /// </summary>
        public QueryStatistics Statistics { get; set; }

        /// <summary>
        /// Gets or sets the subject of the query.
        /// </summary>
        [XDataMember]
        public OrmExpression Subject { get; set; }

        /// <summary>
        /// Gets or sets the where clause.
        /// </summary>
        [XDataMember]
        public OperatorExpression WhereCriteria { get; set; }

        /// <summary>
        /// Gets or sets the ordering specifiers.
        /// </summary>
        [XDataMember]
        public Collection<OrderExpression> OrderBy { get; set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Formats the query for display to a developer.
        /// </summary>
        /// <returns>The formatted query.</returns>
        public override string ToString () {
            //fixme
            var sb = new StringBuilder();

            if (Projection != null && Projection.Items.Count > 0)
                sb.Append("select " + Projection.ToString());

            if (sb.Length > 0)
                sb.Append(" ");

            sb.Append("from " + Subject.ToString());

            if (WhereCriteria != null)
                sb.Append(" where " + WhereCriteria.ToString());

            if (Limit != null)
                sb.Append(" limit " + Limit.Value);

            if (Offset != null)
                sb.Append(" offset " + Offset.Value);
            
            if (OrderBy != null && OrderBy.Count > 0)
                sb.Append(" orderby " + string.Join(", ", OrderBy));

            if (Advice != null && Advice.Count > 0)
                sb.Append(" advice " + string.Join(", ", Advice));

            if (IsDistinct)
                sb.Append(" distinct");

            if (IsScalar)
                sb.Append(" scalar");
            else if (Shape != QueryShape.Sequence)
                sb.Append(" " + Shape.ToString());

            return sb.ToString();
        }
        #endregion

        #region ISerializationLifecycle Members
#if NET4
        void ISerializationLifecycle.AfterReadXml (XSerializer serializer, XmlReader reader) {
            Context = (DataContext) serializer.State;
        }
#endif
        #endregion
    }

    [XDataContract]
    public class ProjectionExpression : OrmExpression {
        #region Constructors
        internal ProjectionExpression () {
            Items = new Collection<OrmExpression>();
        }

        public ProjectionExpression (QueryExpression parent) {
            Items = new Collection<OrmExpression>();
            Parent = parent;
        }

        protected ProjectionExpression (ProjectionExpression copy)
            : base(copy) {

            Items = new Collection<OrmExpression>();
            Parent = copy.Parent;

            foreach (var item in copy.Items)
                Items.Add(item.Clone());
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Deeply clones the query.
        /// </summary>
        /// <returns>A deep clone of the query.</returns>
        public override OrmExpression Clone () {
            return new ProjectionExpression(this);
        }

        /// <summary>
        /// Accepts a visitor and calls Visit(ProjectionExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the projected items to query.  If no projection is
        /// present, all variables on the table will be projected.
        /// </summary>
        [XDataMember]
        public Collection<OrmExpression> Items { get; private set; }

        [XDataMember]
        public QueryExpression Parent { get; internal set; }

        internal TranslatorState ResultTranslatorState { get; set; }

        internal object Results { get; set; }
        #endregion

        #region Object Members
        public override string ToString () {
            return Items.Count == 0 ? "<All>" : string.Join(", ", Items);
        }

        public override bool ValueEquals (object obj) {
            var projection = obj as ProjectionExpression;
            if (projection == null)
                return false;

            return Items.SequenceEqual(projection.Items);
        }
        #endregion
    }

    /// <summary>
    /// Represents an ORM table.
    /// </summary>
    [XDataContract]
    public class TableExpression : OrmExpression
#if NET4
        , ICustomSerializable
#endif
    {
        #region Constructors
        public TableExpression () {
        }

        /// <summary>
        /// Constructs a new TableExpression.
        /// </summary>
        /// <param name="tableMap">The table map referenced by the table.</param>
        public TableExpression (TableMap tableMap) {
            if (tableMap == null)
                throw new ArgumentNullException("tableMap");

            TableMap = tableMap;
        }

        /// <summary>
        /// Constructs a new copy of a TableExpression.
        /// </summary>
        /// <param name="copy">The table to copy.</param>
        protected TableExpression (TableExpression copy)
            : base(copy) {
            TableMap = copy.TableMap;
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Accepts a visitor and calls Visit(TableExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return default(T); // Does nothing.
        }

        /// <summary>
        /// Clones the table.
        /// </summary>
        /// <returns>A clone of the table.</returns>
        public override OrmExpression Clone () {
            return new TableExpression(this);
        }

        /// <summary>
        /// Retrieves the table map referenced by the table.
        /// </summary>
        /// <returns></returns>
        public override TableMap GetResultTableMap () {
            return TableMap;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the table map referenced by the table.
        /// </summary>
        public TableMap TableMap { get; private set; }
        #endregion

        #region Object Members
        /// <summary>
        /// Determines if the specified object is equal to this table
        /// expression. 
        /// </summary>
        /// <param name="obj">The object to check for equality.</param>
        /// <returns>true if the object is equal to this one, false if otherwise.</returns>
        public override bool ValueEquals (object obj) {
            var table = obj as TableExpression;
            if (table == null)
                return false;

            return TableMap == table.TableMap;
        }

        /// <summary>
        /// Returns the table map converted to a string.
        /// </summary>
        /// <returns>The table map string value.</returns>
        public override string ToString () {
            return TableMap.ToString();
        }
        #endregion

        #region ICustomSerializable Members
#if NET4
        void ICustomSerializable.ReadXml (XSerializer serializer, XmlReader reader, string containerElementName) {
            var context = (DataContext) serializer.State;
            var tableName = reader.GetAttribute("Name");
            TableMap = context.Configuration.TableMapsByTableName[tableName];
        }

        void ICustomSerializable.WriteXml (XSerializer serializer, XmlWriter writer) {
            writer.WriteAttributeString("Name", TableMap.TableName);
        }
#endif
        #endregion
    }

    [XDataContract]
    public class ProcedureExpression : OrmExpression {
        #region Constructors
        public ProcedureExpression (TableExpression table, string procedureName, object[] procedureArgs) {
            if (table == null)
                throw new ArgumentNullException("table");

            if (procedureName == null)
                throw new ArgumentNullException("procedureName");

            Table = table;
            ProcedureName = procedureName;
            ProcedureArguments = procedureArgs;
        }

        protected ProcedureExpression (ProcedureExpression copy)
            : base(copy) {
            Table = copy.Table;
            ProcedureName = copy.ProcedureName;
            ProcedureArguments = copy.ProcedureArguments;
        }
        #endregion

        #region OrmExpression Operations
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return default(T); // Does nothing.
        }

        public override OrmExpression Clone () {
            return new ProcedureExpression(this);
        }

        public override TableMap GetResultTableMap () {
            return Table.TableMap;
        }
        #endregion

        #region Accessors
        [XDataMember]
        public TableExpression Table { get; private set; }

        [XDataMember]
        public string ProcedureName { get; private set; }

        [XDataMember]
        public object[] ProcedureArguments { get; private set; }
        #endregion

        #region Object Members
        public override bool ValueEquals (object obj) {
            var procedure = obj as ProcedureExpression;
            if (procedure == null)
                return false;

            return Table.ValueEquals(procedure.Table)
                && ProcedureName == procedure.ProcedureName
                && ProcedureArguments.SequenceEqual(procedure.ProcedureArguments);
        }

        public override string ToString () {
            return Table.ToString() + " " + ProcedureName + "(" + string.Join(",", ProcedureArguments) + ")";
        }
        #endregion
    }

    [XDataContract]
    public class AdviceExpression : OrmExpression {
        #region Constructors
        internal AdviceExpression () {
        }

        public AdviceExpression (AdviceRequests advice, VariableExpression variable) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            Requests = advice;
            Variable = variable;
        }

        protected AdviceExpression (AdviceExpression copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            Requests = copy.Requests;
            Variable = (VariableExpression) copy.Variable.Clone();
        }
        #endregion

        #region OrmExpression Operations
        public override OrmExpression Clone () {
            return new AdviceExpression(this);
        }

        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }
        #endregion

        #region Accessors
        [XDataMember(XDataType.Attribute)]
        public AdviceRequests Requests { get; private set; }

        [XDataMember]
        public VariableExpression Variable { get; private set; }
        #endregion

        #region Object Members
        public override string ToString () {
            return Requests.ToString() + " " + Variable.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Represents an order specifier.
    /// </summary>
    [XDataContract]
    public class OrderExpression : OrmExpression {
        #region Constructors
        internal OrderExpression () {
        }

        /// <summary>
        /// Constructs a new OrderExpression.
        /// </summary>
        /// <param name="direction">The direction of the ordering.</param>
        /// <param name="over">The expression to order over.</param>
        public OrderExpression (OrderDirection direction, OrmExpression over) {
            if (over == null)
                throw new ArgumentNullException("over");

            Direction = direction;
            Over = over;
        }

        /// <summary>
        /// Constructs a new copy of an OrderExpression.
        /// </summary>
        /// <param name="copy">The query to copy.</param>
        protected OrderExpression (OrderExpression copy)
            : base(copy) {
            if (copy == null)
                throw new ArgumentNullException("copy");

            Direction = copy.Direction;
            Over = copy.Over;
        }
        #endregion

        #region OrmExpression Operations
        /// <summary>
        /// Deeply clones the order specifier.
        /// </summary>
        /// <returns>A deep clone of the order specifier.</returns>
        public override OrmExpression Clone () {
            return new OrderExpression(this);
        }

        /// <summary>
        /// Accepts a visitor and calls Visit(OrderExpression).
        /// </summary>
        /// <typeparam name="T">The return type of the visitor.</typeparam>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The result of the visit operation.</returns>
        public override T Accept<T> (IOrmExpressionVisitor<T> visitor) {
            return visitor.Visit(this);
        }

        #endregion

        #region Accessors
        /// <summary>
        /// The direction of the ordering.
        /// </summary>
        [XDataMember(XDataType.Attribute)]
        public OrderDirection Direction { get; private set; }

        /// <summary>
        /// The expression to order over.
        /// </summary>
        [XDataMember]
        public OrmExpression Over { get; private set; }
        #endregion

        #region Object Members
        public override string ToString () {
            return Over.ToString() + " " + Direction.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Specifies the direction of the ordering.
    /// </summary>
    public enum OrderDirection {
        /// <summary>
        /// Order values from lowest to highest.
        /// </summary>
        Ascending,

        /// <summary>
        /// Order values from highest to lowest.
        /// </summary>
        Descending
    };

    internal interface IQuerySource {
        Stack<VariableInfo> GetPath ();
        QueryExpression GetOriginQuery ();
    }
}
