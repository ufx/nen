using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper.Mapping;
using Nen.Data.ObjectRelationalMapper.QueryEngine;
using Nen.Data.ObjectRelationalMapper.QueryModel;
using Nen.Internal;

namespace Nen.Data.ObjectRelationalMapper {
    /// <summary>
    /// Stores the results of a query, with the ability to map the data
    /// contained within to a sequence of objects.
    /// </summary>
    public abstract class DataQueryResults {
        #region Constructors
        internal DataQueryResults (QueryExpression query) {
            Data = new LightSet();
            Query = query;
        }
        #endregion

        #region Data Operations
        internal bool ContainsKey (TableMap tableMap, object key) {
            return GetTable(tableMap).FindRowByPrimaryKey(key) != null;
        }

        internal IEnumerable<Tuple<LightRow, TableMap>> EachRowTableMap (TableMap tableMap, object key) {
            foreach (var currentTableMap in tableMap.GetHierarchy()) {
                var physicalTableMap = currentTableMap;
                if (physicalTableMap.IsOptimizedOutForReads)
                    physicalTableMap = physicalTableMap.GetUpdateHierarchy().First();

                if (!Data.ContainsTable(physicalTableMap.TableName))
                    throw new QueryException(LS.T("Couldn't find referenced table '{0}' in the result set.", physicalTableMap));

                var table = Data[physicalTableMap.TableName];
                LightRow row;

                // When the table has no primary key, this key should be a
                // row index.
                if (physicalTableMap.PrimaryKey is NoPrimaryKey)
                    row = table[(int) key];
                else
                    row = table.FindRowByPrimaryKey(key);

                if (row == null)
                    throw new QueryException(LS.T("Can't find referenced key '{0}' on table '{1}'.", key, physicalTableMap));

                yield return System.Tuple.Create(row, currentTableMap);
            }
        }

        internal LightTable GetTable (TableMap tableMap) {
            if (tableMap == null)
                throw new ArgumentNullException("tableMap");

            var referenceTableMap = tableMap.ReferenceBase ? tableMap.BasePhysicalTableMap : tableMap;
            return GetTableCore(referenceTableMap);
        }

        internal LightTable GetTableCore (TableMap tableMap) {
            if (tableMap == null)
                throw new ArgumentNullException("tableMap");

            if (Data.ContainsTable(tableMap.TableName))
                return Data[tableMap.TableName];

            var table = tableMap.CreateLightTable();
            Data.AddTable(table);
            return table;
        }
        #endregion

        #region Retrieval
        /// <summary>
        /// Gets the results of the query.
        /// </summary>
        /// <returns>The query results as scalar data or object instances.</returns>
        public abstract object GetResults ();
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the raw data associated with the query results.
        /// </summary>
        public LightSet Data { get; private set; }

        /// <summary>
        /// Gets the query that yielded this result.
        /// </summary>
        public QueryExpression Query { get; private set; }
        #endregion
    }

    internal class SequenceDataQueryResults : DataQueryResults {
        #region Constructors
        public SequenceDataQueryResults (QueryExpression query)
            : base(query) {
            FillResults = new List<DbFillResult>();
        }
        #endregion

        #region Retrieval
        public override object GetResults () {
            var mapper = new ResultMapper(this);
            return mapper.Map();
        }
        #endregion

        #region Accessors
        public List<DbFillResult> FillResults { get; private set; }
        #endregion
    }

    internal class ScalarDataQueryResults : DataQueryResults {
        #region Constructors
        internal ScalarDataQueryResults (QueryExpression query)
            : base(query) {
        }
        #endregion

        #region Retrieval
        public override object GetResults () {
            return FillResult.Value;
        }
        #endregion

        #region Accessors
        public ScalarDbFillResult FillResult { get; set; }
        #endregion
    }
}
