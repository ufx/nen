﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Internal;

namespace Nen.Data.OleDb {
    public class OleDatabase : Database {
        #region Constructors
        public OleDatabase (string connectionString)
            : base(connectionString, CultureInfo.CurrentCulture) {
        }
        #endregion

        #region Static Constructors
        public static OleDatabase NewCommaSeparatedValueDatabase (string directoryName, bool firstRowContainsHeaders) {
            return NewDelimitedDatabase(directoryName, firstRowContainsHeaders, ",");
        }

        public static OleDatabase NewDelimitedDatabase (string directoryName, bool firstRowContainsHeaders, string delimiter) {
            var header = firstRowContainsHeaders ? "YES" : "NO";
            return new OleDatabase("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + directoryName + ";Extended Properties='Text;HDR=" + header + ";FMT=Delimited(" + delimiter + ")';");
        }
        #endregion

        #region Generation
        protected override IDbCommand CreateCommandCore () {
            return new OleDbCommand();
        }

        public override IDbConnection CreateConnection()
        {
            return new OleDbConnection();
        }

        public override IDataAdapter CreateDataAdapter (IDbCommand cmd) {
            return new OleDbDataAdapter((OleDbCommand) cmd);
        }

        protected override IDbDataParameter[] DeriveParameters (string procedureName) {
            throw new NotImplementedException(LS.T("This database does not support parameter derivation."));
        }
        #endregion
    }
}
