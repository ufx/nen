using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Data;
using System.Text;

namespace Nen.Data {
    /// <summary>
    /// Speeds up some data operations by providing alternative access methods
    /// that cache slower methods.
    /// </summary>
    public class CachingDataSet {
        private Dictionary<string, DataTable> _tables = new Dictionary<string, DataTable>();

        #region Constructors
        /// <summary>
        /// Constructs a new CachingDataSet using the current culture.
        /// </summary>
        public CachingDataSet ()
            : this(CultureInfo.CurrentCulture) {
        }

        /// <summary>
        /// Constructs a new CachingDataSet.
        /// </summary>
        /// <param name="locale">The locale of the DataSet.</param>
        public CachingDataSet (CultureInfo locale) {
            Value = new DataSet();
            Value.Locale = locale;
            Initialize();
        }

        /// <summary>
        /// Constructs a new CachingDataSet.
        /// </summary>
        /// <param name="value">The DataSet to wrap.</param>
        public CachingDataSet (DataSet value) {
            Value = value;
            Initialize();
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The wrapped DataSet.
        /// </summary>
        public DataSet Value { get; private set; }
        #endregion

        #region Initialization
        private void Initialize () {
            Value.Tables.CollectionChanged += (sender, e) => {
                var table = (DataTable) e.Element;

                if (e.Action == CollectionChangeAction.Remove)
                    _tables.Remove(table.TableName);
                else if (e.Action == CollectionChangeAction.Add)
                    _tables[table.TableName] = table;
            };

            foreach (DataTable table in Value.Tables)
                _tables[table.TableName] = table;
        }
        #endregion

        #region Table Caching
        /// <summary>
        /// Looks up the table with the specified name in a dictionary.
        /// </summary>
        /// <param name="tableName">The name of the table to find.</param>
        /// <returns>The DataTable with the specified name.</returns>
        public DataTable GetTable (string tableName) {
            return _tables[tableName];
        }

        /// <summary>
        /// Determines whether or not the table with the specified name exists in the DataSet.
        /// </summary>
        /// <param name="tableName">The name of the table to find.</param>
        /// <returns>true if the table was found, false if otherwise.</returns>
        public bool ContainsTable (string tableName) {
            return _tables.ContainsKey(tableName);
        }
        #endregion
    }
}
