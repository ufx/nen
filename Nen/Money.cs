﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Data.ObjectRelationalMapper;

namespace Nen {
    [Persistent, Component]
    public struct Money {
        public decimal Value { get; set; }

        public Money(decimal value): this() { Value = value; }

        #region Operators

        public static implicit operator decimal (Money money) {
            return money.Value;
        }

        public static implicit operator Money (decimal value) {
            return new Money { Value = value };
        }

        public static bool operator == (Money lhs, Money rhs) {
            return lhs.Value == rhs.Value;
        }

        public static bool operator != (Money lhs, Money rhs) {
            return lhs.Value != rhs.Value;
        }

        public static bool operator > (Money lhs, Money rhs) {
            return lhs.Value > rhs.Value;
        }

        public static bool operator >= (Money lhs, Money rhs) {
            return lhs.Value >= rhs.Value;
        }

        public static bool operator < (Money lhs, Money rhs) {
            return lhs.Value < rhs.Value;
        }

        public static bool operator <= (Money lhs, Money rhs) {
            return lhs.Value <= rhs.Value;
        }

        #endregion

        #region Object Members
        public override int GetHashCode () {
            return Value.GetHashCode();
        }

        public override bool Equals (object obj) {
            if (!(obj is Money))
                return false;

            return this == (Money) obj;
        }
        #endregion
    }
}
