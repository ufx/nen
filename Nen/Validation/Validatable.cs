using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Nen.Validation {
    /// <summary>
    /// Implements IValidatable methods using ReflectiveValidator.
    /// </summary>
    public abstract class Validatable: IValidatable {
        /// <summary>
        /// Uses Validator to validate the current object.
        /// </summary>
        /// <returns>The error collection returned by Validator.</returns>
        public virtual Collection<ValidationResult> Validate () {
            var results = new Collection<ValidationResult>();
            Validator.TryValidateObject(this, new ValidationContext(this, null, null), results, true);
            return results;
        }
    }
}
