﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Reflection;
using System.Text.RegularExpressions;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace Nen.Validation {
    /// <summary>
    /// Convenience methods to access validation metadata.
    /// </summary>
    public static class ValidationInfo {
        /// <summary>
        /// Gets the MinimumLengthAttribute value, or null.
        /// </summary>
        /// <param name="mi">The member on which the attribute may be declared.</param>
        /// <returns>The value of the attribute, or null.</returns>
        public static int? MinimumLength (MemberInfo mi) {
            var attr = mi.GetAttribute<StringLengthAttribute>(true);
            return attr != null ? (int?) attr.MinimumLength : null;
        }

        /// <summary>
        /// Gets the MaximumLengthAttribute value, or null.
        /// </summary>
        /// <param name="mi">The member on which the attribute may be declared.</param>
        /// <returns>The value of the attribute, or null.</returns>
        public static int? MaximumLength (MemberInfo mi) {
            var attr = mi.GetAttribute<StringLengthAttribute>(true);
            return attr != null ? (int?) attr.MaximumLength : null;
        }

        /// <summary>
        /// Gets the MinimumValueAttribute value, or null.
        /// </summary>
        /// <param name="mi">The member on which the attribute may be declared.</param>
        /// <returns>The value of the attribute, or null.</returns>
        public static object MinimumValue (MemberInfo mi) {
            var attr = mi.GetAttribute<RangeAttribute>(true);
            return attr != null ? attr.Minimum : null;
        }

        /// <summary>
        /// Gets the MaximumValueAttribute value, or null.
        /// </summary>
        /// <param name="mi">The member on which the attribute may be declared.</param>
        /// <returns>The value of the attribute, or null.</returns>
        public static object MaximumValue (MemberInfo mi) {
            var attr = mi.GetAttribute<RangeAttribute>(true);
            return attr != null ? attr.Maximum : null;
        }

        /// <summary>
        /// Gets the RegexAttribute pattern, or null.
        /// </summary>
        /// <param name="mi">The member on which the attribute may be declared.</param>
        /// <returns>The regex pattern, or null.</returns>
        public static Regex Regex (MemberInfo mi) {
            var attr = mi.GetAttribute<RegularExpressionAttribute>(true);
            return attr != null ? new Regex(attr.Pattern) : null;
        }

        /// <summary>
        /// Gets an indicator on whether the member marked required.
        /// </summary>
        /// <param name="mi">The member on which the attribute may be declared.</param>
        /// <returns>true if the member is required, false if otherwise.</returns>
        public static bool IsRequired (MemberInfo mi) {
            return mi.GetAttribute<RequiredAttribute>(true) != null;
        }
    }
}
