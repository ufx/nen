using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel.DataAnnotations;

using Nen.Reflection;

namespace Nen.Validation {
    /// <summary>
    /// Allows objects to be validated.
    /// </summary>
    public interface IValidatable {
        /// <summary>
        /// Validates the object.
        /// </summary>
        /// <returns>A collection of the results of the validation.</returns>
        Collection<ValidationResult> Validate ();
    }
}
