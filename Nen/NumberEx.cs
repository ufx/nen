﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen {
    /// <summary>
    /// Extensions to the number classes (System.Int32, etc).
    /// </summary>
    public static class NumberEx {
        /// <summary>
        /// Returns an enumerable containing values from start, up to and including end.
        /// </summary>
        /// <param name="start">The integer to start with.</param>
        /// <param name="end">The integer to end with.</param>
        /// <returns>An enumerable of values from start, up to and including end.</returns>
        public static IEnumerable<int> UpTo (this int start, int end) {
            for (var i = start; i <= end; ++i)
                yield return i;
        }
    }
}
