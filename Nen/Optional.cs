﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen.Internal;

namespace Nen {
    /// <summary>
    /// Optionals record whether or not a value has been provided by an
    /// external source.
    /// </summary>
    /// <typeparam name="T">The type of optional value.</typeparam>
    public struct Optional<T> {
        private T _value;
        private bool _isProvided;

        #region Constructors
        /// <summary>
        /// Constructs a new optional.
        /// </summary>
        /// <param name="value">The provided value.</param>
        public Optional (T value) {
            _value = value;
            _isProvided = true;
        }
        #endregion

        #region Conversion Operators
        /// <summary>
        /// Creates a new Optional with the provided value.
        /// </summary>
        /// <param name="value">The provided value.</param>
        /// <returns>An optional with the provided value.</returns>
        public static implicit operator Optional<T> (T value) {
            return new Optional<T>(value);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets whether the value has been provided.  When setting
        /// this property false, any previously provided values will be
        /// forgotten.
        /// </summary>
        public bool IsProvided {
            get { return _isProvided; }
            set {
                if (!value)
                    _value = default(T);

                _isProvided = value;
            }
        }

        /// <summary>
        /// Gets or sets a provided value.
        /// </summary>
        public T Value {
            get {
                if (!IsProvided)
                    throw new InvalidOperationException(LS.T("The optional value has not been provided."));

                return _value;
            }
            set {
                _value = value;
                IsProvided = true;
            }
        }
        #endregion
    }
}
