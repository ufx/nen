using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

using Nen.Reflection;

namespace Nen
{
    /// <summary>
    /// Extensions to the System.Convert class.
    /// </summary>
    public static class ConvertEx
    {
        /// <summary>
        /// Makes every effort to change the given value to the specified type.
        /// This is primarily used for weakly typed code that must accept a
        /// variety of input types as gracefully as possible.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The desired destination type.</param>
        /// <returns>The converted value if conversion is possible.</returns>
        public static object ChangeWeakType(object value, Type destinationType)
        {
            if (value == null)
                return value;

            var valueType = value.GetType();

            // Unwrap the destination and value types.
            var finalDestinationType = destinationType.UnwrapIfNullable();

            if (typeof(IFuture).IsAssignableFrom(valueType) && !(typeof(IFuture).IsAssignableFrom(finalDestinationType)))
            {
                valueType = valueType.GetGenericArguments()[0];
                value = ((IFuture)value).Resolve();
            }

            // Find and convert to the appropriate value.
            if (valueType == finalDestinationType)
                return value;

            if (finalDestinationType == typeof(bool))
            {
                if (valueType == typeof(byte))
                    return Convert.ToBoolean((byte)value);
                else if (valueType == typeof(string))
                {
                    if (Equals(value, "0") || Equals(value, "N"))
                        return false;
                    
                    if (Equals(value, "1") || Equals(value, "Y"))
                        return true;
                }
            }
            else if (finalDestinationType == typeof(int))
            {
                if (valueType == typeof(decimal))
                    return Convert.ToInt32(value, CultureInfo.InvariantCulture);

            }
            else if (finalDestinationType == typeof(decimal))
            {
                if (valueType == typeof(int))
                    return Convert.ToDecimal(value, CultureInfo.InvariantCulture);

            }
            else if (finalDestinationType == typeof(TimeSpan))
            {
                if (valueType == typeof(Int64))
                    return new TimeSpan((Int64)value);
                if (valueType == typeof(DateTime))
                    return ((DateTime)value).TimeOfDay;

            }
            else if (finalDestinationType == typeof(char))
            {
                if (valueType == typeof(string))
                {
                    string stringValue = (string)value;
                    if (stringValue.Length == 1)
                        return stringValue[0];
                }
            }
            else if (finalDestinationType == typeof(Guid))
            {
                if (valueType == typeof(string))
                    return new Guid((string)value);
                if (valueType == typeof(byte[]))
                    return new Guid((byte[])value);
            }
            else if (finalDestinationType.IsEnum && value != null)
            {
                if (valueType == typeof(string))
                    return Enum.Parse(finalDestinationType, (string)value, true);

                if (valueType == typeof(Int32)
                    || valueType == typeof(Int16)
                    || valueType == typeof(byte))
                    return Enum.ToObject(finalDestinationType, value);
            }

            // When the values are assignable, just return the unchanged value.
            if (finalDestinationType.IsAssignableFrom(valueType))
                return value;

            // Last ditch conversion.
            return Convert.ChangeType(value, finalDestinationType, CultureInfo.InvariantCulture);
        }
    }
}
