using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Nen.Collections {
    /// <summary>
    /// Allows collection changes to be observed via events.
    /// </summary>
    public interface IObservableCollection : System.Collections.ICollection {
        /// <summary>
        /// Raised whenever items are inserted or removed from the collection.
        /// </summary>
        event EventHandler<CollectionChangedEventArgs> Changed;
    }

    /// <summary>
    /// Allows collection changes to be observed via events.
    /// </summary>
    /// <typeparam name="T">The type contained by the collection.</typeparam>
    public interface IObservableCollection<T> : IObservableCollection, ICollection<T> {
        /// <summary>
        /// Raised whenever items are inserted or removed from the collection.
        /// </summary>
        new event EventHandler<CollectionChangedEventArgs<T>> Changed;
    }

    /// <summary>
    /// A Collection that publishes change events whenever items are inserted
    /// or removed.
    /// </summary>
    /// <remarks>This class accounts for one fatal flaw in the event interface
    /// of System.ComponentModel.BindingList: Whenever an item is changed
    /// through the indexer, e.g., foo[2] = bar, the published event args
    /// neglect to include the item that is effectively removed from the list,
    /// and that item is no longer accessible from within the list.  This is a
    /// significant limitation for many framework consumers that could result
    /// in resource leaks, so IBindingList is avoided where possible.</remarks>
    /// <typeparam name="T">The type contained by the collection.</typeparam>
    public class ObservableCollection<T> : Collection<T>, IObservableCollection<T> {
        private Dictionary<EventHandler<CollectionChangedEventArgs>, EventHandler<CollectionChangedEventArgs<T>>> _convertedChangedDelegates
            = new Dictionary<EventHandler<CollectionChangedEventArgs>, EventHandler<CollectionChangedEventArgs<T>>>();

        #region Constructors
        /// <summary>
        /// Constructs a new ObservableCollection.
        /// </summary>
        public ObservableCollection () {
        }

        /// <summary>
        /// Constructs a new ObservableCollection.
        /// </summary>
        /// <param name="list">The list to wrap for the collection.</param>
        public ObservableCollection (IList<T> list)
            : base(list) {
        }
        #endregion

        #region Events
        /// <summary>
        /// Raised whenever items are inserted or removed from the collection.
        /// </summary>
        public event EventHandler<CollectionChangedEventArgs<T>> Changed;

        event EventHandler<CollectionChangedEventArgs> IObservableCollection.Changed {
            add {
                _convertedChangedDelegates.Add(value, new EventHandler<CollectionChangedEventArgs<T>>(value));
                Changed += _convertedChangedDelegates[value];
            }
            remove {
                Changed -= _convertedChangedDelegates[value];
                _convertedChangedDelegates.Remove(value);
            }
        }
        
        /// <summary>
        /// Raises the Changed event with the specified parameters.
        /// </summary>
        /// <param name="changeAction">The change action that took place.</param>
        /// <param name="index">The index of the changed item.</param>
        /// <param name="item">The item that was inserted or removed.</param>
        protected virtual void OnChanged (CollectionChangeAction changeAction, int index, T item) {
            if (Changed != null)
                Changed(this, new CollectionChangedEventArgs<T>(changeAction, index, item));
        }
        #endregion

        #region Item Changes
        /// <summary>
        /// Removes all elements from the collection, and publishes remove
        /// events for each of them.
        /// </summary>
        protected override void ClearItems () {
            while (Count > 0)
                RemoveItem(Count - 1);
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index, and
        /// publishes an inserted event for it.
        /// </summary>
        /// <param name="index">The zero-based index at which the item should be inserted.</param>
        /// <param name="item">The item to insert.</param>
        protected override void InsertItem (int index, T item) {
            base.InsertItem(index, item);
            OnChanged(CollectionChangeAction.Inserted, index, item);
        }

        /// <summary>
        /// Removes an element from the collection at the specified index, and
        /// publishes a removed event for it.
        /// </summary>
        /// <param name="index">The zero-based index at which the item should be removed.</param>
        protected override void RemoveItem (int index) {
            T item = this[index];
            base.RemoveItem(index);
            OnChanged(CollectionChangeAction.Removed, index, item);
        }

        /// <summary>
        /// Replaces the element at the specified index.  Publishes a remove
        /// event for the replaced item, as well as an insertion event for the
        /// new item.
        /// </summary>
        /// <param name="index">The zero-based index at which the item should be replaced.</param>
        /// <param name="item">The new value for the element at the specified index.</param>
        protected override void SetItem (int index, T item) {
            T oldItem = this[index];
            OnChanged(CollectionChangeAction.Removed, index, oldItem);

            base.SetItem(index, item);
            OnChanged(CollectionChangeAction.Inserted, index, item);
        }
        #endregion
    }

    #region CollectionChanged
    /// <summary>
    /// Used to specify what kind of action took place in the collection.
    /// </summary>
    public enum CollectionChangeAction {
        /// <summary>
        /// An item was inserted in the collection.
        /// </summary>
        Inserted,

        /// <summary>
        /// An item was removed from the collection.
        /// </summary>
        Removed
    }

    /// <summary>
    /// Contains details on collection change action events.
    /// </summary>
    public abstract class CollectionChangedEventArgs : EventArgs {
        /// <summary>
        /// The change action that took place.
        /// </summary>
        public abstract CollectionChangeAction ChangeAction { get; }

        /// <summary>
        /// The zero-based index of the change.
        /// </summary>
        public abstract int Index { get; }

        /// <summary>
        /// The element that was inserted or removed.
        /// </summary>
        public abstract object Item { get; }
    }

    /// <summary>
    /// Contains details on change action events.
    /// </summary>
    /// <typeparam name="T">The type of the element that was inserted or removed.</typeparam>
    public class CollectionChangedEventArgs<T> : CollectionChangedEventArgs {
        private CollectionChangeAction _changeAction;
        private int _index;
        private T _item;

        /// <summary>
        /// Constructs a new CollectionChangedEventArgs.
        /// </summary>
        /// <param name="changeAction">The change action that took place.</param>
        /// <param name="index">The zero-based index of the change.</param>
        /// <param name="item">The element that was inserted or removed.</param>
        public CollectionChangedEventArgs (CollectionChangeAction changeAction, int index, T item) {
            _changeAction = changeAction;
            _index = index;
            _item = item;
        }

        #region Accessors
        /// <summary>
        /// The change action that took place.
        /// </summary>
        public override CollectionChangeAction ChangeAction {
            get { return _changeAction; }
        }

        /// <summary>
        /// The zero-based index of the change.
        /// </summary>
        public override int Index {
            get { return _index; }
        }

        /// <summary>
        /// The specific element that was inserted or removed.
        /// </summary>
        public T SpecificItem {
            get { return _item; }
        }

        /// <summary>
        /// The element that was inserted or removed.
        /// </summary>
        public override object Item {
            get { return _item; }
        }
        #endregion
    }
    #endregion
}
