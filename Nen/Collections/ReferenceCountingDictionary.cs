using System;
using System.Collections.Generic;
using System.Text;

using Nen.Internal;

namespace Nen.Collections {
    /// <summary>
    /// A dictionary which counts the number of times a key is added or
    /// removed.  Keys are only finally removed when the reference count
    /// reaches zero.
    /// </summary>
    /// <typeparam name="TKey">The type of key.</typeparam>
    /// <typeparam name="TValue">The type of value.</typeparam>
    public class ReferenceCountingDictionary<TKey, TValue> : IDictionary<TKey, TValue> {
        private Dictionary<TKey, TValue> _keyValues = new Dictionary<TKey, TValue>();
        private Dictionary<TKey, int> _keyCounts = new Dictionary<TKey, int>();

        #region Reference Counting
        /// <summary>
        /// Retrieves the number of times a key has been inserted, regardless
        /// of the value associated with the key.
        /// </summary>
        /// <param name="key">The key to return a reference count for.</param>
        /// <returns>The reference count.</returns>
        public int GetReferenceCount (TKey key) {
            return _keyCounts.ContainsKey(key) ? _keyCounts[key] : 0;
        }

        /// <summary>
        /// Increments the reference count of the key.  Keys with no prior
        /// value can't have their reference count incremented.
        /// </summary>
        /// <param name="key">The key to increment.</param>
        /// <returns>The new reference count.</returns>
        public int Increment (TKey key) {
            if (!_keyCounts.ContainsKey(key))
                throw new ArgumentException(LS.T("Item must exist in the dictionary for its reference count to be incremented."), "key");

            var count = _keyCounts[key] + 1;
            _keyCounts[key] = count;
            return count;
        }

        /// <summary>
        /// Decrements the reference count of the key.  When the count reaches
        /// zero, the key and value are removed from the dictionary.
        /// </summary>
        /// <param name="key">The key to decrement.</param>
        /// <returns>The new reference count.</returns>
        public int Decrement (TKey key) {
            if (!_keyCounts.ContainsKey(key))
                return 0;

            var count = _keyCounts[key] - 1;
            if (count > 0)
                _keyCounts[key] = count;
            else {
                _keyCounts.Remove(key);
                _keyValues.Remove(key);
            }

            return count;
        }

        /// <summary>
        /// Unconditionally removes the specified key, regardless of 
        /// reference count.
        /// </summary>
        /// <param name="key">The key to remove.</param>
        /// <returns>true if the remove was successful, false if otherwise.</returns>
        public bool RemoveAll (TKey key) {
            if (!_keyCounts.ContainsKey(key))
                return false;

            _keyCounts.Remove(key);
            _keyValues.Remove(key);
            return true;
        }
        #endregion

        #region IDictionary<TKey,TValue> Members
        /// <summary>
        /// Adds the key to the dictionary with the specified value.  Replaces
        /// the value of an existing key and increments the reference count.
        /// </summary>
        /// <param name="key">The key to increment.</param>
        /// <param name="value">The value associated with the key.</param>
        public void Add (TKey key, TValue value) {
            _keyValues[key] = value;

            if (_keyCounts.ContainsKey(key))
                _keyCounts[key]++;
            else
                _keyCounts[key] = 1;
        }

        /// <summary>
        /// Indicates whether an item is contained in the dictionary.
        /// </summary>
        /// <param name="key">The key to query.</param>
        /// <returns>true if the item exists in the dictionary, false if otherwise.</returns>
        public bool ContainsKey (TKey key) {
            return _keyValues.ContainsKey(key);
        }

        /// <summary>
        /// Gets a collection containing the keys in the dictionary.
        /// </summary>
        public ICollection<TKey> Keys {
            get { return _keyValues.Keys; }
        }

        bool IDictionary<TKey, TValue>.Remove (TKey key) {
            return Decrement(key) == 0;
        }

        /// <summary>
        /// Attempts to retrieve the value of the specified key and returns an
        /// indicator for success.
        /// </summary>
        /// <param name="key">The key to try to retrieve the value from.</param>
        /// <param name="value">A place to store the key value.</param>
        /// <returns>true if the value was retrieved, false if the value did not exist.</returns>
        public bool TryGetValue (TKey key, out TValue value) {
            return _keyValues.TryGetValue(key, out value);
        }

        /// <summary>
        /// Gets a collection containing the values in the dictionary.
        /// </summary>
        public ICollection<TValue> Values {
            get { return _keyValues.Values; }
        }

        /// <summary>
        /// Gets or sets the key value.
        /// </summary>
        /// <param name="key">The key to get or set.</param>
        /// <returns>The value associated with the key.</returns>
        public TValue this[TKey key] {
            get { return _keyValues[key]; }
            set { Add(key, value); }
        }

        #endregion

        #region ICollection<KeyValuePair<TKey,TValue>> Members
        void ICollection<KeyValuePair<TKey,TValue>>.Add (KeyValuePair<TKey, TValue> item) {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Unconditionally clears the keys and values in the dictionary,
        /// effectively reducing all reference counts to zero.
        /// </summary>
        public void Clear () {
            _keyCounts.Clear();
            _keyValues.Clear();
        }

        bool ICollection<KeyValuePair<TKey,TValue>>.Contains (KeyValuePair<TKey, TValue> item) {
            return _keyValues.ContainsKey(item.Key);
        }

        void ICollection<KeyValuePair<TKey,TValue>>.CopyTo (KeyValuePair<TKey, TValue>[] array, int arrayIndex) {
            ((ICollection<KeyValuePair<TKey, TValue>>) _keyValues).CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of key/value pairs contained in the dictionary.
        /// </summary>
        public int Count {
            get { return _keyValues.Count; }
        }

        bool ICollection<KeyValuePair<TKey,TValue>>.IsReadOnly {
            get { return ((ICollection<KeyValuePair<TKey, TValue>>) _keyValues).IsReadOnly; }
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Remove (KeyValuePair<TKey, TValue> item) {
            return Decrement(item.Key) == 0;
        }
        #endregion

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members
        /// <summary>
        /// Returns an enumerator that iterates through the dictionary.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator () {
            return _keyValues.GetEnumerator();
        }
        #endregion

        #region IEnumerable Members
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator () {
            return GetEnumerator();
        }
        #endregion
    }
}
