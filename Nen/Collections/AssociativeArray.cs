﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Nen.Collections {
    /// <summary>
    /// A dictionary that isn't a dictionary.  AssociativeArray is only useful
    /// when transmitting dictionary-like data across non-WCF web services,
    /// since using real dictionaries generates an error.
    /// </summary>
    /// <typeparam name="TKey">The type of the keys in the array.</typeparam>
    /// <typeparam name="TValue">The type of the values in the array.</typeparam>
    public class AssociativeArray<TKey, TValue> : Collection<KeyValuePair<TKey, TValue>> {
        #region Constructors
        /// <summary>
        /// Constructs a new AssociativeArray.
        /// </summary>
        public AssociativeArray () {
        }

        /// <summary>
        /// Constructs a new AssociativeArray.
        /// </summary>
        /// <param name="dictionary">The values to initialize the array with.</param>
        public AssociativeArray (IDictionary<TKey, TValue> dictionary) {
            this.AddRange(dictionary);
        }
        #endregion

        #region Key Methods
        /// <summary>
        /// Removes the element with the given key from the array.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns>true if the element was removed, false if otherwise.</returns>
        public bool Remove (TKey key) {
            return this.RemoveFirst(p => Equals(p.Key, key));
        }
        #endregion

        #region Indexer
        /// <summary>
        /// Gets or sets the value with the given key.
        /// </summary>
        /// <param name="key">The key of the element to get or set.</param>
        /// <returns>The value with the given key.</returns>
        public TValue this[TKey key] {
            get { return this.First(p => Equals(p.Key, key)).Value; }
            set {
                Remove(key);
                Add(new KeyValuePair<TKey, TValue>(key, value));
            }
        }
        #endregion

        #region Conversions
        /// <summary>
        /// Converts the array to a dictionary.
        /// </summary>
        /// <returns>A dictionary containing the array values.</returns>
        public Dictionary<TKey, TValue> ToDictionary () {
            var dictionary = new Dictionary<TKey, TValue>(Count);
            dictionary.AddRange(this);
            return dictionary;
        }
        #endregion
    }
}
