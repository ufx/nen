﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Collections {
    public interface IDataStream {
        bool IsFinished { get; set; }
    }

    public interface Input: IDataStream {
        bool IsInputReady { get; }
        event EventHandler InputReady;
    }

    public interface Output: IDataStream {
        bool IsOutputReady { get; }
        event EventHandler OutputReady;
    }

    public interface Input<T> : Input {
        T ReadInput ();
        T PeekInput ();
    }

    public interface Output<T> : Output {
        void WriteOutput (T value);
    }

    public class RingBuffer<T> : Input<T>, Output<T> {
        private int _readPosition;
        private int _writePosition;
        private T[] _buffer;

        public int Capacity { get; private set; }

        public RingBuffer ()
            : this(32) {
        }

        public RingBuffer (int capacity) {
            _buffer = new T[capacity + 1];
            Capacity = capacity;
        }

        public bool IsFinished { get; set; }

        #region Input<T> Members
        public bool IsInputReady {
            get { return _readPosition != _writePosition; }
        }

        public T ReadInput () {
            bool wasOutputReady = IsOutputReady;

            if (!IsInputReady)
                throw new InvalidOperationException();

            T result = _buffer[_readPosition++];

            if (_readPosition == _buffer.Length)
                _readPosition = 0;

            if (!wasOutputReady && OutputReady != null)
                OutputReady(this, EventArgs.Empty);

            return result;
        }

        public T PeekInput () {
            if (IsInputReady)
                return _buffer[_readPosition];
            else
                throw new InvalidOperationException();
        }

        public event EventHandler InputReady;
        #endregion

        #region Output<T> Members
        public bool IsOutputReady {
            get { return (_writePosition + 1) % _buffer.Length != _readPosition; }
        }

        public void WriteOutput (T value) {
            bool wasInputReady = IsInputReady;

            if (!IsOutputReady)
                throw new InvalidOperationException();

            _buffer[_writePosition++] = value;

            if (_writePosition == _buffer.Length)
                _writePosition = 0;

            if (!wasInputReady && InputReady != null)
                InputReady(this, EventArgs.Empty);
        }

        public event EventHandler OutputReady;
        #endregion
    }
}
