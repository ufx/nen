﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Nen.Collections {
    /// <summary>
    /// A KeyedCollection that publishes events when items are inserted or removed.
    /// </summary>
    /// <typeparam name="TKey">The key type.</typeparam>
    /// <typeparam name="TValue">The type of collection items.</typeparam>
    [Serializable]
    public class ObservableKeyedCollection<TKey, TValue> : KeyedCollection<TKey, TValue>, IObservableCollection<TValue> {
        private Dictionary<EventHandler<CollectionChangedEventArgs>, EventHandler<CollectionChangedEventArgs<TValue>>> _convertedChangedDelegates
                = new Dictionary<EventHandler<CollectionChangedEventArgs>, EventHandler<CollectionChangedEventArgs<TValue>>>();

        /// <summary>
        /// Constructs a new ObservableKeyedCollection.
        /// </summary>
        /// <param name="keyFunction">A function to extract the key value from a collection item.</param>
        public ObservableKeyedCollection (Func<TValue, TKey> keyFunction) {
            KeyFunction = keyFunction;
        }

        /// <summary>
        /// Extracts the key value from the specified item.
        /// </summary>
        /// <param name="item">The item to extract the key value from.</param>
        /// <returns>The key value of the item.</returns>
        protected override TKey GetKeyForItem (TValue item) {
            return KeyFunction(item);
        }

        #region Events
        /// <summary>
        /// Raised whenever items are inserted or removed from the collection.
        /// </summary>
        public event EventHandler<CollectionChangedEventArgs<TValue>> Changed;

        event EventHandler<CollectionChangedEventArgs> IObservableCollection.Changed {
            add {
                _convertedChangedDelegates.Add(value, new EventHandler<CollectionChangedEventArgs<TValue>>(value));
                Changed += _convertedChangedDelegates[value];
            }
            remove {
                Changed -= _convertedChangedDelegates[value];
                _convertedChangedDelegates.Remove(value);
            }
        }

        /// <summary>
        /// Raises the Changed event with the specified parameters.
        /// </summary>
        /// <param name="changeAction">The change action that took place.</param>
        /// <param name="index">The index of the changed item.</param>
        /// <param name="item">The item that was inserted or removed.</param>
        protected virtual void OnChanged (CollectionChangeAction changeAction, int index, TValue item) {
            if (Changed != null)
                Changed(this, new CollectionChangedEventArgs<TValue>(changeAction, index, item));
        }
        #endregion

        #region Item Changes
        /// <summary>
        /// Removes all elements from the collection, and publishes remove
        /// events for each of them.
        /// </summary>
        protected override void ClearItems () {
            while (Count > 0)
                RemoveItem(Count - 1);
        }

        /// <summary>
        /// Inserts an element into the collection at the specified index, and
        /// publishes an inserted event for it.
        /// </summary>
        /// <param name="index">The zero-based index at which the item should be inserted.</param>
        /// <param name="item">The item to insert.</param>
        protected override void InsertItem (int index, TValue item) {
            base.InsertItem(index, item);
            OnChanged(CollectionChangeAction.Inserted, index, item);
        }

        /// <summary>
        /// Removes an element from the collection at the specified index, and
        /// publishes a removed event for it.
        /// </summary>
        /// <param name="index">The zero-based index at which the item should be removed.</param>
        protected override void RemoveItem (int index) {
            TValue item = this[index];
            base.RemoveItem(index);
            OnChanged(CollectionChangeAction.Removed, index, item);
        }

        /// <summary>
        /// Replaces the element at the specified index.  Publishes a remove
        /// event for the replaced item, as well as an insertion event for the
        /// new item.
        /// </summary>
        /// <param name="index">The zero-based index at which the item should be replaced.</param>
        /// <param name="item">The new value for the element at the specified index.</param>
        protected override void SetItem (int index, TValue item) {
            TValue oldItem = this[index];
            OnChanged(CollectionChangeAction.Removed, index, oldItem);

            base.SetItem(index, item);
            OnChanged(CollectionChangeAction.Inserted, index, item);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets the function to extract key values from collection items.
        /// </summary>
        public Func<TValue, TKey> KeyFunction { get; private set; }
        #endregion
    }
}
