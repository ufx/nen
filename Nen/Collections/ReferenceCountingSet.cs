using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Nen.Collections {
    /// <summary>
    /// A collection of unique objects which counts the number of times an
    /// object is inserted or removed from the collection.  Objects are only
    /// finally removed when the reference count reaches zero.
    /// </summary>
    /// <typeparam name="T">The type of unique object the set contains.</typeparam>
    public class ReferenceCountingSet<T> : ICollection<T> {
        private Dictionary<T, int> _items = new Dictionary<T, int>();

        #region Reference Counting
        /// <summary>
        /// Retrieves the number of times an item has been inserted.
        /// </summary>
        /// <param name="item">The item to return a reference count for.</param>
        /// <returns>The reference count.</returns>
        public int GetReferenceCount (T item) {
            return _items.ContainsKey(item) ? _items[item] : 0;
        }

        /// <summary>
        /// Increments the refernce count of the item.  Inserts items with no
        /// previous reference count.
        /// </summary>
        /// <param name="item">The item to increment.</param>
        /// <returns>The new reference count.</returns>
        public int Increment (T item) {
            if (_items.ContainsKey(item)) {
                int count = _items[item] + 1;
                _items[item] = count;
                return count;
            } else {
                _items[item] = 1;
                return 1;
            }
        }

        /// <summary>
        /// Decrements the reference count of the item.  When the count reaches
        /// zero, the item is removed from the set.
        /// </summary>
        /// <param name="item">The item to decrement.</param>
        /// <returns>The new reference count.</returns>
        public int Decrement (T item) {
            if (!_items.ContainsKey(item))
                return 0;

            int count = _items[item] - 1;
            if (count > 0)
                _items[item] = count;
            else
                _items.Remove(item);

            return count;

        }
        #endregion

        #region ICollection<T> Members
        void ICollection<T>.Add (T item) {
            Increment(item);
        }

        /// <summary>
        /// Unconditionally clears the items in the set, effectively reducing
        /// all reference counts to zero.
        /// </summary>
        public void Clear () {
            _items.Clear();
        }

        /// <summary>
        /// Indicates whether an item is contained in the set.
        /// </summary>
        /// <param name="item">The item to query.</param>
        /// <returns>true if the item exists in the set, false if otherwise.</returns>
        public bool Contains (T item) {
            return _items.ContainsKey(item);
        }

        /// <summary>
        /// Copies the elements to an existing one-dimensional array, starting at the specified index.
        /// </summary>
        /// <param name="array">The array to copy to.</param>
        /// <param name="arrayIndex">The index to start copying from.</param>
        public void CopyTo (T[] array, int arrayIndex) {
            _items.Keys.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// The number of unique items in the set.
        /// </summary>
        public int Count {
            get { return _items.Count; }
        }

        bool ICollection<T>.IsReadOnly {
            get { return ((ICollection<KeyValuePair<T, int>>) _items).IsReadOnly; }
        }

        bool ICollection<T>.Remove (T item) {
            return Decrement(item) == 0;
        }
        #endregion

        #region IEnumerable<T> Members
        /// <summary>
        /// Returns an enumerator that iterates through the set.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public IEnumerator<T> GetEnumerator () {
            return _items.Keys.GetEnumerator();
        }
        #endregion

        #region IEnumerable Members
        IEnumerator IEnumerable.GetEnumerator () {
            return GetEnumerator();
        }
        #endregion
    }
}
