using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Nen.Reflection;

namespace Nen.Collections {
    /// <summary>
    /// Extensions to the System.Collections ICollection classes.
    /// </summary>
    public static class CollectionEx {
        #region Factorization
        /// <summary>
        /// Factors a collection of old and new items into a collection
        /// inserted and removed items.
        /// </summary>
        /// <typeparam name="T">The type of collection.</typeparam>
        /// <param name="oldItems">The collection of old items.</param>
        /// <param name="newItems">The collection of new items.</param>
        /// <param name="insertedItems">The items contained in newItems but not in oldItems.</param>
        /// <param name="removedItems">The items contaiend in oldItems but not in newItems.</param>
        public static void FactorChanges<T> (ICollection<T> oldItems, ICollection<T> newItems, out IEnumerable<T> insertedItems, out IEnumerable<T> removedItems) {
            insertedItems = newItems.Where(item => !oldItems.Contains(item));
            removedItems = oldItems.Where(item => !newItems.Contains(item));
        }
        #endregion

        #region Insertion
        /// <summary>
        /// Adds the given items to the collection.
        /// </summary>
        /// <typeparam name="T">The item type.</typeparam>
        /// <param name="collection">The collection to add the items to.</param>
        /// <param name="items">The items to add.</param>
        public static void AddRange<T> (this ICollection<T> collection, IEnumerable<T> items) {
            if (items == null)
                return;

            if (collection == null)
                throw new ArgumentNullException("collection");

            foreach (T item in items)
                collection.Add(item);
        }
        #endregion

        #region Item Retrieval
        /// <summary>
        /// Retrieves the internal "Items" list of a
        /// System.Collections.ObjectModel.Collection&lt;&gt; object.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>The internal Items list.</returns>
        public static object GetInternalItems (this ICollection collection) {
            if (collection == null)
                throw new ArgumentNullException("collection");

            return VariableInfo.GetValue(collection, "Items");
        }

        /// <summary>
        /// Retrieves the type of item contained by the collection.
        /// </summary>
        /// <example>new Collection&lt;int&gt;().GetItemType() -> typeof(int)</example>
        /// <param name="collection">The collection to obtain the item type from.</param>
        /// <returns>The type of item contained by the collection.</returns>
        public static Type GetItemType (this ICollection collection) {
            if (collection == null)
                throw new ArgumentNullException("collection");

            return GetItemType(collection.GetType());
        }

        /// <summary>
        /// Retrieves the type of item contained by the collection type.
        /// </summary>
        /// <param name="collectionType">The collection type to obtain the item type from.</param>
        /// <returns>The type of item contained by the collection type.</returns>
        public static Type GetItemType (Type collectionType) {
            if (collectionType == null)
                throw new ArgumentNullException("collectionType");

            var interfaceType = collectionType.GetGenericTypeInstance(typeof(ICollection<>));
            return interfaceType.GetGenericArguments()[0];
        }
        #endregion

        #region Removal
        /// <summary>
        /// Removes the given items from a collection.
        /// </summary>
        /// <typeparam name="T">The item type.</typeparam>
        /// <param name="collection">The collection to remove the items from.</param>
        /// <param name="items">The items to remove.</param>
        public static void RemoveRange<T> (this ICollection<T> collection, IEnumerable<T> items) {
            foreach (T item in items)
                collection.Remove(item);
        }

        /// <summary>
        /// Removes the first item in a collection that matches the specified
        /// predicate.
        /// </summary>
        /// <typeparam name="T">The item type.</typeparam>
        /// <param name="collection">The collection to remove an item from.</param>
        /// <param name="predicate">The predicate that, when matched, causes an item to be removed.</param>
        /// <returns>true if an item matched the predicate and was removed, false if otherwise.</returns>
        public static bool RemoveFirst<T> (this ICollection<T> collection, Func<T, bool> predicate) {
            foreach (T item in collection) {
                if (predicate(item)) {
                    collection.Remove(item);
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region Type Queries
        /// <summary>
        /// Determines if the type or its base types is of type
        /// System.Collections.ObjectModel.Collection&lt;&gt;.
        /// </summary>
        /// <param name="type">The type to check.</param>
        /// <returns>true if the object is of type System.Collections.ObjectModel.Collection&lt;&gt;, false if otherwise.</returns>
        public static bool IsCollection (Type type) {
            return IsCollection(type, true);
        }

        /// <summary>
        /// Determines if the type or its base types is of type
        /// System.Collections.ObjectModel.Collection&lt;&gt;.
        /// </summary>
        /// <param name="type">The type to check.</param>
        /// <param name="inherit">If true, base types are also checked.</param>
        /// <returns>true if the type is of type System.Collections.ObjectModel.Collection&lt;&gt;, false if otherwise.</returns>
        public static bool IsCollection (Type type, bool inherit) {
            return type.IsInstanceOfGenericType(typeof(Collection<>), inherit);
        }
        #endregion
    }
}
