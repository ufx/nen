﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Nen.Collections
{
	public class PriorityQueue<T> : ICollection, IEnumerable, IEnumerable<T> where T: IComparable<T>
	{
		private List<T> _data;
		private Order _order;

		public enum Order { Ascending, Descending };

		#region Constructor
		public PriorityQueue(Order order)
		{
			_data = new List<T>();
			_order = order;
		}

		public PriorityQueue(Order order, int capacity)
		{
			_data = new List<T>(capacity);
			_order = order;
		}
		#endregion

		#region ICollection<T>
		public int Count
		{
			get { return _data.Count; }
		}

		public bool IsSynchronized
		{
			get { return false; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public object SyncRoot
		{
			get { return this; }
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _data.GetEnumerator();
		}

		 IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return _data.GetEnumerator();
		}

		public void CopyTo(T[] array, int index)
		{
			_data.CopyTo(array, index);
		}

		public void CopyTo(Array array, int index)
		{
			((IList)_data).CopyTo(array, index);
		}
		#endregion

		#region Heap Operations
		public void Push(T obj)
		{
			int cur = Count;
			_data.Add(obj);

			for (; cur > 0; cur = HeapParent(cur))
			{
				if (Compare(_data[HeapParent(cur)], obj))
				{
					var tmp = _data[cur];
					_data[cur] = _data[HeapParent(cur)];
					_data[HeapParent(cur)] = tmp;
				}
				else
					break;
			}
		}

		public T Pop(int n)
		{
			var top = _data[0];
			var last = _data[_data.Count - 1];
			_data.RemoveAt(_data.Count - 1);
			int c;

			if (_data.Count == 0)
				return top;

			for (c = HeapRight(n); c < _data.Count; n = c, c = HeapRight(c))
			{
				if (Compare(_data[c], _data[c - 1]))
					c--;
				if (Compare(last, _data[c]))
					_data[n] = _data[c];
				else
					break;
			}

			if (c == _data.Count && Compare(last, _data[--c]))
			{
				_data[n] = _data[c];
				n = c;
			}

			_data[n] = last;
			return top;
		}

		public T Pop()
		{
			return Pop(0);
		}
		#endregion

		#region Private Methods
		private int HeapParent(int n)
		{
			return (n - 1) / 2;
		}

		private int HeapRight(int n)
		{
			return 2 + n * 2;
		}

		private bool Compare(T x, T y)
		{
			if (_order == Order.Descending)
				return x.CompareTo(y) < 0;
			else
				return x.CompareTo(y) > 0;
		}
		#endregion

		#region Properties
		public T Top
		{
			get { return Count > 0 ? _data[0] : default(T); }
		}
		#endregion
	}
}
