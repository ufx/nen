﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Collections {
    /// <summary>
    /// Extensions to the System.Collections.Generic.Stack class.
    /// </summary>
    public static class StackEx {
        /// <summary>
        /// Pushes all items within an enumerable onto a stack.
        /// </summary>
        /// <typeparam name="T">The type of element contained within the stack.</typeparam>
        /// <param name="stack">The stack to push values onto.</param>
        /// <param name="items">The items to push onto the stack.</param>
        public static void PushAll<T> (this Stack<T> stack, IEnumerable<T> items) {
            if (stack == null)
                throw new ArgumentNullException("stack");

            foreach (T item in items)
                stack.Push(item);
        }

        /// <summary>
        /// Constructs a new StackScope wrapping the given item.
        /// </summary>
        /// <typeparam name="T">The type of element contained within the stack.</typeparam>
        /// <param name="stack">The stack to construct a PushScope for.</param>
        /// <param name="item">The item to push.</param>
        /// <returns>A new StackScope wrapping the given item.</returns>
        public static StackScope<T> PushScope<T> (this Stack<T> stack, T item) {
            if (stack == null)
                throw new ArgumentNullException("stack");

            return new StackScope<T>(stack, item);
        }
    }
}
