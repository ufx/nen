using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if NET4
using System.Dynamic;
#endif
using System.Collections;

namespace Nen.Collections {
	/// <summary>
	/// Extensions to the System.Collections IDictionary classes.
	/// </summary>
	public static class DictionaryEx {
		/// <summary>
		/// Activates the specified key on the dictionary.  If the key was not
		/// previously inserted into the dictionary, a new instance of the
		/// value will be added and returned.
		/// </summary>
		/// <param name="dictionary">The dictionary.</param>
		/// <param name="key">The key to activate</param>
		/// <returns>The newly activated value or the existing key.</returns>
		public static TValue Activate<TKey, TValue> (this IDictionary<TKey, TValue> dictionary, TKey key) where TValue : new() {
            TValue value;
            if (dictionary.TryGetValue(key, out value))
                return value;

			value = new TValue();
			dictionary[key] = value;
			return value;
		}

        /// <summary>
        /// Gets a key or returns default(TValue) if it is not present in the dictionary.
        /// </summary>
        /// <typeparam name="TKey">The dictionary key type.</typeparam>
        /// <typeparam name="TValue">The dictionary value type.</typeparam>
        /// <param name="dictionary">A dictionary.</param>
        /// <param name="key">A key.</param>
        /// <returns>The associated dictionary value for the given key, or default(TValue).</returns>
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue result;

            if (dictionary.TryGetValue(key, out result))
                return result;

            return default(TValue);
        }

		/// <summary>
		/// Transforms the keys of a dictionary. A new dictionary with the corresponding
		/// new keys will be returned.
		/// </summary>
		/// <typeparam name="TKey">The original key type.</typeparam>
		/// <typeparam name="TValue">The value type.</typeparam>
		/// <typeparam name="TKey2">The new key type.</typeparam>
		/// <param name="dict">A dictionary.</param>
		/// <param name="fn">A function mapping the original key type to the new key type.</param>
		/// <returns>A new dictionary.</returns>
		public static Dictionary<TKey2, TValue> MapKeys<TKey, TValue, TKey2> (this IDictionary<TKey, TValue> dict, Func<TKey, TKey2> fn) {
			var result = new Dictionary<TKey2, TValue>();

			foreach (var kv in dict)
				result.Add(fn(kv.Key), kv.Value);

			return result;
		}

		/// <summary>
		/// Sets a value with the given path on the dictionary.  Useful for
		/// nested dictionaries.
		/// </summary>
		/// <typeparam name="TKey">The dictionary key type.</typeparam>
		/// <param name="dictionary">The dictionary.</param>
		/// <param name="path">The key path that leads to the value.</param>
		/// <param name="value">The value to set at the end of the path.</param>
		public static void SetPath<TKey> (this IDictionary<TKey, object> dictionary, IEnumerable<TKey> path, object value) {
			var current = dictionary;
			foreach (var key in path.SkipLast()) {
				if (current.ContainsKey(key))
					current = (IDictionary<TKey, object>)current[key];
				else {
					current = new Dictionary<TKey, object>();
					dictionary[key] = current;
				}
			}

			var lastKey = path.Last();
			current[lastKey] = value;
		}

		/// <summary>
		/// Gets a value with the given path on the dictionary.  Useful for
		/// nested dictionaries.
		/// </summary>
		/// <typeparam name="TKey">The dictionary key type.</typeparam>
		/// <param name="dictionary">The dictionary.</param>
		/// <param name="path">The key path that leads to the value.</param>
		/// <returns>The value at the end of the path.</returns>
		public static object GetPath<TKey> (this IDictionary<TKey, object> dictionary, IEnumerable<TKey> path) {
			var current = dictionary;
			foreach (var key in path.SkipLast())
				current = (IDictionary<TKey, object>)current[key];

			var lastKey = path.Last();
			return current[lastKey];
		}

		public static Dictionary<T, T> FromPairs<T> (params T[] pairs) where T : class {
			if (pairs == null)
				return null;

			var result = new Dictionary<T, T>();
			for (int i = 0; i < pairs.Length; i += 2) {
				T value = null;
				if (i + 1 < pairs.Length)
					value = pairs[i + 1];

				result[pairs[i]] = value;
			}
			return result;
		}

		public static Dictionary<TKey, TValue> Merge<TKey, TValue> (this Dictionary<TKey, TValue> dictionary, Dictionary<TKey, TValue> value) {
			if (dictionary == null && value == null)
				return null;

			Dictionary<TKey, TValue> merged = new Dictionary<TKey, TValue>();

			if (dictionary != null)
				merged.AddRange(dictionary);

			if (value != null) {
				foreach (var entry in value)
					merged[entry.Key] = entry.Value;
			}

			return merged;
		}

#if NET4
		public static ExpandoObject Expando (this IDictionary<string, object> dictionary) {
			var expando = new ExpandoObject();
			var expandoDic = (IDictionary<string, object>)expando;

			foreach (var item in dictionary) {
				bool alreadyProcessed = false;

				if (item.Value is IDictionary<string, object>) {
					expandoDic.Add(item.Key, Expando((IDictionary<string, object>)item.Value));
					alreadyProcessed = true;
				} else if (item.Value is ICollection) {
					var itemList = new List<object>();
					foreach (var item2 in (ICollection)item.Value)
						if (item2 is IDictionary<string, object>)
							itemList.Add(Expando((IDictionary<string, object>)item2));
						else
							itemList.Add(Expando(new Dictionary<string, object> { { "Unknown", item2 } }));

					if (itemList.Count > 0) {
						expandoDic.Add(item.Key, itemList);
						alreadyProcessed = true;
					}
				}

				if (!alreadyProcessed)
					expandoDic.Add(item);
			}

			return expando;
		}
#endif
	}
}
