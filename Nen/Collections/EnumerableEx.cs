using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Collections {
    /// <summary>
    /// Extensions to the System.Collections.IEnumerable classes.
    /// </summary>
    public static class EnumerableEx {
        #region Conversions
        /// <summary>
        /// Converts all of the items in the enumerable ToString, or "null".
        /// </summary>
        /// <typeparam name="TSource">The source enumerable type.</typeparam>
        /// <param name="source">The source to convert.</param>
        /// <returns>Converted strings of each item.</returns>
        public static IEnumerable<string> ToStrings<TSource> (this IEnumerable<TSource> source) {
            return source.Select(i => i == null ? "null" : i.ToString());
        }
        #endregion

        #region Iteration
        /// <summary>
        /// Executes an action on each element in an enumerable.
        /// </summary>
        /// <typeparam name="T">The source enumerable type.</typeparam>
        /// <param name="source">The source to execute actions upon.</param>
        /// <param name="action">The action to execute.</param>
        public static IEnumerable<T> Each<T>(this IEnumerable<T> source, Action<T> action) {
            foreach (T item in source)
                action(item);

            return source;
        }

        public static IEnumerable<IEnumerable<T>> Split<T> (this IEnumerable<T> source, Func<T, bool> delimiter) {
            var items = new List<T>();
            foreach (var item in source) {
                if (delimiter(item)) {
                    yield return items;
                    items = new List<T>();
                    continue;
                }

                items.Add(item);
            }

            yield return items;
        }
        #endregion

        #region Ordering
        /// <summary>
        /// Orders an enumerable containing elements that contain other
        /// elements with children appearing before the elements that contain
        /// them.
        /// </summary>
        /// <typeparam name="T">The source enumerable type.</typeparam>
        /// <param name="source">The source to order.</param>
        /// <param name="isChild">A function which determines whether or not the second argument is a child of the first.</param>
        /// <returns>A new ordered enumerable.</returns>
        public static IEnumerable<T> OrderByChildrenFirst<T> (this IEnumerable<T> source, Func<T, T, bool> isChild) {
            return source.ToArray().OrderByChildrenFirst(isChild);
        }

        public static List<T> TopologicalSort<T> (this IEnumerable<T> source, Func<T, IEnumerable<T>> getEdges) {
            var items = source.ToArray();
            var visited = new HashSet<T>();

            var result = new List<T>(items.Length);
            foreach (var item in items)
                TopologicalSortCore(item, visited, result, getEdges);
            return result;
        }

        private static void TopologicalSortCore<T> (T item, HashSet<T> visited, List<T> result, Func<T, IEnumerable<T>> getEdges) {
            if (visited.Contains(item))
                return;

            visited.Add(item);

            var edges = getEdges(item);
            if (edges != null) {
                foreach (var edge in edges)
                    TopologicalSortCore(edge, visited, result, getEdges);
            }

            result.Add(item);
        }

        #endregion

        #region Restrictions
        /// <summary>
        /// Skips the last element in an enumerable.
        /// </summary>
        /// <typeparam name="T">The source enumerable type.</typeparam>
        /// <param name="source">The source to skip the last element in.</param>
        /// <returns>An enumerable that skips the last element.</returns>
        public static IEnumerable<T> SkipLast<T> (this IEnumerable<T> source) {
            return source.Take(source.Count() - 1);
        }

        /// <summary>
        /// Filters null values from a sequence.
        /// </summary>
        /// <typeparam name="T">The source enumerable type.</typeparam>
        /// <param name="source">The source to filter.</param>
        /// <returns>A filtered sequence.</returns>
        public static IEnumerable<T> WhereNotNull<T> (this IEnumerable<T> source) where T : class {
            return source.Where(i => i != null);
        }
        #endregion

        #region Testing
        /// <summary>
        /// Determines whether the 'whole' sequence starts with the 'part'
        /// sequence.
        /// </summary>
        /// <typeparam name="T">The type of the elements of the input sequences.</typeparam>
        /// <param name="whole">A sequence to compare to the part.</param>
        /// <param name="part">A sequence to compare to the whole.</param>
        /// <returns>true if the whole starts with the part, otherwise false.</returns>
        public static bool SequenceStartsWith<T> (this IEnumerable<T> whole, IEnumerable<T> part) {
            if (whole == null)
                throw new ArgumentNullException("whole");

            if (part == null)
                throw new ArgumentNullException("part");


            using (var wholeEnumerator = whole.GetEnumerator()) {
                using (var partEnumerator = part.GetEnumerator()) {
                    while (partEnumerator.MoveNext()) {
                        if (!wholeEnumerator.MoveNext() || !Equals(partEnumerator.Current, wholeEnumerator.Current))
                            return false;
                    }
                }
            }

            return true;
        }
        #endregion

        #region Unions
        /// <summary>
        /// Appends a single item onto an enumerable.
        /// </summary>
        /// <typeparam name="T">The source enumerable and item type.</typeparam>
        /// <param name="source">The source enumerable to union.</param>
        /// <param name="item">The item to append.</param>
        /// <returns>An enumerable with the item appended.</returns>
        public static IEnumerable<T> Union<T> (this IEnumerable<T> source, T item) {
            return source.Union(new T[] { item });
        }
        #endregion

        #region Strings
        public static string StringJoin<T> (this IEnumerable<T> source, string separator) {
            return string.Join(separator, source.Select(o => o.ToString()).ToArray());
        }
        #endregion
    }
}
