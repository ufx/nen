using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Nen.Collections {
    /// <summary>
    /// Extensions to the System.Collections IList classes.
    /// </summary>
    public static class ListEx {
        /// <summary>
        /// Adds the given items to the collection.
        /// </summary>
        /// <typeparam name="T">The item type.</typeparam>
        /// <param name="list">The list to add the items to.</param>
        /// <param name="items">The items to add.</param>
        public static void AddRange<T> (IList list, IEnumerable<T> items) {
            // This method is not an extension due to ambiguity between it and the CollectionEx variant.

            foreach (T item in items)
                list.Add(item);
        }

        /// <summary>
        /// Yields the elements of the list in the given indexes.
        /// </summary>
        /// <typeparam name="T">The type of element within the list.</typeparam>
        /// <param name="list">The list that contains the elements.</param>
        /// <param name="indexes">The indexes to yield.</param>
        /// <returns>The elements at the given indexes.</returns>
        public static IEnumerable<T> At<T> (this IList<T> list, IEnumerable<int> indexes) {
            return indexes.Select(i => list[i]);
        }

        /// <summary>
        /// Creates an instance of a generic list with the given type.
        /// </summary>
        /// <param name="innerType">The type the list contains.</param>
        /// <returns>A new instance of the generic list with the given type.</returns>
        public static object CreateList (Type innerType) {
            return ActivatorEx.CreateGenericInstance(typeof(List<>), innerType);
        }

        /// <summary>
        /// Determines if the given type implements IList&lt;&gt;
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsList (Type type) {
            foreach (Type interfaceType in type.GetInterfaces())
                if (interfaceType.GetGenericTypeDefinition() == typeof(IList<>))
                    return true;

            return false;
        }

        /// <summary>
        /// Orders a list containing elements that contain other
        /// elements with children appearing before the elements that contain
        /// them.
        /// </summary>
        /// <typeparam name="T">The source list type.</typeparam>
        /// <param name="source">The source to order.</param>
        /// <param name="isChild">A function which determines whether or not the second argument is a child of the first.</param>
        /// <returns>A new ordered enumerable.</returns>
        public static IEnumerable<T> OrderByChildrenFirst<T> (this IList<T> source, Func<T, T, bool> isChild) {
            var orderedSource = new List<T>();
            var hashSet = new HashSet<T>();

            for (var i = 0; i < source.Count; ++i)
                OrderByChildrenOfItem(source[i], source, i, hashSet, orderedSource, isChild);

            return orderedSource;
        }

        private static void OrderByChildrenOfItem<T> (T item, IList<T> source, int startIndex, HashSet<T> hashSet, List<T> orderedSource, Func<T, T, bool> isChild) {
            if (hashSet.Contains(item))
                return;
            else
                hashSet.Add(item);

            for (var i = startIndex; i < source.Count; ++i) {
                T nextItem = source[i];

                if (hashSet.Contains(nextItem) || Equals(item, nextItem))
                    continue;

                if (isChild(item, nextItem))
                    OrderByChildrenOfItem(nextItem, source, i + 1, hashSet, orderedSource, isChild);
            }

            orderedSource.Add(item);
        }
    }
}
