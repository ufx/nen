﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Collections {
    /// <summary>
    /// Fixes a position on a stack to a scope.  When the scope is
    /// entered / constructed the item is pushed onto the stack, and when the
    /// scope is exited / disposed the item is popped.
    /// </summary>
    /// <typeparam name="T">The type of item.</typeparam>
    public class StackScope<T> : IDisposable {
        private Stack<T> _stack;

        #region Constructor
        /// <summary>
        /// Constructs a new StackScope and pushes the item onto the stack.
        /// </summary>
        /// <param name="stack">The stack to push the item onto.</param>
        /// <param name="item">The item to push onto the stack.</param>
        public StackScope (Stack<T> stack, T item) {
            if (stack == null)
                throw new ArgumentNullException("stack");

            _stack = stack;
            _stack.Push(item);
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Pops the item from the stack.
        /// </summary>
        public void Dispose () {
            if (_stack == null)
                throw new ObjectDisposedException("StackScope<T>");

            _stack.Pop();
            _stack = null;
        }
        #endregion
    }
}
