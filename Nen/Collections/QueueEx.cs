﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Collections
{
    public static class QueueEx
    {
        public static void EnqueueRange<T>(this Queue<T> queue, IEnumerable<T> items)
        {
            if (items == null)
                return;

            if (queue == null)
                throw new ArgumentNullException("queue");

            foreach (var item in items)
                queue.Enqueue(item);
        }
    }
}
