﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Caching
{
    public interface ICache
    {
        object this[string key] { get; set; }
        object Get(string key);
        void Insert(string key, object value);
        void Insert(string key, object value, CacheOptions options);
        bool Remove(string key);
        void Clear();
    }

    public class CacheOptions
    {
        public DateTime AbsoluteExpiration { get; set; }
        public TimeSpan SlidingExpiration { get; set; }
        public TimeSpan RelativeExpiration { get; set; }

        public CacheOptions() { }

        public static CacheOptions Absolute(DateTime absoluteExpiration)
        {
            return new CacheOptions() { AbsoluteExpiration = absoluteExpiration };
        }

        public static CacheOptions Relative(TimeSpan relativeExpiration)
        {
            return new CacheOptions() { RelativeExpiration = relativeExpiration };
        }

        public static CacheOptions Sliding(TimeSpan slidingExpiration)
        {
            return new CacheOptions() { SlidingExpiration = slidingExpiration };
        }
    }
}
