﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;

namespace Nen.Caching
{
    public class WebCache : ICache
    {
        private Cache _cache;

        public WebCache(Cache cache)
        {
            _cache = cache;
        }

        public object this[string key]
        {
            get { return _cache[key]; }
            set { _cache[key] = value; }
        }

        public object Get(string key)
        {
            return _cache.Get(key);
        }

        public void Insert(string key, object value)
        {
            _cache.Insert(key, value);
        }

        public void Insert(string key, object value, CacheOptions options)
        {
            DateTime absoluteExpiration = Cache.NoAbsoluteExpiration;
            if (options.RelativeExpiration != default(TimeSpan))
                absoluteExpiration = DateTime.UtcNow.Add(options.RelativeExpiration);
            else if (options.AbsoluteExpiration != default(DateTime) && options.AbsoluteExpiration != DateTime.MaxValue)
                absoluteExpiration = options.AbsoluteExpiration;

            var slidingExpiration = (options.SlidingExpiration == default(TimeSpan) ? Cache.NoSlidingExpiration : options.SlidingExpiration);
            _cache.Insert(key, value, null, absoluteExpiration, slidingExpiration);
        }

        public bool Remove(string key)
        {
            return _cache.Remove(key) == null;
        }

        public void Clear()
        {
            var entries = _cache.Cast<System.Collections.DictionaryEntry>().ToArray();
            foreach (var entry in entries)
                _cache.Remove((string)entry.Key);
        }
    }
}
