﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using Nen.Linq.Expressions;
using Nen.Data.ObjectRelationalMapper;

namespace Nen.Linq.Dynamic
{
    public static class NenDynamicQueryable
    {
        #region First
        public static T First<T>(this IQueryable<T> source, string predicate, params object[] values)
        {
            return (T)First((IQueryable)source, predicate, values);
        }

        public static object First(this IQueryable source, string predicate, params object[] values)
        {
            return Execute(DynamicCall(source, "First", predicate, values));
        }

        public static object First(this IQueryable source)
        {
            return Execute(ExpressionEx.Call(source, "First"));
        }

        public static T FirstOrDefault<T>(this IQueryable<T> source, string predicate, params object[] values)
        {
            return (T)FirstOrDefault((IQueryable)source, predicate, values);
        }

        public static object FirstOrDefault(this IQueryable source, string predicate, params object[] values)
        {
            return Execute(DynamicCall(source, "FirstOrDefault", predicate, values));
        }

        public static object FirstOrDefault(this IQueryable source)
        {
            return Execute(ExpressionEx.Call(source, "FirstOrDefault"));
        }
        #endregion

        public static IQueryable Include(this IQueryable source, string keySelector, params object[] values)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (keySelector == null) throw new ArgumentNullException("keySelector");

            var keyLambda = DynamicExpression.ParseLambda(source.ElementType, null, keySelector, values);

            return source.Provider.CreateQuery(
                Expression.Call(
                    typeof(QueryableEx), "Include",
                    new Type[] { source.ElementType, keyLambda.Body.Type },
                    source.Expression, Expression.Quote(keyLambda)));
        }

        private static object Execute(IQueryable source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return source.Provider.Execute(source.Expression);
        }

        private static T Execute<T>(IQueryable<T> source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return source.Provider.Execute<T>(source.Expression);
        }

        public static IQueryable DynamicCall(this IQueryable source, string methodName, string predicate, params object[] values)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (predicate == null)
                throw new ArgumentNullException("predicate");

            var lambda = DynamicExpression.ParseLambda(source.ElementType, typeof(bool), predicate, values);
            return ExpressionEx.Call(source, methodName, lambda);
        }
    }
}
