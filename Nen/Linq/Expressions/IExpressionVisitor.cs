﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Nen.Linq.Expressions {
    /// <summary>
    /// Visits specific kinds of LINQ expressions.
    /// </summary>
    /// <typeparam name="T">The result of the visit.</typeparam>
    public interface IExpressionVisitor<T> {
        // Operator expressions

        /// <summary>
        /// Visits a binary expression.
        /// </summary>
        /// <param name="expression">The binary expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (BinaryExpression expression);

        /// <summary>
        /// Visits a conditional expression.
        /// </summary>
        /// <param name="expression">The conditional expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (ConditionalExpression expression);

        /// <summary>
        /// Visits a type binary expression.
        /// </summary>
        /// <param name="expression">The type binary expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (TypeBinaryExpression expression);

        /// <summary>
        /// Visits a unary expression.
        /// </summary>
        /// <param name="expression">The unary expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (UnaryExpression expression);

        // Consruction expressions

        /// <summary>
        /// Visits a list initialization expression.
        /// </summary>
        /// <param name="expression">The list initialization expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (ListInitExpression expression);

        /// <summary>
        /// Visits a new array expression.
        /// </summary>
        /// <param name="expression">The new array expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (NewArrayExpression expression);

        /// <summary>
        /// Visits a new expression.
        /// </summary>
        /// <param name="expression">The new expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (NewExpression expression);

        /// <summary>
        /// Visits a member initialization expression.
        /// </summary>
        /// <param name="expression">The member initialization expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (MemberInitExpression expression);

        // Value expressions

        /// <summary>
        /// Visits a member expression.
        /// </summary>
        /// <param name="expression">The member expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (MemberExpression expression);

        /// <summary>
        /// Visits a parameter expression.
        /// </summary>
        /// <param name="expression">The parameter expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (ParameterExpression expression);

        /// <summary>
        /// Visits a constant expression.
        /// </summary>
        /// <param name="expression">The constant expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (ConstantExpression expression);

        // Other expressions

        /// <summary>
        /// Visits a lambda expression.
        /// </summary>
        /// <param name="expression">The lambda expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (LambdaExpression expression);

        /// <summary>
        /// Visits an invocation expression.
        /// </summary>
        /// <param name="expression">The invocation expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (InvocationExpression expression);

        /// <summary>
        /// Visits a method call expresion.
        /// </summary>
        /// <param name="expression">The method call expression to visit.</param>
        /// <returns>The result of the visit.</returns>
        T Visit (MethodCallExpression expression);
    }
}
