﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

using Nen.Internal;

namespace Nen.Linq.Expressions {
    /// <summary>
    /// Extensions to the System.Linq.Expressions.Expression class.
    /// </summary>
    public static class ExpressionEx {
        #region Visitation
        /// <summary>
        /// Determines the specific type of expression and executes the 
        /// appropriately typed visit method.
        /// </summary>
        /// <typeparam name="T">The return value of the visitor.</typeparam>
        /// <param name="expression">The expression to visit.</param>
        /// <param name="visitor">The visitor to accept.</param>
        /// <returns>The visit result.</returns>
        [DebuggerStepThrough]
        public static T Accept<T> (this Expression expression, IExpressionVisitor<T> visitor) {
            if (expression == null)
                throw new ArgumentNullException("expression");

            // Operator expressions
            if (expression is BinaryExpression)
                return visitor.Visit((BinaryExpression) expression);

            if (expression is ConditionalExpression)
                return visitor.Visit((ConditionalExpression) expression);

            if (expression is TypeBinaryExpression)
                return visitor.Visit((TypeBinaryExpression) expression);

            if (expression is UnaryExpression)
                return visitor.Visit((UnaryExpression) expression);

            // Construction expressions
            if (expression is ListInitExpression)
                return visitor.Visit((ListInitExpression) expression);

            if (expression is NewArrayExpression)
                return visitor.Visit((NewArrayExpression) expression);

            if (expression is NewExpression)
                return visitor.Visit((NewExpression) expression);

            if (expression is MemberInitExpression)
                return visitor.Visit((MemberInitExpression) expression);

            // Value expressions
            if (expression is MemberExpression)
                return visitor.Visit((MemberExpression) expression);

            if (expression is ParameterExpression)
                return visitor.Visit((ParameterExpression) expression);

            if (expression is ConstantExpression)
                return visitor.Visit((ConstantExpression) expression);

            // Other expressions
            if (expression is LambdaExpression)
                return visitor.Visit((LambdaExpression) expression);

            if (expression is InvocationExpression)
                return visitor.Visit((InvocationExpression) expression);

            if (expression is MethodCallExpression)
                return visitor.Visit((MethodCallExpression) expression);

            throw new NotImplementedException(LS.T("Expression type '{0}' couldn't be visited.", expression.GetType()));
        }
        #endregion

        #region IQueryable Call
        internal static IQueryable Call (IQueryable source, string methodName) {
            if (source == null)
                throw new ArgumentNullException("source");

            return Call(source, methodName, new Expression[] { source.Expression });
        }

        internal static IQueryable Call (IQueryable source, string methodName, LambdaExpression lambda) {
            if (source == null)
                throw new ArgumentNullException("source");

            return Call(source, methodName, new Expression[] { source.Expression, Expression.Quote(lambda) });
        }

        private static IQueryable Call (IQueryable source, string methodName, Expression[] args) {
            var expression = Expression.Call(typeof(Queryable), methodName, new Type[] { source.ElementType }, args);
            return source.Provider.CreateQuery(expression);
        }
        #endregion

        #region ForEach
#if NET4
        public static BlockExpression ForEach (Expression enumerable, Func<Expression, Expression> loopAction) {
            // Locals
            var enumeratorLocal = Expression.Parameter(typeof(IEnumerator), "enumerator");

            // Get the enumerator
            // enumerator = enumerable.GetEnumerator();
            var getEnumeratorCall = Expression.Call(enumerable, typeof(IEnumerable).GetMethod("GetEnumerator"));
            var assignEnumerator = Expression.Assign(enumeratorLocal, getEnumeratorCall);

            // Loop setup
            var endLoopLabel = Expression.Label();
            var moveNextCall = Expression.Call(enumeratorLocal, typeof(IEnumerator).GetMethod("MoveNext"));
            var currentInstance = Expression.Property(enumeratorLocal, "Current");

            // Loop
            var loopBody = Expression.IfThenElse(moveNextCall, loopAction(currentInstance), Expression.Break(endLoopLabel));
            var loop = Expression.Loop(loopBody, endLoopLabel);

            // Create the iterator
            return Expression.Block(new ParameterExpression[] { enumeratorLocal }, assignEnumerator, loop);
        }
#endif
        #endregion

        #region Boolean Expressions
#if NET4
        public static UnaryExpression IsNull (Expression expression) {
            return Expression.IsTrue(Expression.Equal(expression, Expression.Constant(null)));
        }
#endif
        #endregion
    }
}
