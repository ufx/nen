﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Nen.Linq.Expressions {
    /// <summary>
    /// A LINQ expression visitor with default visitor implementations that
    /// simply return clones of their expression. Convenient for writing
    /// expression transformers.
    /// </summary>
    public class CopyingExpressionVisitor: IExpressionVisitor<Expression> {
        // TODO: preserve as much structure as possible instead of copying everything.

        #region IExpressionVisitor<Expression> Members

        public virtual Expression Visit (BinaryExpression expression) {
            var lhs = expression.Left.Accept(this);
            var rhs = expression.Right.Accept(this);
            var conversion = expression.Conversion != null ? expression.Conversion.Accept(this) : null;

            return Expression.MakeBinary(expression.NodeType, lhs, rhs,
                expression.IsLiftedToNull, expression.Method, (LambdaExpression)conversion);
        }

        public virtual Expression Visit (ConditionalExpression expression) {
            var test = expression.Test.Accept(this);
            var ifTrue = expression.IfTrue.Accept(this);
            var ifFalse = expression.IfFalse.Accept(this);

            return Expression.Condition(test, ifTrue, ifFalse);
        }

        public virtual Expression Visit (TypeBinaryExpression expression) {
            return Expression.TypeIs(expression.Expression, expression.TypeOperand);
        }

        public virtual Expression Visit (UnaryExpression expression) {
            var operand = expression.Operand.Accept(this);
            return Expression.MakeUnary(expression.NodeType, operand, expression.Type, expression.Method);
        }

        public virtual ElementInit Visit (ElementInit init) {
            return Expression.ElementInit(init.AddMethod, init.Arguments.Select(a => a.Accept(this)).ToArray());
        }

        public virtual Expression Visit (ListInitExpression expression) {
            return Expression.ListInit((NewExpression)expression.NewExpression.Accept(this),
                expression.Initializers.Select(i => Visit(i)).ToArray());
        }

        public virtual Expression Visit (NewArrayExpression expression) {
            if (expression.NodeType == ExpressionType.NewArrayBounds)
                return Expression.NewArrayBounds(expression.Type.GetElementType(),
                    expression.Expressions.Select(e => e.Accept(this)).ToArray());

            return Expression.NewArrayInit(expression.Type.GetElementType(),
                expression.Expressions.Select(e => e.Accept(this)).ToArray());
        }

        public virtual Expression Visit (NewExpression expression) {
            return Expression.New(expression.Constructor, 
                expression.Arguments.Select(a => a.Accept(this)).ToArray(),
                expression.Members);
        }

        public MemberBinding Visit (MemberBinding binding) {
            if (binding is MemberListBinding)
                return Visit((MemberListBinding)binding);
            else if (binding is MemberMemberBinding)
                return Visit((MemberMemberBinding)binding);

            throw new Exception("Invalid member binding type.");
        }

        public virtual MemberBinding Visit (MemberListBinding binding) {
            return Expression.ListBind(binding.Member, binding.Initializers.Select(b => Visit(b)).ToArray());
        }

        public virtual MemberBinding Visit (MemberMemberBinding binding) {
            return Expression.MemberBind(binding.Member, binding.Bindings.Select(b => Visit(b)).ToArray());
        }

        public virtual Expression Visit (MemberInitExpression expression) {
            var bindings = expression.Bindings.Select(b => Visit(b)).ToArray();

            return Expression.MemberInit((NewExpression)expression.NewExpression.Accept(this), bindings);
        }

        public virtual Expression Visit (MemberExpression expression) {
            var expr = expression.Expression.Accept(this);
            return Expression.MakeMemberAccess(expr, expression.Member);
        }

        public virtual Expression Visit (ParameterExpression expression) {
            return expression;
        }

        public virtual Expression Visit (ConstantExpression expression) {
            return expression;
        }

        public virtual Expression Visit (LambdaExpression expression) {
            return Expression.Lambda(expression.Type, expression.Body.Accept(this), expression.Parameters);
        }

        public virtual Expression Visit (InvocationExpression expression) {
            return Expression.Invoke(expression.Expression.Accept(this),
                expression.Arguments.Select(a => a.Accept(this)).ToArray());
        }

        public virtual Expression Visit (MethodCallExpression expression) {
            return Expression.Call(
                expression.Object != null ? expression.Object.Accept(this) : null,
                expression.Method,
                expression.Arguments.Select(a => a.Accept(this)).ToArray());
        }

        #endregion
    }
}
