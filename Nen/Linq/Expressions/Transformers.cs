﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Nen.Collections;

namespace Nen.Linq.Expressions {
    /// <summary>
    /// Replaces ParameterExpressions in the body of a lambda expression
    /// with other expressions.
    /// </summary>
    public class SubstituteParametersTransform : CopyingExpressionVisitor {
        private IDictionary<string, Expression> _transforms;
        private ReferenceCountingSet<string> _shadows = new ReferenceCountingSet<string>();

        public SubstituteParametersTransform (IDictionary<string, Expression> transforms) {
            _transforms = transforms;
        }

        public LambdaExpression Transform (LambdaExpression expr) {
            return Expression.Lambda(expr.Type, expr.Body.Accept(this), expr.Parameters);
        }

        public override Expression Visit (LambdaExpression expression) {
            try {
                foreach (var p in expression.Parameters)
                    _shadows.Increment(p.Name);

                return base.Visit(expression);
            } finally {
                foreach (var p in expression.Parameters)
                    _shadows.Decrement(p.Name);
            }
        }

        public override Expression Visit (ParameterExpression expression) {
            if (!_shadows.Contains(expression.Name) && _transforms.ContainsKey(expression.Name))
                return _transforms[expression.Name];

            return expression;
        }
    }
}