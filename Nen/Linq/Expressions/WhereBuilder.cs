﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Nen.Linq.Expressions
{
    public class WhereBuilder<T>
    {
        public Expression<Predicate<T>> Expr { get; private set; }

        public WhereBuilder() { }
        public WhereBuilder(Expression<Predicate<T>> expr)
        {
            Expr = expr;
        }

        public WhereBuilder<T> Where(Expression<Predicate<T>> clause)
        {
            if (Expr == null)
                return new WhereBuilder<T>(clause);

            return new WhereBuilder<T>(Expression.Lambda<Predicate<T>>(Expression.AndAlso(Expr.Body, clause.Body), Expr.Parameters));
        }
    }
}
