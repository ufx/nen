using System;
using System.Collections.Generic;
using System.Text;
using Nen.Reflection;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.Concurrent;

namespace Nen {
    /// <summary>
    /// Extensions to the System.Activator class.
    /// </summary>
    public static class ActivatorEx {
        private static ConcurrentDictionary<Tuple<Type, Type>, Type> _genericTypes0 = new ConcurrentDictionary<Tuple<Type, Type>, Type>();

        private static Type GetGenericType (Type genericType, Type genericTypeArgument) {
            var key = Tuple.Create(genericType, genericTypeArgument);

            Type completeType;
            if (!_genericTypes0.TryGetValue(key, out completeType))
            {
                completeType = genericType.MakeGenericType(genericTypeArgument);
                _genericTypes0[key] = completeType;
            }
            return completeType;
            
        }

        /// <summary>
        /// Creates an instance of a generic type.
        /// </summary>
        /// <param name="genericType">The generic type class, e.g., typeof(Foo&lt;&gt;).</param>
        /// <param name="genericTypeArgument">The generic type argument.</param>
        /// <returns>A new instance of the type.</returns>
        public static object CreateGenericInstance (Type genericType, Type genericTypeArgument) {
            return Activator.CreateInstance(GetGenericType(genericType, genericTypeArgument));
        }

        public static object CreateGenericInstance (Type genericType, Type genericTypeArgument, Type ctorArgumentType, object ctorArgument) {
            return DynamicActivator.CreateInstance(GetGenericType(genericType, genericTypeArgument), ctorArgumentType, ctorArgument);
        }

        public static object CreateGenericInstance (Type genericType, Type genericTypeArgument, Type[] ctorArgumentTypes, object ctorArgument1, object ctorArgument2) {
            return DynamicActivator.CreateInstance(GetGenericType(genericType, genericTypeArgument), ctorArgumentTypes, ctorArgument1, ctorArgument2);
        }

        /// <summary>
        /// Creates an instance of a generic type.
        /// </summary>
        /// <param name="genericType">The generic type class, e.g., typeof(Foo&lt;&gt;).</param>
        /// <param name="genericTypeArguments">The generic type arguments.</param>
        /// <param name="ctorArguments">Arguments to pass to the type constructor.</param>
        /// <returns>A new instance of the type.</returns>
        public static object CreateGenericInstance (Type genericType, Type[] genericTypeArguments, params object[] ctorArguments) {
            if (genericType == null)
                throw new ArgumentNullException("genericType");

            var type = genericType.MakeGenericType(genericTypeArguments);
            return Activator.CreateInstance(type, TypeEx.InstanceBindingFlags, null, ctorArguments, CultureInfo.CurrentCulture);
        }
    }

    public static class DynamicActivator {
        #region 0 Ctor Arguments
        private static Dictionary<Type, Func<object>> _ctors0 = new Dictionary<Type, Func<object>>();
        private static object _ctor0Lock = new object();

        private static Func<object> GetConstructor (Type type) {
            Func<object> ctor;
            lock (_ctor0Lock)
            {
                if (_ctors0.TryGetValue(type, out ctor))
                    return ctor;

                var ctorLambda = Expression.Lambda<Func<object>>(Expression.New(type));
                ctor = ctorLambda.Compile();

                _ctors0[type] = ctor;
            }
            return ctor;
        }

        public static object CreateInstance (Type type) {
            var ctor = GetConstructor(type);
            return ctor();
        }
        #endregion

        #region 1 Ctor Argument
        private static Dictionary<Tuple<Type, Type>, Func<object, object>> _ctors1 = new Dictionary<Tuple<Type, Type>, Func<object, object>>();
        private static object _ctor1Lock = new object();

        private static Func<object, object> GetConstructor (Type type, Type ctorArgumentType) {
            var key = Tuple.Create(type, ctorArgumentType);
            Func<object, object> ctor;
            lock (_ctor1Lock)
            {
                if (_ctors1.TryGetValue(key, out ctor))
                    return ctor;

                var reflectionCtor = type.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { ctorArgumentType }, null);
                var arg1Parameter = Expression.Parameter(typeof(object), "arg1");
                var arg1Converted = Expression.Convert(arg1Parameter, ctorArgumentType);
                var ctorLambda = Expression.Lambda<Func<object, object>>(Expression.New(reflectionCtor, arg1Converted), arg1Parameter);
                ctor = ctorLambda.Compile();

                _ctors1[key] = ctor;
            }
            return ctor;
        }

        public static object CreateInstance (Type type, Type ctorArgumentType, object ctorArgument) {
            var ctor = GetConstructor(type, ctorArgumentType);
            return ctor(ctorArgument);
        }
        #endregion

        #region 2 Ctor Arguments
        private static Dictionary<Tuple<Type, Type, Type>, Func<object, object, object>> _ctors2 = new Dictionary<Tuple<Type, Type, Type>, Func<object, object, object>>();
        private static object _ctor2Lock = new object();

        private static Func<object, object, object> GetConstructor (Type type, Type[] ctorArgumentTypes) {
            var key = Tuple.Create(type, ctorArgumentTypes[0], ctorArgumentTypes[1]);
            Func<object, object, object> ctor;
            lock (_ctor2Lock)
            {
                if (_ctors2.TryGetValue(key, out ctor))
                    return ctor;

                var reflectionCtor = type.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, ctorArgumentTypes, null);
                var arg1Parameter = Expression.Parameter(typeof(object), "arg1");
                var arg1Converted = Expression.Convert(arg1Parameter, ctorArgumentTypes[0]);
                var arg2Parameter = Expression.Parameter(typeof(object), "arg2");
                var arg2Converted = Expression.Convert(arg2Parameter, ctorArgumentTypes[1]);
                var ctorLambda = Expression.Lambda<Func<object, object, object>>(Expression.New(reflectionCtor, arg1Converted, arg2Converted), arg1Parameter, arg2Parameter);
                ctor = ctorLambda.Compile();

                _ctors2[key] = ctor;
            }
            return ctor;
        }

        public static object CreateInstance (Type type, Type[] ctorArgumentTypes, object ctorArgument1, object ctorArgument2) {
            var ctor = GetConstructor(type, ctorArgumentTypes);
            return ctor(ctorArgument1, ctorArgument2);
        }
        #endregion
    }
}
