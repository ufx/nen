﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Nen.Reflection;

namespace Nen {
    public static class EnumEx {
        public static object GetUnderlyingValueIfEnum (object value) {
            if (value == null)
                return null;

            var valueType = value.GetType();
            if (!valueType.IsEnum)
                return value;

            var underlyingType = Enum.GetUnderlyingType(valueType);
            return Convert.ChangeType(value, underlyingType);
        }

        public static Dictionary<TEnum, string> GetDescriptionDictionary<TEnum> () {
            var values = typeof(TEnum).GetVariables(BindingFlags.Public | BindingFlags.Static, MemberTypes.Field);
            return values.ToDictionary(v => (TEnum) Enum.Parse(typeof(TEnum), v.Name), v => v.Description);
        }
    }
}
