﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Workflow {
    [Serializable]
    public class BlockActivity : Activity {
        public override Continuation Execute (Continuation currentContinuation) {
            return BlockingDelimiter.Block(currentContinuation);
        }
    }
}
