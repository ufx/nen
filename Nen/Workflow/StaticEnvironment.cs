﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Nen.ObjectModel;

namespace Nen.Workflow {
    public class StaticEnvironmentEntry {
        public PropertyInfo Property;
        public NenClass ActivityType;
        public int ContextId;
    }

    public class StaticEnvironment {
        public Dictionary<string, StaticEnvironmentEntry> Variables = new Dictionary<string, StaticEnvironmentEntry>();
        public StaticEnvironment Parent;
        public NenClass ActivityType;
        public int TopContextId;

        public StaticEnvironment CreateChild (Activity activity) {
            return new StaticEnvironment { Parent = this, ActivityType = activity.Class, TopContextId = TopContextId };
        }
    }
}
