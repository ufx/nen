﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Nen.Collections;
using Nen.Reflection;

namespace Nen.Workflow {
    [Serializable]
    public class DataflowActivity: Activity, IEventData {
        public List<Activity> Children { get; set; }
        private Dictionary<Activity, DependencyInfo> _dependencies;
        private int _activityIndex;
        private string _eventKey;
        [NonSerialized]
        private ActivityContext _homeContext; // TODO: need a better way of getting the right context to notify

        [Serializable]
        class DependencyInfo {
            public Input[] Inputs;
            public Output[] Outputs;
        }

        public DataflowActivity () {
            Children = new List<Activity>();
        }

        protected override void Prepare (StaticEnvironment env) {
            base.Prepare(env);
        }

        public override Continuation Execute (Continuation currentContinuation) {
            _activityIndex = -1;
            _eventKey = Guid.NewGuid().ToString();
            _dependencies = new Dictionary<Activity, DependencyInfo>();
            _homeContext = ActivityContext.CurrentContext;

            return currentContinuation.Push(Fire).Push(Next);
        }

        private Continuation Next (Continuation currentContinuation) {
            _homeContext = ActivityContext.CurrentContext;

            if (_activityIndex != -1) {
                var activity = GetContext(Children[_activityIndex].ContextId);
                var deps = new DependencyInfo {
                    Inputs = GetInputs(activity).ToArray(),
                    Outputs = GetOutputs(activity).ToArray()
                };

                foreach (var input in deps.Inputs)
                    input.InputReady += NotifyDataReady;

                foreach (var output in deps.Outputs)
                    output.OutputReady += NotifyDataReady;

                _dependencies[activity] = deps;
            }

            _activityIndex++;
            if (_activityIndex < Children.Count)
                return currentContinuation.Push(Next).Call(this, Children[_activityIndex]);
            else
                return currentContinuation;
        }

        private Continuation Fire (Continuation currentContinuation) {
            _homeContext = ActivityContext.CurrentContext;

            if (_dependencies.Count > 0) {
                var fireContinuation = currentContinuation = currentContinuation.Push(Fire);
                var hasUnfinishedWork = false;

                foreach (var kv in _dependencies) {
                    if (kv.Value.Inputs.All(input => input.IsInputReady) && 
                        kv.Value.Outputs.All(output => output.IsOutputReady)) {
                        currentContinuation = currentContinuation.Push(kv.Key.Execute);
                    }

                    if (kv.Value.Outputs.Any(output => !output.IsFinished))
                        hasUnfinishedWork = true;
                }

                if (fireContinuation == currentContinuation)
                    if (hasUnfinishedWork)
                        return Sync.Wait(_eventKey, this, currentContinuation);
                    else
                        return currentContinuation.Next;
            }

            return currentContinuation;
        }

        private void NotifyDataReady (object sender, EventArgs e) {
            Sync.Notify(_homeContext ?? ActivityContext.CurrentContext, _eventKey, sender);
        }

        private IEnumerable<Input> GetInputs (Activity activity) {
            return GetDependencies<Input>(activity);
        }

        private IEnumerable<Output> GetOutputs (Activity activity) {
            return GetDependencies<Output>(activity);
        }

        private IEnumerable<T> GetDependencies<T> (Activity activity) where T: IDataStream {
            foreach (var pi in activity.GetType().GetProperties()) {
                if (pi.GetIndexParameters().Length > 0)
                    continue;

                if (typeof(T).IsAssignableFrom(pi.PropertyType))
                    yield return (T)pi.GetValue(activity, null);

                if (typeof(IEnumerable<T>).IsAssignableFrom(pi.PropertyType))
                    foreach (var val in ((IEnumerable<T>)pi.GetValue(activity, null)))
                        yield return val;
            }
        }

        #region IEventData Members

        object IEventData.EventData { get { return null; } set { } }

        #endregion
    }
}
