﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.ObjectModel;

namespace Nen.Workflow {
    [Serializable]
    public class LoopActivity : Activity {
        public static readonly NenProperty<int> LoopStartProperty;
        public static readonly NenProperty<int> LoopEndProperty;

        public Activity Action { get; set; }

        [Local]
        public int LoopStart {
            get { return (int)GetValue(LoopStartProperty); }
            set { SetValue(LoopStartProperty, value); }
        }

        [Local]
        public int LoopEnd {
            get { return (int)GetValue(LoopEndProperty); }
            set { SetValue(LoopEndProperty, value); }
        }

        [Local]
        public int LoopValue { get; private set; }

        public override Continuation Execute (Continuation currentContinuation) {
            LoopValue = LoopStart;

            return NextIteration(currentContinuation);
        }

        private Continuation NextIteration (Continuation currentContinuation) {
            bool inRange = LoopStart < LoopEnd ? (LoopValue < LoopEnd) : (LoopValue > LoopStart);

            if (inRange)
                return currentContinuation.Push(FinishIteration).Call(this, Action);
            else
                return currentContinuation;
        }

        private Continuation FinishIteration (Continuation currentContinuation) {
            LoopValue += LoopStart < LoopEnd ? 1 : -1;
            return NextIteration(currentContinuation);
        }
    }
}
