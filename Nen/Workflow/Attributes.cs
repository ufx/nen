﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Workflow {
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class LocalAttribute : Attribute { }
}
