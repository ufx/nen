﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Nen.Runtime.Serialization;

namespace Nen.Workflow {
    [Serializable]
    public class BlockingDelimiter: ISerializable {
        public static readonly BlockingDelimiter Instance = new BlockingDelimiter();
        private BlockingDelimiter () { }

        public static Continuation Block (Continuation currentContinuation) {
            for (Continuation c = currentContinuation; c != null; c = c.Next)
                if (c.Delimiter == BlockingDelimiter.Instance)
                    return c;

            return null; // if nobody wants to take control, go idle.
        }

        #region ISerializable Members

        public void GetObjectData (SerializationInfo info, StreamingContext context) {
            info.SetType(typeof(SingletonSurrogate<BlockingDelimiter>));
        }

        #endregion
    }
}
