﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Workflow {
    [Serializable]
    public class ParallelActivity : Activity {
        public List<Activity> Children { get; private set; }
        [Local]
        private int _startedCount;
        [Local]
        private int _completedCount;

        public ParallelActivity () {
            Children = new List<Activity>();
        }

        public override Continuation Execute (Continuation currentContinuation) {
            _startedCount = 0;
            _completedCount = 0;
            return NextChild(currentContinuation);
        }

        private Continuation NextChild (Continuation currentContinuation) {
            if (_startedCount < Children.Count) {
                Activity child = Children[_startedCount++];

                return currentContinuation.Push(NextChild, BlockingDelimiter.Instance).Push(FinishChild).Call(this, child);
            } else if (_completedCount == Children.Count) {
                return currentContinuation;
            } else {
                return BlockingDelimiter.Block(currentContinuation);
            }
        }

        private Continuation FinishChild (Continuation currentContinuation) {
            _completedCount++;
            return currentContinuation;
        }
    }
}
