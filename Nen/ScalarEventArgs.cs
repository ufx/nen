﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen
{
	public class ScalarEventArgs<T>: EventArgs
	{
		public T Value { get; set; }

		public ScalarEventArgs() { }
		public ScalarEventArgs(T value)
		{
			Value = value;
		}
	}
}
