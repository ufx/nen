﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.IO;

using Nen.Collections;
using Nen.Internal;
using Nen.Net.Sockets;

namespace Nen.Messaging {
    public class StompClient : PersistentTcpClient {
        private NetworkStream _stream;
        private StreamWriter _writer;
        private bool _disposed;
        private List<StompFrame> _reconnectFrames = new List<StompFrame>();
        private object _writerLock = new object();

        #region Constructors / Disposer
        public StompClient () {
        }

        protected override void Dispose (bool disposing) {
            if (disposing && !_disposed) {
                _disposed = true;

                if (_writer != null)
                    _writer.Dispose();

                base.Dispose(disposing);
            }
        }
        #endregion

        #region Connection Maintenance
        protected override void OnConnected () {
            base.OnConnected();

            lock (_writerLock) {
                _stream = UnderlyingClient.GetStream();
                _writer = new StreamWriter(_stream);

                foreach (var frame in _reconnectFrames) {
                    if (frame.Command == "CONNECT")
                        WriteConnectFrame(frame);
                    else
                        frame.Write(_writer);
                }
            }
        }
        #endregion

        #region Reading / Writing
        public void WriteFrame (StompFrame frame) {
            lock (_writerLock)
                frame.Write(_writer);
        }

        private string WriteConnectFrame (StompFrame frame) {
            lock (_writerLock) {
                frame.Write(_writer);

                var connected = ReadFrame();
                if (connected != null && connected.Headers.ContainsKey("session")) {
                    SessionId = connected.Headers["session"];
                    return SessionId;
                }
            }

            return null;
        }

        public StompFrame ReadFrame () {
            return Once<StompFrame>(() => {
                try {
                    var frame = StompFrame.Read(_stream);
                    if (frame == null)
                        return null;

                    // Check for errors.
                    if (frame.Command == null || string.Compare(frame.Command, "ERROR", true) == 0 || frame.IsMalformed) {
                        string message = frame.Command + ": Frame is malformed";
                        if (frame.Headers.ContainsKey("message"))
                            message = frame.Headers["message"];
                        throw new StompException(message);
                    } else if (frame.IsMalformed)
                        throw new StompException("Frame is malformed");

                    return frame;
                } catch (IOException ex) {
                    var socketException = ex.InnerException as SocketException;
                    if (socketException != null && socketException.SocketErrorCode == SocketError.TimedOut)
                        throw new StompException("Read Timeout", socketException);
                    throw;
                }
            });
        }

        public void Write (StompMessage message) {
            Once(() => StompSend(message.Destination, message.Body, message.Headers));
        }

        public StompMessage Read () {
            var frame = ReadFrame();
            if (frame == null)
                return null;

            if (string.Compare(frame.Command, "MESSAGE", true) != 0)
                throw new StompException(LS.T("Expected MESSAGE frame, received {0}", frame.Command));

            var message = new StompMessage();
            message.Body = frame.Body;
            message.Headers = frame.Headers;
            return message;
        }
        #endregion

        #region Commands
        public string StompConnect () {
            return StompConnect(null, null, null);
        }

        public string StompConnect (Dictionary<string, string> headers) {
            return StompConnect(null, null, headers);
        }

        public string StompConnect (string login, string passcode, Dictionary<string, string> headers) {
            var otherHeaders = new Dictionary<string, string>();

            if (login != null)
                otherHeaders["login"] = login;

            if (passcode != null)
                otherHeaders["passcode"] = passcode;

            var connect = new StompFrame("CONNECT", headers.Merge(otherHeaders));
            SessionId = WriteConnectFrame(connect);

            _reconnectFrames.Add(connect);
            return SessionId;
        }

        public void StompSend (string destination, string body) {
            StompSend(destination, body, null);
        }

        public void StompSend (string destination, string body, Dictionary<string, string> headers) {
            if (destination == null)
                throw new ArgumentNullException("destination");

            if (body == null)
                throw new ArgumentNullException("body");

            var frame = new StompFrame("SEND", body, headers.Merge(DictionaryEx.FromPairs("destination", destination)));
            WriteFrame(frame);
        }

        public void StompSubscribe (string destination) {
            StompSubscribe(destination, Ack.Auto, null);
        }

        public void StompSubscribe (string destination, Ack ack, Dictionary<string, string> headers) {
            if (destination == null)
                throw new ArgumentNullException("destination");

            var otherHeaders = new Dictionary<string, string>();
            otherHeaders["destination"] = destination;
            if (ack != Ack.Auto)
                otherHeaders["ack"] = ack.ToString().ToLower();

            var frame = new StompFrame("SUBSCRIBE", otherHeaders.Merge(headers));
            WriteFrame(frame);

            _reconnectFrames.Add(frame);
        }

        public void StompUnsubscribe (string destination, Dictionary<string, string> headers) {
            if (destination == null)
                throw new ArgumentNullException("destination");

            var frame = new StompFrame("UNSUBSCRIBE", headers.Merge(DictionaryEx.FromPairs("destination", destination)));
            WriteFrame(frame);

            _reconnectFrames.RemoveAll(f => f.Headers["destination"] == destination);
        }

        public void StompBegin (string transactionId, Dictionary<string, string> headers) {
            if (transactionId == null)
                throw new ArgumentNullException("transactionId");

            var frame = new StompFrame("BEGIN", headers.Merge(DictionaryEx.FromPairs("transaction", transactionId)));
            WriteFrame(frame);
        }

        public void StompCommit (string transactionId, Dictionary<string, string> headers) {
            if (transactionId == null)
                throw new ArgumentNullException("transactionId");

            var frame = new StompFrame("COMMIT", headers.Merge(DictionaryEx.FromPairs("transaction", transactionId)));
            WriteFrame(frame);
        }

        public void StompAck (string messageId, string transactionId, Dictionary<string, string> headers) {
            if (messageId == null)
                throw new ArgumentNullException("messageId");

            var otherHeaders = new Dictionary<string, string>();
            otherHeaders["message-id"] = messageId;
            if (transactionId != null)
                otherHeaders["transaction"] = transactionId;

            var frame = new StompFrame("ACK", headers.Merge(otherHeaders));
            WriteFrame(frame);
        }

        public void StompAck (StompMessage message) {
            if (message == null)
                throw new ArgumentNullException("message");

            StompAck(message.MessageId, null, null);
        }

        public void StompAbort (string transactionId, Dictionary<string, string> headers) {
            if (transactionId == null)
                throw new ArgumentNullException("transactionId");

            var frame = new StompFrame("ABORT", headers.Merge(DictionaryEx.FromPairs("transaction", transactionId)));
            WriteFrame(frame);
        }

        public void StompDisconnect (Dictionary<string, string> headers) {
            var frame = new StompFrame("DISCONNECT", headers);
            WriteFrame(frame);

            _reconnectFrames.Clear();
        }
        #endregion

        #region Accessors
        public string SessionId { get; private set; }
        #endregion
    }

    public class StompFrame {
        #region Constructors
        public StompFrame () {
            Headers = new Dictionary<string, string>();
        }

        public StompFrame (string command, Dictionary<string, string> headers)
            : this(command, null, headers) {
        }

        public StompFrame (string command, string body, Dictionary<string, string> headers) {
            Command = command;
            Body = body;
            Headers = headers;
        }
        #endregion

        #region Reading / Writing
        public void Write (StreamWriter writer) {
            writer.Write(Command + '\n');

            if (Headers != null) {
                foreach (var header in Headers)
                    writer.Write(header.Key + ":" + header.Value + '\n');
            }

            if (Body != null) {
                // Include content-length if the message body contains a terminating character.
                if (Body.Contains('\0'))
                    writer.Write("content-length:" + Body.Length + '\n');

                writer.Write('\n' + Body);
            } else
                writer.Write('\n');

            writer.Write("\0\n");
            writer.Flush();
        }

        public static StompFrame Read (Stream stream) {
            int lastByte = 0;
            int bytesRemaining = -1;
            StompFrame frame = null;

            using (var buffer = new MemoryStream()) {
                while (true) {
                    var b = stream.ReadByte();
                    if (frame == null && b == '\n' && lastByte == '\n') {
                        frame = BuildFrame(Encoding.UTF8.GetString(buffer.ToArray()));

                        if (frame.Headers.ContainsKey("content-length"))
                            bytesRemaining = int.Parse(frame.Headers["content-length"]);

                        buffer.SetLength(0);
                        continue;
                    }

                    if (b == -1 || (b == '\0' && bytesRemaining <= 0)) {
                        // Discard the last newline.
                        b = stream.ReadByte();

                        var body = Encoding.UTF8.GetString(buffer.ToArray());
                        if (frame == null)
                            frame = new StompFrame { IsMalformed = true };
                        frame.Body = body;
                        return frame;
                    }

                    if (bytesRemaining > 0)
                        bytesRemaining--;

                    buffer.WriteByte((byte) b);
                    lastByte = b;
                }
            }
        }

        private static StompFrame BuildFrame (string content) {
            var frame = new StompFrame();

            var lines = content.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length == 0) {
                frame.IsMalformed = true;
                return frame;
            }

            frame.Command = lines[0];

            for (int i = 1; i < lines.Length; i++) {
                var line = lines[i];
                var separatorIndex = line.IndexOf(':');
                if (separatorIndex == -1) {
                    frame.Headers["bad-header" + i] = line;
                    frame.IsMalformed = true;
                    continue;
                }

                var header = line.Substring(0, separatorIndex).ToLower();
                var value = line.Substring(separatorIndex + 1, line.Length - separatorIndex - 1).Trim();

                frame.Headers[header] = value.Trim();
            }

            return frame;
        }
        #endregion

        public string Command { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public string Body { get; set; }
        public bool IsMalformed { get; set; }

        public override string ToString () {
            return Command ?? base.ToString();
        }
    }

    [Serializable]
    public class StompException : Exception {
        public StompException () {
        }

        public StompException (string message, Exception inner)
            : base(message, inner) {
        }

        public StompException (string message)
            : base(message) {
        }

        protected StompException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }

    public class StompMessage {
        public StompMessage () {
            Headers = new Dictionary<string, string>();
        }

        public StompMessage (string body) {
            Body = body;
        }

        public Dictionary<string, string> Headers { get; set; }
        public string Body { get; set; }

        public string Destination {
            get { return Headers.ContainsKey("destination") ? Headers["destination"] : null; }
            set { Headers["destination"] = value; }
        }

        public string MessageId {
            get { return Headers.ContainsKey("message-id") ? Headers["message-id"] : null; }
            set { Headers["message-id"] = value; }
        }

        public bool IsPersistent {
            get { return Headers.ContainsKey("persistent") && Headers["persistent"] == "true"; }
            set { Headers["persistent"] = value.ToString().ToLower(); }
        }

        public string CorrelationId {
            get { return Headers.ContainsKey("correlation-id") ? Headers["correlation-id"] : null; }
            set { Headers["correlation-id"] = value; }
        }

        public string ReplyTo {
            get { return Headers.ContainsKey("reply-to") ? Headers["reply-to"] : null; }
            set { Headers["reply-to"] = value; }
        }
    }

    public enum Ack {
        Auto,
        Client
    }
}
