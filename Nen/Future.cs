using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Nen {
    #region IFuture
    /// <summary>
    /// Captures a computation with deferred resolution.
    /// </summary>
    public interface IFuture {
        /// <summary>
        /// Raised when the computation has been resolved.
        /// </summary>
        event EventHandler<ComputationResultEventArgs> Resolved;

        /// <summary>
        /// Determines whether a computation is resolved.
        /// </summary>
        bool IsResolved { get; }

        /// <summary>
        /// Resolves the computation on the current thread.
        /// </summary>
        /// <returns>The computation result.</returns>
        object Resolve ();
    }

    /// <summary>
    /// Captures a computation with deferred resolution.
    /// </summary>
    /// <typeparam name="T">The type of the result.</typeparam>
    public interface IFuture<T> : IFuture {
        /// <summary>
        /// Begins resolution of the computation.
        /// </summary>
        /// <param name="callback">The callback to invoke upon resolving the computation.</param>
        /// <param name="state">The callback state.</param>
        /// <returns>The status of the asynchronous operation.</returns>
        IAsyncResult BeginResolve (AsyncCallback callback, object state);

        /// <summary>
        /// Blocks until the resolution of the computation.
        /// </summary>
        /// <param name="result">The asynchronous operation status.</param>
        /// <returns>The result of the computation.</returns>
        T EndResolve (IAsyncResult result);

        /// <summary>
        /// Resolves the computation on the current thread.
        /// </summary>
        /// <returns>The computation result.</returns>
        new T Resolve ();

        /// <summary>
        /// Raised when the computation has been resolved.
        /// </summary>
        new event EventHandler<ComputationResultEventArgs<T>> Resolved;
    }

    /// <summary>
    /// Captures a variable result in the IFuture interface.  These results may
    /// change.
    /// </summary>
    public interface IVariableFuture : IFuture {
        /// <summary>
        /// Gets or sets the result vaule.
        /// </summary>
        object Value { get; set; }
    }

    /// <summary>
    /// Captures a variable result in the IFuture interface.  These results may
    /// change.
    /// </summary>
    /// <typeparam name="T">The type of result.</typeparam>
    public interface IVariableFuture<T> : IVariableFuture, IFuture<T> {
        /// <summary>
        /// Gets or sets the result value.
        /// </summary>
        new T Value { get; set; }
    }
    #endregion

    /// <summary>
    /// Core implementation of IFuture that stores its result.
    /// </summary>
    /// <typeparam name="T">The type of the result.</typeparam>
    public abstract class FutureCore<T> : IFuture<T> {
        private Func<T> _resolveCallback;
        private object _resolveCallbackCheckLock = new object();
        private Dictionary<EventHandler<ComputationResultEventArgs>, EventHandler<ComputationResultEventArgs<T>>> _convertedResolvedDelegates;

        #region Constructors
        /// <summary>
        /// Constructs a new FutureCore.
        /// </summary>
        protected FutureCore () { }

        /// <summary>
        /// Constructs a new FutureCore with a precomputed result.
        /// </summary>
        /// <param name="result">The precomputed result.</param>
        protected FutureCore (T result)
            : this() {
            Result = result;
        }
        #endregion

        #region Computation Resolution
        private void EnsureResolveCallback()
        {
            lock (_resolveCallbackCheckLock)
            {
                if (_resolveCallback == null)
                {
                    _resolveCallback = Resolve;
                    _convertedResolvedDelegates = new Dictionary<EventHandler<ComputationResultEventArgs>, EventHandler<ComputationResultEventArgs<T>>>();
                }
            }
        }

        /// <summary>
        /// Begins resolution of the computation.
        /// </summary>
        /// <param name="callback">The callback to invoke upon resolving the computation.</param>
        /// <param name="state">The callback state.</param>
        /// <returns>The status of the asynchronous operation.</returns>
        public IAsyncResult BeginResolve (AsyncCallback callback, object state) {
            EnsureResolveCallback();
            return _resolveCallback.BeginInvoke(callback, state);
        }

        /// <summary>
        /// Blocks until the resolution of the computation.
        /// </summary>
        /// <param name="result">The asynchronous operation status.</param>
        /// <returns>The result of the computation.</returns>
        public T EndResolve (IAsyncResult result) {
            EnsureResolveCallback();
            return _resolveCallback.EndInvoke(result);
        }

        /// <summary>
        /// Resolves the computation on the current thread.
        /// </summary>
        /// <returns>The computation result.</returns>
        public abstract T Resolve ();

        object IFuture.Resolve () {
            return Resolve();
        }

        /// <summary>
        /// Raised when the computation has been resolved.
        /// </summary>
        public abstract event EventHandler<ComputationResultEventArgs<T>> Resolved;

        event EventHandler<ComputationResultEventArgs> IFuture.Resolved {
            add {
                EnsureResolveCallback();
                _convertedResolvedDelegates.Add(value, new EventHandler<ComputationResultEventArgs<T>>(value));
                Resolved += _convertedResolvedDelegates[value];
            }
            remove {
                EnsureResolveCallback();
                Resolved -= _convertedResolvedDelegates[value];
                _convertedResolvedDelegates.Remove(value);
            }
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Determines whether a computation is resolved.
        /// </summary>
        public abstract bool IsResolved { get; }

        /// <summary>
        /// Accesses the computation result if present.
        /// </summary>
        protected T Result { get; set; }
        #endregion

        #region Object Members
        public override string ToString () {
            if (IsResolved) {
                if (Result == null)
                    return "null";
                else
                    return Result.ToString();
            }

            return "Unresolved " + typeof(T).Name;
        }
        #endregion
    }

    /// <summary>
    /// Captures a computation with deferred resolution.
    /// </summary>
    public class Future<T> : FutureCore<T> {
        private bool _isResolved;

        #region Constructors
        /// <summary>
        /// Constructs a new Future around the given computation.
        /// </summary>
        /// <param name="computation">The computation to resolve.</param>
        public Future (Func<T> computation) {
            Computation = computation;
        }
        #endregion

        #region Computation Resolution
        /// <summary>
        /// Resolves the computation on the current thread.
        /// </summary>
        /// <returns>The computation result.</returns>
        public override T Resolve () {
            lock (this) {
                if (_isResolved)
                    return Result;

                Result = Computation();
                _isResolved = true;
                OnResolved();

                return Result;
            }
        }

        /// <summary>
        /// Raised when the computation has been resolved.
        /// </summary>
        public override event EventHandler<ComputationResultEventArgs<T>> Resolved;

        /// <summary>
        /// Called when the computation has been resolved.
        /// </summary>
        protected virtual void OnResolved () {
            if (Resolved != null)
                Resolved(this, new ComputationResultEventArgs<T>(Result));
        }
        #endregion

        #region Accessors
        /// <summary>
        /// The computation to resolve.
        /// </summary>
        public Func<T> Computation { get; private set; }

        /// <summary>
        /// Determines whether a computation is resolved.
        /// </summary>
        public override bool IsResolved {
            get { return _isResolved; }
        }
        #endregion
    }

    /// <summary>
    /// Wraps a precomputed result in the Future interface.  This is useful
    /// when a computation is usually accessed via the future interface, but
    /// its result is known already.
    /// </summary>
    /// <typeparam name="T">The type of result.</typeparam>
    public abstract class PreComputedFuture<T> : FutureCore<T> {
        #region Constructors
        /// <summary>
        /// Creates a new PreComputedFuture.
        /// </summary>
        public PreComputedFuture () {
        }
        
        /// <summary>
        /// Constructs a new PreComputedFuture with a precomputed result.
        /// </summary>
        /// <param name="result">The precomputed result.</param>
        public PreComputedFuture (T result)
            : base(result) {
        }
        #endregion

        #region Computation Resolution
        /// <summary>
        /// Immediately returns the precomputed result.
        /// </summary>
        /// <returns>The precomputed result.</returns>
        public override T Resolve () {
            return Result;
        }

        /// <summary>
        /// This event is never raised because the result is already known.
        /// </summary>
        public override event EventHandler<ComputationResultEventArgs<T>> Resolved {
            add { }
            remove { }
        }
        #endregion

        public override string ToString () {
            return Equals(Result, default(T)) ? "F:(default)" : "F:" + Result.ToString();
        }
    }

    /// <summary>
    /// Wraps a constant result in the Future interface.  These results
    /// will never change.
    /// </summary>
    /// <typeparam name="T">The type of result.</typeparam>
    public class ConstantFuture<T> : PreComputedFuture<T> {
        #region Constructors
        /// <summary>
        /// Constructs a new ConstantFuture.
        /// </summary>
        /// <param name="result">The type of result.</param>
        public ConstantFuture (T result)
            : base(result) {
        }
        #endregion

        public override bool IsResolved {
            get { return true; }
        }
    }

    /// <summary>
    /// Wraps a variable result in the Future interface.  These results may
    /// change.
    /// </summary>
    /// <typeparam name="T">The type of result.</typeparam>
    public class VariableFuture<T> : PreComputedFuture<T>, IVariableFuture<T> {
        private bool _isResolved;

        #region Constructors
        /// <summary>
        /// Constructs a new VariableFuture.
        /// </summary>
        public VariableFuture ()
            : base() {
        }

        /// <summary>
        /// Constructs a new VariableFuture.
        /// </summary>
        /// <param name="result">The precomputed result.</param>
        public VariableFuture (T result)
            : base(result) {
        }
        #endregion

        #region Computation Resolution
        /// <summary>
        /// Returns the stored result value.
        /// </summary>
        /// <returns>The stored result value.</returns>
        public override T Resolve () {
            return Result;
        }

        /// <summary>
        /// This event is never raised because the result is already known.
        /// </summary>
        public override event EventHandler<ComputationResultEventArgs<T>> Resolved {
            add { }
            remove { }
        }
        #endregion

        #region Accessors
        object IVariableFuture.Value {
            get { return Value; }
            set {
                _isResolved = true;
                Value = (T) value;
            }
        }

        /// <summary>
        /// Gets or sets the result value.
        /// </summary>
        public T Value {
            get { return Result; }
            set { Result = value; }
        }

        /// <summary>
        /// Always returns true because the result is already known.
        /// </summary>
        public override bool IsResolved {
            get { return _isResolved; }
        }
        #endregion
    }

    /// <summary>
    /// Captures a list computation with deferred resolution.
    /// </summary>
    /// <typeparam name="T">The type of list.</typeparam>
    public class ListFuture<T> : Future<List<T>>, IList<T> {
        private List<T> _addedItems = new List<T>();

        #region Constructors
        /// <summary>
        /// Constructs a new ListFuture around the given computation.
        /// </summary>
        /// <param name="computation"></param>
        public ListFuture (Func<List<T>> computation)
            : base(computation) {
        }
        #endregion

        #region Future Members
        /// <summary>
        /// Called when the list has been resolved, after added items have
        /// been merged with the result.
        /// </summary>
        protected override void OnResolved () {
            Result.AddRange(_addedItems);
            base.OnResolved();
        }
        #endregion

        #region IList<T> Members
        /// <summary>
        /// Resolves the list computation and returns the index of the item.
        /// </summary>
        /// <param name="item">The item to search for.</param>
        /// <returns>The index.</returns>
        public int IndexOf (T item) {
            return Resolve().IndexOf(item);
        }

        /// <summary>
        /// Resolves the list computation and inserts the item at the given index.
        /// </summary>
        /// <param name="index">The index at which the item is inserted.</param>
        /// <param name="item">The item to insert.</param>
        public void Insert (int index, T item) {
            Resolve().Insert(index, item);
        }

        /// <summary>
        /// Resolves the list computation and removes the item at the given index.
        /// </summary>
        /// <param name="index">The index at which the item is to be removed.</param>
        public void RemoveAt (int index) {
            Resolve().RemoveAt(index);
        }

        /// <summary>
        /// Resolves the list computation and accesses the item at the given index.
        /// </summary>
        /// <param name="index">The index to access.</param>
        /// <returns>The item.</returns>
        public T this[int index] {
            get { return Resolve()[index]; }
            set { Resolve()[index] = value; }
        }
        #endregion

        #region ICollection<T> Members
        /// <summary>
        /// Resolves the list computation and adds the item to it.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public void Add (T item) {
            _addedItems.Add(item);

            if (IsResolved)
                Resolve().Add(item);
        }

        /// <summary>
        /// Resolves the list computation and clears the list.
        /// </summary>
        public void Clear () {
            _addedItems.Clear();
            Resolve().Clear();
        }

        /// <summary>
        /// Resolves the computation and determines if the list contains the given item.
        /// </summary>
        /// <param name="item">The item to query.</param>
        /// <returns>true if the item exists in the list, false if otherwise.</returns>
        public bool Contains (T item) {
            if (_addedItems.Contains(item))
                return true;

            return Resolve().Contains(item);
        }

        /// <summary>
        /// Resolves the list computation and copies the items to the given array.
        /// </summary>
        /// <param name="array">The array to copy the items into.</param>
        /// <param name="arrayIndex">The index to begin copying from.</param>
        public void CopyTo (T[] array, int arrayIndex) {
            Resolve().CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Accesses the number of items within the list.  Possibly resolves
        /// the list computation.
        /// </summary>
        public virtual int Count {
            get { return Resolve().Count; }
        }

        bool ICollection<T>.IsReadOnly {
            get { return ((ICollection<T>) Resolve()).IsReadOnly; }
        }

        /// <summary>
        /// Resolves the list computation and removes the item from it.
        /// </summary>
        /// <param name="item">The item to remove.</param>
        /// <returns>true if the item was removed, false if otherwise.</returns>
        public bool Remove (T item) {
            if (_addedItems.Remove(item))
                return true;

            return Resolve().Remove(item);
        }
        #endregion

        #region IEnumerable<T> Members
        /// <summary>
        ///  Resolves the list computation and retrieves an enumerator for its contents.
        /// </summary>
        /// <returns>The list enumerator.</returns>
        public IEnumerator<T> GetEnumerator () {
            return Resolve().GetEnumerator();
        }
        #endregion

        #region IEnumerable Members
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator () {
            return GetEnumerator();
        }
        #endregion

        #region Added Items
        /// <summary>
        /// Gets the list of newly added items.
        /// </summary>
        public IEnumerable<T> AddedItems {
            get { return _addedItems; }
        }

        /// <summary>
        /// Clears the list of newly added items, but does not remove them
        /// from the underlying result list if the values have previously
        /// been merged.
        /// </summary>
        public void ClearAddedItems () {
            _addedItems.Clear();
        }
        #endregion
    }

    #region ComputationResult
    /// <summary>
    /// Contains data for deferred computation results.
    /// </summary>
    public abstract class ComputationResultEventArgs : EventArgs {
        /// <summary>
        /// Accesses the computation result.
        /// </summary>
        public abstract object Result { get; }
    }

    /// <summary>
    /// Contains data for deferred computation results.
    /// </summary>
    public class ComputationResultEventArgs<T> : ComputationResultEventArgs {
        #region Constructors
        /// <summary>
        /// Constructs a new ComputationResultEventArgs with the given result.
        /// </summary>
        /// <param name="result">The computation result.</param>
        public ComputationResultEventArgs (T result) {
            SpecificResult = result;
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Accesses the specific computation result.
        /// </summary>
        public T SpecificResult { get; private set; }

        /// <summary>
        /// Accesses the computation result.
        /// </summary>
        public override object Result {
            get { return SpecificResult; }
        }
        #endregion
    }
    #endregion
}
