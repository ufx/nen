﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;

namespace Nen.Windows.Platform {
    /// <summary>
    /// Provides low level access to GDI Windows API functions.
    /// </summary>
    [CLSCompliant(false)]
    public class Gdi {
        public const int GGO_METRICS = 0;
        public const int GGO_BITMAP = 1;
        public const int GGO_NATIVE = 2;
        public const int GGO_BEZIER = 3;
        public const int GGO_UNHINTED = 0x100;

        public const int TT_PRIM_LINE = 1;
        public const int TT_PRIM_QSPLINE = 2;
        public const int TT_PRIM_CSPLINE = 3;

        [DllImport("gdi32.dll")]
        public static extern int GetGlyphOutline (IntPtr hdc, int uChar, int uFormat, ref GLYPHMETRICS lpgm,
                                                   int cbBuffer, IntPtr lpvBuffer, ref MAT2 lpmat2);

        public struct FIXED {
            public ushort fract;
            public short value;

            public double ToDouble () {
                return (double)value + (double)fract * (1.0 / 65536.0);
            }

            public int GetBits () {
                return ((ushort)value << 16) | fract;
            }

            public static FIXED FromBits (int bits) {
                FIXED fix = new FIXED();

                fix.value = (short)(bits >> 16);
                fix.fract = (ushort)(bits & 0xFFFF);

                return fix;
            }
        }

        public struct MAT2 {
            public FIXED eM11;
            public FIXED eM12;
            public FIXED eM21;
            public FIXED eM22;
        }

        public struct POINT {
            public int X;
            public int Y;
        }

        public struct POINTFX {
            public FIXED X;
            public FIXED Y;

            public PointF ToPoint () {
                return new PointF((float)X.ToDouble(), (float)Y.ToDouble());
            }
        }

        public struct TTPOLYGONHEADER {
            public int cb;
            public int dwType;
            public POINTFX pfxStart;
        }

        public struct TTPOLYCURVE {
            public short wType;
            public short cpfx;
            public POINTFX apfx;
        }

        public struct GLYPHMETRICS {
            public int gmBlackBoxX;
            public int gmBlackBoxY;
            public POINT gmptGlyphOrigin;
            public short gmCellIncX;
            public short gmCellIncY;
        }
    }
}
