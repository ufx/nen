using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Linq;
using System.Text;

namespace Nen.Reflection {
    /// <summary>
    /// Extensions to the System.Reflection.MemberInfo class.
    /// </summary>
    public static class MemberInfoEx {
        #region Attributes
        /// <summary>
        /// Retrieves the custom attributes identified by the given type.
        /// </summary>
        /// <typeparam name="T">The type of attribute.</typeparam>
        /// <param name="member">The member to search.</param>
        /// <param name="inherit">Specifies whether to search the member's inheritance chain to find the attributes.</param>
        /// <returns>An array of matching attributes.</returns>
        public static T[] GetAttributes<T> (this MemberInfo member, bool inherit) where T : Attribute {
            return member.GetCustomAttributes(typeof(T), inherit).Cast<T>().ToArray();
        }

        /// <summary>
        /// Retrieves the first custom attribute identified by the given type.
        /// </summary>
        /// <typeparam name="T">The type of attribute.</typeparam>
        /// <param name="member">The member to search.</param>
        /// <param name="inherit">Specifies whether to search the member's inheritance chain to find the attributes.</param>
        /// <returns>The first value in an array of matching attributes, or null if the attribute wasn't found.</returns>
        public static T GetAttribute<T> (this MemberInfo member, bool inherit) where T : Attribute {
            return member.GetAttributes<T>(inherit).ElementAtOrDefault(0);
       }
        #endregion

        #region Description
        /// <summary>
        /// Retrieves either the
        /// System.ComponentModel.DescriptionAttribute.Description value for
        /// the member, or the member name if the attribute was not found.
        /// </summary>
        /// <param name="member">The member to locate a description for.</param>
        /// <returns>The description, or the member name if the description was not found.</returns>
        public static string GetDescription (this MemberInfo member) {
            var attribute = member.GetAttribute<DescriptionAttribute>(false);
            return attribute == null ? member.Name : attribute.Description;
        }
        #endregion
    }
}
