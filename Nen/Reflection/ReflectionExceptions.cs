using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Globalization;
using System.Text;

namespace Nen.Reflection {
    /// <summary>
    /// The exception that is thrown when a variable that is expected to exist
    /// does not.
    /// </summary>
    [Serializable]
    public class MissingVariableException : Exception {
        /// <summary>
        /// Constructs a new MissingVariableException.
        /// </summary>
        public MissingVariableException () {
        }

        /// <summary>
        /// Constructs a new MissingVariableException with the specified variable.
        /// </summary>
        /// <param name="variable">The variable to obtain the exception message from.</param>
        public MissingVariableException (VariableInfo variable)
            : base(GetMessage(variable)) {
        }

        /// <summary>
        /// Constructs a new MissingVariableException with the specified variable and inner exception.
        /// </summary>
        /// <param name="variable">The variable to obtain the exception message from.</param>
        /// <param name="inner">The inner exception.</param>
        public MissingVariableException (VariableInfo variable, Exception inner)
            : base(GetMessage(variable), inner) {
        }

        /// <summary>
        /// Constructs a new MissingVariableException with the specified message and inner exception.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public MissingVariableException (string message, Exception inner)
            : base(message, inner) {
        }

        /// <summary>
        /// Constructs a new MissingVariableException with the specified message.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public MissingVariableException (string message)
            : base(message) {
        }

        /// <summary>
        /// Constructs a new MissingVariableException with the serialized data.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The serialization context.</param>
        protected MissingVariableException (SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }

        private static string GetMessage (VariableInfo variable) {
            if (variable == null)
                throw new ArgumentNullException("variable");

            return string.Format(CultureInfo.InvariantCulture, "Could not find variable '{0}' on type '{1}'", variable.Member, variable.Member.DeclaringType);
        }
    }
}
