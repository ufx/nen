using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml;

using Nen.Collections;
using Nen.Internal;
using Nen.Runtime.Serialization;

namespace Nen.Reflection {
    /// <summary>
    /// Wrapper class for PropertyInfo and FieldInfo members that allows them
    /// to be treated as one unified type.
    /// </summary>
    [XDataContract]
    public abstract class VariableInfo {
        #region VariableInfo Loading
        /// <summary>
        /// Creates a VariableInfo from a given PropertyInfo or FieldInfo member.
        /// </summary>
        /// <param name="member">The member to create a VariableInfo for.</param>
        /// <returns>A FieldVariableInfo or PropertyVariableInfo wrapper.</returns>
        public static VariableInfo Create (MemberInfo member) {
            if (member == null)
                throw new ArgumentNullException("member");

            var field = member as FieldInfo;
            if (field != null)
                return CreateField(field);

            var property = member as PropertyInfo;
            if (property != null)
                return CreateProperty(property);

            throw new ArgumentException(LS.T("Unable to create VariableInfo for member of type '{0}'.", member.GetType()), "member");
        }

        internal static VariableInfo CreateField (FieldInfo field) {
#if NET35
            return new ReflectionFieldVariableInfo(field);
#else
            if (field.DeclaringType.IsStruct())
                return new ReflectionFieldVariableInfo(field);
            else
                return new DynamicFieldVariableInfo(field);
#endif
        }

        internal static VariableInfo CreateProperty (PropertyInfo property) {
#if NET35
            return new ReflectionPropertyVariableInfo(property);
#else
            if (property.DeclaringType.IsStruct())
                return new ReflectionPropertyVariableInfo(property);
            else
                return new DynamicPropertyVariableInfo(property);
#endif

        }
        #endregion

        #region Value Access
        /// <summary>
        /// Returns the value of the variable on the specified object.
        /// </summary>
        /// <param name="obj">The object this variable belongs to.</param>
        /// <returns>The variable value.</returns>
        public abstract object GetValue (object obj);

        /// <summary>
        /// Sets the value of the variable on the specified object.
        /// </summary>
        /// <param name="obj">The object this variable belongs to.</param>
        /// <param name="value">The new variable value.</param>
        public abstract void SetValue (object obj, object value);
        #endregion

        #region Value Access Convenience
        /// <summary>
        /// Returns the value of the variable with the specified name on the object.
        /// </summary>
        /// <param name="obj">The object the variable belongs to.</param>
        /// <param name="variableName">The name of the variable.</param>
        /// <returns>The variable value.</returns>
        public static object GetValue (object obj, string variableName) {
            if (obj == null)
                throw new ArgumentNullException("obj");

            var variable = obj.GetType().GetVariable(variableName);
            return variable.GetValue(obj);
        }

        public static object GetValue (object obj, MemberInfo member) {
            if (obj == null)
                throw new ArgumentNullException("obj");

            var variable = VariableInfo.Create(member);
            return variable.GetValue(obj);
        }

        /// <summary>
        /// Returns a collection of values for each of the specified variables on the object.
        /// </summary>
        /// <param name="obj">The object these variables belong to.</param>
        /// <param name="variables">The variables whose values must be retrieved.</param>
        /// <returns>A collection of values for each variable.</returns>
        public static IEnumerable<object> GetValues (object obj, IEnumerable<VariableInfo> variables) {
            return variables.Select(variable => variable.GetValue(obj));
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Returns an indicator on whether the variable can be read.
        /// </summary>
        public abstract bool CanRead { get; }

        /// <summary>
        /// Returns an indicator on whether the variable can be written.
        /// </summary>
        public abstract bool CanWrite { get; }

        /// <summary>
        /// Retrieves the System.ComponentModel.DescriptionAttribute.Description value for the member, or the member name if the attribute was not found.
        /// </summary>
        public string Description {
            get { return Member.GetDescription(); }
        }

        /// <summary>
        /// Retrieves the wrapped member.
        /// </summary>
        public abstract MemberInfo Member { get; }

        /// <summary>
        /// Retrieves the type of the wrapped member.
        /// </summary>
        public abstract Type MemberType { get; }

        /// <summary>
        /// Retrieves the name of the referenced member.
        /// </summary>
        public virtual string Name {
            get { return Member.Name; }
        }
        #endregion

        #region Additional Types
        public static IEnumerable<Type> GetSubTypes () {
            yield return typeof(WeaklyTypedVariableInfo);
#if NET4
            yield return typeof(DynamicFieldVariableInfo);
            yield return typeof(DynamicPropertyVariableInfo);
#endif
            yield return typeof(ReflectionFieldVariableInfo);
            yield return typeof(ReflectionPropertyVariableInfo);
            yield return typeof(CompositeVariableInfo);
            yield return typeof(PortableVariableInfo);
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Determines whether the object is a VariableInfo referencing the
        /// same member as this instance.
        /// </summary>
        /// <param name="obj">The object with a member to check.</param>
        /// <returns>true if the object is a VariableInfo referencing the same member, false if otherwise.</returns>
        public override bool Equals (object obj) {
            var variable = obj as VariableInfo;
            if (obj == null)
                return false;

            return variable.Member == Member;
        }

        /// <summary>
        /// Retrieves the referenced member hash code.
        /// </summary>
        /// <returns>The hash code of the referenced member.</returns>
        public override int GetHashCode () {
            return Member.GetHashCode();
        }

        /// <summary>
        /// Returns the Name property of this variable.
        /// </summary>
        /// <returns>The Name value.</returns>
        public override string ToString () {
            return Name;
        }
        #endregion
    }

    /// <summary>
    /// Decorates the underlying variable with weak typing, attempting to
    /// perform conversions on values given to SetValue().  GetValue()
    /// is unaffected.
    /// </summary>
    [XDataContract]
    public class WeaklyTypedVariableInfo : VariableInfo {
        #region Constructor
        /// <summary>
        /// Constructs a new WeaklyTypedVariableInfo.
        /// </summary>
        /// <param name="innerVariable">The variable to convert types for.</param>
        public WeaklyTypedVariableInfo (VariableInfo innerVariable) {
            if (innerVariable == null)
                throw new ArgumentNullException("innerVariable");

            InnerVariable = innerVariable;
        }
        #endregion

        #region VariableInfo Value Access
        /// <summary>
        /// Directly returns the value of the variable on the specified object.
        /// </summary>
        /// <param name="obj">The object this variable belongs to.</param>
        /// <returns>The direct variable value, with no conversions.</returns>
        public override object GetValue (object obj) {
            return InnerVariable.GetValue(obj);
        }

        /// <summary>
        /// Attempts to convert the given value to the underlying variable type,
        /// and sets the value on the specified object.
        /// </summary>
        /// <param name="obj">The object this variable belongs to.</param>
        /// <param name="value">The new variable value.</param>
        public override void SetValue (object obj, object value) {
            var convertedValue = ConvertEx.ChangeWeakType(value, MemberType);
            InnerVariable.SetValue(obj, convertedValue);
        }
        #endregion

        #region VariableInfo Accessors
        /// <summary>
        /// Gets the inner wrapped variable.
        /// </summary>
        [XDataMember]
        public VariableInfo InnerVariable { get; private set; }

        /// <summary>
        /// Returns an indicator on whether the variable can be read.
        /// </summary>
        public override bool CanRead {
            get { return InnerVariable.CanRead; }
        }

        /// <summary>
        /// Returns an indicator on whether the variable can be written.
        /// </summary>
        public override bool CanWrite {
            get { return InnerVariable.CanWrite; }
        }

        /// <summary>
        /// Retrieves the wrapped member.
        /// </summary>
        public override MemberInfo Member {
            get { return InnerVariable.Member; }
        }

        /// <summary>
        /// Retrieves the type of the wrapped member.
        /// </summary>
        public override Type MemberType {
            get { return InnerVariable.MemberType; }
        }

        /// <summary>
        /// Retrieves the name of the wrapped variable.
        /// </summary>
        public override string Name {
            get { return InnerVariable.Name; }
        }
        #endregion
    }

    /// <summary>
    /// Provides unified access to the endpoint of a series of variables.
    /// This is useful when addressing a reference's inner variable.
    /// </summary>
    [XDataContract]
    public class CompositeVariableInfo : VariableInfo {
        #region Constructors
        /// <summary>
        /// Constructs a new CompositeVariableInfo.
        /// </summary>
        /// <param name="components">The components that make up the composite.</param>
        public CompositeVariableInfo (VariableInfo[] components) {
            if (components == null)
                throw new ArgumentNullException("components");

            Components = components;
        }

        /// <summary>
        /// Constructs a new CompositeVariableInfo.
        /// </summary>
        /// <param name="component1">The first variable in the component.</param>
        /// <param name="component2">The second variable in the component.</param>
        public CompositeVariableInfo (VariableInfo component1, VariableInfo component2) {
            if (component1 == null)
                throw new ArgumentNullException("component1");

            if (component2 == null)
                throw new ArgumentNullException("component2");

            Components = new VariableInfo[] { component1, component2 };
        }
        #endregion

        #region VariableInfo Value Access
        /// <summary>
        /// Returns the end value of the composite.
        /// </summary>
        /// <param name="obj">The object the composite starts with.</param>
        /// <returns>The end value specified by the last variable in the composite components.</returns>
        public override object GetValue (object obj) {
            if (obj == null)
                throw new ArgumentNullException("obj");

            var instance = obj;
            foreach (var component in Components.Take(Components.Length - 1)) {
                instance = component.GetValue(instance);
                if (instance == null) {
                    if (PropogateNulls)
                        return null;
                    else
                        throw new NullReferenceException(LS.T("The inner component is null."));
                }
            }

            return EndComponent.GetValue(instance);
        }

        /// <summary>
        /// Sets the end value of the composite.
        /// </summary>
        /// <param name="obj">The object the composite starts with.</param>
        /// <param name="value">The end value specified by the last value in the composite components.</param>
        public override void SetValue (object obj, object value) {
            if (obj == null)
                throw new ArgumentNullException("obj");

            SetValueCore(obj, value, Components);
        }

        private void SetValueCore (object obj, object value, IEnumerable<VariableInfo> components) {
            var component = components.First();

            if (components.Count() == 1) {
                component.SetValue(obj, value);
                return;
            }

            var inner = component.GetValue(obj);
            if (inner == null) {
                if (InstantiateNullComponents) {
                    inner = Activator.CreateInstance(component.MemberType);
                    component.SetValue(obj, inner);
                } else
                    throw new NullReferenceException(LS.T("The inner component is null."));
            }

            SetValueCore(inner, value, components.Skip(1));

            if (component.MemberType.IsValueType)
                component.SetValue(obj, inner);
        }
        #endregion

        #region VariableInfo Accessors
        /// <summary>
        /// Returns an indicator on whether the end component can be read.
        /// </summary>
        public override bool CanRead {
            get { return EndComponent.CanRead; }
        }

        /// <summary>
        /// Returns an indicator on whether the end component can be written.
        /// </summary>
        public override bool CanWrite {
            get { return EndComponent.CanWrite; }
        }

        /// <summary>
        /// Retrieves the end component member.
        /// </summary>
        public override MemberInfo Member {
            get { return EndComponent.Member; }
        }

        /// <summary>
        /// Retrieves the type of the end component member.
        /// </summary>
        public override Type MemberType {
            get { return EndComponent.MemberType; }
        }

        /// <summary>
        /// Retrieves the name of all components, joined by a period.
        /// </summary>
        public override string Name {
            get { return string.Join(".", Components.Select(v => v.Name).ToArray()); }
        }
        #endregion

        #region Structure
        public IEnumerable<VariableInfo> Flatten () {
            foreach (var component in Components) {
                if (component is CompositeVariableInfo) {
                    foreach (var subComponent in ((CompositeVariableInfo) component).Components)
                        yield return subComponent;
                } else
                    yield return component;
            }
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Gets or sets the components that make up the composite.
        /// </summary>
        [XDataMember]
        public VariableInfo[] Components { get; private set; }

        /// <summary>
        /// Gets or sets a flag to instantiate null components.  When true and
        /// an intermediate component (not the end component) is null, it will
        /// be instantiated via its default constructor.  When false,
        /// NullReferenceException is thrown instead.  This flag only applies
        /// within SetValue.  
        /// </summary>
        [XDataMember(XDataType.Attribute)]
        public bool InstantiateNullComponents { get; set; }

        /// <summary>
        /// Gets or sets a flag to propogate null components.  When true and an
        /// intermediate component (not the end component) is null, it will be
        /// propogated as if the end component were null.  When false,
        /// NullReferenceException is thrown instead.  This flag only applies
        /// within GetValue.
        /// </summary>
        [XDataMember(XDataType.Attribute)]
        public bool PropogateNulls { get; set; }

        /// <summary>
        /// Gets the last component that makes up the composite.
        /// </summary>
        public VariableInfo EndComponent {
            get { return Components[Components.Length - 1]; }
        }
        #endregion

        #region Object Members
        /// <summary>
        /// Determines whether the object is a CompositeVariableInfo
        /// referencing all the same components as this instance.
        /// </summary>
        /// <param name="obj">The object with a composite to check.</param>
        /// <returns>true if the object is a CompositeVariableInfo referencing all the same components, false if otherwise.</returns>
        public override bool Equals (object obj) {
            var compositeVariable = obj as CompositeVariableInfo;
            if (compositeVariable == null)
                return false;

            return Components.SequenceEqual(compositeVariable.Components);
        }

        /// <summary>
        /// Retrieves the hash code of the Name property.
        /// </summary>
        /// <returns>The hash code of the Name property.</returns>
        public override int GetHashCode () {
            return Name.GetHashCode();
        }
        #endregion
    }

#if NET4
    /// <summary>
    /// Wraps a PropertyInfo in the VariableInfo interface.
    /// </summary>
    [XDataContract]
    public class DynamicPropertyVariableInfo : VariableInfo, ICustomSerializable {
        private PropertyInfo _property;
        private Func<object, object> _getValue;
        private Action<object, object> _setValue;

        #region Constructor
        // For serialization only.
        private DynamicPropertyVariableInfo () {
        }

        /// <summary>
        /// Constructs a new DynamicPropertyVariableInfo.
        /// </summary>
        /// <param name="property">The wrapped property.</param>
        public DynamicPropertyVariableInfo (PropertyInfo property) {
            if (property == null)
                throw new ArgumentNullException("property");

            _property = property;
        }
        #endregion

        #region VariableInfo Value Access
        /// <summary>
        /// Returns the value of the property on the specified object.
        /// </summary>
        /// <param name="obj">The object this property belongs to.</param>
        /// <returns>The property value.</returns>
        public override object GetValue (object obj) {
            if (_getValue == null) {
                var getMethod = _property.GetGetMethod(true);

                var instanceParameter = Expression.Parameter(typeof(object), "instance");
                var propertyMember = getMethod.IsStatic ? null : Expression.Convert(instanceParameter, _property.DeclaringType);

                var getLambda = Expression.Lambda<Func<object, object>>(Expression.Convert(Expression.Property(propertyMember, _property), typeof(object)), instanceParameter);
                _getValue = getLambda.Compile();
            }

            return _getValue(obj);
        }

        /// <summary>
        /// Sets the value of the property on the specified object.
        /// </summary>
        /// <param name="obj">The object this property belongs to.</param>
        /// <param name="value">The new property value.</param>
        public override void SetValue (object obj, object value) {
            if (_setValue == null) {
                var setMethod = _property.GetSetMethod(true);

                var valueParameter = Expression.Parameter(typeof(object), "value");
                var instanceParameter = Expression.Parameter(typeof(object), "instance");
                var propertyMember = setMethod.IsStatic ? null : Expression.Convert(instanceParameter, _property.DeclaringType);

                var setLambda = Expression.Lambda<Action<object, object>>(Expression.Assign(
                    Expression.Property(propertyMember, _property),
                    Expression.Convert(valueParameter, _property.PropertyType)),
                    instanceParameter, valueParameter);
                _setValue = setLambda.Compile();
            }

            _setValue(obj, value);
        }

        public override MemberInfo Member {
            get { return _property; }
        }
        #endregion

        #region VariableInfo Accessors
        /// <summary>
        /// Returns an indicator on whether the property can be read.
        /// </summary>
        public override bool CanRead {
            get { return _property.CanRead; }
        }

        /// <summary>
        /// Returns an indicator on whether the property can be written.
        /// </summary>
        public override bool CanWrite {
            get { return _property.CanWrite; }
        }

        /// <summary>
        /// Returns the type of the wrapped property.
        /// </summary>
        public override Type MemberType {
            get { return _property.PropertyType; }
        }
        #endregion

        #region ICustomSerializable Members
        void ICustomSerializable.ReadXml (XSerializer serializer, XmlReader reader, string containerElementName) {
            var memberFullName = reader.GetAttribute("Property");
            var memberParts = memberFullName.Split(new string[] { "::" }, StringSplitOptions.None);

            var type = Type.GetType(memberParts[0]);
            _property = type.GetProperty(memberParts[1], TypeEx.InstanceBindingFlags);
        }

        void ICustomSerializable.WriteXml (XSerializer serializer, XmlWriter writer) {
            var member = Member;
            writer.WriteAttributeString("Property", member.DeclaringType.GetPortableName() + "::" + member.Name);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Returns the wrapped property.
        /// </summary>
        public PropertyInfo Property {
            get { return _property; }
        }
        #endregion
    }

    /// <summary>
    /// Wraps a FieldInfo in the VariableInfo interface.
    /// </summary>
    [XDataContract]
    public class DynamicFieldVariableInfo : VariableInfo, ICustomSerializable {
        private FieldInfo _field;
        private Func<object, object> _getValue;
        private Action<object, object> _setValue;

        #region Constructor
        // For serialization only.
        private DynamicFieldVariableInfo () {
        }

        /// <summary>
        /// Constructs a new DynamicFieldVariableInfo.
        /// </summary>
        /// <param name="field">The wrapped field.</param>
        public DynamicFieldVariableInfo (FieldInfo field) {
            if (field == null)
                throw new ArgumentNullException("field");

            _field = field;
        }
        #endregion

        #region VariableInfo Value Access
        /// <summary>
        /// Returns the value of the field on the specified object.
        /// </summary>
        /// <param name="obj">The object this field belongs to.</param>
        /// <returns>The field value.</returns>
        public override object GetValue (object obj) {
            if (_getValue == null) {
                var instanceParameter = Expression.Parameter(typeof(object), "instance");
                var fieldMember = _field.IsStatic ? null : Expression.Convert(instanceParameter, _field.DeclaringType);

                var getLambda = Expression.Lambda<Func<object, object>>(Expression.Convert(Expression.Field(fieldMember, _field), typeof(object)), instanceParameter);
                _getValue = getLambda.Compile();
            }

            return _getValue(obj);
        }

        /// <summary>
        /// Sets the value of the field on the specified object.
        /// </summary>
        /// <param name="obj">The object this field belongs to.</param>
        /// <param name="value">The new field value.</param>
        public override void SetValue (object obj, object value) {
            if (_setValue == null) {
                var valueParameter = Expression.Parameter(typeof(object), "value");
                var instanceParameter = Expression.Parameter(typeof(object), "instance");
                var fieldMember = _field.IsStatic ? null : Expression.Convert(instanceParameter, _field.DeclaringType);

                var setLambda = Expression.Lambda<Action<object, object>>(Expression.Assign(
                    Expression.Field(fieldMember, _field),
                    Expression.Convert(valueParameter, _field.FieldType)),
                    instanceParameter, valueParameter);
                _setValue = setLambda.Compile();
            }

            _setValue(obj, value);
        }

        public override MemberInfo Member {
            get { return _field; }
        }
        #endregion

        #region VariableInfo Accessors
        /// <summary>
        /// Fields can always be read.
        /// </summary>
        public override bool CanRead {
            get { return true; }
        }

        /// <summary>
        /// Returns an indicator on whether the field can only be set in a constructor.
        /// </summary>
        public override bool CanWrite {
            get { return !_field.IsInitOnly; }
        }

        /// <summary>
        /// Returns the type of the wrapped field.
        /// </summary>
        public override Type MemberType {
            get { return _field.FieldType; }
        }
        #endregion

        #region ICustomSerializable Members
        void ICustomSerializable.ReadXml (XSerializer serializer, XmlReader reader, string containerElementName) {
            var memberFullName = reader.GetAttribute("Field");
            var memberParts = memberFullName.Split(new string[] { "::" }, StringSplitOptions.None);

            var type = Type.GetType(memberParts[0]);
            _field = type.GetField(memberParts[1], TypeEx.InstanceBindingFlags);
        }

        void ICustomSerializable.WriteXml (XSerializer serializer, XmlWriter writer) {
            var member = Member;
            writer.WriteAttributeString("Field", member.DeclaringType.GetPortableName() + "::" + member.Name);
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Returns the wrapped field.
        /// </summary>
        public FieldInfo Field {
            get { return _field; }
        }
        #endregion
    }
#endif

    /// <summary>
    /// Wraps a FieldInfo in the VariableInfo interface, accessible via reflection.
    /// </summary>
    [XDataContract]
    public class ReflectionFieldVariableInfo : VariableInfo, ICustomSerializable {
        private FieldInfo _field;

        #region Constructor
        // For serialization only.
        private ReflectionFieldVariableInfo () {
        }

        /// <summary>
        /// Constructs a new ReflectionFieldVariableInfo.
        /// </summary>
        /// <param name="field">The wrapped field.</param>
        public ReflectionFieldVariableInfo (FieldInfo field) {
            if (field == null)
                throw new ArgumentNullException("field");

            _field = field;
        }
        #endregion

        #region VariableInfo Value Access
        /// <summary>
        /// Returns the value of the field on the specified object.
        /// </summary>
        /// <param name="obj">The object this field belongs to.</param>
        /// <returns>The field value.</returns>
        public override object GetValue (object obj) {
            return _field.GetValue(obj);
        }

        /// <summary>
        /// Sets the value of the field on the specified object.
        /// </summary>
        /// <param name="obj">The object this field belongs to.</param>
        /// <param name="value">The new field value.</param>
        public override void SetValue (object obj, object value) {
            _field.SetValue(obj, value);
        }

        public override MemberInfo Member {
            get { return _field; }
        }
        #endregion

        #region VariableInfo Accessors
        /// <summary>
        /// Fields can always be read.
        /// </summary>
        public override bool CanRead {
            get { return true; }
        }

        /// <summary>
        /// Returns an indicator on whether the field can only be set in a constructor.
        /// </summary>
        public override bool CanWrite {
            get { return !_field.IsInitOnly; }
        }

        /// <summary>
        /// Returns the type of the wrapped field.
        /// </summary>
        public override Type MemberType {
            get { return _field.FieldType; }
        }
        #endregion

        #region ICustomSerializable Members
#if NET4
        void ICustomSerializable.ReadXml (XSerializer serializer, XmlReader reader, string containerElementName) {
            var memberFullName = reader.GetAttribute("Field");
            var memberParts = memberFullName.Split(new string[] { "::" }, StringSplitOptions.None);

            var type = Type.GetType(memberParts[0]);
            _field = type.GetField(memberParts[1], TypeEx.InstanceBindingFlags);
        }

        void ICustomSerializable.WriteXml (XSerializer serializer, XmlWriter writer) {
            var member = Member;
            writer.WriteAttributeString("Field", member.DeclaringType.GetPortableName() + "::" + member.Name);
        }
#endif
        #endregion

        #region Accessors
        /// <summary>
        /// Returns the wrapped field.
        /// </summary>
        public FieldInfo Field {
            get { return _field; }
        }
        #endregion
    }

    /// <summary>
    /// Wraps a PropertyInfo in the VariableInfo interface, accessible via reflection.
    /// </summary>
    [XDataContract]
    public class ReflectionPropertyVariableInfo : VariableInfo, ICustomSerializable {
        private PropertyInfo _property;

        #region Constructor
        // For serialization only.
        private ReflectionPropertyVariableInfo () {
        }

        /// <summary>
        /// Constructs a new ReflectionPropertyVariableInfo.
        /// </summary>
        /// <param name="property">The wrapped property.</param>
        public ReflectionPropertyVariableInfo (PropertyInfo property) {
            if (property == null)
                throw new ArgumentNullException("property");

            _property = property;
        }
        #endregion

        #region VariableInfo Value Access
        /// <summary>
        /// Returns the value of the property on the specified object.
        /// </summary>
        /// <param name="obj">The object this property belongs to.</param>
        /// <returns>The property value.</returns>
        public override object GetValue (object obj) {
            return _property.GetValue(obj, null);
        }

        /// <summary>
        /// Sets the value of the property on the specified object.
        /// </summary>
        /// <param name="obj">The object this property belongs to.</param>
        /// <param name="value">The new property value.</param>
        public override void SetValue (object obj, object value) {
            _property.SetValue(obj, value, null);
        }

        public override MemberInfo Member {
            get { return _property; }
        }
        #endregion

        #region VariableInfo Accessors
        /// <summary>
        /// Returns an indicator on whether the property can be read.
        /// </summary>
        public override bool CanRead {
            get { return _property.CanRead; }
        }

        /// <summary>
        /// Returns an indicator on whether the property can be written.
        /// </summary>
        public override bool CanWrite {
            get { return _property.CanWrite; }
        }

        /// <summary>
        /// Returns the type of the wrapped property.
        /// </summary>
        public override Type MemberType {
            get { return _property.PropertyType; }
        }
        #endregion

        #region ICustomSerializable Members
#if NET4
        void ICustomSerializable.ReadXml (XSerializer serializer, XmlReader reader, string containerElementName) {
            var memberFullName = reader.GetAttribute("Property");
            var memberParts = memberFullName.Split(new string[] { "::" }, StringSplitOptions.None);

            var type = Type.GetType(memberParts[0]);
            _property = type.GetProperty(memberParts[1], TypeEx.InstanceBindingFlags);
        }

        void ICustomSerializable.WriteXml (XSerializer serializer, XmlWriter writer) {
            var member = Member;
            writer.WriteAttributeString("Property", member.DeclaringType.GetPortableName() + "::" + member.Name);
        }
#endif
        #endregion

        #region Accessors
        /// <summary>
        /// Returns the wrapped property.
        /// </summary>
        public PropertyInfo Property {
            get { return _property; }
        }
        #endregion
    }

    /// <summary>
    /// A portable version of VariableInfo that defers lookup of the actual
    /// variable type information to the last minute, so that metadata such
    /// as name and variable type can be used where the variable doesn't exist.
    /// </summary>
    [XDataContract]
    public class PortableVariableInfo : VariableInfo, ICustomSerializable, ISerializationSurrogate {
        private string _name;
        private string _declaringTypeName;
        private string _memberTypeName;
        private VariableInfo _source;
        private Type _memberType;

        #region Constructors
        public PortableVariableInfo () {
        }

        public PortableVariableInfo (VariableInfo source) {
            SetSource(source);
        }
        #endregion

        #region Variable Source
        public VariableInfo GetSource () {
            if (_source == null) {
                var type = Type.GetType(_declaringTypeName);
                _source = type.GetVariable(_name);
            }

            return _source;
        }

        public void SetSource (VariableInfo source) {
            _source = source;
            _name = source.Name;
            _declaringTypeName = source.Member.DeclaringType.GetPortableName();
            _memberType = source.MemberType;
            _memberTypeName = source.MemberType.GetPortableName();
        }
        #endregion

        #region ICustomSerializable Members
#if NET4
        void ICustomSerializable.ReadXml (XSerializer serializer, XmlReader reader, string containerElementName) {
            var variableFullData = reader.GetAttribute("Variable");
            var variableParts = variableFullData.Split(new string[] { "::" }, StringSplitOptions.None);

            _declaringTypeName = variableParts[0];
            _name = variableParts[1];
            _memberTypeName = variableParts[2];
        }

        void ICustomSerializable.WriteXml (XSerializer serializer, XmlWriter writer) {
            writer.WriteAttributeString("Variable", _declaringTypeName + "::" + _name + "::" + _memberTypeName);
        }
#endif
        #endregion

        #region VariableInfo Accessors
        public override MemberInfo Member {
            get { return GetSource().Member; }
        }

        public override Type MemberType {
            get {
                if (_memberType == null)
                    _memberType = Type.GetType(_memberTypeName);

                return _memberType;
            }
        }

        public override string Name {
            get { return _name; }
        }

        public override bool CanRead {
            get { return GetSource().CanRead; }
        }

        public override bool CanWrite {
            get { return GetSource().CanWrite; }
        }

        public override object GetValue (object obj) {
            return GetSource().GetValue(obj);
        }

        public override void SetValue (object obj, object value) {
            GetSource().SetValue(obj, value);
        }
        #endregion

        #region ISerializationSurrogate Members
#if NET4
        object ISerializationSurrogate.ReadObjectData (XSerializer serializer) {
            // No need to convert.
            return this;
        }

        void ISerializationSurrogate.WriteObjectData (XSerializer serializer, object obj) {
            SetSource((VariableInfo) obj);
        }
#endif
        #endregion
    }

    public class FutureVariableInfo : VariableInfo {
        #region Constructors
        public FutureVariableInfo (VariableInfo futureVariable) {
            if (futureVariable == null)
                throw new ArgumentNullException("futureVariable");

            FutureVariable = futureVariable;
        }
        #endregion

        #region VariableInfo Value Access
        public override object GetValue (object obj) {
            var future = (IFuture) FutureVariable.GetValue(obj);
            if (future == null)
                return null;
            return future.Resolve();
        }

        public override void SetValue (object obj, object value) {
            var future = (IVariableFuture) FutureVariable.GetValue(obj);
            if (future == null)
                future = (IVariableFuture) Activator.CreateInstance(FutureVariable.MemberType);
            future.Value = value;
        }
        #endregion

        #region VariableInfo Accessors
        public VariableInfo FutureVariable { get; private set; }

        public override bool CanRead {
            get { return FutureVariable.CanRead; }
        }

        public override bool CanWrite {
            get { return FutureVariable.CanWrite && FutureVariable.MemberType is IVariableFuture; }
        }

        public override MemberInfo Member {
            get { return FutureVariable.Member; }
        }

        public override Type MemberType {
            get { return FutureVariable.MemberType; }
        }
        #endregion
    }
}
