using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Linq;
using System.IO;
using System.Text;

using Nen.Collections;

namespace Nen.Reflection {
    /// <summary>
    /// Extensions to the System.Assembly class.
    /// </summary>
    public static class AssemblyEx {
        /// <summary>
        /// Determines whether or not an assembly is provided by Microsoft.
        /// </summary>
        /// <param name="assembly">The assembly to check.</param>
        /// <returns>true if the assembly is provided by Microsoft, false if otherwise.</returns>
        public static bool IsMicrosoftAssembly (this Assembly assembly) {
            var company = assembly.GetAttribute<AssemblyCompanyAttribute>();
            return company == null || company.Company == "Microsoft Corporation";
        }

        #region Attributes
        /// <summary>
        /// Gets the custom attributes for this assembly as specified by type.
        /// </summary>
        /// <typeparam name="T">The type of attribute.</typeparam>
        /// <param name="assembly">The assembly where the attributes live.</param>
        /// <returns>The custom attributes.</returns>
        public static T[] GetAttributes<T> (this Assembly assembly) where T : Attribute {
            return assembly.GetCustomAttributes(typeof(T), false).Cast<T>().ToArray();
        }

        /// <summary>
        /// Gets a custom attribute for this assembly as specified by type.
        /// </summary>
        /// <typeparam name="T">The type of attribute.</typeparam>
        /// <param name="assembly">The assembly where the attribute lives.</param>
        /// <returns>The attribute if it exists, or null.</returns>
        public static T GetAttribute<T> (this Assembly assembly) where T : Attribute {
            return assembly.GetAttributes<T>().ElementAtOrDefault(0);
        }

        /// <summary>
        /// Gets the exported types defined in this assembly that are visible outside the assembly with the specified attribute.
        /// </summary>
        /// <typeparam name="T">The type of attribute to search for.</typeparam>
        /// <param name="assembly">The assembly where the types live.</param>
        /// <param name="inherit">Specifies whether to search the type's inheritence chain to find the attributes.</param>
        /// <returns>The exported types in the assembly that are marked with the specified attribute.</returns>
        public static IEnumerable<Type> GetExportedTypesWithAttribute<T> (this Assembly assembly, bool inherit) where T : Attribute {
            return assembly.GetExportedTypes().Where(type => type.IsDefined(typeof(T), inherit));
        }
        #endregion

        #region Resources
        public static string ReadManifestResource (this Assembly assembly, string manifestResourceName) {
            using (var stream = assembly.GetManifestResourceStream(manifestResourceName)) {
                using (var reader = new StreamReader(stream))
                    return reader.ReadToEnd();
            }
        }
        #endregion
    }
}
