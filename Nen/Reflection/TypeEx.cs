using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Data;
using System.Runtime.CompilerServices;
using System.Collections.Concurrent;

namespace Nen.Reflection {
    /// <summary>
    /// Extensions to the Type class.
    /// </summary>
    public static class TypeEx {
        /// <summary>
        /// Convenience aggregate of Instance, NonPublic, and Public BindingFlags.
        /// </summary>
        public static readonly BindingFlags InstanceBindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

        /// <summary>
        /// Convenience aggregate of Static, NonPublic, and Public BindingFlags.
        /// </summary>
        public static readonly BindingFlags StaticBindingFlags = BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public;

        /// <summary>
        /// Convenience aggregate of Instance, NonPublic, Public, and DeclaredOnly BindingFlags.
        /// </summary>
        public static readonly BindingFlags DeclaredInstanceBindingFlags = InstanceBindingFlags | BindingFlags.DeclaredOnly;

        /// <summary>
        /// Convenience aggregate of Field and Property MemberTypes.
        /// </summary>
        public static readonly MemberTypes AllVariableMemberTypes = MemberTypes.Field | MemberTypes.Property;

        #region Members
        /// <summary>
        /// Finds all members within a type marked with the specified attribute.
        /// </summary>
        /// <typeparam name="TAttribute">The attribute to search for.</typeparam>
        /// <param name="type">The type to search within.</param>
        /// <param name="memberType">The type of member to retrieve.</param>
        /// <param name="bindingAttr">The member binding criteria.</param>
        /// <param name="inherit">Specifies whether to search the member's inheritance chain to find the attribute.</param>
        /// <returns>A filtered list of members with the specified criteria.</returns>
        public static MemberInfo[] FindMembersWithAttribute<TAttribute> (this Type type, MemberTypes memberType, BindingFlags bindingAttr, bool inherit)
            where TAttribute : Attribute {
            return type.FindMembers(memberType, bindingAttr, (m, x) => m.IsDefined(typeof(TAttribute), inherit), null);
        }
        #endregion

        #region Nullables
        /// <summary>
        /// Determines if the specified type is a Nullable type.
        /// </summary>
        /// <param name="type">The type to check.</param>
        /// <returns>true if the type is an instance of Nullable, false if otherwise.</returns>
        public static bool IsNullable (this Type type) {
            return type.IsInstanceOfGenericType(typeof(Nullable<>), false);
        }

        /// <summary>
        /// Unwraps a nullable type into its underlying type iff the type is
        /// a Nullable.
        /// </summary>
        /// <param name="type">The type to unwrap.</param>
        /// <returns>The unwrapped type, or the original type if it wasn't a Nullable.</returns>
        public static Type UnwrapIfNullable (this Type type) {
            return IsNullable(type) ? Nullable.GetUnderlyingType(type) : type;
        }
        #endregion

        #region Generics
        /// <summary>
        /// Determines if the specified type is an instance of a generic type.
        /// </summary>
        /// <example>typeof(Collection&lt;int&gt;).IsInstanceOfGenericType(Collection&lt;&gt;) -> true</example>
        /// <param name="type">The type to check.</param>
        /// <param name="genericType">The generic type to check the type against.</param>
        /// <returns>true if the type or its base types is an instance of the generic type, false if otherwise.</returns>
        public static bool IsInstanceOfGenericType (this Type type, Type genericType) {
            return IsInstanceOfGenericType(type, genericType, true);
        }

        /// <summary>
        /// Determines if the specified type is an instance of a generic type.
        /// </summary>
        /// <example>typeof(Collection&lt;int&gt;).IsInstanceOfGenericType(Collection&lt;&gt;) -> true</example>
        /// <param name="type">The type to check.</param>
        /// <param name="genericType">The generic type to check the type against.</param>
        /// <param name="inherit">If true, base types are also checked.</param>
        /// <returns>true if the type is an instance of the generic type, false if otherwise.</returns>
        public static bool IsInstanceOfGenericType (this Type type, Type genericType, bool inherit) {
            if (type == null)
                throw new ArgumentNullException("type");

            if (type.IsGenericType && type.GetGenericTypeDefinition() == genericType)
                return true;

            if (inherit) {
                foreach (var iface in type.GetInterfaces()) {
                    if (iface.IsInstanceOfGenericType(genericType, true))
                        return true;
                }

                if (type.BaseType != null)
                    return type.BaseType.IsInstanceOfGenericType(genericType, true);

            }
            return false;
        }

        /// <summary>
        /// Retrieves the type that is the instance of the generic type in an
        /// inherited type hierarchy.
        /// </summary>
        /// <param name="type">The type to retrieve a generic type instance for.</param>
        /// <param name="genericType">The generic type to retrieve an instance of.</param>
        /// <returns>The type in this hierarchy that is an instance of the generic type.</returns>
        /// <example>typeof(Collection&lt;int&gt;).GetGenericTypeInstance(typeof(Collection&lt;&gt;)) -> typeof(Collection&lt;int&gt;)</example>
        public static Type GetGenericTypeInstance (this Type type, Type genericType) {
            if (type == null)
                throw new ArgumentNullException("type");

            foreach (var iface in type.GetInterfaces()) {
                if (iface.IsInstanceOfGenericType(genericType, false))
                    return iface;
            }

            if (type.IsInstanceOfGenericType(genericType, false))
                return type;

            if (type.BaseType != null)
                return type.BaseType.GetGenericTypeInstance(genericType);

            return null;
        }
        #endregion

        #region Iteration
        /// <summary>
        /// Retrieves all base types in the hierarchy of this type.
        /// </summary>
        /// <param name="type">The type to retrieve base types for.</param>
        /// <returns>An enumerable of base types in the hierarchy of this type.</returns>
        public static IEnumerable<Type> GetBaseTypes (this Type type) {
            if (type == null)
                throw new ArgumentNullException("type");

            for (var current = type.BaseType; current != null; current = current.BaseType)
                yield return current;
        }

        /// <summary>
        /// Retrieves all all types in the hierarchy of this type.
        /// </summary>
        /// <param name="type">The type to retrieve a hierarchy for.</param>
        /// <returns>An enumerable of types in the hierarchy of this type.</returns>
        public static IEnumerable<Type> GetHierarchy (this Type type) {
            while (type != null) {
                yield return type;
                type = type.BaseType;
            }
        }
        #endregion

        #region Queries
        /// <summary>
        /// Determines if the instance is the default value of the type.  The
        /// default value for classes is null.
        /// </summary>
        /// <param name="type">The type to check a default vaule for.</param>
        /// <param name="instance">The instance that is possibly the default value.</param>
        /// <returns>true if the instance equals the default value, false if otherwise.</returns>
        public static bool IsDefaultValue (this Type type, object instance) {
            if (type == null)
                throw new ArgumentNullException("type");

            if (type.IsValueType) {
                var defaultInstance = Activator.CreateInstance(type);
                return defaultInstance.Equals(instance);
            }

            return instance == null;
        }


        /// <summary>
        /// Determines if the specified type is a "simple" type, and is usually
        /// regarded with primitive semantics.
        /// </summary>
        /// <param name="type">The type to check.</param>
        /// <returns>true if the type is a string or array, false if the type is another class, and true for all other cases.</returns>
        public static bool IsSimple (this Type type) {
            if (type == null)
                throw new ArgumentNullException("type");

            var finalType = type.UnwrapIfNullable();

            // These types are treated as simple.
            if (finalType.IsEnum || finalType.IsArray)
                return true;

            // Some basic structs and classes are typically treated like simple values.
            if (finalType == typeof(string) || finalType == typeof(Guid) || finalType == typeof(decimal) || finalType == typeof(DateTime) || finalType == typeof(TimeSpan))
                return true;

            // Classes and structs represent some complexity.
            if (finalType.IsClass || finalType.IsStruct())
                return false;

            // All others are assumed simple.
            return true;
        }


        /// <summary>
        /// Determines if the specified type is non-primitive structure type.
        /// </summary>
        /// <param name="type">The type to check.</param>
        /// <returns>true if the type is  a non-primitive structure, false if otherwise.</returns>
        public static bool IsStruct (this Type type) {
            if (type == null)
                throw new ArgumentNullException("type");

            return type.IsValueType && !type.IsPrimitive;
        }

        /// <summary>
        /// Determines if the specified type is at the top of an object
        /// hierarchy.
        /// </summary>
        /// <param name="type">The type to check.</param>
        /// <returns>true if the type's base is object or ValueType, false if otherwise.</returns>
        public static bool IsTopOfHierarchy (this Type type) {
            if (type == null)
                throw new ArgumentNullException("type");

            return type.BaseType == typeof(object) || type.BaseType == typeof(ValueType);
        }

        /// <summary>
        /// Determiens whether the type is anonymous. This code is dangerous
        /// and not guaranteed to work across .Net compiler versions.
        /// </summary>
        /// <param name="type">The type to check for anonymity.</param>
        /// <returns>true if the type is anonymous, false if otherwise.</returns>
        public static bool IsAnonymous (this Type type) {
            return Attribute.IsDefined(type, typeof(CompilerGeneratedAttribute), false)
                   && type.IsGenericType && type.Name.Contains("AnonymousType")
                   && (type.Name.StartsWith("<>") || type.Name.StartsWith("VB$"))
                   && (type.Attributes & TypeAttributes.NotPublic) == TypeAttributes.NotPublic;
        }
        #endregion

        #region Variables
        private static ConcurrentDictionary<Tuple<Type, string, BindingFlags>, VariableInfo> _typeVariables = new ConcurrentDictionary<Tuple<Type, string, BindingFlags>, VariableInfo>();

        /// <summary>
        /// Retrieves the instance VariableInfo with the given name.
        /// </summary>
        /// <param name="type">The type where the variable lives.</param>
        /// <param name="variableName">The name of the variable.</param>
        /// <returns>The instance FieldVariableInfo or PropertyVariableInfo that matches the criteria.</returns>
        public static VariableInfo GetVariable (this Type type, string variableName) {
            return type.GetVariable(variableName, InstanceBindingFlags);
        }

        /// <summary>
        /// Retrieves the VariableInfo with the specified criteria.
        /// </summary>
        /// <param name="type">The type where the variable lives.</param>
        /// <param name="variableName">The name of the variable.</param>
        /// <param name="bindingAttr">The variable binding criteria.</param>
        /// <returns>The FieldVariableInfo or PropertyVariableInfo that matches the criteria.</returns>
        public static VariableInfo GetVariable (this Type type, string variableName, BindingFlags bindingAttr) {
            return GetVariable(type, variableName, bindingAttr, true);
        }

        /// <summary>
        /// Retrieves the VariableInfo with the specified criteria.
        /// </summary>
        /// <param name="type">The type where the variable lives.</param>
        /// <param name="variableName">The name of the variable.</param>
        /// <param name="throwOnError">true if the method should throw MissingMemberException when the variable can't be found, false if the method should return null.</param>
        /// <param name="bindingAttr">The variable binding criteria.</param>
        /// <returns>The FieldVariableInfo or PropertyVariableInfo that matches the criteria.</returns>
        public static VariableInfo GetVariable (this Type type, string variableName, BindingFlags bindingAttr, bool throwOnError) {
            if (type == null)
                throw new ArgumentNullException("type");

            VariableInfo result = null;

            var variableKey = Tuple.Create(type, variableName, bindingAttr);
            if (_typeVariables.TryGetValue(variableKey, out result))
            {
                if (result == null && throwOnError)
                    throw new MissingMemberException(type.Name, variableName);
                return result;
            }

            var property = type.GetProperty(variableName, bindingAttr);
            if (property != null)
                result = VariableInfo.CreateProperty(property);
            else
            {
                var field = type.GetField(variableName, bindingAttr);
                if (field != null)
                    result = VariableInfo.CreateField(field);
            }

            if (result == null && throwOnError)
                throw new MissingMemberException(type.Name, variableName);

            _typeVariables[variableKey] = result;
            return result;
        }

        /// <summary>
        /// Retrieves the CompositeVariableInfo with the given name path.
        /// </summary>
        /// <param name="type">The type where the composite begins.</param>
        /// <param name="variableNames">The name path of the composite components.</param>
        /// <returns>A composite with the given name path.</returns>
        public static CompositeVariableInfo GetVariable (this Type type, string[] variableNames) {
            return type.GetVariable(variableNames, InstanceBindingFlags);
        }

        /// <summary>
        /// Retrieves the CompositeVariableInfo with the specified criteria.
        /// </summary>
        /// <param name="type">The type where the composite begins.</param>
        /// <param name="variableNames">The name path of the composite components.</param>
        /// <param name="bindingAttr">The variable binding criteria.</param>
        /// <returns>A composite that matches the criteria.</returns>
        public static CompositeVariableInfo GetVariable (this Type type, string[] variableNames, BindingFlags bindingAttr) {
            var components = new List<VariableInfo>(variableNames.Length);

            var currentType = type;
            foreach (var variableName in variableNames) {
                var variable = currentType.GetVariable(variableName, bindingAttr);
                currentType = variable.MemberType;
                components.Add(variable);
            }

            return new CompositeVariableInfo(components.ToArray());
        }

        /// <summary>
        /// Retrieves all instance variables on the given type.
        /// </summary>
        /// <param name="type">The type where the variables live.</param>
        /// <returns>A collection of instance variables.</returns>
        public static IEnumerable<VariableInfo> GetVariables (this Type type) {
            return type.GetVariables(InstanceBindingFlags, AllVariableMemberTypes);
        }

        /// <summary>
        /// Retrieves all variables with the specified criteria.
        /// </summary>
        /// <param name="type">The type where the variables live.</param>
        /// <param name="bindingAttr">The variable binding criteria.</param>
        /// <param name="memberTypes">The type of variable to retrieve.</param>
        /// <returns>A collection of variables.</returns>
        public static IEnumerable<VariableInfo> GetVariables (this Type type, BindingFlags bindingAttr, MemberTypes memberTypes) {
            if (type == null)
                throw new ArgumentNullException("type");

            foreach (var member in type.GetMembers(bindingAttr)) {
                if (((memberTypes & MemberTypes.Property) == MemberTypes.Property) && member is PropertyInfo)
                    yield return VariableInfo.CreateProperty((PropertyInfo) member);
                else if (((memberTypes & MemberTypes.Field) == MemberTypes.Field) && member is FieldInfo)
                    yield return VariableInfo.CreateField((FieldInfo) member);
            }
        }
        #endregion

        #region Names
        public static string GetPortableName (this Type type) {
            return type.FullName + "," + type.Assembly.GetName().Name;
        }
        #endregion
    }
}
