﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;

namespace Nen.DirectoryServices {
    /// <summary>
    /// Extensions to the System.DirectoryServices.SearchResult class.
    /// </summary>
    public static class SearchResultEx {
        /// <summary>
        /// Reads the first property in the collection with the given name.
        /// </summary>
        /// <param name="result">The search result containing properties to read.</param>
        /// <param name="propertyName">The name of the collection of properties to read.</param>
        /// <returns>The first value of the property collection, or null.</returns>
        public static object ReadScalar (this SearchResult result, string propertyName) {
            var properties = result.Properties[propertyName];
            if (properties != null && properties.Count > 0)
                return properties[0];

            return null;
        }
    }
}
