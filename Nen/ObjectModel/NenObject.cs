﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.ObjectModel {
    [Serializable]
    public class NenObject {
        public NenClass Class { get; private set; }
        private Dictionary<NenProperty, object> _properties;
        private Dictionary<NenProperty, EventHandler<NenObjectPropertyChangedEventArgs>> _listeners;

        public NenObject () {
            Class = NenClass.GetClass(GetType());
        }

        public virtual NenObject Clone () {
            NenObject clone = (NenObject)MemberwiseClone();

            if (_properties != null)
                clone._properties = new Dictionary<NenProperty, object>(_properties);
            clone._listeners = null;
            return clone;
        }

        private void EnsureProperties () {
            if (_properties == null)
                _properties = new Dictionary<NenProperty, object>();
        }

        private void EnsureListeners () {
            if (_listeners == null)
                _listeners = new Dictionary<NenProperty, EventHandler<NenObjectPropertyChangedEventArgs>>();
        }

        public IEnumerable<KeyValuePair<NenProperty, object>> Properties {
            get {
                EnsureProperties();
                return _properties;
            }
        }

        public object GetRawValue (NenProperty property) {
            EnsureProperties();

            object rv;
            if (_properties.TryGetValue(property, out rv))
                return rv;

            return property.DefaultValue;
        }

        public object GetValue (NenProperty property) {
            object rv = GetRawValue(property);

            if (rv is NenBinding)
                rv = ((NenBinding)rv).GetValue(this);

            return rv;
        }

        public void SetValue (NenProperty property, object value) {
            EnsureProperties();
            _properties[property] = value;
            NotifyListeners(property, NenObjectChangeType.PropertySet, value);
        }

        public void UnsetValue (NenProperty property, object value) {
            if (_properties == null)
                return;

            if (_properties.Remove(property))
                NotifyListeners(property, NenObjectChangeType.PropertyUnset, null);
        }

        public void RegisterChangeListener (NenProperty property, EventHandler<NenObjectPropertyChangedEventArgs> handler) {
            EnsureListeners();

            EventHandler<NenObjectPropertyChangedEventArgs> existingHandler;

            if (_listeners.TryGetValue(property, out existingHandler))
                existingHandler += handler;
            else
                existingHandler = handler;

            _listeners[property] = existingHandler;
        }

        public void UnregisterChangeListener (NenProperty property, EventHandler<NenObjectPropertyChangedEventArgs> handler) {
            if (_listeners == null)
                return;

            EventHandler<NenObjectPropertyChangedEventArgs> existingHandler;

            if (_listeners.TryGetValue(property, out existingHandler))
                _listeners[property] = existingHandler - handler;
        }

        private void NotifyListeners (NenProperty property, NenObjectChangeType changeType, object newValue) {
            if (_listeners == null)
                return;

            EventHandler<NenObjectPropertyChangedEventArgs> handler;

            if (_listeners.TryGetValue(property, out handler)) {
                handler(this, new NenObjectPropertyChangedEventArgs() { Property = property, ChangeType = changeType, NewValue = newValue });
            }
        }
    }

    public enum NenObjectChangeType {
        PropertySet, PropertyUnset
    }

    public class NenObjectPropertyChangedEventArgs : EventArgs {
        public NenProperty Property { get; internal set; }
        public object NewValue { get; internal set; }
        public NenObjectChangeType ChangeType { get; internal set; }
    }
}
