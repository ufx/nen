﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nen;
using System.Runtime.Serialization;

namespace Nen.ObjectModel {
    [Serializable]
    public class NenProperty: IObjectReference {
        public string Name { get; private set; }
        public object DefaultValue { get; private set; }
        public NenClass DeclaringClass { get; private set; }
        public Type Type { get; private set; }

        protected NenProperty (string name, object defaultValue, NenClass declaringClass, Type type) {
            Name = name;
            DefaultValue = defaultValue;
            DeclaringClass = declaringClass;
            Type = type;
        }

        public static NenProperty Register (string propertyName, Type declaringType, Type valueType) {
            return Register(propertyName, declaringType, valueType, null);
        }

        public static NenProperty Register (string propertyName, Type declaringType, Type valueType, object defaultValue) {
            return Register(propertyName, NenClass.GetClass(declaringType), valueType, defaultValue);
        }

        public static NenProperty Register (string propertyName, NenClass declaringClass, Type valueType, object defaultValue) {
            var property = ActivatorEx.CreateGenericInstance(typeof(NenProperty<>), valueType.Enlist().ToArray(),
                propertyName, defaultValue, declaringClass, valueType
            ) as NenProperty;

            declaringClass.RegisterProperty(property);
            return property;
        }

        #region IObjectReference Members

        public object GetRealObject (StreamingContext context) {
            NenProperty property;

            if (DeclaringClass.DeclaredProperties.TryGetValue(Name, out property))
                return property;

            return this;
        }

        #endregion
    }

    [Serializable]
    public class NenProperty<T> : NenProperty {
        protected NenProperty (string name, object defaultValue, NenClass declaringClass, Type type): 
            base(name, defaultValue, declaringClass, type) { }
    }
}
