﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.ObjectModel {
    [Serializable]
    public abstract class NenBinding {
        public abstract object GetValue (NenObject bindingContext);

        public abstract void SetValue (NenObject bindingContext, object value);

        public virtual bool CanSetValue (NenObject bindingContext) {
            return false;
        }
    }
}
