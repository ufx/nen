﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen {
    /// <summary>
    /// Extensions to the System.String class.
    /// </summary>
    public static class StringEx {
        /// <summary>
        /// Separates the left string from the right string with the given
        /// separator value, unless the left value is null or empty.
        /// </summary>
        /// <param name="left">The potentially missing left side of the separation.</param>
        /// <param name="separator">The value to insert between an existing left and right string.</param>
        /// <param name="right">The right side of the separation.</param>
        /// <returns>When the left value is not null or empty, the separated value.  Otherwise, the right value.</returns>
        public static string Separate (this string left, string separator, string right) {
            if (string.IsNullOrEmpty(left))
                return right;

            if (string.IsNullOrEmpty(right))
                return left;

            return left + separator + right;
        }

        /// <summary>
        /// Convert any IEnumerable&lt;char&gt; to its string representation.
        /// </summary>
        /// <param name="enumValue">An IEnumerable&lt;char&gt;</param>
        /// <returns>A string.</returns>
        public static string AsString (this IEnumerable<char> enumValue) {
            return new string(enumValue.ToArray());
        }

        public static string AfterFirst (this string str, string value) {
            if (string.IsNullOrEmpty(str))
                return str;

            var index = str.IndexOf(value);
            if (index == -1)
                return str;

            return str.Substring(index + value.Length, str.Length - index - value.Length);
        }

        public static string AfterLast(this string str, string value)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            var index = str.LastIndexOf(value);
            if (index == -1)
                return str;

            return str.Substring(index + value.Length, str.Length - index - value.Length);
        }

        public static string BeforeFirst (this string str, string value) {
            if (string.IsNullOrEmpty(str))
                return str;

            var index = str.IndexOf(value);
            if (index == -1)
                return str;

            return str.Substring(0, index);
        }

        public static string BetweenFirst (this string str, string value1, string value2) {
            if (string.IsNullOrEmpty(str))
                return str;

            var firstIndex = str.IndexOf(value1);
            var lastIndex = str.IndexOf(value2, firstIndex + 1);
            if (lastIndex == -1)
                lastIndex = str.Length;

            return str.Substring(firstIndex + 1, lastIndex - firstIndex - 1);
        }

        public static string ToProperCase (this string str) {
            if (string.IsNullOrEmpty(str) || char.IsUpper(str[0]))
                return str;

            return char.ToUpper(str[0]) + str.Substring(1);
        }

        public static string TrimEnd(this string str, string snip)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(snip))
                return str;

            while (str.EndsWith(snip))
                str = str.Substring(0, str.Length - snip.Length);
            return str;
        }

        // Not an extension method because its name is annoyingly long.
        public static string ToUpperWithUnderscores(string str)
        {
            return ChangeCaseWithUnderscoresCore(str, char.ToUpper);
        }

        // Not an extension method because its name is annoyingly long.
        public static string ToLowerWithUnderscores (string str)
        {
            return ChangeCaseWithUnderscoresCore(str, char.ToLower);
        }

        private static string ChangeCaseWithUnderscoresCore (string str, Func<char, char> change)
        {
            if (str == null)
                throw new ArgumentNullException("str");

            var separatorFound = false;
            var sb = new StringBuilder();

            for (var i = 0; i < str.Length; i++)
            {
                var c = str[i];

                // Short-circuit separator underscores.
                if (c == '_')
                {
                    separatorFound = false;
                    sb.Append(c);
                    continue;
                }

                // Append an underscore if this is an uppercase character between lowercase characters.
                if (char.IsUpper(c))
                {
                    if (separatorFound)
                    {
                        sb.Append('_');
                        separatorFound = false;
                    }
                }
                else if (char.IsLower(c))
                    separatorFound = true;

                sb.Append(change(c));
            }

            return sb.ToString();
        }
    }
}
