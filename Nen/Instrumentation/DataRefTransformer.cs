﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;
using System.Diagnostics;
using Mono.Cecil.Cil;
using SR = System.Reflection;

namespace Nen.Data.ObjectRelationalMapper {
    [AttributeUsage(AttributeTargets.Property)]
    public class AutoRefAttribute : Attribute {
    }
}

namespace Nen.Instrumentation {
	public class DataRefTransformer {
		public SR.StrongNameKeyPair SigningKey { get; set; }

		public void Discombobulate (string path) {
			var module = ModuleDefinition.ReadModule(path, new ReaderParameters { ReadSymbols = true });

			var nen = module.AssemblyReferences.FirstOrDefault(asmRef => asmRef.FullName == typeof(DataRefTransformer).Assembly.FullName);
			
			var dataRefType = new TypeReference("Nen.Data.ObjectRelationalMapper", "DataRef`1", module, nen);
			dataRefType.GenericParameters.Add(new GenericParameter(dataRefType));

			foreach (var type in module.Types) {
				if (!type.HasCustomAttributes || !type.HasProperties)
					continue;

				var attr = type.CustomAttributes.FirstOrDefault(a => a.Constructor.DeclaringType.FullName == "Nen.Data.ObjectRelationalMapper.PersistentAttribute");

				if (attr == null)
					continue;

				List<FieldDefinition> backingFields = new List<FieldDefinition>();

				if (type.Methods.Any(m => m.Name == "__init_DataRef"))
					continue; // don't re-instrument

				foreach (var property in type.Properties) {
                    var pAttr = property.CustomAttributes.FirstOrDefault(a => a.Constructor.DeclaringType.FullName == "Nen.Data.ObjectRelationalMapper.AutoRefAttribute");

					if (pAttr == null)
						continue;

					FieldDefinition field = null;

					foreach (var insn in property.GetMethod.Body.Instructions) {
						if (insn.OpCode.Code == Code.Ldfld) {
							field = insn.Operand as FieldDefinition;
							break;
						}
					}

					if (field == null)
						continue;

					type.Fields.Remove(field);

					var genericInstance = new GenericInstanceType(dataRefType);
					genericInstance.GenericArguments.Add(property.PropertyType);

					field = new FieldDefinition("<" + property.Name + ">__dataRef", FieldAttributes.Private, genericInstance);
					type.Fields.Add(field);
					backingFields.Add(field);

					var get_Value = new MethodReference("get_Value", dataRefType.GenericParameters[0], genericInstance);
					get_Value.HasThis = true;
					var set_Value = new MethodReference("set_Value", module.TypeSystem.Void, genericInstance);
					set_Value.HasThis = true;
					set_Value.Parameters.Add(new ParameterDefinition(dataRefType.GenericParameters[0]));


					var getBody = property.GetMethod.Body;
					var setBody = property.SetMethod.Body;

					getBody.Instructions.Clear();
					getBody.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
					getBody.Instructions.Add(Instruction.Create(OpCodes.Ldfld, field));
					getBody.Instructions.Add(Instruction.Create(OpCodes.Callvirt, get_Value));
					getBody.Instructions.Add(Instruction.Create(OpCodes.Ret));

					setBody.Instructions.Clear();
					setBody.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
					setBody.Instructions.Add(Instruction.Create(OpCodes.Ldfld, field));
					setBody.Instructions.Add(Instruction.Create(OpCodes.Ldarg_1));
					setBody.Instructions.Add(Instruction.Create(OpCodes.Callvirt, set_Value));
					setBody.Instructions.Add(Instruction.Create(OpCodes.Ret));

					var debuggerBrowsableAttribute = new TypeReference("System.Diagnostics", "DebuggerBrowsableAttribute", module, module.TypeSystem.Corlib);
					var debuggerBrowsableState = new TypeReference("System.Diagnostics", "DebuggerBrowsableState", module, module.TypeSystem.Corlib, true);
					var debuggerBrowsableAttributeCtor = new MethodReference(".ctor", module.TypeSystem.Void, debuggerBrowsableAttribute) { HasThis = true };
					debuggerBrowsableAttributeCtor.Parameters.Add(new ParameterDefinition(debuggerBrowsableState));

					var dbAttr = new CustomAttribute(debuggerBrowsableAttributeCtor);
					dbAttr.ConstructorArguments.Add(new CustomAttributeArgument(module.TypeSystem.Int32, 0));
					property.CustomAttributes.Add(dbAttr);
				}

				if (backingFields.Count > 0) {
					var method = new MethodDefinition("__init_DataRef", MethodAttributes.Private | MethodAttributes.HideBySig, module.TypeSystem.Void);
					method.Body.MaxStackSize = 8;

					var il = method.Body.GetILProcessor();

					foreach (var field in backingFields) {
						var ctor = new MethodReference(".ctor", module.TypeSystem.Void, field.FieldType) { HasThis = true };

						il.Emit(OpCodes.Ldarg_0);
						il.Emit(OpCodes.Newobj, ctor);
						il.Emit(OpCodes.Stfld, field);
					}

					il.Emit(OpCodes.Ret);
					type.Methods.Add(method);

					foreach (var existingCtor in type.Methods.Where(m => m.IsConstructor)) {
						var eil = existingCtor.Body.GetILProcessor();

						var load = eil.Create(OpCodes.Ldarg_0);
						var call = eil.Create(OpCodes.Callvirt, method);
						eil.InsertBefore(eil.Body.Instructions.First(), load);
						eil.InsertAfter(load, call);
					}
				}
			}

			module.Write(path, new WriterParameters { WriteSymbols = true, StrongNameKeyPair = SigningKey });
		}
	}
}
