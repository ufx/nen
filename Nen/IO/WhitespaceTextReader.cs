﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Nen.IO {
    [Flags]
    public enum WhitespaceTextAttributes {
        TrimBeginWhitespace = 1,
        TrimEndWhitespace = 2,
        IgnoreExtraLineEndings = 4,

        All = TrimBeginWhitespace | TrimEndWhitespace | IgnoreExtraLineEndings
    }

    public class WhitespaceTextReader : TextReader {
        private WhitespaceTextAttributes _attributes;
        private TextReader _reader;
        private Queue<int> _characters = new Queue<int>();
        private char _lastCharacter;
        private bool _atBeginningOfText = true;

        #region Constructors
        public WhitespaceTextReader (TextReader reader)
            : this(reader, WhitespaceTextAttributes.All) {
        }

        public WhitespaceTextReader (TextReader reader, WhitespaceTextAttributes attributes) {
            _reader = reader;
            _attributes = attributes;
        }
        #endregion

        #region TextReader
        public override void Close () {
            _reader.Close();
        }

        protected override void Dispose (bool disposing) {
            if (disposing) {
                _reader.Dispose();
                _reader = null;
            }

            base.Dispose(disposing);
        }

        public override int Peek () {
            if (_characters.Count == 0)
                QueueNextCharacters();

            return _characters.Peek();
        }

        public override int Read () {
            if (_characters.Count == 0)
                QueueNextCharacters();

            return _characters.Dequeue();
        }

        private void QueueNextCharacters () {
            while (true) {
                // Fall through the end of the text.
                var value = _reader.Read();
                if (value == -1) {
                    if ((_attributes & WhitespaceTextAttributes.TrimEndWhitespace) == WhitespaceTextAttributes.TrimEndWhitespace)
                        _characters.Clear();

                    _characters.Enqueue(-1);
                    return;
                }

                var character = (char) value;

                if (char.IsWhiteSpace(character)) {
                    if (_atBeginningOfText &&
                        (_attributes & WhitespaceTextAttributes.TrimBeginWhitespace) == WhitespaceTextAttributes.TrimBeginWhitespace)
                        continue;

                    if ((_attributes & WhitespaceTextAttributes.IgnoreExtraLineEndings) == WhitespaceTextAttributes.IgnoreExtraLineEndings
                        && ((_lastCharacter == '\r' || _lastCharacter == '\n') && (character == '\r' || character == '\n')))
                        continue;

                    _lastCharacter = character;
                    _characters.Enqueue(character);
                } else {
                    _atBeginningOfText = false;
                    _lastCharacter = character;
                    _characters.Enqueue(character);
                    return;
                }
            }
        }
        #endregion
    }
}
