﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.IO {
    public static class Csv {
        public static string FormatValue (object o) {
            if (o == null || o == DBNull.Value)
                return "(null)";

            var str = o.ToString();
            if (str.Contains(","))
                return "\"" + str.Replace("\"", "\"\"") + "\"";

            return str;
        }
    }
}
