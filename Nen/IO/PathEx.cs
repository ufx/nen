﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Nen.IO {
    public static class PathEx {
        private static char[] _separators = new char[] { '\\', '/' };

        public static string UpOneLevel (string path) {
            var trimmedPath = path.TrimEnd(_separators);
            var itemName = Path.GetFileName(trimmedPath);
            var result = path.Substring(0, trimmedPath.Length - itemName.Length).TrimEnd(_separators);

            return result == "" ? null : result;
        }

        public static string RelativeTo (string path, string relativeRoot) {
            var fullPath = Path.GetFullPath(path);
            var fullRelativeRoot = Path.GetFullPath(relativeRoot);

            if (!fullPath.StartsWith(fullRelativeRoot) || fullRelativeRoot.Length > fullPath.Length)
                return null;

            return fullPath.Substring(fullRelativeRoot.Length, fullPath.Length - fullRelativeRoot.Length).TrimStart(_separators);
        }

        public static string TopLevelDirectory (string path) {
            var trimmedPath = path.TrimStart(_separators);
            var separatorIndex = trimmedPath.IndexOfAny(_separators);
            if (separatorIndex == -1)
                return trimmedPath;

            return trimmedPath.Substring(0, separatorIndex);
        }
    }
}
