﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Nen.IO.Compression {
    public static class GZip {
        public static void CompressFile (string sourceFileName, string destinationFileName) {
            var buffer = new byte[1024 * 32];
            var sourceFile = new FileInfo(sourceFileName);

            using (var read = sourceFile.OpenRead())
            using (var write = File.Create(destinationFileName))
            using (var compressedWrite = new GZipStream(write, CompressionMode.Compress))
                BufferedCopy(read, compressedWrite);
        }

        public static void DecompressFile (string sourceFileName, string destinationFileName) {
            var sourceFile = new FileInfo(sourceFileName);

            using (var read = sourceFile.OpenRead())
            using (var decompressedRead = new GZipStream(read, CompressionMode.Decompress))
            using (var write = File.Create(destinationFileName))
                BufferedCopy(decompressedRead, write);
        }

        public static void DecompressFile (Stream sourceStream, string destinationFileName) {
            using (var decompressedRead = new GZipStream(sourceStream, CompressionMode.Decompress))
            using (var write = File.Create(destinationFileName))
                BufferedCopy(decompressedRead, write);
        }

        private static void BufferedCopy (Stream read, Stream write) {
            var buffer = new byte[1024 * 32];
            int bytes;
            do {
                bytes = read.Read(buffer, 0, buffer.Length);
                write.Write(buffer, 0, bytes);
            } while (bytes == buffer.Length);
        }
    }
}
