﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Nen.IO {
    public static class FileEx {
        public static void ForceDelete (string path) {
            MakeWritable(path);
            File.Delete(path);
        }

        public static void MakeWritable (string path) {
            var attributes = File.GetAttributes(path);
            if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                File.SetAttributes(path, attributes ^ FileAttributes.ReadOnly);
        }
    }
}
