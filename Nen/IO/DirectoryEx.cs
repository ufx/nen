﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Nen.IO {
    public static class DirectoryEx {
        public static void ForceDelete (string path) {
            foreach (var file in Directory.GetFiles(path, "*.*", SearchOption.AllDirectories))
                FileEx.MakeWritable(file);

            Directory.Delete(path, true);
        }
    }
}
