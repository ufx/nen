﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;

namespace Nen.Diagnostics {
    public class DebugWriter : TextWriter {
        #region Write
        public override void Write (bool value) {
            Debug.Write(value);
        }

        public override void Write (char value) {
            Debug.Write(value);
        }

        public override void Write (char[] buffer) {
            Debug.Write(buffer);
        }

        public override void Write (decimal value) {
            Debug.Write(value);
        }

        public override void Write (double value) {
            Debug.Write(value);
        }

        public override void Write (float value) {
            Debug.Write(value);
        }

        public override void Write (int value) {
            Debug.Write(value);
        }

        public override void Write (long value) {
            Debug.Write(value);
        }

        public override void Write (object value) {
            Debug.Write(value);
        }

        public override void Write (string format, params object[] arg) {
            Debug.Write(string.Format(format, arg));
        }

        public override void Write (string value) {
            Debug.Write(value);
        }
        #endregion

        public override Encoding Encoding {
            get { return Encoding.UTF8; }
        }

        public override void Flush () {
            base.Flush();
        }
    }
}
