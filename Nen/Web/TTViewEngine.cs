﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Web.Mvc;
using System.Web;
using Nen.Text.TreeTemplate;
using System.IO;

namespace Nen.Web {
    //public class TTViewEngine : VirtualPathProviderViewEngine {
    //    public TTViewEngine () {
    //        MasterLocationFormats = new[] {
    //            "~/Views/Layout/{0}.tree"
    //        };

    //        AreaMasterLocationFormats = new[] {
    //            "~/Areas/{2}/Views/Layout/{0}.tree"
    //        };

    //        ViewLocationFormats = PartialViewLocationFormats = new[] {
    //            "~/Views/{1}/{0}.tree",
    //            "~/Views/Shared/{0}.tree"
    //        };

    //        AreaViewLocationFormats = AreaPartialViewLocationFormats = new[] {
    //            "~/Areas/{2}/Views/{1}/{0}.tree", "~/Areas/{2}/Views/Shared/{0}.tree"
    //        };
    //    }

    //    protected override IView CreatePartialView (ControllerContext controllerContext, string partialPath) {
    //        return new TTView(partialPath, null);
    //    }

    //    protected override IView CreateView (ControllerContext controllerContext, string viewPath, string masterPath) {
    //        return new TTView(viewPath, masterPath);
    //    }
    //}

    //public class TTView : IView {
    //    public string ViewPath { get; set; }
    //    public string MasterPath { get; set; }

    //    public TTView (string viewPath, string masterPath) {
    //        ViewPath = viewPath;
    //        MasterPath = masterPath;
    //    }

    //    public void Render (ViewContext viewContext, System.IO.TextWriter writer) {
    //        var path = viewContext.HttpContext.ApplicationInstance.Server.MapPath(ViewPath);
    //        var builder = new TTXElementBuilder();

    //        builder.Environment["Model"] = viewContext.ViewData.Model;
    //        builder.Environment["Data"] = viewContext.ViewData;
    //        builder.Environment["Context"] = viewContext;
    //        builder.Environment["Sections"] = new Dictionary<string, object>();
    //        builder.AddTagLibrary("mvc", typeof(TTMvcTagLibrary));

    //        var expr = new TTParser().Parse(File.ReadAllText(path));

    //        var xml = builder.Evaluate(expr);
    //        writer.Write(xml.ToString());
    //    }
    //}

    //public class TTMvcTagLibrary {
    //    public object Section (TTXElementBuilder builder, TTElementExpression expr) {
    //        return null;
    //    }

    //    public object Placeholder (TTXElementBuilder builder, TTElementExpression expr) {
    //        return null;
    //    }

    //    public object Master (TTXElementBuilder builder, TTElementExpression expr) {
    //        return null;
    //    }
    //}
}
