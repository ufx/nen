﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.SqlServerCe;
using System.Globalization;
using System.Linq;
using System.Text;

using Nen.Internal;

namespace Nen.Data.SqlServerCe
{
    //public class SqlServerCeDatabase : Database {
    //    #region Constructors
    //    /// <summary>
    //    /// Constructs a new SqlServerCeDatabase.
    //    /// </summary>
    //    public SqlServerCeDatabase ()
    //        : this(CultureInfo.CurrentCulture) {
    //    }

    //    /// <summary>
    //    /// Constructs a new SqlServerCeDatabase.
    //    /// </summary>
    //    /// <param name="connectionString">The connection string to use when executing commands.</param>
    //    public SqlServerCeDatabase (string connectionString)
    //        : this(connectionString, CultureInfo.CurrentCulture) {
    //    }

    //    /// <summary>
    //    /// Constructs a new SqlServerCeDatabase.
    //    /// </summary>
    //    /// <param name="culture">The culture of the data stored in the database.</param>
    //    public SqlServerCeDatabase (CultureInfo culture)
    //        : this(null, culture) {
    //    }

    //    /// <summary>
    //    /// Constructs a new SqlServerCeDatabase.
    //    /// </summary>
    //    /// <param name="connectionString">The connection string to use when executing commands.</param>
    //    /// <param name="culture">The culture of the data stored in the database.</param>
    //    public SqlServerCeDatabase (string connectionString, CultureInfo culture)
    //        : base(connectionString, culture) {
    //    }
    //    #endregion

    //    #region Formatters
    //    public override SqlFormatter CreateFormatter () {
    //        return new SqlServerCeFormatter();
    //    }
    //    #endregion

    //    #region Generation
    //    public override IDataAdapter CreateDataAdapter (IDbCommand cmd) {
    //        return new SqlCeDataAdapter((SqlCeCommand) cmd);
    //    }

    //    protected override IDbCommand CreateCommandCore () {
    //        return new SqlCeCommand();
    //    }

    //    public override IDbConnection CreateConnection()
    //    {
    //        return new SqlCeConnection(ConnectionString);
    //    }

    //    protected override IDbDataParameter[] DeriveParameters (string procedureName) {
    //        throw new NotImplementedException(LS.T("This database does not support parameter derivation."));
    //    }
    //    #endregion
    //}
}
