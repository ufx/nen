﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using Nen.Data.SqlModel;

namespace Nen.Data.Oracle
{
    public class OracleFormatter : SqlFormatter
    {
        private int _compoundCounter = 0;
        private int _selectCounter = 0;

        #region Constructor
        /// <summary>
        /// Constucts a new SqlServerFormatter.
        /// </summary>
        public OracleFormatter()
            : base(() => new global::Oracle.DataAccess.Client.OracleCommand())
        {
        }
        #endregion

        #region Visitation
        /// <summary>
        /// Formats a select statement.
        /// </summary>
        /// <param name="sqlSelect">The select to format.</param>
        public override void Visit(SqlSelect sqlSelect)
        {
            // Open a refcursor if needed for a compound block.
            if (_compoundCounter > 0)
            {
                _selectCounter++;
                Text.Append("OPEN :" + _selectCounter + " FOR ");

                var cursor = (global::Oracle.DataAccess.Client.OracleParameter)Command.CreateParameter();
                cursor.ParameterName = ":N__REFCURSOR" + _selectCounter + "__";
                cursor.OracleDbType = global::Oracle.DataAccess.Client.OracleDbType.RefCursor;
                cursor.Direction = ParameterDirection.Output;
                Command.Parameters.Add(cursor);
            }

            // Window structure:

            // SELECT <*, or <sole column>>
            // FROM (
            //  SELECT <columns>, ROW_NUMBER() OVER(<orderbys>) AS N__ROW_NUMBER__
            //  FROM <queries> <joins>
            //  WHERE <expression>
            // ) AS N__WINDOW__
            // WHERE N__ROW_NUMBER__ > <offset>

            // SELECT <column1, column2, ..., columnN>
            Text.Append("SELECT ");

            if (sqlSelect.IsDistinct)
                Text.Append(" DISTINCT ");

            var where = sqlSelect.Where;
            SqlBinaryOperatorExpression wherePagination = null;

            var useWindow = false;
            if (sqlSelect.Offset.HasValue || sqlSelect.Limit.HasValue)
            {
                int? limit = sqlSelect.Limit;
                SqlIdentifier rowNumber;

                if (sqlSelect.OrderBy.Count == 0)
                {
                    // Use default rownum, so no windowing subselect necessary.
                    rowNumber = new SqlIdentifier("rownum");
                }
                else
                {
                    // OrderBy specified, windowing subselect necessary.
                    useWindow = true;
                    rowNumber = new SqlIdentifier("N__ROW_NUMBER__");

                    // An extra row number in the result set isn't a big deal, unless
                    // this is a subselect that must return only one expression.
                    // Handle this case.
                    if (sqlSelect.Columns.Count == 1)
                    {
                        Text.Append(sqlSelect.Columns.First().Alias);
                    }
                    else
                        Text.Append("*");

                    Text.Append(" FROM (SELECT ");
                }

                // Setup pagination where criteria.
                if (sqlSelect.Offset.HasValue)
                {
                    wherePagination = new SqlBinaryOperatorExpression(rowNumber, ">", new SqlLiteralExpression(sqlSelect.Offset.Value));
                    if (limit.HasValue)
                        limit += sqlSelect.Offset.Value;
                }

                if (limit.HasValue)
                    wherePagination = SqlBinaryOperatorExpression.Join(wherePagination, "AND", new SqlBinaryOperatorExpression(rowNumber, "<=", new SqlLiteralExpression(limit.Value)));

                if (!useWindow)
                    where = SqlBinaryOperatorExpression.Join(where, "AND", wherePagination);
            }

            VisitEach(", ", sqlSelect.Columns);

            if (useWindow)
            {
                Text.Append(", ROW_NUMBER() OVER(");
                GenerateOrderBy(sqlSelect);

                Text.Append(") N__ROW_NUMBER__");
            }

            // FROM <query1, query2, ..., queryN>
            Text.Append(" FROM ");

            if (sqlSelect.From.Count > 0)
                VisitEach(", ", sqlSelect.From);
            else // DUAL is implied if no other tables are specified.
                Text.Append("DUAL");

            // <join1 join2 ... joinN>
            VisitEach(" ", sqlSelect.Joins);

            // WHERE <expression>
            if (where != null)
            {
                Text.Append(" WHERE ");
                where.Accept(this);
            }

            // ORDER BY is not necessary if ROW_NUMBER() is present.
            if (useWindow)
            {
                // Finalize the query window.
                Text.Append(") N__WINDOW__ WHERE ");
                wherePagination.Accept(this);
            }
            else
            {
                // ORDER BY <query1, query2, ..., queryN>
                GenerateOrderBy(sqlSelect);
            }

            // GROUP BY <query1, query2, ..., queryN>
            if (sqlSelect.GroupBy.Count > 0)
            {
                Text.Append(" GROUP BY ");
                VisitEach(", ", sqlSelect.GroupBy);
            }
        }

        public override void Visit(SqlCompoundExpression sqlCompound)
        {
            _compoundCounter++;

            try
            {
                Text.Append("BEGIN\r\n");
                VisitEach("; \r\n", sqlCompound.Expressions);
                Text.Append("; \r\nEND;");

            }
            finally
            {
                _compoundCounter--;
            }
        }

        /// <summary>
        /// Formats a variable-length string data type.
        /// </summary>
        /// <param name="str">The variable-length string to format.</param>
        public override void Visit(SqlVariableLengthString str)
        {
            if (str.Length == int.MaxValue)
                Text.Append("CLOB");
            else
                Text.AppendFormat(CultureInfo.InvariantCulture, "VARCHAR2({0})", str.Length);
        }

        /// <summary>
        /// Formats a variable-length Unicode string data type.
        /// </summary>
        /// <param name="str">The variable-length Unicode string data type to format.</param>
        public override void Visit(SqlUnicodeVariableLengthString str)
        {
            if (str.Length == int.MaxValue)
                Text.Append("NCLOB");
            else
                Text.Append("NVARCHAR2(" + str.Length + ")");
        }

        /// <summary>
        /// Formats a globally unique identifier data type.
        /// </summary>
        /// <param name="guid">The globally unique identifier to format.</param>
        public override void Visit(SqlGuid guid)
        {
            // Don't use RAW(16) due to endianness problems.
            Text.Append("CHAR(32)");
        }

        /// <summary>
        /// Formats a variable-length binary data type.
        /// </summary>
        /// <param name="binary">The variable-length binary data type to format.</param>
        public override void Visit(SqlVariableLengthBinary binary)
        {
            Text.Append("BLOB");
        }

        /// <summary>
        /// Formats a boolean data type.
        /// </summary>
        /// <param name="boolean">The boolean to format.</param>
        public override void Visit(SqlBoolean boolean)
        {
            Text.Append("CHAR(1)");
        }

        /// <summary>
        /// Formats a function.
        /// </summary>
        /// <param name="function">The function to format.</param>
        public override void Visit(SqlFunction function)
        {
            // All dates are represented as numbers where 1 == 1 day.
            // Represent all date arithmetic in this format.

            if (function.FunctionName == "CURRENT_TIMESTAMP")
            {
                Text.Append("SYSDATE");
                return;
            }
            else if (function.FunctionName == "datediff")
            {
                if (function.FunctionArguments == null || function.FunctionArguments.Count != 3)
                    throw new ArgumentException("datediff take only three arguments", "function");

                var arguments = function.FunctionArguments.ToArray();

                var timeSpanProperty = (SqlLiteralExpression)arguments[0];

                // todo: use MONTHS_BETWEEN for months and years properties.

                Text.Append("(CAST(");
                arguments[1].Accept(this);
                Text.Append(" AS DATE) - CAST(");
                arguments[2].Accept(this);
                Text.Append("AS DATE))");

                var multiplier = GetDayMultiplier((string)timeSpanProperty.Value);
                if (multiplier != 1)
                    Text.Append(" * " + multiplier.ToString());
            }
            else if (function.FunctionName == "dateadd")
            {
                if (function.FunctionArguments == null || function.FunctionArguments.Count != 3)
                    throw new ArgumentException("datediff take only three arguments", "function");

                var arguments = function.FunctionArguments.ToArray();

                var timeSpanProperty = (SqlLiteralExpression)arguments[0];

                // todo: use MONTHS_ADD for months and years properties.

                Text.Append("(");
                arguments[1].Accept(this);
                Text.Append(" + (");

                arguments[2].Accept(this);

                var multiplier = GetDayMultiplier((string)timeSpanProperty.Value);
                if (multiplier != 1)
                    Text.Append(" / " + multiplier.ToString());

                Text.Append("))");
            }
            else
                base.Visit(function);
        }

        public override void Visit(SqlTime time)
        {
            Text.Append("TIMESTAMP");
        }

        public override void Visit(SqlAlterTable alterTable)
        {
            if (alterTable == null)
                throw new ArgumentNullException("alterTable");

            Text.Append("ALTER TABLE ");
            GenerateCompoundIdentifierName(alterTable.SchemaName, alterTable.Name);

            if (alterTable.Constraints.Count > 0)
            {
                Text.Append(" ADD (\r\n    ");
                VisitEach(", \r\n    ", alterTable.Constraints);
                Text.Append(")");
            }
            else if (alterTable.AddedColumns.Count > 0)
            {
                Text.Append(" ADD \r\n    ");
                VisitEach(", \r\n    ", alterTable.AddedColumns);
            }
        }

        public override void Visit(SqlProcedure sqlProcedure)
        {
            throw new NotImplementedException();
        }

        private int GetDayMultiplier(string timeSpanProperty)
        {
            switch (timeSpanProperty)
            {
                case "Days": return 1;
                case "Hours": return 24;
                case "Milliseconds": return 86400000;
                case "Minutes": return 1400;
                case "Seconds": return 86400;
                default:
                    throw new NotImplementedException();
            }
        }
        #endregion

        #region Parameters and Values
        protected override IDbDataParameter GetParameter(object value, DbType? dbType)
        {
            // ODP.Net can't support parameter reuse, so create this parameter anew every time.
            return CreateParameter(value, dbType);
        }

        public override string FormatParameterName(string name)
        {
            return ":" + name;
        }

        /// <summary>
        /// Determines whether the identifier is a keyword.
        /// </summary>
        /// <param name="identifier">The possible keyword identifier.</param>
        /// <returns>true if the identifier is a keyword, false if otherwise.</returns>
        protected override bool IsKeyword(string identifier)
        {
            if (base.IsKeyword(identifier))
                return true;

            switch (identifier.ToLower())
            {
                case "date":
                case "number":
                case "sequence":
                case "level":
                case "user":
                    return true;
            }

            return false;
        }

        protected override DbType GetDbType(DbType type)
        {
            switch (type)
            {
                case DbType.Guid:
                    return DbType.AnsiString;

                case DbType.Boolean:
                    return DbType.AnsiStringFixedLength;
            }

            return type;
        }

        public override object FormatValue(object value)
        {
            if (value is Guid)
                return ((Guid)value).ToString("N");
            else if (value is bool)
                return ((bool)value) ? "Y" : "N";
            else if (value is TimeSpan)
                return new DateTime(0001, 01, 01).Add((TimeSpan)value);

            return base.FormatValue(value);
        }
        #endregion
    }
}
