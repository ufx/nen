﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;
using Nen.Data.ObjectRelationalMapper.Mapping;

namespace Nen.Data.Oracle
{
    public class OracleConventions : SequenceConventions
    {
        public override string GetDataIdentifierName(string[] parts, DataIdentifierType type)
        {
            // Standard convention is to UPPER_CASE identifiers.
            var cased = string.Join("_", parts.Select(StringEx.ToUpperWithUnderscores));

            // All identifiers are limited to 30 characters.  Cut them off.
            return cased.Length > 30 ? cased.Substring(0, 30) : cased;
        }

        public override int? GetIdentifierLengthLimit()
        {
            // All identifiers are limited to 30 characters.
            return 30;
        }
    }
}
