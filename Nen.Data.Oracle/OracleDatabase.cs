﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using System.Globalization;
using System.Data;
using System.Collections.ObjectModel;

namespace Nen.Data.Oracle
{
    public class OracleDatabase : Database
    {
        #region Constructors
        /// <summary>
        /// Constructs a new OracleDatabase.
        /// </summary>
        public OracleDatabase()
            : this(CultureInfo.CurrentCulture)
        {
        }

        /// <summary>
        /// Constructs a new OracleDatabase.
        /// </summary>
        /// <param name="connectionString">The connection string to use when executing commands.</param>
        public OracleDatabase(string connectionString)
            : this(connectionString, CultureInfo.CurrentCulture)
        {
        }

        /// <summary>
        /// Constructs a new OracleDatabase.
        /// </summary>
        /// <param name="culture">The culture of the data stored in the database.</param>
        public OracleDatabase(CultureInfo culture)
            : this(null, culture)
        {
        }

        /// <summary>
        /// Constructs a new SqlDatabase.
        /// </summary>
        /// <param name="connectionString">The connection string to use when executing commands.</param>
        /// <param name="culture">The culture of the data stored in the database.</param>
        public OracleDatabase(string connectionString, CultureInfo culture)
            : base(connectionString, culture)
        {
        }
        #endregion

        #region Generation
        /// <summary>
        /// Creates a data adapter to be used with this database.
        /// </summary>
        /// <param name="cmd">The select command this data adapter is for.</param>
        /// <returns>A data adapter to be used with this database.</returns>
        public override IDataAdapter CreateDataAdapter(IDbCommand cmd)
        {
            return new OracleDataAdapter((OracleCommand)cmd);
        }

        /// <summary>
        /// Creates a command to be used with this database.
        /// </summary>
        /// <returns>A command to be used with this database.</returns>
        protected override IDbCommand CreateCommandCore()
        {
            return new OracleCommand();
        }

        /// <summary>
        /// Creates a connection to be used with this database.
        /// </summary>
        /// <returns>A connection to be used with this database.</returns>
        public override IDbConnection CreateConnection()
        {
            return new OracleConnection();
        }

        /// <summary>
        /// Creates a formatter to be used with this database.
        /// </summary>
        /// <returns>A formatter to be used with this database.</returns>
        public override SqlFormatter CreateFormatter()
        {
            return new OracleFormatter();
        }

        /// <summary>
        /// Derives the list of parameters needed to execute a procedure.
        /// </summary>
        /// <param name="procedureName">The procedure name.</param>
        /// <returns>An array of parameters to be used with the procedure.</returns>
        protected override IDbDataParameter[] DeriveParameters(string procedureName)
        {
            using (var conn = new OracleConnection(ConnectionString))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = procedureName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;
                    OracleCommandBuilder.DeriveParameters(cmd);
                    return cmd.Parameters.Cast<OracleParameter>().Select(p => (OracleParameter)p.Clone()).ToArray();
                }
            }
        }
        #endregion

        #region Schema
        public override DataSchema GetSchema()
        {
            var schema = new DataSchema();

            // Fetch tables and their schema
            schema.Tables = GetSchemaTables();

            return schema;
        }

        private Collection<DataTable> GetSchemaTables()
        {
            // todo: indexes
            // todo: other constraints
            return null;
        }

        private void GetTableConstraints(Dictionary<string, DataTable> tablesByName)
        {
            throw new NotImplementedException();
        }
        #endregion

        public override bool AreModifiedRowsReturnedAccurate
        {
            get { return false; }
        }
    }
}
