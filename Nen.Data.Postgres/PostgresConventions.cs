﻿using Nen.Data.ObjectRelationalMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nen.Data.Postgres
{
    public class PostgresConventions : SequenceConventions
    {
        public override string GetDataIdentifierName(string[] parts, DataIdentifierType type)
        {
            // Standard convention is to lower_case identifiers.
            return string.Join("_", parts.Select(StringEx.ToLowerWithUnderscores));
        }
    }
}
