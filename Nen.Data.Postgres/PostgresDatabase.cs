﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nen.Data.Postgres
{
    public class PostgresDatabase : Database
    {
        #region Constructors
        /// <summary>
        /// Constructs a new PostgresDatabase.
        /// </summary>
        public PostgresDatabase()
            : this(CultureInfo.CurrentCulture)
        {
        }

        /// <summary>
        /// Constructs a new PostgresDatabase.
        /// </summary>
        /// <param name="connectionString">The connection string to use when executing commands.</param>
        public PostgresDatabase(string connectionString)
            : this(connectionString, CultureInfo.CurrentCulture)
        {
        }

        /// <summary>
        /// Constructs a new PostgresDatabase.
        /// </summary>
        /// <param name="culture">The culture of the data stored in the database.</param>
        public PostgresDatabase(CultureInfo culture)
            : this(null, culture)
        {
        }

        /// <summary>
        /// Constructs a new SqlDatabase.
        /// </summary>
        /// <param name="connectionString">The connection string to use when executing commands.</param>
        /// <param name="culture">The culture of the data stored in the database.</param>
        public PostgresDatabase(string connectionString, CultureInfo culture)
            : base(connectionString, culture)
        {
        }
        #endregion

        #region Generation
        /// <summary>
        /// Creates a data adapter to be used with this database.
        /// </summary>
        /// <param name="cmd">The select command this data adapter is for.</param>
        /// <returns>A data adapter to be used with this database.</returns>
        public override IDataAdapter CreateDataAdapter(IDbCommand cmd)
        {
            return new NpgsqlDataAdapter((NpgsqlCommand)cmd);
        }

        /// <summary>
        /// Creates a command to be used with this database.
        /// </summary>
        /// <returns>A command to be used with this database.</returns>
        protected override IDbCommand CreateCommandCore()
        {
            return new NpgsqlCommand();
        }

        /// <summary>
        /// Creates a connection to be used with this database.
        /// </summary>
        /// <returns>A connection to be used with this database.</returns>
        public override IDbConnection CreateConnection()
        {
            return new NpgsqlConnection();
        }

        /// <summary>
        /// Creates a formatter to be used with this database.
        /// </summary>
        /// <returns>A formatter to be used with this database.</returns>
        public override SqlFormatter CreateFormatter()
        {
            return new PostgresFormatter();
        }

        /// <summary>
        /// Derives the list of parameters needed to execute a procedure.
        /// </summary>
        /// <param name="procedureName">The procedure name.</param>
        /// <returns>An array of parameters to be used with the procedure.</returns>
        protected override IDbDataParameter[] DeriveParameters(string procedureName)
        {
            using (var conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = procedureName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;
                    NpgsqlCommandBuilder.DeriveParameters(cmd);
                    return cmd.Parameters.Cast<NpgsqlParameter>().Select(p => (NpgsqlParameter)p.Clone()).ToArray();
                }
            }
        }
        #endregion

        /// <summary>
        /// Prepare is technically supported, but it does not support multiple commands
        /// so it is not useful to us.
        /// </summary>
        public override bool IsPrepareSupported
        {
            get { return false; }
        }
    }
}
