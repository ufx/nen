﻿using Nen.Data.SqlModel;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nen.Data.Postgres
{
    public class PostgresFormatter : SqlFormatter
    {
        #region Constructor
        public PostgresFormatter()
            : base(() => new NpgsqlCommand())
        {
        }
        #endregion

        #region Statement Visitation
        public override void Visit(SqlSelect sqlSelect)
        {
            base.Visit(sqlSelect);

            if (sqlSelect.Limit.HasValue)
                Text.Append(" LIMIT " + sqlSelect.Limit.Value);

            if (sqlSelect.Offset.HasValue)
                Text.Append(" OFFSET " + sqlSelect.Offset.Value);
        }

        public override void Visit(SqlProcedure sqlProcedure)
        {
            if (sqlProcedure == null)
                throw new ArgumentNullException("sqlProcedure");

            Text.Append("SELECT * FROM ");
            Text.Append(sqlProcedure.ProcedureName);
            Text.Append("(");

            if (sqlProcedure.ProcedureArguments.Count > 0)
            {
                Text.Append(" ");
                VisitEach(", ", sqlProcedure.ProcedureArguments);
            }
            Text.Append(")");
        }
        #endregion

        #region Fragment Visitation
        public override void Visit(SqlFunction function)
        {
            if (function.FunctionName == "dateadd")
            {
                // [date] + '[number] [type]'

                var args = function.FunctionArguments.ToArray();

                args[2].Accept(this);
                Text.Append(" + ");

                Text.Append("'");

                var number = (SqlLiteralExpression)args[1];
                Text.Append(number.Value.ToString());
                Text.Append(" ");

                var unit = (SqlLiteralExpression)args[0];
                Text.Append(unit.Value.ToString());
                Text.Append("'");
            }
            else if (function.FunctionName == "datediff")
            {
                // extract(epoch from [date] - [date]) / (unit amount in seconds)

                var args = function.FunctionArguments.ToArray();

                Text.Append("extract(epoch from ");
                args[1].Accept(this);
                Text.Append(" - ");
                args[2].Accept(this);
                Text.Append(") / ");

                // Append unit divisor
                var unit = (SqlLiteralExpression) args[0];
                var divisor = GetSecondDivisor(unit.Value.ToString());
                Text.Append(divisor);
            }
            else
                base.Visit(function);
        }

        private float GetSecondDivisor(string unit)
        {
            switch (unit)
            {
                case "Days": return 86400;
                case "Hours": return 3600;
                case "Milliseconds": return .001f;
                case "Minutes": return 60;
                case "Seconds": return 1;
                default:
                    throw new NotImplementedException();
            }
        }

        #endregion

        #region DataType and Declaration Visitation
        public override void Visit(SqlVariableLengthBinary binary)
        {
            Text.Append("BYTEA");
        }

        public override void Visit(SqlGuid guid)
        {
            Text.Append("UUID");
        }

        public override void Visit(SqlBoolean boolean)
        {
            Text.Append("BOOL");
        }

        public override void Visit(SqlUnicodeVariableLengthString str)
        {
            Visit(new SqlVariableLengthString(str.Length));
        }
        #endregion

        #region Schema Visitation
        public override void Visit(SqlAlterTable alterTable)
        {
            if (alterTable == null)
                throw new ArgumentNullException("alterTable");

            Text.Append("ALTER TABLE ");
            GenerateCompoundIdentifierName(alterTable.SchemaName, alterTable.Name);

            if (alterTable.AddedColumns.Count > 0)
            {
                Text.Append(" \r\n    ADD COLUMN ");
                VisitEach(", \r\n    ADD COLUMN ", alterTable.AddedColumns);
            }

            if (alterTable.Constraints.Count > 0)
            {
                Text.Append(" \r\n    ADD ");
                VisitEach(", \r\n    ADD ", alterTable.Constraints);
            }
        }
        #endregion

        #region Miscellaneous
        protected override bool IsKeyword(string identifier)
        {
            if (base.IsKeyword(identifier))
                return true;

            switch (identifier.ToLower())
            {
                case "user":
                case "sequence":
                    return true;
            }

            return false;
        }
        #endregion
    }
}
