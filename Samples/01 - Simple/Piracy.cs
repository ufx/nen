﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nen.Data.ObjectRelationalMapper;

namespace SimpleModels.Models {
    [Persistent]
    public class PirateShip {
        public string Name { get; set; }
        public DateTime DateBuilt { get; set; }
        public int NumCdsInCargo { get; set; }
    }
}
