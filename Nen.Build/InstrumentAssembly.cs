﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Utilities;
using System.Reflection;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;

namespace Nen.Build {
	public class InstrumentAssembly: AppDomainIsolatedTask {
		public string Source { get; set; }
		public string SigningKey { get; set; }

		public override bool Execute () {
			var transformer = new DataRefTransformer();
			transformer.Discombobulate(Source);

			return true;
		}
	}
}
